package com.agrosolutions.sai.view;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import org.apache.log4j.Logger;
//import org.marre.sms.SmsException;
import org.primefaces.event.map.OverlaySelectEvent;
import org.primefaces.model.chart.CartesianChartModel;
import org.primefaces.model.chart.ChartSeries;
import org.primefaces.model.map.DefaultMapModel;
import org.primefaces.model.map.LatLng;
import org.primefaces.model.map.MapModel;
import org.primefaces.model.map.Marker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.exception.SaiException;
import com.agrosolutions.sai.model.Irrigacao;
import com.agrosolutions.sai.model.Irrigante;
import com.agrosolutions.sai.model.Plantio;
import com.agrosolutions.sai.model.Setor;
import com.agrosolutions.sai.model.TipoPerfil;
import com.agrosolutions.sai.service.IrrigacaoService;
import com.agrosolutions.sai.service.IrriganteService;
import com.agrosolutions.sai.service.PlantioService;
import com.agrosolutions.sai.service.SetorService;
import com.agrosolutions.sai.service.SmsService;
import com.agrosolutions.sai.util.ResourceBundle;

/**
* Use case : Consultar Fazenda
* 
* @since   : Fev 21, 2011, 03:23:20 AM
* @author  : rpf1404@gmail.com
*/

@Component("consultarFazendaBean")
@Scope("session")
public class ConsultarFazendaBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = 5894513222000965523L;

	public static Logger logger = Logger.getLogger(ConsultarFazendaBean.class);

	public static final String MSG_TITULO = "Consultar Fazenda";

	@Autowired
	private IrriganteService irriganteService;
	@Autowired
	private PlantioService plantioService;
	@Autowired
	private SetorService setorService;
	@Autowired
	private IrrigacaoService irrigacaoService;
	@Autowired
	private SmsService smsService;
	
	private Irrigante irrigante;
	private List<Setor> setores;
	private Plantio plantioSelecionado;
	private Irrigante irriganteSelecionado;
	private Setor setorSelecionado;

	private Boolean modoSatelite;
	private Marker marker;  	
    private String central;
    private CartesianChartModel grPlantioTempo;  
    private MapModel irriganteMarcado; 
    
	public void carregarDadosIniciais() {
		if (irrigante == null && getUsuarioLogado().getPerfil().getTipo().equals(TipoPerfil.Irrigante)) {
			irrigante = irriganteService.pesquisarPorUsuario(getUsuarioLogado());
		}
		
		if(setores==null){
			setores = setorService.pesquisar(irrigante);
			for (Setor s : setores) {
				s.setPlantio(plantioService.pesquisarPorSetor(s));
			}
		}
		
		criarGraficoTempo();
	}

	public String voltar() {
		return "consultarIrrigante?faces-redirect=true";
	}
	
    public void modoSatelite() {
 	  	modoSatelite = true; 
 	  	
 	  	adicionarMarcador();
	}
    private void adicionarMarcador(){
    	irriganteMarcado = new DefaultMapModel();
     	LatLng coord;
    
     	String latitude, longitude,nome;
     	
     	latitude = getIrrigante().getLatitude();
     	longitude = getIrrigante().getLongitude();
     	nome = getIrrigante().getNome();
     	
     	coord = new LatLng(Double.parseDouble(latitude),Double.parseDouble(longitude)); 
 		
     	central = latitude + "," + longitude;
     	
     	irriganteMarcado.addOverlay(new Marker(coord,nome,irrigante,"http://maps.google.com/mapfiles/ms/micons/blue-dot.png"));
    }
    
    public void marcadorSelecionado(OverlaySelectEvent event) {  
        marker = (Marker) event.getOverlay();  
        irriganteSelecionado = (Irrigante) marker.getData();
    }

    public void modoTabela() {
    	modoSatelite = false;
	}

    private void criarGraficoTempo() {  
    	grPlantioTempo = new CartesianChartModel();  
    	Date dataFim = new Date();
    	Calendar dataIni = new GregorianCalendar();
    	dataIni.setTime(dataFim);
    	dataIni.roll(Calendar.DAY_OF_MONTH, -5);
    	List<Irrigacao> lista = irrigacaoService.pesquisarPorIrrigantePeriodo(dataIni.getTime(), dataFim, irrigante);
    	List<String> listaPlantio = new ArrayList<String>();
    	//Identificando os plantios
    	for (Irrigacao irrigacao : lista) {
    		if (!listaPlantio.contains("Setor " + irrigacao.getPlantio().getSetor().getIdentificacao() + " " + irrigacao.getPlantio().getCultivo().getVariedade().getNome())) {
    			listaPlantio.add("Setor " + irrigacao.getPlantio().getSetor().getIdentificacao() + " " + irrigacao.getPlantio().getCultivo().getVariedade().getNome());
			}
		}
        ChartSeries[] p = new ChartSeries[listaPlantio.size()];  
		int i = 0;
    	for (String l : listaPlantio) {
            p[i] = new ChartSeries();
            p[i].setLabel(l);
        	for (Irrigacao irrigacao : lista) {
        		if (l.equals("Setor " + irrigacao.getPlantio().getSetor().getIdentificacao() + " " + irrigacao.getPlantio().getCultivo().getVariedade().getNome())) {
        			Calendar data = new GregorianCalendar();
        			data.setTime(irrigacao.getData());
        	        p[i].set(""+data.get(Calendar.DAY_OF_MONTH), irrigacao.getTempoIrrigacao());  
    			}
    		}
        	grPlantioTempo.addSeries(p[i]);
    		i++;
		}
    }      
//   public void enviarSMS() throws SaiException, SmsException{
//		List<Irrigacao> irrigacoes = irrigacaoService.pesquisarPorIrriganteData(new Date(), irrigante);
//		Long idSetorLido = null;
//		Long idSetor = 0L;
//		String msg = "";
//		String tel = "";
//		int qtdMsg=0;
//		if (!irrigacoes.isEmpty()) {
//		   	tel = "55"+ irrigante.getCelular().replace("(", "").replace(")", "").replace("-", "");
//			msg = "SAI-INFORMA:" + (irrigante.getLocalizacao().length()>6?irrigante.getLocalizacao().substring(0,6):irrigante.getLocalizacao()) + "\n";
//			for (Irrigacao i : irrigacoes) {
//				idSetorLido = i.getPlantio().getSetor().getId();
//				String tempo = i.getTemIrrigacao()?converterTempo(i.getTempoIrrigacao()):"0";
//				if (idSetor != idSetorLido) {
//					idSetor = idSetorLido;
//					if ((msg + "S-" +  i.getPlantio().getSetor().getIdentificacao() + "\n  " + (i.getPlantio().getCultivo().getCultura().getNome().split(" "))[0] + " " + (i.getPlantio().getCultivo().getVariedade().getNome().split(" "))[0] + " " + tempo + "\n").length() > 150) {
//						qtdMsg++;
//						System.out.println("SMS Enviado para:" + tel + " - Tamanho msg:" + msg.length());
//						System.out.println(msg);
//						smsService.enviarSMS(tel, msg);
//						msg = "SAI-INFORMA(cont.):" + (irrigante.getLocalizacao().length()>6?irrigante.getLocalizacao().substring(0,6):irrigante.getLocalizacao()) + "\n";
//					}
//					msg = msg + "S-" +  i.getPlantio().getSetor().getIdentificacao() + "\n";
//				}
//				if (mesmaCultura(i, irrigacoes)){
//					Calendar data = new GregorianCalendar();
//					data.setTime(i.getPlantio().getCultivo().getDataInicio());
//					String dataCultivo = data.get(Calendar.DAY_OF_MONTH) + "/" + data.getDisplayName(Calendar.MONTH, 0, Locale.getDefault())+ "/" + data.get(Calendar.YEAR); 
//					msg = msg + "  " + trata((i.getPlantio().getCultivo().getCultura().getNome().split(" "))[0] + " " + (i.getPlantio().getCultivo().getVariedade().getNome().split(" "))[0]) + " de " +  dataCultivo + "-" + tempo + "\n";
//				}else{
//					msg = msg + "  " + trata((i.getPlantio().getCultivo().getCultura().getNome().split(" "))[0] + " " + (i.getPlantio().getCultivo().getVariedade().getNome().split(" "))[0]) + "-" + tempo + "\n";
//				}
//			}
//			qtdMsg++;
//			System.out.println("SMS Enviado para:" + tel + " - Tamanho msg:" + msg.length());
//			System.out.println(msg);
//			System.out.println("Total msg = " + qtdMsg);
//			smsService.enviarSMS(tel, msg);
//			infoMsg(ResourceBundle.getMessage("smsEnviado")+ ".", null);
//		}else{
//			errorMsg(ResourceBundle.getMessage("fazendaInfo")+ ".", null);
//		}
//	}
	
	private String converterTempo(BigDecimal tempo){
		int minutos = tempo.intValue();   
		int minuto = minutos % 60;   
		int hora = minutos / 60;   
		String hms = String.format ("%02dh:%02dm", hora, minuto);   
		return (hms);  
		
	}

	private boolean mesmaCultura(Irrigacao i, List<Irrigacao> lista){
		int x = 0;
		for (Irrigacao ir : lista) {
			if (ir.getPlantio().getSetor().equals(i.getPlantio().getSetor()) && ir.getPlantio().getCultivo().getVariedade().equals(i.getPlantio().getCultivo().getVariedade())) {
				x++;
			}
		}
		if (x > 1) {
			return true;  
		}else{
			return false;  
		}
	}
   
	public String irrigacao() {
		return "/paginas/irrigante/consultarIrrigacao?faces-redirect=true";
	}

	public String eficiencia() {
		return "cadastrarEficiencia?faces-redirect=true";
	}

	public Irrigante getIrrigante() {
		return irrigante;
	}

	public void setIrrigante(Irrigante irrigante) {
		this.irrigante = irrigante;
	}

	public Plantio getPlantioSelecionado() {
		return plantioSelecionado;
	}

	public void setPlantioSelecionado(Plantio plantioSelecionado) {
		this.plantioSelecionado = plantioSelecionado;
	}

	public List<Setor> getSetores() {
		return setores;
	}

	public void setSetores(List<Setor> setores) {
		this.setores = setores;
	}

	public Setor getSetorSelecionado() {
		return setorSelecionado;
	}

	public void setSetorSelecionado(Setor setorSelecionado) {
		this.setorSelecionado = setorSelecionado;
	}

    public Boolean getModoSatelite() {
		return modoSatelite;
	}

	public void setModoSatelite(Boolean modoSatelite) {
		this.modoSatelite = modoSatelite;
	}

	public Marker getMarker() {
		return marker;
	}

	public void setMarker(Marker marker) {
		this.marker = marker;
	}

	public String getCentral() {
		return central;
	}

	public void setCentral(String central) {
		this.central = central;
	}

	public String trata (String passa){  
	      passa = passa.replaceAll("[�����]","A");  
	      passa = passa.replaceAll("[�����]","a");  
	      passa = passa.replaceAll("[����]","E");  
	      passa = passa.replaceAll("[����]","e");  
	      passa = passa.replaceAll("����","I");  
	      passa = passa.replaceAll("����","i");  
	      passa = passa.replaceAll("[�����]","O");  
	      passa = passa.replaceAll("[�����]","o");  
	      passa = passa.replaceAll("[����]","U");  
	      passa = passa.replaceAll("[����]","u");  
	      passa = passa.replaceAll("�","C");  
	      passa = passa.replaceAll("�","c");   
	      passa = passa.replaceAll("[��]","y");  
	      passa = passa.replaceAll("�","Y");  
	      passa = passa.replaceAll("�","n");  
	      passa = passa.replaceAll("�","N");  
	      passa = passa.replaceAll("['<>\\|/]","");  
	      return passa;  
	   }

	public CartesianChartModel getGrPlantioTempo() {
		return grPlantioTempo;
	}

	public void setGrPlantioTempo(CartesianChartModel grPlantioTempo) {
		this.grPlantioTempo = grPlantioTempo;
	}

	public void setIrriganteMarcado(MapModel irriganteMarcado) {
		this.irriganteMarcado = irriganteMarcado;
	}

	public MapModel getIrriganteMarcado() {
		return irriganteMarcado;
	}

	public void setIrriganteSelecionado(Irrigante irriganteSelecionado) {
		this.irriganteSelecionado = irriganteSelecionado;
	}

	public Irrigante getIrriganteSelecionado() {
		return irriganteSelecionado;
	}  

}
