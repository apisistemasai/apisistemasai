package com.agrosolutions.sai.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.Cultura;
import com.agrosolutions.sai.model.Variedade;
import com.agrosolutions.sai.service.CulturaService;
import com.agrosolutions.sai.service.VariedadeService;
import com.agrosolutions.sai.util.ResourceBundle;

/**
* Use case : Consultar Variedade
* 
* @since   : Jan 17, 2012, 20:23:20 PM
* @author  : raphael.ferreira@ivia.com.br
*/

@Component("consultarVariedadeBean")
@Scope("view")
public class ConsultarVariedadeBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = 7438614384486558667L;

	public static Logger logger = Logger.getLogger(ConsultarVariedadeBean.class);

	public static final String MSG_TITULO = "Consultar Variedades";

	@Autowired
	private VariedadeService variedadeService;
	@Autowired
	private CulturaService culturaService;
	
	private LazyDataModel<Variedade> variedades;
	private Variedade variedadeSelecionado;
	private Variedade variedadeBusca;
	private List<Cultura> culturas;
	
	public void carregarDadosIniciais() {
		if (variedadeBusca == null){
			variedadeBusca = new Variedade();
			variedadeBusca.setSortField("nome");
			variedadeBusca.setSortOrder(SortOrder.ASCENDING);
			
			this.setCulturas(culturaService.obterTodos());
			consultar();
		}
	}
	public void consultar(){ 
		variedades = new LazyDataModel<Variedade>() {

			private static final long serialVersionUID = -4451428626988853159L;

			@Override
			public List<Variedade> load(int first, int pageSize, String sortField,
					SortOrder sortOrder, Map<String, String> filters) {
				List<Variedade> variedades = new ArrayList<Variedade>();
		
		        if(sortField == null){
					sortField = "nome";
					sortOrder = SortOrder.ASCENDING;
				}
		        
				variedadeBusca.setSortField(sortField);
				variedadeBusca.setSortOrder(sortOrder);
				variedades = variedadeService.pesquisar(variedadeBusca, first, pageSize);
		
				return variedades;
			}
		 };
		 int quantidade = variedadeService.quantidade(variedadeBusca);
		 variedades.setRowCount(quantidade==0?1:quantidade);
	}
	
    public void excluir(){
	    variedadeService.excluir(variedadeSelecionado);
		consultar();
		infoMsg(ResourceBundle.getMessage("variedadeExcluir")+ ".", "");
    }

	public String cadastro() {
		return "cadastrarVariedade?faces-redirect=true";
	}

	public LazyDataModel<Variedade> getVariedades() {
		return variedades;
	}

	public void setVariedades(LazyDataModel<Variedade> variedades) {
		this.variedades = variedades;
	}

	public Variedade getVariedadeSelecionado() {
		return variedadeSelecionado;
	}

	public void setVariedadeSelecionado(Variedade variedadeSelecionado) {
		this.variedadeSelecionado = variedadeSelecionado;
	}

	public void setVariedadeBusca(Variedade variedadeBusca) {
		this.variedadeBusca = variedadeBusca;
	}

	public Variedade getVariedadeBusca() {
		return variedadeBusca;
	}
	public void setCulturaService(CulturaService culturaService) {
		this.culturaService = culturaService;
	}
	public CulturaService getCulturaService() {
		return culturaService;
	}
	public void setCulturas(List<Cultura> culturas) {
		this.culturas = culturas;
	}
	public List<Cultura> getCulturas() {
		return culturas;
	}
	
	
}
