package com.agrosolutions.sai.view;

import java.awt.Color;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.primefaces.event.map.OverlaySelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.map.DefaultMapModel;
import org.primefaces.model.map.LatLng;
import org.primefaces.model.map.MapModel;
import org.primefaces.model.map.Marker;
import org.primefaces.model.map.Polyline;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.Coordenada;
import com.agrosolutions.sai.model.Cultura;
import com.agrosolutions.sai.model.Municipio;
import com.agrosolutions.sai.model.Variedade;
import com.agrosolutions.sai.service.CoordenadaService;
import com.agrosolutions.sai.service.CulturaService;
import com.agrosolutions.sai.service.MunicipioService;
import com.agrosolutions.sai.service.VariedadeService;
import com.agrosolutions.sai.util.ResourceBundle;

/**
 * Use case : Consultar Municipio
 * 
 * @since : Dez 30, 2011, 20:23:20 PM
 * @author : raphael@iasoft.me
 */

@Component("consultarMunicipioBean")
@Scope("session")
public class ConsultarMunicipioBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = 6366889099407071985L;

	public static Logger logger = Logger
			.getLogger(ConsultarMunicipioBean.class);

	@Autowired
	private MunicipioService municipioService;
	@Autowired
	private CulturaService culturaService;
	@Autowired
	private VariedadeService variedadeService;
	@Autowired
	private CoordenadaService coordenadaService;

	private LazyDataModel<Municipio> municipios;
	private Municipio municipioBusca;
	private Municipio municipioSelecionado;
	private List<Cultura> culturas;
	private List<Variedade> variedades;

	private Boolean modoSatelite = false;
	private MapModel municipioMarcado;
	private Marker marker;
	private String central;
	private String localizacao;
	private StreamedContent file;
	private Integer zoomMapa;
	private MapModel model;
	
	public void carregarDadosIniciais() {
		
		if (municipioBusca == null) {
			municipioBusca = new Municipio();
			municipioBusca.setSortField("nome");
			municipioBusca.setSortOrder(SortOrder.ASCENDING);
			
			setZoomMapa(4);
			
			municipioBusca.setBacia(getUsuarioLogado().getBacias().get(0));
			central = municipioBusca.getBacia().getLatitude()+ ","+ municipioBusca.getBacia().getLongitude();


			// Popular combobox de filtros
			this.culturas = culturaService.obterTodos();

			// Fazer consulta inicial
			consultar();
		}

		if (localizacao != null && !localizacao.isEmpty()) {
			municipioBusca.setLocalizacao(localizacao);
			localizacao = null;
			// Fazer consulta inicial
			consultar();
		}
	}


	public void carregarVariedades() {
		if (municipioBusca.getCultura() == null) {
			variedades = null;
		} else {
			variedades = variedadeService
					.pesquisar(municipioBusca.getCultura());
		}
		municipioBusca.setVariedade(null);
	}

	public String atividadeEconomica() {
		return "consultarAtividadesEconomica?faces-redirect=true";
	}

	public void consultar() {
		municipios = new LazyDataModel<Municipio>() {
		
			private static final long serialVersionUID = -5194413540113728712L;

			@Override
			public List<Municipio> load(int first, int pageSize,
					String sortField, SortOrder sortOrder,
					Map<String, String> filters) {
				List<Municipio> municipios = new ArrayList<Municipio>();

				if (sortField == null) {
					sortField = "nome";
					sortOrder = SortOrder.ASCENDING;
				}

				municipioBusca.setSortField(sortField);
				municipioBusca.setSortOrder(sortOrder);

				municipios = municipioService.pesquisar(municipioBusca, first, pageSize);

				return municipios;
			}
		};
		int quantidade = municipioService.quantidade(municipioBusca);
		municipios.setRowCount(quantidade);
		if (modoSatelite) {
			modoSatelite();
		}
	}

	public void modoSatelite() {
		
		modoSatelite = true;
		LatLng coord;
		municipioMarcado = new DefaultMapModel();
		List<Municipio> municipios = recuperarMunicipios();
		
		for (Municipio i : municipios) {
			if (i.getLatitude() != null && i.getLongitude() != null && !i.getLatitude().isEmpty() && !i.getLongitude().isEmpty()) {
				// Coordenadas
				coord = new LatLng(Double.parseDouble(i.getLatitude().replaceAll("\\s","")),
						Double.parseDouble(i.getLongitude().replaceAll("\\s","")));
				// Icones e Dados
				municipioMarcado
						.addOverlay(new Marker(coord, i.getNome(), i,
								"http://maps.google.com/mapfiles/ms/micons/blue-dot.png"));
			} else {
				warnMsg(ResourceBundle.getMessage("preenchaCoordenadas")+i.getNome(), null);
			}
			
			// Desenha poligono
			List<Coordenada> coordenadas = new ArrayList<Coordenada>();
			coordenadas = i.getCoordenadasPoligono();
			Polyline polyline = new Polyline();
			if (!coordenadas.isEmpty()) {	
				for (Coordenada coor : coordenadas) {
					polyline.getPaths().add(new LatLng(coor.getLatitude(),coor.getLongitude()));							
				}
			}
			polyline.setStrokeWeight(5);
			polyline.setStrokeColor("#"+gerarHexCor());
			polyline.setStrokeOpacity(1);
			municipioMarcado.addOverlay(polyline);
		}
	}
	
	public String gerarHexCor(){
		int red = (int) (( Math.random()*255)+1);
		int green = (int) (( Math.random()*255)+1);
		int blue = (int) (( Math.random()*255)+1);
		
		Color RandomC = new Color(red,green,blue);
		int RandomRGB = (RandomC.getRGB());
		String RandomRGB2Hex = Integer.toHexString(RandomRGB);
		if (RandomRGB2Hex.length() < 6) {
			RandomRGB2Hex = "000000".substring(0, 6 - RandomRGB2Hex.length()) + RandomRGB2Hex;
	   }
		
		return RandomRGB2Hex.substring(0, 6);
	}

	public void modoTabela() {
		modoSatelite = false;
	}

	public void marcadorSelecionado(OverlaySelectEvent event) {
		marker = (Marker) event.getOverlay();
		municipioSelecionado = (Municipio) marker.getData();
	}

	public void excluir() {
		municipioService.excluir(municipioSelecionado);
		municipioBusca = null;
		municipioSelecionado = null;
		infoMsg("Municipio exclu�do com sucesso.", null);
	}


	private List<Municipio> recuperarMunicipios() {
		List<Municipio> municipios = null;
		if (this.municipios.getRowCount() > this.municipios.getPageSize()) {
			municipios = municipioService.pesquisar(municipioBusca, 0,
					this.municipios.getRowCount());
		} else {
			municipios = new ArrayList<Municipio>();
			for (Municipio i : this.municipios) {
				municipios.add(i);
			}
		}
		return municipios;
	}

	public String cadastro() {
		return "cadastrarMunicipio?faces-redirect=true";
	}

	public String atividade() {
		return "consultarAtividadeEconomica?faces-redirect=true";
	}

	public LazyDataModel<Municipio> getMunicipios() {
		return municipios;
	}

	public void setMunicipios(LazyDataModel<Municipio> municipios) {
		this.municipios = municipios;
	}

	public Municipio getMunicipioBusca() {
		return municipioBusca;
	}

	public void setMunicipioBusca(Municipio municipioBusca) {
		this.municipioBusca = municipioBusca;
	}
	
	public Municipio getMunicipioSelecionado() {
		return municipioSelecionado;
	}

	public void setMunicipioSelecionado(Municipio municipioSelecionado) {
		this.municipioSelecionado = municipioSelecionado;
	}

	public Boolean getModoSatelite() {
		return modoSatelite;
	}

	public void setModoSatelite(Boolean modoSatelite) {
		this.modoSatelite = modoSatelite;
	}

	public MapModel getMunicipioMarcado() {
		return municipioMarcado;
	}

	public void setMunicipioMarcado(MapModel municipioMarcado) {
		this.municipioMarcado = municipioMarcado;
	}

	public Marker getMarker() {
		return marker;
	}

	public void setMarker(Marker marker) {
		this.marker = marker;
	}

	public String getCentral() {
		return central;
	}

	public void setCentral(String central) {
		this.central = central;
	}

	public List<Cultura> getCulturas() {
		return culturas;
	}

	public void setCulturas(List<Cultura> culturas) {
		this.culturas = culturas;
	}

	public List<Variedade> getVariedades() {
		return variedades;
	}

	public void setVariedades(List<Variedade> variedades) {
		this.variedades = variedades;
	}

	public String getLocalizacao() {
		return localizacao;
	}

	public void setLocalizacao(String localizacao) {
		this.localizacao = localizacao;
	}

	public StreamedContent getFile() {
		return file;
	}

	public void setFile(StreamedContent file) {
		this.file = file;
	}

	public Integer getZoomMapa() {
		return zoomMapa;
	}

	public void setZoomMapa(Integer zoomMapa) {
		this.zoomMapa = zoomMapa;
	}

	public MapModel getModel() {
		return model;
	}

	public void setModel(MapModel model) {
		this.model = model;
	}

}
