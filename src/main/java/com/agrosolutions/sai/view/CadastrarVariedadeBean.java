package com.agrosolutions.sai.view;

import java.io.Serializable;
import java.util.List;

import javax.faces.event.ActionEvent;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.Cultura;
import com.agrosolutions.sai.model.Variedade;
import com.agrosolutions.sai.service.CulturaService;
import com.agrosolutions.sai.service.VariedadeService;
import com.agrosolutions.sai.util.ResourceBundle;

/**
* Use case : Cadastrar Variedade
* 
* @since   : Jan 17, 2012, 03:23:20 AM
* @author  : raphael.ferreira@ivia.com.br
*/

@Component("cadastrarVariedadeBean")
@Scope("session")
public class CadastrarVariedadeBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = -3455988459235196638L;

	public static Logger logger = Logger.getLogger(CadastrarVariedadeBean.class);

	public static final String MSG_TITULO = "Cadastro de Variedade";

	@Autowired
	private VariedadeService variedadeService;
	@Autowired
	private CulturaService culturaService;
	
	private Variedade variedade;
	private List<Cultura> culturas;
	private String operacao;

	
	public void carregarDadosIniciais() {
		operacao = getOperacao();
		variedade = getVariedade();
		
		if (operacao.equals("NOVO") && variedade == null) {
			variedade = new Variedade();
		}
		
		culturas = culturaService.obterTodos();
	}
	
	public void gravar(ActionEvent actionEvent) {
		variedade = variedadeService.salvar(variedade); 
		if (operacao.equals("NOVO")) {
			variedade = null;
			infoMsg(ResourceBundle.getMessage("variedadeSalvar")+ ".", MSG_TITULO);
		}else{
			infoMsg(ResourceBundle.getMessage("variedadeAlterar")+ ".", MSG_TITULO);
		}
    }

	public String voltar() {
		return "consultarVariedade?faces-redirect=true";
	}

	public Variedade getVariedade() {
		if (variedade == null) {
			variedade = (Variedade) getFlashScope().get("variedade");
		}
		return variedade;
	}

	public void setVariedade(Variedade variedade) {
		this.variedade = variedade;
		getFlashScope().put("variedade", variedade);
	}

	public List<Cultura> getCulturas() {
		return culturas;
	}

	public void setCulturas(List<Cultura> culturas) {
		this.culturas = culturas;
	}

	public String getOperacao() {
		if (operacao == null) {
			operacao = (String) getFlashScope().get("operacao");
		}
		return operacao;
	}

	public void setOperacao(String operacao) {
		this.operacao = operacao;
		getFlashScope().put("operacao", operacao);
	}
	
}
