package com.agrosolutions.sai.view;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.List;

import javax.faces.model.SelectItem;

import org.apache.log4j.Logger;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.SortOrder;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.Estacao;
import com.agrosolutions.sai.service.ClimaService;
import com.agrosolutions.sai.service.EstacaoService;
import com.agrosolutions.sai.util.ResourceBundle;

/**
* Use case : Consultar Estacao
* 
* @since   : Mar 01, 2011, 14:23:20 AM
* @author  : rpf1404@gmail.com
*/

@Component("consultarEstacaoBean")
@Scope("session")
public class ConsultarEstacaoBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = 53421938612047741L;

	public static Logger logger = Logger.getLogger(ConsultarEstacaoBean.class);

	@Autowired
	private EstacaoService estacaoService;
	@Autowired
	private ClimaService climaService;
	
	private List<Estacao> estacoes;
	private SelectItem[] distritos;
	private Estacao estacaoSelecionado;
	private Boolean multiDistrito;
	private Estacao estacaoBusca;
	
	public void carregarDadosIniciais() {
		if (estacaoBusca == null){
			estacaoBusca = new Estacao();
			estacaoBusca.setSortField("nome");
    		estacaoBusca.setSortOrder(SortOrder.ASCENDING);
			estacaoBusca.setDistritos(super.getUsuarioLogado().getDistritos());
    		consultar();
		}
	}
	
	public void consultar(){
		estacoes = estacaoService.pesquisarEstacao(estacaoBusca, null, null);
	}
	
	public void excluir(){
	   estacaoService.excluir(estacaoSelecionado);
		infoMsg(ResourceBundle.getMessage("estacaoExcluir")+ ".", null);
	} 
	
	public void importarArquivo(FileUploadEvent event) {  
    	UploadedFile arq = event.getFile();
		InputStream in;
		try {
			in = new BufferedInputStream(arq.getInputstream());
			if (event.getFile().getFileName().endsWith(".htm")) {
				climaService.importarArquivoInmet(in, estacaoSelecionado);
			} else {
				climaService.importarArquivoCampbell(in, estacaoSelecionado);
			}
	        infoMsg(event.getFile().getFileName() + " foi carregado.", null);
		} catch (IOException e) {
	        errorMsg(event.getFile().getFileName() + " não foi carregado.", null);
			e.printStackTrace();
		}
    }  
	
	public void carregaEstacao(Estacao e){
		estacaoSelecionado = e;
	}

	public String cadastro() {
		return "cadastrarEstacao?faces-redirect=true";
	}

	public String dados() {
		return "consultarDadosEMA?faces-redirect=true";
	}

	public List<Estacao> getEstacoes() {
		return estacoes;
	}

	public void setEstacoes(List<Estacao> estacoes) {
		this.estacoes = estacoes;
	}

	public SelectItem[] getDistritos() {
		return distritos;
	}

	public void setDistritos(SelectItem[] distritos) {
		this.distritos = distritos;
	}

	public Boolean getMultiDistrito() {
		return multiDistrito;
	}

	public Estacao getEstacaoSelecionado() {
		return estacaoSelecionado;
	}

	public void setEstacaoSelecionado(Estacao estacaoSelecionado) {
		this.estacaoSelecionado = estacaoSelecionado;
	}

	public void setMultiDistrito(Boolean multiDistrito) {
		this.multiDistrito = multiDistrito;
	}

	public void setEstacaoBusca(Estacao estacaoBusca) {
		this.estacaoBusca = estacaoBusca;
	}

	public Estacao getEstacaoBusca() {
		return estacaoBusca;
	}
}
