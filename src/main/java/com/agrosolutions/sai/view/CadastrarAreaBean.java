package com.agrosolutions.sai.view;

import java.io.Serializable;
import java.util.List;

import javax.faces.event.ActionEvent;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.Area;
import com.agrosolutions.sai.model.Distrito;
import com.agrosolutions.sai.service.AreaService;
import com.agrosolutions.sai.util.ResourceBundle;

/**
* Use case : Cadastrar Area
* 
* @since   : Jan 06, 2012, 15:00:20 AM
* @author  : raphael.ferreira@ivia.com.br
*/

@Component("cadastrarAreaBean")
@Scope("session")
public class CadastrarAreaBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = -3028443469794245093L;

	public static Logger logger = Logger.getLogger(CadastrarAreaBean.class);

	public static final String MSG_TITULO = "Cadastro de área";

	@Autowired
	private AreaService areaService;
	
	private Area area;
	private List<Distrito> distritos;
	private Boolean multiDistrito;
	private String operacao;

	public void carregarDadosIniciais() {
		operacao = getOperacao();
		area = getArea();
		
		if (operacao.equals("NOVO") && area == null) {
			area = new Area();
			if (getUsuarioLogado().getDistritos().size() == 1) {
				area.setDistrito(getUsuarioLogado().getDistritos().get(0));
				multiDistrito = false;
			}else{
				multiDistrito = true;
				distritos = getUsuarioLogado().getDistritos();
			}
		}
	}
	
	public void gravar(ActionEvent actionEvent) {
		area = areaService.salvar(area); 
		if (operacao.equals("NOVO")) {
			area = null;
			infoMsg(ResourceBundle.getMessage("areaSalvar")+ ".", MSG_TITULO);
		}else{
			infoMsg(ResourceBundle.getMessage("areaAlterar")+ ".", MSG_TITULO);
		}
    }

	public String voltar() {
		return "consultarArea?faces-redirect=true";
	}

	public Area getArea() {
		if (area == null) {
			area = (Area) getFlashScope().get("area");
		}
		return area;
	}

	public void setArea(Area area) {
		this.area = area;
		getFlashScope().put("area", area);
	}

	public Boolean getMultiDistrito() {
		return multiDistrito;
	}

	public void setMultiDistrito(Boolean multiDistrito) {
		this.multiDistrito = multiDistrito;
	}

	public List<Distrito> getDistritos() {
		return distritos;
	}

	public void setDistritos(List<Distrito> distritos) {
		this.distritos = distritos;
	}
	public String getOperacao() {
		if (operacao == null) {
			operacao = (String) getFlashScope().get("operacao");
		}
		return operacao;
	}

	public void setOperacao(String operacao) {
		this.operacao = operacao;
		getFlashScope().put("operacao", operacao);
	}
}
