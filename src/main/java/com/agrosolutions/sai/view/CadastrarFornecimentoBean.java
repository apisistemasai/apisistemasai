package com.agrosolutions.sai.view;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import javax.faces.event.ActionEvent;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.exception.SaiException;
import com.agrosolutions.sai.model.Agua;
import com.agrosolutions.sai.model.Irrigacao;
import com.agrosolutions.sai.service.AguaService;
import com.agrosolutions.sai.service.IrrigacaoService;
import com.agrosolutions.sai.service.IrriganteService;
import com.agrosolutions.sai.util.ResourceBundle;

/**
* Use case : Cadastrar Fornecimento
* 
* @since   : Set 21, 2013, 03:45:20 AM
* @author  : rpf1404@gmail.com
*/

@Component("cadastrarFornecimentoBean")
@Scope("session")
public class CadastrarFornecimentoBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = -5579998320256085613L;

	public static Logger logger = Logger.getLogger(CadastrarFornecimentoBean.class);

	@Autowired
	private IrriganteService irriganteService;
	@Autowired
	private AguaService aguaService;
	@Autowired
	private IrrigacaoService irrigacaoService;
	
	private Agua aguaSelecionada;
	private String operacao;
	private List<Agua> aguas;
	private boolean loteValido = false;
	private boolean aguaValido = false;
	
	public void carregarDadosIniciais() {
		if (getOperacao().equals("NOVO") && getAguas() == null) {
			setAguas(new ArrayList<Agua>());
			Agua agua = new Agua();
			getAguas().add(agua);
		}
		if ((getOperacao().equals("ALTERAR") || getOperacao().equals("VER")) && getAguaSelecionada() != null && aguas == null) {
			getAguaSelecionada().setSortField("data");
			setAguas(aguaService.pesquisar(getAguaSelecionada(), null, null));
		}
	}
	public void gravar(ActionEvent actionEvent) {
		setAguas(aguaService.salvarLote(getAguas()));
		if (getOperacao().equals("NOVO")) {
			infoMsg(ResourceBundle.getMessage("fornecimentoSalvar"), null);
		}else{
			infoMsg(ResourceBundle.getMessage("fornecimentoAlterar"), null);
		}
    }
	public void verificarLote(Agua agua) {
		if(agua.getIrrigante().getLocalizacao()!=null){
			try {
				agua.setIrrigante(irriganteService.pesquisarPorLote(agua.getIrrigante().getLocalizacao(), super.getDistritosPesquisa()));
				loteValido = true;
				agua.setLacre(agua.getIrrigante().getLacreHidrometro());
				agua.setCodigo(agua.getIrrigante().getCodigo());
				verificarAnterior(agua);
			} catch (SaiException e) {
				errorMsg(e.getMessage(), null);
			}
		}
	}

	public void verificarLacre(Agua agua) {
		if(agua.getIrrigante().getLacreHidrometro() == null || !agua.getIrrigante().getLacreHidrometro().equals(agua.getLacre())){
			try {
				agua.getIrrigante().setLacreHidrometro(agua.getLacre());
				agua.setIrrigante(irriganteService.salvar(agua.getIrrigante()));
			} catch (SaiException e) {
				errorMsg(e.getMessage(), null);
			}
		}
	}

	public void verificarCodigo(Agua agua) {
		if(agua.getIrrigante().getCodigo() == null || !agua.getIrrigante().getCodigo().equals(agua.getCodigo())){
			agua.getIrrigante().setCodigo(agua.getCodigo());
			try {
				agua.setIrrigante(irriganteService.salvar(agua.getIrrigante()));
			} catch (SaiException e) {
				errorMsg(e.getMessage(), null);
			}
		}
	}

	public void mudarData(Agua agua) {
		if (agua.getIrrigante().getId() !=null){
			verificarAnterior(agua);
		}
	}

	public void verificarAnterior(Agua agua) {
		if(agua!=null){
			try {
				Agua anterior = aguaService.pesquisarAnterior(agua.getIrrigante(), agua.getData());
				
				for (Agua a : aguas) {
					Calendar dataApresent = new GregorianCalendar();
					dataApresent.setTime(a.getData());

					Calendar dataAnteriorApresent = new GregorianCalendar();
					dataAnteriorApresent.setTime(anterior.getData());

					Calendar dataAtualApresent = new GregorianCalendar();
					dataAtualApresent.setTime(agua.getData());

					if (dataApresent.after(dataAnteriorApresent) && dataApresent.before(dataAtualApresent)){
						anterior = a;
					}
					
				}
				
				aguaValido = true;
				Calendar data = new GregorianCalendar();
				data.setTime(anterior.getData());
				data.add(Calendar.DAY_OF_MONTH, 1);
				agua.setDataInicial(data.getTime());
				agua.setLeituraanterior(anterior.getLeituraatual());
				List<Irrigacao> irrigacoes = irrigacaoService.pesquisarPorIrrigantePeriodo(data.getTime(), agua.getData(), agua.getIrrigante());
				BigDecimal et = new BigDecimal(0);
				BigDecimal irr = new BigDecimal(0);
				for (Irrigacao i : irrigacoes) {
					et = et.add(i.getEtc());
					irr = irr.add(i.getIrrigacao());
				}
				agua.setIrr(irr.multiply(new BigDecimal(10)));
				agua.setEt(et.multiply(new BigDecimal(10)));
				
			} catch (SaiException e) {
				errorMsg(e.getMessage(), null);
				agua.setLeituraanterior(0L);
				aguaValido = false;
			}
			
		}
	}

	public void excluir() {
		getAguas().remove(getAguaSelecionada());
		if (getAguaSelecionada().getId()!= null) {
			aguaService.excluir(getAguaSelecionada());
		}
		setAguaSelecionada(null);
		infoMsg(ResourceBundle.getMessage("aguaRemovido"), null);
    }

	public void adicionarRegistro(ActionEvent actionEvent) {
		Agua agua = new Agua();
		getAguas().add(agua);
		loteValido = false;
		aguaValido = false;

    }

	public String voltar() {
		aguas = null;
		return "consultarFornecimento?faces-redirect=true";
	}
	public void setAguas(List<Agua> aguas) {
		this.aguas = aguas;
	}
	public List<Agua> getAguas() {
		return aguas;
	}
	public void setOperacao(String operacao) {
		this.operacao = operacao;
	}
	public String getOperacao() {
		return operacao;
	}
	public void setAguaSelecionada(Agua aguaSelecionada) {
		this.aguaSelecionada = aguaSelecionada;
	}
	public Agua getAguaSelecionada() {
		return aguaSelecionada;
	}
	public void setLoteValido(boolean loteValido) {
		this.loteValido = loteValido;
	}
	public boolean isLoteValido() {
		return loteValido;
	}
	public void setAguaValido(boolean aguaValido) {
		this.aguaValido = aguaValido;
	}
	public boolean isAguaValido() {
		return aguaValido;
	}
	
}
