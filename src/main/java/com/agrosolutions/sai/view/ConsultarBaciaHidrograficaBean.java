package com.agrosolutions.sai.view;

import java.io.Serializable;
import java.util.List;

import org.apache.log4j.Logger;
import org.primefaces.event.map.OverlaySelectEvent;
import org.primefaces.model.SortOrder;
import org.primefaces.model.map.DefaultMapModel;
import org.primefaces.model.map.LatLng;
import org.primefaces.model.map.MapModel;
import org.primefaces.model.map.Marker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.BaciaHidrografica;
import com.agrosolutions.sai.service.BaciaHidrograficaService;
import com.agrosolutions.sai.util.ResourceBundle;

/**
* Use case : Consultar BaciaHidrografica
* 
* @since   : Jan 06, 2016, 14:23:20 AM
* @author  : raphael@iasoft.me
*/

@Component("consultarBaciaHidrograficaBean")
@Scope("view")
public class ConsultarBaciaHidrograficaBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = -4050604945230760593L;

	public static Logger logger = Logger.getLogger(ConsultarBaciaHidrograficaBean.class);

	@Autowired
	private BaciaHidrograficaService baciaHidrograficaService;
	
	private List<BaciaHidrografica> baciaHidrograficas;
	private BaciaHidrografica baciaHidrograficaSelecionado;
	private Boolean modoSatelite;
	private MapModel baciaHidrograficasMarcado;  
    private Marker marker;  	
    private String central;
    private BaciaHidrografica baciaHidrograficaBusca;

    public void carregarDadosIniciais() {
    	if (baciaHidrograficaBusca == null){
    		baciaHidrograficaBusca = new BaciaHidrografica();
    		baciaHidrograficaBusca.setSortField("nome");
    		baciaHidrograficaBusca.setSortOrder(SortOrder.ASCENDING);
    		baciaHidrograficaBusca.setBacias(getUsuarioLogado().getBacias());
        	consultar();
    	}
    	
    	LatLng coord;
		central = "0,0";

		baciaHidrograficasMarcado = new DefaultMapModel();
		for (BaciaHidrografica d : baciaHidrograficas) {
			if (d.getLatitude() != null && !d.getLatitude().isEmpty()) {
		        //Coordenadas  
		        coord = new LatLng(Double.parseDouble(d.getLatitude()), Double.parseDouble(d.getLongitude()));  
		        //Icones e Dados  
		        baciaHidrograficasMarcado.addOverlay(new Marker(coord, d.getNome(), null, "http://maps.google.com/mapfiles/ms/micons/blue-dot.png"));  
			}
		}
	}
    
    public void consultar(){
    	baciaHidrograficas = baciaHidrograficaService.pesquisar(baciaHidrograficaBusca, null, null);
    }

	public String atividadeEconomica() {
		return "consultarAtividadesEconomica?faces-redirect=true";
	}

	public void marcadorSelecionado(OverlaySelectEvent event) {  
        marker = (Marker) event.getOverlay();  
    } 
    public String cadastro() {
		return "cadastrarBaciaHidrografica?faces-redirect=true";
	}

    public void modoSatelite() {
    	modoSatelite = true;
	}

    public void modoTabela() {
    	modoSatelite = false;
	}

    public void excluir(){
	   baciaHidrograficaService.excluir(baciaHidrograficaSelecionado);
		infoMsg(ResourceBundle.getMessage("baciaHidrograficaExcluir")+ ".","");
   }  	

   public List<BaciaHidrografica> getBaciaHidrograficas() {
		return baciaHidrograficas;
	}

	public void setBaciaHidrograficas(List<BaciaHidrografica> baciaHidrograficas) {
		this.baciaHidrograficas = baciaHidrograficas;
	}

	public BaciaHidrografica getBaciaHidrograficaSelecionado() {
		return baciaHidrograficaSelecionado;
	}

	public void setBaciaHidrograficaSelecionado(BaciaHidrografica baciaHidrograficaSelecionado) {
		this.baciaHidrograficaSelecionado = baciaHidrograficaSelecionado;
	}

	public Boolean getModoSatelite() {
		return modoSatelite;
	}

	public void setModoSatelite(Boolean modoSatelite) {
		this.modoSatelite = modoSatelite;
	}

	public MapModel getBaciaHidrograficasMarcado() {
		return baciaHidrograficasMarcado;
	}

	public void setBaciaHidrograficasMarcado(MapModel baciaHidrograficasMarcado) {
		this.baciaHidrograficasMarcado = baciaHidrograficasMarcado;
	}

	public Marker getMarker() {
		return marker;
	}

	public void setMarker(Marker marker) {
		this.marker = marker;
	}

	public String getCentral() {
		return central;
	}

	public void setCentral(String central) {
		this.central = central;
	}

	public void setBaciaHidrograficaBusca(BaciaHidrografica baciaHidrograficaBusca) {
		this.baciaHidrograficaBusca = baciaHidrograficaBusca;
	}

	public BaciaHidrografica getBaciaHidrograficaBusca() {
		return baciaHidrograficaBusca;
	}
	
}
