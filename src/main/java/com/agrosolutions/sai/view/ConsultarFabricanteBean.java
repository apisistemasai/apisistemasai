package com.agrosolutions.sai.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.Fabricante;
import com.agrosolutions.sai.model.TipoFabricante;
import com.agrosolutions.sai.service.FabricanteService;
import com.agrosolutions.sai.util.ResourceBundle;

/**
 * Use case : Consultar Fabricante
 * 
 * @since : Jan 27, 2012, 23:23:20 AM
 * @author : raphael.ferreira@ivia.com.br
 */

@Component("consultarFabricanteBean")
@Scope("view")
public class ConsultarFabricanteBean extends ManagerBean implements
		Serializable {

	private static final long serialVersionUID = 2357476627027394279L;

	public static Logger logger = Logger.getLogger(ConsultarFabricanteBean.class);

	public static final String MSG_TITULO = "Consultar Fabricantes";

	@Autowired
	private FabricanteService fabricanteService;

	private LazyDataModel<Fabricante> fabricantes;
	private Fabricante fabricanteSelecionado;
	private Fabricante fabricanteBusca;

	public void carregarDadosIniciais() {
		if (fabricanteBusca == null) {
			fabricanteBusca = new Fabricante();
			fabricanteBusca.setSortField("nome");
			fabricanteBusca.setSortOrder(SortOrder.ASCENDING);
			consultar();
		}
	}

	public void consultar() {
		fabricantes = new LazyDataModel<Fabricante>() {

			private static final long serialVersionUID = 1L;

			@Override
			public List<Fabricante> load(int first, int pageSize,
					String sortField, SortOrder sortOrder,
					Map<String, String> filters) {
				List<Fabricante> fabricantes = new ArrayList<Fabricante>();

				if (sortField == null) {
					sortField = "nome";
					sortOrder = SortOrder.ASCENDING;
				}

				fabricanteBusca.setSortField(sortField);
				fabricanteBusca.setSortOrder(sortOrder);
				fabricantes = fabricanteService.pesquisar(fabricanteBusca,first, pageSize);

				return fabricantes;
			}
		};
		int quantidade = fabricanteService.quantidade(fabricanteBusca);
		fabricantes.setRowCount(quantidade == 0 ? 1 : quantidade);
	}

	public String cadastro() {
		return "cadastrarFabricante?faces-redirect=true";
	}

	public void excluir() {
		fabricanteService.excluir(fabricanteSelecionado);
		consultar();
		infoMsg(ResourceBundle.getMessage("fabricanteExcluir") + ".", "");
	}

	public Fabricante getFabricanteSelecionado() {
		return fabricanteSelecionado;
	}

	public void setFabricanteSelecionado(Fabricante fabricanteSelecionado) {
		this.fabricanteSelecionado = fabricanteSelecionado;
	}

	public void setFabricantes(LazyDataModel<Fabricante> fabricantes) {
		this.fabricantes = fabricantes;
	}

	public LazyDataModel<Fabricante> getFabricantes() {
		return fabricantes;
	}

	public void setFabricanteBusca(Fabricante fabricanteBusca) {
		this.fabricanteBusca = fabricanteBusca;
	}

	public Fabricante getFabricanteBusca() {
		return fabricanteBusca;
	}
	public TipoFabricante[] getTiposFabricante() {
		return TipoFabricante.values();
	}
	
}
