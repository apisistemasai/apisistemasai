package com.agrosolutions.sai.view;

import java.io.Serializable;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.Categoria;
import com.agrosolutions.sai.service.CategoriaService;
import com.agrosolutions.sai.util.ResourceBundle;

/**
 * Use case : Consultar Categoria
 * 
 * @since : Nov 26, 2011, 22:23:20 AM
 * @author : raphael.ferreira@ivia.com.br
 */

@Component("consultarCategoriaBean")
@Scope("view")
public class ConsultarCategoriaBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = 3014368717947587732L;

	public static Logger logger = Logger
			.getLogger(ConsultarCategoriaBean.class);

	public static final String MSG_TITULO = "Consultar Categoria";

	@Autowired
	private CategoriaService categoriaService;

	private List<Categoria> categorias;
	private Categoria categoriaSelecionada;

	public void carregarDadosIniciais() {
		categorias = categoriaService.obterTodos();
	}

	public void excluir() {
		categoriaService.excluir(categoriaSelecionada);
		categorias = categoriaService.obterTodos();
		infoMsg(ResourceBundle.getMessage("categoriaExcluir")+ ".", MSG_TITULO);
	}

	public String cadastro() {
		return "cadastrarCategoria?faces-redirect=true";
	}

	public List<Categoria> getCategorias() {
		return categorias;
	}

	public void setCategorias(List<Categoria> categorias) {
		this.categorias = categorias;
	}

	public Categoria getCategoriaSelecionada() {
		return categoriaSelecionada;
	}

	public void setCategoriaSelecionada(Categoria categoriaSelecionada) {
		this.categoriaSelecionada = categoriaSelecionada;
	}

}
