package com.agrosolutions.sai.view;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.primefaces.model.StreamedContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.exception.SaiException;
import com.agrosolutions.sai.service.IrriganteService;
import com.agrosolutions.sai.util.DateUtils;
import com.agrosolutions.sai.util.RelatorioUtil;
import com.agrosolutions.sai.util.ResourceBundle;


@Component("relFriBean")
@Scope("session")
public class RelFriBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = -8320656327562741022L;

	public static Logger logger = Logger.getLogger(RelFriBean.class);

	@Autowired
	private RelatorioUtil relatorioUtil;
	@Autowired
	private IrriganteService irriganteService;

	private Boolean exibirErroLote = false;
	private String localizacao;
	private Date dataIni;
	private Date dataFim;
	private StreamedContent file;
	private Integer extensaoArquivo;
	
	public String carregar(){
		return null;
	}
	   
	public String imprimir() {
		boolean erro = false;
		try {

			// validando campos da entidade
			if ( getDataIni() == null || getDataFim() == null ){
				errorMsg(ResourceBundle.getMessage("friData")+ ".", null);
				erro = true;
			}
			
			if (!erro && getDataIni().after(getDataFim())){
				errorMsg(ResourceBundle.getMessage("demandaDataM")+ ".", null);
				erro = true;
			}
			if ( !localizacao.isEmpty() ){
				try {
					irriganteService.pesquisarPorLote(localizacao, super.getDistritosPesquisa());
				} catch (SaiException e) {
					errorMsg(e.getMessage(), null);
					erro = true;
				}
			}

			if (erro) {
				return null;
			}
			
			// Par�metros enviados para gerar o relat�rio
			Map<String, Object> parametros = new HashMap<String, Object>();
			parametros.put( "lote", localizacao.isEmpty() ? "" : getLocalizacao().toUpperCase() );
			parametros.put( "dtIni", DateUtils.toString(getDataIni()) );
			parametros.put( "dtFim", DateUtils.toString(getDataFim()) );
			parametros.put( "listaDistrito", prepararDistritos() );
			
			// Imprime o relat�rio
			if (getExtensaoArquivo() == 1 || getExtensaoArquivo() == 0){
				file = relatorioUtil.relatorio("rptFri_Analitico.jasper", parametros, "fri.pdf");
			}else{
				file = relatorioUtil.relatorioXLS("rptFri_Analitico.jasper", parametros, "fri.xls");
			}
			
		} catch (SaiException e) {
			errorMsg(ResourceBundle.getMessage("ocorreuErro")+ ":" + e.getMessage(), null);
			logger.warn("Ocorreu o seguinte erro: " + e.getMessage() + e.getCause().getMessage());
			return null;
		} catch (Exception e) {
			errorMsg(ResourceBundle.getMessage("ocorreuErro")+ ":" + e.getMessage(), null);
			logger.fatal("Ocorreu o seguinte erro: " + e.getMessage() + e.getCause().getMessage());
			return null;
		}
		infoMsg(ResourceBundle.getMessage("friRelatorio"), null);
		return null;
	}
	
	public void consultarLotePorNome() {
		
		if(irriganteService.pesquisarPorLote(this.localizacao, super.getDistritosPesquisa()) == null) {
			this.localizacao = null;
			setExibirErroLote(true);
		}
	}


	public StreamedContent getFile() {
		return file;
	}

	public void setFile(StreamedContent file) {
		this.file = file;
	}
	
	public Date getDataFim() {
		return dataFim;
	}

	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}

	public Date getDataIni() {
		return dataIni;
	}

	public void setDataIni(Date dataIni) {
		this.dataIni = dataIni;
	}

	public String getLocalizacao() {
		return localizacao;
	}

	public void setLocalizacao(String localizacao) {
		this.localizacao = localizacao;
	}

	public Integer getExtensaoArquivo() {
		return extensaoArquivo;
	}

	public void setExtensaoArquivo(Integer extensaoArquivo) {
		this.extensaoArquivo = extensaoArquivo;
	}

	public Boolean getExibirErroLote() {
		return exibirErroLote;
	}

	public void setExibirErroLote(Boolean exibirErroLote) {
		this.exibirErroLote = exibirErroLote;
	}
}
