package com.agrosolutions.sai.view;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.FormulaPlantio;
import com.agrosolutions.sai.model.Irrigacao;
import com.agrosolutions.sai.model.Plantio;
import com.agrosolutions.sai.model.Setor;
import com.agrosolutions.sai.model.TipoPerfil;
import com.agrosolutions.sai.service.IrrigacaoService;
import com.agrosolutions.sai.service.PlantioService;
import com.agrosolutions.sai.util.ResourceBundle;
import com.lowagie.text.BadElementException;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;

/**
* Use case : Consultar Irriga��o
* 
* @since   : Mar 19, 2011, 03:23:20 AM
* @author  : rpf1404@gmail.com
*/

@Component("consultarIrrigacaoBean")
@Scope("session")
public class ConsultarIrrigacaoBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = -1848858482837807246L;

	public static Logger logger = Logger.getLogger(ConsultarIrrigacaoBean.class);

	@Autowired
	private IrrigacaoService irrigacaoService;
	@Autowired
	private PlantioService plantioService;
	
	private Plantio plantio;
	private List<Irrigacao> irrigacoes;
	private Boolean origemPlantio;
	private Boolean origemFazenda;
	private Boolean recalculado;
	private String memoria;
	private Setor setor;
	private FormulaPlantio formula;
	
	public void carregarDadosIniciais() {
		if (irrigacoes == null) {
			irrigacoes = irrigacaoService.pesquisarPorPlantio(plantio);
			recalculado = false;
		}
		formula = plantioService.verificarFormula(plantio);
	}

	public void preProcessPDF(Object document) throws IOException,BadElementException, DocumentException {
		Document pdf = (Document) document;
		//ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
		//String logo = servletContext.getRealPath("") + File.separator + "img" + File.separator + "logoPrincipal.png";
		pdf.addTitle("Historico de Irriga��o do " + plantio.getSetor().getIrrigante().getNome());
		pdf.addSubject("Teste");
		pdf.addAuthor(getUsuarioLogado().getNome());
		pdf.addCreationDate();
		pdf.left(5);
		pdf.right(5);
		//pdf.add(Image.getInstance(logo));
	}
	
	public String voltar() {
		String retorno = null;
		if (origemPlantio) {
			retorno = "consultarPlantio?faces-redirect=true";
		}
		
		if (origemFazenda) {
			retorno = "consultarFazenda?faces-redirect=true";
		}
		
		if (getUsuarioLogado().getPerfil().getTipo().equals(TipoPerfil.Irrigante)) {
			retorno = "/homeIrrigante?faces-redirect=true";
		}
		irrigacoes = null;
		recalculado = false;
		return retorno;
	}

	public String recalcular() {
		irrigacoes = irrigacaoService.recalcular(getPlantio());
		recalculado = true;
		infoMsg(ResourceBundle.getMessage("irrigacaoHistorico")+ ".","");
		return null;
	}

	public String salvar() {
		List<Irrigacao> ir = new ArrayList<Irrigacao>();
		irrigacaoService.deletar(getPlantio());
		for (Irrigacao i : irrigacoes) {
			ir.add(irrigacaoService.salvar(i));
		}
		irrigacoes = ir;
		recalculado = false;
		infoMsg(ResourceBundle.getMessage("historicoSalvar")+ ".","");
		return null;
	}

	public Plantio getPlantio() {
		return plantio;
	}

	public void setPlantio(Plantio plantio) {
		this.plantio = plantio;
	}

	public List<Irrigacao> getIrrigacoes() {
		return irrigacoes;
	}

	public void setIrrigacoes(List<Irrigacao> irrigacoes) {
		this.irrigacoes = irrigacoes;
	}

	public Boolean getOrigemPlantio() {
		return origemPlantio;
	}

	public void setOrigemPlantio(Boolean origemPlantio) {
		this.origemPlantio = origemPlantio;
	}

	public Boolean getOrigemFazenda() {
		return origemFazenda;
	}

	public void setOrigemFazenda(Boolean origemFazenda) {
		this.origemFazenda = origemFazenda;
	}

	public String getMemoria() {
		return memoria;
	}

	public void setMemoria(String memoria) {
		this.memoria = memoria;
	}

	public Boolean getRecalculado() {
		return recalculado;
	}

	public void setRecalculado(Boolean recalculado) {
		this.recalculado = recalculado;
	}

	public void setSetor(Setor setor) {
		this.setor = setor;
	}

	public Setor getSetor() {
		return setor;
	}

	public FormulaPlantio getFormula() {
		return formula;
	}

	public void setFormula(FormulaPlantio formula) {
		this.formula = formula;
	}

}
