package com.agrosolutions.sai.view;

import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.Acesso;
import com.agrosolutions.sai.model.TipoPerfil;
import com.agrosolutions.sai.model.Usuario;
import com.agrosolutions.sai.service.AcessoService;
import com.agrosolutions.sai.service.AuthenticationService;
import com.agrosolutions.sai.service.DistritoService;
import com.agrosolutions.sai.service.UsuarioService;
import com.agrosolutions.sai.util.ResourceBundle;


@SuppressWarnings("serial")
@Component("LoginBean")
@Scope("request")
public class LoginBean extends ManagerBean implements Serializable {

	@Autowired
	private AuthenticationService authenticationService;
	@Autowired
	private UsuarioService usuarioService;
	@Autowired
	private AcessoService acessoService;
	@Autowired
	private DistritoService distritoService;
	
	private String login;
	private String senha;
	private String senhaNova;
	private String senhaConfirma;
	private String msg;
	private String email;
	
	/* HTML Control */
	private Boolean exibirOpcoesResetAcesso = true;
	private Boolean exibirBoxLogin = true;
	private Boolean exibirBoxRecuperarSenha = false;
	private Boolean exibirBoxAlterarSenha = false;
	private Boolean boxRecuperarLogin = false;
	private Boolean exibirBtnLogin = true;
	
	
	public void carregarDadosIniciais() {
		// Verifica se existe alguma mensagem e exibe
		msg = (String) getFlashScope().get("msg");

	    if (msg != null){
	    	infoMsg(msg, null);
	    	getFlashScope().clear();
	    }
	}
	
	public String autenticar() {
		String retorno = "home?faces-redirect=true";
	    senhaNova = getRequestParameter("senhaN");
	    senhaConfirma = getRequestParameter("senhaCN");
	    
	    Acesso acesso = acessoService.pesquisarUltimo(login);
	    
	    if (senhaNova != null && senhaConfirma != null) {
	    	String mensagem = authenticationService.login(login, senha);
    	 if(senhaNova.equals(senhaConfirma)){
		    if (mensagem.equalsIgnoreCase("ok")) {
		    	Usuario usu = getUsuarioLogado();
		    	usu.setPassword(senhaNova);
		    	usu.setConfirmarSenha(senhaConfirma);
		    	usu.setConfirmarEmail(usu.getEmail());
				usuarioService.salvar(usu);
		    }else{
		    	this.senha = new String();
		    	errorMsg(mensagem, null);
		    	return null;
		    }
    	 }else{
		    senhaConfirma = new String();
		    errorMsg(ResourceBundle.getMessage("senhaConfere")+ ".", null);
	    	return null;
    	 }
		}else{
		    String mensagemLogin = authenticationService.login(login, senha);
		    if (!mensagemLogin.equalsIgnoreCase("ok")) {
		    	this.login = new String();
		    	this.senha = new String();
		    	errorMsg(mensagemLogin, null);
		    	return "loginInvalido";
		    }
		}
	    

		HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();

		//Setando atributo logado como true para posterior identifica��o
		request.getSession().setAttribute("logado", true);
		
		//Setando o nome do usu�rio na sess�o
		request.getSession().setAttribute("usuario", login);
		
		//Setando a data do ultimo acesso
	    if (acesso !=null && acesso.getData() != null) {
	    	request.getSession().setAttribute("dataUltimoAcesso", acesso.getData());
		}
	    
	    //	Se for multidistrito, adiciona na sessao o distrito padr�o
	    if (super.getUsuarioLogado().getDistritos().size() > 1) {
	    	if (getUsuarioLogado().getPerfil().getTipo().equals(TipoPerfil.Administrativo)) {
	    		request.getSession().setAttribute("distritosUsuario", distritoService.pesquisarTodos());
	    	} else {
	    		request.getSession().setAttribute("distritosUsuario", super.getUsuarioLogado().getDistritos());	    		
	    	}
	    	request.getSession().setAttribute("multidistrito", true);
	    	if (super.getUsuarioLogado().getDistritoPadrao() == null) {
	    		retorno = "/distritoPadrao?faces-redirect=true";
	    	} else {
	    		super.setDistritoPadrao( super.getUsuarioLogado().getDistritoPadrao() );
	    	}
	    }else{
	    	request.getSession().setAttribute("distritosUsuario", super.getUsuarioLogado().getDistritos().get(0) );
	    	request.getSession().setAttribute("multidistrito", false);
	    }
	    
	    if (getUsuarioLogado().getPerfil().getTipo().equals(TipoPerfil.Irrigante)) {
			retorno = "homeIrrigante?faces-redirect=true";
		}

	    if (getUsuarioLogado().getModulos() != null && getUsuarioLogado().getModulos().size() > 0){
	    	if (getUsuarioLogado().getModulos().size() > 1 ){
	    		retorno = "homeModulos?faces-redirect=true";
	    	} else {
	    		retorno = getUsuarioLogado().getModulos().get(0).getUrl();
	    	}
		}
		
		return retorno;
	}
	
	public String esqueceuSenha() throws MalformedURLException {
		Usuario usu = null;
		
		if (login != null) {
			usu = usuarioService.pesquisarPorLogin(login);
			if (usu.getEmail() != null && usu.getEmail().isEmpty()) {
				errorMsg(ResourceBundle.getMessage("emailNaoCadastrado"), null);
		    	return null;
			}else{
				URL urlServidor = new URL(getRequestSession().getRequestURL().toString());
				String urlComplementacao = FacesContext.getCurrentInstance().getApplication().getViewHandler().getActionURL(FacesContext.getCurrentInstance(), "/redefinirSenha.xhtml");
				urlComplementacao += "?usr=USUARIO_ID&hash=USUARIO_HASH";
				
				urlComplementacao = new URL(urlServidor.getProtocol(), urlServidor.getHost(), urlServidor.getPort(), urlComplementacao).toString();
				usuarioService.enviarEmailSenha(usu, urlComplementacao);
			}
		}else{
	    	errorMsg(ResourceBundle.getMessage("loginRequired"), null);
	    	return null;
		}
		
		// Exibe um trecho do email para onde a mensagem foi enviada
		int index = usu.getEmail().indexOf("@");
		String parteEmail = usu.getEmail().substring(0, 3)+"..."+usu.getEmail().substring(index, index+4)+"...";
		
		getFlashScope().put("msg", ResourceBundle.getMessage("senhaEnviada")+parteEmail);
    	return "/login?faces-redirect=true";
	}
	
	public String logout() {
		authenticationService.logout();
		return "/login?faces-redirect=true";
	}
	
	public boolean anyGranted(String value) {
		// pegando a lista de grants passado
		String[] listaGrants = value.split(",");
		if (authenticationService.getUsuarioLogado()!= null) {
			for (GrantedAuthority permissao : authenticationService.getUsuarioLogado().getAuthorities()) {
				
				for (String grant : listaGrants) {
					if (permissao.getAuthority().equals(grant))
						return true;				
				}

			}
		}

		return false;
	}
	
	public String recuperarLogin() {
		List<Usuario> usuarios  = new ArrayList<Usuario>();
		
		if (email != null) {
			usuarios = usuarioService.pesquisarPorEmail(email);
			usuarioService.enviarEmailRecuperacaoLogin(usuarios);
		}else{
	    	errorMsg(ResourceBundle.getMessage("emailRequired"), null);
	    	return null;
		}
		
    	return "/login?faces-redirect=true";
	}
	
	public void exibirBoxRecuperarSenha(){
		exibirBoxLogin = false;
		exibirBoxRecuperarSenha  = true;
		exibirOpcoesResetAcesso = false;
		exibirBtnLogin = false;
	}
	
	public void exibirBoxAlterarSenha(){
		exibirBoxLogin = true;
		exibirBoxRecuperarSenha  = false;
		exibirBoxAlterarSenha = true;
		exibirOpcoesResetAcesso = false;
	}
	
	public void exibirBoxRecuperarLogin(){
		exibirBoxLogin = false;
		exibirBoxRecuperarSenha  = false;
		exibirOpcoesResetAcesso = false;
		exibirBtnLogin = false;
		setRecuperarLogin(true);
	}
	
	public String getLogin() {
		return login;
	}
	
	public void setLogin(String login) {
		this.login = login;
	}
	
	public String getSenha() {
		return senha;
	}
	
	public void setSenha(String senha) {
		this.senha = senha;
	}
	
	public Boolean isAdm() {
		return getUsuarioLogado().getUsername().equals("Adm");
	}
	
	public String getSenhaNova() {
		return senhaNova;
	}
	
	public void setSenhaNova(String senhaNova) {
		this.senhaNova = senhaNova;
	}
	
	public String getSenhaConfirma() {
		return senhaConfirma;
	}
	
	public void setSenhaConfirma(String senhaConfirma) {
		this.senhaConfirma = senhaConfirma;
	}
	
	public Date getDataUltimoAcesso() {
		return (Date) ((HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest()).getSession().getAttribute("dataUltimoAcesso");
	}
	
	public String getMsg() {
		return msg;
	}
	
	public void setMsg(String msg) {
		this.msg = msg;
	}
	
	public Boolean getExibirBoxRecuperarSenha() {
		return exibirBoxRecuperarSenha;
	}
	
	public void setExibirBoxRecuperarSenha(Boolean exibirBoxRecuperarSenha) {
		this.exibirBoxRecuperarSenha = exibirBoxRecuperarSenha;
	}

	public Boolean getExibirBoxLogin() {
		return exibirBoxLogin;
	}

	public void setExibirBoxLogin(Boolean exibirBoxLogin) {
		this.exibirBoxLogin = exibirBoxLogin;
	}

	public Boolean getExibirBoxAlterarSenha() {
		return exibirBoxAlterarSenha;
	}

	public void setExibirBoxAlterarSenha(Boolean exibirBoxAlterarSenha) {
		this.exibirBoxAlterarSenha = exibirBoxAlterarSenha;
	}

	public Boolean getExibirOpcoesResetAcesso() {
		return exibirOpcoesResetAcesso;
	}

	public void setExibirOpcoesResetAcesso(Boolean exibirOpcoesResetAcesso) {
		this.exibirOpcoesResetAcesso = exibirOpcoesResetAcesso;
	}

	public Boolean getExibirBtnLogin() {
		return exibirBtnLogin;
	}

	public void setExibirBtnLogin(Boolean exibirBtnLogin) {
		this.exibirBtnLogin = exibirBtnLogin;
	}

	public Boolean getExibirBoxRecuperarLogin() {
		return getRecuperarLogin();
	}

	public void setExibirBoxRecuperarLogin(Boolean exibirBoxRecuperarLogin) {
		this.setRecuperarLogin(exibirBoxRecuperarLogin);
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Boolean getRecuperarLogin() {
		return boxRecuperarLogin;
	}

	public void setRecuperarLogin(Boolean boxRecuperarLogin) {
		this.boxRecuperarLogin = boxRecuperarLogin;
	}	
}
