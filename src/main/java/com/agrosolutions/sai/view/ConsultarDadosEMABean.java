package com.agrosolutions.sai.view;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.apache.log4j.Logger;
import org.primefaces.model.chart.CartesianChartModel;
import org.primefaces.model.chart.LineChartSeries;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.Clima;
import com.agrosolutions.sai.model.Estacao;
import com.agrosolutions.sai.model.Mes;
import com.agrosolutions.sai.service.ClimaService;
import com.agrosolutions.sai.util.ResourceBundle;

/**
* Use case : Consultar Dados Metereologico
* 
* @since   : Abr 07, 2012, 03:45:20 AM
* @author  : rpf1404@gmail.com
*/

@Component("consultarDadosEMABean")
@Scope("session")
public class ConsultarDadosEMABean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = -5446264908755299625L;

	public static Logger logger = Logger.getLogger(ConsultarDadosEMABean.class);

	public static final String MSG_TITULO = "Consultar Dados Metereologico";

	@Autowired
	private ClimaService climaService;
	
	private Estacao estacao;
	private CartesianChartModel grafico;
	private Integer ano;
	private Mes mes;
	private boolean temGrafico;
	
	private boolean tMax=true;
	private boolean tMin=true;
	private boolean vento=true;
	private boolean humidade=true;
	private boolean radiacao=true;
	private boolean chuva=true;
	private boolean cEfetiva=true;
	private boolean eto=true;
	
	public void carregarDadosIniciais() {
		grafico = new CartesianChartModel();
		if (mes == null && ano == null) {
			Calendar data = new GregorianCalendar();
			data.setTime(new Date());
			ano = data.get(Calendar.YEAR);
			for (Mes m : Mes.values()) {
				if (m.ordinal() == data.get(Calendar.MONTH)){
					mes = m;		
				}
			}
		}
		if (mes != null && ano != null) {
			Calendar dataIni = new GregorianCalendar();
			Calendar dataFim = new GregorianCalendar();
			dataIni.set(ano, mes.ordinal(), 1);
			dataFim.set(ano, mes.ordinal()+1, 1);
			estacao.setClimas(climaService.estacaoPorPeriodo(estacao, dataIni.getTime(), dataFim.getTime()));
			if (estacao.getClimas().isEmpty()) {
				temGrafico = false;
				infoMsg(ResourceBundle.getMessage("climaExcluir")+ ".", null);
			}else{
				temGrafico = true;
				carregarGrafico();
			}
		}
	}
	private void carregarGrafico() {
		LineChartSeries tMax = new LineChartSeries();
		LineChartSeries tMin = new LineChartSeries();
		LineChartSeries humidade = new LineChartSeries();
		LineChartSeries vento = new LineChartSeries();
		LineChartSeries rad = new LineChartSeries();
		LineChartSeries chuva = new LineChartSeries();
		LineChartSeries eto = new LineChartSeries();

		tMax.setLabel(ResourceBundle.getMessage("tempMaxCompleta"));
		tMin.setLabel(ResourceBundle.getMessage("tempMinCompleta"));
		humidade.setLabel(ResourceBundle.getMessage("umidadeCompleta"));
		vento.setLabel(ResourceBundle.getMessage("ventoCompleta"));
		rad.setLabel(ResourceBundle.getMessage("radiacao"));
		chuva.setLabel(ResourceBundle.getMessage("chuva"));
		eto.setLabel(ResourceBundle.getMessage("eto"));

		for (Clima c : estacao.getClimas()) {
			Calendar data = new GregorianCalendar();
			data.setTime(c.getData());
			
			tMax.set(data.get(Calendar.DAY_OF_MONTH)+"", c.getTempMax()==null?new BigDecimal(0):c.getTempMax());
			tMin.set(data.get(Calendar.DAY_OF_MONTH)+"", c.getTempMin()==null?new BigDecimal(0):c.getTempMin());
			humidade.set(data.get(Calendar.DAY_OF_MONTH)+"", c.getUmidadeMed()==null?new BigDecimal(0):c.getUmidadeMed());
			vento.set(data.get(Calendar.DAY_OF_MONTH)+"", c.getVento()==null?new BigDecimal(0):c.getVento());
			rad.set(data.get(Calendar.DAY_OF_MONTH)+"", c.getRadiacao()==null?new BigDecimal(0):c.getRadiacao());
			chuva.set(data.get(Calendar.DAY_OF_MONTH)+"", c.getChuva()==null?new BigDecimal(0):c.getChuva());
			eto.set(data.get(Calendar.DAY_OF_MONTH)+"", c.getEtoCalculado()==null?new BigDecimal(0):c.getEtoCalculado());
		}
		

		if (this.tMax) {
			grafico.addSeries(tMax);
		}
		if (this.tMin) {
			grafico.addSeries(tMin);
		}
		if (this.humidade) {
			grafico.addSeries(humidade);
		}
		if (this.vento) {
			grafico.addSeries(vento);
		}
		if (this.radiacao) {
			grafico.addSeries(rad);
		}
		if (this.chuva) {
			grafico.addSeries(chuva);
		}
		if (this.eto) {
			grafico.addSeries(eto);
		}
	}
	public String voltar() {
		return "consultarEstacao?faces-redirect=true";
	}
	public Estacao getEstacao() {
		return estacao;
	}
	public void setEstacao(Estacao estacao) {
		this.estacao = estacao;
	}
	public CartesianChartModel getGrafico() {
		return grafico;
	}
	public void setGrafico(CartesianChartModel grafico) {
		this.grafico = grafico;
	}
	public boolean istMax() {
		return tMax;
	}
	public void settMax(boolean tMax) {
		this.tMax = tMax;
	}
	public boolean istMin() {
		return tMin;
	}
	public void settMin(boolean tMin) {
		this.tMin = tMin;
	}
	public boolean isVento() {
		return vento;
	}
	public void setVento(boolean vento) {
		this.vento = vento;
	}
	public boolean isHumidade() {
		return humidade;
	}
	public void setHumidade(boolean humidade) {
		this.humidade = humidade;
	}
	public boolean isRadiacao() {
		return radiacao;
	}
	public void setRadiacao(boolean radiacao) {
		this.radiacao = radiacao;
	}
	public boolean isChuva() {
		return chuva;
	}
	public void setChuva(boolean chuva) {
		this.chuva = chuva;
	}
	public boolean iscEfetiva() {
		return cEfetiva;
	}
	public void setcEfetiva(boolean cEfetiva) {
		this.cEfetiva = cEfetiva;
	}
	public boolean isEto() {
		return eto;
	}
	public void setEto(boolean eto) {
		this.eto = eto;
	}
	public Mes[] getMeses() {
		return Mes.values();
	}
	public Integer getAno() {
		return ano;
	}
	public void setAno(Integer ano) {
		this.ano = ano;
	}
	public Mes getMes() {
		return mes;
	}
	public void setMes(Mes mes) {
		this.mes = mes;
	}
	public boolean isTemGrafico() {
		return temGrafico;
	}
	public void setTemGrafico(boolean temGrafico) {
		this.temGrafico = temGrafico;
	}
}
