package com.agrosolutions.sai.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.event.ActionEvent;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.Bomba;
import com.agrosolutions.sai.model.Fabricante;
import com.agrosolutions.sai.model.Filtro;
import com.agrosolutions.sai.model.Irrigante;
import com.agrosolutions.sai.model.TipoFabricante;
import com.agrosolutions.sai.model.Tubo;
import com.agrosolutions.sai.model.Valvula;
import com.agrosolutions.sai.service.BombaService;
import com.agrosolutions.sai.service.FabricanteService;
import com.agrosolutions.sai.service.FiltroService;
import com.agrosolutions.sai.service.IrriganteService;
import com.agrosolutions.sai.service.SetorService;
import com.agrosolutions.sai.service.TuboService;
import com.agrosolutions.sai.service.ValvulaService;
import com.agrosolutions.sai.util.ResourceBundle;

/**
 * Use case : Cadastrar Sistema de Irrigação
 * 
 * @since : Fev 20, 2011, 20:23:20 PM
 * @author : rpf1404@gmail.com
 */

@Component("cadastrarSistemaBean")
@Scope("session")
public class CadastrarSistemaBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = -3539217161253634343L;
	public static Logger logger = Logger.getLogger(CadastrarSistemaBean.class);

	public static final String MSG_TITULO = "Cadastrar Sistema de Irrigação";

	@Autowired
	private IrriganteService irriganteService;
	@Autowired
	private BombaService bombaService;
	@Autowired
	private FiltroService filtroService;
	@Autowired
	private ValvulaService valvulaService;
	@Autowired
	private TuboService tuboService;
	@Autowired
	private SetorService setorService;
	@Autowired
	private FabricanteService fabricanteService;

	private Irrigante irrigante;
	private Bomba[] bombasSelecionadas;
	private Filtro[] filtrosSelecionadas;
	private Valvula[] valvulasSelecionadas;
	private Tubo[] tubosSelecionadas;
	private String operacao;
	private Bomba bombaSelecionada;
	private Bomba bombaBusca;
	private Filtro filtroSelecionada;
	private Filtro filtroBusca;
	private Valvula valvulaSelecionada;
	private Valvula valvulaBusca;
	private Tubo tuboSelecionada;
	private Tubo tuboBusca;
	private BombaDataModel bombas;
	private FiltroDataModel filtros;
	private ValvulaDataModel valvulas;
	private TuboDataModel tubos;
	private List<Fabricante> fabricantes;

	public void carregarDadosIniciais() {
		irrigante.setSetores(setorService.pesquisar(irrigante));

		if (bombasSelecionadas == null) {
			irrigante.setBombas(bombaService.pesquisarPorIrrigante(irrigante));
			int i = 0;
			bombasSelecionadas = new Bomba[irrigante.getBombas().size()];
			for (Bomba b : irrigante.getBombas()) {
				bombasSelecionadas[i++] = b;
			}
		}
		if (filtrosSelecionadas == null) {
			irrigante.setFiltros(filtroService.pesquisarPorIrrigante(irrigante));
			int i = 0;
			filtrosSelecionadas = new Filtro[irrigante.getFiltros().size()];
			for (Filtro f : irrigante.getFiltros()) {
				filtrosSelecionadas[i++] = f;
			}
		}
		if (valvulasSelecionadas == null) {
			irrigante.setValvulas(valvulaService
					.pesquisarPorIrrigante(irrigante));
			int i = 0;
			valvulasSelecionadas = new Valvula[irrigante.getValvulas().size()];
			for (Valvula v : irrigante.getValvulas()) {
				valvulasSelecionadas[i++] = v;
			}
		}
		if (tubosSelecionadas == null) {
			irrigante.setTubos(tuboService.pesquisarPorIrrigante(irrigante));
			int i = 0;
			tubosSelecionadas = new Tubo[irrigante.getTubos().size()];
			for (Tubo t : irrigante.getTubos()) {
				tubosSelecionadas[i++] = t;
			}
		}
		irrigante.getUsuario().setConfirmarEmail(
				irrigante.getUsuario().getEmail());
		irrigante.getUsuario().setConfirmarSenha(
				irrigante.getUsuario().getPassword());
	}

	public void consultarBombas() {
		List<Bomba> bombas = bombaService.pesquisar(bombaBusca);
		for (Bomba b : bombasSelecionadas) {
			if (bombas.contains(b)) {
				bombas.remove(b);
			}
			bombas.add(b);
		}
		this.bombas = new BombaDataModel(bombas);
	}

	public void consultarFiltros() {
		List<Filtro> filtros = filtroService.pesquisar(filtroBusca);
		for (Filtro f : filtrosSelecionadas) {
			if (filtros.contains(f)) {
				filtros.remove(f);
			}
			filtros.add(f);
		}
		this.filtros = new FiltroDataModel(filtros);	
	}

	public void consultarValvulas() {
		List<Valvula> valvulas = valvulaService.pesquisar(valvulaBusca);
		for (Valvula v : valvulasSelecionadas) {
			if (valvulas.contains(v)) {
				valvulas.remove(v);
			}
			valvulas.add(v);
		}
		this.valvulas = new ValvulaDataModel(valvulas);	
	}
	
	public void consultarTubos() {
		List<Tubo> tubos = tuboService.pesquisar(tuboBusca);
		for (Tubo t : tubosSelecionadas) {
			if (tubos.contains(t)) {
				tubos.remove(t);
			}
			tubos.add(t);
		}
		this.tubos = new TuboDataModel(tubos);
	}

	public void incluirBomba() {
		List<Bomba> bombas = new ArrayList<Bomba>();
		for (Bomba b : bombasSelecionadas) {
			bombas.add(b);
		}
		irrigante.setBombas(bombas);
	}

	public void incluirFiltro() {
		List<Filtro> filtros = new ArrayList<Filtro>();
		for (Filtro f : filtrosSelecionadas) {
			filtros.add(f);
		}
		irrigante.setFiltros(filtros);
	}

	public void incluirValvula() {
		List<Valvula> valvulas = new ArrayList<Valvula>();
		for (Valvula l : valvulasSelecionadas) {
			valvulas.add(l);
		}
		irrigante.setValvulas(valvulas);
	}

	public void incluirTubo() {
		List<Tubo> tubos = new ArrayList<Tubo>();
		for (Tubo t : tubosSelecionadas) {
			tubos.add(t);
		}
		irrigante.setTubos(tubos);
	}

	public void excluirBomba() {
		List<Bomba> bombas = new ArrayList<Bomba>();
		for (Bomba b : bombasSelecionadas) {
			if (!b.equals(bombaSelecionada)) {
				bombas.add(b);
			}
		}
		irrigante.setBombas(bombas);
		int i = 0;
		bombasSelecionadas = new Bomba[irrigante.getBombas().size()];
		for (Bomba b : irrigante.getBombas()) {
			bombasSelecionadas[i++] = b;
		}
	}

	public void excluirFiltro() {
		List<Filtro> filtros = new ArrayList<Filtro>();
		for (Filtro f : filtrosSelecionadas) {
			if (!f.equals(filtroSelecionada)) {
				filtros.add(f);
			}
		}
		irrigante.setFiltros(filtros);
		int i = 0;
		filtrosSelecionadas = new Filtro[irrigante.getFiltros().size()];
		for (Filtro f : irrigante.getFiltros()) {
			filtrosSelecionadas[i++] = f;
		}
	}

	public void excluirValvula() {
		List<Valvula> valvulas = new ArrayList<Valvula>();
		for (Valvula v : valvulasSelecionadas) {
			if (!v.equals(valvulaSelecionada)) {
				valvulas.add(v);
			}
		}
		irrigante.setValvulas(valvulas);
		int i = 0;
		valvulasSelecionadas = new Valvula[irrigante.getValvulas().size()];
		for (Valvula v : irrigante.getValvulas()) {
			valvulasSelecionadas[i++] = v;
		}
	}

	public void excluirTubo() {
		List<Tubo> tubos = new ArrayList<Tubo>();
		for (Tubo t : tubosSelecionadas) {
			if (!t.equals(tuboSelecionada)) {
				tubos.add(t);
			}
		}
		irrigante.setTubos(tubos);
		int i = 0;
		tubosSelecionadas = new Tubo[irrigante.getTubos().size()];
		for (Tubo t : irrigante.getTubos()) {
			tubosSelecionadas[i++] = t;
		}
	}

	public void gravar(ActionEvent actionEvent) {
		irrigante = irriganteService.salvar(irrigante);
		infoMsg(ResourceBundle.getMessage("sistemaSalvar")+ ".", MSG_TITULO);
	}

	public void prepararBomba(ActionEvent actionEvent) {
		bombas = new BombaDataModel(bombaService.obterTodos());
		fabricantes = fabricanteService.pesquisar(TipoFabricante.Bomba);
		bombaBusca = new Bomba();
	}

	public void prepararFiltro(ActionEvent actionEvent) {
		filtros = new FiltroDataModel(filtroService.obterTodos());
		fabricantes = fabricanteService.pesquisar(TipoFabricante.Filtro);
		filtroBusca = new Filtro();
	}
	
	public void prepararValvula(ActionEvent actionEvent) {
		valvulas = new ValvulaDataModel(valvulaService.obterTodos());
		fabricantes = fabricanteService.pesquisar(TipoFabricante.Valvula);
		valvulaBusca = new Valvula();
	}

	public void prepararTubo(ActionEvent actionEvent) {
		tubos = new TuboDataModel(tuboService.obterTodos());
		fabricantes = fabricanteService.pesquisar(TipoFabricante.Tubo);
		tuboBusca = new Tubo();
	}
	
	public String voltar() {
		return "consultarIrrigante?faces-redirect=true";
	}

	public Irrigante getIrrigante() {
		return irrigante;
	}

	public void setIrrigante(Irrigante irrigante) {
		this.irrigante = irrigante;
	}

	public Bomba[] getBombasSelecionadas() {
		return bombasSelecionadas;
	}

	public void setBombasSelecionadas(Bomba[] bombasSelecionadas) {
		this.bombasSelecionadas = bombasSelecionadas;
	}

	public String getOperacao() {
		return operacao;
	}

	public void setOperacao(String operacao) {
		this.operacao = operacao;
	}

	public Bomba getBombaSelecionada() {
		return bombaSelecionada;
	}

	public void setBombaSelecionada(Bomba bombaSelecionada) {
		this.bombaSelecionada = bombaSelecionada;
	}

	public BombaDataModel getBombas() {
		return bombas;
	}

	public void setBombas(BombaDataModel bombas) {
		this.bombas = bombas;
	}

	public Filtro getFiltroSelecionada() {
		return filtroSelecionada;
	}

	public void setFiltroSelecionada(Filtro filtroSelecionada) {
		this.filtroSelecionada = filtroSelecionada;
	}

	public FiltroDataModel getFiltros() {
		return filtros;
	}

	public void setFiltros(FiltroDataModel filtros) {
		this.filtros = filtros;
	}

	public Filtro[] getFiltrosSelecionadas() {
		return filtrosSelecionadas;
	}

	public void setFiltrosSelecionadas(Filtro[] filtrosSelecionadas) {
		this.filtrosSelecionadas = filtrosSelecionadas;
	}

	public ValvulaService getValvulaService() {
		return valvulaService;
	}

	public void setValvulaService(ValvulaService valvulaService) {
		this.valvulaService = valvulaService;
	}

	public Valvula[] getValvulasSelecionadas() {
		return valvulasSelecionadas;
	}

	public void setValvulasSelecionadas(Valvula[] valvulasSelecionadas) {
		this.valvulasSelecionadas = valvulasSelecionadas;
	}

	public ValvulaDataModel getValvulas() {
		return valvulas;
	}

	public void setValvulas(ValvulaDataModel valvulas) {
		this.valvulas = valvulas;
	}

	public Valvula getValvulaSelecionada() {
		return valvulaSelecionada;
	}

	public void setValvulaSelecionada(Valvula valvulaSelecionada) {
		this.valvulaSelecionada = valvulaSelecionada;
	}

	public Tubo[] getTubosSelecionadas() {
		return tubosSelecionadas;
	}

	public void setTubosSelecionadas(Tubo[] tubosSelecionadas) {
		this.tubosSelecionadas = tubosSelecionadas;
	}

	public Tubo getTuboSelecionada() {
		return tuboSelecionada;
	}

	public void setTuboSelecionada(Tubo tuboSelecionada) {
		this.tuboSelecionada = tuboSelecionada;
	}

	public TuboDataModel getTubos() {
		return tubos;
	}

	public void setTubos(TuboDataModel tubos) {
		this.tubos = tubos;
	}

	public void setTuboService(TuboService tuboService) {
		this.tuboService = tuboService;
	}

	public Bomba getBombaBusca() {
		return bombaBusca;
	}

	public void setBombaBusca(Bomba bombaBusca) {
		this.bombaBusca = bombaBusca;
	}

	public Filtro getFiltroBusca() {
		return filtroBusca;
	}

	public void setFiltroBusca(Filtro filtroBusca) {
		this.filtroBusca = filtroBusca;
	}

	public Valvula getValvulaBusca() {
		return valvulaBusca;
	}

	public void setValvulaBusca(Valvula valvulaBusca) {
		this.valvulaBusca = valvulaBusca;
	}

	public Tubo getTuboBusca() {
		return tuboBusca;
	}

	public void setTuboBusca(Tubo tuboBusca) {
		this.tuboBusca = tuboBusca;
	}

	public List<Fabricante> getFabricantes() {
		return fabricantes;
	}

	public void setFabricantes(List<Fabricante> fabricantes) {
		this.fabricantes = fabricantes;
	}

}
