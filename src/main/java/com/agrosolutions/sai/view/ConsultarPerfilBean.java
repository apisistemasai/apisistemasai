package com.agrosolutions.sai.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.Perfil;
import com.agrosolutions.sai.model.TipoPerfil;
import com.agrosolutions.sai.service.PerfilService;
import com.agrosolutions.sai.util.ResourceBundle;

/**
* Use case : Consultar Perfis
* 
* @since   : Jan 11, 2012, 23:23:20 AM
* @author  : raphael.ferreira@ivia.com.br
*/

@Component("consultarPerfilBean")
@Scope("view")
public class ConsultarPerfilBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = -1491478310506344216L;

	public static Logger logger = Logger.getLogger(ConsultarPerfilBean.class);

	public static final String MSG_TITULO = "Consultar Perfis";

	@Autowired
	private PerfilService perfilService;
	
	private LazyDataModel<Perfil> perfis;
	private Perfil perfilSelecionado;
	private Perfil perfilBusca;
	
	public void carregarDadosIniciais() {
		if (perfilBusca == null){
			perfilBusca = new Perfil();
			perfilBusca.setSortField("descricao");
			perfilBusca.setSortOrder(SortOrder.ASCENDING);
			consultar();
		}
	}

	public void consultar(){
		perfis = new LazyDataModel<Perfil>() {
	
			private static final long serialVersionUID = -7970564538984402322L;

			@Override
			public List<Perfil> load(int first, int pageSize, 
				String sortField, SortOrder sortOrder,
				Map<String, String> filters) {
				
				List<Perfil> perfis = new ArrayList<Perfil>();

				if (sortField == null) {
					sortField = "descricao";
					sortOrder = SortOrder.ASCENDING;
				}

				perfilBusca.setSortField(sortField);
				perfilBusca.setSortOrder(sortOrder);
				perfis = perfilService.pesquisar(perfilBusca,first, pageSize);

				return perfis;
			}
		};
		int quantidade = perfilService.quantidade(perfilBusca);
		perfis.setRowCount(quantidade == 0 ? 1 : quantidade);
	}
	
	public String cadastro() {
		return "cadastrarPerfil?faces-redirect=true";
	}

   public void excluir(){
	    perfilService.excluir(perfilSelecionado);
		consultar();
	    infoMsg(ResourceBundle.getMessage("perfilExcluir")+ ".", "");
   }  	

   public LazyDataModel<Perfil> getPerfis() {
		return perfis;
	}

	public void setPerfis(LazyDataModel<Perfil> perfis) {
		this.perfis = perfis;
	}

	public Perfil getPerfilSelecionado() {
		return perfilSelecionado;
	}

	public void setPerfilSelecionado(Perfil perfilSelecionado) {
		this.perfilSelecionado = perfilSelecionado;
	}

	public void setPerfilBusca(Perfil perfilBusca) {
		this.perfilBusca = perfilBusca;
	}

	public Perfil getPerfilBusca() {
		return perfilBusca;
	}
	
	public TipoPerfil[] getTiposPerfil() {
		return TipoPerfil.values();
	}

}
