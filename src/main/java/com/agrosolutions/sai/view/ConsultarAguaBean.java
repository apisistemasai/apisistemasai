package com.agrosolutions.sai.view;

import java.io.Serializable;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.Agua;
import com.agrosolutions.sai.model.Irrigante;
import com.agrosolutions.sai.model.TipoPerfil;
import com.agrosolutions.sai.service.AguaService;
import com.agrosolutions.sai.service.IrriganteService;
import com.agrosolutions.sai.util.ResourceBundle;

/**
* Use case : Consultar Agua
* 
* @since   : julho 05, 2012, 14:00:20 AM
* @author  : walljr@gmail.com
*/

@Component("consultarAguaBean")
@Scope("session")
public class ConsultarAguaBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = 2290911949522172496L;

	public static Logger logger = Logger.getLogger(ConsultarAguaBean.class);

	public static final String MSG_TITULO = "Consultar Agua";

	@Autowired
	private AguaService aguaService;
	@Autowired
	private IrriganteService irriganteService;
	
	private List<Agua> aguas;
	private Agua aguaSelecionado;
	private Irrigante irrigante;
		
	
	public void carregarDadosIniciais() {
		if (getUsuarioLogado().getPerfil().getTipo().equals(TipoPerfil.Irrigante)) {
			irrigante = irriganteService.pesquisarPorUsuario(getUsuarioLogado());
		}
		aguas = aguaService.pesquisar(irrigante);
	}

	public void excluir(){
		aguaService.excluir(aguaSelecionado);
		aguaSelecionado = null;
		infoMsg(ResourceBundle.getMessage("aguaExcluir")+ ".", MSG_TITULO);
	}
	
	public String cadastro() {
		return "cadastrarAgua?faces-redirect=true";
	}
	
	public String voltar() {
		return "consultarIrrigante?faces-redirect=true";
	}

	public List<Agua> getAguas() {
		return aguas;
	}

	public void setAguas(List<Agua> aguas) {
		this.aguas = aguas;
	}

	public Irrigante getIrrigante() {
		return irrigante;
	}

	public void setIrrigante(Irrigante irrigante) {
		this.irrigante = irrigante;
	}

	public Agua getAguaSelecionado() {
		return aguaSelecionado;
	}

	public void setAguaSelecionado(Agua aguaSelecionado) {
		this.aguaSelecionado = aguaSelecionado;
	}

}
