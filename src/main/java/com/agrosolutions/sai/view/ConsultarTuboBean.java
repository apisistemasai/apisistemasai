package com.agrosolutions.sai.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.Fabricante;
import com.agrosolutions.sai.model.TipoFabricante;
import com.agrosolutions.sai.model.TipoTubo;
import com.agrosolutions.sai.model.Tubo;
import com.agrosolutions.sai.service.FabricanteService;
import com.agrosolutions.sai.service.TuboService;
import com.agrosolutions.sai.util.ResourceBundle;

/**
* Use case : Consultar Tubo
* 
* @since   : Maio 15, 2012, 12:55:20 AM
* @author  : walljr@gmail.com
*/

@Component("consultarTuboBean")
@Scope("view")
public class ConsultarTuboBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = 1044600311686654047L;

	public static Logger logger = Logger.getLogger(ConsultarTuboBean.class);

	public static final String MSG_TITULO = "Consultar Tubos";

	@Autowired
	private TuboService tuboService;
	@Autowired
	private FabricanteService fabricanteService;
	
	private LazyDataModel<Tubo> tubos;
	private Tubo tuboSelecionada;
	private Tubo tuboBusca;
	private List<Fabricante> fabricantes;

	public void carregarDadosIniciais() {
		if (tuboBusca == null){
			tuboBusca = new Tubo();
			tuboBusca.setSortField("modelo");
			tuboBusca.setSortOrder(SortOrder.ASCENDING);
			
			this.setFabricantes(fabricanteService.pesquisar(TipoFabricante.Tubo));
			consultar();
		}
	}

	public void consultar(){
		tubos = new LazyDataModel<Tubo>() {

			private static final long serialVersionUID = 4733929438754918810L;

			@Override
			public List<Tubo> load(int first, int pageSize, String sortField,
					SortOrder sortOrder, Map<String, String> filters){ 
				List<Tubo> tubos = new ArrayList<Tubo>();

				if (sortField == null) {
					sortField = "modelo";
					sortOrder = SortOrder.ASCENDING;
				}

				tuboBusca.setSortField(sortField);
				tuboBusca.setSortOrder(sortOrder);
				tubos = tuboService.pesquisar(tuboBusca, first, pageSize);

				return tubos;
			}
		};
		int quantidade = tuboService.quantidade(tuboBusca);
		tubos.setRowCount(quantidade == 0 ? 1 : quantidade);
	}
	
	public void excluir(){
	    tuboService.excluir(tuboSelecionada);
	    consultar();
		infoMsg(ResourceBundle.getMessage("tuboExcluir")+ ".", MSG_TITULO);
   }

   public String cadastro() {
		return "cadastrarTubo?faces-redirect=true";
	}

public Tubo getTuboSelecionada() {
	return tuboSelecionada;
}

public void setTuboSelecionada(Tubo tuboSelecionada) {
	this.tuboSelecionada = tuboSelecionada;
}

public void setTubos(LazyDataModel<Tubo> tubos) {
	this.tubos = tubos;
}

public LazyDataModel<Tubo> getTubos() {
	return tubos;
}

public void setTuboBusca(Tubo tuboBusca) {
	this.tuboBusca = tuboBusca;
}

public Tubo getTuboBusca() {
	return tuboBusca;
}

public void setFabricantes(List<Fabricante> fabricantes) {
	this.fabricantes = fabricantes;
}

public List<Fabricante> getFabricantes() {
	return fabricantes;
}

public TipoTubo[] getTipos() {
	return TipoTubo.values();
}	

}
