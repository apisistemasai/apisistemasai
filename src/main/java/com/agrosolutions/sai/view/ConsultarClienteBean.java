package com.agrosolutions.sai.view;

import java.io.Serializable;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.Cliente;
import com.agrosolutions.sai.service.ClienteService;
import com.agrosolutions.sai.util.ResourceBundle;

/**
 * Use case : Consultar Cliente
 * 
 * @since : Nov 29, 2011, 22:23:20 AM
 * @author : janine.freitas@ivia.com.br 
 */

@Component("consultarClienteBean")
@Scope("view")
public class ConsultarClienteBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = 3014368717947587732L;

	public static Logger logger = Logger
			.getLogger(ConsultarClienteBean.class);

	public static final String MSG_TITULO = "Consultar Cliente";

	@Autowired
	private ClienteService clienteService;

	private List<Cliente> clientes;
	private Cliente clienteSelecionado;

	public void carregarDadosIniciais() {
		clientes = clienteService.obterTodos();
	}

	public void excluir() {
		clienteService.excluir(clienteSelecionado);
		clientes = clienteService.obterTodos();
		infoMsg(ResourceBundle.getMessage("clienteExcluir")+ ".", MSG_TITULO);
	}

	public String cadastro() {
		return "cadastrarCliente?faces-redirect=true";
	}

	public Cliente getClienteSelecionado() {
		return clienteSelecionado;
	}

	public void setClienteSelecionado(Cliente clienteSelecionado) {
		this.clienteSelecionado = clienteSelecionado;
	}
	public List<Cliente> getClientes() {
		return clientes;
	}
}
