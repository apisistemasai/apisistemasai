package com.agrosolutions.sai.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.Emissor;
import com.agrosolutions.sai.model.Fabricante;
import com.agrosolutions.sai.model.TipoEmissor;
import com.agrosolutions.sai.service.EmissorService;
import com.agrosolutions.sai.service.FabricanteService;
import com.agrosolutions.sai.util.ResourceBundle;

/**
* Use case : Consultar Emissor
* 
* @since   : Jan 07, 2011, 01:23:20 AM
* @author  : raphael.ferreira@ivia.com.br
*/

@Component("consultarEmissorBean")
@Scope("view")
public class ConsultarEmissorBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = -744241185264735963L;

	public static Logger logger = Logger.getLogger(ConsultarEmissorBean.class);

	public static final String MSG_TITULO = "Consultar Emissores";

	@Autowired
	private EmissorService emissorService;
	@Autowired
	private FabricanteService fabricanteService;
	
	private LazyDataModel<Emissor> emissores;
	private Emissor emissorSelecionado;
	private Emissor emissorBusca;
	private List<Fabricante> fabricantes;
	
	public void carregarDadosIniciais() {
		if (emissorBusca == null){
			emissorBusca = new Emissor();
			emissorBusca.setSortField("modelo");
			emissorBusca.setSortOrder(SortOrder.ASCENDING);
			
			consultar();
		}
	}
	
	public void consultar(){
		emissores = new LazyDataModel<Emissor>() {
			private static final long serialVersionUID = 2112009557784844759L;

			@Override
			public List<Emissor> load(int first, int pageSize, String sortField,
					SortOrder sortOrder, Map<String, String> filters){ 
				List<Emissor> emissores = new ArrayList<Emissor>();

				if (sortField == null) {
					sortField = "modelo";
					sortOrder = SortOrder.ASCENDING;
				}

				emissorBusca.setSortField(sortField);
				emissorBusca.setSortOrder(sortOrder);
				emissores = emissorService.pesquisar(emissorBusca, first, pageSize);

				return emissores;
			}
		};
		int quantidade = emissorService.quantidade(emissorBusca);
		emissores.setRowCount(quantidade == 0 ? 1 : quantidade);
	}

	public void excluir(){
	    emissorService.excluir(emissorSelecionado);
	    emissorSelecionado = null;
		consultar();

		infoMsg(ResourceBundle.getMessage("emissorExcluir")+ ".", "");
   }

   public String cadastro() {
		return "cadastrarEmissor?faces-redirect=true";
	}

	public Emissor getEmissorSelecionado() {
		return emissorSelecionado;
	}

	public void setEmissorSelecionado(Emissor emissorSelecionado) {
		this.emissorSelecionado = emissorSelecionado;
	}

	public void setEmissores(LazyDataModel<Emissor> emissores) {
		this.emissores = emissores;
	}

	public LazyDataModel<Emissor> getEmissores() {
		return emissores;
	}

	public void setEmissorBusca(Emissor emissorBusca) {
		this.emissorBusca = emissorBusca;
	}

	public Emissor getEmissorBusca() {
		return emissorBusca;
	}

	public void setFabricantes(List<Fabricante> fabricantes) {
		this.fabricantes = fabricantes;
	}

	public List<Fabricante> getFabricantes() {
		return fabricantes;
	}
	public TipoEmissor[] getTipos() {
		return TipoEmissor.values();
	}
}
