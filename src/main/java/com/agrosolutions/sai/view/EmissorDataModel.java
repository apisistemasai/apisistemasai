package com.agrosolutions.sai.view;

import java.util.List;

import javax.faces.model.ListDataModel;

import org.primefaces.model.SelectableDataModel;

import com.agrosolutions.sai.model.Emissor;
  
public class EmissorDataModel extends ListDataModel<Emissor> implements SelectableDataModel<Emissor> {    
  
    public EmissorDataModel() {  
    }  
  
    public EmissorDataModel(List<Emissor> data) {  
        super(data);  
    }  
      
    @Override  
    public Emissor getRowData(String rowKey) {  
          
        @SuppressWarnings("unchecked")
		List<Emissor> datas = (List<Emissor>) getWrappedData();  
          
        for(Emissor data : datas) {  
            if(data.getId().equals(rowKey))  
                return data;  
        }  
          
        return null;  
    }  
  
    @Override  
    public Object getRowKey(Emissor data) {  
        return data.getId();  
    }  
}  
                     