package com.agrosolutions.sai.view;

import java.io.Serializable;
import java.util.List;

import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.Defensivo;
import com.agrosolutions.sai.service.DefensivoService;
import com.agrosolutions.sai.util.ResourceBundle;

/**
* Use case : Consultar Defensivo
* 
* @since   : Jan 27, 2012, 23:23:20 AM
* @author  : raphael.ferreira@ivia.com.br
*/

@Component("consultarDefensivoBean")
@Scope("view")
public class ConsultarDefensivoBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = -921419392669459739L;

	public static final String MSG_TITULO = "Consultar Defensivos";

	@Autowired
	private DefensivoService defensivoService;
	
	private List<Defensivo> defensivos;
	private Defensivo defensivoSelecionado;
	private Defensivo defensivoBusca;
	
	public void carregarDadosIniciais() {
		if (defensivoBusca == null){
			defensivoBusca = new Defensivo();
			defensivoBusca.setSortField("nome");
			defensivoBusca.setSortOrder(SortOrder.ASCENDING);
			
			consultar();
		}
	}
	
	public void consultar(){
		defensivos = defensivoService.pesquisar(defensivoBusca, null, null); 
	}
	
	public String cadastro() {
		return "cadastrarDefensivo?faces-redirect=true";
	}

   public void excluir(){
	   defensivoService.excluir(defensivoSelecionado);
		infoMsg(ResourceBundle.getMessage("defensivoExcluir")+ ".", MSG_TITULO);
   }

	public List<Defensivo> getDefensivos() {
		return defensivos;
	}
	
	public void setDefensivos(List<Defensivo> defensivos) {
		this.defensivos = defensivos;
	}
	
	public Defensivo getDefensivoSelecionado() {
		return defensivoSelecionado;
	}
	
	public void setDefensivoSelecionado(Defensivo defensivoSelecionado) {
		this.defensivoSelecionado = defensivoSelecionado;
	}

	public void setDefensivoBusca(Defensivo defensivoBusca) {
		this.defensivoBusca = defensivoBusca;
	}

	public Defensivo getDefensivoBusca() {
		return defensivoBusca;
	}  	

}
