package com.agrosolutions.sai.view;

import java.io.Serializable;
import java.util.List;

import javax.faces.event.ActionEvent;

import org.apache.log4j.Logger;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.Elemento;
import com.agrosolutions.sai.service.ElementoService;
import com.agrosolutions.sai.util.ResourceBundle;

/**
* Use case : Consultar e Cadastrar Elemento de Nutri��o
* 
* @since   : Dez 03, 2015, 23:23:20 AM
* @author  : raphael@iasoft.me
*/

@Component("consultarElementoBean")
@Scope("view")
public class ConsultarElementoBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = 6334902747812228155L;

	public static Logger logger = Logger.getLogger(ConsultarElementoBean.class);
	public static String prefix = "ConsultarElementoBean -";

	@Autowired
	private ElementoService elementoService;
	
	private List<Elemento> elementos;
	private Elemento elementoSelecionado;
	private Elemento elementoBusca;
	
	public void carregarDadosIniciais() {
		logger.debug(prefix + "carregarDadosIniciais....starting..." );
		if (elementoBusca == null){
			elementoBusca = new Elemento();
			elementoBusca.setSortField("nome");
			elementoBusca.setSortOrder(SortOrder.ASCENDING);
			
			consultar();	
		}
		logger.debug(prefix + "carregarDadosIniciais....end" );
	}

	public void adicionarElemento(ActionEvent actionEvent) {
		logger.debug(prefix + "adicionarElemento....starting...elementos:" + elementos==null?0:elementos.size());
		//Cria novo elemento
		Elemento elemento = new Elemento();
		elementos.add(elemento);
		logger.debug(prefix + "adicionarElemento....end....elementos:" + elementos==null?0:elementos.size());
    }

	public void salvar(ActionEvent actionEvent) {
		if (elementos.size()> 0){
			for (Elemento elemento : elementos) {
				elementoService.salvar(elemento);
			}
			consultar();
			infoMsg(ResourceBundle.getMessage("elementoSalvar"), null);
		}
    }
	
	public void consultar(){	
		logger.debug(prefix + "consultar:" + elementoBusca.getNome());
		elementos = elementoService.pesquisar(elementoBusca, null, null);
	}

	public void excluir(){
		logger.debug(prefix + "excluir....starting...elemento:" + elementoSelecionado.getNome());
		elementoService.excluir(elementoSelecionado);
		consultar();
	    infoMsg(ResourceBundle.getMessage("elementoExcluir"),"");
	}

	public ElementoService getElementoService() {
		return elementoService;
	}

	public void setElementoService(ElementoService elementoService) {
		this.elementoService = elementoService;
	}

	public List<Elemento> getElementos() {
		return elementos;
	}

	public void setElementos(List<Elemento> elementos) {
		this.elementos = elementos;
	}

	public Elemento getElementoSelecionado() {
		return elementoSelecionado;
	}

	public void setElementoSelecionado(Elemento elementoSelecionado) {
		this.elementoSelecionado = elementoSelecionado;
	}

	public Elemento getElementoBusca() {
		return elementoBusca;
	}

	public void setElementoBusca(Elemento elementoBusca) {
		this.elementoBusca = elementoBusca;
	}
}
