package com.agrosolutions.sai.view;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.primefaces.model.StreamedContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.exception.SaiException;
import com.agrosolutions.sai.util.DateUtils;
import com.agrosolutions.sai.util.RelatorioUtil;
import com.agrosolutions.sai.util.ResourceBundle;

/**
* Use case : Relatorio de Plantio Expirado
* 
* @since   : Jun 14, 2013, 20:23:20 PM
* @author  : rpf1404@gmail.com
*/

@Component("relPlantioExpiradoBean")
@Scope("session")
public class RelPlantioExpiradoBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = 2000210947938981045L;

	public static Logger logger = Logger.getLogger(RelPlantioExpiradoBean.class);

	@Autowired
	private RelatorioUtil relatorioUtil;

	private Date data;
	private StreamedContent file;
	
	public String imprimir() {
		boolean erro = false;
		try {

			// validando campos da entidade
			if ( getData() == null ){
				errorMsg(ResourceBundle.getMessage("validacaoData")+ ".", null);
				erro = true;
			}

			if (erro) {
				return null;
			}

			Map<String, Object> parametros = new HashMap<String, Object>();
			
			parametros.put("data", "'" + DateUtils.toString(getData()) + "'");
			parametros.put("listaDistrito", prepararDistritos());
			file = relatorioUtil.relatorio("rptPlantio_Expirado.jasper", parametros, "plantioExpirado.pdf");

		} catch (SaiException e) {
			errorMsg(ResourceBundle.getMessage("ocorreuErro")+ ":" + e.getMessage(), null);
			logger.warn("Ocorreu o seguinte erro: " + e.getMessage() + e.getCause().getMessage());
			return null;
		} catch (Exception e) {
			errorMsg(ResourceBundle.getMessage("ocorreuErro")+ ":" + e.getMessage(), null);
			logger.fatal("Ocorreu o seguinte erro: " + e.getMessage() + e.getCause().getMessage());
			return null;
		}
		infoMsg(ResourceBundle.getMessage("demandaRelatorio")+ ".", null);
		return null;
	}


	public Date getData() {
		return data;
	}


	public void setData(Date data) {
		this.data = data;
	}


	public StreamedContent getFile() {
		return file;
	}

	public void setFile(StreamedContent file) {
		this.file = file;
	}

}
