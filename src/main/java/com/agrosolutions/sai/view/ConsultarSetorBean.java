package com.agrosolutions.sai.view;

import java.io.Serializable;
import java.util.List;

import javax.faces.model.SelectItem;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.Irrigante;
import com.agrosolutions.sai.model.Setor;
import com.agrosolutions.sai.model.TipoSolo;
import com.agrosolutions.sai.service.SetorService;
import com.agrosolutions.sai.util.ResourceBundle;

/**
* Use case : Consultar Setor
* 
* @since   : Jan 06, 2011, 19:23:20 AM
* @author  : raphael.ferreira@ivia.com.br
*/

@Component("consultarSetorBean")
@Scope("session")
public class ConsultarSetorBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = 2313412072709520370L;

	public static Logger logger = Logger.getLogger(ConsultarSetorBean.class);

	public static final String MSG_TITULO = "Consultar Setores";

	@Autowired
	private SetorService setorService;
	
	private List<Setor> setores;
	private Setor setorSelecionado;
	private Irrigante irrigante;
	private SelectItem[] tiposSolo;
	
	
	public void carregarDadosIniciais() {
		setores = setorService.pesquisar(irrigante);
		tiposSolo = criarFiltroTiposSolo();
	}

	private SelectItem[] criarFiltroTiposSolo()  {  
		SelectItem[] options = new SelectItem[TipoSolo.values().length + 1];  
		
		options[0] = new SelectItem("", "Todas");  
		for(int i = 0; i < TipoSolo.values().length; i++) {  
		    options[i + 1] = new SelectItem(TipoSolo.values()[i].getDescricao(), TipoSolo.values()[i].getDescricao());  
		}  
		return options;  
	}  
 
	public void excluir(){
		setorService.excluir(setorSelecionado);
		setorSelecionado = null;
		infoMsg(ResourceBundle.getMessage("setorExcluir")+ ".", MSG_TITULO);
	}
	
	public String cadastro() {
		return "cadastrarSetor?faces-redirect=true";
	}
	
	public String voltar() {
		return "consultarIrrigante?faces-redirect=true";
	}

	public List<Setor> getSetores() {
		return setores;
	}

	public void setSetores(List<Setor> setores) {
		this.setores = setores;
	}

	public Irrigante getIrrigante() {
		return irrigante;
	}

	public void setIrrigante(Irrigante irrigante) {
		this.irrigante = irrigante;
	}

	public Setor getSetorSelecionado() {
		return setorSelecionado;
	}

	public void setSetorSelecionado(Setor setorSelecionado) {
		this.setorSelecionado = setorSelecionado;
	}

	public SelectItem[] getTiposSolo() {
		return tiposSolo;
	}

	public void setTiposSolo(SelectItem[] tiposSolo) {
		this.tiposSolo = tiposSolo;
	}

}
