package com.agrosolutions.sai.view;

import java.util.List;

import javax.faces.model.ListDataModel;

import org.primefaces.model.SelectableDataModel;

import com.agrosolutions.sai.model.Bomba;
  
public class BombaDataModel extends ListDataModel<Bomba> implements SelectableDataModel<Bomba> {    
  
    public BombaDataModel() {  
    }  
  
    public BombaDataModel(List<Bomba> data) {  
        super(data);  
    }  
      
    @Override  
    public Bomba getRowData(String rowKey) {  
          
        @SuppressWarnings("unchecked")
		List<Bomba> datas = (List<Bomba>) getWrappedData();  
          
        for(Bomba bomba : datas) {  
            if(bomba.getId().equals(rowKey))  
                return bomba;  
        }  
          
        return null;  
    }  
  
    @Override  
    public Object getRowKey(Bomba bomba) {  
        return bomba.getId();  
    }  
}  
                     