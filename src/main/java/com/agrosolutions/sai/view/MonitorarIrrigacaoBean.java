package com.agrosolutions.sai.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.Area;
import com.agrosolutions.sai.model.Cultivo;
import com.agrosolutions.sai.model.Cultura;
import com.agrosolutions.sai.model.Distrito;
import com.agrosolutions.sai.model.Irrigacao;
import com.agrosolutions.sai.model.Irrigante;
import com.agrosolutions.sai.model.Plantio;
import com.agrosolutions.sai.model.Variedade;
import com.agrosolutions.sai.service.AreaService;
import com.agrosolutions.sai.service.CulturaService;
import com.agrosolutions.sai.service.IrrigacaoService;
import com.agrosolutions.sai.service.VariedadeService;
import com.agrosolutions.sai.util.ResourceBundle;

/**
* Use case : Monitorar Irriga��o
* 
* @since   : Jul 25, 2012, 20:23:20 PM
* @author  : rpf1404@gmail.com
*/

@Component("monitorarIrrigacaoBean")
@Scope("session")
public class MonitorarIrrigacaoBean extends ManagerBean implements Serializable {


	private static final long serialVersionUID = 7393562986149156504L;

	public static Logger logger = Logger.getLogger(MonitorarIrrigacaoBean.class);

	@Autowired
	private IrrigacaoService irrigacaoService;
	@Autowired
	private AreaService areaService;
	@Autowired
	private CulturaService culturaService;
	@Autowired
	private VariedadeService variedadeService;
	
	private Irrigacao[] irrigacoesSelecionadas;
	private LazyDataModel<Irrigacao> irrigacoes;
	private Irrigacao irrigacaoBusca;
	private Irrigacao irrigacaoSelecionado;
	private List<Area> areas;
	private List<Cultura> culturas;
	private List<Variedade> variedades;
	private String motivo;
	   
   public void carregarDadosIniciais() {
	   //	Reseta o objeto da busca quando muda de distrito
	   if ( super.getMultidistrito() ) {
		   if ( (this.irrigacaoBusca != null) && (super.getDistritoPadrao().getId() != this.irrigacaoBusca.getPlantio().getCultivo().getIrrigante().getArea().getDistrito().getId()) )
			   irrigacaoBusca = null;			
		}
	   
	   if(irrigacaoBusca == null){
		   irrigacaoBusca = new Irrigacao();
		   irrigacaoBusca.setPlantio(new Plantio());
		   irrigacaoBusca.getPlantio().setEnviarEmail(null);
		   irrigacaoBusca.getPlantio().setEnviarSMS(null);
		   irrigacaoBusca.getPlantio().setCultivo(new Cultivo());
		   irrigacaoBusca.getPlantio().getCultivo().setIrrigante(new Irrigante());
		   irrigacaoBusca.setSortField("tempoIrrigacao");
		   irrigacaoBusca.setSortOrder(SortOrder.ASCENDING);
		   irrigacaoBusca.setData(new Date());
		   preparaBuscaDistrito();
		   
		   //Popular combobox de filtros
		   this.areas = areaService.pesquisar(super.getDistritosPesquisa());
		   this.culturas = culturaService.obterTodos();
		   //Fazer consulta inicial
		   motivo = "";
		   consultar();
	   }
	}

	public void carregarVariedades() {
		if(irrigacaoBusca.getPlantio().getCultivo().getCultura() == null){
			variedades = null;
		}else{
			variedades = variedadeService.pesquisar(irrigacaoBusca.getPlantio().getCultivo().getCultura());
		}
		irrigacaoBusca.getPlantio().getCultivo().setVariedade(null);
	}  
   
	private void preparaBuscaDistrito() {
		Distrito distrito = new Distrito();
		distrito.setId(super.getDistritosPesquisa().get(0).getId());
		Area area = new Area();
		area.setDistrito(distrito);
		irrigacaoBusca.getPlantio().getCultivo().getIrrigante().setArea(area);
	}
   
	public void consultar() {
		if (irrigacaoBusca.getPlantio().getCultivo().getIrrigante().getArea() == null) {
			preparaBuscaDistrito();
		}
		irrigacoes = new LazyDataModel<Irrigacao>() {

			private static final long serialVersionUID = 1940101658734393569L;

			@Override
			public List<Irrigacao> load(int first, int pageSize, String sortField,
					SortOrder sortOrder, Map<String, String> filters) {
				List<Irrigacao> irrigacoes = new ArrayList<Irrigacao>();

		        if(sortField == null){
					sortField = "tempoIrrigacao";
					sortOrder = SortOrder.ASCENDING;
				}
				if (irrigacaoBusca.getPlantio().getCultivo().getIrrigante().getArea() == null) {
					preparaBuscaDistrito();
				}
		        
				irrigacaoBusca.setSortField(sortField);
				irrigacaoBusca.setSortOrder(sortOrder);
				irrigacoes = irrigacaoService.pesquisar(irrigacaoBusca, first, pageSize);

				return irrigacoes;
			}
        };
        irrigacoes.setRowCount(irrigacaoService.quantidade(irrigacaoBusca));
	}
	public String consultarIrrigante() {
		return "/paginas/irrigante/consultarIrrigante?faces-redirect=true";
	}
   public void excluir(){
	  if (irrigacoesSelecionadas == null) {
	   infoMsg(ResourceBundle.getMessage("irrigacaoSelecionada")+ ".", null);
	  }
	  else {
	   for (Irrigacao i : irrigacoesSelecionadas) {
		   	i.setMotivo(motivo);
            irrigacaoService.excluir(i);  
      }  
		infoMsg(ResourceBundle.getMessage("irrigacaoExcluir")+ ".", null);
	  }
   }
   
	public String recalcular() {
		irrigacaoService.recalcular(getUsuarioLogado());
		infoMsg(ResourceBundle.getMessage("recalculoSucesso")+ ".","");
		return null;
	}

	public LazyDataModel<Irrigacao> getIrrigacoes() {
		return irrigacoes;
	}
	
	public void setIrrigacoes(LazyDataModel<Irrigacao> irrigacoes) {
		this.irrigacoes = irrigacoes;
	}
	
	public Irrigacao getIrrigacaoBusca() {
		return irrigacaoBusca;
	}
	
	public void setIrrigacaoBusca(Irrigacao irrigacaoBusca) {
		this.irrigacaoBusca = irrigacaoBusca;
	}
	
	public Irrigacao getIrrigacaoSelecionado() {
		return irrigacaoSelecionado;
	}
	
	public void setIrrigacaoSelecionado(Irrigacao irrigacaoSelecionado) {
		this.irrigacaoSelecionado = irrigacaoSelecionado;
	}
	
	public List<Area> getAreas() {
		return areas;
	}
	
	public void setAreas(List<Area> areas) {
		this.areas = areas;
	}
	
	public List<Cultura> getCulturas() {
		return culturas;
	}
	
	public void setCulturas(List<Cultura> culturas) {
		this.culturas = culturas;
	}
	
	public List<Variedade> getVariedades() {
		return variedades;
	}
	
	public void setVariedades(List<Variedade> variedades) {
		this.variedades = variedades;
	}
	
	public Irrigacao[] getIrrigacoesSelecionadas() {
		return irrigacoesSelecionadas;
	}
	
	public void setIrrigacoesSelecionadas(Irrigacao[] irrigacoesSelecionadas) {
		this.irrigacoesSelecionadas = irrigacoesSelecionadas;
	}

	public String getMotivo() {
		return motivo;
	}

	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}
}
