package com.agrosolutions.sai.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.ArduinoIn;
import com.agrosolutions.sai.service.ArduinoInService;


@Component("consultarSensoresBean")
@Scope("session")
public class ConsultarSensoresBean extends ManagerBean implements Serializable {
	
	private static final long serialVersionUID = 5481270010824088774L;
	public static Logger logger = Logger.getLogger(ConsultarSensoresBean.class);
	
	public static final String MSG_TITULO = "Consultar Sensores";
	
	@Autowired
	private ArduinoInService arduinoInService;
	private List<ArduinoIn> sensores = new ArrayList<ArduinoIn>();
	
	
	public void carregarDadosIniciais() {
		sensores = arduinoInService.obterTodos();
	}


	public List<ArduinoIn> getSensores() {
		return sensores;
	}
	
	public void setSensores(List<ArduinoIn> sensores) {
		this.sensores = sensores;
	}

}
