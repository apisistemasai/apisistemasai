package com.agrosolutions.sai.view;

import java.util.List;

import javax.faces.model.ListDataModel;

import org.primefaces.model.SelectableDataModel;

import com.agrosolutions.sai.model.Nutriente;
  
public class NutrienteDataModel extends ListDataModel<Nutriente> implements SelectableDataModel<Nutriente> {    
  
    public NutrienteDataModel() {  
    }  
  
    public NutrienteDataModel(List<Nutriente> data) {  
        super(data);  
    }  
      
    @Override  
    public Nutriente getRowData(String rowKey) {  
          
        @SuppressWarnings("unchecked")
		List<Nutriente> datas = (List<Nutriente>) getWrappedData();  
          
        for(Nutriente data : datas) {  
            if(data.getId().equals(rowKey))  
                return data;  
        }  
          
        return null;  
    }  
  
    @Override  
    public Object getRowKey(Nutriente data) {  
        return data.getId();  
    }  
}  
                     