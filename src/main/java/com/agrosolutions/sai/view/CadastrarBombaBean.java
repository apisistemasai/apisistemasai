package com.agrosolutions.sai.view;

import java.io.Serializable;
import java.util.List;

import javax.faces.event.ActionEvent;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.AcionamentoBomba;
import com.agrosolutions.sai.model.Bomba;
import com.agrosolutions.sai.model.Fabricante;
import com.agrosolutions.sai.model.TipoBomba;
import com.agrosolutions.sai.model.TipoFabricante;
import com.agrosolutions.sai.service.BombaService;
import com.agrosolutions.sai.service.FabricanteService;
import com.agrosolutions.sai.util.ResourceBundle;

/**
* Use case : Cadastrar Bomba
* 
* @since   : Jan 06, 2012, 22:10:20 AM
* @author  : raphael.ferreira@ivia.com.br
*/

@Component("cadastrarBombaBean")
@Scope("session")
public class CadastrarBombaBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = -425450556876285711L;

	public static Logger logger = Logger.getLogger(CadastrarBombaBean.class);

	public static final String MSG_TITULO = "Cadastro de Bomba";

	@Autowired
	private BombaService bombaService;
	@Autowired
	private FabricanteService fabricanteService;
	
	private Bomba bomba;
	private List<Fabricante> fabricantes;
	private Fabricante fabricante;
	private String operacao;
	
	public void carregarDadosIniciais() {
		operacao = getOperacao();
		bomba = getBomba();
		fabricante = getFabricante();
		
		if (operacao.equals("NOVO") && bomba == null) {
			bomba = new Bomba();
		}
		if (fabricantes == null) {
			fabricantes = fabricanteService.pesquisar(TipoFabricante.Bomba);
		}
		if (fabricante != null) {
			bomba.setFabricante(fabricante);
			fabricante = null;
		}
	}
	
	public void gravar(ActionEvent actionEvent) {
		bomba = bombaService.salvar(bomba);
		if (operacao.equals("NOVO")) {
			bomba = null;
			infoMsg(ResourceBundle.getMessage("bombaSalvar")+ ".", MSG_TITULO);
		}else{
			infoMsg(ResourceBundle.getMessage("bombaAlterar")+ ".", MSG_TITULO);
		}
    }

	public String novoFabricante() {
		return "cadastrarFabricante?faces-redirect=true";
	}

	public String voltar() {
		return "consultarBomba?faces-redirect=true";
	}
	
	public Bomba getBomba() {
		if (bomba == null) {
			bomba = (Bomba) getFlashScope().get("bomba");
		}
		return bomba;
	}

	public void setBomba(Bomba bomba) {
		this.bomba = bomba;
		getFlashScope().put("bomba", bomba);
	}

	public List<Fabricante> getFabricantes() {
		return fabricantes;
	}

	public void setFabricantes(List<Fabricante> fabricantes) {
		this.fabricantes = fabricantes;
	}

	public Fabricante getFabricante() {
		if (fabricante == null) {
			fabricante = (Fabricante) getFlashScope().get("fabricante");
		}
		return fabricante;
	}

	public void setFabricante(Fabricante fabricante) {
		this.fabricante = fabricante;
		getFlashScope().put("fabricante", fabricante);
		
	}

	public TipoBomba[] getTipos() {
		return TipoBomba.values();
	}
	public AcionamentoBomba[] getAcionamentos() {
		return AcionamentoBomba.values();
	}

	public String getOperacao() {
		if (operacao == null) {
			operacao = (String) getFlashScope().get("operacao");
		}
		return operacao;
	}

	public void setOperacao(String operacao) {
		this.operacao = operacao;
		getFlashScope().put("operacao", operacao);
	}
}
