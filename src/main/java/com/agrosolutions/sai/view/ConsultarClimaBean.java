package com.agrosolutions.sai.view;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.Area;
import com.agrosolutions.sai.model.Clima;
import com.agrosolutions.sai.model.Mes;
import com.agrosolutions.sai.model.TipoPerfil;
import com.agrosolutions.sai.service.ClimaService;
import com.agrosolutions.sai.service.IrriganteService;
import com.agrosolutions.sai.util.ResourceBundle;

/**
* Use case : Consultar Clima
* 
* @since   : Mar 08, 2011, 19:23:20 AM
* @author  : rpf1404@gmail.com
*/

@Component("consultarClimaBean")
@Scope("session")
public class ConsultarClimaBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = -5658220567105554889L;

	public static Logger logger = Logger.getLogger(ConsultarClimaBean.class);

	public static final String MSG_TITULO = "Consultar Clima";

	@Autowired
	private ClimaService climaService;
	@Autowired
	private IrriganteService irriganteService;
	
	private List<Clima> climas;
	private Area area;
	private Integer ano;
	private Mes mes;

	
	public void carregarDadosIniciais() {
		if (area == null && getUsuarioLogado().getPerfil().getTipo().equals(TipoPerfil.Irrigante)) {
			area = irriganteService.pesquisarPorUsuario(getUsuarioLogado()).getArea();
		}

		if (mes == null && ano == null) {
			Calendar data = new GregorianCalendar();
			data.setTime(new Date());
			ano = data.get(Calendar.YEAR);
			for (Mes m : Mes.values()) {
				if (m.ordinal() == data.get(Calendar.MONTH)){
					mes = m;		
				}
			}
		}
		if (mes != null && ano != null) {
			Calendar dataIni = new GregorianCalendar();
			Calendar dataFim = new GregorianCalendar();
			dataIni.set(ano, mes.ordinal(), 1);
			dataFim.set(ano, mes.ordinal()+1, 1);
			
			climas = climaService.pesquisarPorPeriodo(area, dataIni.getTime(), dataFim.getTime());
			if (climas.isEmpty()) {
				infoMsg(ResourceBundle.getMessage("climaExcluir")+ ".", null);
			}
		}
	}

    public String cadastro() {
		return "cadastrarClima?faces-redirect=true";
	}

	public String voltar() {
		if (getUsuarioLogado().getPerfil().getTipo().equals(TipoPerfil.Irrigante)) {
			return "/homeIrrigante?faces-redirect=true";
		} else {
			return "consultarArea?faces-redirect=true";
		}
	}

	public List<Clima> getClimas() {
		return climas;
	}

	public void setClimas(List<Clima> climas) {
		this.climas = climas;
	}

	public Area getArea() {
		return area;
	}

	public void setArea(Area area) {
		this.area = area;
	}

	public Mes[] getMeses() {
		return Mes.values();
	}

	public Integer getAno() {
		return ano;
	}

	public void setAno(Integer ano) {
		this.ano = ano;
	}

	public Mes getMes() {
		return mes;
	}

	public void setMes(Mes mes) {
		this.mes = mes;
	}

}
