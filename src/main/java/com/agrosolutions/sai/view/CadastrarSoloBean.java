package com.agrosolutions.sai.view;

import java.io.Serializable;

import javax.faces.event.ActionEvent;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.Solo;
import com.agrosolutions.sai.model.TipoSolo;
import com.agrosolutions.sai.service.SoloService;
import com.agrosolutions.sai.util.ResourceBundle;

/**
* Use case : Cadastrar Solo
* 
* @since   : Abr 01, 2012, 22:10:20 AM
* @author  : rpf1404@gmail.com
*/

@Component("cadastrarSoloBean")
@Scope("session")
public class CadastrarSoloBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = 3216438620459756201L;

	public static Logger logger = Logger.getLogger(CadastrarSoloBean.class);

	public static final String MSG_TITULO = "Cadastro de Solo";

	@Autowired
	private SoloService soloService;
	
	private Solo solo;
	private String operacao;

	public void carregarDadosIniciais() {
		operacao = getOperacao();
		solo = getSolo();

		if (operacao.equals("NOVO") && solo == null) {
			solo = new Solo();
		}
	}
	
	public void gravar(ActionEvent actionEvent) {
		solo = soloService.salvar(solo);
		if (operacao.equals("NOVO")) {
			solo = null;
			infoMsg(ResourceBundle.getMessage("soloSalvar")+ ".", MSG_TITULO);
		}else{
			infoMsg(ResourceBundle.getMessage("soloAlterar")+ ".", MSG_TITULO);
		}
    }

	public String voltar() {
		return "consultarSolo?faces-redirect=true";
	}
	
	public TipoSolo[] getTipos() {
		return TipoSolo.values();
	}

	public Solo getSolo() {
		if (solo == null) {
			solo = (Solo) getFlashScope().get("solo");
		}
		return solo;
	}

	public void setSolo(Solo solo) {
		this.solo = solo;
		getFlashScope().put("solo", solo);
	}

	public String getOperacao() {
		if (operacao == null) {
			operacao = (String) getFlashScope().get("operacao");
		}
		return operacao;
	}

	public void setOperacao(String operacao) {
		this.operacao = operacao;
		getFlashScope().put("operacao", operacao);
	}
}
