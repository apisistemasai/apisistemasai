package com.agrosolutions.sai.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.event.ActionEvent;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.Area;
import com.agrosolutions.sai.model.Distrito;
import com.agrosolutions.sai.model.Estacao;
import com.agrosolutions.sai.service.AreaService;
import com.agrosolutions.sai.service.DistritoService;
import com.agrosolutions.sai.service.EstacaoService;
import com.agrosolutions.sai.util.ResourceBundle;

/**
* Use case : Cadastrar Estacao
* 
* @since   : Mar 01, 2012, 15:00:20 AM
* @author  : raphael.ferreira@ivia.com.br
*/

@Component("cadastrarEstacaoBean")
@Scope("session")
public class CadastrarEstacaoBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = 4499512157516611325L;

	public static Logger logger = Logger.getLogger(CadastrarEstacaoBean.class);

	public static final String MSG_TITULO = "Cadastro de Estação";

	@Autowired
	private EstacaoService estacaoService;
	@Autowired
	private DistritoService distritoService;
	@Autowired
	private AreaService areaService;
	
	private Estacao estacao;
	private List<Distrito> distritos = new ArrayList<Distrito>();
	private Boolean multiDistrito;
	private List<Area> areas;
	private Area[] areasSelecionadas;
	private String operacao;

	public void carregarDadosIniciais() {
		operacao = getOperacao();
		estacao = getEstacao();
		distritos = getDistritosPesquisa();

		if (operacao.equals("NOVO") && estacao == null) {
			estacao = new Estacao();
			if ( multidistrito ) {
				estacao.setDistrito( getUsuarioLogado().getDistritoPadrao() );
			} else {
				estacao.setDistrito(getUsuarioLogado().getDistritos().get(0));
			}
		}
		
		if ((operacao.equals("ALTERAR") || operacao.equals("VER")) && estacao != null) {
			ArrayList<Distrito> dists = new ArrayList<Distrito>();
			dists.add(estacao.getDistrito());
			areas = areaService.pesquisar(dists);
			if(estacao.getAreas() != null){
				int i = 0;
				areasSelecionadas = new Area[estacao.getAreas().size()];
				for (Area reg : estacao.getAreas()) {
					areasSelecionadas[i++] = reg;
				}
			}
		}

		if (operacao.equals("ALTERAR") || operacao.equals("NOVO")){		
			areas = areaService.pesquisar(distritos);
		}
	}
	
	public void carregarAreas() {
		if(estacao.getDistrito()==null){
			areas = null;
		}else{
			ArrayList<Distrito> dists = new ArrayList<Distrito>();
			dists.add(estacao.getDistrito());
			areas = areaService.pesquisar(dists);
		}
	}  
	
	public void gravar(ActionEvent actionEvent) {
		prepararAreas();
		estacao = estacaoService.salvar(estacao); 
		if (operacao.equals("NOVO")) {
			estacao = null;
			areasSelecionadas = null;
			infoMsg(ResourceBundle.getMessage("estacaoSalvar")+ ".", MSG_TITULO);
		}else{
			infoMsg(ResourceBundle.getMessage("estacaoAlterar")+ ".", MSG_TITULO);
		}
    }

	private void prepararAreas() {
		if (areasSelecionadas.length > 0) {
			List<Area> regs  = new ArrayList<Area>();
			for (int i = 0; i < areasSelecionadas.length; i++) {
				regs.add(areasSelecionadas[i]);
			}
			estacao.setAreas(regs);
		}
	}

	public String voltar() {
		return "consultarEstacao?faces-redirect=true";
	}

	public Estacao getEstacao() {
		if (estacao == null) {
			estacao = (Estacao) getFlashScope().get("estacao");
		}
		return estacao;
	}

	public void setEstacao(Estacao estacao) {
		this.estacao = estacao;
		getFlashScope().put("estacao", estacao);
	}

	public List<Distrito> getDistritos() {
		return distritos;
	}

	public void setDistritos(List<Distrito> distritos) {
		this.distritos = distritos;
	}

	public Boolean getMultiDistrito() {
		return multiDistrito;
	}

	public void setMultiDistrito(Boolean multiDistrito) {
		this.multiDistrito = multiDistrito;
	}

	public List<Area> getAreas() {
		return areas;
	}

	public void setAreas(List<Area> areas) {
		this.areas = areas;
	}

	public Area[] getAreasSelecionadas() {
		return areasSelecionadas;
	}

	public void setAreasSelecionadas(Area[] areasSelecionadas) {
		this.areasSelecionadas = areasSelecionadas;
	}

	public String getOperacao() {
		if (operacao == null) {
			operacao = (String) getFlashScope().get("operacao");
		}
		return operacao;
	}

	public void setOperacao(String operacao) {
		this.operacao = operacao;
		getFlashScope().put("operacao", operacao);
	}
}
