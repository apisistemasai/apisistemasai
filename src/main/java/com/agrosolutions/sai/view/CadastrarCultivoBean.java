package com.agrosolutions.sai.view;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import javax.faces.event.ActionEvent;

import org.apache.log4j.Logger;
import org.primefaces.event.DateSelectEvent;
import org.primefaces.event.ItemSelectEvent;
import org.primefaces.model.chart.CartesianChartModel;
import org.primefaces.model.chart.LineChartSeries;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.Cultivo;
import com.agrosolutions.sai.model.Cultura;
import com.agrosolutions.sai.model.Estadio;
import com.agrosolutions.sai.model.EstadioCultivo;
import com.agrosolutions.sai.model.Irrigante;
import com.agrosolutions.sai.model.Plantio;
import com.agrosolutions.sai.model.Referencia;
import com.agrosolutions.sai.model.Setor;
import com.agrosolutions.sai.model.TipoFertirrigacao;
import com.agrosolutions.sai.model.TipoFormulaGotejo;
import com.agrosolutions.sai.model.TipoManejoIrrigacao;
import com.agrosolutions.sai.model.Variedade;
import com.agrosolutions.sai.service.CultivoService;
import com.agrosolutions.sai.service.CulturaService;
import com.agrosolutions.sai.service.EstadioService;
import com.agrosolutions.sai.service.PlantioService;
import com.agrosolutions.sai.service.ReferenciaService;
import com.agrosolutions.sai.service.SetorService;
import com.agrosolutions.sai.service.VariedadeService;
import com.agrosolutions.sai.util.BigDecimalUtil;
import com.agrosolutions.sai.util.ResourceBundle;

/**
* Use case : Cadastrar Cultivo
* 
* @since   : Jan 18, 2012, 03:45:20 AM
* @author  : raphael.ferreira@ivia.com.br
*/

@Component("cadastrarCultivoBean")
@Scope("session")
public class CadastrarCultivoBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = -5579998320256085613L;

	public static Logger logger = Logger.getLogger(CadastrarCultivoBean.class);

	public static final String MSG_TITULO = "Cadastro de Cultivo";

	@Autowired
	private CultivoService cultivoService;
	@Autowired
	private SetorService setorService;
	@Autowired
	private CulturaService culturaService;
	@Autowired
	private VariedadeService variedadeService;
	@Autowired
	private ReferenciaService referenciaService;
	@Autowired
	private PlantioService plantioService;
	@Autowired
	private EstadioService estadioService;
	
	private Plantio plantioSelecionado;
	private Cultivo cultivo;
	private Irrigante irrigante;
	private List<Cultura> culturas;
	private List<Variedade> variedades;
	private List<Referencia> referencias;
	private List<Setor> setores;
	private String operacao;
	private CartesianChartModel grafico;

	private boolean kc=true;
	private boolean esgotamento=true;
	private boolean enraizamento=true;
	private boolean rendimento=true;
	private boolean altura=true;
	private boolean dc=true;
	private boolean exibirGrafico;

	
	public void carregarDadosIniciais() {
		if (operacao.equals("NOVO") && cultivo == null) {
			cultivo = new Cultivo(irrigante);
			cultivo.setPlantio(new ArrayList<Plantio>());
			Plantio plantio = new Plantio(cultivo);
			plantio.setTipoFormulaGotejo(TipoFormulaGotejo.AreaCoberturaKl);
			cultivo.getPlantio().add(plantio);
		}
		
		if ((operacao.equals("ALTERAR") || operacao.equals("VER")) && cultivo != null) {
			if(variedades == null){
				cultivo.setPlantio(plantioService.pesquisar(cultivo));
				variedades = variedadeService.pesquisar(cultivo.getCultura());
			}
			if(referencias == null){
				cultivo.setPlantio(plantioService.pesquisar(cultivo));
			}
			referencias = referenciaService.pesquisar(cultivo.getVariedade());
			cultivo.getReferencia().setEstadios(estadioService.pesquisar(cultivo.getReferencia()));
		}
		
		culturas = culturaService.obterTodos();
		setores = setorService.pesquisar(irrigante);
		grafico = new CartesianChartModel();
		
		//Grafico
		if (cultivo.getVariedade() != null && cultivo.getDataInicio() != null) {
			carregarGrafico();
			exibirGrafico = true;
        }else{
        	exibirGrafico = false;
        }
	}
	
	public void itemGrafico(ItemSelectEvent event) {  
		infoMsg(grafico.getSeries().get(event.getSeriesIndex()).getLabel() + "=" + grafico.getSeries().get(event.getSeriesIndex()).getData().toString(), null);
    }
	
	private void carregarGrafico() {
		LineChartSeries kc = new LineChartSeries();
		LineChartSeries prof = new LineChartSeries();
		LineChartSeries esg = new LineChartSeries();
		LineChartSeries rend = new LineChartSeries();
		LineChartSeries alt = new LineChartSeries();
		LineChartSeries dc = new LineChartSeries();
		
		kc.setLabel("Kc");
		prof.setLabel("Profundidade de Enraizamento");
		esg.setLabel("Esgotamento");
		rend.setLabel("Fator de Rendimento");
		alt.setLabel("Altura");
		dc.setLabel("Di�metro de Copa");

		Calendar data = new GregorianCalendar();
		Long dias = 0L;
		BigDecimal kcv = new BigDecimal(0);
		BigDecimal akcv = new BigDecimal(0);
		BigDecimal esgv = new BigDecimal(0);
		BigDecimal rendv = new BigDecimal(0);
		BigDecimal altv = new BigDecimal(0);
		BigDecimal prv = new BigDecimal(0);
		BigDecimal aprv = new BigDecimal(0);
		BigDecimal dcv = new BigDecimal(0);
		BigDecimal adcv = new BigDecimal(0);
		int ordemprv = 0;
		int ordemkcv = 0;
		int ordemdcv = 0;
		
		for (Estadio e : cultivo.getReferencia().getEstadios()) {
			if (e.getOrdem().equals(1)) {
				data.setTime(cultivo.getDataInicio());
			}else{
				data.add(Calendar.DAY_OF_MONTH, dias.intValue() );
			}
			dias = e.getQtdDias();  
			if(e.getKc() != null) {
				kcv = e.getKc();
				ordemkcv = e.getOrdem();
				akcv = kcv;
			}
			if(e.getDiametroCopa() != null) {
				dcv = e.getDiametroCopa();
				ordemdcv = e.getOrdem();
				adcv = dcv;
			}
			if(e.getEsgotamento() != null) esgv = e.getEsgotamento();
			if(e.getFatorRendimento() != null) rendv = e.getFatorRendimento();
			if(e.getAlturaFruta() != null) altv = e.getAlturaFruta();
			if(e.getProfundidadeRaiz() != null) {
				prv = e.getProfundidadeRaiz();
				ordemprv = e.getOrdem();
				aprv = prv;
			}
			
			if(e.getKc()==null) akcv = proximoKc(kcv, ordemkcv, akcv);
			kc.set(data.get(Calendar.DAY_OF_MONTH) + "/" + data.getDisplayName(Calendar.MONTH, 0, Locale.getDefault())+ "/" + data.get(Calendar.YEAR), e.getKc()==null?akcv:e.getKc());
			if(e.getProfundidadeRaiz()==null) aprv = proximaRaiz(prv, ordemprv, aprv);
			prof.set(data.get(Calendar.DAY_OF_MONTH) + "/" + data.getDisplayName(Calendar.MONTH, 0, Locale.getDefault())+ "/" + data.get(Calendar.YEAR), e.getProfundidadeRaiz()==null?aprv:e.getProfundidadeRaiz());
			esg.set(data.get(Calendar.DAY_OF_MONTH) + "/" + data.getDisplayName(Calendar.MONTH, 0, Locale.getDefault())+ "/" + data.get(Calendar.YEAR), e.getEsgotamento()==null?esgv:e.getEsgotamento());
			rend.set(data.get(Calendar.DAY_OF_MONTH) + "/" + data.getDisplayName(Calendar.MONTH, 0, Locale.getDefault())+ "/" + data.get(Calendar.YEAR), e.getFatorRendimento()==null?rendv:e.getFatorRendimento());
			alt.set(data.get(Calendar.DAY_OF_MONTH) + "/" + data.getDisplayName(Calendar.MONTH, 0, Locale.getDefault())+ "/" + data.get(Calendar.YEAR), e.getAlturaFruta()==null?altv==null?new BigDecimal(0):altv:e.getAlturaFruta());
			if(e.getDiametroCopa()==null) adcv = proximoDc(dcv, ordemdcv, adcv);
			dc.set(data.get(Calendar.DAY_OF_MONTH) + "/" + data.getDisplayName(Calendar.MONTH, 0, Locale.getDefault())+ "/" + data.get(Calendar.YEAR), e.getDiametroCopa()==null?adcv:e.getDiametroCopa());
		}

		if (this.kc) {
			grafico.addSeries(kc);
		}
		if (this.enraizamento) {
			grafico.addSeries(prof);
		}
		if (this.esgotamento) {
			grafico.addSeries(esg);
		}
		if (this.rendimento) {
			grafico.addSeries(rend);
		}
		if (this.altura) {
			grafico.addSeries(alt);
		}
		if (this.dc) {
			grafico.addSeries(dc);
		}

	}
	
	private BigDecimal proximoDc(BigDecimal valorAnterior, int ordem, BigDecimal valorAcumulado){
		int qtd = 1;
		BigDecimal valorProximo = new BigDecimal(0);
		//Calcular diferen�a do Diametro de Copa
		for (Estadio e : cultivo.getReferencia().getEstadios()) {
			if (e.getOrdem() > ordem) {
				if (e.getDiametroCopa() != null) {
					valorProximo = e.getDiametroCopa();
					break;
				}else{
					qtd++;
				}
			}
		}
		valorAcumulado = calcularValor(valorAnterior, valorAcumulado, qtd,
				valorProximo); 
		return valorAcumulado;
	}
	private BigDecimal calcularValor(BigDecimal valorAnterior,
			BigDecimal valorAcumulado, int qtd, BigDecimal valorProximo) {
		BigDecimal diferenca = valorProximo.subtract(valorAnterior);
		BigDecimal divide = diferenca.divide(new BigDecimal(qtd), BigDecimalUtil.MC_RUP);
		valorAcumulado = valorAcumulado.add(divide);
		return valorAcumulado;
	}
	
	private BigDecimal proximoKc(BigDecimal valorAnterior, int ordem, BigDecimal valorAcumulado){
		int qtd = 1;
		BigDecimal valorProximo = new BigDecimal(0);
		//Calcular diferen�a do KC
		for (Estadio e : cultivo.getReferencia().getEstadios()) {
			if (e.getOrdem() > ordem) {
				if (e.getKc() != null) {
					valorProximo = e.getKc();
					break;
				}else{
					qtd++;
				}
			}
		}
		valorAcumulado = calcularValor(valorAnterior, valorAcumulado, qtd,
				valorProximo); 
		return valorAcumulado;
	}

	private BigDecimal proximaRaiz(BigDecimal valorAnterior, int ordem, BigDecimal valorAcumulado){
		int qtd = 1;
		BigDecimal valorProximo = new BigDecimal(0);
		//Calcular diferença da prof raiz
		for (Estadio e : cultivo.getReferencia().getEstadios()) {
			if (e.getOrdem() > ordem) {
				if (e.getProfundidadeRaiz() != null) {
					valorProximo = e.getProfundidadeRaiz();
					break;
				}else{
					qtd++;
				}
			}
		}
		valorAcumulado = calcularValor(valorAnterior, valorAcumulado, qtd,
				valorProximo); 
		return valorAcumulado;
	}
	
	public void carregarVariedades() {
		if(cultivo.getCultura()==null){
			variedades = null;
		}else{
			variedades = variedadeService.pesquisar(cultivo.getCultura());
		}
		cultivo.setVariedade(null);
	}
	
	public void carregarReferencias() {
		if(cultivo.getVariedade()==null){
			referencias = null;
		}else{
			referencias = referenciaService.pesquisar(cultivo.getVariedade());
		}
		cultivo.setReferencia(null);
	}
	
	public void mudarData(DateSelectEvent event) {
		cultivo.setDataInicio(event.getDate());
	}

	public void gravar(ActionEvent actionEvent) {
		cultivo = cultivoService.salvar(cultivo);
		if (operacao.equals("NOVO")) {
			cultivo = null;
			infoMsg(ResourceBundle.getMessage("cultivoSalvar")+ ".", MSG_TITULO);
		}else{
			infoMsg(ResourceBundle.getMessage("cultivoAlterar")+ ".", MSG_TITULO);
		}
    }

	public void excluir() {
		cultivo.getPlantio().remove(plantioSelecionado);
		if (plantioSelecionado.getId()!= null) {
			plantioService.excluir(plantioSelecionado);
		}
		plantioSelecionado = null;
		infoMsg(ResourceBundle.getMessage("plantioRemovido")+ ".", MSG_TITULO);
    }

	public void adicionarPlantio(ActionEvent actionEvent) {
		//Cria novo plantio
		Plantio plantio = new Plantio(cultivo);
		//Pega plantio anterior
		Plantio pAnterior = cultivo.getPlantio().get(cultivo.getPlantio().size()-1);
		//Descobre o index do setor do plantio anterior
		int index = setores.lastIndexOf(pAnterior.getSetor());
		//Definir o novo setor.
		if (index < setores.size()-1) {
			plantio.setSetor(setores.get(index + 1));
			cultivo.getPlantio().add(plantio);
		} else {
			infoMsg(ResourceBundle.getMessage("setorCadastrado")+ ".", MSG_TITULO);
		}
    }

	public String voltar() {
		return "consultarCultivo?faces-redirect=true";
	}
	
	public Irrigante getIrrigante() {
		return irrigante;
	}

	public void setIrrigante(Irrigante irrigante) {
		this.irrigante = irrigante;
	}

	public EstadioCultivo[] getEstadios() {
		return EstadioCultivo.values();
	}

	public TipoFertirrigacao[] getTiposFertirrigacao() {
		return TipoFertirrigacao.values();
	}

	public TipoManejoIrrigacao[] getTiposManejo() {
		return TipoManejoIrrigacao.values();
	}

	public Cultivo getCultivo() {
		return cultivo;
	}

	public void setCultivo(Cultivo cultivo) {
		this.cultivo = cultivo;
	}

	public List<Cultura> getCulturas() {
		return culturas;
	}

	public void setCulturas(List<Cultura> culturas) {
		this.culturas = culturas;
	}

	public List<Variedade> getVariedades() {
		return variedades;
	}

	public void setVariedades(List<Variedade> variedades) {
		this.variedades = variedades;
	}

	public List<Setor> getSetores() {
		return setores;
	}

	public void setSetores(List<Setor> setores) {
		this.setores = setores;
	}

	public Plantio getPlantioSelecionado() {
		return plantioSelecionado;
	}

	public void setPlantioSelecionado(Plantio plantioSelecionado) {
		this.plantioSelecionado = plantioSelecionado;
	}

	public String getOperacao() {
		return operacao;
	}

	public void setOperacao(String operacao) {
		this.operacao = operacao;
	}

	public CartesianChartModel getGrafico() {
		return grafico;
	}

	public void setGrafico(CartesianChartModel grafico) {
		this.grafico = grafico;
	}

	public void setKc(boolean kc) {
		this.kc = kc;
	}

	public boolean isEsgotamento() {
		return esgotamento;
	}

	public boolean isExibirGrafico() {
		return exibirGrafico;
	}

	public void setExibirGrafico(boolean exibirGrafico) {
		this.exibirGrafico = exibirGrafico;
	}

	public void setEsgotamento(boolean esgotamento) {
		this.esgotamento = esgotamento;
	}

	public boolean isEnraizamento() {
		return enraizamento;
	}

	public void setEnraizamento(boolean enraizamento) {
		this.enraizamento = enraizamento;
	}

	public boolean isRendimento() {
		return rendimento;
	}

	public void setRendimento(boolean rendimento) {
		this.rendimento = rendimento;
	}

	public boolean isAltura() {
		return altura;
	}

	public void setAltura(boolean altura) {
		this.altura = altura;
	}

	public boolean isKc() {
		return kc;
	}
	
	public List<Referencia> getReferencias() {
		return referencias;
	}

	public void setReferencias(List<Referencia> referencias) {
		this.referencias = referencias;
	}
	public boolean isDc() {
		return dc;
	}
	public void setDc(boolean dc) {
		this.dc = dc;
	}
}
