package com.agrosolutions.sai.view;

import java.io.Serializable;
import java.util.Date;

import javax.faces.event.ActionEvent;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.Energia;
import com.agrosolutions.sai.model.Irrigante;
import com.agrosolutions.sai.service.EnergiaService;
import com.agrosolutions.sai.util.ResourceBundle;

/**
* Use case : Cadastrar Energia
* 
* @since   : Maio 24, 2012, 10:23:20 AM
* @author  : walljr@gmail.com
*/

@Component("cadastrarEnergiaBean")
@Scope("session")
public class CadastrarEnergiaBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = -8325889365559618635L;

	public static Logger logger = Logger.getLogger(CadastrarEnergiaBean.class);

	public static final String MSG_TITULO = "Cadastro de Energia";

	@Autowired
	private EnergiaService energiaService;
	
	private Energia energia;
	private Irrigante irrigante;
	private String operacao;
	
	public void carregarDadosIniciais() {
		operacao = getOperacao();
		
		if (energia == null) {
			energia = new Energia(new Date());	
			energia.setIrrigante(irrigante);
		}
	}
	
   public void gravar(ActionEvent actionEvent) {
		energia = energiaService.salvar(energia); 
		energia = null;
		infoMsg(ResourceBundle.getMessage("energiaSalvar")+ ".", null);
	}

	public String voltar() {
		return "consultarEnergia?faces-redirect=true";
	}

	public String getOperacao() {
		if (operacao == null) {
			operacao = (String) getFlashScope().get("operacao");
		}
		return operacao;
	}

	public void setOperacao(String operacao) {
		this.operacao = operacao;
		getFlashScope().put("operacao", operacao);
	}

	public EnergiaService getEnergiaService() {
		return energiaService;
	}

	public void setEnergiaService(EnergiaService energiaService) {
		this.energiaService = energiaService;
	}

	public Energia getEnergia() {
		return energia;
	}

	public void setEnergia(Energia energia) {
		this.energia = energia;
	}

	public Irrigante getIrrigante() {
		return irrigante;
	}

	public void setIrrigante(Irrigante irrigante) {
		this.irrigante = irrigante;
	}

}
