package com.agrosolutions.sai.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.Atividade;
import com.agrosolutions.sai.model.Cultura;
import com.agrosolutions.sai.service.AtividadeService;
import com.agrosolutions.sai.service.CulturaService;
import com.agrosolutions.sai.util.ResourceBundle;

/**
* Use case : Consultar Cultura
* 
* @since   : Jan 17, 2012, 23:23:20 AM
* @author  : rpf1404@gmail.com
*/

@Component("consultarCulturaBean")
@Scope("session")
public class ConsultarCulturaBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = -1051195733952820436L;

	public static Logger logger = Logger.getLogger(ConsultarCulturaBean.class);

	public static final String MSG_TITULO = "Consultar Culturas";

	@Autowired
	private AtividadeService atividadeService;
	
	@Autowired
	private CulturaService culturaService;
	private LazyDataModel<Cultura> culturas;
	private Cultura culturaSelecionado;
	private List<Atividade> atividades;
	private Cultura culturaBusca;

	
	public void carregarDadosIniciais() {
		if(culturaBusca == null){
			culturaBusca = new Cultura();
			culturaBusca.setSortField("nome");
			culturaBusca.setSortOrder(SortOrder.ASCENDING);

			this.setAtividades(atividadeService.obterTodos());
			consultar();
		}
	}
 
	public void consultar() {
		culturas = new LazyDataModel<Cultura>() {
		
		private static final long serialVersionUID = -3761808601893304874L;
	
		@Override
		public List<Cultura> load(int first, int pageSize, String sortField,
				SortOrder sortOrder, Map<String, String> filters) {
			List<Cultura> culturas = new ArrayList<Cultura>();
	
	        if(sortField == null){
				sortField = "nome";
				sortOrder = SortOrder.ASCENDING;
			}
	        
			culturaBusca.setSortField(sortField);
			culturaBusca.setSortOrder(sortOrder);
			culturas = culturaService.pesquisar(culturaBusca, first, pageSize);
	
			return culturas;
		}
	 };
	 int quantidade = culturaService.quantidade(culturaBusca);
	 culturas.setRowCount(quantidade==0?1:quantidade);
	}
	   
	public String cadastro() {
		return "cadastrarCultura?faces-redirect=true";
	}
	
    public void excluir(){
	    culturaService.excluir(culturaSelecionado);
	    culturaBusca  = null;
		culturaSelecionado = null;
		infoMsg(ResourceBundle.getMessage("culturaExcluir")+ ".", null);
	}

	public Cultura getCulturaSelecionado() {
		return culturaSelecionado;
	}
	
	public void setCulturaSelecionado(Cultura culturaSelecionado) {
		this.culturaSelecionado = culturaSelecionado;
	}

	public void setCulturaBusca(Cultura culturaBusca) {
		this.culturaBusca = culturaBusca;
	}

	public Cultura getCulturaBusca() {
		return culturaBusca;
	}

	public void setCulturas(LazyDataModel<Cultura> culturas) {
		this.culturas = culturas;
	}

	public LazyDataModel<Cultura> getCulturas() {
		return culturas;
	}

	public void setAtividades(List<Atividade> atividades) {
		this.atividades = atividades;
	}
	
	public List<Atividade> getAtividades() {
		return atividades;
	}  	
}
