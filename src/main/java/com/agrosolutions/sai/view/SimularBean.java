package com.agrosolutions.sai.view;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.faces.event.ActionEvent;

import org.apache.log4j.Logger;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.Clima;
import com.agrosolutions.sai.model.Emissor;
import com.agrosolutions.sai.model.Irrigacao;
import com.agrosolutions.sai.model.Irrigante;
import com.agrosolutions.sai.model.Plantio;
import com.agrosolutions.sai.model.TipoCalculoKL;
import com.agrosolutions.sai.model.TipoEmissor;
import com.agrosolutions.sai.model.TipoFormulaGotejo;
import com.agrosolutions.sai.model.TipoFormulaMicro;
import com.agrosolutions.sai.service.ClimaService;
import com.agrosolutions.sai.service.IrrigacaoService;
import com.agrosolutions.sai.service.PlantioService;
import com.agrosolutions.sai.util.BigDecimalUtil;

/**
* Use case : Simular Irrigação
* 
* @since   : Aug 2, 2012, 10:45:20 AM
* @author  : rpf1404@gmail.com
*/

@Component("simularBean")
@Scope("session")
public class SimularBean extends ManagerBean implements Serializable {


	private static final long serialVersionUID = -2544745268584569269L;

	public static Logger logger = Logger.getLogger(SimularBean.class);

	@Autowired
	private PlantioService plantioService;
	@Autowired
	private ClimaService climaService;
	@Autowired
	private IrrigacaoService irrigacaoService;
	
	private Plantio plantio;
	private Irrigacao irrigacao;
	private Irrigante irrigante;
	private Emissor[] emissoresSelecionados;
	private Boolean gotejo;
	private Boolean micro;
	private Boolean gotejoKL;
	
	private BigDecimal eto;
	private BigDecimal kc;

	private BigDecimal itn;
	private BigDecimal dw2;
	private BigDecimal am;
	private BigDecimal espacoPlanta;
	private BigDecimal pamParcial;
	private BigDecimal pamParcial2;
	private BigDecimal emissorPlanta;
	private BigDecimal pam;
	private BigDecimal itn2;
	private BigDecimal vazaoTotal;
	private BigDecimal la;
	private BigDecimal lb;
	private BigDecimal tih;
	private BigDecimal ti;
	private BigDecimal chuva;
	private BigDecimal sp;
	private BigDecimal sr;
	private BigDecimal espE;

	private BigDecimal totalEmissores;
	private BigDecimal totalPlanta;
	private BigDecimal eficiencia;
	private BigDecimal vazao;

	public void carregarDadosIniciais() {
		if (gotejo==null) {gotejo = false;}
		if (micro==null) {micro = false;}
		irrigante = plantio.getSetor().getIrrigante();
		Clima clima = climaService.ultimoPorArea(irrigante.getArea());
		irrigacao = irrigacaoService.checarIrrigacao(clima, plantio);
		
		
		if(emissoresSelecionados == null){
			if(plantio.getEmissores() != null && !plantio.getEmissores().isEmpty()){
				int i = 0;
				emissoresSelecionados = new Emissor[plantio.getEmissores().size()];
				for (Emissor e : plantio.getEmissores()) {
					emissoresSelecionados[i++] = e;
					verificarEmissor(e);
				}
			}else{
				for (Emissor e : plantio.getSetor().getEmissores()) {
					verificarEmissor(e);
				}
			}
		}
		if(micro && gotejo){
			micro = false;
			gotejo = false;
		}
		if (gotejo && plantio.getTipoCalculoKL()!=null) {
			gotejoKL = true;
		}

	}
	
	private void verificarEmissor(Emissor e){
		if (e.getTipoEmissor().equals(TipoEmissor.Gotejamento)){
			gotejo = true;
		}
		if (e.getTipoEmissor().equals(TipoEmissor.Microaspersao)){
			micro = true;
		}
	}

	public void gravar(ActionEvent actionEvent) {
		List<Emissor> emissores = new ArrayList<Emissor>();
		for (int i = 0; i < emissoresSelecionados.length; i++) {
			emissores.add(emissoresSelecionados[i]);
		}
		plantio.setEmissores(emissores);
		plantio = plantioService.salvar(plantio);
		infoMsg("Plantio alterado com sucesso.", null);
    }

	public String voltar() {
		gotejo = null;
		micro = null;
		gotejoKL  = null;
		emissoresSelecionados = null;
		return "manterPlantio?faces-redirect=true";
	}
	
	public void emissorSelecionado(SelectEvent event) {
    	verificaEmissor();
    }

    public void emissorDeselecionado(UnselectEvent event) {  
    	verificaEmissor();
    }  
    
	private void verificaEmissor() {
		gotejo = false;
		micro = false;
		gotejoKL = false;
		for (int i = 0; i < emissoresSelecionados.length; i++) {
			verificarEmissor(emissoresSelecionados[i]);
		}
	}  
  
	public void verificarFormulaGotejo() {
		if(plantio.getTipoFormulaGotejo()==null){
			gotejoKL = false;
		}else{
			if(plantio.getTipoFormulaGotejo().equals(TipoFormulaGotejo.AreaCoberturaKl) || plantio.getTipoFormulaGotejo().equals(TipoFormulaGotejo.FaixaKl)){
			gotejoKL = true;
			}
		}
	}  
	
    public Irrigante getIrrigante() {
		return irrigante;
	}

	public void setIrrigante(Irrigante irrigante) {
		this.irrigante = irrigante;
	}

	public TipoFormulaGotejo[] getTiposFormulaGotejo() {
		return TipoFormulaGotejo.values();
	}

	public TipoFormulaMicro[] getTiposFormulaMicro() {
		return TipoFormulaMicro.values();
	}

	public TipoCalculoKL[] getTiposCalculoKL() {
		return TipoCalculoKL.values();
	}

	public Plantio getPlantio() {
		return plantio;
	}

	public void setPlantio(Plantio plantio) {
		this.plantio = plantio;
	}

	public Emissor[] getEmissoresSelecionados() {
		return emissoresSelecionados;
	}

	public void setEmissoresSelecionados(Emissor[] emissoresSelecionados) {
		this.emissoresSelecionados = emissoresSelecionados;
	}

	public Boolean getGotejo() {
		return gotejo;
	}

	public void setGotejo(Boolean gotejo) {
		this.gotejo = gotejo;
	}
	public Boolean getMicro() {
		return micro;
	}

	public void setMicro(Boolean micro) {
		this.micro = micro;
	}

	public Boolean getGotejoKL() {
		return gotejoKL;
	}

	public void setGotejoKL(Boolean gotejoKL) {
		this.gotejoKL = gotejoKL;
	}

	public PlantioService getPlantioService() {
		return plantioService;
	}

	public void setPlantioService(PlantioService plantioService) {
		this.plantioService = plantioService;
	}

	public BigDecimal getItn() {
		itn = irrigacao.getEtc().subtract(irrigacao.getChuva());
		return itn;
	}

	public void setItn(BigDecimal itn) {
		this.itn = itn;
	}

	public BigDecimal getDw2() {
		dw2 = new BigDecimal(plantio.getEspE()).multiply(new BigDecimal(plantio.getEspE()));
		return dw2;
	}

	public void setDw2(BigDecimal dw2) {
		this.dw2 = dw2;
	}

	public BigDecimal getAm() {
		am = new BigDecimal(Math.PI).multiply(dw2);
		return am.setScale(2, BigDecimal.ROUND_UP);
	}

	public void setAm(BigDecimal am) {
		this.am = am;
	}

	public BigDecimal getEspacoPlanta() {
		espacoPlanta = new BigDecimal(plantio.getEspacamento1()).multiply(new BigDecimal(plantio.getEspacamento2()));
		return espacoPlanta;
	}

	public void setEspacoPlanta(BigDecimal espacoPlanta) {
		this.espacoPlanta = espacoPlanta;
	}

	public BigDecimal getPamParcial() {
		pamParcial = am.divide(espacoPlanta.multiply(new BigDecimal(4)), BigDecimalUtil.MC_RUP);
		return pamParcial;
	}

	public void setPamParcial(BigDecimal pamParcial) {
		this.pamParcial = pamParcial;
	}

	public BigDecimal getPamParcial2() {
		pamParcial2 = pamParcial.multiply(BigDecimalUtil._100);
		return pamParcial2;
	}

	public void setPamParcial2(BigDecimal pamParcial2) {
		this.pamParcial2 = pamParcial2;
	}

	public BigDecimal getEmissorPlanta() {
		emissorPlanta = new BigDecimal(plantio.getTotalEmissores()).divide(new BigDecimal(plantio.getTotalPlanta()), BigDecimalUtil.MC_RUP);
		return emissorPlanta;
	}

	public void setEmissorPlanta(BigDecimal emissorPlanta) {
		this.emissorPlanta = emissorPlanta;
	}

	public BigDecimal getPam() {
		pam = getEmissorPlanta().multiply(pamParcial2);
		//Se PAM for menor que 65% considerar 65%.
		if(pam.compareTo(new BigDecimal(65)) < 0){
			pam = new BigDecimal(65);
		}

		return pam;
	}

	public void setPam(BigDecimal pam) {
		this.pam = pam;
	}

	public BigDecimal getItn2() {
		itn2 = itn.divide(irrigacao.getEficiencia(), BigDecimalUtil.MC_RUP);
		return itn2;
	}

	public void setItn2(BigDecimal itn2) {
		this.itn2 = itn2;
	}

	public BigDecimal getVazaoTotal() {
		vazaoTotal = plantio.getFormula().getVazao().multiply(emissorPlanta);
		return vazaoTotal;
	}

	public void setVazaoTotal(BigDecimal vazaoTotal) {
		this.vazaoTotal = vazaoTotal;
	}

	public BigDecimal getLa() {
		la = vazaoTotal.divide(espacoPlanta, BigDecimalUtil.MC_RUP);
		return la;
	}

	public void setLa(BigDecimal la) {
		this.la = la;
	}

	public BigDecimal getLb() {
		lb = itn2.multiply(pam.divide(BigDecimalUtil._100, BigDecimalUtil.MC_RUP));
		return lb;
	}

	public void setLb(BigDecimal lb) {
		this.lb = lb;
	}

	public BigDecimal getTih() {
		tih = lb.divide(la, BigDecimalUtil.MC_RUP);
		return tih;
	}

	public void setTih(BigDecimal tih) {
		this.tih = tih;
	}

	public BigDecimal getTi() {
		ti = tih.multiply(new BigDecimal(60));
		return ti;
	}

	public void setTi(BigDecimal ti) {
		this.ti = ti;
	}

	public Irrigacao getIrrigacao() {
		return irrigacao;
	}

	public void setIrrigacao(Irrigacao irrigacao) {
		this.irrigacao = irrigacao;
	}

	public BigDecimal getEto() {
		if (eto == null) {
			eto = irrigacao.getEto();
		}
		return eto;
	}

	public void setEto(BigDecimal eto) {
		this.eto = eto;
	}

	public BigDecimal getKc() {
		if (kc == null) {
			kc = irrigacao.getKc();
		}
		return kc;
	}

	public void setKc(BigDecimal kc) {
		this.kc = kc;
	}

	public BigDecimal getChuva() {
		if (chuva == null) {
			chuva = irrigacao.getChuva();
		}
		return chuva;
	}

	public void setChuva(BigDecimal chuva) {
		this.chuva = chuva;
	}

	public BigDecimal getSp() {
		if (sp == null) {
			sp = new BigDecimal(plantio.getEspacamento2());
		}
		return sp;
	}

	public void setSp(BigDecimal sp) {
		this.sp = sp;
	}

	public BigDecimal getSr() {
		if (sr == null) {
			sr = new BigDecimal(plantio.getEspacamento1());
		}
		return sr;
	}

	public void setSr(BigDecimal sr) {
		this.sr = sr;
	}

	public BigDecimal getEspE() {
		if (espE == null) {
			espE = new BigDecimal(plantio.getEspE());
		}
		return espE;
	}

	public void setEspE(BigDecimal espE) {
		this.espE = espE;
	}

	public BigDecimal getTotalEmissores() {
		if (totalEmissores == null) {
			totalEmissores = new BigDecimal(plantio.getTotalEmissores());
		}
		return totalEmissores;
	}

	public void setTotalEmissores(BigDecimal totalEmissores) {
		this.totalEmissores = totalEmissores;
	}

	public BigDecimal getTotalPlanta() {
		if (totalPlanta == null) {
			totalPlanta = new BigDecimal(plantio.getTotalPlanta());
		}
		return totalPlanta;
	}

	public void setTotalPlanta(BigDecimal totalPlanta) {
		this.totalPlanta = totalPlanta;
	}

	public BigDecimal getEficiencia() {
		if (eficiencia == null) {
			eficiencia = irrigacao.getEficiencia();
		}
		return eficiencia;
	}

	public void setEficiencia(BigDecimal eficiencia) {
		this.eficiencia = eficiencia;
	}

	public BigDecimal getVazao() {
		if (vazao == null) {
			vazao = plantio.getFormula().getVazao();
		}
		return vazao;
	}

	public void setVazao(BigDecimal vazao) {
		this.vazao = vazao;
	}

}
