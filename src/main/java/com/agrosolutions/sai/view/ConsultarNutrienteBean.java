package com.agrosolutions.sai.view;

import java.io.Serializable;
import java.util.List;

import org.apache.log4j.Logger;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.Elemento;
import com.agrosolutions.sai.model.Nutriente;
import com.agrosolutions.sai.model.TipoNutriente;
import com.agrosolutions.sai.service.ElementoService;
import com.agrosolutions.sai.service.NutrienteService;
import com.agrosolutions.sai.util.ResourceBundle;

/**
* Use case : Consultar Nutriente
* 
* @since   : Dez 06, 2012, 23:23:20 AM
* @author  : raphael@iasoft.me
*/

@Component("consultarNutrienteBean")
@Scope("view")
public class ConsultarNutrienteBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = -4423066456531254076L;

	public static Logger logger = Logger.getLogger(ConsultarNutrienteBean.class);

	@Autowired
	private NutrienteService nutrienteService;
	@Autowired
	private ElementoService elementoService;
	
	private List<Nutriente> nutrientes;
	private Nutriente nutrienteSelecionado;
	private Nutriente nutrienteBusca;
	private List<Elemento> elementos;
	
	public void carregarDadosIniciais() {
		if (nutrienteBusca == null){
			nutrienteBusca = new Nutriente();
			nutrienteBusca.setSortField("nome");
			nutrienteBusca.setSortOrder(SortOrder.ASCENDING);
			elementos = elementoService.obterTodos();
			consultar();	
		}
	}

	public String cadastro() {
		return "cadastrarNutriente?faces-redirect=true";
	}
	
	public void consultar(){	
		nutrientes = nutrienteService.pesquisar(nutrienteBusca, null, null);
	}

	public void excluir(){
	   nutrienteService.excluir(nutrienteSelecionado);
	   infoMsg(ResourceBundle.getMessage("nutrienteExcluir"),null);
	}

	public NutrienteService getNutrienteService() {
		return nutrienteService;
	}

	public void setNutrienteService(NutrienteService nutrienteService) {
		this.nutrienteService = nutrienteService;
	}

	public List<Nutriente> getNutrientes() {
		return nutrientes;
	}

	public void setNutrientes(List<Nutriente> nutrientes) {
		this.nutrientes = nutrientes;
	}

	public Nutriente getNutrienteSelecionado() {
		return nutrienteSelecionado;
	}

	public void setNutrienteSelecionado(Nutriente nutrienteSelecionado) {
		this.nutrienteSelecionado = nutrienteSelecionado;
	}

	public Nutriente getNutrienteBusca() {
		return nutrienteBusca;
	}

	public void setNutrienteBusca(Nutriente nutrienteBusca) {
		this.nutrienteBusca = nutrienteBusca;
	}

	public List<Elemento> getElementos() {
		return elementos;
	}

	public void setElementos(List<Elemento> elementos) {
		this.elementos = elementos;
	}

	public TipoNutriente[] getTipos() {
		return TipoNutriente.values();
	}

}
