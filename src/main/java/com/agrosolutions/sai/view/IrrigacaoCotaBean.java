package com.agrosolutions.sai.view;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.exception.SaiException;
import com.agrosolutions.sai.model.IrrigacaoCota;
import com.agrosolutions.sai.model.Irrigante;
import com.agrosolutions.sai.service.IrrigacaoCotaService;
import com.agrosolutions.sai.service.IrriganteService;

/**
 * @since : Mar 10, 2015, 09:15:32 AM
 * @author : Kleben Ribeiro
 * @email : kleben.ribeiro@ivia.com.br
 */

@Component("irrigacaoCotaBean")
@Scope("view")
public class IrrigacaoCotaBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = -5605350666401348677L;

	public static Logger logger = Logger.getLogger(IrrigacaoCotaBean.class);

	public static final String MSG_TITULO = "Irriga��o por Cota";

	@Autowired
	private IrriganteService irriganteService;
	@Autowired
	private IrrigacaoCotaService irrigacaoCotaService;

	private List<IrrigacaoCota> irrigacaoCota = new ArrayList<IrrigacaoCota>();
//	private LazyDataModel<Irrigante> irrigantes;
	//private List<Irrigante> todosIrrigantes;
	//private Irrigante irriganteBusca;
	private BigDecimal volumeDisponivelTotal;
	private BigDecimal areaIrrigadaTotal;
	private BigDecimal eficienciaDistribuicao;
	private BigDecimal diasIrrigadosSemana;
	private BigDecimal volumeDisponivelPorHa;
	private Boolean recalcular = false;
	
	
	public void carregarDadosIniciais() {
		if (!recalcular) {
			volumeDisponivelTotal = new BigDecimal(50000);
			eficienciaDistribuicao = new BigDecimal(95);
			diasIrrigadosSemana = new BigDecimal(6);
			areaIrrigadaTotal = irrigacaoCotaService.calcularAreaIrrigadaTotal(getDistritosPesquisa());
			recalcular();
		}
	}
	
	public void recalcular() {
		try {
			setRecalcular(true);
			irrigacaoCota = new ArrayList<IrrigacaoCota>();
			calcularVolumeDisponivelPorHa();
			calcularCota();			
		} catch (SaiException e) {
			errorMsg(e.getMessage(), null);
		}
	}
	
	private void calcularVolumeDisponivelPorHa() {
		volumeDisponivelPorHa = irrigacaoCotaService.calcularVolumeDisponivelHa(volumeDisponivelTotal, areaIrrigadaTotal, eficienciaDistribuicao, diasIrrigadosSemana);		
	}
	
	private void calcularCota() {
		IrrigacaoCota cota = new IrrigacaoCota();
		List<Irrigante> irrigantes = new ArrayList<Irrigante>(); 
		irrigantes = irriganteService.pesquisarPorDistrito(getDistritosPesquisa());
		// Popular lista irrigacaoCota
		for (Irrigante irrigante : irrigantes) {
			cota = irrigacaoCotaService.calcularCotaPorIrrigante(irrigante, volumeDisponivelPorHa);
			irrigacaoCota.add(cota);			
		}
	}
	
	public BigDecimal getVolumeDisponivelTotal() {
		return volumeDisponivelTotal;
	}

	public void setVolumeDisponivelTotal(BigDecimal volumeDisponivelTotal) {
		this.volumeDisponivelTotal = volumeDisponivelTotal;
	}

	public BigDecimal getAreaIrrigadaTotal() {
		return areaIrrigadaTotal;
	}

	public void setAreaIrrigadaTotal(BigDecimal areaIrrigadaTotal) {
		this.areaIrrigadaTotal = areaIrrigadaTotal;
	}

	public BigDecimal getEficienciaDistribuicao() {
		return eficienciaDistribuicao;
	}

	public void setEficienciaDistribuicao(BigDecimal eficienciaDistribuicao) {
		this.eficienciaDistribuicao = eficienciaDistribuicao;
	}

	public BigDecimal getDiasIrrigadosSemana() {
		return diasIrrigadosSemana;
	}

	public void setDiasIrrigadosSemana(BigDecimal diasIrrigadosSemana) {
		this.diasIrrigadosSemana = diasIrrigadosSemana;
	}

	public BigDecimal getVolumeDisponivelPorHa() {
		return volumeDisponivelPorHa;
	}

	public void setVolumeDisponivelPorHa(BigDecimal volumeDisponivelPorHa) {
		this.volumeDisponivelPorHa = volumeDisponivelPorHa;
	}
	
	public List<IrrigacaoCota> getIrrigacaoCota() {
		return irrigacaoCota;
	}
	
	public void setIrrigacaoCota(List<IrrigacaoCota> irrigacaoCota) {
		this.irrigacaoCota = irrigacaoCota;
	}

	public Boolean getRecalcular() {
		return recalcular;
	}

	public void setRecalcular(Boolean recalcular) {
		this.recalcular = recalcular;
	}	
}