package com.agrosolutions.sai.view;

import java.io.Serializable;
import java.util.List;

import javax.faces.event.ActionEvent;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.Fabricante;
import com.agrosolutions.sai.model.TipoFabricante;
import com.agrosolutions.sai.model.TipoTubo;
import com.agrosolutions.sai.model.Tubo;
import com.agrosolutions.sai.service.FabricanteService;
import com.agrosolutions.sai.service.TuboService;
import com.agrosolutions.sai.util.ResourceBundle;

/**
* Use case : Cadastrar Tubo
* 
* @since   : Maio 15, 2012, 13:00:20 AM
* @author  : walljr@gmail.com
*/

@Component("cadastrarTuboBean")
@Scope("session")
public class CadastrarTuboBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = 459865772874917683L;

	public static Logger logger = Logger.getLogger(CadastrarTuboBean.class);

	public static final String MSG_TITULO = "Cadastro de Tubo";

	@Autowired
	private TuboService tuboService;
	@Autowired
	private FabricanteService fabricanteService;
	
	private Tubo tubo;
	private List<Fabricante> fabricantes;
	private Fabricante fabricante;
	private String operacao;
	
	public void carregarDadosIniciais() {
		operacao = getOperacao();
		tubo = getTubo();
		fabricante = getFabricante();
		
		if (operacao.equals("NOVO") && tubo == null) {
			tubo = new Tubo();
		}
		if (fabricantes == null) {
			fabricantes = fabricanteService.pesquisar(TipoFabricante.Tubo);
		}
		if (fabricante != null) {
			tubo.setFabricante(fabricante);
			fabricante = null;
		}
	}
	
	public void gravar(ActionEvent actionEvent) {
		tubo = tuboService.salvar(tubo);
		if (operacao.equals("NOVO")) {
			tubo = null;
			infoMsg(ResourceBundle.getMessage("tuboSalvar")+ ".", MSG_TITULO);
		}else{
			infoMsg(ResourceBundle.getMessage("tuboAlterar")+ ".", MSG_TITULO);
		}
    }

	public String novoFabricante() {
		return "cadastrarFabricante?faces-redirect=true";
	}

	public String voltar() {
		return "consultarTubo?faces-redirect=true";
	}

	public Tubo getTubo() {
		return tubo;
	}

	public void setTubo(Tubo tubo) {
		this.tubo = tubo;
	}

	public List<Fabricante> getFabricantes() {
		return fabricantes;
	}

	public void setFabricantes(List<Fabricante> fabricantes) {
		this.fabricantes = fabricantes;
	}

	public Fabricante getFabricante() {
		return fabricante;
	}

	public void setFabricante(Fabricante fabricante) {
		this.fabricante = fabricante;
	}

	public String getOperacao() {
		return operacao;
	}

	public void setOperacao(String operacao) {
		this.operacao = operacao;
	}
	public TipoTubo[] getTipos() {
		return TipoTubo.values();
	}
}
