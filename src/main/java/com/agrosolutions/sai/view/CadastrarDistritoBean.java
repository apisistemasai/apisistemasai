package com.agrosolutions.sai.view;

import java.io.Serializable;

import javax.faces.event.ActionEvent;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.Distrito;
import com.agrosolutions.sai.service.DistritoService;
import com.agrosolutions.sai.util.ResourceBundle;

/**
* Use case : Cadastrar Distrito
* 
* @since   : Jan 06, 2012, 15:00:20 AM
* @author  : raphael.ferreira@ivia.com.br
*/

@Component("cadastrarDistritoBean")
@Scope("session")
public class CadastrarDistritoBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = 4966644135386030844L;

	public static Logger logger = Logger.getLogger(CadastrarDistritoBean.class);

	public static final String MSG_TITULO = "Cadastro de Distrito";

	@Autowired
	private DistritoService distritoService;
	
	private Distrito distrito;
	private String operacao;

	public void carregarDadosIniciais() {
		operacao = getOperacao();
		distrito = getDistrito();
		
		if (operacao.equals("NOVO") && distrito == null) {
			setDistrito(new Distrito());
		}
	}
	
	public void gravar(ActionEvent actionEvent) {
		distrito = distritoService.salvar(distrito); 
		if (operacao.equals("NOVO")) {
		distrito = null;
			infoMsg(ResourceBundle.getMessage("distritoSalvar")+ ".", MSG_TITULO);
		}else{
			infoMsg(ResourceBundle.getMessage("distritoAlterar")+ ".", MSG_TITULO);
		}
	}
	public String voltar() {
		return "consultarDistrito?faces-redirect=true";
	}

	public Distrito getDistrito() {
		if (distrito == null) {
			distrito = (Distrito) getFlashScope().get("distrito");
		}
		return distrito;
	}

	public void setDistrito(Distrito distrito) {
		this.distrito = distrito;
		getFlashScope().put("distrito", distrito);
	}
	
	public String getOperacao() {
		if (operacao == null) {
			operacao = (String) getFlashScope().get("operacao");
		}
		return operacao;
	}

	public void setOperacao(String operacao) {
		this.operacao = operacao;
		getFlashScope().put("operacao", operacao);
	}
}
