package com.agrosolutions.sai.view;

import java.io.Serializable;
import java.util.List;

import javax.faces.event.ActionEvent;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.Categoria;
import com.agrosolutions.sai.model.TipoCategoria;
import com.agrosolutions.sai.service.CategoriaService;
import com.agrosolutions.sai.util.ResourceBundle;

/**
* Use case : Cadastrar Categoria
* 
* @since   : Nov 26, 2012, 22:10:20 AM
* @author  : raphael.ferreira@ivia.com.br
*/

@Component("cadastrarCategoriaBean")
@Scope("session")
public class CadastrarCategoriaBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = -1335171156978434238L;

	public static Logger logger = Logger.getLogger(CadastrarCategoriaBean.class);

	public static final String MSG_TITULO = "Cadastro de Categoria";

	@Autowired
	private CategoriaService categoriaService;
	
	private Categoria categoria;
	private String operacao;
	private List<Categoria> categoriasPai;
	
	public void carregarDadosIniciais() {
		if (operacao.equals("NOVO") && categoria == null) {
			categoria = new Categoria();
		}
		categoriasPai = categoriaService.obterSuperiores();
	}
	
	public void gravar(ActionEvent actionEvent) {
		categoria = categoriaService.salvar(categoria);
		if (operacao.equals("NOVO")) {
			categoria = null;
			infoMsg(ResourceBundle.getMessage("categoriaSalvar")+ ".", MSG_TITULO);
		}else{
			infoMsg(ResourceBundle.getMessage("categoriaAlterar")+ ".", MSG_TITULO);
		}
    }

	public String voltar() {
		return "consultarCategorias?faces-redirect=true";
	}

	public Categoria getCategoria() {
		return categoria;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}

	public String getOperacao() {
		return operacao;
	}

	public void setOperacao(String operacao) {
		this.operacao = operacao;
	}

	public TipoCategoria[] getTipos() {
		return TipoCategoria.values();
	}

	public List<Categoria> getCategoriasPai() {
		return categoriasPai;
	}

	public void setCategoriasPai(List<Categoria> categoriasPai) {
		this.categoriasPai = categoriasPai;
	}
}
