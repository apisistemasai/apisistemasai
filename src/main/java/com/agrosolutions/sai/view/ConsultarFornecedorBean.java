package com.agrosolutions.sai.view;

import java.io.Serializable;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.Fornecedor;
import com.agrosolutions.sai.service.FornecedorService;
import com.agrosolutions.sai.util.ResourceBundle;

/**
 * Use case : Consultar Fornecedor
 * 
 * @since : Nov 26, 2011, 22:23:20 AM
 * @author : mateus.gabriel@ivia.com.br
 */

@Component("consultarFornecedorBean")
@Scope("view")
public class ConsultarFornecedorBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = 3014368717947587732L;

	public static Logger logger = Logger
			.getLogger(ConsultarFornecedorBean.class);

	public static final String MSG_TITULO = "Consultar Fornecedor";

	@Autowired
	private FornecedorService fornecedorService;

	private List<Fornecedor> fornecedores;
	private Fornecedor fornecedorSelecionado;

	public void carregarDadosIniciais() {
		fornecedores = fornecedorService.obterTodos();
	}

	public void excluir() {
		fornecedorService.excluir(fornecedorSelecionado);
		fornecedores = fornecedorService.obterTodos();
		infoMsg(ResourceBundle.getMessage("fornecedorExcluir")+ ".", MSG_TITULO);
	}

	public String cadastro() {
		return "cadastrarFornecedor?faces-redirect=true";
	}

	public List<Fornecedor> getFornecedores() {
		return fornecedores;
	}

	public void setFornecedores(List<Fornecedor> fornecedores) {
		this.fornecedores = fornecedores;
	}
	
	public Fornecedor getFornecedorSelecionado() {
		return fornecedorSelecionado;
	}

	public void setFornecedorSelecionado(Fornecedor fornecedorSelecionado) {
		this.fornecedorSelecionado = fornecedorSelecionado;

	}
}
