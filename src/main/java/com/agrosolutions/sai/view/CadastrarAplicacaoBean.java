package com.agrosolutions.sai.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.FertirrigacaoAplicada;
import com.agrosolutions.sai.model.Irrigante;
import com.agrosolutions.sai.model.Nutriente;
import com.agrosolutions.sai.model.Plantio;
import com.agrosolutions.sai.model.Tanque;
import com.agrosolutions.sai.service.FertirrigacaoAplicadaService;
import com.agrosolutions.sai.service.NutrienteService;
import com.agrosolutions.sai.service.PlantioService;
import com.agrosolutions.sai.service.TanqueService;
import com.agrosolutions.sai.util.ResourceBundle;

/**
* Use case : Cadastrar Aplicacao
* 
* @since   : Dez 9, 2015, 03:23:20 AM
* @author  : raphael@iasoft.me
*/

@Component("cadastrarAplicacaoBean")
@Scope("session")
public class CadastrarAplicacaoBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = 748258391993576616L;

	public static Logger logger = Logger.getLogger(CadastrarAplicacaoBean.class);

	@Autowired
	private FertirrigacaoAplicadaService aplicacaoService;
	@Autowired
	private NutrienteService nutrienteService;
	@Autowired
	private TanqueService tanqueService;
	@Autowired
	private PlantioService plantioService;
	
	private FertirrigacaoAplicada aplicacao;
	private List<Tanque> tanques;
	private NutrienteDataModel nutrientes;
	private Nutriente[] nutrientesSelecionadas;
	private List<Plantio> plantios;
	private Irrigante irrigante;
	private String operacao;
	private String tempoInicial;
	private String tempoFinal;
	
	public void carregarDadosIniciais() {
		operacao = getOperacao();
		aplicacao = getAplicacao();
		setTanques(tanqueService.obterTodos());
		setPlantios(plantioService.pesquisarPorIrrigante(getIrrigante()));
		if (getOperacao().equals("NOVO") && getAplicacao() == null) {
			aplicacao = new FertirrigacaoAplicada();
		}else{
			if(nutrientesSelecionadas == null && aplicacao.getNutrientes() !=null){
				int i = 0;
				nutrientesSelecionadas = new Nutriente[aplicacao.getNutrientes().size()];
				for (Nutriente func : aplicacao.getNutrientes()) {
					nutrientesSelecionadas[i++] = func;
				}
			}
		}
		if (getNutrientes() == null){
			setNutrientes(new NutrienteDataModel(nutrienteService.obterTodos()));
		}
	}
	
	public String gravar() {
		String volta = null;
		prepararNutrientes();
		if (aplicacao.getTanque() == null){
			errorMsg(ResourceBundle.getMessage("escolhaTanque"), null);
			return null;
		}
			
		if (aplicacao.getPlantio() == null){
			errorMsg(ResourceBundle.getMessage("escolhaPlantio"), null);
			return null;
		}
		
		if (tempoInicial != null) {
			String[] tempo = tempoInicial.split(":");
			Calendar tempoI = new GregorianCalendar();
			tempoI.setTime(new Date());
			tempoI.set(Calendar.HOUR, Integer.parseInt(tempo[0]));
			tempoI.set(Calendar.MINUTE, Integer.parseInt(tempo[1]));
			aplicacao.setDataIni(tempoI.getTime());
		} 
		if (tempoFinal != null) {
			String[] tempo = tempoFinal.split(":");
			Calendar tempoI = new GregorianCalendar();
			tempoI.setTime(new Date());
			tempoI.set(Calendar.HOUR, Integer.parseInt(tempo[0]));
			tempoI.set(Calendar.MINUTE, Integer.parseInt(tempo[1]));
			aplicacao.setDataFim(tempoI.getTime());
		} 
		aplicacao = aplicacaoService.salvar(aplicacao); 

		if (operacao.equals("NOVO")) {
			aplicacao = new FertirrigacaoAplicada();
			nutrientesSelecionadas = null;
			infoMsg(ResourceBundle.getMessage("aplicacaoSalvar"), null);
		}else{
			infoMsg(ResourceBundle.getMessage("aplicacaoAlterar"), null);
		}

		return volta;
	}

	private void prepararNutrientes() {
		if (nutrientesSelecionadas.length > 0) {
			List<Nutriente> nuts  = new ArrayList<Nutriente>();
			for (int i = 0; i < nutrientesSelecionadas.length; i++) {
				nuts.add(nutrientesSelecionadas[i]);
			}
			aplicacao.setNutrientes(nuts);
		}
	}
	public String voltar() {
		String volta = "consultarAplicacoes?faces-redirect=true";

		return volta;
	}

	public FertirrigacaoAplicada getAplicacao() {
		if (aplicacao==null){
			aplicacao = (FertirrigacaoAplicada) getFlashScope().get("aplicacao");
		}
		return aplicacao;
	}

	public void setAplicacao(FertirrigacaoAplicada aplicacao) {
		this.aplicacao = aplicacao;
		getFlashScope().put("aplicacao", aplicacao);
	}

	public String getOperacao() {
		if (operacao == null) {
			operacao = (String) getFlashScope().get("operacao");
		}
		return operacao;
	}

	public void setOperacao(String operacao) {
		this.operacao = operacao;
		getFlashScope().put("operacao", operacao);
	}

	public List<Tanque> getTanques() {
		return tanques;
	}

	public void setTanques(List<Tanque> tanques) {
		this.tanques = tanques;
	}


	public List<Plantio> getPlantios() {
		return plantios;
	}

	public void setPlantios(List<Plantio> plantios) {
		this.plantios = plantios;
	}

	public Irrigante getIrrigante() {
		if (irrigante == null) {
			irrigante = (Irrigante) getFlashScope().get("irrigante");
		}
		return irrigante; 
	}

	public void setIrrigante(Irrigante irrigante) {
		this.irrigante = irrigante;
		getFlashScope().put("irrigante", irrigante);
	}

	public Nutriente[] getNutrientesSelecionadas() {
		return nutrientesSelecionadas;
	}

	public void setNutrientesSelecionadas(Nutriente[] nutrientesSelecionadas) {
		this.nutrientesSelecionadas = nutrientesSelecionadas;
	}

	public NutrienteDataModel getNutrientes() {
		return nutrientes;
	}

	public void setNutrientes(NutrienteDataModel nutrientes) {
		this.nutrientes = nutrientes;
	}

	public String getTempoInicial() {
		return tempoInicial;
	}

	public void setTempoInicial(String tempoInicial) {
		this.tempoInicial = tempoInicial;
	}

	public String getTempoFinal() {
		return tempoFinal;
	}

	public void setTempoFinal(String tempoFinal) {
		this.tempoFinal = tempoFinal;
	}


}
