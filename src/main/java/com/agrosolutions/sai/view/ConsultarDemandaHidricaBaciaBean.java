package com.agrosolutions.sai.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.Area;
import com.agrosolutions.sai.model.DemandaHidrica;
import com.agrosolutions.sai.service.AreaService;
import com.agrosolutions.sai.service.DemandaHidricaService;

/**
 * @since : Set 24, 2015, 10:27:55 AM
 * @author : Kleben Ribeiro
 * @email : kleben.ribeiro@ivia.com.br
 */

@Component("consultarDemandaHidricaBean")
@Scope("view")
public class ConsultarDemandaHidricaBaciaBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = 737088869632165736L;
	public static Logger logger = Logger.getLogger(ConsultarDemandaHidricaBaciaBean.class);
	public static final String MSG_TITULO = "Demanda Hidrica da Bacia";
	@Autowired
	private DemandaHidricaService demandaHidricaService;
	@Autowired
	private AreaService areaService;
	private List<DemandaHidrica> demandaHidrica = new ArrayList<DemandaHidrica>();
	
	
	public void carregarDadosIniciais() {
		List<Area> areas;
		areas = areaService.pesquisar(getDistritosPesquisa());
		
		for (Area area : areas) {
			demandaHidricaService.salvar(area);			
		}
	}
	
	public List<DemandaHidrica> getDemandaHidrica() {
		return demandaHidrica;
	}
	
	public void setDemandaHidrica(List<DemandaHidrica> demandaHidrica) {
		this.demandaHidrica = demandaHidrica;
	}
		
}