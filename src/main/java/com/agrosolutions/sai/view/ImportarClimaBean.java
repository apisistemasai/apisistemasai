package com.agrosolutions.sai.view;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.List;

import org.apache.log4j.Logger;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.Area;
import com.agrosolutions.sai.model.Clima;
import com.agrosolutions.sai.model.Estacao;
import com.agrosolutions.sai.service.ClimaService;
import com.agrosolutions.sai.service.DemandaHidricaService;
import com.agrosolutions.sai.service.EstacaoService;

/**
* Use case : Consultar Irrigante
* 
* @since   : Mar 19, 2013, 20:23:20 PM
* @author  : rpf1404@gmail.com
*/

@Component("importarClimaBean")
@Scope("session")
public class ImportarClimaBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = -8165160274391772230L;

	public static Logger logger = Logger.getLogger(ImportarClimaBean.class);

	@Autowired
	private EstacaoService estacaoService;
	@Autowired
	private ClimaService climaService;
	@Autowired
	private DemandaHidricaService demandaHidricaService;

	private List<Estacao> estacoes;
	private Estacao estacao;
	
	public String carregar(){
		this.estacoes = estacaoService.pesquisar(getUsuarioLogado().getDistritos());
		if (!estacoes.isEmpty()) {
			this.estacao = estacoes.get(0);
		}
		return null;
	}
	   
	public void importarClima(FileUploadEvent event) {
		List<Clima> climas = null;
		logger.debug("ImportarClima Iniciando...");
		if (estacoes.isEmpty() || estacao == null) {
			logger.debug("ImportarClima sem esta��o");
	        errorMsg(" Esta��o n�o foi selecionada.", null);
		}else{
			logger.debug("ImportarClima com esta��o, arquivo:" + event.getFile().getFileName());
	    	UploadedFile arq = event.getFile();
			InputStream in;
			try {
				in = new BufferedInputStream(arq.getInputstream());
				if (event.getFile().getFileName().endsWith(".htm")) {
					logger.debug("ImportarClima com esta��o, Inmet");
					climas = climaService.importarArquivoInmet(in, estacao);
				} 
				if (event.getFile().getFileName().endsWith(".dat")) {
					logger.debug("ImportarClima com esta��o, Campbell");
					 climas = climaService.importarArquivoCampbellDibau(in, estacao);
				}
				if (event.getFile().getFileName().endsWith(".CSV") || event.getFile().getFileName().endsWith(".csv")) {
					logger.debug("ImportarClima com esta��o, Funceme");
					climas = climaService.importarArquivoFunceme(in, estacao);
					for (Area a : estacao.getAreas()) {
						for (Clima c: climas){
							Clima cNovo = new Clima(c);
							cNovo.setArea(a);
							climaService.salvarBacia(cNovo);
						}
					}
				} else{
					for (Area a : estacao.getAreas()) {
						for (Clima c: climas){
							Clima cNovo = new Clima(c);
							cNovo.setArea(a);
							climaService.salvar(cNovo);
						}
					}
				}
		        infoMsg(event.getFile().getFileName() + " foi carregado.", null);
			} catch (IOException e) {
				logger.debug("ImportarClima erro arquivo:" + e.getMessage());
		        errorMsg(event.getFile().getFileName() + " n�o foi carregado.", null);
				e.printStackTrace();
			}
		}
    }

	public List<Estacao> getEstacoes() { 
		return estacoes;
	}

	public void setEstacoes(List<Estacao> estacoes) {
		this.estacoes = estacoes;
	}

	public Estacao getEstacao() {
		return estacao;
	}

	public void setEstacao(Estacao estacao) {
		this.estacao = estacao;
	}  
}
