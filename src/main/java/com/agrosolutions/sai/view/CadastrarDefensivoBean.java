package com.agrosolutions.sai.view;

import java.io.Serializable;

import javax.faces.event.ActionEvent;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.Defensivo;
import com.agrosolutions.sai.service.DefensivoService;
import com.agrosolutions.sai.util.ResourceBundle;

/**
* Use case : Cadastrar Defensivo
* 
* @since   : Jan 27, 2012, 05:23:20 AM
* @author  : raphael.ferreira@ivia.com.br
*/

@Component("cadastrarDefensivoBean")
@Scope("session")
public class CadastrarDefensivoBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = 2953730849500946608L;

	public static Logger logger = Logger.getLogger(CadastrarDefensivoBean.class);

	public static final String MSG_TITULO = "Cadastro de Defensivo";

	@Autowired
	private DefensivoService defensivoService;
	
	private Defensivo defensivo;
	private String operacao;
	
	public void carregarDadosIniciais() {
		operacao = getOperacao();
		defensivo = getDefensivo();
		
		if (operacao.equals("NOVO") && defensivo == null) {
			defensivo = new Defensivo();
		}
	}
	
	public void gravar(ActionEvent actionEvent) {
		defensivo = defensivoService.salvar(defensivo); 
		if (operacao.equals("NOVO")) {
		defensivo = null;
			infoMsg(ResourceBundle.getMessage("defensivoSalvar")+ ".", MSG_TITULO);
		}else{
			infoMsg(ResourceBundle.getMessage("defensivoAlterar")+ ".", MSG_TITULO);
		}
	}

	public String voltar() {
		return "consultarDefensivo?faces-redirect=true";
	}

	public Defensivo getDefensivo() {
		if (defensivo == null) {
			defensivo = (Defensivo) getFlashScope().get("defensivo");
		}
		return defensivo;
	}

	public void setDefensivo(Defensivo defensivo) {
		this.defensivo = defensivo;
		getFlashScope().put("defensivo", defensivo);
	}

	public String getOperacao() {
		if (operacao == null) {
			operacao = (String) getFlashScope().get("operacao");
		}
		return operacao;
	}

	public void setOperacao(String operacao) {
		this.operacao = operacao;
		getFlashScope().put("operacao", operacao);
	}
}
