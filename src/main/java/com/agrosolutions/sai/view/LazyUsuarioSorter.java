package com.agrosolutions.sai.view;

import java.util.Comparator;

import org.primefaces.model.SortOrder;

import com.agrosolutions.sai.model.Usuario;

public class LazyUsuarioSorter implements Comparator<Usuario> {

    private String sortField;
    
    private SortOrder sortOrder;
    
    public LazyUsuarioSorter(String sortField, SortOrder sortOrder) {
        this.sortField = sortField;
        this.sortOrder = sortOrder;
    }

    @SuppressWarnings("rawtypes")
	public int compare(Usuario car1, Usuario car2) {
        try {
            Object value1 = Usuario.class.getField(this.sortField).get(car1);
            Object value2 = Usuario.class.getField(this.sortField).get(car2);

            @SuppressWarnings("unchecked")
			int value = ((Comparable)value1).compareTo(value2);
            
            return SortOrder.ASCENDING.equals(sortOrder) ? value : -1 * value;
        }
        catch(Exception e) {
            throw new RuntimeException();
        }
    }
}