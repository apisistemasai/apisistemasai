package com.agrosolutions.sai.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.faces.model.SelectItem;

import org.apache.log4j.Logger;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.Solo;
import com.agrosolutions.sai.model.TipoSolo;
import com.agrosolutions.sai.service.SoloService;
import com.agrosolutions.sai.util.ResourceBundle;

/**
* Use case : Consultar Solo
* 
* @since   : Abr 01, 2012, 22:23:20 AM
* @author  : rpf1404@gmail.com
*/

@Component("consultarSoloBean")
@Scope("view")
public class ConsultarSoloBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = -4510565141331971402L;

	public static Logger logger = Logger.getLogger(ConsultarSoloBean.class);

	public static final String MSG_TITULO = "Consultar Bombas";

	@Autowired
	private SoloService soloService;
	
	private LazyDataModel<Solo> solos;
	private Solo soloSelecionado;
	private SelectItem[] tipos;
	private Solo soloBusca;

	public void carregarDadosIniciais() {
		if (soloBusca == null){
			soloBusca = new Solo();
			soloBusca.setSortField("nome");
			soloBusca.setSortOrder(SortOrder.ASCENDING);
			consultar();
		}
	}
	public void consultar(){
		solos = new LazyDataModel<Solo>() {

			private static final long serialVersionUID = 5153900617136223305L;

			@Override
			public List<Solo> load(int first, int pageSize,
					String sortField, SortOrder sortOrder,
					Map<String, String> filters) {
				List<Solo> solos = new ArrayList<Solo>();

				if (sortField == null) {
					sortField = "nome";
					sortOrder = SortOrder.ASCENDING;
				}

				soloBusca.setSortField(sortField);
				soloBusca.setSortOrder(sortOrder);
				solos = soloService.pesquisar(soloBusca,first, pageSize);

				return solos;
			}
		};
		int quantidade = soloService.quantidade(soloBusca);
		solos.setRowCount(quantidade == 0 ? 1 : quantidade);
	}

	public void excluir(){
	    soloService.excluir(soloSelecionado);
		consultar();
		infoMsg(ResourceBundle.getMessage("soloExcluir")+ ".", MSG_TITULO);
   }

   public String cadastro() {
		return "cadastrarSolo?faces-redirect=true";
   }	
   public SoloService getSoloService() {
		return soloService;
	}
	public void setSoloService(SoloService soloService) {
		this.soloService = soloService;
	}
	public LazyDataModel<Solo> getSolos() {
		return solos;
	}
	public void setSolos(LazyDataModel<Solo> solos) {
		this.solos = solos;
	}
	public Solo getSoloSelecionado() {
		return soloSelecionado;
	}
	public void setSoloSelecionado(Solo soloSelecionado) {
		this.soloSelecionado = soloSelecionado;
	}
	public SelectItem[] getTipos() {
		return tipos;
	}
	public void setTipos(SelectItem[] tipos) {
		this.tipos = tipos;
	}
	public Solo getSoloBusca() {
		return soloBusca;
	}
	public void setSoloBusca(Solo soloBusca) {
		this.soloBusca = soloBusca;
	}
	public TipoSolo[] getTiposSolo() {
		return TipoSolo.values();
	}
}
