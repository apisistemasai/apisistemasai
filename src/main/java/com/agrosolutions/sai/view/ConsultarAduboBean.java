package com.agrosolutions.sai.view;

import java.io.Serializable;
import java.util.List;

import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.Adubo;
import com.agrosolutions.sai.service.AduboService;
import com.agrosolutions.sai.util.ResourceBundle;

/**
* Use case : Consultar Adubo
* 
* @since   : Jan 27, 2012, 23:23:20 AM
* @author  : raphael.ferreira@ivia.com.br
*/

@Component("consultarAduboBean")
@Scope("view")
public class ConsultarAduboBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = -1054159219288063324L;

	public static final String MSG_TITULO = "Consultar Adubos";

	@Autowired
	private AduboService aduboService;
	
	private List<Adubo> adubos;
	private Adubo aduboSelecionado;
	private Adubo aduboBusca;
	
	public void carregarDadosIniciais() {
		if (aduboBusca == null){
			aduboBusca = new Adubo();
			aduboBusca.setSortField("nome");
			aduboBusca.setSortOrder(SortOrder.ASCENDING);
			
			consultar();
		}
	}
	
	public void consultar(){
		adubos = aduboService.pesquisar(aduboBusca, null, null);
		
	}

	public String cadastro() {
		return "cadastrarAdubo?faces-redirect=true";
	}

   public void excluir(){
	   aduboService.excluir(aduboSelecionado);
	   infoMsg(ResourceBundle.getMessage("aduboExcluir")+ ".", MSG_TITULO);
   }

	public List<Adubo> getAdubos() {
		return adubos;
	}
	
	public void setAdubos(List<Adubo> adubos) {
		this.adubos = adubos;
	}
	
	public Adubo getAduboSelecionado() {
		return aduboSelecionado;
	}
	
	public void setAduboSelecionado(Adubo aduboSelecionado) {
		this.aduboSelecionado = aduboSelecionado;
	}

	public void setAduboBusca(Adubo aduboBusca) {
		this.aduboBusca = aduboBusca;
	}

	public Adubo getAduboBusca() {
		return aduboBusca;
	}  	

}
