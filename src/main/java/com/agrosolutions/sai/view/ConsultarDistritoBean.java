package com.agrosolutions.sai.view;

import java.io.Serializable;
import java.util.List;

import org.apache.log4j.Logger;
import org.primefaces.event.map.OverlaySelectEvent;
import org.primefaces.model.SortOrder;
import org.primefaces.model.map.DefaultMapModel;
import org.primefaces.model.map.LatLng;
import org.primefaces.model.map.MapModel;
import org.primefaces.model.map.Marker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.Distrito;
import com.agrosolutions.sai.service.DistritoService;
import com.agrosolutions.sai.util.ResourceBundle;

/**
* Use case : Consultar Distrito
* 
* @since   : Jan 06, 2011, 14:23:20 AM
* @author  : raphael.ferreira@ivia.com.br
*/

@Component("consultarDistritoBean")
@Scope("view")
public class ConsultarDistritoBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = 2454639501837481047L;

	public static Logger logger = Logger.getLogger(ConsultarDistritoBean.class);

	public static final String MSG_TITULO = "Consultar Distritos";

	@Autowired
	private DistritoService distritoService;
	
	private List<Distrito> distritos;
	private Distrito distritoSelecionado;
	private Boolean modoSatelite;
	private MapModel distritosMarcado;  
    private Marker marker;  	
    private String central;
    private Distrito distritoBusca;

    public void carregarDadosIniciais() {
    	if (distritoBusca == null){
    		distritoBusca = new Distrito();
    		distritoBusca.setSortField("nome");
    		distritoBusca.setSortOrder(SortOrder.ASCENDING);
    		distritoBusca.setDistritos(getUsuarioLogado().getDistritos());
        	consultar();
    	}
    	
    	LatLng coord;
		central = "0,0";

		distritosMarcado = new DefaultMapModel();
		for (Distrito d : distritos) {
			if (d.getLatitude() != null && !d.getLatitude().isEmpty()) {
		        //Coordenadas  
		        coord = new LatLng(Double.parseDouble(d.getLatitude()), Double.parseDouble(d.getLongitude()));  
		        //Icones e Dados  
		        distritosMarcado.addOverlay(new Marker(coord, d.getNome(), null, "http://maps.google.com/mapfiles/ms/micons/blue-dot.png"));  
			}
		}
	}
    
    public void consultar(){
    	distritos = distritoService.pesquisar(distritoBusca, null, null);
    }

    public void marcadorSelecionado(OverlaySelectEvent event) {  
        marker = (Marker) event.getOverlay();  
    } 
    public String cadastro() {
		return "cadastrarDistrito?faces-redirect=true";
	}

    public void modoSatelite() {
    	modoSatelite = true;
	}

    public void modoTabela() {
    	modoSatelite = false;
	}

    public void excluir(){
	   distritoService.excluir(distritoSelecionado);
		infoMsg(ResourceBundle.getMessage("distritoExcluir")+ ".","");
   }  	

   public List<Distrito> getDistritos() {
		return distritos;
	}

	public void setDistritos(List<Distrito> distritos) {
		this.distritos = distritos;
	}

	public Distrito getDistritoSelecionado() {
		return distritoSelecionado;
	}

	public void setDistritoSelecionado(Distrito distritoSelecionado) {
		this.distritoSelecionado = distritoSelecionado;
	}

	public Boolean getModoSatelite() {
		return modoSatelite;
	}

	public void setModoSatelite(Boolean modoSatelite) {
		this.modoSatelite = modoSatelite;
	}

	public MapModel getDistritosMarcado() {
		return distritosMarcado;
	}

	public void setDistritosMarcado(MapModel distritosMarcado) {
		this.distritosMarcado = distritosMarcado;
	}

	public Marker getMarker() {
		return marker;
	}

	public void setMarker(Marker marker) {
		this.marker = marker;
	}

	public String getCentral() {
		return central;
	}

	public void setCentral(String central) {
		this.central = central;
	}

	public void setDistritoBusca(Distrito distritoBusca) {
		this.distritoBusca = distritoBusca;
	}

	public Distrito getDistritoBusca() {
		return distritoBusca;
	}
	
}
