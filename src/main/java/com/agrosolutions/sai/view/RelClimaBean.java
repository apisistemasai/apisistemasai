package com.agrosolutions.sai.view;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.primefaces.model.StreamedContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.exception.SaiException;
import com.agrosolutions.sai.service.IrriganteService;
import com.agrosolutions.sai.util.DateUtils;
import com.agrosolutions.sai.util.RelatorioUtil;
import com.agrosolutions.sai.util.ResourceBundle;

/**
* Use case : Relatorio de fornecimento relativo a irriga��o
* 
* @since   : Out 04, 2013, 20:23:20 PM
* @author  : rpf1404@gmail.com
*/

@Component("relClimaBean")
@Scope("session")
public class RelClimaBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = -8320656327562741022L;

	public static Logger logger = Logger.getLogger(RelClimaBean.class);

	@Autowired
	private RelatorioUtil relatorioUtil;
	@Autowired
	private IrriganteService irriganteService;
	private Date dataIni;
	private Date dataFim;
	private StreamedContent file;
	private Integer extensaoArquivo;
	
	   
	public String imprimir() {
		boolean erro = false;
		try {

			// validando campos da entidade
			if ( getDataIni() == null || getDataFim() == null ){
				errorMsg(ResourceBundle.getMessage("erroDataVazia")+ ".", null);
				erro = true;
			}
			
			if (!erro && getDataIni().after(getDataFim())){
				errorMsg(ResourceBundle.getMessage("erroDataInvalida")+ ".", null);
				erro = true;
			}

			if (erro) {
				return null;
			}
			
			// Par�metros enviados para gerar o relat�rio
			Map<String, Object> parametros = new HashMap<String, Object>();
			parametros.put( "dataIni", DateUtils.toString(getDataIni()) );
			parametros.put( "dataFim", DateUtils.toString(getDataFim()) );
			
			// Imprime o relat�rio
			if (getExtensaoArquivo() == 1 || getExtensaoArquivo() == 0){
				file = relatorioUtil.relatorio("rptClima.jasper", parametros, "clima.pdf");
			}else{
				file = relatorioUtil.relatorioXLS("rptClima.jasper", parametros, "clima.xls");
			}
			
		} catch (SaiException e) {
			errorMsg(ResourceBundle.getMessage("ocorreuErro")+ ":" + e.getMessage(), null);
			logger.warn("Ocorreu o seguinte erro: " + e.getMessage() + e.getCause().getMessage());
			return null;
		} catch (Exception e) {
			errorMsg(ResourceBundle.getMessage("ocorreuErro")+ ":" + e.getMessage(), null);
			logger.fatal("Ocorreu o seguinte erro: " + e.getMessage() + e.getCause().getMessage());
			return null;
		}
		infoMsg(ResourceBundle.getMessage("friRelatorio"), null);
		return null;
	}


	public StreamedContent getFile() {
		return file;
	}

	public void setFile(StreamedContent file) {
		this.file = file;
	}
	
	public Date getDataFim() {
		return dataFim;
	}

	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}

	public Date getDataIni() {
		return dataIni;
	}

	public void setDataIni(Date dataIni) {
		this.dataIni = dataIni;
	}

	public Integer getExtensaoArquivo() {
		return extensaoArquivo;
	}

	public void setExtensaoArquivo(Integer extensaoArquivo) {
		this.extensaoArquivo = extensaoArquivo;
	}
	
}
