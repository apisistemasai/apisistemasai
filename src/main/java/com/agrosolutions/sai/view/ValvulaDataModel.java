package com.agrosolutions.sai.view;

import java.util.List;

import javax.faces.model.ListDataModel;

import org.primefaces.model.SelectableDataModel;

import com.agrosolutions.sai.model.Valvula;
  
public class ValvulaDataModel extends ListDataModel<Valvula> implements SelectableDataModel<Valvula> {    
  
    public ValvulaDataModel() {  
    }  
  
    public ValvulaDataModel(List<Valvula> data) {  
        super(data);  
    }  
      
    @Override  
    public Valvula getRowData(String rowKey) {  
          
        @SuppressWarnings("unchecked")
		List<Valvula> datas = (List<Valvula>) getWrappedData();  
          
        for(Valvula valvula : datas) {  
            if(valvula.getId().equals(rowKey))  
                return valvula;  
        }  
          
        return null;  
    }  
  
    @Override  
    public Object getRowKey(Valvula valvula) {  
        return valvula.getId();  
    }  
}  
                     