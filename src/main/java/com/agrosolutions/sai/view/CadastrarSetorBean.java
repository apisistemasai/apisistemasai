package com.agrosolutions.sai.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.event.ActionEvent;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.Bomba;
import com.agrosolutions.sai.model.Emissor;
import com.agrosolutions.sai.model.Fabricante;
import com.agrosolutions.sai.model.Irrigante;
import com.agrosolutions.sai.model.Plantio;
import com.agrosolutions.sai.model.Setor;
import com.agrosolutions.sai.model.Solo;
import com.agrosolutions.sai.model.TipoFabricante;
import com.agrosolutions.sai.model.TipoSolo;
import com.agrosolutions.sai.service.BombaService;
import com.agrosolutions.sai.service.EmissorService;
import com.agrosolutions.sai.service.FabricanteService;
import com.agrosolutions.sai.service.PlantioService;
import com.agrosolutions.sai.service.SetorService;
import com.agrosolutions.sai.service.SoloService;
import com.agrosolutions.sai.util.ResourceBundle;

/**
 * Use case : Cadastrar Setor
 * 
 * @since : Jan 06, 2012, 20:23:20 PM
 * @author : raphael.ferreira@ivia.com.br
 */

@Component("cadastrarSetorBean")
@Scope("session")
public class CadastrarSetorBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = 4577585038415380273L;

	public static Logger logger = Logger.getLogger(CadastrarSetorBean.class);

	public static final String MSG_TITULO = "Cadastro de Setor";

	@Autowired
	private SetorService setorService;
	@Autowired
	private BombaService bombaService;
	@Autowired
	private SoloService soloService;
	@Autowired
	private PlantioService plantioService;

	private Setor setor;
	private Irrigante irrigante;
	private List<Bomba> bombas;
	private List<Solo> solos;
	private List<Setor> setores;
	private Solo solo;
	private Setor setorCopia;

	@Autowired
	private EmissorService emissorService;
	@Autowired
	private FabricanteService fabricanteService;

	private EmissorDataModel emissores;
	private Emissor[] emissoresSelecionados;
	private Emissor emissorSelecionado;
	private String operacao;
	private List<Fabricante> fabricantes;
	private Emissor emissorBusca;
	private List<Plantio> plantios;

	public void carregarDadosIniciais() {
		operacao = getOperacao();
		setor = getSetor();
		irrigante = getIrrigante();

		if (operacao.equals("NOVO") && setor == null) {
			setor = new Setor();
			setor.setIrrigante(irrigante);
			emissoresSelecionados = new Emissor[0];
		}
		if (operacao.equals("ALTERAR") || operacao.equals("VER")) {
			if (emissoresSelecionados == null) {
				int i = 0;
				emissoresSelecionados = new Emissor[setor.getEmissores().size()];
				for (Emissor emissor : setor.getEmissores()) {
					emissoresSelecionados[i++] = emissor;
				}
			}
		}

		bombas = bombaService.pesquisarPorIrrigante(irrigante);
		solos = soloService.obterTodos();
		setores = setorService.pesquisar(irrigante);
		solo = null;
		setorCopia = null;
	}

	public void consultarEmissores() {
		List<Emissor> emissores = emissorService.pesquisar(emissorBusca, null, null);
		for (Emissor e : emissoresSelecionados) {
			if (emissores.contains(e)) {
				emissores.remove(e);
			}
			emissores.add(e);
		}
		this.emissores = new EmissorDataModel(emissores);
	}

	public void incluirEmissor() {
		List<Emissor> emissores = new ArrayList<Emissor>();
		for (Emissor emissor : emissoresSelecionados) {
			emissores.add(emissor);
		}
		setor.setEmissores(emissores);
	}

	public void excluirEmissor() {
		List<Emissor> emissores = new ArrayList<Emissor>();
		for (Emissor e : emissoresSelecionados) {
			if (!e.equals(emissorSelecionado)) {
				emissores.add(e);
			}
		}
		setor.setEmissores(emissores);
		int i = 0;
		emissoresSelecionados = new Emissor[setor.getEmissores().size()];
		for (Emissor emissor : setor.getEmissores()) {
			emissoresSelecionados[i++] = emissor;
		}
	}

	public void carregarSolo() {
		if (solo != null) {
			setor.setCc(solo.getCc());
			setor.setPmp(solo.getPmp());
			setor.setEsgotamento(solo.getEsgotamento());
			setor.setMassaSolo(solo.getMassaSolo());
			setor.setMaxTaxaInfiltracaoChuva(solo.getMaxTaxaInfiltracaoChuva());
			setor.setTipoSolo(solo.getTipoSolo());
			setor.setUmidadeInicial(solo.getUmidadeInicial());
		}
	}

	public void carregarSetor() {
		if (setorCopia != null) {
			setor.setCc(setorCopia.getCc());
			setor.setPmp(setorCopia.getPmp());
			setor.setEsgotamento(setorCopia.getEsgotamento());
			setor.setMassaSolo(setorCopia.getMassaSolo());
			setor.setMaxTaxaInfiltracaoChuva(setorCopia
					.getMaxTaxaInfiltracaoChuva());
			setor.setTipoSolo(setorCopia.getTipoSolo());
			setor.setFatorHidrico(setorCopia.getFatorHidrico());
			setor.setUmidadeInicial(setorCopia.getUmidadeInicial());
			List<Emissor> emissoresNovo = new ArrayList<Emissor>();
			for (Emissor emissor : setorCopia.getEmissores()) {
				emissoresNovo.add(emissor);
			}

			setor.setEmissores(emissoresNovo);
			setor.setAreaTotal(setorCopia.getAreaTotal());
			setor.setObservacao(setorCopia.getObservacao());
		}
	}

	public String gravar() {
		setor = setorService.salvar(setor);
		atualizaEmissoresPlantios(setor);
		solo = null;
		setorCopia = null;
		if (operacao.equals("NOVO")) {
			setor = null;
			emissoresSelecionados = null;
			infoMsg(ResourceBundle.getMessage("setorSalvar")+ ".", MSG_TITULO);
		}else{
			infoMsg(ResourceBundle.getMessage("setorAlterar")+ ".", MSG_TITULO);
		}
		return null;
	}

	public void prepararEmissor(ActionEvent actionEvent) {
		emissores = new EmissorDataModel(emissorService.obterTodos());
		fabricantes = fabricanteService.pesquisar(TipoFabricante.Emissor);
		emissorBusca = new Emissor();
	}

	public String voltar() {
		emissoresSelecionados = null;
		emissores = null;
		solos = null;
		setor = null;
		operacao = null;
		bombas = null;
		return "consultarSetor?faces-redirect=true";
	}
	
	public void atualizaEmissoresPlantios(Setor setor) {
		plantios = plantioService.pesquisarPorSetor(setor); 
		// Apaga emissores e f�rmulas
		for (Plantio plantio : plantios) {
			plantio.setTipoCalculoKL(null);
			plantio.setTipoFormulaGotejo(null);
			plantio.setTipoFormulaMicro(null);
			plantio.setEmissores(null);
			plantio.setFormula(null);
			plantio.setFormula(plantioService.verificarFormula(plantio));
			plantio = plantioService.definirTipoFormula( plantio );
			plantioService.salvar(plantio);
		}
	}

	public TipoSolo[] getTiposSolo() {
		return TipoSolo.values();
	}

	public Setor getSetor() {
		if (setor == null) {
			setor = (Setor) getFlashScope().get("setor");
		}
		return setor;
	}

	public void setSetor(Setor setor) {
		this.setor = setor;
		getFlashScope().put("setor", setor);
	}

	public Irrigante getIrrigante() {
		if (irrigante == null) {
			irrigante = (Irrigante) getFlashScope().get("irrigante");
		}
		return irrigante;
	}

	public void setIrrigante(Irrigante i) {
		this.irrigante = i;
		getFlashScope().put("irrigante", irrigante);

	}

	public List<Bomba> getBombas() {
		return bombas;
	}

	public void setBombas(List<Bomba> bombas) {
		this.bombas = bombas;
	}

	public EmissorDataModel getEmissores() {
		return emissores;
	}

	public void setEmissores(EmissorDataModel emissores) {
		this.emissores = emissores;
	}

	public Emissor[] getEmissoresSelecionados() {
		return emissoresSelecionados;
	}

	public void setEmissoresSelecionados(Emissor[] emissoresSelecionados) {
		this.emissoresSelecionados = emissoresSelecionados;
	}

	public String getOperacao() {
		if (operacao == null) {
			operacao = (String) getFlashScope().get("operacao");
		}
		return operacao;
	}

	public void setOperacao(String operacao) {
		this.operacao = operacao;
		getFlashScope().put("operacao", operacao);
	}

	public Emissor getEmissorSelecionado() {
		return emissorSelecionado;
	}

	public void setEmissorSelecionado(Emissor emissorSelecionado) {
		this.emissorSelecionado = emissorSelecionado;
	}

	public List<Solo> getSolos() {
		return solos;
	}

	public void setSolos(List<Solo> solos) {
		this.solos = solos;
	}

	public Solo getSolo() {
		return solo;
	}

	public void setSolo(Solo solo) {
		this.solo = solo;
	}

	public List<Setor> getSetores() {
		return setores;
	}

	public void setSetores(List<Setor> setores) {
		this.setores = setores;
	}

	public Setor getSetorCopia() {
		return setorCopia;
	}

	public void setSetorCopia(Setor setorCopia) {
		this.setorCopia = setorCopia;
	}

	public void setFabricantes(List<Fabricante> fabricantes) {
		this.fabricantes = fabricantes;
	}

	public List<Fabricante> getFabricantes() {
		return fabricantes;
	}

	public Emissor getEmissorBusca() {
		return emissorBusca;
	}

	public void setEmissorBusca(Emissor emissorBusca) {
		this.emissorBusca = emissorBusca;
	}

	public List<Plantio> getPlantios() {
		return plantios;
	}

	public void setPlantios(List<Plantio> plantios) {
		this.plantios = plantios;
	}
}
