package com.agrosolutions.sai.model;

import com.agrosolutions.sai.util.ResourceBundle;

public enum TipoTubo {

	Aco(0, "tuboAco"), 
	Aluminio(1, "aluminio"),
	PVC(2, "pvc"),	
	Polietileno(3, "polietileno"),
	Ferro(4, "ferro");

	private Integer id;
	
	private String descricao;

	private TipoTubo(Integer id, String descricao){
		this.id = id;
		this.descricao = descricao;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the descricao
	 */
	public String getDescricao() {
		return ResourceBundle.getMessage(descricao);
	}

	/**
	 * @param descricao the descricao to set
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}	
}
