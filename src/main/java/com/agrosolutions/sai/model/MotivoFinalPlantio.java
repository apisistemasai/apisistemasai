package com.agrosolutions.sai.model;

import com.agrosolutions.sai.util.ResourceBundle;

public enum MotivoFinalPlantio {

	Colheita(0, "colheita"),
	Doenca(1, "doenca"),
	Praga(2, "praga"),
	Mudanca(3, "mudancaCultura"),
	Catastrofe(4, "catastrofe"),
	Estiagem(5, "estiagem"),
	Outros(6, "outros");
	
	private Integer id;
	
	private String descricao;

	private MotivoFinalPlantio(Integer id, String descricao){
		this.id = id;
		this.descricao = descricao;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the descricao
	 */
	public String getDescricao() {
		return ResourceBundle.getMessage(descricao);
	}

	/**
	 * @param descricao the descricao to set
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}	
}
