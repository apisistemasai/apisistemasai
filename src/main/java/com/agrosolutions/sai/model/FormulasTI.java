package com.agrosolutions.sai.model;

import com.agrosolutions.sai.util.ResourceBundle;

public enum FormulasTI {

	GotejoAreaPam(0, "formulaGotejoAreaCoberturaPAM"),
	GotejoAreaKl(1, "formulaGotejoAreaCoberturaKL"),
	GotejoFaixaPam(2, "formulaGotejoFaixaPAM"),
	GotejoFaixaKl(3, "formulaGotejoFaixaKL"),
	MicroHasteNormalPAM(4, "formulaMicroHasteNPAM"),
	MicroHasteAlongada(5, "formulaMicroHasteA"),
	Aspercao(6, "formulaAspercao"),
	MicroHasteNormalKL(7, "formulaMicroHasteNKL"),
	BalancoHidricoMicro(8, "balancoHidricoMicro");
	
	private Integer id;
	
	private String descricao;

	private FormulasTI(Integer id, String descricao){
		this.id = id;
		this.descricao = descricao;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the descricao
	 */
	public String getDescricao() {
		return ResourceBundle.getMessage(descricao);
	}

	/**
	 * @param descricao the descricao to set
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}	
}
