package com.agrosolutions.sai.model;

import com.agrosolutions.sai.interfaces.SerializableInterface;
import java.math.BigDecimal;

public class IrrigacaoCota extends Model implements SerializableInterface {

	private static final long serialVersionUID = -5277710902904610013L;
	
	private Irrigante irrigante;
	private BigDecimal vazaoMedia;
	private BigDecimal volumeDisponivel;
	private BigDecimal tempoIrrigacaoHoras;
	private BigDecimal tempoIrrigacaoMinutos;
	
	
	public IrrigacaoCota(Irrigante irrigante, BigDecimal vazaoMedia, BigDecimal volumeDisponivel, BigDecimal tempoIrrigacaoHoras, BigDecimal tempoIrrigacaoMinutos) {
		this.irrigante = irrigante;
		this.vazaoMedia = vazaoMedia;
		this.volumeDisponivel = volumeDisponivel;
		this.tempoIrrigacaoHoras = tempoIrrigacaoHoras;
		this.tempoIrrigacaoMinutos = tempoIrrigacaoMinutos;
	}
	
	public IrrigacaoCota() {
		super();
	}

	public Irrigante getIrrigante() {
		return irrigante;
	}
	public void setIrrigante(Irrigante irrigante) {
		this.irrigante = irrigante;
	}
	public BigDecimal getVazaoMedia() {
		return vazaoMedia;
	}
	public void setVazaoMedia(BigDecimal vazaoMedia) {
		this.vazaoMedia = vazaoMedia;
	}
	public BigDecimal getVolumeDisponivel() {
		return volumeDisponivel;
	}
	public void setVolumeDisponivel(BigDecimal volumeDisponivel) {
		this.volumeDisponivel = volumeDisponivel;
	}
	public BigDecimal getTempoIrrigacaoHoras() {
		return tempoIrrigacaoHoras;
	}
	public void setTempoIrrigacaoHoras(BigDecimal tempoIrrigacaoHoras) {
		this.tempoIrrigacaoHoras = tempoIrrigacaoHoras;
	}
	public BigDecimal getTempoIrrigacaoMinutos() {
		return tempoIrrigacaoMinutos;
	}
	public void setTempoIrrigacaoMinutos(BigDecimal tempoIrrigacaoMinutos) {
		this.tempoIrrigacaoMinutos = tempoIrrigacaoMinutos;
	}
	
}
