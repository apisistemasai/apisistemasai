package com.agrosolutions.sai.model;

import com.agrosolutions.sai.util.ResourceBundle;

public enum TipoNutriente {

	Tipo1(0, "tipo1"),
	Tipo2(1, "tipo2"),
	Tipo3(2, "tipo3"),
	Tipo4(3, "tipo4"),
	Tipo5(4, "tipo5");
	
	private Integer id;
	
	private String descricao;

	private TipoNutriente(Integer id, String descricao){
		this.id = id;
		this.descricao = descricao;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the descricao
	 */
	public String getDescricao() {
		return ResourceBundle.getMessage(descricao);
	}

	/**
	 * @param descricao the descricao to set
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}	
}
