package com.agrosolutions.sai.model;

import com.agrosolutions.sai.interfaces.SerializableInterface;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Acesso extends Model implements SerializableInterface {

	private static final long serialVersionUID = 430867894003666975L;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date data;
	@ManyToOne
	private Usuario usuario;
	
	public Acesso() {
		super();
	}
	public Acesso(Date data, Usuario usuario) {
		super();
		this.data = data;
		this.usuario = usuario;
}
	public Date getData() {
		return data;
	}
	public void setData(Date data) {
		this.data = data;
	}
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

}
