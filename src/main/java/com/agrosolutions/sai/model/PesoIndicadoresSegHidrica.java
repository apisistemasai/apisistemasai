package com.agrosolutions.sai.model;

import java.math.BigDecimal;


public enum PesoIndicadoresSegHidrica {
	
	ALTA(new BigDecimal(0.5), "Alta" ),
	MEDIA(new BigDecimal(0.75), "M�dia" ),
	BAIXA(new BigDecimal(1), "Baixa" );
	
	private BigDecimal valor;
	private String descricao;

	private PesoIndicadoresSegHidrica(BigDecimal valor, String descricao){
		this.setValor(valor);
		this.setDescricao(descricao);
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
}
