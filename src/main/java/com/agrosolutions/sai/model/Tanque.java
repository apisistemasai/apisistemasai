package com.agrosolutions.sai.model;

import com.agrosolutions.sai.interfaces.SerializableInterface;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;
import org.primefaces.model.SortOrder;


@Entity
@Audited
public class Tanque extends Model implements SerializableInterface {

	private static final long serialVersionUID = 4197241584810489225L;

	@Column(unique=true)
	private String nome;
	@ManyToOne
	private Tanque tanqueDestino;
	@ManyToOne
	private Bomba bomba;
	@Column
	private BigDecimal vazao;
	@Column
	private BigDecimal volume;
	
	@Transient
	private String sortField;
	@Transient
	private SortOrder sortOrder;


	public void setSortField(String sortField) {
		this.sortField = sortField;
	}
	public String getSortField() {
		return sortField;
	}
	public void setSortOrder(SortOrder sortOrder) {
		this.sortOrder = sortOrder;
	}
	public SortOrder getSortOrder() {
		return sortOrder;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Tanque getTanqueDestino() {
		return tanqueDestino;
	}
	public void setTanqueDestino(Tanque tanqueDestino) {
		this.tanqueDestino = tanqueDestino;
	}
	public BigDecimal getVolume() {
		return volume;
	}
	public void setVolume(BigDecimal volume) {
		this.volume = volume;
	}
	public BigDecimal getVazao() {
		return vazao;
	}
	public void setVazao(BigDecimal vazao) {
		this.vazao = vazao;
	}
	public Bomba getBomba() {
		return bomba;
	}
	public void setBomba(Bomba bomba) {
		this.bomba = bomba;
	}
	
}
