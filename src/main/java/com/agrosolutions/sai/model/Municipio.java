package com.agrosolutions.sai.model;

import com.agrosolutions.sai.interfaces.SerializableInterface;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.envers.Audited;
import org.primefaces.model.SortOrder;
import org.primefaces.model.chart.CartesianChartModel;
import org.primefaces.model.chart.PieChartModel;

@Entity
@Audited
public class Municipio extends Model implements SerializableInterface {

	private static final long serialVersionUID = 5305231776998091309L;

	private String nome;
	private String foto;
	private String localizacao;
	private String latitude;
	private String longitude;
	private BigDecimal areaTotal;
	private BigDecimal areaIrrigada;
	private String codigo;
	@Column(length = 800)
	private String observacao;
	@Temporal(TemporalType.DATE)
	private Date dataCadastro;
	private Integer classificacao;
	@ManyToOne
	private BaciaHidrografica bacia;

	@OneToMany(fetch=FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "municipio")
	@Fetch(FetchMode.SUBSELECT)
	@BatchSize(size = 30)
	private List<Coordenada> coordenadasPoligono;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "municipio")
	@Fetch(FetchMode.SUBSELECT)
	@BatchSize(size = 30)
	private List<AtividadeEconomica> atividades;
	
	@Transient
	private Cultura cultura;
	@Transient
	private Variedade variedade;
	@Transient
	private String sortField;
	@Transient
	private SortOrder sortOrder;
	@Transient
	private Integer alturaGrafico;
	@Transient
	private PieChartModel graficoIndicadorAreaIrrigada;
	@Transient
	private CartesianChartModel graficoIndicadoresSegurancaKgHa;
	@Transient
	private CartesianChartModel graficoIndicadoresSegurancaKgM3;
	@Transient
	private CartesianChartModel graficoIndicadoresEconomicoReceitaLiquida;
	@Transient
	private CartesianChartModel graficoIndicadoresVBP;
	@Transient
	private CartesianChartModel graficoIndicadoresEconomicoReceitaLiquidaHa;
	@Transient
	private CartesianChartModel graficoIndicadoresEconomicoReceitaLiquidaM3;
	@Transient
	private CartesianChartModel graficoIndicadoresSegSocialEmpregosHa;
	@Transient
	private CartesianChartModel graficoIndicadoresSegSocialEmpregosM3;
	@Transient
	private CartesianChartModel graficoIndicadoresSegHidricaM3Ha;
	@Transient
	private CartesianChartModel graficoIndicadoresSegHidricaLitrosSegundoHa;
	@Transient
	private CartesianChartModel graficoIndicadoresCorteHidrico;
	@Transient
	private PieChartModel graficoIndicadorCicloCultura; 
	
	public Municipio() {
		super();
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getLocalizacao() {
		return localizacao;
	}

	public void setLocalizacao(String localizacao) {
		this.localizacao = localizacao;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public BigDecimal getAreaTotal() {
		return areaTotal;
	}

	public void setAreaTotal(BigDecimal areaTotal) {
		this.areaTotal = areaTotal;
	}

	public BigDecimal getAreaIrrigada() {
		return areaIrrigada;
	}

	public void setAreaIrrigada(BigDecimal areaIrrigada) {
		this.areaIrrigada = areaIrrigada;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public String getSortField() {
		return sortField;
	}

	public void setSortField(String sortField) {
		this.sortField = sortField;
	}

	public SortOrder getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(SortOrder sortOrder) {
		this.sortOrder = sortOrder;
	}

	public List<AtividadeEconomica> getAtividades() {
		return atividades;
	}

	public void setAtividades(List<AtividadeEconomica> atividades) {
		this.atividades = atividades;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getCodigo() {
		return codigo;
	}

	public PieChartModel getGraficoIndicadorAreaIrrigada() {
		return graficoIndicadorAreaIrrigada;
	}

	public void setGraficoIndicadorAreaIrrigada(
			PieChartModel graficoIndicadorAreaIrrigada) {
		this.graficoIndicadorAreaIrrigada = graficoIndicadorAreaIrrigada;
	}

	public CartesianChartModel getGraficoIndicadoresSegurancaKgHa() {
		return graficoIndicadoresSegurancaKgHa;
	}

	public void setGraficoIndicadoresSegurancaKgHa(
			CartesianChartModel graficoIndicadoresSegurancaKgHa) {
		this.graficoIndicadoresSegurancaKgHa = graficoIndicadoresSegurancaKgHa;
	}

	public CartesianChartModel getGraficoIndicadoresSegurancaKgM3() {
		return graficoIndicadoresSegurancaKgM3;
	}

	public void setGraficoIndicadoresSegurancaKgM3(
			CartesianChartModel graficoIndicadoresSegurancaKgM3) {
		this.graficoIndicadoresSegurancaKgM3 = graficoIndicadoresSegurancaKgM3;
	}

	public CartesianChartModel getGraficoIndicadoresEconomicoReceitaLiquidaHa() {
		return graficoIndicadoresEconomicoReceitaLiquidaHa;
	}

	public void setGraficoIndicadoresEconomicoReceitaLiquidaHa(
			CartesianChartModel graficoIndicadoresEconomicoReceitaLiquidaHa) {
		this.graficoIndicadoresEconomicoReceitaLiquidaHa = graficoIndicadoresEconomicoReceitaLiquidaHa;
	}

	public CartesianChartModel getGraficoIndicadoresEconomicoReceitaLiquidaM3() {
		return graficoIndicadoresEconomicoReceitaLiquidaM3;
	}

	public void setGraficoIndicadoresEconomicoReceitaLiquidaM3(
			CartesianChartModel graficoIndicadoresEconomicoReceitaLiquidaM3) {
		this.graficoIndicadoresEconomicoReceitaLiquidaM3 = graficoIndicadoresEconomicoReceitaLiquidaM3;
	}

	public CartesianChartModel getGraficoIndicadoresSegSocialEmpregosHa() {
		return graficoIndicadoresSegSocialEmpregosHa;
	}

	public void setGraficoIndicadoresSegSocialEmpregosHa(
			CartesianChartModel graficoIndicadoresSegSocialEmpregosHa) {
		this.graficoIndicadoresSegSocialEmpregosHa = graficoIndicadoresSegSocialEmpregosHa;
	}

	public CartesianChartModel getGraficoIndicadoresSegSocialEmpregosM3() {
		return graficoIndicadoresSegSocialEmpregosM3;
	}

	public void setGraficoIndicadoresSegSocialEmpregosM3(
			CartesianChartModel graficoIndicadoresSegSocialEmpregosM3) {
		this.graficoIndicadoresSegSocialEmpregosM3 = graficoIndicadoresSegSocialEmpregosM3;
	}

	public CartesianChartModel getGraficoIndicadoresSegHidricaM3Ha() {
		return graficoIndicadoresSegHidricaM3Ha;
	}

	public void setGraficoIndicadoresSegHidricaM3Ha(
			CartesianChartModel graficoIndicadoresSegHidricaM3Ha) {
		this.graficoIndicadoresSegHidricaM3Ha = graficoIndicadoresSegHidricaM3Ha;
	}

	public CartesianChartModel getGraficoIndicadoresSegHidricaLitrosSegundoHa() {
		return graficoIndicadoresSegHidricaLitrosSegundoHa;
	}

	public void setGraficoIndicadoresSegHidricaLitrosSegundoHa(
			CartesianChartModel graficoIndicadoresSegHidricaLitrosSegundoHa) {
		this.graficoIndicadoresSegHidricaLitrosSegundoHa = graficoIndicadoresSegHidricaLitrosSegundoHa;
	}

	public CartesianChartModel getGraficoIndicadoresCorteHidrico() {
		return graficoIndicadoresCorteHidrico;
	}

	public void setGraficoIndicadoresCorteHidrico(CartesianChartModel graficoIndicadoresCorteHidrico) {
		this.graficoIndicadoresCorteHidrico = graficoIndicadoresCorteHidrico;
	}

	public Integer getAlturaGrafico() {
		return alturaGrafico;
	}

	public void setAlturaGrafico(Integer alturaGrafico) {
		this.alturaGrafico = alturaGrafico;
	}

	public CartesianChartModel getGraficoIndicadoresVBP() {
		return graficoIndicadoresVBP;
	}

	public void setGraficoIndicadoresVBP(CartesianChartModel graficoIndicadoresVBP) {
		this.graficoIndicadoresVBP = graficoIndicadoresVBP;
	}

	public CartesianChartModel getGraficoIndicadoresEconomicoReceitaLiquida() {
		return graficoIndicadoresEconomicoReceitaLiquida;
	}

	public void setGraficoIndicadoresEconomicoReceitaLiquida(
			CartesianChartModel graficoIndicadoresEconomicoReceitaLiquida) {
		this.graficoIndicadoresEconomicoReceitaLiquida = graficoIndicadoresEconomicoReceitaLiquida;
	}

	public PieChartModel getGraficoIndicadorCicloCultura() {
		return graficoIndicadorCicloCultura;
	}

	public void setGraficoIndicadorCicloCultura(
			PieChartModel graficoIndicadorCicloCultura) {
		this.graficoIndicadorCicloCultura = graficoIndicadorCicloCultura;
	}

	public BaciaHidrografica getBacia() {
		return bacia;
	}

	public void setBacia(BaciaHidrografica bacia) {
		this.bacia = bacia;
	}

	public List<Coordenada> getCoordenadasPoligono() {
		return coordenadasPoligono;
	}

	public void setCoordenadasPoligono(List<Coordenada> coordenadasPoligono) {
		this.coordenadasPoligono = coordenadasPoligono;
	}

	public String getFoto() {
		return foto;
	}

	public void setFoto(String foto) {
		this.foto = foto;
	}

	public Cultura getCultura() {
		return cultura;
	}

	public void setCultura(Cultura cultura) {
		this.cultura = cultura;
	}

	public Variedade getVariedade() {
		return variedade;
	}

	public void setVariedade(Variedade variedade) {
		this.variedade = variedade;
	}

	public Integer getClassificacao() {
		return classificacao;
	}

	public void setClassificacao(Integer classificacao) {
		this.classificacao = classificacao;
	}
}