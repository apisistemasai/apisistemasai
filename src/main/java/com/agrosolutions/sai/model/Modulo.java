package com.agrosolutions.sai.model;

import com.agrosolutions.sai.interfaces.SerializableInterface;
import javax.persistence.Column;
import javax.persistence.Entity;


/**
 * Classe que representa um modulo do sistema. 
 * 
 * @author Raphael Ferreira 
 *
 */
@Entity
public class Modulo extends Model implements SerializableInterface {
	
	private static final long serialVersionUID = 9059327627186317940L;

	@Column(unique = true)
	private String siglas;
	private String descricao;
	private String url;
	private String imagem;

	public String getSiglas() {
		return siglas;
	}
	public void setSiglas(String siglas) {
		this.siglas = siglas;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getImagem() {
		return imagem;
	}
	public void setImagem(String imagem) {
		this.imagem = imagem;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}

}
