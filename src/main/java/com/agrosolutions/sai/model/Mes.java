package com.agrosolutions.sai.model;

import com.agrosolutions.sai.util.ResourceBundle;

public enum Mes {

	JANEIRO(1, "janeiro"), 
	FEVEREIRO(2, "fevereiro"), 
	MARCO(3, "marco"), 
	ABRIL(4, "abril"), 
	MAIO(5, "maio"), 
	JUNHO(6, "junho"), 
	JULHO(7, "julho"), 
	AGOSTO(8, "agosto"), 
	SETEMBRO(9, "setembro"), 
	OUTUBRO(10, "outubro"), 
	NOVEMBRO(11, "novembro"), 
	DEZEMBRO(12, "dezembro");
	
	private Integer id;
	
	private String descricao;
	
	protected String nomeMes = null;
	
	private Mes(Integer id, String descricao) {
		this.id = id;
		this.descricao = descricao;
	}
	
	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	
	/**
	 * @return the descricao
	 */
	public String getDescricao() {
		return ResourceBundle.getMessage(descricao);
	}

	/**
	 * @param descricao the descricao to set
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}	
}
