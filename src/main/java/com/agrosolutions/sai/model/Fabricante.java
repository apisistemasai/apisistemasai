package com.agrosolutions.sai.model;

import com.agrosolutions.sai.interfaces.SerializableInterface;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;
import org.primefaces.model.SortOrder;

@Entity
@Audited
public class Fabricante extends Model implements SerializableInterface {

	private static final long serialVersionUID = 1220001178594702231L;
	@Column(unique=true)
	private String nome;
	private TipoFabricante tipoFabricante;
	@Column(length=800)
	private String observacao;
	
	@Transient
	private String sortField;
	@Transient
	private SortOrder sortOrder;

	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public TipoFabricante getTipoFabricante() {
		return tipoFabricante;
	}
	public void setTipoFabricante(TipoFabricante tipoFabricante) {
		this.tipoFabricante = tipoFabricante;
	}
	public String getObservacao() {
		return observacao;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public void setSortField(String sortField) {
		this.sortField = sortField;
	}
	public String getSortField() {
		return sortField;
	}
	public void setSortOrder(SortOrder sortOrder) {
		this.sortOrder = sortOrder;
	}
	public SortOrder getSortOrder() {
		return sortOrder;
	}
	
}
