package com.agrosolutions.sai.model;


import com.agrosolutions.sai.interfaces.SerializableInterface;
import javax.persistence.Entity;

/**
 * Classe que representa as categorias de contas 
 * @author raphael.ferreira
 *
 */
@Entity
public class Categoria extends Model implements SerializableInterface  {

	private static final long serialVersionUID = -879307323066140764L;
	private String codigo;
	private String nome;
	private Integer ordem;
	private Categoria pai;
	private TipoCategoria tipo;
	
	
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Integer getOrdem() {
		return ordem;
	}
	public void setOrdem(Integer ordem) {
		this.ordem = ordem;
	}
	public Categoria getPai() {
		return pai;
	}
	public void setPai(Categoria pai) {
		this.pai = pai;
	}
	public TipoCategoria getTipo() {
		return tipo;
	}
	public void setTipo(TipoCategoria tipo) {
		this.tipo = tipo;
	}
}
