package com.agrosolutions.sai.model;

import com.agrosolutions.sai.interfaces.SerializableInterface;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

import org.hibernate.annotations.BatchSize;
import org.hibernate.envers.Audited;
import org.primefaces.model.SortOrder;

@Entity
@Audited
@BatchSize(size = 30)
public class Emissor extends Model implements SerializableInterface {

    private static final long serialVersionUID = 992029385972110765L;
    private TipoEmissor tipoEmissor;
    @ManyToOne
    private Fabricante fabricante;
    
    @Column(unique = true)
    private String modelo;
    private BigDecimal vazao;
    private BigDecimal pressao;
    private BigDecimal raio;
    @Transient
    private String sortField;
    @Transient
    private SortOrder sortOrder;

    public TipoEmissor getTipoEmissor() {
        return tipoEmissor;
    }

    public void setTipoEmissor(TipoEmissor tipoEmissor) {
        this.tipoEmissor = tipoEmissor;
    }

    public Fabricante getFabricante() {
        return fabricante;
    }

    public void setFabricante(Fabricante fabricante) {
        this.fabricante = fabricante;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public BigDecimal getVazao() {
        return vazao;
    }

    public void setVazao(BigDecimal vazao) {
        this.vazao = vazao;
    }

    public BigDecimal getPressao() {
        return pressao;
    }

    public void setPressao(BigDecimal pressao) {
        this.pressao = pressao;
    }

    public BigDecimal getRaio() {
        return raio == null ? new BigDecimal(0) : raio;
    }

    public void setRaio(BigDecimal raio) {
        this.raio = raio;
    }

    public String getSortField() {
        return sortField;
    }

    public void setSortField(String sortField) {
        this.sortField = sortField;
    }

    public SortOrder getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(SortOrder sortOrder) {
        this.sortOrder = sortOrder;
    }
}
