package com.agrosolutions.sai.model;

import com.agrosolutions.sai.interfaces.SerializableInterface;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Transient;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.envers.Audited;
import org.primefaces.model.SortOrder;

/**
 * Classe que representa os perfis associados aos usu�rios. 
 * @author raphael.ferreira
 *
 */
@Entity
@Audited
@BatchSize(size=10)
public class Perfil extends Model implements SerializableInterface {

	private static final long serialVersionUID = 7066768532815676706L;

	private String descricao;
	private TipoPerfil tipo;
	
	@Transient
	private String sortField;
	@Transient
	private SortOrder sortOrder;
	
	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
	@Fetch(FetchMode.SUBSELECT)
	@BatchSize(size=10)
	@JoinTable(name = "funcionalidade_perfil", schema="sai" ,  
			joinColumns = @JoinColumn(name = "id_perfil"),
			inverseJoinColumns = @JoinColumn(name = "id_funcionalidade"))
	private List<Funcionalidade> funcionalidades;

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public List<Funcionalidade> getFuncionalidades() {
		return funcionalidades;
	}
	public void setFuncionalidades(List<Funcionalidade> funcionalidades) {
		this.funcionalidades = funcionalidades;
	}

	public TipoPerfil getTipo() {
		return tipo;
	}

	public void setTipo(TipoPerfil tipo) {
		this.tipo = tipo;
	}

	public void setSortField(String sortField) {
		this.sortField = sortField;
	}

	public String getSortField() {
		return sortField;
	}

	public void setSortOrder(SortOrder sortOrder) {
		this.sortOrder = sortOrder;
	}

	public SortOrder getSortOrder() {
		return sortOrder;
	}
}
