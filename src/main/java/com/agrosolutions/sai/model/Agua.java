package com.agrosolutions.sai.model;

import com.agrosolutions.sai.interfaces.SerializableInterface;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.BatchSize;
import org.hibernate.envers.Audited;
import org.primefaces.model.SortOrder;

import com.agrosolutions.sai.util.BigDecimalUtil;

@Entity
@Audited
@BatchSize(size=30)
public class Agua extends Model implements SerializableInterface {

	private static final long serialVersionUID = 1043369975689498271L;
	@Temporal(TemporalType.DATE)
	private Date data;
	private Date dataInicial;
	private BigDecimal valor;		
	private Long leituraanterior; 	
	private Long leituraatual;		
	private BigDecimal k2fixo; 	
	private BigDecimal k2variavel;
	private BigDecimal et;
	private BigDecimal irr;
	
	private String lacre;
	@ManyToOne
	private Irrigante irrigante;

	@Transient
	private Long quantidade; 	
	@Transient
	private String sortField;
	@Transient
	private SortOrder sortOrder;
	@Transient
	private String codigo;
	@Transient
	private BigDecimal fri;
	
	public Agua() {
		super();
		irrigante = new Irrigante();
	}

	public Agua(Date data) {
		super();
		this.data = data;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public Long getQuantidade() {
		if (leituraanterior != null && leituraatual != null) {
			quantidade = leituraatual - leituraanterior;
		}
		return quantidade;
	}

	public void setQuantidade(Long quantidade) {
		this.quantidade = quantidade;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public Irrigante getIrrigante() {
		return irrigante;
	}

	public void setIrrigante(Irrigante irrigante) {
		this.irrigante = irrigante;
	}

	public Long getLeituraanterior() {
		return leituraanterior;
	}

	public void setLeituraanterior(Long leituraanterior) {
		this.leituraanterior = leituraanterior;
	}

	public Long getLeituraatual() {
		return leituraatual;
	}

	public void setLeituraatual(Long leituraatual) {
		this.leituraatual = leituraatual;
	}

	public BigDecimal getK2fixo() {
		return k2fixo;
	}

	public void setK2fixo(BigDecimal k2fixo) {
		this.k2fixo = k2fixo;
	}

	public BigDecimal getK2variavel() {
		return k2variavel;
	}

	public void setK2variavel(BigDecimal k2variavel) {
		this.k2variavel = k2variavel;
	}
	
	public Date getDataInicial() {
		return dataInicial;
	}

	public void setDataInicial(Date dataInicial) {
		this.dataInicial = dataInicial;
	}

	public void setLacre(String lacre) {
		this.lacre = lacre;
	}

	public String getLacre() {
		return lacre;
	}
	
	public String getPeriodo(){
		Calendar data = new GregorianCalendar();
		data.setTime(this.data);
		
		return data.getDisplayName(Calendar.MONTH, 0, Locale.getDefault()) + "/" + data.get(Calendar.YEAR);
		
	}

	public void setSortField(String sortField) {
		this.sortField = sortField;
	}

	public String getSortField() {
		return sortField;
	}

	public void setSortOrder(SortOrder sortOrder) {
		this.sortOrder = sortOrder;
	}

	public SortOrder getSortOrder() {
		return sortOrder;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setEt(BigDecimal et) {
		this.et = et;
	}

	public BigDecimal getEt() {
		return et;
	}

	public void setIrr(BigDecimal irr) {
		this.irr = irr;
	}

	public BigDecimal getIrr() {
		return irr;
	}

	public void setFri(BigDecimal fri) {
		this.fri = fri;
	}

	public BigDecimal getFri() {
		if (getQuantidade()!=null && irr != null && irr.intValue() != 0) {
			fri = new BigDecimal(getQuantidade()).divide(irr, BigDecimalUtil.MC_RUP);
		}
		return fri; 
	}

}
