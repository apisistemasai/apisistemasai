package com.agrosolutions.sai.model;

import com.agrosolutions.sai.interfaces.SerializableInterface;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class DemandaHidrica extends Model implements SerializableInterface {
	
	private static final long serialVersionUID = -2034411338207060435L;
	
	@Temporal(TemporalType.DATE)
	private Date data;
	@ManyToOne
	private Clima clima;
        
	@ManyToOne
	private Plantio plantio;
        
	private BigDecimal eto;
	private BigDecimal kc;
	private BigDecimal etc;
        
	@ManyToOne
	private Irrigante irrigante;
	

	public DemandaHidrica() {
		super();
	}

	public DemandaHidrica(Date data, Clima clima, Plantio plantio,
			BigDecimal eto, BigDecimal kc, BigDecimal etc, Irrigante irrigante) {
		super();
		this.data = data;
		this.clima = clima;
		this.plantio = plantio;
		this.eto = eto;
		this.kc = kc;
		if (kc != null && eto != null) {
			this.etc = kc.multiply(eto).setScale(2, RoundingMode.FLOOR);
		}
		this.irrigante = irrigante;
	}
	
	public Date getData() {
		return data;
	}
	public void setData(Date data) {
		this.data = data;
	}
	public Clima getClima() {
		return clima;
	}
	public void setClima(Clima clima) {
		this.clima = clima;
	}
	public Plantio getPlantio() {
		return plantio;
	}
	public void setPlantio(Plantio plantio) {
		this.plantio = plantio;
	}
	public BigDecimal getEto() {
		return eto;
	}
	public void setEto(BigDecimal eto) {
		this.eto = eto;
	}
	public BigDecimal getKc() {
		return kc;
	}
	public void setKc(BigDecimal kc) {
		this.kc = kc;
	}
	public BigDecimal getEtc() {
		return etc;
	}
	public void setEtc(BigDecimal etc) {
		this.etc = etc;
	}
	public Irrigante getIrrigante() {
		return irrigante;
	}
	public void setIrrigante(Irrigante irrigante) {
		this.irrigante = irrigante;
	}
}