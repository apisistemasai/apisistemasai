package com.agrosolutions.sai.model;

import com.agrosolutions.sai.interfaces.SerializableInterface;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.envers.Audited;
import org.primefaces.model.SortOrder;

@Entity
@Audited
public class BaciaHidrografica extends Model implements SerializableInterface {


	private String nome;

	private String uf;

	private String localizacao;

	private String latitude;

	private String longitude;

	private BigDecimal volumeTotal;
	
	private Integer zoommapa;

	@Column(length = 800)
	private String observacao;

	@Temporal(TemporalType.DATE)
	private Date dataCadastro;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "bacia")
	@Fetch(FetchMode.SUBSELECT)
	@BatchSize(size = 30)
	private List<AtividadeEconomica> atividades;

	@Transient
	private
	List<BaciaHidrografica> bacias;

	@Transient
	private String sortField;
	@Transient
	private SortOrder sortOrder;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getUf() {
		return uf;
	}

	public void setUf(String uf) {
		this.uf = uf;
	}

	public String getLocalizacao() {
		return localizacao;
	}

	public void setLocalizacao(String localizacao) {
		this.localizacao = localizacao;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public void setSortField(String sortField) {
		this.sortField = sortField;
	}

	public String getSortField() {
		return sortField;
	}

	public void setSortOrder(SortOrder sortOrder) {
		this.sortOrder = sortOrder;
	}

	public SortOrder getSortOrder() {
		return sortOrder;
	}

	public BigDecimal getVolumeTotal() {
		return volumeTotal;
	}

	public void setVolumeTotal(BigDecimal volumeTotal) {
		this.volumeTotal = volumeTotal;
	}

	public List<BaciaHidrografica> getBacias() {
		return bacias;
	}

	public void setBacias(List<BaciaHidrografica> bacias) {
		this.bacias = bacias;
	}

	public Integer getZoommapa() {
		return zoommapa;
	}

	public void setZoommapa(Integer zoommapa) {
		this.zoommapa = zoommapa;
	}

	public List<AtividadeEconomica> getAtividades() {
		return atividades;
	}

	public void setAtividades(List<AtividadeEconomica> atividades) {
		this.atividades = atividades;
	}
}