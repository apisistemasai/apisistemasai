package com.agrosolutions.sai.model;

import com.agrosolutions.sai.interfaces.SerializableInterface;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.BatchSize;
import org.hibernate.envers.Audited;

@Entity
@Audited
public class Indicadores extends Model implements SerializableInterface {

	private static final long serialVersionUID = 5185016274688266749L;
	
	@Temporal(TemporalType.DATE)
	private Date data;
	
	@ManyToOne
	@BatchSize(size = 30)
	private AtividadeEconomica atividadeEconomica;
	
	/**SEGURAN�A PRODUTIVA**/
	private BigDecimal produtividade;	// Kg
	private BigDecimal laminaAplicada;	// m�
	private BigDecimal kgHa;	// m�
	private BigDecimal kgM3;	// m�
	private PesoIndicadores pesoKgHa;
	private PesoIndicadores pesoKgM3;
	
	/**SEGURAN�A ECONOMICA**/
	private BigDecimal receitaLiquida;
	private BigDecimal vbp; //Valor Bruto da Produ��o (R$)

	private BigDecimal receitaLiquidaHa;
	private BigDecimal receitaLiquidaM3;
	private PesoIndicadores pesoReceitaLiquidaHa;
	private PesoIndicadores pesoReceitaLiquidaM3;
	
	/**SEGURAN�A SOCIAL**/
	private BigDecimal empregos;
	private BigDecimal empregosHa;
	private BigDecimal empregosM3;
	private PesoIndicadores pesoEmpregosHa;
	private PesoIndicadores pesoEmpregosM3;
	
	/**SEGURAN�A HIDRICA**/
	private BigDecimal litrosSegundo;
	private BigDecimal m3Ha;
	private BigDecimal litrosSegundoHa;
	private PesoIndicadoresSegHidrica pesoM3Ha;
	private PesoIndicadoresSegHidrica pesoLitrosSegundoHa;
	private PesoIndicadorCiclo cicloCultura;
	private BigDecimal qtdDiasCicloAnual;
	
	private BigDecimal pesoMedio;
	private BigDecimal r;
	
	private BigDecimal areaTotal;
	private BigDecimal consumoBacia;
	private BigDecimal chm3;
	private BigDecimal ca;

	@Transient
	private List<AtividadeEconomica> municipios;
	
	public Indicadores(){
		super();
	}
	public Indicadores(AtividadeEconomica atividadeEconomica){
		super();
		this.atividadeEconomica = atividadeEconomica;
	}
	public BigDecimal getProdutividade() {
		return produtividade;
	}
	public void setProdutividade(BigDecimal produtividade) {
		this.produtividade = produtividade;
	}
	public BigDecimal getLaminaAplicada() {
		return laminaAplicada;
	}
	public void setLaminaAplicada(BigDecimal laminaAplicada) {
		this.laminaAplicada = laminaAplicada;
	}
	public BigDecimal getKgHa() {
		return kgHa;
	}
	public void setKgHa(BigDecimal kgHa) {
		this.kgHa = kgHa;
	}
	public BigDecimal getKgM3() {
		return kgM3;
	}
	public void setKgM3(BigDecimal kgM3) {
		this.kgM3 = kgM3;
	}
	public BigDecimal getReceitaLiquida() {
		return receitaLiquida;
	}
	public void setReceitaLiquida(BigDecimal receitaLiquida) {
		this.receitaLiquida = receitaLiquida;
	}
	public BigDecimal getReceitaLiquidaHa() {
		return receitaLiquidaHa;
	}
	public void setReceitaLiquidaHa(BigDecimal receitaLiquidaHa) {
		this.receitaLiquidaHa = receitaLiquidaHa;
	}
	public BigDecimal getReceitaLiquidaM3() {
		return receitaLiquidaM3;
	}
	public void setReceitaLiquidaM3(BigDecimal receitaLiquidaM3) {
		this.receitaLiquidaM3 = receitaLiquidaM3;
	}
	public BigDecimal getEmpregos() {
		return empregos;
	}
	public void setEmpregos(BigDecimal empregos) {
		this.empregos = empregos;
	}
	public BigDecimal getEmpregosHa() {
		return empregosHa;
	}
	public void setEmpregosHa(BigDecimal empregosHa) {
		this.empregosHa = empregosHa;
	}
	public BigDecimal getEmpregosM3() {
		return empregosM3;
	}
	public void setEmpregosM3(BigDecimal empregosM3) {
		this.empregosM3 = empregosM3;
	}
	public BigDecimal getLitrosSegundo() {
		return litrosSegundo;
	}
	public void setLitrosSegundo(BigDecimal litrosSegundo) {
		this.litrosSegundo = litrosSegundo;
	}
	public BigDecimal getLitrosSegundoHa() {
		return litrosSegundoHa;
	}
	public void setLitrosSegundoHa(BigDecimal litrosSegundoHa) {
		this.litrosSegundoHa = litrosSegundoHa;
	}
	public BigDecimal getM3Ha() {
		return m3Ha;
	}
	public void setM3Ha(BigDecimal m3Ha) {
		this.m3Ha = m3Ha;
	}
	public PesoIndicadores getPesoKgHa() {
		return pesoKgHa;
	}
	public void setPesoKgHa(PesoIndicadores pesoKgHa) {
		this.pesoKgHa = pesoKgHa;
	}
	public PesoIndicadores getPesoKgM3() {
		return pesoKgM3;
	}
	public void setPesoKgM3(PesoIndicadores pesoKgM3) {
		this.pesoKgM3 = pesoKgM3;
	}
	public PesoIndicadores getPesoReceitaLiquidaHa() {
		return pesoReceitaLiquidaHa;
	}
	public void setPesoReceitaLiquidaHa(PesoIndicadores pesoReceitaLiquidaHa) {
		this.pesoReceitaLiquidaHa = pesoReceitaLiquidaHa;
	}
	public PesoIndicadores getPesoReceitaLiquidaM3() {
		return pesoReceitaLiquidaM3;
	}
	public void setPesoReceitaLiquidaM3(PesoIndicadores pesoReceitaLiquidaM3) {
		this.pesoReceitaLiquidaM3 = pesoReceitaLiquidaM3;
	}
	public PesoIndicadores getPesoEmpregosHa() {
		return pesoEmpregosHa;
	}
	public void setPesoEmpregosHa(PesoIndicadores pesoEmpregosHa) {
		this.pesoEmpregosHa = pesoEmpregosHa;
	}
	public PesoIndicadores getPesoEmpregosM3() {
		return pesoEmpregosM3;
	}
	public void setPesoEmpregosM3(PesoIndicadores pesoEmpregosM3) {
		this.pesoEmpregosM3 = pesoEmpregosM3;
	}

	public BigDecimal getPesoMedio() {
		return pesoMedio;
	}
	public void setPesoMedio(BigDecimal pesoMedio) {
		this.pesoMedio = pesoMedio;
	}
	public PesoIndicadoresSegHidrica getPesoM3Ha() {
		return pesoM3Ha;
	}
	public void setPesoM3Ha(PesoIndicadoresSegHidrica pesoM3Ha) {
		this.pesoM3Ha = pesoM3Ha;
	}
	public PesoIndicadoresSegHidrica getPesoLitrosSegundoHa() {
		return pesoLitrosSegundoHa;
	}
	public void setPesoLitrosSegundoHa(PesoIndicadoresSegHidrica pesoLitrosSegundoHa) {
		this.pesoLitrosSegundoHa = pesoLitrosSegundoHa;
	}
	public BigDecimal getVbp() {
		return vbp;
	}
	public void setVbp(BigDecimal vbp) {
		this.vbp = vbp;
	}
	public PesoIndicadorCiclo getCicloCultura() {
		return cicloCultura;
	}
	public void setCicloCultura(PesoIndicadorCiclo cicloCultura) {
		this.cicloCultura = cicloCultura;
	}
	public Date getData() {
		return data;
	}
	public void setData(Date data) {
		this.data = data;
	}
	public AtividadeEconomica getAtividadeEconomica() {
		return atividadeEconomica;
	}
	public void setAtividadeEconomica(AtividadeEconomica atividadeEconomica) {
		this.atividadeEconomica = atividadeEconomica;
	}
	public BigDecimal getAreaTotal() {
		return areaTotal;
	}
	public void setAreaTotal(BigDecimal areaTotal) {
		this.areaTotal = areaTotal;
	}
	public List<AtividadeEconomica> getMunicipios() {
		return municipios;
	}
	public void setMunicipios(List<AtividadeEconomica> municipios) {
		this.municipios = municipios;
	}
	public BigDecimal getQtdDiasCicloAnual() {
		return qtdDiasCicloAnual;
	}
	public void setQtdDiasCicloAnual(BigDecimal qtdDiasCicloAnual) {
		this.qtdDiasCicloAnual = qtdDiasCicloAnual;
	}
	public BigDecimal getR() {
		return r;
	}
	public void setR(BigDecimal r) {
		this.r = r;
	}
	public BigDecimal getConsumoBacia() {
		return consumoBacia;
	}
	public void setConsumoBacia(BigDecimal consumoBacia) {
		this.consumoBacia = consumoBacia;
	}
	public BigDecimal getChm3() {
		return chm3;
	}
	public void setChm3(BigDecimal chm3) {
		this.chm3 = chm3;
	}
	public BigDecimal getCa() {
		return ca;
	}
	public void setCa(BigDecimal ca) {
		this.ca = ca;
	}	
}