package com.agrosolutions.sai.model;

import com.agrosolutions.sai.interfaces.SerializableInterface;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;
import org.primefaces.model.SortOrder;


@Entity
@Audited
public class Nutriente extends Model implements SerializableInterface {

	private static final long serialVersionUID = -4017552929581575731L;

	@Column(unique=true)
	private String nome;
	@ManyToOne
	private Elemento elemento;
	@Column
	private Integer percentual;
	@Column
	private TipoNutriente tipoNutriente;
	
	@Transient
	private String sortField;
	@Transient
	private SortOrder sortOrder;

	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setSortField(String sortField) {
		this.sortField = sortField;
	}
	public String getSortField() {
		return sortField;
	}
	public void setSortOrder(SortOrder sortOrder) {
		this.sortOrder = sortOrder;
	}
	public SortOrder getSortOrder() {
		return sortOrder;
	}
	public Elemento getElemento() {
		return elemento;
	}
	public void setElemento(Elemento elemento) {
		this.elemento = elemento;
	}
	public Integer getPercentual() {
		return percentual;
	}
	public void setPercentual(Integer percentual) {
		this.percentual = percentual;
	}
	public TipoNutriente getTipoNutriente() {
		return tipoNutriente;
	}
	public void setTipoNutriente(TipoNutriente tipoNutriente) {
		this.tipoNutriente = tipoNutriente;
	}	
}
