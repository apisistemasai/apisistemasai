package com.agrosolutions.sai.model;

import com.agrosolutions.sai.interfaces.SerializableInterface;

public class GraficoBarra implements Comparable<GraficoBarra>, SerializableInterface {
	private final String und;
	private final String valorEixoX;
	private final double valorEixoY;
	
	
	public GraficoBarra(String valorEixoX, double valorEixoY, String und){
		this.und = und;
		this.valorEixoX = valorEixoX;
		this.valorEixoY = valorEixoY;
	}
	

	public String getValorEixoX() {
		return valorEixoX;
	}

	public double getValorEixoY() {
		return valorEixoY;
	}

	public String getUnd() {
		return und;
	}



	@Override
	public int compareTo(GraficoBarra o) {
		return Double.compare(this.valorEixoY, o.valorEixoY) > -1 ? -1 : (Double.compare(this.valorEixoY, o.valorEixoY) > 0) ? 1 : 0;
	}

}
