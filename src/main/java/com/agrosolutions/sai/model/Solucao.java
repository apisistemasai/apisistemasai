package com.agrosolutions.sai.model;

import com.agrosolutions.sai.interfaces.SerializableInterface;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import org.hibernate.annotations.BatchSize;
import org.hibernate.envers.Audited;
import org.primefaces.model.SortOrder;


@Entity
@Audited
public class Solucao extends Model implements SerializableInterface {

	private static final long serialVersionUID = 7678210881580244378L;

	@OneToMany(fetch = FetchType.EAGER)
	@BatchSize(size=10)
	private List<Nutriente> nutrientes;
	
	@Transient
	private String sortField;
	@Transient
	private SortOrder sortOrder;


	public void setSortField(String sortField) {
		this.sortField = sortField;
	}
	public String getSortField() {
		return sortField;
	}
	public void setSortOrder(SortOrder sortOrder) {
		this.sortOrder = sortOrder;
	}
	public SortOrder getSortOrder() {
		return sortOrder;
	}
	public List<Nutriente> getNutrientes() {
		return nutrientes;
	}
	public void setNutrientes(List<Nutriente> nutrientes) {
		this.nutrientes = nutrientes;
	}
	
}
