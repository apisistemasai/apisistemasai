package com.agrosolutions.sai.model;

import com.agrosolutions.sai.util.ResourceBundle;

public enum TipoManejoIrrigacao {

	NAOPOSSUI(0, "naoPossui"),
	SENSORUMIDADE(1, "sensorUmidade"),
	TENSIOMETRO(2, "tensiometro"),
	TANQUECLASSEA(3, "tanqueClasseA"),
	EMA(4, "EMA"),
	OUTRO(5, "outro");
	
	private Integer id;
	
	private String descricao;

	private TipoManejoIrrigacao(Integer id, String descricao){
		this.id = id;
		this.descricao = descricao;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the descricao
	 */
	public String getDescricao() {
		return ResourceBundle.getMessage(descricao);
	}

	/**
	 * @param descricao the descricao to set
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}	
	
	/**
	 * M�todo que retorna o valor do enum correspondente a descri��o
	 * 
	 * @param descri��o
	 * @return
	 */
	public static TipoManejoIrrigacao fromDescricao(String descricao) {
		for (TipoManejoIrrigacao tipo : values()) {
			if (tipo.descricao.toLowerCase().equals(descricao.toLowerCase())) {
				return tipo;
			}
		}
		throw new RuntimeException(TipoIrrigante.class.getName() + ResourceBundle.getMessage("valorIdentificador") + " "
				+ ResourceBundle.getMessage(descricao));
	}	
}
