package com.agrosolutions.sai.model;

import com.agrosolutions.sai.interfaces.SerializableInterface;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.validator.constraints.Email;
import org.primefaces.model.SortOrder;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

@Entity
@Audited
public class Usuario extends Model implements SerializableInterface , UserDetails {

	private static final long serialVersionUID = -8451679170281063697L;

	private String nome;

	@Column(unique = true)
	private String username;

	private String password;
	
	@Email
	private String email;
	
	private boolean ativo;
	
	private boolean senhaExpirada;
	
	@Column(length=16)
	@NotAudited
	private String hashCodigo;

	@ManyToMany(fetch = FetchType.EAGER)
	@Fetch(FetchMode.SUBSELECT)
	@BatchSize(size = 10)
	@JoinTable(name = "usuario_distrito", schema = "sai", joinColumns = @JoinColumn(name = "id_usuario"), inverseJoinColumns = @JoinColumn(name = "id_distrito"))
	@NotAudited
	private List<Distrito> distritos;

	@ManyToMany(fetch = FetchType.EAGER)
	@Fetch(FetchMode.SUBSELECT)
	@BatchSize(size = 10)
	@JoinTable(name = "usuario_baciaHidrografica", schema = "sai", joinColumns = @JoinColumn(name = "id_usuario"), inverseJoinColumns = @JoinColumn(name = "id_baciaHidrografica"))
	@NotAudited
	private List<BaciaHidrografica> bacias;

	@ManyToMany(fetch = FetchType.EAGER)
	@Fetch(FetchMode.SUBSELECT)
	@BatchSize(size = 10)
	@JoinTable(name = "usuario_modulo", schema = "sai", joinColumns = @JoinColumn(name = "id_usuario"), inverseJoinColumns = @JoinColumn(name = "id_modulo"))
	@NotAudited
	private List<Modulo> modulos;
	
	@ManyToOne
	private Perfil perfil;

	@ManyToOne
	private Distrito distritoPadrao;
	
	@ManyToOne
	private BaciaHidrografica baciaPadrao;

	@Transient
	private String confirmarSenha;
	
	@Transient
	private String confirmarEmail;	
	
	
	@Transient
	private String sortField;
	
	@Transient
	private SortOrder sortOrder;
	
	@Transient
	private Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
	

	public Collection<GrantedAuthority> getAuthorities() {
		List<GrantedAuthority> auts = new ArrayList<GrantedAuthority>();

		if (perfil != null) {
			if (perfil.getFuncionalidades() != null) {
				auts.addAll(perfil.getFuncionalidades());
			}
		}
		return auts;
	}

	@Transient
	public boolean isEnabled() {
		return ativo;
	}

	@Transient
	public boolean isAccountNonExpired() {
		return !senhaExpirada;
	}

	@Transient
	public boolean isAccountNonLocked() {
		return true;
	}

	@Transient
	public boolean isCredentialsNonExpired() {
		return true;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	public void setSenhaExpirada(boolean senhaExpirada) {
		this.senhaExpirada = senhaExpirada;
	}

	public void setAuthorities(Collection<GrantedAuthority> authorities) {
		this.authorities = authorities;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getConfirmarSenha() {
		return confirmarSenha;
	}

	public void setConfirmarSenha(String confirmarSenha) {
		this.confirmarSenha = confirmarSenha;
	}

	public String getSortField() {
		return sortField;
	}

	public void setSortField(String sortField) {
		this.sortField = sortField;
	}

	public SortOrder getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(SortOrder sortOrder) {
		this.sortOrder = sortOrder;
	}

	public Perfil getPerfil() {
		return perfil;
	}

	public void setPerfil(Perfil perfil) {
		this.perfil = perfil;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getConfirmarEmail() {
		return confirmarEmail;
	}

	public void setConfirmarEmail(String confirmarEmail) {
		this.confirmarEmail = confirmarEmail;
	}

	public List<Distrito> getDistritos() {
		return distritos;
	}

	public void setDistritos(List<Distrito> distritos) {
		this.distritos = distritos;
	}

	public String getHashCodigo() {
		return hashCodigo;
	}

	public void setHashCodigo(String hashCodigo) {
		this.hashCodigo = hashCodigo;
	}

	public Distrito getDistritoPadrao() {
		return distritoPadrao;
	}

	public void setDistritoPadrao(Distrito distritoPadrao) {
		this.distritoPadrao = distritoPadrao;
	}

	public List<Modulo> getModulos() {
		return modulos;
	}

	public void setModulos(List<Modulo> modulos) {
		this.modulos = modulos;
	}

	public List<BaciaHidrografica> getBacias() {
		return bacias;
	}

	public void setBacias(List<BaciaHidrografica> bacias) {
		this.bacias = bacias;
	}

	public BaciaHidrografica getBaciaPadrao() {
		return baciaPadrao;
	}

	public void setBaciaPadrao(BaciaHidrografica baciaPadrao) {
		this.baciaPadrao = baciaPadrao;
	}

}
