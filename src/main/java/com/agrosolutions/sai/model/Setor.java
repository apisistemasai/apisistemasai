package com.agrosolutions.sai.model;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.envers.Audited;

import com.agrosolutions.sai.util.BigDecimalUtil;
 
@Entity
@Audited
@BatchSize(size=30)
public class Setor extends Model {

	private static final long serialVersionUID = -6769871541506685700L;
	private Integer identificacao;
	private TipoSolo tipoSolo;
	private BigDecimal cc;						//Capacidade de campo
	private BigDecimal pmp;						//Ponto de mucha permanente
	private BigDecimal maxTaxaInfiltracaoChuva;	//Maxima taxa de infiltra��o da chuva
	private BigDecimal massaSolo;				//Massa do Solo
	private BigDecimal fatorHidrico;			//Fator de disponibilidade hidrica
	private BigDecimal esgotamento;				//Esgotamento de umidade inicial do solo
	private BigDecimal umidadeInicial;			//Umidade inicial do solo dispon�veis
	private BigDecimal areaTotal;
	private BigDecimal vazaoObservada;
	
	@ManyToOne
	private Irrigante irrigante;

	@OneToMany(fetch = FetchType.EAGER)
	@BatchSize(size=10)
	private List<Emissor> emissores;
	
	@Column(length=800)
	private String observacao;

	@OneToMany(mappedBy="setor")
	@Fetch(FetchMode.SUBSELECT)
	@BatchSize(size=30)
	private List<Plantio> plantio;
	
	@OneToMany(mappedBy="setor")
	private List<TesteEficiencia> testes;

	@Transient
	private BigDecimal totalUmidadeSolo;

	public BigDecimal getTotalUmidadeSolo() {
		if (cc != null && pmp != null){
			totalUmidadeSolo = cc.subtract(pmp);
		}
		return totalUmidadeSolo;
	}
	public void setTotalUmidadeSolo(BigDecimal totalUmidadeSolo) {
		this.totalUmidadeSolo = totalUmidadeSolo;
	}
	public TipoSolo getTipoSolo() {
		return tipoSolo;
	}
	public void setTipoSolo(TipoSolo tipoSolo) {
		this.tipoSolo = tipoSolo;
	}
	public BigDecimal getAreaTotal() {
		return areaTotal;
	}
	public void setAreaTotal(BigDecimal areaTotal) {
		this.areaTotal = areaTotal;
	}
	public Irrigante getIrrigante() {
		return irrigante;
	}
	public void setIrrigante(Irrigante irrigante) {
		this.irrigante = irrigante;
	}
	public List<Emissor> getEmissores() {
		return emissores;
	}
	public void setEmissores(List<Emissor> emissores) {
		this.emissores = emissores;
	}
	public String getObservacao() {
		return observacao;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public List<Plantio> getPlantio() {
		return plantio;
	}
	public void setPlantio(List<Plantio> plantio) {
		this.plantio = plantio;
	}
	public Integer getIdentificacao() {
		return identificacao;
	}
	public void setIdentificacao(Integer identificacao) {
		this.identificacao = identificacao;
	}
	public BigDecimal getCc() {
		return cc;
	}
	public void setCc(BigDecimal cc) {
		this.cc = cc;
	}
	public BigDecimal getPmp() {
		return pmp;
	}
	public void setPmp(BigDecimal pmp) {
		this.pmp = pmp;
	}
	public BigDecimal getMaxTaxaInfiltracaoChuva() {
		return maxTaxaInfiltracaoChuva;
	}
	public void setMaxTaxaInfiltracaoChuva(BigDecimal maxTaxaInfiltracaoChuva) {
		this.maxTaxaInfiltracaoChuva = maxTaxaInfiltracaoChuva;
	}
	public BigDecimal getEsgotamento() {
		return esgotamento;
	}
	public void setEsgotamento(BigDecimal esgotamento) {
		this.esgotamento = esgotamento;
	}
	public BigDecimal getUmidadeInicial() {
		return umidadeInicial;
	}
	public void setUmidadeInicial(BigDecimal umidadeInicial) {
		this.umidadeInicial = umidadeInicial;
	}
	public List<TesteEficiencia> getTestes() {
		return testes;
	}
	public void setTestes(List<TesteEficiencia> testes) {
		this.testes = testes;
	}
	public BigDecimal getMassaSolo() {
		return massaSolo;
	}
	public void setMassaSolo(BigDecimal massaSolo) {
		this.massaSolo = massaSolo;
	}
	public BigDecimal getFatorHidrico() {
		return fatorHidrico;
	}
	public void setFatorHidrico(BigDecimal fatorHidrico) {
		this.fatorHidrico = fatorHidrico;
	}

	public BigDecimal cad() {
		BigDecimal parcial = cc.subtract(pmp);
		BigDecimal parcial2 = parcial.divide(new BigDecimal(100), BigDecimalUtil.MC_RUP);
		
		return parcial2.multiply(massaSolo);
	}
	public BigDecimal getVazaoObservada() {
		return vazaoObservada;
	}
	public void setVazaoObservada(BigDecimal vazaoObservada) {
		this.vazaoObservada = vazaoObservada;
	}
	
}
