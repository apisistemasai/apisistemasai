package com.agrosolutions.sai.model;

import java.math.BigDecimal;


public enum CicloCultura {
	
	PERMANENTE(new BigDecimal(0.5), "Permanente" ),
	LONGO(new BigDecimal(0.75), "Tempo Longo" ),
	CURTO(new BigDecimal(1), "Tempo Curto" );
	
	public BigDecimal valor;
	private String descricao;

	private CicloCultura(BigDecimal valor, String descricao){
		this.setValor(valor);
		this.setDescricao(descricao);
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
}
