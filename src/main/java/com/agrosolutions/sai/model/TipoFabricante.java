package com.agrosolutions.sai.model;

import com.agrosolutions.sai.util.ResourceBundle;

public enum TipoFabricante {

	Bomba(0, "bomba"),
	Emissor(1, "emissor"),
	Filtro(2, "filtro"),
	Valvula(3, "valvula"),
	Tubo(4, "tubo"),
	Todos(5, "todos");
	
	private Integer id;
	
	private String descricao;

	private TipoFabricante(Integer id, String descricao){
		this.id = id;
		this.descricao = descricao;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the descricao
	 */
	public String getDescricao() {
		return ResourceBundle.getMessage(descricao);
	}

	/**
	 * @param descricao the descricao to set
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}	

	/**
	 * M�todo que retorna o valor do enum correspondente a descri��o
	 * 
	 * @param descri��o
	 * @return
	 */
	public static TipoFabricante fromDescricao(String descricao) {
		for (TipoFabricante tipo : values()) {
			if (tipo.descricao.toLowerCase().equals(descricao.toLowerCase())) {
				return tipo;
			}
		}
		throw new RuntimeException(TipoFabricante.class.getName() + "valorIdentificador" +" "
				+ descricao);
	}	

}
