package com.agrosolutions.sai.model;

import com.agrosolutions.sai.interfaces.SerializableInterface;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Transient;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.envers.Audited;

@Entity
@Audited
   public class Referencia extends Model implements SerializableInterface {

	private static final long serialVersionUID = -8868705569013435335L;

	private String autor;
	private TipoMetodologia tipoMetodologiaETo;
	private TipoMetodologia tipoMetodologiaETc;
	private Integer ano;
	private Boolean coautor;
	private String cidadep;
	private String estadop;
	private String paisp;
	@ManyToOne
	private Variedade variedade;
	@Column(length=800)
	private String observacao;

	@OneToMany(cascade = CascadeType.ALL, mappedBy="referencia", fetch=FetchType.EAGER)
	@Fetch(FetchMode.SUBSELECT)
	@BatchSize(size=10)
	@OrderBy("ordem asc")
	private List<Estadio> estadios;
	@Transient
	private Long totalDias;
	
	public String getAutor() {
		return autor;
	}
	public void setAutor(String autor) {
		this.autor = autor;
	}
	public Integer getAno() {
		return ano;
	}
	public void setAno(Integer ano) {
		this.ano = ano;
	}
	public Boolean getCoautor() {
		return coautor;
	}
	public void setCoautor(Boolean coautor) {
		this.coautor = coautor;
	}
	public String getCidadep() {
		return cidadep;
	}
	public void setCidadep(String cidadep) {
		this.cidadep = cidadep;
	}
	public String getEstadop() {
		return estadop;
	}
	public void setEstadop(String estadop) {
		this.estadop = estadop;
	}
	public String getPaisp() {
		return paisp;
	}
	public void setPaisp(String paisp) {
		this.paisp = paisp;
	}
	public TipoMetodologia getTipoMetodologiaETo() {
		return tipoMetodologiaETo;
	}
	public void setTipoMetodologiaETo(TipoMetodologia tipoMetodologiaETo) {
		this.tipoMetodologiaETo = tipoMetodologiaETo;
	}
	public TipoMetodologia getTipoMetodologiaETc() {
		return tipoMetodologiaETc;
	}
	public void setTipoMetodologiaETc(TipoMetodologia tipoMetodologiaETc) {
		this.tipoMetodologiaETc = tipoMetodologiaETc;
	}
	public Variedade getVariedade() {
		return variedade;
	}
	public void setVariedade(Variedade variedade) {
		this.variedade = variedade;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public String getObservacao() {
		return observacao;
	}
	public void setTotalDias(Long totalDias) {
		this.totalDias = totalDias;
	}
	public Long getTotalDias() {
		totalDias = 0L;
		if (estadios != null && estadios.isEmpty()) {
			for (Estadio e : estadios) {
				totalDias = totalDias + e.getQtdDias();
			}
		}
		return totalDias;
	}
	public List<Estadio> getEstadios() {
		return estadios;
	}
	public void setEstadios(List<Estadio> estadios) {
		this.estadios = estadios;
	}
	
}

	

