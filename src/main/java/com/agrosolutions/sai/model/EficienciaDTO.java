package com.agrosolutions.sai.model;

import com.agrosolutions.sai.interfaces.SerializableInterface;
import java.math.BigDecimal;

public class EficienciaDTO implements Comparable<EficienciaDTO>, SerializableInterface {

	BigDecimal valorClasse; 
	
	public EficienciaDTO(BigDecimal linha1Emissor1Media) {
		valorClasse = linha1Emissor1Media;
	}

	public BigDecimal getValorClasse() {
		return valorClasse;
	}

	public void setValorClasse(BigDecimal valorClasse) {
		this.valorClasse = valorClasse;
	}

	@Override
	public int compareTo(EficienciaDTO o) {
		return this.getValorClasse().compareTo(((EficienciaDTO)o).getValorClasse());	
	}


}
