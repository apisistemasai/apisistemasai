package com.agrosolutions.sai.model;

import com.agrosolutions.sai.interfaces.SerializableInterface;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.BatchSize;
import org.hibernate.envers.Audited;

@Entity
@Audited
@BatchSize(size = 30)
public class Plantio extends Model implements SerializableInterface {

	private static final long serialVersionUID = -366490572795584207L;
	
	@ManyToOne
	@BatchSize(size = 30)
	private Setor setor;
	
	@ManyToOne
	@BatchSize(size = 30)
	private Cultivo cultivo;

	@Temporal(TemporalType.DATE)
	private Date dataFim;
	
	private Boolean fertirrigacao;
	
	private TipoFertirrigacao tipoFertirrigacao;
	
	private TipoManejoIrrigacao tipoManejoIrrigacao;
	
	private MotivoFinalPlantio motivoFinalizacao;
	
	private Double espacamento1;
	
	private Double espacamento2;
	
	private Double espacamento3;
	
	private Integer totalEmissores;
	
	private Integer totalPlanta;
	
	private BigDecimal areatotal;
	
	private Double espE;
	
	private Double espLE;
	
	private TipoFormulaGotejo tipoFormulaGotejo;
	
	private TipoCalculoKL tipoCalculoKL;
	
	private TipoFormulaMicro tipoFormulaMicro;
	
	private Integer turnoRega;

	@Transient
	public BigDecimal pam;
	
	@Column(nullable = false, columnDefinition = "boolean default false")
	private Boolean consorcio = false;
	
	@Column(nullable = false, columnDefinition = "boolean default false")
	private Boolean sequeiro = false;
	
	@Transient
	private FormulaPlantio formula;

	@OneToMany
	private List<Doenca> doencas;
	
	@OneToMany
	private List<Praga> pragas;
	
	@OneToMany
	private List<Defensivo> defensivos;
	
	@OneToMany
	private List<Adubo> adubos;
	
	@OneToMany
	private List<Colheita> colheitas;
	
	@OneToMany
	@BatchSize(size = 10)
	private List<Emissor> emissores;
	
	@Column(nullable = false, columnDefinition = "boolean default false")
	private Boolean enviarSMS = true;
	
	@Column(nullable = false, columnDefinition = "boolean default false")
	private Boolean enviarEmail = true;
	
	public Plantio() {
		super();
		turnoRega = 1;
	}

	public Plantio(Cultivo cultivo) {
		super();
		this.cultivo = cultivo;
		this.turnoRega = 1;
	}

	public Setor getSetor() {
		return setor;
	}

	public void setSetor(Setor setor) {
		this.setor = setor;
	}

	public Cultivo getCultivo() {
		return cultivo;
	}

	public void setCultivo(Cultivo cultivo) {
		this.cultivo = cultivo;
	}

	public Boolean getConsorcio() {
		return consorcio;
	}

	public void setConsorcio(Boolean consorcio) {
		this.consorcio = consorcio;
	}

	public Boolean getFertirrigacao() {
		return fertirrigacao;
	}

	public void setFertirrigacao(Boolean fertirrigacao) {
		this.fertirrigacao = fertirrigacao;
	}

	public TipoFertirrigacao getTipoFertirrigacao() {
		return tipoFertirrigacao;
	}

	public void setTipoFertirrigacao(TipoFertirrigacao tipoFertirrigacao) {
		this.tipoFertirrigacao = tipoFertirrigacao;
	}

	public TipoManejoIrrigacao getTipoManejoIrrigacao() {
		return tipoManejoIrrigacao;
	}

	public void setTipoManejoIrrigacao(TipoManejoIrrigacao tipoManejoIrrigacao) {
		this.tipoManejoIrrigacao = tipoManejoIrrigacao;
	}

	public Double getEspacamento1() {
		return espacamento1;
	}

	public void setEspacamento1(Double espacamento1) {
		this.espacamento1 = espacamento1;
	}

	public Double getEspacamento2() {
		return espacamento2;
	}

	public void setEspacamento2(Double espacamento2) {
		this.espacamento2 = espacamento2;
	}

	public Double getEspacamento3() {
		return espacamento3;
	}

	public void setEspacamento3(Double espacamento3) {
		this.espacamento3 = espacamento3;
	}

	public List<Doenca> getDoencas() {
		return doencas;
	}

	public void setDoencas(List<Doenca> doencas) {
		this.doencas = doencas;
	}

	public List<Praga> getPragas() {
		return pragas;
	}

	public void setPragas(List<Praga> pragas) {
		this.pragas = pragas;
	}

	public Double getEspE() {
		return espE;
	}

	public void setEspE(Double espE) {
		this.espE = espE;
	}

	public Double getEspLE() {
		return espLE;
	}

	public void setEspLE(Double espLE) {
		this.espLE = espLE;
	}

	public BigDecimal getPam() {
		return pam;
	}

	public void setPam(BigDecimal pam) {
		this.pam = pam;
	}

	public List<Defensivo> getDefensivos() {
		return defensivos;
	}

	public void setDefensivos(List<Defensivo> defensivos) {
		this.defensivos = defensivos;
	}

	public List<Adubo> getAdubos() {
		return adubos;
	}

	public void setAdubos(List<Adubo> adubos) {
		this.adubos = adubos;
	}

	public Integer getTotalEmissores() {
		return totalEmissores;
	}

	public void setTotalEmissores(Integer totalEmissores) {
		this.totalEmissores = totalEmissores;
	}

	public Integer getTotalPlanta() {
		return totalPlanta;
	}

	public void setTotalPlanta(Integer totalPlanta) {
		this.totalPlanta = totalPlanta;
	}

	public Date getDataFim() {
		return dataFim;
	}

	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}

	public List<Colheita> getColheitas() {
		return colheitas;
	}

	public void setColheitas(List<Colheita> colheitas) {
		this.colheitas = colheitas;
	}

	public void setAreatotal(BigDecimal areatotal) {
		this.areatotal = areatotal;
	}

	public BigDecimal getAreatotal() {
		return areatotal;
	}

	public MotivoFinalPlantio getMotivoFinalizacao() {
		return motivoFinalizacao;
	}

	public void setMotivoFinalizacao(MotivoFinalPlantio motivoFinalizacao) {
		this.motivoFinalizacao = motivoFinalizacao;
	}

	public List<Emissor> getEmissores() {
		return emissores;
	}

	public void setEmissores(List<Emissor> emissores) {
		this.emissores = emissores;
	}

	public TipoFormulaGotejo getTipoFormulaGotejo() {
		return tipoFormulaGotejo;
	}

	public void setTipoFormulaGotejo(TipoFormulaGotejo tipoFormulaGotejo) {
		this.tipoFormulaGotejo = tipoFormulaGotejo;
	}

	public Integer getTurnoRega() {
		return turnoRega;
	}

	public void setTurnoRega(Integer turnoRega) {
		this.turnoRega = turnoRega;
	}

	public TipoCalculoKL getTipoCalculoKL() {
		return tipoCalculoKL;
	}

	public void setTipoCalculoKL(TipoCalculoKL tipoCalculoKL) {
		this.tipoCalculoKL = tipoCalculoKL;
	}

	public TipoFormulaMicro getTipoFormulaMicro() {
		return tipoFormulaMicro;
	}

	public void setTipoFormulaMicro(TipoFormulaMicro tipoFormulaMicro) {
		this.tipoFormulaMicro = tipoFormulaMicro;
	}

	public FormulaPlantio getFormula() {
		return formula;
	}

	public void setFormula(FormulaPlantio formula) {
		this.formula = formula;
	}

	public void setSequeiro(Boolean sequeiro) {
		this.sequeiro = sequeiro;
	}

	public Boolean getSequeiro() {
		return sequeiro;
	}

	public Boolean getEnviarSMS() {
		return enviarSMS;
	}

	public void setEnviarSMS(Boolean enviarSMS) {
		this.enviarSMS = enviarSMS;
	}

	public Boolean getEnviarEmail() {
		return enviarEmail;
	}

	public void setEnviarEmail(Boolean enviarEmail) {
		this.enviarEmail = enviarEmail;
	}

	public String getDescricao(){
		String retorno = "";
		if (cultivo!=null){
			Calendar data = new GregorianCalendar();
			data.setTime(cultivo.getDataInicio());
			retorno += cultivo.getCultura().getNome() + "-" + cultivo.getVariedade().getNome() + "-" + data.get(Calendar.DAY_OF_MONTH) + "/" + (data.get(Calendar.MONTH)+1) + "/" + data.get(Calendar.YEAR);    
		}
		return retorno;
	}

}
