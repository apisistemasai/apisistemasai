package com.agrosolutions.sai.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import com.agrosolutions.sai.model.Bomba;

/**
 * Converter espec�fico para sele��o com {@link Bomba}, onde o primeiro item selecionado � vazio
 * 
 * @author Raphael.Ferreira
 */
@FacesConverter(value = "bombaConverter")
public class BombaConverter implements Converter {

	private static final Map<Bomba, String> BOMBAS = new HashMap<Bomba, String>();

	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String stringValor) {

		if ("".equals(stringValor)) {
			return null;
		}
		// Procura pela bomba correspondente ao select item selecionado:
		for (Entry<Bomba, String> entry : BOMBAS.entrySet()) {
			if (entry.getValue().equals(stringValor)) {
				return entry.getKey();
			}
		}

		throw new RuntimeException("Erro inesperado no converter " + getClass().getName()
				+ ": Refer�ncia para uma entidade foi perdida.");
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object bomba) {
		// Se o objeto selecionado for o valor vazio retorna
		// a string contendo a descri��o.
		if ("".equals(bomba.toString())) {
			return "";
		}

		// Adiciona as demais bombas, que s�o Bomba ao Map de BOMBAS, id
		// do mesmo.
		synchronized (BOMBAS) {
			if (BOMBAS.containsKey(bomba)) {
				BOMBAS.remove(bomba);
			}
			BOMBAS.put((Bomba) bomba, ((Bomba) bomba).getId().toString());
			return BOMBAS.get(bomba);
		}
	}
}
