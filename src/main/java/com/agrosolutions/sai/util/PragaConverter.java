package com.agrosolutions.sai.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import com.agrosolutions.sai.model.Praga;

/**
 * Converter espec�fico para sele��o com {@link Praga}, onde o primeiro item selecionado � vazio
 * 
 * @author Raphael.Ferreira
 */
@FacesConverter(value = "pragaConverter")
public class PragaConverter implements Converter {

	private static final Map<Praga, String> PRAGAS = new HashMap<Praga, String>();

	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String stringValor) {

		if ("".equals(stringValor)) {
			return null;
		}
		// Procura pela praga correspondente ao select item selecionado:
		for (Entry<Praga, String> entry : PRAGAS.entrySet()) {
			if (entry.getValue().equals(stringValor)) {
				return entry.getKey();
			}
		}

		throw new RuntimeException("Erro inesperado no converter " + getClass().getName()
				+ ": Refer�ncia para uma entidade foi perdida.");
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object praga) {
		// Se o objeto selecionado for o valor vazio retorna
		// a string contendo a descri��o.
		if ("".equals(praga.toString())) {
			return "";
		}

		synchronized (PRAGAS) {
			if (!PRAGAS.containsKey(praga)) {
				PRAGAS.put((Praga) praga, ((Praga) praga).getId().toString());
				return ((Praga) praga).getId().toString();
			}
			return PRAGAS.get(praga);
		}
	}
}
