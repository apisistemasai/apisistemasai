package com.agrosolutions.sai.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import com.agrosolutions.sai.model.Estacao;

/**
 * Converter espec�fico para sele��o com {@link Estacao}, onde o primeiro item selecionado � vazio
 * 
 * @author Raphael.Ferreira
 */
@FacesConverter(value = "estacaoConverter")
public class EstacaoConverter implements Converter {

	private static final Map<Estacao, String> ESTACOES = new HashMap<Estacao, String>();

	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String stringValor) {

		if ("".equals(stringValor)) {
			return null;
		}
		// Procura pela cultura correspondente ao select item selecionado:
		for (Entry<Estacao, String> entry : ESTACOES.entrySet()) {
			if (entry.getValue().equals(stringValor)) {
				return entry.getKey();
			}
		}

		throw new RuntimeException("Erro inesperado no converter " + getClass().getName()
				+ ": Refer�ncia para uma entidade foi perdida.");
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object estacao) {
		// Se o objeto selecionado for o valor vazio retorna
		// a string contendo a descri��o.
		if ("".equals(estacao.toString())) {
			return "";
		}

		synchronized (ESTACOES) {
			if (ESTACOES.containsKey(estacao)) {
				ESTACOES.remove(estacao);
			}
			ESTACOES.put((Estacao) estacao, ((Estacao) estacao).getId().toString());
			return ESTACOES.get(estacao);
		}
	}
}
