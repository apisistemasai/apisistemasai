package com.agrosolutions.sai.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import com.agrosolutions.sai.model.Fabricante;

/**
 * Converter espec�fico para sele��o com {@link Fabricante}, onde o primeiro item selecionado � vazio
 * 
 * @author Raphael.Ferreira
 */
@FacesConverter(value = "fabricanteConverter")
public class FabricanteConverter implements Converter {

	private static final Map<Fabricante, String> FABRICANTES = new HashMap<Fabricante, String>();

	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String stringValor) {

		if ("".equals(stringValor)) {
			return null;
		}
		// Procura pelo fabricante correspondente ao select item selecionado:
		for (Entry<Fabricante, String> entry : FABRICANTES.entrySet()) {
			if (entry.getValue().equals(stringValor)) {
				return entry.getKey();
			}
		}

		throw new RuntimeException("Erro inesperado no converter " + getClass().getName()
				+ ": Refer�ncia para uma entidade foi perdida.");
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object fabricante) {
		// Se o objeto selecionado for o valor vazio retorna
		// a string contendo a descri��o.
		if ("".equals(fabricante.toString())) {
			return "";
		}

		// Adiciona os demais fabricantes, que s�o Fabricante ao Map de Fabricantes, id
		// do mesmo.
		synchronized (FABRICANTES) {
			if (!FABRICANTES.containsKey(fabricante)) {
				FABRICANTES.put((Fabricante) fabricante, ((Fabricante) fabricante).getId().toString());
				return ((Fabricante) fabricante).getId().toString();
			}
			return FABRICANTES.get(fabricante);
		}
	}
}
