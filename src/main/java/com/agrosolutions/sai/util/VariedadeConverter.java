package com.agrosolutions.sai.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import com.agrosolutions.sai.model.Variedade;

/**
 * Converter espec�fico para sele��o com {@link Variedade}, onde o primeiro item selecionado � vazio
 * 
 * @author Raphael.Ferreira
 */
@FacesConverter(value = "variedadeConverter")
public class VariedadeConverter implements Converter {

	private static final Map<Variedade, String> VARIEDADES = new HashMap<Variedade, String>();

	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String stringValor) {

		if ("".equals(stringValor)) {
			return null;
		}
		// Procura pela variedade correspondente ao select item selecionado:
		for (Entry<Variedade, String> entry : VARIEDADES.entrySet()) {
			if (entry.getValue().equals(stringValor)) {
				return entry.getKey();
			}
		}

		throw new RuntimeException("Erro inesperado no converter " + getClass().getName()
				+ ": Refer�ncia para uma entidade foi perdida.");
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object variedade) {
		// Se o objeto selecionado for o valor vazio retorna
		// a string contendo a descri��o.
		if ("".equals(variedade.toString())) {
			return "";
		}

		synchronized (VARIEDADES) {
			if (VARIEDADES.containsKey(variedade)) {
				VARIEDADES.remove(variedade);
			}
			VARIEDADES.put((Variedade) variedade, ((Variedade) variedade).getId().toString());
			return VARIEDADES.get(variedade);
		}
	}
}
