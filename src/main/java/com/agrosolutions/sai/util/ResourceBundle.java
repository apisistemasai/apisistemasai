package com.agrosolutions.sai.util;

import java.util.Locale;
import java.util.MissingResourceException;
import javax.faces.context.FacesContext;

/**
 *
 * @author Raphael Ferreira
 */
public class ResourceBundle {

    public static String getMessage(String key) {
        return getMessageFromResourceBundle(key);
    }

    private static String getMessageFromResourceBundle(String key) {
        java.util.ResourceBundle bundle = null;
        String bundleName = "com.agrosolutions.sai.bundles.Resources";
        String message = "";
        Locale locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();

        try {
            bundle = java.util.ResourceBundle.getBundle(bundleName, locale, getCurrentLoader(bundleName));
            message = bundle.getString(key);
        } catch (MissingResourceException e) {
        }
        return message;
    }

    public static ClassLoader getCurrentLoader(Object fallbackClass) {
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        return loader==null?fallbackClass.getClass().getClassLoader():loader;
    }
}