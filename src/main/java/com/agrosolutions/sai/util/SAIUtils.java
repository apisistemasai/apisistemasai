package com.agrosolutions.sai.util;

import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import javax.faces.component.EditableValueHolder;
import javax.faces.component.UIComponent;

import org.springframework.security.core.context.SecurityContextHolder;

import com.agrosolutions.sai.model.Mes;
import com.agrosolutions.sai.model.Usuario;

/**
 * Classe utilit�ria
 * @author IVIA
 * @since 09/08/2010
 *
 */
public class SAIUtils {

	public static final String FORMATO_DATA = "dd/MM/yyyy"; 
	
	/**
	 * Formata um valor para o real(R$)
	 * @param valor
	 * @return valor formatado
	 */
	 public static String formataValor(Double valor) {
		 
		 NumberFormat formato = NumberFormat.getInstance(); 
		 formato.setMinimumFractionDigits(2);  
		 if(valor != null) {			  
			 return formato.format(valor);
		 } else {
			 return formato.format(0.0);
		 }
	 }
	 
	 /**
	  * Formata uma data de acordo com o par�metro passado
	  * Ex. par�metro formato: dd/MM/yyyy, dd/MM/yyyy HH:mm:ss, dd/MM
	  * @param formato
	  * @param data
	  * @return data formatada
	  */
	 public static String formataData(String formato, Date data) {
		 
		 SimpleDateFormat sdf = new SimpleDateFormat(formato);
		 
		 return sdf.format(data);
		 
	 }
	 
	 /**
	  * Recebe o m�s ordinal e o transforma em extenso
	  * @param mes
	  * @return
	  * @throws ParseException
	  */
	 public static String converteMesEmExtenso(String mes)
			throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("MM",new Locale("pt","br"));
		Date mesDate = sdf.parse(mes);
		sdf.applyPattern("MMMM");
		return sdf.format(mesDate);
	}
	 
	 /**
	  * Recebe uma data e retorna o mes do enumeration
	  * @param data
	  * @return Mes
	  */
	 public static Mes converteDataEmMes(Date data)	{
		 
		 Calendar calendar = Calendar.getInstance();
		 
		 calendar.setTime(data);
			
		// Teste de verifica��o do m�s de lan�amento. A lista � quebrada por per�odo.
		if(calendar.get(Calendar.MONTH) == 0) {
			return Mes.JANEIRO;
		} else if(calendar.get(Calendar.MONTH) == 1) {
			return Mes.FEVEREIRO;
		} else if(calendar.get(Calendar.MONTH) == 2) {
			return Mes.MARCO;
		} else if(calendar.get(Calendar.MONTH) == 3) {
			return Mes.ABRIL;
		} else if(calendar.get(Calendar.MONTH) == 4) {
			return Mes.MAIO;
		} else if(calendar.get(Calendar.MONTH) == 5) {
			return Mes.JUNHO;
		} else if(calendar.get(Calendar.MONTH) == 6) {
			return Mes.JULHO;
		} else if(calendar.get(Calendar.MONTH) == 7) {
			return Mes.AGOSTO;
		} else if(calendar.get(Calendar.MONTH) == 8) {
			return Mes.SETEMBRO;
		} else if(calendar.get(Calendar.MONTH) == 9) {
			return Mes.OUTUBRO;
		} else if(calendar.get(Calendar.MONTH) == 10) {
			return Mes.NOVEMBRO;
		} else {
			return Mes.DEZEMBRO;
		}

	}

	/**
	 * Obt�m o usu�rio logado na sess�o
	 * @return
	 */
	public static Usuario getUsuarioLogado() {
		if (SecurityContextHolder.getContext().getAuthentication() == null) {
			return null;
		}
		return (Usuario) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	}
	
	
	public static void cleanSubmittedValues(UIComponent component) {
		if (component instanceof EditableValueHolder) {
			EditableValueHolder evh = (EditableValueHolder) component;
			evh.setSubmittedValue(null);
			evh.setValue(null);
			evh.setLocalValueSet(false);
			evh.setValid(true);
		}
		if(component.getChildCount()>0){
			for (UIComponent child : component.getChildren()) {
				cleanSubmittedValues(child);
			}
		}
	}
}
