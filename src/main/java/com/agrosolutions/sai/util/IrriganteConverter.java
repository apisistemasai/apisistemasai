package com.agrosolutions.sai.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import com.agrosolutions.sai.model.Irrigante;

/**
 * Converter espec�fico para sele��o com {@link Irrigante}, onde o primeiro item selecionado � vazio
 * 
 * @author Raphael.Ferreira
 */
@FacesConverter(value = "irriganteConverter")
public class IrriganteConverter implements Converter {

	private static final Map<Irrigante, String> IRRIGANTES = new HashMap<Irrigante, String>();

	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String stringValor) {

		if ("".equals(stringValor)) {
			return null;
		}
		// Procura pela cultura correspondente ao select item selecionado:
		for (Entry<Irrigante, String> entry : IRRIGANTES.entrySet()) {
			if (entry.getValue().equals(stringValor)) {
				return entry.getKey();
			}
		}

		throw new RuntimeException("Erro inesperado no converter " + getClass().getName()
				+ ": Refer�ncia para uma entidade foi perdida.");
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object irrigante) {
		// Se o objeto selecionado for o valor vazio retorna
		// a string contendo a descri��o.
		if ("".equals(irrigante.toString())) {
			return "";
		}

		synchronized (IRRIGANTES) {
			if (IRRIGANTES.containsKey(irrigante)) {
				IRRIGANTES.remove(irrigante);
			}
			IRRIGANTES.put((Irrigante) irrigante, ((Irrigante) irrigante).getId().toString());
			return IRRIGANTES.get(irrigante);
		}
	}
}
