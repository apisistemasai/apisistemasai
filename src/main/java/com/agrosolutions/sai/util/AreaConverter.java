package com.agrosolutions.sai.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import com.agrosolutions.sai.model.Area;

/**
 * Converter espec�fico para sele��o com {@link Area}, onde o primeiro item selecionado � vazio
 * 
 * @author Raphael.Ferreira
 */
@FacesConverter(value = "areaConverter")
public class AreaConverter implements Converter {

	private static final Map<Area, String> AREAS = new HashMap<Area, String>();

	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String stringValor) {

		if ("".equals(stringValor)) {
			return null;
		}
		// Procura pela area correspondente ao select item selecionado:
		for (Entry<Area, String> entry : AREAS.entrySet()) {
			if (entry.getValue().equals(stringValor)) {
				return entry.getKey();
			}
		}

		throw new RuntimeException("Erro inesperado no converter " + getClass().getName()
				+ ": Refer�ncia para uma entidade foi perdida.");
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object area) {
		// Se o objeto selecionado for o valor vazio retorna
		// a string contendo a descri��o.
		if ("".equals(area.toString())) {
			return "";
		}

		synchronized (AREAS) {
			if (AREAS.containsKey(area)) {
				AREAS.remove(area);
			}
			AREAS.put((Area) area, ((Area) area).getId().toString());
			return AREAS.get(area);
		}
	}
}
