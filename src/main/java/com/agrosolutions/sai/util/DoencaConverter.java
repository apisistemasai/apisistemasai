package com.agrosolutions.sai.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import com.agrosolutions.sai.model.Doenca;

/**
 * Converter espec�fico para sele��o com {@link Doenca}, onde o primeiro item selecionado � vazio
 * 
 * @author Raphael.Ferreira
 */
@FacesConverter(value = "doencaConverter")
public class DoencaConverter implements Converter {

	private static final Map<Doenca, String> DOENCAS = new HashMap<Doenca, String>();

	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String stringValor) {

		if ("".equals(stringValor)) {
			return null;
		}
		// Procura pela doenca correspondente ao select item selecionado:
		for (Entry<Doenca, String> entry : DOENCAS.entrySet()) {
			if (entry.getValue().equals(stringValor)) {
				return entry.getKey();
			}
		}

		throw new RuntimeException("Erro inesperado no converter " + getClass().getName()
				+ ": Refer�ncia para uma entidade foi perdida.");
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object doenca) {
		// Se o objeto selecionado for o valor vazio retorna
		// a string contendo a descri��o.
		if ("".equals(doenca.toString())) {
			return "";
		}

		synchronized (DOENCAS) {
			if (!DOENCAS.containsKey(doenca)) {
				DOENCAS.put((Doenca) doenca, ((Doenca) doenca).getId().toString());
				return ((Doenca) doenca).getId().toString();
			}
			return DOENCAS.get(doenca);
		}
	}
}
