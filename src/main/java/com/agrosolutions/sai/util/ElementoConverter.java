package com.agrosolutions.sai.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import com.agrosolutions.sai.model.Elemento;

/**
 * Converter espec�fico para sele��o com {@link Elemento}, onde o primeiro item selecionado � vazio
 * 
 * @author Raphael.Ferreira
 */
@FacesConverter(value = "elementoConverter")
public class ElementoConverter implements Converter {

	private static final Map<Elemento, String> ELEMENTOS = new HashMap<Elemento, String>();

	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String stringValor) {

		if ("".equals(stringValor)) {
			return null;
		}
		// Procura pelo elemento correspondente ao select item selecionado:
		for (Entry<Elemento, String> entry : ELEMENTOS.entrySet()) {
			if (entry.getValue().equals(stringValor)) {
				return entry.getKey();
			}
		}

		throw new RuntimeException("Erro inesperado no converter " + getClass().getName()
				+ ": Refer�ncia para uma entidade foi perdida.");
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object elemento) {
		// Se o objeto selecionado for o valor vazio retorna
		// a string contendo a descri��o.
		if ("".equals(elemento.toString())) {
			return "";
		}

		// Adiciona as demais elementos, que s�o Elemento ao Map de ELEMENTOS, id
		// do mesmo.
		synchronized (ELEMENTOS) {
			if (!ELEMENTOS.containsKey(elemento)) {
				ELEMENTOS.put((Elemento) elemento, ((Elemento) elemento).getId().toString());
				return ((Elemento) elemento).getId().toString();
			}
			return ELEMENTOS.get(elemento);
		}
	}
}
