package com.agrosolutions.sai.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import com.agrosolutions.sai.model.Referencia;

/**
 * Converter espec�fico para sele��o com {@link Referencia}, onde o primeiro item selecionado � vazio
 * 
 * @author Waldemar.Junior
 */
@FacesConverter(value = "referenciaConverter")
public class ReferenciaConverter implements Converter {

	private static final Map<Referencia, String> REFERENCIAS = new HashMap<Referencia, String>();

	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String stringValor) {

		if ("".equals(stringValor)) {
			return null;
		}
		// Procura pela referencia correspondente ao select item selecionado:
		for (Entry<Referencia, String> entry : REFERENCIAS.entrySet()) {
			if (entry.getValue().equals(stringValor)) {
				return entry.getKey();
			}
		}

		throw new RuntimeException("Erro inesperado no converter " + getClass().getName()
				+ ": Refer�ncia para uma entidade foi perdida.");
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object referencia) {
		// Se o objeto selecionado for o valor vazio retorna
		// a string contendo a descri��o.
		if ("".equals(referencia.toString())) {
			return "";
		}

		synchronized (REFERENCIAS) {
			if (REFERENCIAS.containsKey(referencia)) {
				REFERENCIAS.remove(referencia);
			}
			REFERENCIAS.put((Referencia) referencia, ((Referencia) referencia).getId().toString());
			return REFERENCIAS.get(referencia);
		}
	}
}
