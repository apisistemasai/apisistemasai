package com.agrosolutions.sai.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.agrosolutions.sai.service.ParametroService;


public class ImageServlet extends HttpServlet {

	private static final long serialVersionUID = -915876148772932301L;
	final String caminho = System.getProperty("imagem_sai");
	ParametroService parametroService;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String[] arquivo = request.getRequestURI().split("/");
		String[] tipo = arquivo[arquivo.length - 1].split("\\.");
		if (arquivo[arquivo.length - 2].equals("publico")) {
			response.setContentType("image/jpeg");
			if (tipo[tipo.length - 1].toLowerCase().equals("gif")) {
				response.setContentType("image/gif");
			}
			if (tipo[tipo.length - 1].toLowerCase().equals("png")) {
				response.setContentType("image/png");
			}
			if (tipo[tipo.length - 1].toLowerCase().equals("swf")) {
				response.setContentType("application/x-shockwave-flash");  
			}
		} else {
			if (tipo[tipo.length - 1].toLowerCase().equals("doc") || tipo[tipo.length - 1].toLowerCase().equals("docx")) {
				response.setContentType("application/msword");
			}
			if (tipo[tipo.length - 1].toLowerCase().equals("pdf")) {
				response.setContentType("application/pdf");
			}
			if (tipo[tipo.length - 1].toLowerCase().equals("xls") || tipo[tipo.length - 1].toLowerCase().equals("xlsx")) {
				response.setContentType("application/excel");
			}
		}
		response.setHeader("Content-disposition", "attachment;filename=" + arquivo[arquivo.length - 1].replace("%20", " ")
				+ ";creation-date=" + new Date());
		
		try {
			FileInputStream inputStream;
			File file = new File(caminho + File.separator + arquivo[arquivo.length - 1].replace("%20", " "));
			if (file.exists()) {
				inputStream = new FileInputStream(file);
			}else{
				inputStream = new FileInputStream(new File(caminho + File.separator + "semImagem.jpg"));
			}
			OutputStream output = response.getOutputStream();
			byte[] bytes = new byte[1024];
			int bytesLidos = 0;

			while ((bytesLidos = inputStream.read(bytes)) != -1) {
				output.write(bytes, 0, bytesLidos);
				output.flush();
			}
			inputStream.close();
		} catch (Exception e) {
			System.out.println("Erro no servlet:");
			e.printStackTrace();
		}
	}
}
