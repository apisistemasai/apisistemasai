package com.agrosolutions.sai.util;

public class ResponseJson {

    private Boolean success;
    private String message;
    private Object object;

    public ResponseJson(Boolean success, String message, Object object) {
        this.success = success;
        this.message = message;
        this.object = object;
    }

    public Boolean getCod() {
        return success;
    }

    public void setCod(Boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return object;
    }

    public void setData(String object) {
        this.object = object;
    }
}
