//package com.agrosolutions.sai.util;
//
//import javax.servlet.ServletContextEvent;
//import javax.servlet.ServletContextListener;
//
//import org.apache.log4j.Logger;
//import org.quartz.CronTrigger;
//import org.quartz.JobDetail;
//import org.quartz.Scheduler;
//import org.quartz.impl.StdSchedulerFactory;
//
///**
//* Classe Servlet Listener
//*/
//public class AgendadorServicoQuartz implements ServletContextListener {
//	public static Logger log = Logger.getLogger(AgendadorServicoQuartz.class);
//	public static final String SMS_JOB = "emitirSMSTempoIrrigacao";
//	public static final String EMAIL_JOB = "emitirEmailTempoIrrigacao";
//	/**
//	* M�todo executado quando se inicia a aplica��o.
//	*/
//	public void contextInitialized(ServletContextEvent event) {
//		//Chama o executor do agendamento.
//		agendar();
//	}
//	/**
//	* M�todo executado quando a aplica��o � finalizada.
//	*/
//	public void contextDestroyed(ServletContextEvent event) {
//		try {
//			Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
//			// Remove o agendamento.
//			scheduler.removeJobListener(SMS_JOB);
//			scheduler.removeJobListener(EMAIL_JOB);
//		}
//		catch (Exception e) {
//			log.error("Ocorreu um erro ao remover agendamento do Quartz.", e);
//		}
//	}
//
//	/**
//	* M�todo que realiza o agendamento
//	*/
//	public void agendar(){
//		try {
//			// Obtendo uma inst�ncia de agendamento.
//			Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
//			if (scheduler.getJobDetail(SMS_JOB, Scheduler.DEFAULT_GROUP) == null) {
//				//
//				// Criando um novo job
//				JobDetail jobDetail = new JobDetail(SMS_JOB, Scheduler.DEFAULT_GROUP,
//				EnviarSMSTI.class);
//				// Criando �Trigger� (usando Cron) associado ao Job e Scheduler
//				CronTrigger trigger = new CronTrigger(SMS_JOB, Scheduler.DEFAULT_GROUP);
//				// Parametros Cron(? ? ? ? * *): Segundos, Minutos, Hora, Dia do M�s,
//				// M�s, Dia_da_Semana e Ano (opcional)
//				trigger.setCronExpression("0 00 17 * * ?"); // Agendado pra todas as seg. - 12h
//				// Adicionando �job� ao agendador de servi�os.
//				scheduler.scheduleJob(jobDetail, trigger);
//			}
//		}
//		catch (Exception e) {
//			log.error("Ocorreu um erro ao agendar ["+ SMS_JOB + "] no Quartz.", e);
//		}
//		try {
//			// Obtendo uma inst�ncia de agendamento.
//			Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
//			if (scheduler.getJobDetail(EMAIL_JOB, Scheduler.DEFAULT_GROUP) == null) {
//				//
//				// Criando um novo job
//				JobDetail jobDetail = new JobDetail(EMAIL_JOB, Scheduler.DEFAULT_GROUP,
//				EnviarEmailTI.class);
//				// Criando �Trigger� (usando Cron) associado ao Job e Scheduler
//				CronTrigger trigger = new CronTrigger(EMAIL_JOB, Scheduler.DEFAULT_GROUP);
//				// Parametros Cron(? ? ? ? * *): Segundos, Minutos, Hora, Dia do M�s,
//				// M�s, Dia_da_Semana e Ano (opcional)
//				trigger.setCronExpression("0 00 18 * * ?"); // Agendado pra todas as seg. - 12h
//				// Adicionando �job� ao agendador de servi�os.
//				scheduler.scheduleJob(jobDetail, trigger);
//			}
//		}
//		catch (Exception e) {
//			log.error("Ocorreu um erro ao agendar ["+ EMAIL_JOB + "] no Quartz.", e);
//		}
//
//	}
//}