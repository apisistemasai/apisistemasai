package com.agrosolutions.sai.util;

import java.util.Iterator;

import javax.faces.application.FacesMessage;
import javax.faces.context.ExceptionHandler;
import javax.faces.context.ExceptionHandlerWrapper;
import javax.faces.context.FacesContext;
import javax.faces.event.ExceptionQueuedEvent;
import javax.faces.event.ExceptionQueuedEventContext;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.orm.hibernate3.HibernateOptimisticLockingFailureException;

import com.agrosolutions.sai.exception.SaiException;


public class SaiExceptionHandler extends ExceptionHandlerWrapper {

	private final ExceptionHandler wrapped;

	public SaiExceptionHandler(ExceptionHandler wrapped) {
		this.wrapped = wrapped;
	}

	@Override
	public javax.faces.context.ExceptionHandler getWrapped() {
		return wrapped;
	}

	@Override
	public void handle() {
		Iterator<?> eventIt = getUnhandledExceptionQueuedEvents().iterator();

		// Iterate through the queued exceptions
		while (eventIt.hasNext()) {
			ExceptionQueuedEvent event = (ExceptionQueuedEvent) eventIt.next();
			ExceptionQueuedEventContext context = (ExceptionQueuedEventContext) event.getSource();
			Throwable ex = context.getException();
			ex.printStackTrace();

			// See if it's an exception I'm interested in
			try {
				if (getRootCause(ex) != null && getRootCause(ex).getCause() != null) {
					if (getRootCause(ex).getCause() instanceof SaiException) {
						String erro = "";
						// Pega o erro do array de mensagens.
						if(((SaiException) getRootCause(ex).getCause()).getMessages()!=null){
							for (String msg : ((SaiException) getRootCause(ex).getCause()).getMessages()) {
								erro += msg + " \n ";
							}
						}
						// Pega o erro da mensagem unica.
						if (((SaiException) getRootCause(ex).getCause()).getMessage() != null) {
							erro += ((SaiException) getRootCause(ex).getCause()).getMessage();
						}
						addErrorMessage(erro);
						
					} else if (getRootCause(ex).getCause() instanceof DataIntegrityViolationException) {
						if (ex.getLocalizedMessage().contains("cadastrarUsuarioBean.gravar") || ex.getLocalizedMessage().contains("cadastrarTecnicoBean.gravar") || ex.getLocalizedMessage().contains("cadastrarIrriganteBean.gravar")) {
							addErrorMessage(ResourceBundle.getMessage("jaExisteUsuario"));
						}else if (ex.getLocalizedMessage().contains("cadastrarVariedadeBean.gravar") || 
								ex.getLocalizedMessage().contains("cadastrarCulturaBean.gravar")|| 
								ex.getLocalizedMessage().contains("cadastrarPragaBean.gravar")|| 
								ex.getLocalizedMessage().contains("cadastrarAtividadeBean.gravar")|| 
								ex.getLocalizedMessage().contains("cadastrarAduboBean.gravar")|| 
								ex.getLocalizedMessage().contains("cadastrarFabricanteBean.gravar")|| 
								ex.getLocalizedMessage().contains("cadastrarDefensivoBean.gravar")) {
							addErrorMessage(ResourceBundle.getMessage("jaExisteRegistro"));
						}else if (ex.getLocalizedMessage().contains("cadastrarEmissorBean.gravar") || 
								ex.getLocalizedMessage().contains("cadastrarBombaBean.gravar")){
							addErrorMessage(ResourceBundle.getMessage("jaExisteModelo"));
						}else {
							addErrorMessage(ResourceBundle.getMessage("problemaBD"));
						}
					} else if (getRootCause(ex).getCause() instanceof HibernateOptimisticLockingFailureException) {
						addErrorMessage(ResourceBundle.getMessage("outroUsusarioAlterou"));
					} else if (getRootCause(ex).getCause() instanceof EmptyResultDataAccessException) {
						addAvisoMessage(ResourceBundle.getMessage("semResultadoPesquisa"));
					}
				}
				if (!hasMessages()) {
					addErrorMessage(ResourceBundle.getMessage("ocorreuErro"));
				}
			} finally {
				// remove it if you processed it
				eventIt.remove();
			}
		}
		getWrapped().handle();
	}

	private boolean hasMessages() {
		return !FacesContext.getCurrentInstance().getMessageList().isEmpty();
	}

	private void addErrorMessage(String message) {
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, message, null ));
	}
	private void addAvisoMessage(String message) {
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, message , null));
	}
	
}