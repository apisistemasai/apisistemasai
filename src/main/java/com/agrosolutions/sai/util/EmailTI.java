//package com.agrosolutions.sai.util;
//
//import java.io.File;
//import java.io.FileOutputStream;
//import java.io.InputStream;
//import java.util.ArrayList;
//import java.util.Calendar;
//import java.util.Date;
//import java.util.GregorianCalendar;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//import java.util.Properties;
//import java.util.Set;
//
//import javax.activation.DataHandler;
//import javax.activation.DataSource;
//import javax.activation.FileDataSource;
//import javax.mail.Message;
//import javax.mail.Part;
//import javax.mail.Session;
//import javax.mail.Transport;
//import javax.mail.internet.InternetAddress;
//import javax.mail.internet.MimeBodyPart;
//import javax.mail.internet.MimeMessage;
//import javax.mail.internet.MimeMultipart;
//import javax.persistence.EntityManager;
//import javax.persistence.EntityManagerFactory;
//import javax.persistence.NoResultException;
//import javax.persistence.Persistence;
//import javax.persistence.Query;
//
//import org.apache.commons.collections.map.HashedMap;
//
//import com.agrosolutions.sai.exception.SaiException;
//import com.agrosolutions.sai.model.Acesso;
//import com.agrosolutions.sai.model.Irrigacao;
//import com.agrosolutions.sai.model.Irrigante;
//import com.agrosolutions.sai.model.Mensagem;
//import com.agrosolutions.sai.model.Parametro;
//import com.agrosolutions.sai.model.TipoMensagem;
//
//public class EmailTI {
//	private static final String MSG_ERRO_INESPERADO = "Ocorreu um erro inesperado ao enviar e-mail. Por favor, entre em contato com o suporte t�cnico.";
//	private static EntityManagerFactory entityManagerFactory;
//	private String emailOrigem;
//	private String nomeUsuarioOrigem;
//	private String senhaUsuarioOrigem;
//	private String smtpHost;
//	private int smtpPort;
//	private Properties configuracaoEmail;
//
//	public EmailTI() {
//		List<Irrigacao> irrigacoes = pesquisarPorData();
//		List<Irrigacao> irrigacoesIrrigante = null;
//		String emailLido = "";
//		String email = "";
//		Long idIrriganteLido = null;
//		Long idIrrigante = 0L;
//		Long idVariedadeLido = null;
//		Long idVariedade = 0L;
//		String msg = "";
//		int qtdMsg=0;
//		Irrigante irrigante = null;
//		EntityManager em = getEntityManager();
//		String tempo = "";
//		String tempoLido = "";
//		boolean temIrrigacao = false;
//		boolean primeiroSetor = true;
//		
//		em.getTransaction().begin();
//		//Percorrendo as irriga��es que est�o em ordem de telefone, seguido pelo lote, tempo de irrigacao, variedade e setor
//		for (Irrigacao i : irrigacoes) {
//			//Iniciando variaveis de checagem
//			emailLido = i.getPlantio().getSetor().getIrrigante().getUsuario().getEmail();
//			tempoLido = i.getTempo();
//			if (!tempoLido.equals("N�o irrigar")) {
//				temIrrigacao = true;
//			}			
//			idIrriganteLido = i.getPlantio().getSetor().getIrrigante().getId();
//			idVariedadeLido = i.getPlantio().getCultivo().getVariedade().getId();
//			
//			//Se n�o tiver celular, ou se n�o for para enviarSMS ou se o tempo for superior a 240min, n�o gerar mensagem. 
//			if (emailLido.isEmpty() || !i.getEnviarEmail() || !i.getTemIrrigacao()) {
//				//Ignorar essa irrigacao e ler o proximo.
//				continue;
//			}
//			
//			//Se mudar o celular
//			if (!email.equals(emailLido)) {
//				//limpar o tempo
//				tempo = "";
//				//Se ja tiver mensagem preparada deve enviar ela e preparar um nova.
//				if (!msg.isEmpty()) {
//					//Se n�o tiver irriga��o, terminar de montar a mensagem.
//					if (!temIrrigacao) {
//					 	Acesso acesso = null;
//					 	try {
//							acesso = pesquisarUltimoAcesso(i.getPlantio().getSetor().getIrrigante().getUsuario().getUsername());
//						} catch (NoResultException e) {
//						}
//						String msgAcesso = "";
//						if (acesso != null ) {
//							Calendar data = new GregorianCalendar();
//							data.setTime(acesso.getData());
//							msgAcesso = "Seu �ltimo acesso foi em:" + data.get(Calendar.DAY_OF_MONTH) + "/" + (data.get(Calendar.MONTH)+1) + "/" + data.get(Calendar.YEAR) + " as " + data.get(Calendar.HOUR_OF_DAY) + ":" + data.get(Calendar.MINUTE);
//						} else {
//							msgAcesso = "Voc� ainda n�o visitou sua p�gina.";
//						}
//						
//						msg = msg + " n�o precisa irrigar. " + msgAcesso + " \n";
//					}
//					qtdMsg++;
//					enviar(msg, email, irrigante, em, irrigacoesIrrigante);
//				}
//			   	//email = "rpf1404@gmail.com";
//				//Pegando celular Lido para depois verificar se mudou.
//				email = emailLido;
//				//Mensagem inicial
//				msg = "SAI-INFORMA:";
//				//Iniciar lista com as irriga��es desse irrigante.
//				irrigacoesIrrigante = new ArrayList<Irrigacao>();
//
//			}
//			
//			//Adicionando irriga��o para depois alterar o indicador que o sms foi enviado na hora de enviar a mensagem.
//			irrigacoesIrrigante.add(i);
//			//Verificar se mudou o lote
//			if (idIrrigante != idIrriganteLido) {
//				//Pegando o irrigante em quest�o para gravar na hora de enviar a mensagem
//				irrigante = i.getPlantio().getSetor().getIrrigante();
//				idVariedade = 0L;
//				idIrrigante = irrigante.getId();
//				if (temIrrigacao) {
//					msg = msg + "\nLote:" + (i.getPlantio().getSetor().getIrrigante().getLocalizacao().length()>6?i.getPlantio().getSetor().getIrrigante().getLocalizacao().substring(0,6):i.getPlantio().getSetor().getIrrigante().getLocalizacao());
//				}else{
//					msg = msg + i.getPlantio().getSetor().getIrrigante().getLocalizacao()+ ",";
//				}
//			}
//			//Verifica se mudou o tempo
//			if (temIrrigacao && !tempo.equals(tempoLido)){
//				tempo = tempoLido;
//				idVariedade = 0L;
//				msg = msg + "\n" + tempo;
//			}
//
//			if (temIrrigacao) {
//				if (idVariedade != idVariedadeLido) {
//					primeiroSetor = true;
//					idVariedade = idVariedadeLido;
//					msg = msg + "\n" + " " + (i.getPlantio().getCultivo().getCultura().getNome().split(" "))[0] + " " + (i.getPlantio().getCultivo().getVariedade().getNome().split(" "))[0] + ":" ;
//				}
//				if(primeiroSetor){
//					primeiroSetor = false;
//					msg = msg + "S-" + i.getPlantio().getSetor().getIdentificacao();
//				}else{
//					msg = msg + "," + i.getPlantio().getSetor().getIdentificacao();
//				}
//			}
//		}
//		qtdMsg++;
//		System.out.println("Total email = " + qtdMsg);
//		if(!msg.isEmpty()){
//			enviar(msg, email, irrigante, em, irrigacoesIrrigante);
//			irrigacoesIrrigante = new ArrayList<Irrigacao>();
//		}
//		em.getTransaction().commit();
//		em.close();
//	}
//	@SuppressWarnings("unused")
//	private boolean mesmaCultura(Irrigacao i, List<Irrigacao> lista){
//		int x = 0;
//		for (Irrigacao ir : lista) {
//			if (ir.getPlantio().getSetor().equals(i.getPlantio().getSetor()) && ir.getPlantio().getCultivo().getVariedade().equals(i.getPlantio().getCultivo().getVariedade())) {
//				x++;
//			}
//		}
//		if (x > 1) {
//			return true;  
//		}else{
//			return false;  
//		}
//	}
//
//	private void enviar(String msg, String dest, Irrigante i, EntityManager em, List<Irrigacao> irrigacoesIrrigante) {
//		
//		System.out.println("Email Enviado para:" + dest + " - Tamanho msg:" + msg.length());
//		System.out.println(msg);
//		
//		List<String> destinatarios = new ArrayList<String>();
//		destinatarios.add(dest);
//		enviarEmail(destinatarios,"S@I - Tempo Irriga��o do dia", msg, null);
//		//Gerar Mensagem
//		List<Irrigante> irrigantes = new ArrayList<Irrigante>();
//		irrigantes.add(i);
//		Mensagem m = new Mensagem(TipoMensagem.Email, "TI", msg, null, new Date(), null, irrigantes, i.getArea().getDistrito());
//		salvarMensagem(m, em);
//		//Atualizar status das irriga��es do irrigante
//		for (Irrigacao irr : irrigacoesIrrigante) {
//			irr.setEmailEnviado(true);
//			salvarIrrigacao(irr, em);
//		}
//		
//	}
//
//
//	static {
//		entityManagerFactory = Persistence.createEntityManagerFactory("maindatabase");
//	}
//	
//	public EntityManager getEntityManager() {
//		return entityManagerFactory.createEntityManager();
//	}
//
//	@SuppressWarnings("unchecked")
//	public List<Irrigacao> pesquisarPorData() {
//	 	Map<String, Object> parametros = new HashMap<String, Object>();
//	 	parametros.put("pData", new Date());
//	 	Query queryNamed = executarPesquisa("Irrigacao.EmailTI", parametros);
//		return queryNamed.getResultList();
//	}
//
//	private Query executarPesquisa(String query,
//		Map<String, Object> parametros) {
//		Query queryNamed = getEntityManager().createNamedQuery(query);
//		if(parametros!=null){
//			Set<String> keyParametros = parametros.keySet();
//			for (String nomeParamentro : keyParametros) {
//				queryNamed.setParameter(nomeParamentro, parametros.get(nomeParamentro));
//			}
//		}
//		return queryNamed;
//	}	
//	
//	public Acesso pesquisarUltimoAcesso(String userName) {
//		@SuppressWarnings("unchecked")
//		Map<String, Object> parametros = new HashedMap();
//	 	parametros.put("pUsuario", userName);
//		Query queryNamed = executarPesquisa("Acesso.ultimoPorUsuario", parametros);
//		return (Acesso) queryNamed.getSingleResult();
//	}	
//	
//	public Parametro pesquisarPorCodigo(String codigo) {
//		@SuppressWarnings("unchecked")
//		Map<String, Object> parametros = new HashedMap();
//		parametros.put("pCodigo", codigo);
//
//		Query queryNamed = executarPesquisa("Parametro.porCodigo", parametros);
//		return (Parametro) queryNamed.getSingleResult();
//	}	
//
//	public Irrigacao salvarIrrigacao(Irrigacao entidade, EntityManager em) {
//		entidade = em.merge(entidade);
//		return entidade;
//	}
//
//	public Mensagem salvarMensagem(Mensagem entidade, EntityManager em) {
//		em.persist(entidade);
//		return entidade;
//	}
//	
//	private void configurarEmail() throws SaiException {
//		Parametro parametro;
//		if (configuracaoEmail == null) {
//			
//			parametro = pesquisarPorCodigo("emailOrigem");
//			emailOrigem = parametro.getValor();
//
//			parametro = pesquisarPorCodigo("nomeUsuarioOrigem");
//			nomeUsuarioOrigem = parametro.getValor();
//
//			parametro = pesquisarPorCodigo("senhaUsuarioOrigem");
//			senhaUsuarioOrigem = parametro.getValor();
//
//			parametro = pesquisarPorCodigo("smtpHost");
//			smtpHost  = parametro.getValor();
//
//			parametro = pesquisarPorCodigo("smtpPort");
//			smtpPort  = Integer.parseInt(parametro.getValor());
//			
//			Properties lProperties = new Properties();
//			lProperties.put("mail.smtp.host", smtpHost);
//			lProperties.put("mail.smtp.port", smtpPort);
//			lProperties.put("mail.debug", "true");
//			if (senhaUsuarioOrigem != null){ 
//				lProperties.put("mail.smtp.auth", "true");
//			}else{
//				lProperties.put("mail.smtp.auth", "false");
//			}
//			if (smtpHost.equals("smtp.gmail.com")) {
//				lProperties.put("mail.smtp.starttls.enable", "true");
//				lProperties.put("mail.smtp.socketFactory.port", "465");
//				lProperties.put("mail.smtp.socketFactory.fallback", "false");
//				lProperties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
//			}
//
//			configuracaoEmail = lProperties;
//		}
//	}
//
//	private InternetAddress[] criarListaInternetAddress(List<String> destinatarios) throws Exception {
//		InternetAddress[] lListaAddress = new InternetAddress[destinatarios.size()];
//		for (int i = 0; i < destinatarios.size(); i++) {
//			lListaAddress[i] = new InternetAddress(destinatarios.get(i));
//		}
//		return lListaAddress;
//	}
//
//	public void enviarEmail(List<String> destinatarios, String assunto, String conteudo, Map<String, InputStream> anexos) throws SaiException {
//		configurarEmail();
//		try {
//			// E-mail de Origem:
//			InternetAddress lAddressOrigem = new InternetAddress(emailOrigem, nomeUsuarioOrigem);
//			// E-mail(s) de Destino:
//			InternetAddress[] lListaAddressDestino = criarListaInternetAddress(destinatarios);
//
//			Session lSession = Session.getInstance(configuracaoEmail, null);
//			MimeMessage lMineMessage = new MimeMessage(lSession);
//			lMineMessage.setSentDate(new Date());
//			lMineMessage.setFrom(lAddressOrigem);
//			lMineMessage.addRecipients(Message.RecipientType.BCC, lListaAddressDestino);
//
//			// Assunto do e-mail:
//			lMineMessage.setSubject(assunto);
//			java.util.Properties properties = System.getProperties();
//			properties.setProperty("file.encoding", "ISO-8859-1");
//			lMineMessage.setHeader("Content-Type", "text/plain; charset=iso-8859-1");
//			
//			// add handlers for main MIME types
//			if (anexos == null || anexos.isEmpty()) {
//				// Conte�do do e-mail:
//				lMineMessage.setContent(conteudo, "text/html");
//								
//			} else {
//				MimeMultipart lMultiPartRoot = new MimeMultipart("mixed");
//
//				// Cria o BodyPart do conte�do do e-mail:
//				MimeBodyPart lBodyPartCorpo = new MimeBodyPart();
//				lBodyPartCorpo.setContent(conteudo, "text/html");
//				lMultiPartRoot.addBodyPart(lBodyPartCorpo);
//
//				// Cria os BodyParts para os anexos do e-mail:
//				for (String nomeArquivo : anexos.keySet()) {
//					// Cria um arquivo tempor�rio para o anexo:
//					File lArquivoTemporario = criarArquivoTemporario(anexos.get(nomeArquivo));
//					DataSource lFileDataSource = new FileDataSource(lArquivoTemporario);
//					// Cria o BodyPart do anexo:
//					MimeBodyPart lBodyPartAnexo = new MimeBodyPart();
//					lBodyPartAnexo.setDisposition(Part.ATTACHMENT);
//					lBodyPartAnexo.setDataHandler(new DataHandler(lFileDataSource));
//					lBodyPartAnexo.setFileName(nomeArquivo);
//					lMultiPartRoot.addBodyPart(lBodyPartAnexo);
//				}
//				lMineMessage.setContent(lMultiPartRoot);	
//			}
//			lMineMessage.saveChanges();
//
//			// Conecta-se ao servidor e envia o e-mail:
//			Transport lTranportEnvio = lSession.getTransport("smtp");
//			lTranportEnvio.connect(smtpHost, smtpPort, emailOrigem, senhaUsuarioOrigem);
//			lTranportEnvio.sendMessage(lMineMessage, lMineMessage.getAllRecipients());
//			lTranportEnvio.close();
//		} catch (Exception pEx) {
//			throw new SaiException(MSG_ERRO_INESPERADO, pEx);
//		}
//	}
//
//	/**
//	 * Cria um arquivo tempor�rio a partir do inputStream
//	 * 
//	 * @param inputStream
//	 * @return Arquivo tempor�rio.
//	 * @throws Exception
//	 */
//	private File criarArquivoTemporario(InputStream inputStream) throws Exception {
//		File arquivoTemporario = File.createTempFile("sai", null);
//		arquivoTemporario.deleteOnExit();
//		FileOutputStream lFos = new FileOutputStream(arquivoTemporario);
//		int readByte;
//		while ((readByte = inputStream.read()) != -1) {
//			lFos.write(readByte);
//		}
//		lFos.close();
//		return arquivoTemporario;
//	}
//
//}
