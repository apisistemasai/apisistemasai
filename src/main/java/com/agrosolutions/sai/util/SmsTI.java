//package com.agrosolutions.sai.util;
//import java.io.IOException;
//import java.io.InputStream;
//import java.io.StringWriter;
//import java.io.UnsupportedEncodingException;
//import java.net.MalformedURLException;
//import java.net.URL;
//import java.net.URLEncoder;
//import java.util.ArrayList;
//import java.util.Calendar;
//import java.util.Date;
//import java.util.GregorianCalendar;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//import java.util.Set;
//
//import javax.persistence.EntityManager;
//import javax.persistence.EntityManagerFactory;
//import javax.persistence.NoResultException;
//import javax.persistence.Persistence;
//import javax.persistence.Query;
//
//import org.apache.commons.io.IOUtils;
//
//import com.agrosolutions.sai.model.Acesso;
//import com.agrosolutions.sai.model.Irrigacao;
//import com.agrosolutions.sai.model.Irrigante;
//import com.agrosolutions.sai.model.Mensagem;
//import com.agrosolutions.sai.model.TipoMensagem;
//
//public class SmsTI {
//	private static EntityManagerFactory entityManagerFactory;
//	public static final Integer MAX_CARACTERES = 140;
//
//	public SmsTI() {
//	 	Map<String, Object> parametros = new HashMap<String, Object>();
//	 	parametros.put("pData", new Date());
//	
//		List<Irrigacao> irrigacoes = pesquisarPorData("Irrigacao.SMSTI", parametros);
//		List<Irrigacao> irrigacoesIrrigante = null;
//		String celularLido = "";
//		String celular = "";
//		Long idIrriganteLido = null;
//		Long idIrrigante = 0L;
//		Long idVariedadeLido = null;
//		Long idVariedade = 0L;
//		String msg = "";
//		int qtdMsg=0;
//		Irrigante irrigante = null;
//		EntityManager em = getEntityManager();
//		String tempo = "";
//		String tempoLido = "";
//		boolean temIrrigacao = false;
//		boolean primeiroSetor = true;
//		
//		em.getTransaction().begin();
//		//Percorrendo as irriga��es que est�o em ordem de telefone, seguido pelo lote, tempo de irrigacao, variedade e setor
//		for (Irrigacao i : irrigacoes) {
//			//Iniciando variaveis de checagem
//			celularLido = i.getPlantio().getSetor().getIrrigante().getCelular().replace("(", "").replace(")", "").replace("-", "");
//			tempoLido = i.getTempo();
//			if (!tempoLido.equals("N�o irrigar")) {
//				temIrrigacao = true;
//			}			
//			idIrriganteLido = i.getPlantio().getSetor().getIrrigante().getId();
//			idVariedadeLido = i.getPlantio().getCultivo().getVariedade().getId();
//			
//			//Se n�o tiver celular, ou se n�o for para enviarSMS ou se o tempo for superior a 240min, n�o gerar mensagem. 
//			if (celularLido.isEmpty() || !i.getEnviarSms() || !i.getTemIrrigacao()) {
//				//Ignorar essa irrigacao e ler o proximo.
//				continue;
//			}
//			
//			//Se mudar o celular
//			if (!celular.equals(celularLido)) {
//				//limpar o tempo
//				tempo = "";
//				//Se ja tiver mensagem preparada deve enviar ela e preparar um nova.
//				if (!msg.isEmpty()) {
//					//Se n�o tiver irriga��o, terminar de montar a mensagem.
//					if (!temIrrigacao) {
//					 	parametros.clear();
//					 	parametros.put("pUsuario", i.getPlantio().getSetor().getIrrigante().getUsuario().getUsername());
//					 	Acesso acesso = null;
//					 	try {
//							acesso = pesquisarUltimoAcesso("Acesso.ultimoPorUsuario", parametros);
//						} catch (NoResultException e) {
//						}
//						String msgAcesso = "";
//						if (acesso != null ) {
//							Calendar data = new GregorianCalendar();
//							data.setTime(acesso.getData());
//							msgAcesso = "Seu �ltimo acesso foi em:" + data.get(Calendar.DAY_OF_MONTH) + "/" + (data.get(Calendar.MONTH)+1) + "/" + data.get(Calendar.YEAR) + " as " + data.get(Calendar.HOUR_OF_DAY) + ":" + data.get(Calendar.MINUTE);
//						} else {
//							msgAcesso = "Voc� ainda n�o visitou sua p�gina.";
//						}
//						if ((msg + " n�o precisa irrigar. " + msgAcesso + " \n").length() > MAX_CARACTERES) {
//							msg = msg + " n�o precisa irrigar. \n";
//						}else{
//							msg = msg + " n�o precisa irrigar. " + msgAcesso + " \n";
//						}
//					}
//					qtdMsg++;
//					enviar(msg, celular, irrigante, em, irrigacoesIrrigante);
//				}
//			   	//tel = "558588186358";
//				//Pegando celular Lido para depois verificar se mudou.
//				celular = celularLido;
//				//Mensagem inicial
//				msg = "SAI-INFORMA:";
//				//Iniciar lista com as irriga��es desse irrigante.
//				irrigacoesIrrigante = new ArrayList<Irrigacao>();
//
//			}
//			
//			//Adicionando irriga��o para depois alterar o indicador que o sms foi enviado na hora de enviar a mensagem.
//			irrigacoesIrrigante.add(i);
//			//Verificar se mudou o lote
//			if (idIrrigante != idIrriganteLido) {
//				//Pegando o irrigante em quest�o para gravar na hora de enviar a mensagem
//				irrigante = i.getPlantio().getSetor().getIrrigante();
//				idVariedade = 0L;
//				idIrrigante = irrigante.getId();
//				if (temIrrigacao) {
//					//Se o tamanho da mensagem for ultrapassar os maxCaracteres caracteres ent�o devemos enviar a msg
//					if ((msg + "\nLote:" + (i.getPlantio().getSetor().getIrrigante().getLocalizacao().length()>6?i.getPlantio().getSetor().getIrrigante().getLocalizacao().substring(0,6):i.getPlantio().getSetor().getIrrigante().getLocalizacao())).length() > MAX_CARACTERES) {
//						qtdMsg++;
//						enviar(msg, celular, irrigante, em, irrigacoesIrrigante);
//						irrigacoesIrrigante = new ArrayList<Irrigacao>();
//						msg = "SAI-INFORMA(cont.):";
//					}
//					//Acrescenta na mensagem o lote
//					msg = msg + "\nLote:" + (i.getPlantio().getSetor().getIrrigante().getLocalizacao().length()>6?i.getPlantio().getSetor().getIrrigante().getLocalizacao().substring(0,6):i.getPlantio().getSetor().getIrrigante().getLocalizacao());
//				}else{
//					msg = msg + i.getPlantio().getSetor().getIrrigante().getLocalizacao()+ ",";
//				}
//			}
//			//Verifica se mudou o tempo
//			if (temIrrigacao && !tempo.equals(tempoLido)){
//				tempo = tempoLido;
//				idVariedade = 0L;
//				if ((msg  + "\n" + tempo).length() > MAX_CARACTERES) {
//					qtdMsg++;
//					enviar(msg, celular, irrigante, em, irrigacoesIrrigante);
//					irrigacoesIrrigante = new ArrayList<Irrigacao>();
//					msg = "SAI-INFORMA(cont.):\nLote:" + (i.getPlantio().getSetor().getIrrigante().getLocalizacao().length()>6?i.getPlantio().getSetor().getIrrigante().getLocalizacao().substring(0,6):i.getPlantio().getSetor().getIrrigante().getLocalizacao());
//				}
//				msg = msg + "\n" + tempo;
//			}
//
//			if (temIrrigacao) {
//				if (idVariedade != idVariedadeLido) {
//					primeiroSetor = true;
//					idVariedade = idVariedadeLido;
//					if ((msg + "\n" + " " + trata((i.getPlantio().getCultivo().getCultura().getNome().split(" "))[0] + " " + (i.getPlantio().getCultivo().getVariedade().getNome().split(" "))[0]) + ":" ).length() > MAX_CARACTERES) {
//						qtdMsg++;
//						enviar(msg, celular, irrigante, em, irrigacoesIrrigante);
//						irrigacoesIrrigante = new ArrayList<Irrigacao>();
//						msg = "SAI-INFORMA(cont.):\nLote:" + (i.getPlantio().getSetor().getIrrigante().getLocalizacao().length()>6?i.getPlantio().getSetor().getIrrigante().getLocalizacao().substring(0,6):i.getPlantio().getSetor().getIrrigante().getLocalizacao()+ "\n");
//						msg = msg + tempo;
//					}
//					msg = msg + "\n" + " " + trata((i.getPlantio().getCultivo().getCultura().getNome().split(" "))[0] + " " + (i.getPlantio().getCultivo().getVariedade().getNome().split(" "))[0] + ":") ;
//				}
//				if(primeiroSetor){
//					primeiroSetor = false;
//					if ((msg + "S-" + i.getPlantio().getSetor().getIdentificacao()).length() > MAX_CARACTERES) {
//						qtdMsg++;
//						enviar(msg, celular, irrigante, em, irrigacoesIrrigante);
//						irrigacoesIrrigante = new ArrayList<Irrigacao>();
//						msg = "SAI-INFORMA(cont.):\nLote:" + (i.getPlantio().getSetor().getIrrigante().getLocalizacao().length()>6?i.getPlantio().getSetor().getIrrigante().getLocalizacao().substring(0,6):i.getPlantio().getSetor().getIrrigante().getLocalizacao())+ "\n";
//						msg = msg + tempo + "\n";
//						msg = msg + " " + trata((i.getPlantio().getCultivo().getCultura().getNome().split(" "))[0] + " " + (i.getPlantio().getCultivo().getVariedade().getNome().split(" "))[0] + ":");
//					}
//					msg = msg + "S-" + i.getPlantio().getSetor().getIdentificacao();
//				}else{
//					if ((msg + "," + i.getPlantio().getSetor().getIdentificacao()).length() > MAX_CARACTERES) {
//						qtdMsg++;
//						enviar(msg, celular, irrigante, em, irrigacoesIrrigante);
//						irrigacoesIrrigante = new ArrayList<Irrigacao>();
//						msg = "SAI-INFORMA(cont.):\nLote:" + (i.getPlantio().getSetor().getIrrigante().getLocalizacao().length()>6?i.getPlantio().getSetor().getIrrigante().getLocalizacao().substring(0,6):i.getPlantio().getSetor().getIrrigante().getLocalizacao()) + "\n";
//						msg = msg + tempo + "\n";
//						msg = msg + " " + trata((i.getPlantio().getCultivo().getCultura().getNome().split(" "))[0] + " " + (i.getPlantio().getCultivo().getVariedade().getNome().split(" "))[0] + ":");
//					}
//					msg = msg + "," + i.getPlantio().getSetor().getIdentificacao();
//				}
//			}
//		}
//		qtdMsg++;
//		System.out.println("Total msg = " + qtdMsg);
//		if(!msg.isEmpty()){
//			enviar(msg, celular, irrigante, em, irrigacoesIrrigante);
//			irrigacoesIrrigante = new ArrayList<Irrigacao>();
//		}
//		em.getTransaction().commit();
//		em.close();
//	}
//	
//	@SuppressWarnings("unused")
//	private boolean mesmaCultura(Irrigacao i, List<Irrigacao> lista){
//		int x = 0;
//		for (Irrigacao ir : lista) {
//			if (ir.getPlantio().getSetor().equals(i.getPlantio().getSetor()) && ir.getPlantio().getCultivo().getVariedade().equals(i.getPlantio().getCultivo().getVariedade())) {
//				x++;
//			}
//		}
//		if (x > 1) {
//			return true;  
//		}else{
//			return false;  
//		}
//	}
//
//	@SuppressWarnings("unused")
//	private void enviarOld(String msg, String dest, Irrigante i, EntityManager em, List<Irrigacao> irrigacoesIrrigante) {
//		String user = "rpf1404@gmail.com";
//		String password = "ipqf2908";
//		String codpessoa = "1163";
//		String assinatura = "SaI";
//		String resultMsg = "";
//
//		
//		System.out.println("SMS Enviado para:" + dest + " - Tamanho msg:" + msg.length());
//		System.out.println(msg);
//		
//		try {
//			String msgSMS = URLEncoder.encode(msg, "UTF-8");
//			assinatura = URLEncoder.encode(assinatura, "UTF-8");
//			String connection =
//			"http://web.smscel.com.br/sms/views/getsmsc.do?user="+user +"&password="+ password +"&codpessoa="+ codpessoa +"&to="+ dest
//			        +"&msg="+ msgSMS+"&assinatura="+ assinatura +"&enviarimediato=S&data=&hora=";
//			URL url = new URL(connection);
//			       
//			InputStream input = url.openStream();
//			byte[] b = new byte[800];
//			input.read(b, 0, b.length);
//			String xmlRetorno = new String(b);
//			if (xmlRetorno.length() > 13) {
//				// Passando os dados de XML para Objetos Java
//				String resultado = xmlRetorno.substring((xmlRetorno.lastIndexOf("<nro_retorno>")+ 13), xmlRetorno.lastIndexOf("</nro_retorno>"));
//				resultMsg = xmlRetorno.substring((xmlRetorno.lastIndexOf("<msg_retorno>")+ 13), xmlRetorno.lastIndexOf("</msg_retorno>"));
//				if (resultado.equals("000")) {
//					//Gerar Mensagem
//					List<Irrigante> irrigantes = new ArrayList<Irrigante>();
//					irrigantes.add(i);
//					Mensagem m = new Mensagem(TipoMensagem.SMS, "TI", msg, null, new Date(), null, irrigantes, i.getArea().getDistrito());
//					salvarMensagem(m, em);
//					//Atualizar status das irriga��es do irrigante
//					for (Irrigacao irr : irrigacoesIrrigante) {
//						irr.setSmsEnviado(true);
//						salvarIrrigacao(irr, em);
//					}
//				}
//			}
//			System.out.println("Mensagem Retorno:" + xmlRetorno);
//		} catch (UnsupportedEncodingException e) {
//			System.out.println("Erro ao enviar para:" + dest);
//			System.out.println("Mensagem Erro:" + resultMsg);
//			e.printStackTrace();
//		} catch (MalformedURLException e) {
//			System.out.println("Erro ao enviar para:" + dest);
//			System.out.println("Mensagem Erro:" + resultMsg);
//			e.printStackTrace();
//		} catch (IOException e) {
//			System.out.println("Erro ao enviar para:" + dest);
//			System.out.println("Mensagem Erro:" + resultMsg);
//			e.printStackTrace();
//		} catch (Exception e) {
//			System.out.println("Erro ao enviar para:" + dest);
//			System.out.println("Mensagem Erro:" + resultMsg);
//			e.printStackTrace();
//		}
//	}
//
//	public void enviar(String msg, String dest, Irrigante i, EntityManager em, List<Irrigacao> irrigacoesIrrigante) {
//		String user = "inovagri";
//		String password = "inovagri";
//		String resultMsg = "";
//		try{
//			String connection =
//			"http://api2.infobip.com/api/sendsms/plain?user="+user +"&password="+ password +"&sender=SaI&SMSText="+ msg+"&GSM=55"+ dest;
//			
//			URL url = new URL(connection);
//			       
//			InputStream input = url.openStream();
//			StringWriter writer = new StringWriter();
//			IOUtils.copy(input, writer, "UTF-8");
//			resultMsg = writer.toString();
//			Long resultado = Long.parseLong(resultMsg);
//			if (resultado > 0) {
//				//Gerar Mensagem
//				List<Irrigante> irrigantes = new ArrayList<Irrigante>();
//				irrigantes.add(i);
//				Mensagem m = new Mensagem(TipoMensagem.SMS, "TI:"+resultado, msg, null, new Date(), null, irrigantes, i.getArea().getDistrito());
//				salvarMensagem(m, em);
//				//Atualizar status das irriga��es do irrigante
//				for (Irrigacao irr : irrigacoesIrrigante) {
//					irr.setSmsEnviado(true);
//					salvarIrrigacao(irr, em);
//				}
//			}else{
//				System.out.println("Erro ao enviar para:" + dest);
//				System.out.println("Mensagem Erro:" + resultado);
//			}
//		} catch (UnsupportedEncodingException e) {
//			System.out.println("Erro de UnsupportedEncodingException ao enviar para:" + dest);
//			System.out.println("Mensagem Erro:" + resultMsg);
//			e.printStackTrace();
//		} catch (MalformedURLException e) {
//			System.out.println("Erro de MalformedURLException ao enviar para:" + dest);
//			System.out.println("Mensagem Erro:" + resultMsg);
//			e.printStackTrace();
//		} catch (IOException e) {
//			System.out.println("Erro de IOException ao enviar para:" + dest);
//			System.out.println("Mensagem Erro:" + resultMsg);
//			e.printStackTrace();
//		}
//	}
//
///*	private void enviar(String msg, String dest, Irrigante i, EntityManager em, Irrigacao ir) {
//		// Enviar SMS com clickatell
//		SmsSender smsSender;
//		System.out.println("SMS Enviado para:" + dest + " - Tamanho msg:" + msg.length());
//		System.out.println(msg);
//		try {
//			smsSender = SmsSender.getClickatellSender("raphael_pf", "ipqf2908", "3373152");
//			smsSender.connect();
//			smsSender.sendTextSms(msg, dest, null );
//			smsSender.disconnect();
//			//Alterar irriga��o para indicar que foi enviada o SMS
//			//Altera o tempo para indicar que foi enviado.
//			ir.setSmsEnviado(true);
//			ir = salvarIrrigacao(ir, em);
//			//Gerar Mensagem
//			List<Irrigante> irrigantes = new ArrayList<Irrigante>();
//			irrigantes.add(i);
//			Mensagem m = new Mensagem(TipoMensagem.SMS, "TI", msg, null, new Date(), null, irrigantes, i.getArea().getDistrito());
//			salvarMensagem(m, em);
//		} catch (SmsException e) {
//			System.out.println("Erro ao enviar para:" + dest);
//			e.printStackTrace();
//		} catch (IOException e) {
//			System.out.println("Erro ao enviar para:" + dest);
//			e.printStackTrace();
//		}
//	}
//*/	
//	static {
//		entityManagerFactory = Persistence.createEntityManagerFactory("maindatabase");
//	}
//	
//	public EntityManager getEntityManager() {
//		return entityManagerFactory.createEntityManager();
//	}
//
//	@SuppressWarnings("unchecked")
//	public List<Irrigacao> pesquisarPorData(String query, Map<String, Object> parametros) {
//		Query queryNamed = executarPesquisa(query, parametros);
//		return queryNamed.getResultList();
//	}
//
//	private Query executarPesquisa(String query,
//		Map<String, Object> parametros) {
//		Query queryNamed = getEntityManager().createNamedQuery(query);
//		if(parametros!=null){
//			Set<String> keyParametros = parametros.keySet();
//			for (String nomeParamentro : keyParametros) {
//				queryNamed.setParameter(nomeParamentro, parametros.get(nomeParamentro));
//			}
//		}
//		return queryNamed;
//	}	
//	
//	public Acesso pesquisarUltimoAcesso(String query, Map<String, Object> parametros) {
//		Query queryNamed = executarPesquisa(query, parametros);
//		return (Acesso) queryNamed.getSingleResult();
//	}	
//	
//	public Irrigacao salvarIrrigacao(Irrigacao entidade, EntityManager em) {
//		entidade = em.merge(entidade);
//		return entidade;
//	}
//
//	public Mensagem salvarMensagem(Mensagem entidade, EntityManager em) {
//		em.persist(entidade);
//		return entidade;
//	}
//
//	public String trata (String passa){  
//	      passa = passa.replaceAll("[�����]","A");  
//	      passa = passa.replaceAll("[�����]","a");  
//	      passa = passa.replaceAll("[����]","E");  
//	      passa = passa.replaceAll("[����]","e");  
//	      passa = passa.replaceAll("����","I");  
//	      passa = passa.replaceAll("����","i");  
//	      passa = passa.replaceAll("[�����]","O");  
//	      passa = passa.replaceAll("[�����]","o");  
//	      passa = passa.replaceAll("[����]","U");  
//	      passa = passa.replaceAll("[����]","u");  
//	      passa = passa.replaceAll("�","C");  
//	      passa = passa.replaceAll("�","c");   
//	      passa = passa.replaceAll("[��]","y");  
//	      passa = passa.replaceAll("�","Y");  
//	      passa = passa.replaceAll("�","n");  
//	      passa = passa.replaceAll("�","N");  
//	      passa = passa.replaceAll("['<>\\|/]","");  
//	      return passa;  
//   }	
//}
