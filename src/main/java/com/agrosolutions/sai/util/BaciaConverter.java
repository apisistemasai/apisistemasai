package com.agrosolutions.sai.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import com.agrosolutions.sai.model.BaciaHidrografica;;

/**
 * Converter espec�fico para sele��o com {@link BaciaHidrografica}, onde o primeiro item selecionado � vazio
 * 
 * @author Raphael.Ferreira
 */
@FacesConverter(value = "baciaConverter")
public class BaciaConverter implements Converter {

	private static final Map<BaciaHidrografica, String> BACIAS = new HashMap<BaciaHidrografica, String>();

	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String stringValor) {

		if ("".equals(stringValor)) {
			return null;
		}
		// Procura pela bacia correspondente ao select item selecionado:
		for (Entry<BaciaHidrografica, String> entry : BACIAS.entrySet()) {
			if (entry.getValue().equals(stringValor)) {
				return entry.getKey();
			}
		}

		throw new RuntimeException("Erro inesperado no converter " + getClass().getName()
				+ ": Refer�ncia para uma entidade foi perdida.");
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object bacia) {
		// Se o objeto selecionado for o valor vazio retorna
		// a string contendo a descri��o.
		if ("".equals(bacia.toString())) {
			return "";
		}

		synchronized (BACIAS) {
			if (!BACIAS.containsKey(bacia)) {
				BACIAS.put((BaciaHidrografica) bacia, ((BaciaHidrografica) bacia).getId().toString());
				return ((BaciaHidrografica) bacia).getId().toString();
			}
			return BACIAS.get(bacia);
		}
	}
}
