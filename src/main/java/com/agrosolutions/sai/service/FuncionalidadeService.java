package com.agrosolutions.sai.service;

import java.util.List;

import com.agrosolutions.sai.model.Funcionalidade;

public interface FuncionalidadeService {
	
	Funcionalidade salvar(Funcionalidade funcionalidade);
	List<Funcionalidade> obterTodos();
}
