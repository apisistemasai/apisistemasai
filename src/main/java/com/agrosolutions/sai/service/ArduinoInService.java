package com.agrosolutions.sai.service;

import java.util.List;

import com.agrosolutions.sai.exception.SaiException;
import com.agrosolutions.sai.model.ArduinoIn;


public interface ArduinoInService {

	List<ArduinoIn> obterTodos() throws SaiException;
}
