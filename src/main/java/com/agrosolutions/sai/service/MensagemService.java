package com.agrosolutions.sai.service;

import java.util.List;

import com.agrosolutions.sai.model.Mensagem;

public interface MensagemService {
	
	Mensagem salvar(Mensagem msg);

	List<Mensagem> pesquisar(Mensagem entidade, Integer first, Integer pageSize);

	int quantidade(Mensagem entidade);
}
