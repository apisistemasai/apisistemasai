package com.agrosolutions.sai.service;

import java.util.List;

import com.agrosolutions.sai.model.Estadio;
import com.agrosolutions.sai.model.Referencia;
 
public interface EstadioService {
	
	List<Estadio> pesquisar(Referencia param);
	void excluir(Estadio setor);
}
