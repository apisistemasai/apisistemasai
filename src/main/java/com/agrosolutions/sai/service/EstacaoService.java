package com.agrosolutions.sai.service;

import java.util.List;

import com.agrosolutions.sai.model.Distrito;
import com.agrosolutions.sai.model.Estacao;

public interface EstacaoService {
	
	Estacao salvar(Estacao estacao);
	List<Estacao> pesquisar(List<Distrito> distrito);
	void excluir(Estacao estacao);
	List<Estacao> pesquisarEstacao(Estacao entidade, Integer first, Integer pageSize);
}
