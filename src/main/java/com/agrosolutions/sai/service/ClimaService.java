package com.agrosolutions.sai.service;

import java.io.InputStream;
import java.util.Date;
import java.util.List;

import com.agrosolutions.sai.model.Area;
import com.agrosolutions.sai.model.Clima;
import com.agrosolutions.sai.model.Distrito;
import com.agrosolutions.sai.model.Estacao;

public interface ClimaService {
	
	Clima salvar(Clima clima);
	List<Clima> pesquisarPorArea(Area entidade);
	Clima pesquisarPorData(Area entidade, Date data);
	List<Clima> pesquisarPorPeriodo(Area entidade, Date dataIni, Date dataFim);
	Clima ultimoPorArea(Area area);
	List<Clima> estacaoPorPeriodo(Estacao estacao, Date dataIni, Date dataFim);
	List<Clima> importarArquivoInmet(InputStream in, Estacao estacao);
	List<Clima> importarArquivoCampbell(InputStream in, Estacao estacao);
	Clima ultimoPorDistrito(Distrito distrito);
	List<Clima> importarArquivoCampbellDibau(InputStream in, Estacao estacao);
	List<Clima> importarArquivoFunceme(InputStream in, Estacao estacao);
	Clima prepararClimaCalcularEto(Clima clima);
	Clima salvarBacia(Clima clima);
}
