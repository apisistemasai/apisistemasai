package com.agrosolutions.sai.service;

import java.util.List;

import com.agrosolutions.sai.model.Distrito;
import com.agrosolutions.sai.model.Irrigante;
import com.agrosolutions.sai.model.Usuario;

public interface IrriganteService {
	
	Irrigante salvar(Irrigante irrigante);
	int quantidade(Irrigante entidade);
	List<Irrigante> pesquisar(Irrigante entidade, Integer first, Integer pageSize);
	void excluir(Irrigante irrigante);
	Irrigante pesquisarPorUsuario(Usuario usu);
	Irrigante pesquisarPorLote(String lote, List<Distrito> distritos);
	List<Irrigante> pesquisarPorDistrito(List<Distrito> distritos);
	List<Irrigante> pesquisarPorDistrito(List<Distrito> distritos,
			Integer indiceInicio, Integer quantidade);
}
