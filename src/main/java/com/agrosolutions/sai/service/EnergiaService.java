package com.agrosolutions.sai.service;

import java.util.List;

import com.agrosolutions.sai.model.Energia;
import com.agrosolutions.sai.model.Irrigante;

public interface EnergiaService {
	
	Energia salvar(Energia energia);
	List<Energia> pesquisar(Irrigante param);
	void excluir(Energia energia);
}
