package com.agrosolutions.sai.service;

import java.util.List;

import com.agrosolutions.sai.model.Adubo;

public interface AduboService {
	
	Adubo salvar(Adubo adubo);
	void excluir(Adubo adubo);
	List<Adubo> obterTodos();
	List<Adubo> pesquisar(Adubo entidade, Integer first, Integer pageSize);

}
