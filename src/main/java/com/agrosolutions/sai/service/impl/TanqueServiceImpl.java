package com.agrosolutions.sai.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.agrosolutions.sai.dao.TanqueDAO;
import com.agrosolutions.sai.exception.SaiException;
import com.agrosolutions.sai.model.Tanque;
import com.agrosolutions.sai.service.TanqueService;

@Service("tanqueService")
public class TanqueServiceImpl implements TanqueService {

	@Autowired
	private TanqueDAO tanqueDAO;

	@Transactional
	public Tanque salvar(Tanque tanque) {
		return tanqueDAO.salvar(tanque);
	}

	public List<Tanque> obterTodos() {
		return tanqueDAO.obterTodos();
	}

	@Transactional
	@Override
	public void excluir(Tanque tanque) {
		tanqueDAO.excluir(tanque);
	}
	
	@Transactional
	@Override
	public List<Tanque> pesquisar(Tanque entidade, Integer first, Integer pageSize) {
		List<Tanque> tanques = null;
		try {
		} catch (Exception e) {
			throw new SaiException(e.getMessage());
		}
		tanques = tanqueDAO.obterPorParametro(entidade, first, pageSize);	
		
		return tanques;
	}
}
