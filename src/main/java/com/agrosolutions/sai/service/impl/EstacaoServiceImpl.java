package com.agrosolutions.sai.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.agrosolutions.sai.dao.EstacaoDAO;
import com.agrosolutions.sai.exception.SaiException;
import com.agrosolutions.sai.model.Distrito;
import com.agrosolutions.sai.model.Estacao;
import com.agrosolutions.sai.service.EstacaoService;

@Service("estacaoService")
public class EstacaoServiceImpl implements EstacaoService {

	@Autowired
	private EstacaoDAO estacaoDAO;

	@Transactional
	public Estacao salvar(Estacao estacao) {
		return estacaoDAO.salvar(estacao);
	}

	public List<Estacao> pesquisar(List<Distrito> distrito) {
		return estacaoDAO.pesquisar(distrito);
	}
	
	@Transactional
	@Override
	public void excluir(Estacao estacao) {
		estacaoDAO.excluir(estacao);
	}
	
	@Transactional
	@Override
	public List<Estacao> pesquisarEstacao(Estacao entidade, Integer first,
			Integer pageSize) {
		List<Estacao> estacoes = null;
		try {
		} catch (Exception e) {
			throw new SaiException(e.getMessage());
		}
		estacoes = estacaoDAO.obterPorParametro(entidade, first, pageSize);	
		
		return estacoes;
	}	

}
