package com.agrosolutions.sai.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.agrosolutions.sai.dao.EnergiaDAO;
import com.agrosolutions.sai.model.Energia;
import com.agrosolutions.sai.model.Irrigante;
import com.agrosolutions.sai.service.EnergiaService;

@Service("energiaService")
public class EnergiaServiceImpl implements EnergiaService {

	@Autowired
	private EnergiaDAO energiaDAO;

	@Transactional
	public Energia salvar(Energia energia) {
		return energiaDAO.salvar(energia);
	}

	public List<Energia> pesquisar(Irrigante param) {
		return energiaDAO.pesquisar(param);
	}

	@Transactional
	@Override
	public void excluir(Energia energia) {
		energiaDAO.excluir(energia);
	}
}
