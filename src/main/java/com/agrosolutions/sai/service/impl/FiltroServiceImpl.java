package com.agrosolutions.sai.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.agrosolutions.sai.dao.FiltroDAO;
import com.agrosolutions.sai.exception.SaiException;
import com.agrosolutions.sai.model.Fabricante;
import com.agrosolutions.sai.model.Filtro;
import com.agrosolutions.sai.model.Irrigante;
import com.agrosolutions.sai.service.FiltroService;

@Service("filtroService")
public class FiltroServiceImpl implements FiltroService {

	@Autowired
	private FiltroDAO filtroDAO;

	@Transactional
	public Filtro salvar(Filtro filtro) {
		return filtroDAO.salvar(filtro);
	}

	public List<Filtro> pesquisar(Fabricante fabricante) {
		return filtroDAO.pesquisar(fabricante);
	}
	@Override
	public List<Filtro> pesquisarPorIrrigante(Irrigante irrigante) {
		return filtroDAO.pesquisarPorIrrigante(irrigante);
	}
	@Override
	@Transactional
	public List<Filtro> pesquisar(Filtro filtro) {
		return filtroDAO.obterPorParametro(filtro, null, null);
	}
	public List<Filtro> obterTodos() {
		return filtroDAO.obterTodos();
	}

	@Transactional
	@Override
	public void excluir(Filtro filtro) {
		filtroDAO.excluir(filtro);
	}
	
	@Transactional
	public int quantidade(Filtro entidade) {
		int resultado;
		try {
			validarPesquisa(entidade);
		} catch (SaiException e) {
			throw new SaiException(e.getMessages());
		}
		entidade.setSortField("");
		resultado = filtroDAO.quantidade(entidade);
		return resultado;
	}

	@Transactional
	@Override
	public List<Filtro> pesquisar(Filtro entidade, Integer first, Integer pageSize) {
		List<Filtro> filtros = null;
		try {
			validarPesquisa(entidade);
		} catch (SaiException e) {
			throw new SaiException(e.getMessages());
		}
		filtros = filtroDAO.obterPorParametro(entidade, first, pageSize);
		return filtros;
	}
	
	private void validarPesquisa(Filtro entidade) throws SaiException {
		List<String> messages = new ArrayList<String>();

		if (!messages.isEmpty()) {
			throw new SaiException(messages);
		}
	}
}
