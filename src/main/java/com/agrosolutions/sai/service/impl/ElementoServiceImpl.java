package com.agrosolutions.sai.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.agrosolutions.sai.dao.ElementoDAO;
import com.agrosolutions.sai.exception.SaiException;
import com.agrosolutions.sai.model.Elemento;
import com.agrosolutions.sai.service.ElementoService;

@Service("elementoService")
public class ElementoServiceImpl implements ElementoService {

	@Autowired
	private ElementoDAO elementoDAO;

	@Transactional
	public Elemento salvar(Elemento elemento) {
		return elementoDAO.salvar(elemento);
	}

	public List<Elemento> obterTodos() {
		return elementoDAO.obterTodos();
	}

	@Transactional
	@Override
	public void excluir(Elemento elemento) {
		elementoDAO.excluir(elemento);
	}
	
	@Transactional
	@Override
	public List<Elemento> pesquisar(Elemento entidade, Integer first, Integer pageSize) {
		List<Elemento> elementos = null;
		try {
		} catch (Exception e) {
			throw new SaiException(e.getMessage());
		}
		elementos = elementoDAO.obterPorParametro(entidade, first, pageSize);	
		
		return elementos;
	}
}
