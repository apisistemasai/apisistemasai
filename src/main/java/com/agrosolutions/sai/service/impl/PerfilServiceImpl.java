package com.agrosolutions.sai.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.agrosolutions.sai.dao.PerfilDAO;
import com.agrosolutions.sai.exception.SaiException;
import com.agrosolutions.sai.model.Perfil;
import com.agrosolutions.sai.model.TipoPerfil;
import com.agrosolutions.sai.service.PerfilService;

@Service("perfilService")
public class PerfilServiceImpl implements PerfilService {

	@Autowired
	private PerfilDAO perfilDAO;

	@Transactional
	public Perfil salvar(Perfil perfil) {
		return perfilDAO.salvar(perfil);
	}

	public List<Perfil> obterTodos() {
		return perfilDAO.obterTodos();
	}

	@Override
	public List<Perfil> porTipo(TipoPerfil tipo) {
		return perfilDAO.porTipo(tipo);
	}

	@Transactional
	@Override
	public void excluir(Perfil perfil) {
		//perfil.setFuncionalidades(null);
		//perfilDAO.salvar(perfil);
		perfilDAO.excluir(perfil);
	}

	@Transactional
	@Override
	public List<Perfil> pesquisar(Perfil entidade, Integer first,
			Integer pageSize) {
		List<Perfil> perfis = null;
		try {
			validarPesquisa(entidade);
		} catch (SaiException e) {
			throw new SaiException(e.getMessages());
		}
		perfis = perfilDAO.obterPorParametro(entidade, first, pageSize);
		return perfis;
	}

	@Transactional
	public int quantidade(Perfil entidade) {
		int resultado;
		try {
			validarPesquisa(entidade);
		} catch (SaiException e) {
			throw new SaiException(e.getMessages());
		}
		entidade.setSortField("");
		resultado = perfilDAO.quantidade(entidade);
		return resultado;
	}
	
	private void validarPesquisa(Perfil entidade) throws SaiException {
		List<String> messages = new ArrayList<String>();

		if (!messages.isEmpty()) {
			throw new SaiException(messages);
		}
	}
}
