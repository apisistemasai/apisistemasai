package com.agrosolutions.sai.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.agrosolutions.sai.dao.EstadioDAO;
import com.agrosolutions.sai.model.Estadio;
import com.agrosolutions.sai.model.Referencia;
import com.agrosolutions.sai.service.EstadioService;
 
@Service("estadioService")
public class EstadioServiceImpl implements EstadioService {

	@Autowired
	private EstadioDAO estadioDAO;

	public List<Estadio> pesquisar(Referencia param) {
		return estadioDAO.pesquisar(param);
	}

	@Transactional
	@Override
	public void excluir(Estadio e) {
		estadioDAO.excluir(e);
	}
	
}
