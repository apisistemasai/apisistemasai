package com.agrosolutions.sai.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.agrosolutions.sai.dao.FornecedorDAO;
import com.agrosolutions.sai.exception.SaiException;
import com.agrosolutions.sai.model.Fornecedor;
import com.agrosolutions.sai.service.FornecedorService;

@Service("fornecedorService")
public class FornecedorServiceImpl implements FornecedorService {
	
	@Autowired
	private FornecedorDAO fornecedorDAO;

	@Transactional
	public Fornecedor salvar(Fornecedor c) {
		try {
			validar(c);
		} catch (SaiException e) {
			throw new SaiException(e.getMessages());
		}
		
		return fornecedorDAO.salvar(c);
	}

	public List<Fornecedor> obterTodos() {
		return fornecedorDAO.obterTodos();
	}
	
	
	private void validar(Fornecedor c) throws SaiException {
		List<String> messages = new ArrayList<String>();
	

		if (!messages.isEmpty()) {
			throw new SaiException(messages);
		}
	}

	@Transactional
	public void excluir(Fornecedor c) {
		fornecedorDAO.excluir(c);
	}
}

