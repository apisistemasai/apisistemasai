package com.agrosolutions.sai.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.agrosolutions.sai.dao.PlantioDAO;
import com.agrosolutions.sai.dao.SetorDAO;
import com.agrosolutions.sai.model.Area;
import com.agrosolutions.sai.model.Distrito;
import com.agrosolutions.sai.model.GraficoPizza;
import com.agrosolutions.sai.model.Irrigante;
import com.agrosolutions.sai.model.Setor;
import com.agrosolutions.sai.service.SetorService;
 
@Service("setorService")
public class SetorServiceImpl implements SetorService {

	@Autowired
	private SetorDAO setorDAO;
	@Autowired
	private PlantioDAO plantioDAO;

	@Transactional
	public Setor salvar(Setor setor) {
		return setorDAO.salvar(setor);
	}

	public List<Setor> pesquisar(Irrigante param) {
		return setorDAO.pesquisar(param);
	}

	@Transactional
	@Override
	public void excluir(Setor setor) {
		if (!setor.getEmissores().isEmpty()) {
			setor.setEmissores(null);
			setor = setorDAO.salvar(setor);
		}
		setorDAO.excluir(setor);
	}
	
	@Override
	public List<GraficoPizza> irrigacaoPorDistrito(Distrito d) {
		List<GraficoPizza> retorno = new ArrayList<GraficoPizza>();
		GraficoPizza vpm;
		//Total de area irrigada
		BigDecimal totalHecIrrigado = setorDAO.totalHecComPlantioIrrigadoPorDistrito(d);
		vpm = new GraficoPizza("�rea Irrigada", totalHecIrrigado==null?0L:totalHecIrrigado.longValue(), null, "ha");
		retorno.add(vpm);

		//Total de area preservada no distrito
		BigDecimal totalHecPreservado = d.getAreaPreservada();
		vpm = new GraficoPizza("�rea Preservada", totalHecPreservado==null?0L:totalHecPreservado.longValue(), null, "ha");
		retorno.add(vpm);

		//Total de area n�o irrigada
		Long totalHecNaoIrrigado = d.getAreaTotal().longValue() - (totalHecPreservado==null?0L:totalHecPreservado.longValue()) - (totalHecIrrigado==null?0L:totalHecIrrigado.longValue());
		vpm = new GraficoPizza("�rea N�o Irrigada", totalHecNaoIrrigado, null, "ha");
		retorno.add(vpm);
		
		//Total de area em sequeiro
		BigDecimal totalHecSequeiro = plantioDAO.totalHecSequeiro(d);
		vpm = new GraficoPizza("�rea em sequeiro", totalHecSequeiro==null?0L:totalHecSequeiro.longValue(), null, "ha");
		retorno.add(vpm);
		
		//Total de area em sequeiro
		BigDecimal totalHecConsorcio = plantioDAO.totalHecConsorcio(d);
		vpm = new GraficoPizza("�rea em cons�rcio", totalHecConsorcio==null?0L:totalHecConsorcio.longValue(), null, "ha");
		retorno.add(vpm);

		return retorno;
	}
	
	@Override
	public List<GraficoPizza> irrigacaoPorArea(Area a) {
		List<GraficoPizza> retorno = new ArrayList<GraficoPizza>();
		GraficoPizza vpm;
		//Total de area irrigada
		BigDecimal totalHecIrrigado = setorDAO.totalHecComPlantioPorArea(a);
		vpm = new GraficoPizza("�rea Irrigada", totalHecIrrigado==null?0L:totalHecIrrigado.longValue(), null, "ha");
		retorno.add(vpm);

		//Total de area preservada no distrito
		BigDecimal totalHecPreservado = a.getAreaTotal().subtract(a.getAreaIrrigada());
		vpm = new GraficoPizza("�rea Preservada", totalHecPreservado==null?0L:totalHecPreservado.longValue(), null, "ha");
		retorno.add(vpm);

		//Total de area n�o irrigada
		Long totalHecNaoIrrigado = a.getAreaTotal().longValue() - (totalHecPreservado==null?0L:totalHecPreservado.longValue()) - (totalHecIrrigado==null?0L:totalHecIrrigado.longValue());
		vpm = new GraficoPizza("�rea N�o Irrigada", totalHecNaoIrrigado==null?0L:totalHecNaoIrrigado.longValue(), null, "ha");
		retorno.add(vpm);
		
		//Total de area em sequeiro
		BigDecimal totalHecSequeiro = plantioDAO.totalHecSequeiro(a);
		vpm = new GraficoPizza("�rea em sequeiro", totalHecSequeiro==null?0L:totalHecSequeiro.longValue(), null, "ha");
		retorno.add(vpm);
		
		//Total de area em sequeiro
		BigDecimal totalHecConsorcio = plantioDAO.totalHecConsorcio(a);
		vpm = new GraficoPizza("�rea em cons�rcio", totalHecConsorcio==null?0L:totalHecConsorcio.longValue(), null, "ha");
		retorno.add(vpm);

		return retorno;
	}
	
}
