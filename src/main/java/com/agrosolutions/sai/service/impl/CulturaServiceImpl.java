package com.agrosolutions.sai.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.agrosolutions.sai.dao.CulturaDAO; 
import com.agrosolutions.sai.exception.SaiException;
import com.agrosolutions.sai.model.Area;
import com.agrosolutions.sai.model.Cultura;
import com.agrosolutions.sai.model.GraficoPizza;
import com.agrosolutions.sai.service.CulturaService;

@Service("culturaService")
public class CulturaServiceImpl implements CulturaService {

	@Autowired
	private CulturaDAO culturaDAO;

	@Transactional
	public Cultura salvar(Cultura cultura) {
		return culturaDAO.salvar(cultura);
	}

	public List<Cultura> obterTodos() {
		return culturaDAO.obterTodos();
	}
 
	@Override
	public List<GraficoPizza> culturaAtivaPorArea(Area a, Date data) {
		return culturaDAO.culturaAtivaPorArea(a, data);
	}

	@Transactional
	@Override
	public void excluir(Cultura cultura) {
		culturaDAO.excluir(cultura);
	}

	@Transactional
	public int quantidade(Cultura entidade) {
		int resultado;
		try {
			validarPesquisa(entidade);
		} catch (SaiException e) {
			throw new SaiException(e.getMessages());
		}
		entidade.setSortField("");
		resultado = culturaDAO.quantidade(entidade);
		return resultado;
	}
	
	@Transactional
	@Override
	public List<Cultura> pesquisar(Cultura entidade, Integer first, Integer pageSize) {
		List<Cultura> culturas = null;
		try {
			validarPesquisa(entidade);
		} catch (SaiException e) {
			throw new SaiException(e.getMessages());
		}
		culturas = culturaDAO.obterPorParametro(entidade, first, pageSize);
		return culturas;
	}

	private void validarPesquisa(Cultura entidade) throws SaiException {
		List<String> messages = new ArrayList<String>();
		limparFiltros(entidade);

		if (!messages.isEmpty()) {
			throw new SaiException(messages);
		}
	}

	// Limpando atributos vazio para nulos para n�o entrar na clausula where
	public void limparFiltros(Cultura entidade){
		if (entidade.getNome() !=null && entidade.getNome().equals("")) {
			entidade.setNome(null);
		}
		entidade.setImage(null);
	}
}
