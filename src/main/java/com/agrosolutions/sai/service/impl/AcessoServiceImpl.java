package com.agrosolutions.sai.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.agrosolutions.sai.dao.AcessoDAO;
import com.agrosolutions.sai.model.Acesso;
import com.agrosolutions.sai.model.Usuario;
import com.agrosolutions.sai.service.AcessoService;

@Service("acessoService")
public class AcessoServiceImpl implements AcessoService {

	@Autowired
	private AcessoDAO acessoDAO;

	@Transactional
	public Acesso salvar(Acesso a) {
		return acessoDAO.salvar(a);
	}

	public Acesso pesquisarUltimo(String u) {
		Acesso pesquisarUltimoAcesso = null;
		try {
			pesquisarUltimoAcesso = acessoDAO.pesquisarUltimoAcesso(u);
		} catch (EmptyResultDataAccessException e) {
		}
		return pesquisarUltimoAcesso;
	}
	@Override
	public List<Acesso> pesquisarPorUsuario(Usuario u) {
		return acessoDAO.pesquisarPorUsuario(u);
	}
}
