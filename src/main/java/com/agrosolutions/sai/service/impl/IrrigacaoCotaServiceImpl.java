package com.agrosolutions.sai.service.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.agrosolutions.sai.dao.AreaDAO;
import com.agrosolutions.sai.dao.IrriganteDAO;
import com.agrosolutions.sai.dao.PlantioDAO;
import com.agrosolutions.sai.model.Area;
import com.agrosolutions.sai.model.Distrito;
import com.agrosolutions.sai.model.IrrigacaoCota;
import com.agrosolutions.sai.model.Irrigante;
import com.agrosolutions.sai.model.Plantio;
import com.agrosolutions.sai.model.Setor;
import com.agrosolutions.sai.service.EmissorService;
import com.agrosolutions.sai.service.IrrigacaoCotaService;
import com.agrosolutions.sai.service.PlantioService;
import com.agrosolutions.sai.service.SetorService;
import com.agrosolutions.sai.util.BigDecimalUtil;

@Service("irrigacaoCotaService")
public class IrrigacaoCotaServiceImpl implements IrrigacaoCotaService {

	@Autowired
	private IrriganteDAO irriganteDAO;
	@Autowired
	private AreaDAO areaDAO;
	@Autowired
	private PlantioDAO plantioDAO;
	@Autowired
	private PlantioService plantioService;
	@Autowired
	private SetorService setorService;
	@Autowired
	private EmissorService emissorService;
	
	private List<Irrigante> irrigantes;
	private List<Plantio> plantios;
	private BigDecimal volumeDisponivelTotal;
	private BigDecimal eficienciaDistribuicao;
	private BigDecimal diasIrrigadosSemana;
	private BigDecimal volumeDisponivelHa;
	
	
	public List<Irrigante> calcularAreaIrrigadaPorIrrigante(List<Irrigante> irrigantes) {
		for (Irrigante irrigante : irrigantes) {
			irrigante.setAreaIrrigada(new BigDecimal(0));
			irrigante.setSetores(setorService.pesquisar(irrigante));
			
			for (Setor setor : irrigante.getSetores() ) {
				setor.setPlantio(plantioService.pesquisarPorSetor(setor));
				
				for (Plantio plantio : setor.getPlantio()) {
					if ( plantio.getDataFim() == null)
						irrigante.setAreaIrrigada(irrigante.getAreaIrrigada().add(plantio.getAreatotal()) );
				}
			}
		}
		
		return irrigantes;
	}
	
	public List<Irrigante> consultarIrrigantes() {
		irrigantes = irriganteDAO.obterTodos();
		
		for (Irrigante irrigante : irrigantes) {
			irrigante.setAreaIrrigada(new BigDecimal(0));
			irrigante.setSetores(setorService.pesquisar(irrigante));
			
			for (Setor setor : irrigante.getSetores() ) {
				setor.setPlantio(plantioService.pesquisarPorSetor(setor));
				
				for (Plantio plantio : plantioService.pesquisarPorIrrigante(irrigante)) {
					if ( plantio.getDataFim() == null)
						irrigante.setAreaIrrigada(irrigante.getAreaIrrigada().add(plantio.getAreatotal()) );
				}
				
			}
		}
		
		return irrigantes;
	}
	

	/**
	 * ((volumeDisponivelTotal/areaIrrigada) * eficienciaDistribuicao / 100) * (7/diasIrrigadosSemana)
	 * 
	 * @return
	 */
	public BigDecimal calcularVolumeDisponivelHa( BigDecimal volumeDisponivelTotal, BigDecimal areaIrrigadaTotal, BigDecimal eficienciaDistribuicao, BigDecimal diasIrrigadosSemana) {
		BigDecimal volumeTotal = ( (volumeDisponivelTotal.divide(areaIrrigadaTotal, BigDecimalUtil.MC)).multiply(eficienciaDistribuicao.divide( new BigDecimal(100))) ).multiply( new BigDecimal(7).divide(diasIrrigadosSemana, BigDecimalUtil.MC ), BigDecimalUtil.MC );
		return volumeTotal.setScale(2, RoundingMode.HALF_DOWN);
	}
	
	public BigDecimal calcularAreaIrrigadaTotal(List<Distrito> distritos) {
		
		BigDecimal areaIrrigadaTotal = new BigDecimal(0);
		
		List<Area> areas = areaDAO.pesquisar(distritos);
		for (Area area : areas) {
			List<Plantio> plantios = plantioDAO.ativos(area, new Date());
			for (Plantio plantio : plantios) {
				areaIrrigadaTotal = areaIrrigadaTotal.add( plantio.getAreatotal() );
			}
			
		}
		
		return areaIrrigadaTotal;
	}
	
	/**
	 * vazaoPorPlantio = 10000/(se*sl + 0,00001) * vazaoEmissor * areaDoPlantio
	 * vazaoMediaDoLote = (somaoDasVazoesDosPlantios/qtdPlantios)/1000
	 * volumeDisponivel = somaAreaPlantis * volumeDisponivelPorHa
	 * TIdoLote = volumeDisponivel/vazaoMedia
	 * 
	 * TI em horas = TIdoLote(sem casas decimais) 
	 * TI em minutos = casas decimais(TIdoLote)*60 
	 */
	public IrrigacaoCota calcularCotaPorIrrigante(Irrigante irrigante, BigDecimal volumeDisponivelPorHa) {
		System.out.println(irrigante.getLocalizacao());
		if (irrigante.getLocalizacao().equals("C2/3")) {
			System.out.println("ok");
		}
		List<Plantio> plantios = new ArrayList<Plantio>();
		BigDecimal vazao = new BigDecimal(0);
		BigDecimal vazaoMedia = new BigDecimal(0);
		BigDecimal somaAreaPlantis = new BigDecimal(0);
		BigDecimal volumeDisponivel = new BigDecimal(0);
		BigDecimal tempoIrrigacaoHoras = new BigDecimal(0);
		BigDecimal tempoIrrigacaoMinutos = new BigDecimal(0);
		Integer qtdPlantios = 0;
		plantios = plantioService.pesquisarPorIrrigante(irrigante);
		irrigante.setSetores(setorService.pesquisar(irrigante));
		
		
		// Se n�o tem plantios, zera os valores
		if ( plantios.isEmpty() ) return new IrrigacaoCota(irrigante, new BigDecimal(0), new BigDecimal(0), new BigDecimal(0), new BigDecimal(0) );
		
		for (Plantio plantio : plantios) {
			// N�o contabiliza consorcio e sequeiro
			if ( plantio.getConsorcio() || plantio.getSequeiro()) {
				continue;
			}
			
			// Se tem emissor no plantio, atualiza a quantidade de plantios irrigados
			plantio.setEmissores(emissorService.pesquisarPorSetor(plantio.getSetor()));			
			if ( plantio.getEmissores().isEmpty()) {
				continue;
			} else {
				qtdPlantios++;
				somaAreaPlantis = somaAreaPlantis.add( plantio.getAreatotal() );
			}
			
			vazao = vazao.add(
					new BigDecimal(10000).divide(
						new BigDecimal(plantio.getEspE())
						.multiply( new BigDecimal(plantio.getEspLE()), BigDecimalUtil.MC)
						.add(new BigDecimal(0.00001), BigDecimalUtil.MC), BigDecimalUtil.MC
					)
					.multiply(
							plantio.getEmissores().get(0).getVazao().multiply(new BigDecimal(plantio.getTotalEmissores()), BigDecimalUtil.MC) , BigDecimalUtil.MC
					)
					.multiply(
							plantio.getAreatotal(), BigDecimalUtil.MC
					)
					, BigDecimalUtil.MC);
			System.out.println(vazao);
		}
		
		// Se n�o tem emissor, zera os valores
		if (qtdPlantios == 0) return new IrrigacaoCota(irrigante, new BigDecimal(0), new BigDecimal(0), new BigDecimal(0), new BigDecimal(0) );
		
		vazaoMedia = (vazao.divide(new BigDecimal(qtdPlantios), BigDecimalUtil.MC)).divide(new BigDecimal(1000), BigDecimalUtil.MC).setScale(2, RoundingMode.HALF_DOWN);		
		volumeDisponivel = somaAreaPlantis.multiply(volumeDisponivelPorHa).setScale(2, RoundingMode.HALF_DOWN);;
		// TI bruto
		BigDecimal ti = volumeDisponivel.divide(vazaoMedia, BigDecimalUtil.MC);
		tempoIrrigacaoHoras = new BigDecimal(ti.intValue());
		// Separa a parte decimal para calcular os minutos
		BigDecimal minutos = ti.subtract(new BigDecimal(ti.toBigInteger()));
		tempoIrrigacaoMinutos = minutos.multiply( new BigDecimal(60), BigDecimalUtil.MC_RDO);
		tempoIrrigacaoMinutos = new BigDecimal(tempoIrrigacaoMinutos.intValue());
		
		return new IrrigacaoCota(irrigante, vazaoMedia, volumeDisponivel, tempoIrrigacaoHoras, tempoIrrigacaoMinutos );		
	}
	
	/**
	 * Popula as depend�ncias e calcula a �rea plantada
	 * @param irrigante
	 * @return
	 */
	public Irrigante prepararIrrigante(Irrigante irrigante) {
		irrigante.setAreaIrrigada(new BigDecimal(0));
		irrigante.setSetores(setorService.pesquisar(irrigante));
		
		for (Setor setor : irrigante.getSetores() ) {
			setor.setPlantio(plantioService.pesquisarPorSetor(setor));
			
			for (Plantio plantio : setor.getPlantio()) {
				if ( plantio.getTotalEmissores() == 0) continue;
				if ( plantio.getDataFim() == null)
					irrigante.setAreaIrrigada(irrigante.getAreaIrrigada().add(plantio.getAreatotal()) );
			}
			
		}
		
		return irrigante;
	}
	

	public List<Plantio> getPlantios() {
		return plantios;
	}

	public void setPlantios(List<Plantio> plantios) {
		this.plantios = plantios;
	}

	public BigDecimal getVolumeDisponivelTotal() {
		return volumeDisponivelTotal;
	}

	public void setVolumeDisponivelTotal(BigDecimal volumeDisponivelTotal) {
		this.volumeDisponivelTotal = volumeDisponivelTotal;
	}

	public BigDecimal getEficienciaDistribuicao() {
		return eficienciaDistribuicao;
	}

	public void setEficienciaDistribuicao(BigDecimal eficienciaDistribuicao) {
		this.eficienciaDistribuicao = eficienciaDistribuicao;
	}

	public BigDecimal getDiasIrrigadosSemana() {
		return diasIrrigadosSemana;
	}

	public void setDiasIrrigadosSemana(BigDecimal diasIrrigadosSemana) {
		this.diasIrrigadosSemana = diasIrrigadosSemana;
	}

	public BigDecimal getVolumeDisponivelHa() {
		return volumeDisponivelHa;
	}

	public void setVolumeDisponivelHa(BigDecimal volumeDisponivelHa) {
		this.volumeDisponivelHa = volumeDisponivelHa;
	}

}
