package com.agrosolutions.sai.service.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.agrosolutions.sai.exception.SaiException;
import com.agrosolutions.sai.model.Parametro;
import com.agrosolutions.sai.service.EmailService;
import com.agrosolutions.sai.service.ParametroService;

/**
 * Classe usada para enviar emails aos usuarios do sistema.
 */
@Service("emailService")
public class EmailServiceImpl implements EmailService {

	private static final String MSG_ERRO_INESPERADO = "Ocorreu um erro inesperado ao enviar e-mail. Por favor, entre em contato com o suporte t�cnico.";

	@Autowired
	private ParametroService parametroService;

	private String emailOrigem;
	private String nomeUsuarioOrigem;
	private String senhaUsuarioOrigem;
	private String smtpHost;
	private int smtpPort;
	private Properties configuracaoEmail;

	private void configurarEmail() throws SaiException {
		Parametro parametro;
		if (configuracaoEmail == null) {
			
			parametro = parametroService.pesquisarPorCodigo("emailOrigem");
			emailOrigem = parametro.getValor();

			parametro = parametroService.pesquisarPorCodigo("nomeUsuarioOrigem");
			nomeUsuarioOrigem = parametro.getValor();

			parametro = parametroService.pesquisarPorCodigo("senhaUsuarioOrigem");
			senhaUsuarioOrigem = parametro.getValor();

			parametro = parametroService.pesquisarPorCodigo("smtpHost");
			smtpHost  = parametro.getValor();

			parametro = parametroService.pesquisarPorCodigo("smtpPort");
			smtpPort  = Integer.parseInt(parametro.getValor());
			
			Properties lProperties = new Properties();
			lProperties.put("mail.smtp.host", smtpHost);
			lProperties.put("mail.smtp.port", smtpPort);
			lProperties.put("mail.debug", "true");
			if (senhaUsuarioOrigem != null){ 
				lProperties.put("mail.smtp.auth", "true");
			}else{
				lProperties.put("mail.smtp.auth", "false");
			}
			if (smtpHost.equals("smtp.gmail.com")) {
				lProperties.put("mail.smtp.starttls.enable", "true");
				lProperties.put("mail.smtp.socketFactory.port", "465");
				lProperties.put("mail.smtp.socketFactory.fallback", "false");
				lProperties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
			}

			configuracaoEmail = lProperties;
		}
	}

	private InternetAddress[] criarListaInternetAddress(List<String> destinatarios) throws Exception {
		InternetAddress[] lListaAddress = new InternetAddress[destinatarios.size()];
		for (int i = 0; i < destinatarios.size(); i++) {
			lListaAddress[i] = new InternetAddress(destinatarios.get(i));
		}
		return lListaAddress;
	}

	public void enviarEmail(List<String> destinatarios, String assunto, String conteudo, Map<String, InputStream> anexos) throws SaiException {
		configurarEmail();
		try {
			// E-mail de Origem:
			InternetAddress lAddressOrigem = new InternetAddress(emailOrigem, nomeUsuarioOrigem);
			// E-mail(s) de Destino:
			InternetAddress[] lListaAddressDestino = criarListaInternetAddress(destinatarios);

			Session lSession = Session.getInstance(configuracaoEmail, null);
			MimeMessage lMineMessage = new MimeMessage(lSession);
			lMineMessage.setSentDate(new Date());
			lMineMessage.setFrom(lAddressOrigem);
			lMineMessage.addRecipients(Message.RecipientType.BCC, lListaAddressDestino);

			// Assunto do e-mail:
			lMineMessage.setSubject(assunto);
			java.util.Properties properties = System.getProperties();
			properties.setProperty("file.encoding", "ISO-8859-1");
			lMineMessage.setHeader("Content-Type", "text/plain; charset=iso-8859-1");
			// add handlers for main MIME types
			if (anexos == null || anexos.isEmpty()) {
				// Conte�do do e-mail:
				lMineMessage.setContent(conteudo, "text/html");
								
			} else {
				MimeMultipart lMultiPartRoot = new MimeMultipart("mixed");

				// Cria o BodyPart do conte�do do e-mail:
				MimeBodyPart lBodyPartCorpo = new MimeBodyPart();
				lBodyPartCorpo.setContent(conteudo, "text/html");
				lMultiPartRoot.addBodyPart(lBodyPartCorpo);

				// Cria os BodyParts para os anexos do e-mail:
				for (String nomeArquivo : anexos.keySet()) {
					// Cria um arquivo tempor�rio para o anexo:
					File lArquivoTemporario = criarArquivoTemporario(anexos.get(nomeArquivo));
					DataSource lFileDataSource = new FileDataSource(lArquivoTemporario);
					// Cria o BodyPart do anexo:
					MimeBodyPart lBodyPartAnexo = new MimeBodyPart();
					lBodyPartAnexo.setDisposition(Part.ATTACHMENT);
					lBodyPartAnexo.setDataHandler(new DataHandler(lFileDataSource));
					lBodyPartAnexo.setFileName(nomeArquivo);
					lMultiPartRoot.addBodyPart(lBodyPartAnexo);
				}
				lMineMessage.setContent(lMultiPartRoot);	
			}
			lMineMessage.saveChanges();

			// Conecta-se ao servidor e envia o e-mail:
			Transport lTranportEnvio = lSession.getTransport("smtp");
			lTranportEnvio.connect(smtpHost, smtpPort, emailOrigem, senhaUsuarioOrigem);
			lTranportEnvio.sendMessage(lMineMessage, lMineMessage.getAllRecipients());
			lTranportEnvio.close();
		} catch (Exception pEx) {
			throw new SaiException(MSG_ERRO_INESPERADO, pEx);
		}
	}

	/**
	 * Cria um arquivo tempor�rio a partir do inputStream
	 * 
	 * @param inputStream
	 * @return Arquivo tempor�rio.
	 * @throws Exception
	 */
	private File criarArquivoTemporario(InputStream inputStream) throws Exception {
		File arquivoTemporario = File.createTempFile("sai", null);
		arquivoTemporario.deleteOnExit();
		FileOutputStream lFos = new FileOutputStream(arquivoTemporario);
		int readByte;
		while ((readByte = inputStream.read()) != -1) {
			lFos.write(readByte);
		}
		lFos.close();
		return arquivoTemporario;
	}
}