package com.agrosolutions.sai.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.agrosolutions.sai.dao.FabricanteDAO;
import com.agrosolutions.sai.exception.SaiException;
import com.agrosolutions.sai.model.Fabricante;
import com.agrosolutions.sai.model.TipoFabricante;
import com.agrosolutions.sai.service.FabricanteService;

@Service("fabricanteService")

public class FabricanteServiceImpl implements FabricanteService {

	@Autowired
	private FabricanteDAO fabricanteDAO;

	@Transactional
	public Fabricante salvar(Fabricante fabricante) {
		return fabricanteDAO.salvar(fabricante);
	}

	public List<Fabricante> pesquisar(TipoFabricante tipo) {
		return fabricanteDAO.pesquisar(tipo);
	}

	@Transactional
	@Override
	public void excluir(Fabricante fabricante) {
		fabricanteDAO.excluir(fabricante);
	}
	
	@Transactional
	public int quantidade(Fabricante entidade) {
		int resultado;
		try {
			validarPesquisa(entidade);
		} catch (SaiException e) {
			throw new SaiException(e.getMessages());
		}
		entidade.setSortField("");
		resultado = fabricanteDAO.quantidade(entidade);
		return resultado;
	}
	
	@Transactional
	@Override
	public List<Fabricante> pesquisar(Fabricante entidade, Integer first, Integer pageSize) {
		List<Fabricante> fabricantes = null;
		try {
			validarPesquisa(entidade);
		} catch (Exception e) {
			throw new SaiException(e.getMessage());
		}
		fabricantes = fabricanteDAO.obterPorParametro(entidade, first, pageSize);	
		return fabricantes;
	}
	private void validarPesquisa(Fabricante entidade) throws SaiException {
		List<String> messages = new ArrayList<String>();
		limparFiltros(entidade);

		if (!messages.isEmpty()) {
			throw new SaiException(messages);
		}
	}
	public void limparFiltros(Fabricante entidade){
		if (entidade.getNome() !=null && entidade.getNome().equals("")) {
			entidade.setNome(null);
		}
	}
	
}
