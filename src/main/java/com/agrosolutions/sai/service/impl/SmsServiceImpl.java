package com.agrosolutions.sai.service.impl;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.Normalizer;

import org.apache.commons.io.IOUtils;
//import org.marre.SmsSender;
//import org.marre.sms.SmsException;
import org.springframework.stereotype.Service;

import com.agrosolutions.sai.exception.SaiException;
import com.agrosolutions.sai.service.SmsService;

/**
 * Classe usada para enviar sms.
 */
@Service("smsService")
public class SmsServiceImpl implements SmsService {

	private static final String MSG_ERRO_INESPERADO = "Ocorreu um erro inesperado ao enviar o SMS. Por favor, entre em contato com o suporte t�cnico.";

//	public void enviarSMS(String destinatario, String msg) throws SaiException, SmsException {
//		// Enviar SMS com clickatell
//		//SmsSender smsSender;
//		try {
//			smsSender = SmsSender.getClickatellSender("raphael_pf", "ipqf2908", "3373152");
//			smsSender.connect();
//			String msgids = smsSender.sendTextSms(msg, destinatario, null );
//			System.out.println(msgids);
//			smsSender.disconnect();
//		} catch (SmsException e) {
//			e.printStackTrace();
//			throw new SmsException(e.getMessage());
//		} catch (IOException e) {
//			e.printStackTrace();
//			throw new SaiException(MSG_ERRO_INESPERADO);
//		}
//	}
//	@Override
//	public void enviarSMS2(String to, String msg) throws SaiException, SmsException {
//		String user = "rpf1404@gmail.com";
//		String password = "ipqf2908";
//		String codpessoa = "1163";
//		String assinatura = "SaI";
//		String resultMsg = "";
//		char enviarimediato = 'S';
//		try{
//			msg = URLEncoder.encode(msg, "UTF-8");
//			assinatura = URLEncoder.encode(assinatura, "UTF-8");
//			String connection =
//			"http://web.smscel.com.br/sms/views/getsmsc.do?user="+user +"&password="+ password +"&codpessoa="+ codpessoa +"&to="+ to
//			        +"&msg="+ msg+"&assinatura="+ assinatura +"&enviarimediato="+ enviarimediato+"&data=&hora=";
//			URL url = new URL(connection);
//			       
//			InputStream input = url.openStream();
//			byte[] b = new byte[800];
//			input.read(b, 0, b.length);
//			String xmlRetorno = new String(b);
//			String resultado = xmlRetorno.substring((xmlRetorno.lastIndexOf("<nro_retorno>")+ 13), xmlRetorno.lastIndexOf("</nro_retorno>"));
//			resultMsg = xmlRetorno.substring((xmlRetorno.lastIndexOf("<msg_retorno>")+ 13), xmlRetorno.lastIndexOf("</msg_retorno>"));
//			if (!resultado.equals("000")) {
//				throw new SmsException(resultMsg); 
//			}
//		} catch (UnsupportedEncodingException e) {
//			e.printStackTrace();
//			throw new SmsException(resultMsg); 
//		} catch (MalformedURLException e) {
//			e.printStackTrace();
//			throw new SmsException(resultMsg); 
//		} catch (IOException e) {
//			e.printStackTrace();
//			throw new SmsException(resultMsg); 
//		}
//	}
//	
//	@Override
//	public String consultarSaldo() throws SaiException, SmsException {
//		String user = "rpf1404@gmail.com";
//		String password = "ipqf2908";
//		String codpessoa = "1163";
//		String resultMsg = "";
//		try{
//			String connection =
//			"http://web.smscel.com.br/sms/views/getsaldos.do?user="+user +"&password="+ password +"&codpessoa="+ codpessoa;
//			URL url = new URL(connection);
//			       
//			InputStream input = url.openStream();
//			byte[] b = new byte[800];
//			input.read(b, 0, b.length);
//			String xmlRetorno = new String(b);
//			return xmlRetorno.replace("﻿", "");
//		} catch (UnsupportedEncodingException e) {
//			e.printStackTrace();
//			throw new SmsException(resultMsg); 
//		} catch (MalformedURLException e) {
//			e.printStackTrace();
//			throw new SmsException(resultMsg); 
//		} catch (IOException e) {
//			e.printStackTrace();
//			throw new SmsException(resultMsg); 
//		}
//	}
//	
//	@Override
//	public void enviarSMSInfoBip(String to, String msg) throws SaiException, SmsException {
//		String user = "inovagri";
//		String password = "inovagri";
//		String resultMsg = "";
//		msg = removerAcentos(msg);
//		
//		try{
//			String connection =
//			"http://api2.infobip.com/api/sendsms/plain?user="+user +"&password="+ password +"&sender=SaI&SMSText="+ msg+"&GSM="+ to;
//			System.out.println("URL:" + connection);
//			connection.replace(" ", "%20");
//			URL url = new URL(connection);
//			       
//			InputStream input = url.openStream();
//			
//			StringWriter writer = new StringWriter();
//			IOUtils.copy(input, writer, "UTF-8");
//			resultMsg = writer.toString();
//			Long resultado = Long.parseLong(resultMsg);
//			if (resultado <= 0) {
//				throw new SmsException(resultMsg); 
//			}
//		} catch (UnsupportedEncodingException e) {
//			e.printStackTrace();
//			throw new SmsException(resultMsg); 
//		} catch (MalformedURLException e) {
//			e.printStackTrace();
//			throw new SmsException(resultMsg); 
//		} catch (IOException e) {
//			e.printStackTrace();
//			throw new SmsException(resultMsg); 
//		}
//	}
//	
//	@Override
//	public Long enviar(String msg, String dest) {
//		String user = "inovagri";
//		String password = "inovagri";
//		String resultMsg = "";
//		msg = removerAcentos(msg);
//		
//		try{
//			String connection =
//			"http://api2.infobip.com/api/sendsms/plain?user="+user +"&password="+ password +"&sender=SaI&SMSText="+ msg+"&GSM=55"+ dest;
//			connection = connection.replace(" ", "%20");
//			URL url = new URL(connection);
//			       
//			InputStream input = url.openStream();
//			StringWriter writer = new StringWriter();
//			IOUtils.copy(input, writer, "UTF-8");
//			resultMsg = writer.toString();
//		} catch (UnsupportedEncodingException e) {
//			System.out.println("Erro de UnsupportedEncodingException ao enviar para:" + dest);
//			e.printStackTrace();
//		} catch (MalformedURLException e) {
//			System.out.println("Erro de MalformedURLException ao enviar para:" + dest);
//			e.printStackTrace();
//		} catch (IOException e) {
//			System.out.println("Erro de IOException ao enviar para:" + dest);
//			e.printStackTrace();
//		}
//		
//		return resultMsg.equals("")?null:Long.parseLong(resultMsg);
//
//	}
//	
//	public static String removerAcentos(String str) {
//	    return Normalizer.normalize(str, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
//	}
	
}
