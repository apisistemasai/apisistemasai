package com.agrosolutions.sai.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.agrosolutions.sai.dao.TecnicoDAO;
import com.agrosolutions.sai.exception.SaiException;
import com.agrosolutions.sai.model.Distrito;
import com.agrosolutions.sai.model.Tecnico;
import com.agrosolutions.sai.service.TecnicoService;
import com.agrosolutions.sai.service.UsuarioService;

@Service("tecnicoService")
public class TecnicoServiceImpl implements TecnicoService {

	@Autowired
	private TecnicoDAO tecnicoDAO;
	@Autowired
	private UsuarioService usuarioService;

	@Transactional
	public Tecnico salvar(Tecnico tecnico) {
		tecnico.setDataCadastro(new Date());
		tecnico.getUsuario().setAtivo(true);
		tecnico.getUsuario().setNome(tecnico.getNome());
		tecnico.setUsuario(usuarioService.salvar(tecnico.getUsuario()));
		return tecnicoDAO.salvar(tecnico);
	}
	
	@Transactional
	public Tecnico editar(Tecnico tecnico) {
		tecnico.getUsuario().setConfirmarSenha(null);
		usuarioService.salvar(tecnico.getUsuario());
		return tecnicoDAO.salvar(tecnico);
	}

	public List<Tecnico> pesquisar(List<Distrito> distrito) {
		return tecnicoDAO.pesquisar(distrito);
	}

	public List<Tecnico> obterTodos() {
		return tecnicoDAO.obterTodos();
	}

	@Transactional
	@Override
	public void excluir(Tecnico tecnico) {
		tecnicoDAO.excluir(tecnico);
	}

	@Transactional
	@Override
	public List<Tecnico> pesquisar(Tecnico entidade, Integer first, Integer pageSize) {
		List<Tecnico> tecnicos = null;
		try {
			validarPesquisa(entidade);
		} catch (SaiException e) {
			throw new SaiException(e.getMessages());
		}
		tecnicos = tecnicoDAO.obterPorParametro(entidade, first, pageSize);
		
		return tecnicos;
	}

	private void validarPesquisa(Tecnico entidade) throws SaiException {
		List<String> messages = new ArrayList<String>();
				
		if (!messages.isEmpty()) {
			throw new SaiException(messages);
		}
	}
			
	@Transactional
	public int quantidade(Tecnico entidade) {
		int resultado;
		try {
			validarPesquisa(entidade);
		} catch (SaiException e) {
			throw new SaiException(e.getMessages());
		}
		entidade.setSortField("");
		
		resultado = tecnicoDAO.quantidade(entidade);
		return resultado;
	}
	
}
