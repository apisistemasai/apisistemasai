package com.agrosolutions.sai.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.agrosolutions.sai.dao.ParametroDAO;
import com.agrosolutions.sai.model.Parametro;
import com.agrosolutions.sai.service.ParametroService;

@Service("parametroService")
public class ParametroServiceImpl implements ParametroService {

	@Autowired
	private ParametroDAO parametroDAO;

	@Override
	public Parametro pesquisarPorCodigo(String codigo) {
		return parametroDAO.pesquisarPorCodigo(codigo);
	}


}
