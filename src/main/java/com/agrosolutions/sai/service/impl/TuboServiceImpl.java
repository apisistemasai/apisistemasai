package com.agrosolutions.sai.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.agrosolutions.sai.dao.TuboDAO;
import com.agrosolutions.sai.exception.SaiException;
import com.agrosolutions.sai.model.Fabricante;
import com.agrosolutions.sai.model.Irrigante;
import com.agrosolutions.sai.model.Tubo;
import com.agrosolutions.sai.service.TuboService;

@Service("tuboService")
public class TuboServiceImpl implements TuboService {

	@Autowired
	private TuboDAO tuboDAO;

	@Transactional
	public Tubo salvar(Tubo tubo) {
		return tuboDAO.salvar(tubo);
	}

	public List<Tubo> pesquisar(Fabricante fabricante) {
		return tuboDAO.pesquisar(fabricante);
	}
	@Override
	public List<Tubo> pesquisarPorIrrigante(Irrigante irrigante) {
		return tuboDAO.pesquisarPorIrrigante(irrigante);
	}

	@Override
	@Transactional
	public List<Tubo> pesquisar(Tubo tubo) {
		return tuboDAO.obterPorParametro(tubo, null, null);
	}
	
	public List<Tubo> obterTodos() {
		return tuboDAO.obterTodos();
	}

	@Transactional
	@Override
	public void excluir(Tubo tubo) {
		tuboDAO.excluir(tubo);
	}

	@Transactional
	@Override
	public List<Tubo> pesquisar(Tubo entidade, Integer first, Integer pageSize) {
		List<Tubo> tubos = null;
		try {
			validarPesquisa(entidade);
		} catch (SaiException e) {
			throw new SaiException(e.getMessages());
		}
		tubos = tuboDAO.obterPorParametro(entidade, first, pageSize);
		return tubos;
	}
	
	private void validarPesquisa(Tubo entidade) throws SaiException {
		List<String> messages = new ArrayList<String>();

		if (!messages.isEmpty()) {
			throw new SaiException(messages);
		}
	}

	@Transactional
	public int quantidade(Tubo entidade) {
		int resultado;
		try {
			validarPesquisa(entidade);
		} catch (SaiException e) {
			throw new SaiException(e.getMessages());
		}
		entidade.setSortField("");
		resultado = tuboDAO.quantidade(entidade);
		return resultado;
	}
}
