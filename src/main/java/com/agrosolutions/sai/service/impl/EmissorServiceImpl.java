package com.agrosolutions.sai.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.agrosolutions.sai.dao.EmissorDAO;
import com.agrosolutions.sai.exception.SaiException;
import com.agrosolutions.sai.model.Emissor;
import com.agrosolutions.sai.model.Fabricante;
import com.agrosolutions.sai.model.Plantio;
import com.agrosolutions.sai.model.Setor;
import com.agrosolutions.sai.service.EmissorService;

@Service("emissorService")
public class EmissorServiceImpl implements EmissorService {

	@Autowired
	private EmissorDAO emissorDAO;

	@Transactional
	public Emissor salvar(Emissor emissor) {
		return emissorDAO.salvar(emissor);
	}

	public List<Emissor> pesquisar(Fabricante fabricante) {
		return emissorDAO.pesquisar(fabricante);
	}

	public List<Emissor> obterTodos() {
		return emissorDAO.obterTodos();
	}

	@Transactional
	@Override
	public void excluir(Emissor emissor) {
		emissorDAO.excluir(emissor);
	}

	@Override
	public  List<Emissor> pesquisarPorSetor(Setor setor) {
		return emissorDAO.pesquisarPorSetor(setor);
	}

	@Override
	public  List<Emissor> pesquisarPorPlantio(Plantio p) {
		return emissorDAO.pesquisarPorPlantio(p);
	}

	@Override
	@Transactional
	public List<Emissor> pesquisar(Emissor emissor) {
		return emissorDAO.obterPorParametro(emissor, null, null);
	}

	@Transactional
	@Override
	public List<Emissor> pesquisar(Emissor entidade, Integer first, Integer pageSize) {
		List<Emissor> emissores = null;
		try {
			validarPesquisa(entidade);
		} catch (SaiException e) {
			throw new SaiException(e.getMessages());
		}
		emissores = emissorDAO.obterPorParametro(entidade, first, pageSize);
		return emissores;
	}
 
	private void validarPesquisa(Emissor entidade) throws SaiException {
		List<String> messages = new ArrayList<String>();

		if (!messages.isEmpty()) {
			throw new SaiException(messages);
		}
	}

	@Transactional
	public int quantidade(Emissor entidade) {
		int resultado;
		try {
			validarPesquisa(entidade);
		} catch (SaiException e) {
			throw new SaiException(e.getMessages());
		}
		entidade.setSortField("");
		resultado = emissorDAO.quantidade(entidade);
		return resultado;
	}
}
