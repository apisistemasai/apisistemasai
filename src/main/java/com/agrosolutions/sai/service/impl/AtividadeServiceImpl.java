package com.agrosolutions.sai.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.agrosolutions.sai.dao.AtividadeDAO;
import com.agrosolutions.sai.exception.SaiException;
import com.agrosolutions.sai.model.Atividade;
import com.agrosolutions.sai.service.AtividadeService;

@Service("atividadeService")
public class AtividadeServiceImpl implements AtividadeService {

	@Autowired
	private AtividadeDAO atividadeDAO;

	@Transactional
	public Atividade salvar(Atividade atividade) {
		return atividadeDAO.salvar(atividade);
	}

	public List<Atividade> obterTodos() {
		return atividadeDAO.obterTodos();
	}

	@Transactional
	@Override
	public void excluir(Atividade atividade) {
		atividadeDAO.excluir(atividade);
	}
	
	@Transactional
	@Override
	public List<Atividade> pesquisar(Atividade entidade, Integer first, Integer pageSize) {
		List<Atividade> atividades = null;
		try {
		} catch (Exception e) {
			throw new SaiException(e.getMessage());
		}
		atividades = atividadeDAO.obterPorParametro(entidade, first, pageSize);	
		
		return atividades;
	}
}
