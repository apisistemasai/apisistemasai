package com.agrosolutions.sai.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.agrosolutions.sai.dao.CategoriaDAO;
import com.agrosolutions.sai.exception.SaiException;
import com.agrosolutions.sai.model.Categoria;
import com.agrosolutions.sai.service.CategoriaService;

@Service("categoriaService")
public class CategoriaServiceImpl implements CategoriaService {
	
	@Autowired
	private CategoriaDAO categoriaDAO;

	@Transactional
	public Categoria salvar(Categoria c) {
		try {
			validar(c);
		} catch (SaiException e) {
			throw new SaiException(e.getMessages());
		}
		
		return categoriaDAO.salvar(c);
	}

	public List<Categoria> obterTodos() {
		return categoriaDAO.obterTodos();
	}
	
	@Override
	public List<Categoria> obterSuperiores() {
		return categoriaDAO.obterSuperiores();
	}
	
	private void validar(Categoria c) throws SaiException {
		List<String> messages = new ArrayList<String>();
	

		if (!messages.isEmpty()) {
			throw new SaiException(messages);
		}
	}

	@Transactional
	public void excluir(Categoria c) {
		categoriaDAO.excluir(c);
	}
}

