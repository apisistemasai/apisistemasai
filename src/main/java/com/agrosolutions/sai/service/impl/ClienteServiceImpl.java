package com.agrosolutions.sai.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.agrosolutions.sai.dao.ClienteDAO;
import com.agrosolutions.sai.exception.SaiException;
import com.agrosolutions.sai.model.Cliente;
import com.agrosolutions.sai.service.ClienteService;

@Service("clienteService")
public class ClienteServiceImpl implements ClienteService {
	
	@Autowired
	private ClienteDAO clienteDAO;

	@Transactional 
	public Cliente salvar(Cliente c) {
		try {
			validar(c);
		} catch (SaiException e) {
			throw new SaiException(e.getMessages());
		}
		
		return clienteDAO.salvar(c);
	}

	public List<Cliente> obterTodos() {
		return clienteDAO.obterTodos();
	}
	
	private void validar(Cliente c) throws SaiException {
		List<String> messages = new ArrayList<String>();
	

		if (!messages.isEmpty()) {
			throw new SaiException(messages);
		}
	}

	@Transactional
	public void excluir(Cliente c) {
		clienteDAO.excluir(c);
	}


	

}

