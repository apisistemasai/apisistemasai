package com.agrosolutions.sai.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.agrosolutions.sai.dao.TesteEficienciaDAO;
import com.agrosolutions.sai.model.Setor;
import com.agrosolutions.sai.model.TesteEficiencia;
import com.agrosolutions.sai.service.TesteEficienciaService;

@Service("testeEficienciaService")
public class TesteEficienciaServiceImpl implements TesteEficienciaService {

	@Autowired
	private TesteEficienciaDAO testeEficienciaDAO;

	@Transactional
	public TesteEficiencia salvar(TesteEficiencia t) {
		return testeEficienciaDAO.salvar(t);
	}

	@Override
	public List<TesteEficiencia> pesquisarPorSetor(Setor setor) {
		return testeEficienciaDAO.pesquisarPorSetor(setor);
	}

	@Override
	public TesteEficiencia pesquisarPorSetorData(Setor setor, Date data) {
		return testeEficienciaDAO.pesquisarPorSetorData(setor, data);
	}

	@Transactional
	public void excluir(TesteEficiencia t) {
		testeEficienciaDAO.excluir(t);
	}

	@Transactional
	public void salvar(List<TesteEficiencia> t) {
		testeEficienciaDAO.salvar(t);
		
	}
}
