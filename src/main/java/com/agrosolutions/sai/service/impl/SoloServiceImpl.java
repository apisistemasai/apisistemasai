package com.agrosolutions.sai.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.agrosolutions.sai.dao.SoloDAO;
import com.agrosolutions.sai.exception.SaiException;
import com.agrosolutions.sai.model.Solo;
import com.agrosolutions.sai.service.SoloService;

@Service("soloService")
public class SoloServiceImpl implements SoloService {

	@Autowired
	private SoloDAO soloDAO;

	@Transactional
	public Solo salvar(Solo solo) {
		return soloDAO.salvar(solo);
	}

	public List<Solo> pesquisar(String nome) {
		return soloDAO.pesquisar(nome);
	}
	public List<Solo> obterTodos() {
		return soloDAO.obterTodos();
	}

	@Transactional
	@Override
	public void excluir(Solo solo) {
		soloDAO.excluir(solo);
	}
	@Transactional
	@Override
	public List<Solo> pesquisar(Solo entidade, Integer first, Integer pageSize){
		List<Solo> solos = null;
		try {
			validarPesquisa(entidade);
		} catch (SaiException e) {
			throw new SaiException(e.getMessages());
		}
		solos = soloDAO.obterPorParametro(entidade, first, pageSize);
		return solos;
	}

	@Transactional
	@Override
	public int quantidade(Solo entidade) {
		int resultado;
		try {
			validarPesquisa(entidade);
		} catch (SaiException e) {
			throw new SaiException(e.getMessages());
		}
		entidade.setSortField("");
		resultado = soloDAO.quantidade(entidade);
		return resultado;
	}
	
	private void validarPesquisa(Solo entidade) throws SaiException {
		List<String> messages = new ArrayList<String>();

		if (!messages.isEmpty()) {
			throw new SaiException(messages);
		}
	}
}
