package com.agrosolutions.sai.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.agrosolutions.sai.dao.AguaDAO;
import com.agrosolutions.sai.exception.SaiException;
import com.agrosolutions.sai.model.Agua;
import com.agrosolutions.sai.model.Irrigante;
import com.agrosolutions.sai.service.AguaService;
import com.agrosolutions.sai.util.ResourceBundle;

@Service("aguaService")
public class AguaServiceImpl implements AguaService {

	@Autowired
	private AguaDAO aguaDAO;

	@Transactional
	@Override
	public List<Agua> salvarLote(List<Agua> aguas) {
		List<Agua> retorno = new ArrayList<Agua>();
		for (Agua a : aguas) {
			a = aguaDAO.salvar(a);
			retorno.add(a);
		}
		return retorno;
	}
	@Transactional
	public Agua salvar(Agua agua) {
		return aguaDAO.salvar(agua);
	}

	public List<Agua> pesquisar(Irrigante param) {
		return aguaDAO.pesquisar(param);
	}

	@Override
	public Agua pesquisarAnterior(Irrigante param, Date data) {
		Agua agua = null;
		try {
			agua = aguaDAO.pesquisarAnterior(param, data);
		} catch (EmptyResultDataAccessException e) {
			throw new SaiException(ResourceBundle.getMessage("aguaAnteriorErro")+ ".");
		}
		return agua; 
	}

	@Override
	public List<Agua> pesquisarPeriodo(Date data) {
		List<Agua> aguas = null;
		try {
			aguas = aguaDAO.pesquisarPeriodo(data);
		} catch (EmptyResultDataAccessException e) {
			throw new SaiException(ResourceBundle.getMessage("aguaPeriodoErro")+ ".");
		}
		return aguas; 
	}

	@Transactional
	@Override
	public void excluir(Agua agua) {
		aguaDAO.excluir(agua);
	}
	
	@Transactional
	@Override
	public int quantidade(Agua entidade) {
		int resultado;
		try {
			validarPesquisa(entidade);
		} catch (SaiException e) {
			throw new SaiException(e.getMessages());
		}
		entidade.setSortField("");
		resultado = aguaDAO.quantidade(entidade);
		return resultado;
	}

	@Transactional
	@Override
	public List<Agua> pesquisar(Agua entidade, Integer first, Integer pageSize) {
		List<Agua> aguas = null;
		try {
			validarPesquisa(entidade);
		} catch (SaiException e) {
			throw new SaiException(e.getMessages());
		}
		try {
			aguas = aguaDAO.obterPorParametro(entidade, first, pageSize);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return aguas;
	}
	
	private void validarPesquisa(Agua entidade) throws SaiException {
		List<String> messages = new ArrayList<String>();
		limparFiltros(entidade);

		if (!messages.isEmpty()) {
			throw new SaiException(messages);
		}
	}

	// Limpando atributos vazio para nulos para n�o entrar na clausula where
	public void limparFiltros(Agua entidade){
		if (entidade.getIrrigante().getLocalizacao() != null && entidade.getIrrigante().getLocalizacao().equals("")) {
			entidade.getIrrigante().setLocalizacao(null);
		}

	}
	
}
