package com.agrosolutions.sai.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.agrosolutions.sai.dao.NutrienteDAO;
import com.agrosolutions.sai.exception.SaiException;
import com.agrosolutions.sai.model.Nutriente;
import com.agrosolutions.sai.service.NutrienteService;

@Service("nutrienteService")
public class NutrienteServiceImpl implements NutrienteService {

	@Autowired
	private NutrienteDAO NutrienteDAO;

	@Transactional
	public Nutriente salvar(Nutriente nutriente) {
		return NutrienteDAO.salvar(nutriente);
	}

	public List<Nutriente> obterTodos() {
		return NutrienteDAO.obterTodos();
	}

	@Transactional
	@Override
	public void excluir(Nutriente nutriente) {
		NutrienteDAO.excluir(nutriente);
	}
	
	@Transactional
	@Override
	public List<Nutriente> pesquisar(Nutriente entidade, Integer first, Integer pageSize) {
		List<Nutriente> nutrientes = null;
		try {
		} catch (Exception e) {
			throw new SaiException(e.getMessage());
		}
		nutrientes = NutrienteDAO.obterPorParametro(entidade, first, pageSize);	
		
		return nutrientes;
	}
}
