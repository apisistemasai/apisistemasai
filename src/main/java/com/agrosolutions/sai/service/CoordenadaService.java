package com.agrosolutions.sai.service;

import java.util.List;

import com.agrosolutions.sai.model.Coordenada;
import com.agrosolutions.sai.model.Irrigante;


public interface CoordenadaService {

	void salvar(List<Coordenada> coordenadas);

	List<Coordenada> pesquisarPorIrrigante(Irrigante irrigante);
}
