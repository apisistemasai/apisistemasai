package com.agrosolutions.sai.service;

import java.util.List;

import com.agrosolutions.sai.model.Acesso;
import com.agrosolutions.sai.model.Usuario;

public interface AcessoService {
	
	Acesso salvar(Acesso a);
	Acesso pesquisarUltimo(String u);
	List<Acesso> pesquisarPorUsuario(Usuario u);
}
