package com.agrosolutions.sai.service;

import com.agrosolutions.sai.dao.UsuarioDAO;
import com.agrosolutions.sai.service.ServiceInterface;
import com.agrosolutions.sai.util.ResponseJson;
import com.google.gson.Gson;
import java.util.ArrayList;
import org.hibernate.JDBCException;

public class ServiceUser implements ServiceInterface {

    private ResponseJson json;
    private final Gson gson;
    //private final UsuarioDAO dao;

    public ServiceUser() {
//        this.dao = new UsuarioDAO();
        this.gson = new Gson();
    }

    public String getUsers() {
//        try {
//            json = new ResponseJson(true, "Carregar Usuários", (ArrayList<Us>) dao.list());
//            return gson.toJson(json);
//        } catch (JDBCException e) {
//            json = new ResponseJson(false, "Erro ao tentar carregar usuários", "");
//            return gson.toJson(json);
//        }
        return gson.toJson(json);
    }

    @Override
    public String merge(Object obj) {
//        try {
//            User user = (User) obj;
//            dao.merge(user);
//            if (user.getId() != null) {
//                json = new ResponseJson(true, user.getName(), "Alterado com Sucesso");
//            } else {
//                json = new ResponseJson(true, user.getName(), "Salvo com Sucesso");
//            }
//
//            return gson.toJson(json);
//        } catch (JDBCException e) {
//            json = new ResponseJson(false, "Erro ao inserir usuário", "");
//            return gson.toJson(json);
//        }
      return gson.toJson(json);
    }
}
