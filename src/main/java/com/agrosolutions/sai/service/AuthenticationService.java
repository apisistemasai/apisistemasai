package com.agrosolutions.sai.service;

import java.util.Date;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.agrosolutions.sai.dao.AcessoDAO;
import com.agrosolutions.sai.model.Acesso;
import com.agrosolutions.sai.model.Usuario;
import com.agrosolutions.sai.util.ResourceBundle;

@Component
public class AuthenticationService {

	@Autowired
	@Qualifier("authenticationManager")
	private AuthenticationManager authenticationManager;

	@Autowired
	private AcessoDAO acessoDAO;
	
	@Autowired
	private UsuarioService usuarioService;
	
	@Transactional
	public String login(String username, String password) {
		Usuario usuario = usuarioService.pesquisarPorLogin(username);
		if (usuario == null){
			return ResourceBundle.getMessage("loginInvalido");
		}

		try {
			// criptografia MD5
			password = toMd5(password);

			// autenticando spring security
			UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(username, password);
			Authentication authenticate = authenticationManager.authenticate(token);
			SecurityContextHolder.getContext().setAuthentication(authenticate);

			// pegando o usuario
			usuario = (Usuario) getUsuarioLogado();
			
			// verificando se o usuario esta ativo
			if (!usuario.isEnabled()) {
				return ResourceBundle.getMessage("usuarioInativo");
			}

			// Verificando se a senha esta expirada
			if (!usuario.isAccountNonExpired()) {
				return ResourceBundle.getMessage("senhaExpirada");
			}

/*			// verificando se o usuario tem alguma permissao
			if (usuario.getAuthorities().size() == 0) {
				return "Usu�rio n�o tem permiss�o de acesso";
			}
*/
			// verificando autenticacao
			if (authenticate.isAuthenticated()) {
				//Criar registro de acesso.
				Acesso acesso = new Acesso(new Date(), usuario);
				acessoDAO.salvar(acesso);
				return "ok";
			}

		}catch (AuthenticationException e) {
			e.printStackTrace();
		}
		
		return ResourceBundle.getMessage("loginInvalido");
	}


	public void logout() {
		SecurityContextHolder.getContext().setAuthentication(null);
		invalidateSession();
	}


	public Usuario getUsuarioLogado() {
		return (Usuario) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	}

	private void invalidateSession() {
		FacesContext fc = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) fc.getExternalContext().getSession(false);
		session.invalidate();
	}


	/**
	 * Encripta valores com hash md5
	 * @param String valor
	 * @return String md5
	 */
	private String toMd5(String valor){
		Md5PasswordEncoder encoder = new Md5PasswordEncoder();
		return encoder.encodePassword(valor, null);
	}
}
