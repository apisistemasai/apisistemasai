package com.agrosolutions.sai.service;

import java.util.List;

import com.agrosolutions.sai.model.FertirrigacaoAplicada;

public interface FertirrigacaoAplicadaService {

	FertirrigacaoAplicada salvar(FertirrigacaoAplicada atividade);

	List<FertirrigacaoAplicada> obterTodos();

	void excluir(FertirrigacaoAplicada atividade);

	List<FertirrigacaoAplicada> pesquisar(FertirrigacaoAplicada entidade,
			Integer first, Integer pageSize);

}
