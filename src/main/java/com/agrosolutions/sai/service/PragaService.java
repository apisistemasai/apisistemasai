package com.agrosolutions.sai.service;

import java.util.List;

import com.agrosolutions.sai.model.Praga;

public interface PragaService {
	
	Praga salvar(Praga praga);
	List<Praga> obterTodos();
	void excluir(Praga praga);
	List<Praga> pesquisar(Praga enitdade, Integer first, Integer pageSize);
}
