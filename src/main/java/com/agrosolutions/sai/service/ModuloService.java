package com.agrosolutions.sai.service;

import java.util.List;

import com.agrosolutions.sai.model.Modulo;

public interface ModuloService {
	
	Modulo salvar(Modulo modulos);
	List<Modulo> obterTodos();
}
