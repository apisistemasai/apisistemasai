package com.agrosolutions.sai.service;

import java.util.List;

import com.agrosolutions.sai.model.Bomba;
import com.agrosolutions.sai.model.Fabricante;
import com.agrosolutions.sai.model.Irrigante;

public interface BombaService {
	
	Bomba salvar(Bomba bomba);
	List<Bomba> pesquisar(Fabricante entidade);
	void excluir(Bomba bomba);
	List<Bomba> obterTodos();
	List<Bomba> pesquisarPorIrrigante(Irrigante irrigante);
	List<Bomba> pesquisar(Bomba bomba);
	List<Bomba> pesquisar(Bomba bombaBusca, Integer first, Integer pageSize);
	int quantidade(Bomba entidade);


}
