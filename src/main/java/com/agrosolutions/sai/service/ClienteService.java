package com.agrosolutions.sai.service;

import java.util.List;

import com.agrosolutions.sai.model.Cliente;

public interface ClienteService {
	
	Cliente salvar(Cliente c);
	List<Cliente> obterTodos();
	void excluir(Cliente c); 
}
