package com.agrosolutions.sai.service;

import java.util.List;

import com.agrosolutions.sai.model.Fabricante;
import com.agrosolutions.sai.model.Irrigante;
import com.agrosolutions.sai.model.Valvula;

public interface ValvulaService {
	
	Valvula salvar(Valvula valvula);
	List<Valvula> pesquisar(Fabricante entidade);
	void excluir(Valvula valvula);
	List<Valvula> obterTodos();
	List<Valvula> pesquisarPorIrrigante(Irrigante irrigante);
	List<Valvula> pesquisar(Valvula valvula);
	List<Valvula> pesquisar(Valvula entidade, int first, int pageSize);
	int quantidade(Valvula valvulaBusca);
}
