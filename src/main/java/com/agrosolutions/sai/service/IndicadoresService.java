package com.agrosolutions.sai.service;

import java.util.Date;
import java.util.List;

import com.agrosolutions.sai.model.AtividadeEconomica;
import com.agrosolutions.sai.model.BaciaHidrografica;
import com.agrosolutions.sai.model.GraficoBarra;
import com.agrosolutions.sai.model.GraficoPizzaIndicadores;
import com.agrosolutions.sai.model.Indicadores;
import com.agrosolutions.sai.model.Municipio;

public interface IndicadoresService {

	Long totalMunicipiosPorBacia(List<BaciaHidrografica> baciasPesquisa);

	Indicadores salvar(Indicadores indicadores);

	Indicadores pesquisarPorAtividadeEconomica(AtividadeEconomica atividadeEconomica, Date data);
	
	List<Date> pesquisarDatasPorAtividade(AtividadeEconomica atividadeEconomica);

	List<Date> pesquisarDatasPorMunicipio(Municipio municipio);

	List<Date> pesquisarDatasPorBacia(BaciaHidrografica bacia);

	List<Indicadores> pesquisarPorMunicipio(Municipio municipio, Date data);

	List<Indicadores> pesquisarPorBacia(BaciaHidrografica bacia, Date data);

	List<GraficoPizzaIndicadores> totalHaPorCulturaPorMunicipio(Municipio municipio);

	List<GraficoBarra> culturasHaPorMunicipio(Municipio municipio);

	List<GraficoBarra> graficoIndicadoresSegurancaKgM3(Municipio municipio);

	List<GraficoBarra> graficoIndicadoresEconomicoReceitaLiquidaHa(Municipio municipio);

	List<GraficoBarra> graficoIndicadoresEconomicoReceitaLiquidaM3(Municipio municipio);

	List<GraficoBarra> graficoIndicadoresSegSocialEmpregosHa(Municipio municipio);

	List<GraficoBarra> graficoIndicadoresSegSocialEmpregosM3(Municipio municipio);

	List<GraficoBarra> graficoIndicadoresSegHidricaM3Ha(Municipio municipio);

	List<GraficoBarra> graficoIndicadoresSegHidricaLitrosSegundoHa(Municipio municipio);

	List<GraficoPizzaIndicadores> totalHaPorCulturaPorBacia(BaciaHidrografica bacia);
	
	List<GraficoBarra> graficoIndicadoresSegurancaKgHaBacia(BaciaHidrografica bacia);
	
	List<GraficoBarra> graficoIndicadoresSegurancaKgM3Bacia(BaciaHidrografica bacia);

	List<GraficoBarra> graficoIndicadoresSegurancaEconomicaReceitaHaBacia(BaciaHidrografica bacia);

	List<GraficoBarra> graficoIndicadoresSegurancaEconomicaReceitaM3Bacia(BaciaHidrografica bacia);

	List<GraficoBarra> graficoIndicadoresSegurancaSocialEmpregosHaBacia(BaciaHidrografica bacia);

	List<GraficoBarra> graficoIndicadoresSegurancaSocialEmpregosM3Bacia(BaciaHidrografica bacia);

	List<GraficoBarra> graficoIndicadoresSegurancaHidricaM3HaBacia(BaciaHidrografica bacia);

	List<GraficoBarra> graficoIndicadoresSegurancaHidricaLitrosSegundoHaBacia(BaciaHidrografica bacia);

	List<GraficoBarra> graficoIndicadoresCorteHidrico(Municipio municipio);

	List<GraficoBarra> graficoCorteHidricoBacia(BaciaHidrografica bacia);

	List<GraficoBarra> graficoIndicadoresSegurancaEconomicaVBPBacia(BaciaHidrografica bacia);

	List<GraficoBarra> graficoIndicadoresEconomicoVBP(Municipio municipio);

	List<GraficoBarra> graficoIndicadoresEconomicoReceitaLiquida(Municipio municipio);

	List<GraficoBarra> graficoIndicadoresSegurancaEconomicaReceitaBacia(BaciaHidrografica bacia);

	List<GraficoPizzaIndicadores> totalCicloCulturaPorBacia(BaciaHidrografica bacia);

	List<GraficoPizzaIndicadores> graficoIndicadoresCicloCultura(Municipio municipio);

	List<Date> pesquisarDatas();

	List<Indicadores> pesquisarPorData(Date data);
}
