package com.agrosolutions.sai.service;

import java.util.Date;
import java.util.List;

import com.agrosolutions.sai.model.Agua;
import com.agrosolutions.sai.model.Irrigante;

public interface AguaService {
	
	Agua salvar(Agua agua);
	List<Agua> pesquisar(Irrigante param);
	void excluir(Agua agua);
	List<Agua> pesquisar(Agua entidade, Integer first, Integer pageSize);
	int quantidade(Agua entidade);
	List<Agua> salvarLote(List<Agua> aguas);
	Agua pesquisarAnterior(Irrigante param, Date data);
	List<Agua> pesquisarPeriodo(Date data);
}
