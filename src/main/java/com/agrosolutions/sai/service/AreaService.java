package com.agrosolutions.sai.service;

import java.util.List;

import com.agrosolutions.sai.model.Area;
import com.agrosolutions.sai.model.Distrito;

public interface AreaService {
	
	Area salvar(Area area);
	List<Area> pesquisar(List<Distrito> distrito);
	void excluir(Area area);
	List<Area> pesquisarArea(Area entidade, Integer first, Integer pageSize);
}
