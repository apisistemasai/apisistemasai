package com.agrosolutions.sai.service;

import java.util.List;

import com.agrosolutions.sai.model.Distrito;

public interface DistritoService {
	
	Distrito salvar(Distrito distrito);
	List<Distrito> obterTodos(List<Distrito> distritos);
	void excluir(Distrito distrito);
	List<Distrito> pesquisar(Distrito entidade, Integer first, Integer pageSize);
	List<Distrito> pesquisarTodos();
}
