package com.agrosolutions.sai.service;

import java.util.List;

import com.agrosolutions.sai.model.Cultivo;
import com.agrosolutions.sai.model.Cultura;
import com.agrosolutions.sai.model.Irrigante;
import com.agrosolutions.sai.model.Variedade;

public interface CultivoService {
	
	Cultivo salvar(Cultivo cultivo);
	List<Cultivo> pesquisar(Irrigante irrigante);
	List<Variedade> pesquisarVariedades(Irrigante irrigante);
	List<Cultura> pesquisarCulturas(Irrigante irrigante);
	void excluir(Cultivo cultivo);
}
