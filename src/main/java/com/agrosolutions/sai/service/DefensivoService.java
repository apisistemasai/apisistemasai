package com.agrosolutions.sai.service;

import java.util.List;

import com.agrosolutions.sai.model.Defensivo;

public interface DefensivoService {
	
	Defensivo salvar(Defensivo defensivo);
	List<Defensivo> obterTodos();
	void excluir(Defensivo defensivo);
	List<Defensivo> pesquisar(Defensivo entidade, Integer first, Integer pageSize);
}
