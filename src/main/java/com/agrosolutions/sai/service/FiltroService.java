package com.agrosolutions.sai.service;

import java.util.List;

import com.agrosolutions.sai.model.Fabricante;
import com.agrosolutions.sai.model.Filtro;
import com.agrosolutions.sai.model.Irrigante;

public interface FiltroService {
	
	Filtro salvar(Filtro filtro);
	List<Filtro> pesquisar(Fabricante entidade);
	void excluir(Filtro filtro);
	List<Filtro> obterTodos();
	List<Filtro> pesquisarPorIrrigante(Irrigante irrigante);
	List<Filtro> pesquisar(Filtro filtro);
	List<Filtro> pesquisar(Filtro entidade, Integer first, Integer pageSize);
	int quantidade(Filtro filtroBusca);
}
