package com.agrosolutions.sai.service;

import java.util.List;

import com.agrosolutions.sai.model.Fornecedor;

public interface FornecedorService {
	
	Fornecedor salvar(Fornecedor c);
	List<Fornecedor> obterTodos();
	void excluir(Fornecedor c);
}
