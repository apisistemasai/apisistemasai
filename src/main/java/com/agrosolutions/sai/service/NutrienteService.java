package com.agrosolutions.sai.service;

import java.util.List;

import com.agrosolutions.sai.model.Nutriente;

public interface NutrienteService {
	
	Nutriente salvar(Nutriente atividade);
	List<Nutriente> obterTodos();
	void excluir(Nutriente atividade);
	List<Nutriente> pesquisar(Nutriente entidade, Integer first, Integer pageSize);
	
}
