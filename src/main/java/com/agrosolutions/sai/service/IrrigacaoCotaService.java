package com.agrosolutions.sai.service;

import java.math.BigDecimal;
import java.util.List;

import com.agrosolutions.sai.model.Distrito;
import com.agrosolutions.sai.model.IrrigacaoCota;
import com.agrosolutions.sai.model.Irrigante;


public interface IrrigacaoCotaService {
	BigDecimal calcularAreaIrrigadaTotal(List<Distrito> distritos);
	BigDecimal calcularVolumeDisponivelHa( BigDecimal volumeDisponivelTotal, BigDecimal areaIrrigadaTotal, BigDecimal eficienciaDistribuicao, BigDecimal diasIrrigadosSemana);
	List<Irrigante> consultarIrrigantes();
	IrrigacaoCota calcularCotaPorIrrigante(Irrigante irrigante, BigDecimal volumeDisponivelPorHa);
}