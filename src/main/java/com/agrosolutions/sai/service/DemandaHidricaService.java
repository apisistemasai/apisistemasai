package com.agrosolutions.sai.service;

import java.util.List;

import com.agrosolutions.sai.model.Area;
import com.agrosolutions.sai.model.Clima;
import com.agrosolutions.sai.model.DemandaHidrica;

public interface DemandaHidricaService {

	List<DemandaHidrica> salvar(Area a);

	void salvarDemandaHidrica(Clima clima);
	
}