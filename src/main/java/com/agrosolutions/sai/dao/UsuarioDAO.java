package com.agrosolutions.sai.dao;

import java.util.List;

import com.agrosolutions.sai.exception.SaiException;
import com.agrosolutions.sai.model.Usuario;

public interface UsuarioDAO extends DAO<Usuario> {

	public Usuario pesquisar(String username);
	void excluir(Usuario usuario);
	int alterarSenha(Usuario usuario) throws SaiException;
	public void alterarDistritoPadrao(Usuario usuario) throws SaiException;
	public List<Usuario> pesquisarPorEmail(String email);
	
}
