package com.agrosolutions.sai.dao;

import java.util.Date;
import java.util.List;

import com.agrosolutions.sai.model.Irrigacao;
import com.agrosolutions.sai.model.Irrigante;
import com.agrosolutions.sai.model.Plantio;

public interface IrrigacaoDAO extends DAO<Irrigacao> {

	List<Irrigacao> pesquisarPorPlantio(Plantio p);
	Irrigacao pesquisar(Plantio p, Date data);
	List<Irrigacao> pesquisarPorData(Date d);
	List<Irrigacao> pesquisarPorIrriganteData(Date d, Irrigante ir);
	void excluir(Irrigacao irrigacao);
	void enviarSMS(Irrigacao irrigacao);
	List<Irrigacao> pesquisarPorIrrigantePeriodo(Date dIni, Date dFim,
			Irrigante ir);
	void deletar(Plantio p);
}
