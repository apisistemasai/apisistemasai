package com.agrosolutions.sai.dao;

import java.util.List;

import com.agrosolutions.sai.model.Categoria;

public interface CategoriaDAO extends DAO<Categoria> {

	void excluir(Categoria c);

	List<Categoria> obterSuperiores();

}
