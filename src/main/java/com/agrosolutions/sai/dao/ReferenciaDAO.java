package com.agrosolutions.sai.dao;

import java.util.List;

import com.agrosolutions.sai.model.Cultura;
import com.agrosolutions.sai.model.Referencia;
import com.agrosolutions.sai.model.Variedade;

public interface ReferenciaDAO extends DAO<Referencia> {

	List<Referencia> pesquisar(Variedade variedade);
	void excluir(Referencia referencia);
	List<Referencia> pesquisarCultura(Cultura c);
}
