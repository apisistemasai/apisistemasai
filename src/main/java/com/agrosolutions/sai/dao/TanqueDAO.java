package com.agrosolutions.sai.dao;

import com.agrosolutions.sai.model.Tanque;

public interface TanqueDAO extends DAO<Tanque> {

	void excluir(Tanque elemento);
}
