package com.agrosolutions.sai.dao;

import java.util.List;

import com.agrosolutions.sai.model.Energia;
import com.agrosolutions.sai.model.Irrigante;

public interface EnergiaDAO extends DAO<Energia> {

	List<Energia> pesquisar(Irrigante param);
	void excluir(Energia energia);
}
