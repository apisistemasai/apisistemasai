package com.agrosolutions.sai.dao;

import com.agrosolutions.sai.model.Conta;

public interface ContaDAO extends DAO<Conta> {

	void excluir(Conta c);

}
