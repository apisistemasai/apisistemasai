package com.agrosolutions.sai.dao;

import com.agrosolutions.sai.model.Colheita;

public interface ColheitaDAO extends DAO<Colheita> {

	void excluir(Colheita colheita);

}
