package com.agrosolutions.sai.dao;

import java.util.List;

import com.agrosolutions.sai.model.Solo;

public interface SoloDAO extends DAO<Solo> {

	List<Solo> pesquisar(String nome);
	void excluir(Solo solo);

}
