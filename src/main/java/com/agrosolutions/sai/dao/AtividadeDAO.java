package com.agrosolutions.sai.dao;

import com.agrosolutions.sai.model.Atividade;

public interface AtividadeDAO extends DAO<Atividade> {

	void excluir(Atividade atividade);
}
