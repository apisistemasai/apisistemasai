package com.agrosolutions.sai.dao;

import com.agrosolutions.sai.model.Parametro;

public interface ParametroDAO extends DAO<Parametro> {

	Parametro pesquisarPorCodigo(String codigo);
}
