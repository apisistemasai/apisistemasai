package com.agrosolutions.sai.dao;

import java.util.List;

import com.agrosolutions.sai.model.Fabricante;
import com.agrosolutions.sai.model.Irrigante;
import com.agrosolutions.sai.model.Valvula;

public interface ValvulaDAO extends DAO<Valvula> {

	List<Valvula> pesquisar(Fabricante fabricante);

	void excluir(Valvula valvula);

	List<Valvula> pesquisarPorIrrigante(Irrigante irrigante);
}
