package com.agrosolutions.sai.dao;

import com.agrosolutions.sai.model.FertirrigacaoAplicada;

public interface FertirrigacaoAplicadaDAO extends DAO<FertirrigacaoAplicada> {

	void excluir(FertirrigacaoAplicada atividade);
}
