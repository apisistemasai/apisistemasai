package com.agrosolutions.sai.dao;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.persistence.NamedQuery;

import com.agrosolutions.sai.model.Model;

/**
 *
 * @author Raphael
 */

public interface DAO<Entidade extends Model> extends Serializable {

	/**
	 * Persiste ou atualiza a entidade
	 *
	 * @param entidade
	 * @return a propria entidade salva
	 */
	Entidade salvar(Entidade entidade);

	/**
	 * Persiste ou atualiza as entidades
	 *
	 * @param entidade
	 */
	void salvar(List<Entidade> entidades);

	/**
	 * Remove a entidade
	 *
	 * @param entidade
	 */
	void remover(Entidade entidade);

	/**
	 * Remove as entidades
	 *
	 * @param entidade
	 */
	void remover(List<Entidade> entidades);

	/**
	 * Obtem todos as entidades do modelo gerenciado pelo Manager
	 *
	 * @return
	 */
	List<Entidade> obterTodos();

	/**
	 * Obtem a entidade com determinado id
	 *
	 * @param idEntidade
	 * @return entidade caso exista, null caso nao exista
	 */
	Entidade obterPorId(Long idEntidade);

	/**
	 * Obtem as entidades que contem os mesmos valores de atributos da entidade passada por
	 * parametro comešando de <code>indiceInicio</code> com o maximo de registros igual a
	 * <code>tamanhoPagina</code>.
	 *
	 * @param entidade
	 * @param indiceInicio
	 *            indice do primeira entidade que sera retornada
	 * @param quantidade
	 *            quantidade de entidades
	 * @return
	 */
	public List<Entidade> obterPorParametro(Entidade entidade, Integer indiceInicio, Integer quantidade);

	/**
	 * Obtem a quantidade (count) de entidades que contem os mesmos valores de atributos da entidade
	 * passada por parametro
	 *
	 * @param entidade
	 * @return
	 */
	int quantidade(Entidade entidade);

	/**
	 * Obtem entidades executando de um {@link NamedQuery}
	 *
	 * @param query
	 * @param parametros
	 * @return
	 */
	public List<Entidade> executarQuery(String query, Map<String, Object> parametros, Integer indice, Integer qtdItemMaximo);


	/**
	 * Obtem referencia de uma entidade</code>
	 *
	 * @param id
	 * @return
	 */	
	Entidade obterPorReferencia(Long id);

	/**
	 * Executa uma query q retorna uma entidade</code>
	 *
	 * @param query
	 * @param parametros
	 * @return
	 */	
	Entidade executarQuery(String query, Map<String, Object> parametros);	
	
	int executarComando(String query, Map<String, Object> parametros);	
	
}
