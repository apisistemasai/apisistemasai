package com.agrosolutions.sai.dao;

import com.agrosolutions.sai.model.Modulo;

public interface ModuloDAO extends DAO<Modulo> {

}
