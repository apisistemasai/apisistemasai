package com.agrosolutions.sai.dao;

import com.agrosolutions.sai.model.Praga;

public interface PragaDAO extends DAO<Praga> {

	void excluir(Praga praga);

}
