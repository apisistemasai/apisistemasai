package com.agrosolutions.sai.dao;

import java.math.BigDecimal;
import java.util.List;

import com.agrosolutions.sai.model.Area;
import com.agrosolutions.sai.model.Distrito;
import com.agrosolutions.sai.model.Irrigante;
import com.agrosolutions.sai.model.Setor;

public interface SetorDAO extends DAO<Setor> {
 
	List<Setor> pesquisar(Irrigante param);
	void excluir(Setor setor);
	Long totalPorArea(Area a);
	Long totalPorDistrito(Distrito d);
	BigDecimal totalHecPorArea(Area a);
	BigDecimal totalHecPorDistrito(Distrito d);
	BigDecimal totalHecComPlantioPorDistrito(Distrito d);
	BigDecimal totalHecComPlantioPorArea(Area a);
	BigDecimal totalHecComPlantioIrrigadoPorDistrito(Distrito d);
}
