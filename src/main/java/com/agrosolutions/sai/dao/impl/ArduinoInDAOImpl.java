package com.agrosolutions.sai.dao.impl;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.agrosolutions.sai.dao.ArduinoInDAO;
import com.agrosolutions.sai.model.ArduinoIn;

@Repository
public class ArduinoInDAOImpl extends DAOImpl<ArduinoIn> implements ArduinoInDAO {

	private static final long serialVersionUID = -3486186314977645569L;
	static Logger logger = Logger.getLogger(ArduinoInDAOImpl.class);

}
