package com.agrosolutions.sai.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.map.HashedMap;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.primefaces.model.SortOrder;
import org.springframework.stereotype.Repository;

import com.agrosolutions.sai.dao.DistritoDAO;
import com.agrosolutions.sai.model.Distrito;

@Repository
public class DistritoDAOImpl extends DAOImpl<Distrito> implements DistritoDAO {
	
	private static final long serialVersionUID = 6219516014821791631L;
	static Logger logger = Logger.getLogger(DistritoDAOImpl.class);

    @Override
    public void excluir(Distrito distrito) {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
    	parametros.put("pId",distrito.getId());
        executarComando("Distrito.excluir", parametros);
    }	

    @Override
    public List<Distrito> obterTodos(List<Distrito> distritos) {
		@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
		parametros.put("pDistrito", prepararParametros(distritos));
    	
    	return executarQuery("Distrito.todos", parametros, null, null);
    }	
    @Override
	public Criteria criarCriteriaExample(Distrito entidade, MatchMode match) {

		Criteria criteria = super.criarCriteriaExample(entidade, match);
				
		criteria = prepararCriteria(entidade, criteria);		
		return criteria;
	}

	private List<Long> prepararParametros(List<Distrito> entidades) {
		List<Long> codigos = new ArrayList<Long>();
		for (Distrito d : entidades) {
			try {
				codigos.add(d.getId());
			} catch (NumberFormatException e) {
			}
		}
		return codigos;
	}	
	
    private Criteria prepararCriteria(Distrito entidadeLocal, Criteria criteria) {

    	List<Long> parametros = prepararParametros(entidadeLocal.getDistritos());
		criteria.add(Restrictions.in("id", parametros));
		
		if (entidadeLocal.getSortField().equals("nome")) {
			if (entidadeLocal.getSortOrder().equals(SortOrder.DESCENDING)) {
				criteria.addOrder(Order.desc("nome"));
			}else{
				criteria.addOrder(Order.asc("nome"));
			}
		}
		return criteria;	
	}
    
    @Override
    public List<Distrito> pesquisarTodos() {
    	return executarQuery("Distrito.pesquisarTodos", null, null, null);
    }
    
}
