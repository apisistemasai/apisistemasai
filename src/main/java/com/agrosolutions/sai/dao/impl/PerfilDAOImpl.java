package com.agrosolutions.sai.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.map.HashedMap;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.primefaces.model.SortOrder;
import org.springframework.stereotype.Repository;

import com.agrosolutions.sai.dao.PerfilDAO;
import com.agrosolutions.sai.model.Perfil;
import com.agrosolutions.sai.model.TipoPerfil;

@Repository
public class PerfilDAOImpl extends DAOImpl<Perfil> implements PerfilDAO {
	
	private static final long serialVersionUID = -5355465769303979780L;
	static Logger logger = Logger.getLogger(PerfilDAOImpl.class);
	
    @Override
    public void excluir(Perfil perfil) {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
    	parametros.put("pId",perfil.getId());
        executarComando("Perfil.excluir", parametros);
    }	

    @Override
    public List<Perfil> obterTodos() {
    	return executarQuery("Perfil.todos", null, null, null);
    }	

    @Override
    public List<Perfil> porTipo(TipoPerfil tipo) {
    	@SuppressWarnings({ "unchecked", "rawtypes" })
		Map<String, Object> parametros = new HashMap();
    	parametros.put("pTipo", tipo);
    	return executarQuery("Perfil.tipo", parametros, null, null);
    }	
    
    @Override
	public Criteria criarCriteriaExample(Perfil entidade, MatchMode match) {

		Criteria criteria = super.criarCriteriaExample(entidade, match);
				
		criteria = prepararCriteria(entidade, criteria);		
		return criteria;
	}

	private Criteria prepararCriteria(Perfil entidadeLocal, Criteria criteria) {
		if (entidadeLocal.getSortField().equals("descricao")) {
			if (entidadeLocal.getSortOrder().equals(SortOrder.DESCENDING)) {
				criteria.addOrder(Order.desc("descricao"));
			}else{
				criteria.addOrder(Order.asc("descricao"));
			}
		}
		return criteria;
		
	}
}
