package com.agrosolutions.sai.dao.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.collections.map.HashedMap;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.primefaces.model.SortOrder;
import org.springframework.stereotype.Repository;

import com.agrosolutions.sai.dao.IrriganteDAO;
import com.agrosolutions.sai.model.Area;
import com.agrosolutions.sai.model.Cultura;
import com.agrosolutions.sai.model.Distrito;
import com.agrosolutions.sai.model.Irrigante;
import com.agrosolutions.sai.model.TipoIrrigante;
import com.agrosolutions.sai.model.Usuario;
import com.agrosolutions.sai.model.Variedade;

@Repository
public class IrriganteDAOImpl extends DAOImpl<Irrigante> implements IrriganteDAO {
	
	private static final long serialVersionUID = 8119063647120095101L;
	static Logger logger = Logger.getLogger(IrriganteDAOImpl.class);

	@Override
	public Criteria criarCriteriaExample(Irrigante entidade, MatchMode match) {

		Criteria criteria = super.criarCriteriaExample(entidade, match);
				
		criteria = prepararCriteria(entidade, criteria);		
		return criteria;
	}

	private Criteria prepararCriteria(Irrigante entidadeLocal, Criteria criteria) {
		Criteria d = null;
		
		if (entidadeLocal.getSortField().equals("tipoIrrigante.descricao")) {
			if (entidadeLocal.getSortOrder().equals(SortOrder.DESCENDING)) {
				criteria.addOrder(Order.desc("tipoIrrigante"));
			}else{
				criteria.addOrder(Order.asc("tipoIrrigante"));
			}
		}
		if (entidadeLocal.getSortField().equals("nome")) {
			if (entidadeLocal.getSortOrder().equals(SortOrder.DESCENDING)) {
				criteria.addOrder(Order.desc("nome"));
			}else{
				criteria.addOrder(Order.asc("nome"));
			}
		}
		if (entidadeLocal.getSortField().equals("localizacao")) {
			if (entidadeLocal.getSortOrder().equals(SortOrder.DESCENDING)) {
				criteria.addOrder(Order.desc("localizacao"));
			}else{
				criteria.addOrder(Order.asc("localizacao"));
			}
		}

		d = criteria.createCriteria("area");
		criteria = d.add(Restrictions.in("distrito", entidadeLocal.getDistritos()));
		
		if (entidadeLocal.getArea() != null) {
			if (entidadeLocal.getSortField().equals("area.nome")) {
				if (entidadeLocal.getSortOrder().equals(SortOrder.DESCENDING)) {
					d.addOrder(Order.desc("nome"));
				}else{
					d.addOrder(Order.asc("nome"));
				}
			}
			if (entidadeLocal.getArea().getId() != null){
				d.add(Restrictions.eq("id", entidadeLocal.getArea().getId()));
			}else if (entidadeLocal.getArea().getDistrito() != null) {
				Criteria r = d.createCriteria("distrito");   
				r.add(Restrictions.eq("id", entidadeLocal.getArea().getDistrito().getId()));   
			}
		}

		return criteria;
	}

    @Override
    public void excluir(Irrigante irrigante) {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
    	parametros.put("pId",irrigante.getId());
        executarComando("Irrigante.excluir", parametros);
    }	
	
    @Override
    public Irrigante pesquisarPorUsuario(Usuario usu) {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
    	parametros.put("pUsuario", usu);
    	
		return executarQuery("Irrigante.pesquisarPorUsuario", parametros);
    }	
    
    @Override
    public Irrigante pesquisarPorLote(String lote, List<Distrito> distritos) {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
    	parametros.put("pLote", lote);
    	parametros.put("pDistrito", distritos);
    	
		return executarQuery("Irrigante.pesquisarPorLote", parametros);
    }	

    @Override
    public List<Irrigante> pesquisarPorCultura(Irrigante entidade, Integer first, Integer pageSize) {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
    	parametros.put("pCultura", entidade.getCultura().getId());
    	parametros.put("pTipo", entidade.getTipoIrrigante());
    	parametros.put("pArea", entidade.getArea());
    	parametros.put("pDistrito", entidade.getDistritos());

    	Query namedQuery = getSession().getNamedQuery("Irrigante.pesquisarPorCultura"); 
		String query = namedQuery.getQueryString();
    	if (entidade.getSortField()!=null && !entidade.getSortField().isEmpty()) {
    		if (entidade.getSortField().equals("tipoIrrigante.descricao")) {
    			entidade.setSortField("tipoIrrigante");
			}
    		if (entidade.getSortField().equals("area.nome")) {
    			entidade.setSortField("area");
			}
    		query += " ORDER BY i." + entidade.getSortField() + " " + (entidade.getSortOrder().equals(SortOrder.DESCENDING)?"DESC":"ASC");
		}
    	
		return executarQuery(query, parametros, first, pageSize);
    }	

    @Override
    public List<Irrigante> pesquisarPorVariedade(Irrigante entidade, Integer first, Integer pageSize) {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
    	parametros.put("pVariedade", entidade.getVariedade().getId());
    	
    	parametros.put("pTipo", entidade.getTipoIrrigante());
    	parametros.put("pArea", entidade.getArea());
    	parametros.put("pDistrito", entidade.getDistritos());

    	Query namedQuery = getSession().getNamedQuery("Irrigante.pesquisarPorVariedade"); 
		String query = namedQuery.getQueryString();
    	if (entidade.getSortField()!=null) {
    		if (entidade.getSortField().equals("tipoIrrigante.descricao")) {
    			entidade.setSortField("tipoIrrigante");
			}
    		if (entidade.getSortField().equals("area.nome")) {
    			entidade.setSortField("area");
			}
    		if (entidade.getSortField().isEmpty()) {
    			entidade.setSortField("nome");
			}
    		query += " ORDER BY i." + entidade.getSortField() + " " + (entidade.getSortOrder().equals(SortOrder.DESCENDING)?"DESC":"ASC");
		}
    	
		return executarQuery(query, parametros, first, pageSize);
    }	

    @Override
    public Long qtdPorVariedade(Variedade v, TipoIrrigante tipo, Area area, List<Distrito> distritos, Integer first, Integer pageSize) {
    	String jpql = "SELECT count(distinct i.id) FROM Irrigante i JOIN i.area a JOIN i.setores s JOIN s.plantio p JOIN p.cultivo c JOIN c.variedade v WHERE v.id = :pVariedade AND (i.tipoIrrigante = :pTipo OR :pTipo is null) AND (i.area = :pArea OR :pArea is null) AND a.distrito IN (:pDistrito)";
    	return this.entityManager.createQuery(jpql, Long.class)
			.setParameter("pVariedade", v.getId()).setParameter("pTipo", tipo).setParameter("pArea", area).setParameter("pDistrito", distritos)
			.getSingleResult();    
	}	

    @Override
    public Long qtdPorCultura(Cultura v, TipoIrrigante tipo, Area area, List<Distrito> distritos, Integer first, Integer pageSize) {
    	String jpql = "SELECT count(distinct i.id) FROM Irrigante i JOIN i.area a JOIN i.setores s JOIN i.setores s JOIN s.plantio p JOIN p.cultivo c JOIN c.cultura cult WHERE cult.id = :pCultura AND (i.tipoIrrigante = :pTipo OR :pTipo is null) AND (i.area = :pArea OR :pArea is null) AND a.distrito IN (:pDistrito)";
    	return this.entityManager.createQuery(jpql, Long.class)
			.setParameter("pCultura", v.getId()).setParameter("pTipo", tipo).setParameter("pArea", area).setParameter("pDistrito", distritos)
			.getSingleResult();    
	}
    
    @Override
    @SuppressWarnings("unchecked")
	public List<Irrigante> obterPorParametro(Irrigante entidade, Integer indiceInicio, Integer quantidade) {
    	MatchMode match = MatchMode.START;
    	
    	if(entidade.getNome() != null) {
    		match = MatchMode.ANYWHERE;
    	}
    	
		Criteria criterio = criarCriteriaExample(entidade, match);
		if(indiceInicio!=null){
			criterio.setFirstResult(indiceInicio);
			criterio.setMaxResults(quantidade);
		}
		return criterio.list();
	}
    
    @Override
    public List<Irrigante> pesquisarPorDistrito(List<Distrito> distritos) {
    	String jpql = "SELECT i FROM Irrigante i JOIN i.area a WHERE a.distrito IN (:pDistrito)";
    	return this.entityManager.createQuery(jpql, Irrigante.class)
			.setParameter("pDistrito", distritos)
			.getResultList();
    }
    
	@Override
	public List<Irrigante> pesquisarPorDistrito(List<Distrito> distritos, Integer indiceInicio, Integer quantidade) {
		String jpql = "SELECT i FROM Irrigante i JOIN i.area a WHERE a.distrito IN (:pDistrito) ORDER BY i.nome";
		return this.entityManager.createQuery(jpql, Irrigante.class)
				.setFirstResult(indiceInicio).setMaxResults(quantidade)
				.setParameter("pDistrito", distritos).getResultList();
	}
	
	@Override
	public Long totalIrrigantePorDistrito(List<Distrito> distritos) {
		String jpql = "SELECT count(*) as total FROM Irrigante i JOIN i.area a WHERE a.distrito IN (:pDistrito)";
		return this.entityManager.createQuery(jpql, Long.class)
				.setParameter("pDistrito", distritos).getSingleResult();
	}
	
	 @Override
    public List<Irrigante> pesquisarPorArea(Area area) {
    	String jpql = "SELECT i FROM Irrigante i WHERE i.area = (:pArea)";
    	return this.entityManager.createQuery(jpql, Irrigante.class)
			.setParameter("pArea", area)
			.getResultList();
    }

}
