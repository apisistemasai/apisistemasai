package com.agrosolutions.sai.dao.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.collections.map.HashedMap;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.primefaces.model.SortOrder;
import org.springframework.stereotype.Repository;

import com.agrosolutions.sai.dao.FertirrigacaoAplicadaDAO;
import com.agrosolutions.sai.model.FertirrigacaoAplicada;

@Repository
public class FertirrigacaoAplicadaDAOImpl extends DAOImpl<FertirrigacaoAplicada> implements FertirrigacaoAplicadaDAO {

	private static final long serialVersionUID = -7142226634382723762L;
	static Logger logger = Logger.getLogger(FertirrigacaoAplicadaDAOImpl.class);

    @Override
    public void excluir(FertirrigacaoAplicada elemento) {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
    	parametros.put("pId",elemento.getId());
        executarComando("FertirrigacaoAplicada.excluir", parametros);
    }	

    @Override
    public List<FertirrigacaoAplicada> obterTodos() {
    	return executarQuery("FertirrigacaoAplicada.todos", null, null, null);
    }
    
    @Override
	public Criteria criarCriteriaExample(FertirrigacaoAplicada entidade, MatchMode match) {

		Criteria criteria = super.criarCriteriaExample(entidade, match);
				
		criteria = prepararCriteria(entidade, criteria);		
		return criteria;
	}

	private Criteria prepararCriteria(FertirrigacaoAplicada entidadeLocal, Criteria criteria) {
		if (entidadeLocal.getSortField().equals("nome")) {
			if (entidadeLocal.getSortOrder().equals(SortOrder.DESCENDING)) {
				criteria.addOrder(Order.desc("nome"));
			}else{
				criteria.addOrder(Order.asc("nome"));
			}
		}
		if (entidadeLocal.getDataIni()!=null && entidadeLocal.getDataFim()!=null){
			criteria.add(Restrictions.between("data",entidadeLocal.getDataIni(), entidadeLocal.getDataFim()));	
		}
		return criteria;
		
	}
}
