package com.agrosolutions.sai.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.map.HashedMap;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.primefaces.model.SortOrder;
import org.springframework.stereotype.Repository;

import com.agrosolutions.sai.dao.BaciaHidrograficaDAO;
import com.agrosolutions.sai.model.BaciaHidrografica;

@Repository
public class BaciaHidrograficaDAOImpl extends DAOImpl<BaciaHidrografica> implements BaciaHidrograficaDAO {
	
	private static final long serialVersionUID = 6892835107747260165L;

	static Logger logger = Logger.getLogger(BaciaHidrograficaDAOImpl.class);

    @Override
    public void excluir(BaciaHidrografica baciaHidrografica) {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
    	parametros.put("pId",baciaHidrografica.getId());
        executarComando("BaciaHidrografica.excluir", parametros);
    }	

    @Override
    public List<BaciaHidrografica> obterTodos(List<BaciaHidrografica> baciaHidrograficas) {
		@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
		parametros.put("pBaciaHidrografica", prepararParametros(baciaHidrograficas));
    	
    	return executarQuery("BaciaHidrografica.todos", parametros, null, null);
    }	
    @Override
	public Criteria criarCriteriaExample(BaciaHidrografica entidade, MatchMode match) {

		Criteria criteria = super.criarCriteriaExample(entidade, match);
				
		criteria = prepararCriteria(entidade, criteria);		
		return criteria;
	}

	private List<Long> prepararParametros(List<BaciaHidrografica> entidades) {
		List<Long> codigos = new ArrayList<Long>();
		for (BaciaHidrografica d : entidades) {
			try {
				codigos.add(d.getId());
			} catch (NumberFormatException e) {
			}
		}
		return codigos;
	}	
	
    private Criteria prepararCriteria(BaciaHidrografica entidadeLocal, Criteria criteria) {

		if(entidadeLocal.getBacias() != null && !entidadeLocal.getBacias().isEmpty()){
	    	List<Long> parametros = prepararParametros(entidadeLocal.getBacias());
			criteria.add(Restrictions.in("id", parametros));
		}
		
		if (entidadeLocal.getSortField().equals("nome")) {
			if (entidadeLocal.getSortOrder().equals(SortOrder.DESCENDING)) {
				criteria.addOrder(Order.desc("nome"));
			}else{
				criteria.addOrder(Order.asc("nome"));
			}
		}
		return criteria;	
	}
    
    @Override
    public List<BaciaHidrografica> pesquisarTodos() {
    	return executarQuery("BaciaHidrografica.pesquisarTodos", null, null, null);
    }
    
}
