package com.agrosolutions.sai.dao.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.collections.map.HashedMap;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.agrosolutions.sai.dao.TuboDAO;
import com.agrosolutions.sai.model.Fabricante;
import com.agrosolutions.sai.model.Irrigante;
import com.agrosolutions.sai.model.Tubo;

@Repository
public class TuboDAOImpl extends DAOImpl<Tubo> implements TuboDAO {

	private static final long serialVersionUID = 8038910697846288531L;
	static Logger logger = Logger.getLogger(TuboDAOImpl.class);

	@Override
	public List<Tubo> pesquisar(Fabricante fabricante) {
		@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
		parametros.put("pFabricante", fabricante);

		return executarQuery("Tubo.pesquisar", parametros, null, null);
	}

	@Override
	public List<Tubo> pesquisarPorIrrigante(Irrigante irrigante) {
		@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
		parametros.put("pIrrigante", irrigante);

		return executarQuery("Tubo.pesquisarPorIrrigante", parametros, null, null);
	}

	@Override
    public void excluir(Tubo tubo) {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
    	parametros.put("pId",tubo.getId());
        executarComando("Tubo.excluir", parametros);
    }	

    @Override
    public List<Tubo> obterTodos() {
    	return executarQuery("Tubo.todos", null, null, null);
    }	

	@Override
	public Criteria criarCriteriaExample(Tubo entidade, MatchMode match) {

		Criteria criteria = super.criarCriteriaExample(entidade, match);
				
		criteria = prepararCriteria(entidade, criteria);		
		return criteria;
	}

	private Criteria prepararCriteria(Tubo entidadeLocal, Criteria criteria) {
		Criteria a = null;
		if (entidadeLocal.getFabricante() != null) {
			a = criteria.createCriteria("fabricante");
			a.add(Restrictions.eq("id", entidadeLocal.getFabricante().getId()));
		}
		return criteria;
	}
    
}
