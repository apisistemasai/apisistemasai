package com.agrosolutions.sai.dao.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.collections.map.HashedMap;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.primefaces.model.SortOrder;
import org.springframework.stereotype.Repository;

import com.agrosolutions.sai.dao.ElementoDAO;
import com.agrosolutions.sai.model.Elemento;

@Repository
public class ElementoDAOImpl extends DAOImpl<Elemento> implements ElementoDAO {

	private static final long serialVersionUID = -4667305615519481024L;
	static Logger logger = Logger.getLogger(ElementoDAOImpl.class);

    @Override
    public void excluir(Elemento elemento) {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
    	parametros.put("pId",elemento.getId());
        executarComando("Elemento.excluir", parametros);
    }	

    @Override
    public List<Elemento> obterTodos() {
    	return executarQuery("Elemento.todos", null, null, null);
    }
    
    @Override
	public Criteria criarCriteriaExample(Elemento entidade, MatchMode match) {

		Criteria criteria = super.criarCriteriaExample(entidade, match);
				
		criteria = prepararCriteria(entidade, criteria);		
		return criteria;
	}

	private Criteria prepararCriteria(Elemento entidadeLocal, Criteria criteria) {
		if (entidadeLocal.getSortField().equals("nome")) {
			if (entidadeLocal.getSortOrder().equals(SortOrder.DESCENDING)) {
				criteria.addOrder(Order.desc("nome"));
			}else{
				criteria.addOrder(Order.asc("nome"));
			}
		}
		return criteria;
		
	}
}
