package com.agrosolutions.sai.dao.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.collections.map.HashedMap;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.primefaces.model.SortOrder;
import org.springframework.stereotype.Repository;

import com.agrosolutions.sai.dao.NutrienteDAO;
import com.agrosolutions.sai.model.Nutriente;

@Repository
public class NutrienteDAOImpl extends DAOImpl<Nutriente> implements NutrienteDAO {

	private static final long serialVersionUID = -8637419797382639936L;
	static Logger logger = Logger.getLogger(NutrienteDAOImpl.class);

    @Override
    public void excluir(Nutriente Nutriente) {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
    	parametros.put("pId",Nutriente.getId());
        executarComando("Nutriente.excluir", parametros);
    }	

    @Override
    public List<Nutriente> obterTodos() {
    	return executarQuery("Nutriente.todos", null, null, null);
    }
    
    @Override
	public Criteria criarCriteriaExample(Nutriente entidade, MatchMode match) {

		Criteria criteria = super.criarCriteriaExample(entidade, match);
				
		criteria = prepararCriteria(entidade, criteria);		
		return criteria;
	}

	private Criteria prepararCriteria(Nutriente entidadeLocal, Criteria criteria) {
		if (entidadeLocal.getSortField().equals("nome")) {
			if (entidadeLocal.getSortOrder().equals(SortOrder.DESCENDING)) {
				criteria.addOrder(Order.desc("nome"));
			}else{
				criteria.addOrder(Order.asc("nome"));
			}
		}
		return criteria;
		
	}
}
