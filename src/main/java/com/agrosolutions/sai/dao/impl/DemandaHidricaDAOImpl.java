package com.agrosolutions.sai.dao.impl;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.agrosolutions.sai.dao.DemandaHidricaDAO;
import com.agrosolutions.sai.model.DemandaHidrica;

@Repository
public class DemandaHidricaDAOImpl extends DAOImpl<DemandaHidrica> implements DemandaHidricaDAO {
	
	private static final long serialVersionUID = 5257347787855393738L;
	static Logger logger = Logger.getLogger(DemandaHidricaDAOImpl.class);

}