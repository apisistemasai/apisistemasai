package com.agrosolutions.sai.dao.impl;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.primefaces.model.SortOrder;
import org.springframework.stereotype.Repository;

import com.agrosolutions.sai.dao.MensagemDAO;
import com.agrosolutions.sai.model.Mensagem;

@Repository
public class MensagemDAOImpl extends DAOImpl<Mensagem> implements MensagemDAO {
	
	private static final long serialVersionUID = 6759853082184541213L;
	static Logger logger = Logger.getLogger(MensagemDAOImpl.class);

	@Override
	public Criteria criarCriteriaExample(Mensagem entidade, MatchMode match) {

		Criteria criteria = super.criarCriteriaExample(entidade, match);
				
		criteria = prepararCriteria(entidade, criteria);		
		return criteria;
	}

	private Criteria prepararCriteria(Mensagem entidadeLocal, Criteria criteria) {
		criteria = criteria.add(Restrictions.in("distrito", entidadeLocal.getDistritos()));

		if (entidadeLocal.getSortField().equals("dataEnvio")) {
			if (entidadeLocal.getSortOrder().equals(SortOrder.DESCENDING)) {
				criteria.addOrder(Order.desc("dataEnvio"));
			}else{
				criteria.addOrder(Order.asc("dataEnvio"));
			}
		}

		if (entidadeLocal.getDistrito() != null) {
			Criteria d = criteria.createCriteria("distrito");
			d.add(Restrictions.eq("id", entidadeLocal.getDistrito().getId()));
		}

		if (entidadeLocal.getIrrigante() != null) {
			Criteria i = criteria.createCriteria("destinatarios");
			i.add(Restrictions.eq("id", entidadeLocal.getIrrigante().getId()));
		}

		return criteria;
	}
	
}
