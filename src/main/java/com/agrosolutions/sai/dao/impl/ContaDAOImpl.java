package com.agrosolutions.sai.dao.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.collections.map.HashedMap;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.agrosolutions.sai.dao.ContaDAO;
import com.agrosolutions.sai.model.Conta;

@Repository
public class ContaDAOImpl extends DAOImpl<Conta> implements ContaDAO {

	private static final long serialVersionUID = 237678269836784064L;
	static Logger logger = Logger.getLogger(ContaDAOImpl.class);

	@Override
	public void excluir(Conta c) {
		@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
		parametros.put("pId", c.getId());
		executarComando("Conta.excluir", parametros);
	}

	@Override
	public List<Conta> obterTodos() {
		return executarQuery("Conta.todos", null, null, null);
	}
}
