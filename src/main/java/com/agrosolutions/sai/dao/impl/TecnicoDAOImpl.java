package com.agrosolutions.sai.dao.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.collections.map.HashedMap;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.primefaces.model.SortOrder;
import org.springframework.stereotype.Repository;

import com.agrosolutions.sai.dao.TecnicoDAO;
import com.agrosolutions.sai.model.Distrito;
import com.agrosolutions.sai.model.Tecnico;

@Repository
public class TecnicoDAOImpl extends DAOImpl<Tecnico> implements TecnicoDAO {
	
	private static final long serialVersionUID = -3927897479888342626L;
	static Logger logger = Logger.getLogger(TecnicoDAOImpl.class);

	@Override
	public List<Tecnico> pesquisar(List<Distrito> distrito) {
		@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
		parametros.put("pDistrito", distrito);

		return executarQuery("Tecnico.pesquisar", parametros, null, null);
	}
	
    @Override
    public void excluir(Tecnico tecnico) {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
    	parametros.put("pId",tecnico.getId());
        executarComando("Tecnico.excluir", parametros);
    }	
    
    @Override
	public Criteria criarCriteriaExample(Tecnico entidade, MatchMode match) {

		Criteria criteria = super.criarCriteriaExample(entidade, match);
				
		criteria = prepararCriteria(entidade, criteria);		
		return criteria;
	}

	private Criteria prepararCriteria(Tecnico entidadeLocal, Criteria criteria) {
		if (entidadeLocal.getSortField().equals("nome")) {
			if (entidadeLocal.getSortOrder().equals(SortOrder.DESCENDING)) {
				criteria.addOrder(Order.desc("nome"));
			}else{
				criteria.addOrder(Order.asc("nome"));
			}
		}
		return criteria;
	}
}
