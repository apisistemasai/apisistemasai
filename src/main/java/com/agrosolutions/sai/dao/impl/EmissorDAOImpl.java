package com.agrosolutions.sai.dao.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.collections.map.HashedMap;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.agrosolutions.sai.dao.EmissorDAO;
import com.agrosolutions.sai.model.Emissor;
import com.agrosolutions.sai.model.Fabricante;
import com.agrosolutions.sai.model.Plantio;
import com.agrosolutions.sai.model.Setor;

@Repository
public class EmissorDAOImpl extends DAOImpl<Emissor> implements EmissorDAO {

	private static final long serialVersionUID = -6398527580312352945L;
	static Logger logger = Logger.getLogger(EmissorDAOImpl.class);

	@Override
	public List<Emissor> pesquisar(Fabricante fabricante) {
		@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
		parametros.put("pSetor", fabricante);

		return executarQuery("Emissor.pesquisar", parametros, null, null);
	}

	@Override
	public List<Emissor> pesquisarPorPlantio(Plantio plantio) {
		@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
		parametros.put("pPlantio", plantio);

		return executarQuery("Emissor.pesquisarPorPlantio", parametros, null, null);
	}

	@Override
    public void excluir(Emissor emissor) {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
    	parametros.put("pId",emissor.getId());
        executarComando("Emissor.excluir", parametros);
    }	

	@Override
	public List<Emissor> pesquisarPorSetor(Setor setor) {
		@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
		parametros.put("pSetor", setor);

		return executarQuery("Emissor.pesquisarPorSetor", parametros, null, null);
	}

    @Override
    public List<Emissor> obterTodos() {
    	return executarQuery("Emissor.todos", null, null, null);
    }

	@Override
	public Criteria criarCriteriaExample(Emissor entidade, MatchMode match) {

		Criteria criteria = super.criarCriteriaExample(entidade, match);
				
		criteria = prepararCriteria(entidade, criteria);		
		return criteria;
	}

	private Criteria prepararCriteria(Emissor entidadeLocal, Criteria criteria) {
		Criteria a = null;
		if (entidadeLocal.getFabricante() != null) {
			a = criteria.createCriteria("fabricante");
			a.add(Restrictions.eq("id", entidadeLocal.getFabricante().getId()));
		}
		return criteria;
	}	
}
