package com.agrosolutions.sai.dao.impl;

import java.util.List;
import java.util.Map;

import javax.persistence.Query;

import org.apache.commons.collections.map.HashedMap;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.agrosolutions.sai.dao.AtividadeEconomicaDAO;
import com.agrosolutions.sai.model.AtividadeEconomica;
import com.agrosolutions.sai.model.BaciaHidrografica;
import com.agrosolutions.sai.model.Cultura;
import com.agrosolutions.sai.model.Municipio;
import com.agrosolutions.sai.model.Variedade;

@Repository
public class AtividadeEconomicaDAOImpl extends DAOImpl<AtividadeEconomica> implements AtividadeEconomicaDAO {

	private static final long serialVersionUID = -7540552850654142893L;

	static Logger logger = Logger.getLogger(AtividadeEconomicaDAOImpl.class);
	
    @Override
    public void excluir(AtividadeEconomica atividadeEconomica) {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
    	parametros.put("pId",atividadeEconomica.getId());
        executarComando("AtividadeEconomica.excluir", parametros);
    }	

	@Override
	public List<AtividadeEconomica> pesquisarPorMunicipio(Municipio municipio) {
		@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
		parametros.put("pMunicipio", municipio);

		return executarQuery("AtividadeEconomica.pesquisarPorMunicipio", parametros, null, null);
	}

	@Override
	public List<AtividadeEconomica> pesquisarPorBacia(BaciaHidrografica bacia) {
		@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
		parametros.put("pBacia", bacia);

		return executarQuery("AtividadeEconomica.pesquisarPorBacia", parametros, null, null);
	}

	@Override
	public List<AtividadeEconomica> pesquisarTodosDeMunicipio() {

		return executarQuery("AtividadeEconomica.pesquisarTodosDeMunicipio", null, null, null);
	}

	@Override
	public List<AtividadeEconomica> pesquisarTodosDaBacia() {

		return executarQuery("AtividadeEconomica.pesquisarTodosDaBacia", null, null, null);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Cultura> pesquisarCulturas(Municipio municipio) {
		Query queryNamed = entityManager.createNamedQuery("AtividadeEconomica.pesquisarCulturas");

		queryNamed.setParameter("pMunicipio", municipio);
		return queryNamed.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AtividadeEconomica> pesquisarPorVariedades(Variedade variedade) {
		Query queryNamed = entityManager.createNamedQuery("AtividadeEconomica.pesquisarPorVariedades");

		queryNamed.setParameter("pVariedade", variedade);
		return queryNamed.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Variedade> pesquisarVariedades(Municipio municipio) {
		Query queryNamed = entityManager.createNamedQuery("AtividadeEconomica.pesquisarVariedades");

		queryNamed.setParameter("pMunicipio", municipio);
		return queryNamed.getResultList();
	}

}
