package com.agrosolutions.sai.dao.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.Query;

import org.apache.commons.collections.map.HashedMap;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Repository;

import com.agrosolutions.sai.dao.IndicadoresDAO;
import com.agrosolutions.sai.model.AtividadeEconomica;
import com.agrosolutions.sai.model.BaciaHidrografica;
import com.agrosolutions.sai.model.GraficoBarra;
import com.agrosolutions.sai.model.GraficoPizzaIndicadores;
import com.agrosolutions.sai.model.Indicadores;
import com.agrosolutions.sai.model.Municipio;
import com.agrosolutions.sai.util.BigDecimalUtil;
import com.agrosolutions.sai.util.ResourceBundle;

@Repository
public class IndicadoresDAOImpl extends DAOImpl<Indicadores> implements
		IndicadoresDAO {

	private static final long serialVersionUID = -4746635112746046225L;
	
	
	@Override
	public Indicadores pesquisarPorAtividadeEconomica(AtividadeEconomica atividadeEconomica, Date data) {
		try {
			String jpql = "SELECT i FROM Indicadores i WHERE i.atividadeEconomica = :pAtividadeEconomica AND i.data = :pData";
			
			return this.entityManager.createQuery(jpql, Indicadores.class).setParameter("pAtividadeEconomica", atividadeEconomica).setParameter("pData", data).getSingleResult();			
		} catch (Exception e) {
			return null;
		}
	}
	
	@Override
	public List<Indicadores> pesquisarPorData(Date data) {
		@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
		parametros.put("pData", data);

		return executarQuery("Indicadores.pesquisarPorData", parametros, null, null);
	}

	@Override
	public List<Indicadores> pesquisarPorMunicipio(Municipio municipio, Date data) {
		@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
		parametros.put("pMunicipio", municipio);
		parametros.put("pData", data);

		return executarQuery("Indicadores.pesquisarPorMunicipio", parametros, null, null);
	}

	@Override
	public List<Indicadores> pesquisarPorBacia(BaciaHidrografica bacia, Date data) {
		@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
		parametros.put("pBacia", bacia);
		parametros.put("pData", data);

		return executarQuery("Indicadores.pesquisarPorBacia", parametros, null, null);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Date> pesquisarDatasPorAtividade(AtividadeEconomica atividade) {
		Map<String, Object> parametros = new HashedMap();
		parametros.put("pAtividade", atividade);
		Query queryNamed = entityManager.createNamedQuery("Indicadores.pesquisarDatasPorAtividade");
		
		if(parametros!=null){
			Set<String> keyParametros = parametros.keySet();
			for (String nomeParamentro : keyParametros) {
				queryNamed.setParameter(nomeParamentro, parametros.get(nomeParamentro));
			}
		}
		return queryNamed.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Date> pesquisarDatasPorMunicipio(Municipio municipio) {
		Map<String, Object> parametros = new HashedMap();
		parametros.put("pMunicipio", municipio);
		Query queryNamed = entityManager.createNamedQuery("Indicadores.pesquisarDatasPorMunicipio");
		
		if(parametros!=null){
			Set<String> keyParametros = parametros.keySet();
			for (String nomeParamentro : keyParametros) {
				queryNamed.setParameter(nomeParamentro, parametros.get(nomeParamentro));
			}
		}
		return queryNamed.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Date> pesquisarDatasPorBacia(BaciaHidrografica bacia) {
		Map<String, Object> parametros = new HashedMap();
		parametros.put("pBacia", bacia);
		Query queryNamed = entityManager.createNamedQuery("Indicadores.pesquisarDatasPorBacia");
		
		if(parametros!=null){
			Set<String> keyParametros = parametros.keySet();
			for (String nomeParamentro : keyParametros) {
				queryNamed.setParameter(nomeParamentro, parametros.get(nomeParamentro));
			}
		}
		return queryNamed.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Date> pesquisarDatas() {
		Query queryNamed = entityManager.createNamedQuery("Indicadores.pesquisarDatas");
		
		return queryNamed.getResultList();
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<GraficoBarra> culturasHaPorMunicipio(Municipio m) {
		List<GraficoBarra> retorno = new ArrayList<GraficoBarra>();
		String jpql 	 = "SELECT distinct(cult.nome) as nome, ind.kgHa from Indicadores ind JOIN ind.atividadeEconomica a JOIN a.cultura cult WHERE a.municipio = :pMunicipio AND a.dataFim IS NULL GROUP BY cult.nome, ind.kgHa ORDER BY ind.kgHa desc";
		
		try {
			List<Object[]> objs = (List<Object[]>) this.entityManager.createQuery(jpql).setParameter("pMunicipio", m).getResultList();
			for (Object[] p : objs) {
				
				if (p[0] == null || p[1] == null) return null;
				
				String nome = (String) p[0];
				double qtd = new BigDecimal(p[1].toString(), BigDecimalUtil.MC).setScale(2, RoundingMode.FLOOR).doubleValue();
				
				GraficoBarra g = new GraficoBarra( nome, qtd, ResourceBundle.getMessage("ha") );
				retorno.add(g);
			}
		} catch (EmptyResultDataAccessException e) {
			e.getMessage();
			e.getStackTrace();
		}
		
		return retorno;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<GraficoBarra> graficoIndicadoresSegurancaKgM3(Municipio m) {
		List<GraficoBarra> retorno = new ArrayList<GraficoBarra>();
		String jpql 	 = "SELECT distinct(cult.nome) as nome, ind.kgM3 from Indicadores ind JOIN ind.atividadeEconomica a JOIN a.cultura cult WHERE a.municipio = :pMunicipio AND a.dataFim IS NULL GROUP BY cult.nome, ind.kgM3 ORDER BY ind.kgM3 desc";
		
		List<Object[]> objs = (List<Object[]>) this.entityManager.createQuery(jpql).setParameter("pMunicipio", m).getResultList();
		for (Object[] p : objs) {

			if (p[0] == null || p[1] == null) return null;
			
			String nome = (String) p[0];
			double qtd = new BigDecimal(p[1].toString(), BigDecimalUtil.MC).setScale(2, RoundingMode.FLOOR).doubleValue();
			
			GraficoBarra g = new GraficoBarra( nome, qtd, ResourceBundle.getMessage("kgM3") );
			retorno.add(g);
		}
		
		return retorno;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<GraficoBarra> graficoIndicadoresEconomicoReceitaLiquidaHa(Municipio m) {
		List<GraficoBarra> retorno = new ArrayList<GraficoBarra>();
		String jpql 	 = "SELECT distinct(cult.nome) as nome, ind.receitaLiquidaHa from Indicadores ind JOIN ind.atividadeEconomica a JOIN a.cultura cult WHERE a.municipio = :pMunicipio AND a.dataFim IS NULL GROUP BY cult.nome, ind.receitaLiquidaHa ORDER BY ind.receitaLiquidaHa desc";
		
		List<Object[]> objs = (List<Object[]>) this.entityManager.createQuery(jpql).setParameter("pMunicipio", m).getResultList();
		for (Object[] p : objs) {

			if (p[0] == null || p[1] == null) return null;
			
			String nome = (String) p[0];
			double qtd = new BigDecimal(p[1].toString(), BigDecimalUtil.MC).setScale(2, RoundingMode.FLOOR).doubleValue();
			
			GraficoBarra g = new GraficoBarra( nome, qtd, ResourceBundle.getMessage("receitaLiquidaHa") );
			retorno.add(g);
		}
		
		return retorno;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<GraficoBarra> graficoIndicadoresEconomicoReceitaLiquida(Municipio m) {
		List<GraficoBarra> retorno = new ArrayList<GraficoBarra>();
		String jpql 	 = "SELECT distinct(cult.nome) as nome, ind.receitaLiquida from Indicadores ind JOIN ind.atividadeEconomica a JOIN a.cultura cult WHERE a.municipio = :pMunicipio AND a.dataFim IS NULL GROUP BY cult.nome, ind.receitaLiquida ORDER BY ind.receitaLiquida desc";
		
		List<Object[]> objs = (List<Object[]>) this.entityManager.createQuery(jpql).setParameter("pMunicipio", m).getResultList();
		for (Object[] p : objs) {

			if (p[0] == null || p[1] == null) return null;
			
			String nome = (String) p[0];
			double qtd = new BigDecimal(p[1].toString(), BigDecimalUtil.MC).setScale(2, RoundingMode.FLOOR).doubleValue();
			
			GraficoBarra g = new GraficoBarra( nome, qtd, ResourceBundle.getMessage("receitaUN") );
			retorno.add(g);
		}
		
		return retorno;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<GraficoBarra> graficoIndicadoresEconomicoReceitaLiquidaM3(Municipio m) {
		List<GraficoBarra> retorno = new ArrayList<GraficoBarra>();
		String jpql 	 = "SELECT distinct(cult.nome) as nome, ind.receitaLiquidaM3 from Indicadores ind JOIN ind.atividadeEconomica a JOIN a.cultura cult WHERE a.municipio = :pMunicipio AND a.dataFim IS NULL GROUP BY cult.nome, ind.receitaLiquidaM3 ORDER BY ind.receitaLiquidaM3 desc";
		
		List<Object[]> objs = (List<Object[]>) this.entityManager.createQuery(jpql).setParameter("pMunicipio", m).getResultList();
		for (Object[] p : objs) {

			if (p[0] == null || p[1] == null) return null;
			
			String nome = (String) p[0];
			double qtd = new BigDecimal(p[1].toString(), BigDecimalUtil.MC).setScale(2, RoundingMode.FLOOR).doubleValue();
			
			GraficoBarra g = new GraficoBarra( nome, qtd, ResourceBundle.getMessage("receitaLiquidaM3") );
			retorno.add(g);
		}
		
		return retorno;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<GraficoBarra> graficoIndicadoresEconomicoVBP(Municipio m) {
		List<GraficoBarra> retorno = new ArrayList<GraficoBarra>();
		String jpql 	 = "SELECT distinct(cult.nome) as nome, ind.vbp from Indicadores ind JOIN ind.atividadeEconomica a JOIN a.cultura cult WHERE a.municipio = :pMunicipio AND a.dataFim IS NULL GROUP BY cult.nome, ind.vbp ORDER BY ind.vbp desc";
		
		List<Object[]> objs = (List<Object[]>) this.entityManager.createQuery(jpql).setParameter("pMunicipio", m).getResultList();
		for (Object[] p : objs) {

			if (p[0] == null || p[1] == null) return null;
			
			String nome = (String) p[0];
			double qtd = new BigDecimal(p[1].toString(), BigDecimalUtil.MC).setScale(2, RoundingMode.FLOOR).doubleValue();
			
			GraficoBarra g = new GraficoBarra( nome, qtd, ResourceBundle.getMessage("vbpUN") );
			retorno.add(g);
		}
		
		return retorno;
	}
	@Override
	@SuppressWarnings("unchecked")
	public List<GraficoBarra> graficoIndicadoresSegSocialEmpregosHa(Municipio m) {
		List<GraficoBarra> retorno = new ArrayList<GraficoBarra>();
		String jpql 	 = "SELECT distinct(cult.nome) as nome, ind.empregosHa from Indicadores ind JOIN ind.atividadeEconomica a JOIN a.cultura cult WHERE a.municipio = :pMunicipio AND a.dataFim IS NULL GROUP BY cult.nome, ind.empregosHa ORDER BY ind.empregosHa desc";
		
		List<Object[]> objs = (List<Object[]>) this.entityManager.createQuery(jpql).setParameter("pMunicipio", m).getResultList();
		for (Object[] p : objs) {

			if (p[0] == null || p[1] == null) return null;
			
			String nome = (String) p[0];
			double qtd = new BigDecimal(p[1].toString(), BigDecimalUtil.MC).setScale(2, RoundingMode.FLOOR).doubleValue();
			
			GraficoBarra g = new GraficoBarra( nome, qtd, ResourceBundle.getMessage("empregosHa") );
			retorno.add(g);
		}
		
		return retorno;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<GraficoBarra> graficoIndicadoresSegSocialEmpregosM3(Municipio m) {
		List<GraficoBarra> retorno = new ArrayList<GraficoBarra>();
		String jpql 	 = "SELECT distinct(cult.nome) as nome, ind.empregosM3 from Indicadores ind JOIN ind.atividadeEconomica a JOIN a.cultura cult WHERE a.municipio = :pMunicipio AND a.dataFim IS NULL GROUP BY cult.nome, ind.empregosM3 ORDER BY ind.empregosM3 desc";
		
		List<Object[]> objs = (List<Object[]>) this.entityManager.createQuery(jpql).setParameter("pMunicipio", m).getResultList();
		for (Object[] p : objs) {

			if (p[0] == null || p[1] == null) return null;
			
			String nome = (String) p[0];
			double qtd = new BigDecimal(p[1].toString(), BigDecimalUtil.MC).setScale(2, RoundingMode.FLOOR).doubleValue();
			
			GraficoBarra g = new GraficoBarra( nome, qtd, ResourceBundle.getMessage("empregosM3") );
			retorno.add(g);
		}
		
		return retorno;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<GraficoBarra> graficoIndicadoresSegHidricaM3Ha(Municipio m) {
		List<GraficoBarra> retorno = new ArrayList<GraficoBarra>();
		String jpql 	 = "SELECT distinct(cult.nome) as nome, ind.m3Ha from Indicadores ind JOIN ind.atividadeEconomica a JOIN a.cultura cult WHERE a.municipio = :pMunicipio AND a.dataFim IS NULL GROUP BY cult.nome, ind.m3Ha ORDER BY ind.m3Ha desc";
		
		List<Object[]> objs = (List<Object[]>) this.entityManager.createQuery(jpql).setParameter("pMunicipio", m).getResultList();
		for (Object[] p : objs) {

			if (p[0] == null || p[1] == null) return null;
			
			String nome = (String) p[0];
			double qtd = new BigDecimal(p[1].toString(), BigDecimalUtil.MC).setScale(2, RoundingMode.FLOOR).doubleValue();
			
			GraficoBarra g = new GraficoBarra( nome, qtd, ResourceBundle.getMessage("m3Ha") );
			retorno.add(g);
		}
		
		return retorno;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<GraficoBarra> graficoIndicadoresSegHidricaLitrosSegundoHa(Municipio m) {
		List<GraficoBarra> retorno = new ArrayList<GraficoBarra>();
		String jpql 	 = "SELECT distinct(cult.nome) as nome, ind.litrosSegundoHa from Indicadores ind JOIN ind.atividadeEconomica a JOIN a.cultura cult WHERE a.municipio = :pMunicipio AND a.dataFim IS NULL GROUP BY cult.nome, ind.litrosSegundoHa ORDER BY ind.litrosSegundoHa desc";
		
		List<Object[]> objs = (List<Object[]>) this.entityManager.createQuery(jpql).setParameter("pMunicipio", m).getResultList();
		for (Object[] p : objs) {

			if (p[0] == null || p[1] == null) return null;
			
			String nome = (String) p[0];
			double qtd = new BigDecimal(p[1].toString(), BigDecimalUtil.MC).setScale(2, RoundingMode.FLOOR).doubleValue();
			
			GraficoBarra g = new GraficoBarra( nome, qtd, ResourceBundle.getMessage("litrosSegundo") );
			retorno.add(g);
		}
		
		return retorno;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<GraficoPizzaIndicadores> graficoIndicadoresCicloCultura(Municipio m) {
		List<GraficoPizzaIndicadores> retorno = new ArrayList<GraficoPizzaIndicadores>();
		String jpql 	 = "SELECT ind.cicloCultura, count(*) as total from Indicadores ind JOIN ind.atividadeEconomica a JOIN a.cultura cult WHERE a.municipio = :pMunicipio AND a.dataFim IS NULL GROUP BY ind.cicloCultura ORDER BY total desc";
		String jpqlTotal = "SELECT count(*) as total from Indicadores ind JOIN ind.atividadeEconomica a JOIN a.cultura cult WHERE a.municipio = :pMunicipio AND a.dataFim IS NULL ";
		BigDecimal total = new BigDecimal((Long)this.entityManager.createQuery(jpqlTotal).setParameter("pMunicipio", m).getSingleResult());

		List<Object[]> objs = (List<Object[]>) this.entityManager.createQuery(jpql).setParameter("pMunicipio", m).getResultList();
		for (Object[] p : objs) {

			if (p[0] == null || p[1] == null) return null;
			
			String nome = (p[0].toString().equals("0.50") ? "Permanente" : (p[0].toString().equals("0.75") ? "Tempo Long" : "Tempo Curto"));
			double qtd = new BigDecimal(p[1].toString(), BigDecimalUtil.MC).setScale(2, RoundingMode.FLOOR).doubleValue();
			
			GraficoPizzaIndicadores g = new GraficoPizzaIndicadores( nome, qtd, total.doubleValue(), ResourceBundle.getMessage("un"));
			retorno.add(g);
		}
		
		return retorno;
	}

	@Override
	public List<GraficoBarra> graficoIndicadoresCorteHidrico(Municipio m) {
		List<GraficoBarra> retorno = new ArrayList<GraficoBarra>();
		String jpql 	 = ""
				+ "SELECT cultu.nome, ind.pesoMedio "
				+ "FROM Indicadores ind "
				+ "INNER JOIN ind.atividadeEconomica a "
				+ "INNER JOIN a.cultura cultu "
				+ "WHERE a.municipio = :pMunicipio "
				+ "AND ind.pesoMedio IS NOT NULL "
				+ "AND a.dataFim IS NULL "
				+ "ORDER BY ind.pesoMedio";
		
		@SuppressWarnings("unchecked")
		List<Object[]> objs = (List<Object[]>) this.entityManager.createQuery(jpql).setParameter("pMunicipio", m).getResultList();
		for (Object[] p : objs) {

			if (p[0] == null || p[1] == null) return null;
			
			String nome = (String) p[0];
			double qtd = new BigDecimal(p[1].toString(), BigDecimalUtil.MC).setScale(2, RoundingMode.FLOOR).doubleValue();
			
			GraficoBarra g = new GraficoBarra( nome, qtd, null );
			retorno.add(g);
		}
		
		return retorno;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<GraficoBarra> graficoIndicadoresSegurancaKgHaBacia(BaciaHidrografica bacia) {
		List<GraficoBarra> retorno = new ArrayList<GraficoBarra>();
		String jpql 	 = "SELECT cult.nome as cultura, ( SUM(ind.kgHa) / COUNT(a) ) as kgHa from Indicadores ind JOIN ind.atividadeEconomica a JOIN a.cultura cult WHERE ind.kgHa > 0 GROUP BY cult.nome ORDER BY kgHa DESC";
		List<Object[]> objs = (List<Object[]>) this.entityManager.createQuery(jpql).getResultList();
		
		for (Object[] p : objs) {

			if (p[0] == null || p[1] == null) return null;
			
			String cultura = (String) p[0];
			BigDecimal indicador = new BigDecimal(p[1].toString(), BigDecimalUtil.MC).setScale(2, RoundingMode.FLOOR);
			GraficoBarra g = new GraficoBarra( cultura, indicador.doubleValue(), ResourceBundle.getMessage("kgHa") );
			retorno.add(g);
		}
		
		return retorno;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<GraficoBarra> graficoIndicadoresSegurancaKgM3Bacia(BaciaHidrografica bacia) {
		List<GraficoBarra> retorno = new ArrayList<GraficoBarra>();
		String jpql 	 = "SELECT cult.nome as cultura, ( SUM(ind.kgM3) / COUNT(a) ) as kgM3 from Indicadores ind JOIN ind.atividadeEconomica a JOIN a.cultura cult WHERE ind.kgM3 > 0 GROUP BY cult.nome ORDER BY kgM3 DESC";
		List<Object[]> objs = (List<Object[]>) this.entityManager.createQuery(jpql).getResultList();
		
		for (Object[] p : objs) {

			if (p[0] == null || p[1] == null) return null;
			
			String cultura = (String) p[0];
			BigDecimal indicador = new BigDecimal(p[1].toString(), BigDecimalUtil.MC).setScale(2, RoundingMode.FLOOR);
			GraficoBarra g = new GraficoBarra( cultura, indicador.doubleValue(), ResourceBundle.getMessage("kgM3") );
			retorno.add(g);
		}
		
		return retorno;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<GraficoBarra> graficoIndicadoresSegurancaEconomicaReceitaHaBacia(BaciaHidrografica bacia) {
		List<GraficoBarra> retorno = new ArrayList<GraficoBarra>();
		String jpql 	 = "SELECT cult.nome as cultura, ( SUM(ind.receitaLiquidaHa) / COUNT(a) ) as receitaLiquidaHa from Indicadores ind JOIN ind.atividadeEconomica a JOIN a.cultura cult WHERE ind.receitaLiquidaHa > 0 GROUP BY cult.nome ORDER BY receitaLiquidaHa DESC";
		List<Object[]> objs = (List<Object[]>) this.entityManager.createQuery(jpql).getResultList();
		
		for (Object[] p : objs) {

			if (p[0] == null || p[1] == null) return null;
			
			String cultura = (String) p[0];
			BigDecimal indicador = new BigDecimal(p[1].toString(), BigDecimalUtil.MC).setScale(2, RoundingMode.FLOOR);
			GraficoBarra g = new GraficoBarra( cultura, indicador.doubleValue(), ResourceBundle.getMessage("receitaLiquidaHa") );
			retorno.add(g);
		}
		
		return retorno;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<GraficoBarra> graficoIndicadoresSegurancaEconomicaReceitaBacia(BaciaHidrografica bacia) {
		List<GraficoBarra> retorno = new ArrayList<GraficoBarra>();
		String jpql 	 = "SELECT cult.nome as cultura, ( SUM(ind.receitaLiquida) / COUNT(a) ) as receitaLiquida from Indicadores ind JOIN ind.atividadeEconomica a JOIN a.cultura cult WHERE ind.receitaLiquida > 0 GROUP BY cult.nome ORDER BY receitaLiquida DESC";
		List<Object[]> objs = (List<Object[]>) this.entityManager.createQuery(jpql).getResultList();
		
		for (Object[] p : objs) {

			if (p[0] == null || p[1] == null) return null;
			
			String cultura = (String) p[0];
			BigDecimal indicador = new BigDecimal(p[1].toString(), BigDecimalUtil.MC).setScale(2, RoundingMode.FLOOR);
			GraficoBarra g = new GraficoBarra( cultura, indicador.doubleValue(), ResourceBundle.getMessage("receitaUN") );
			retorno.add(g);
		}
		
		return retorno;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<GraficoBarra> graficoIndicadoresSegurancaEconomicaReceitaM3Bacia(BaciaHidrografica bacia) {
		List<GraficoBarra> retorno = new ArrayList<GraficoBarra>();
		String jpql 	 = "SELECT cult.nome as cultura, ( SUM(ind.receitaLiquidaM3) / COUNT(a) ) as receitaLiquidaM3 from Indicadores ind JOIN ind.atividadeEconomica a JOIN a.cultura cult WHERE ind.receitaLiquidaM3 > 0 GROUP BY cult.nome ORDER BY receitaLiquidaM3 DESC";
		List<Object[]> objs = (List<Object[]>) this.entityManager.createQuery(jpql).getResultList();
		
		for (Object[] p : objs) {

			if (p[0] == null || p[1] == null) return null;
			
			String cultura = (String) p[0];
			BigDecimal indicador = new BigDecimal(p[1].toString(), BigDecimalUtil.MC).setScale(2, RoundingMode.FLOOR);
			GraficoBarra g = new GraficoBarra( cultura, indicador.doubleValue(), ResourceBundle.getMessage("receitaLiquidaM3") );
			retorno.add(g);
		}
		
		return retorno;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<GraficoBarra> graficoIndicadoresSegurancaEconomicaVBPBacia(BaciaHidrografica bacia) {
		List<GraficoBarra> retorno = new ArrayList<GraficoBarra>();
		String jpql 	 = "SELECT cult.nome as cultura, ( SUM(ind.vbp) / COUNT(a) ) as vbp from Indicadores ind JOIN ind.atividadeEconomica a JOIN a.cultura cult WHERE ind.vbp > 0 GROUP BY cult.nome ORDER BY vbp DESC";
		List<Object[]> objs = (List<Object[]>) this.entityManager.createQuery(jpql).getResultList();
		
		for (Object[] p : objs) {

			if (p[0] == null || p[1] == null) return null;
			
			String cultura = (String) p[0];
			BigDecimal indicador = new BigDecimal(p[1].toString(), BigDecimalUtil.MC).setScale(2, RoundingMode.FLOOR);
			GraficoBarra g = new GraficoBarra( cultura, indicador.doubleValue(), ResourceBundle.getMessage("vbpUN") );
			retorno.add(g);
		}
		
		return retorno;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<GraficoBarra> graficoIndicadoresSegurancaSocialEmpregosHaBacia(BaciaHidrografica bacia) {
		List<GraficoBarra> retorno = new ArrayList<GraficoBarra>();
		String jpql 	 = "SELECT cult.nome as cultura, ( SUM(ind.empregosHa) / COUNT(a) ) as empregosHa from Indicadores ind JOIN ind.atividadeEconomica a JOIN a.cultura cult WHERE ind.empregosHa > 0 GROUP BY cult.nome ORDER BY empregosHa DESC";
		List<Object[]> objs = (List<Object[]>) this.entityManager.createQuery(jpql).getResultList();
		
		for (Object[] p : objs) {

			if (p[0] == null || p[1] == null) return null;
			
			String cultura = (String) p[0];
			BigDecimal indicador = new BigDecimal(p[1].toString(), BigDecimalUtil.MC).setScale(2, RoundingMode.FLOOR);
			GraficoBarra g = new GraficoBarra( cultura, indicador.doubleValue(), ResourceBundle.getMessage("empregosHa") );
			retorno.add(g);
		}
		
		return retorno;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<GraficoBarra> graficoIndicadoresSegurancaSocialEmpregosM3Bacia(BaciaHidrografica bacia) {
		List<GraficoBarra> retorno = new ArrayList<GraficoBarra>();
		String jpql 	 = "SELECT cult.nome as cultura, ( SUM(ind.empregosM3) / COUNT(a) ) as empregosM3 from Indicadores ind JOIN ind.atividadeEconomica a JOIN a.cultura cult WHERE ind.empregosM3 > 0 GROUP BY cult.nome ORDER BY empregosM3 DESC";
		List<Object[]> objs = (List<Object[]>) this.entityManager.createQuery(jpql).getResultList();
		
		for (Object[] p : objs) {

			if (p[0] == null || p[1] == null) return null;
			
			String cultura = (String) p[0];
			BigDecimal indicador = new BigDecimal(p[1].toString(), BigDecimalUtil.MC).setScale(2, RoundingMode.FLOOR);
			GraficoBarra g = new GraficoBarra( cultura, indicador.doubleValue(), ResourceBundle.getMessage("empregosM3") );
			retorno.add(g);
		}
		
		return retorno;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<GraficoBarra> graficoIndicadoresSegurancaHidricaM3HaBacia(BaciaHidrografica bacia) {
		List<GraficoBarra> retorno = new ArrayList<GraficoBarra>();
		String jpql 	 = "SELECT cult.nome as cultura, ( SUM(ind.m3Ha) / COUNT(a) ) as m3Ha from Indicadores ind JOIN ind.atividadeEconomica a JOIN a.cultura cult WHERE ind.m3Ha > 0 GROUP BY cult.nome ORDER BY m3Ha DESC";
		List<Object[]> objs = (List<Object[]>) this.entityManager.createQuery(jpql).getResultList();
		
		for (Object[] p : objs) {

			if (p[0] == null || p[1] == null) return null;
			
			String cultura = (String) p[0];
			BigDecimal indicador = new BigDecimal(p[1].toString(), BigDecimalUtil.MC).setScale(2, RoundingMode.FLOOR);
			GraficoBarra g = new GraficoBarra( cultura, indicador.doubleValue(), ResourceBundle.getMessage("m3Ha") );
			retorno.add(g);
		}
		
		return retorno;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<GraficoBarra> graficoIndicadoresSegurancaHidricaLitrosSegundoHaBacia(BaciaHidrografica bacia) {
		List<GraficoBarra> retorno = new ArrayList<GraficoBarra>();
		String jpql 	 = "SELECT cult.nome as cultura, ( SUM(ind.litrosSegundoHa) / COUNT(a) ) as litrosSegundoHa from Indicadores ind JOIN ind.atividadeEconomica a JOIN a.cultura cult WHERE ind.litrosSegundoHa > 0 GROUP BY cult.nome ORDER BY litrosSegundoHa DESC";
		List<Object[]> objs = (List<Object[]>) this.entityManager.createQuery(jpql).getResultList();
		
		for (Object[] p : objs) {

			if (p[0] == null || p[1] == null) return null;
			
			String cultura = (String) p[0];
			BigDecimal indicador = new BigDecimal(p[1].toString(), BigDecimalUtil.MC).setScale(2, RoundingMode.FLOOR);
			GraficoBarra g = new GraficoBarra( cultura, indicador.doubleValue(), ResourceBundle.getMessage("litrosSegundoHa") );
			retorno.add(g);
		}
		
		return retorno;
	}

	@Override
	public List<GraficoBarra> graficoCorteHidricoBacia(BaciaHidrografica bacia) {
		List<GraficoBarra> retorno = new ArrayList<GraficoBarra>();
		String jpql 	 = ""
				+ "SELECT distinct(cultu.nome), sum(ind.pesoMedio) / count(cultu.nome) as pesoMedio "
				+ "FROM Indicadores ind "
				+ "INNER JOIN ind.atividadeEconomica a "
				+ "INNER JOIN a.cultura cultu "
				+ "WHERE ind.pesoMedio IS NOT NULL "
				+ "GROUP BY cultu.nome "
				+ "ORDER BY pesoMedio DESC";
		
		@SuppressWarnings("unchecked")
		List<Object[]> objs = (List<Object[]>) this.entityManager.createQuery(jpql).getResultList();
		for (Object[] p : objs) {

			if (p[0] == null || p[1] == null) return null;
			
			String nome = (String) p[0];
			double qtd = new BigDecimal(p[1].toString(), BigDecimalUtil.MC).setScale(2, RoundingMode.FLOOR).doubleValue();
			
			GraficoBarra g = new GraficoBarra( nome, qtd, null );
			retorno.add(g);
		}
		
		return retorno;
	}

}