package com.agrosolutions.sai.dao.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.collections.map.HashedMap;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.agrosolutions.sai.dao.ValvulaDAO;
import com.agrosolutions.sai.model.Fabricante;
import com.agrosolutions.sai.model.Irrigante;
import com.agrosolutions.sai.model.Valvula;

@Repository
public class ValvulaDAOImpl extends DAOImpl<Valvula> implements ValvulaDAO {

	private static final long serialVersionUID = 840768536125749901L;
	static Logger logger = Logger.getLogger(ValvulaDAOImpl.class);

	@Override
	public List<Valvula> pesquisar(Fabricante fabricante) {
		@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
		parametros.put("pFabricante", fabricante);

		return executarQuery("Valvula.pesquisar", parametros, null, null);
	}

	@Override
	public List<Valvula> pesquisarPorIrrigante(Irrigante irrigante) {
		@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
		parametros.put("pIrrigante", irrigante);

		return executarQuery("Valvula.pesquisarPorIrrigante", parametros, null, null);
	}

	@Override
    public void excluir(Valvula valvula) {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
    	parametros.put("pId",valvula.getId());
        executarComando("Valvula.excluir", parametros);
    }	

    @Override
    public List<Valvula> obterTodos() {
    	return executarQuery("Valvula.todos", null, null, null);
    }	
    
    @Override
	public Criteria criarCriteriaExample(Valvula entidade, MatchMode match) {

		Criteria criteria = super.criarCriteriaExample(entidade, match);
				
		criteria = prepararCriteria(entidade, criteria);		
		return criteria;
	}

	private Criteria prepararCriteria(Valvula entidadeLocal, Criteria criteria) {
		Criteria c = null;
		if (entidadeLocal.getFabricante() != null) {
			c = criteria.createCriteria("fabricante");
			c.add(Restrictions.eq("id", entidadeLocal.getFabricante().getId()));
		}
		
		

		return criteria;
	}
}
