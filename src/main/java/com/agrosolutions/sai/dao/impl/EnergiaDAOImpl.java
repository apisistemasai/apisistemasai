package com.agrosolutions.sai.dao.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.collections.map.HashedMap;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.agrosolutions.sai.dao.EnergiaDAO;
import com.agrosolutions.sai.model.Energia;
import com.agrosolutions.sai.model.Irrigante;

@Repository
public class EnergiaDAOImpl extends DAOImpl<Energia> implements EnergiaDAO {

	private static final long serialVersionUID = 7461546661599370730L;
	static Logger logger = Logger.getLogger(EnergiaDAOImpl.class);

	@Override
	public List<Energia> pesquisar(Irrigante irrigante) {
		@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
		parametros.put("pIrrigante", irrigante);

		return executarQuery("Energia.pesquisar", parametros, null, null);
	}

	@Override
    public void excluir(Energia energia) {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
    	parametros.put("pId",energia.getId());
        executarComando("Energia.excluir", parametros);
    }	
	
}
