package com.agrosolutions.sai.dao.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.agrosolutions.sai.dao.ModuloDAO;
import com.agrosolutions.sai.model.Modulo;

@Repository
public class ModuloDAOImpl extends DAOImpl<Modulo> implements ModuloDAO {

	private static final long serialVersionUID = 7012826961484903767L;
	static Logger logger = Logger.getLogger(ModuloDAOImpl.class);

    @Override
    public List<Modulo> obterTodos() {
    	return executarQuery("Modulo.todos", null, null, null);
    }	
}
