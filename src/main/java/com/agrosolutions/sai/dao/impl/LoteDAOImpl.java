/*package com.agrosolutions.sai.dao.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.collections.map.HashedMap;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.agrosolutions.sai.dao.LoteDAO;
import com.agrosolutions.sai.model.Irrigante;
import com.agrosolutions.sai.model.Lote;

@Repository
public class LoteDAOImpl extends DAOImpl<Lote> implements LoteDAO {

	private static final long serialVersionUID = -5961772288158204148L;
	static Logger logger = Logger.getLogger(LoteDAOImpl.class);

	@Override
	public List<Lote> pesquisar(Irrigante irrigante) {
		@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
		parametros.put("pIrrigante", irrigante);

		return executarQuery("Lote.pesquisar", parametros, null, null);
	}

	@Override
    public void excluir(Lote lote) {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
    	parametros.put("pId",lote.getId());
        executarComando("Lote.excluir", parametros);
    }	
}
*/