package com.agrosolutions.sai.dao.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.map.HashedMap;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.primefaces.model.SortOrder;
import org.springframework.stereotype.Repository;

import com.agrosolutions.sai.dao.AguaDAO;
import com.agrosolutions.sai.model.Agua;
import com.agrosolutions.sai.model.Irrigante;

@Repository
public class AguaDAOImpl extends DAOImpl<Agua> implements AguaDAO {

	private static final long serialVersionUID = -5442323873812001664L;
	static Logger logger = Logger.getLogger(AguaDAOImpl.class);

	@Override
	public List<Agua> pesquisar(Irrigante irrigante) {
		@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
		parametros.put("pIrrigante", irrigante);

		return executarQuery("Agua.pesquisar", parametros, null, null);
	}

	@Override
	public Agua pesquisarAnterior(Irrigante irrigante, Date data) {
		@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
		parametros.put("pIrrigante", irrigante);
		parametros.put("pData", data);

		return executarQuery("Agua.pesquisarAnterior", parametros);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Agua> pesquisarPeriodo(Date data) {
		Calendar d = new GregorianCalendar();
		d.setTime(data);
		Map<String, Object> parametros = new HashedMap();
		parametros.put("pMes", d.get(Calendar.MONTH)+1);
		parametros.put("pAno", d.get(Calendar.YEAR));

		return executarQuery("Agua.pesquisarPeriodo", parametros, null, null);
	}

	@Override
    public void excluir(Agua agua) {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
    	parametros.put("pId",agua.getId());
        executarComando("Agua.excluir", parametros);
    }	
    @Override
	public Criteria criarCriteriaExample(Agua entidade, MatchMode match) {

		Criteria criteria = super.criarCriteriaExample(entidade, match);
				
		criteria = prepararCriteria(entidade, criteria);		
		return criteria;
	}

	private Criteria prepararCriteria(Agua entidadeLocal, Criteria criteria) {
		if (entidadeLocal.getSortField().equals("data")) {
			if (entidadeLocal.getSortOrder() !=null && entidadeLocal.getSortOrder().equals(SortOrder.DESCENDING)) {
				criteria.addOrder(Order.desc("data"));
			}else{
				criteria.addOrder(Order.asc("data"));
			}
		}

		Criteria c = criteria.createCriteria("irrigante").add( Example.create(entidadeLocal.getIrrigante()));
		if (entidadeLocal.getSortField().equals("irrigante.localizacao")) {
			if (entidadeLocal.getSortOrder().equals(SortOrder.DESCENDING)) {
				c.addOrder(Order.desc("localizacao"));
			}else{
				c.addOrder(Order.asc("localizacao"));
			}
		}

		if (entidadeLocal.getIrrigante().getLocalizacao() != null && !entidadeLocal.getIrrigante().getLocalizacao().isEmpty()) {
			c.add(Restrictions.eq("localizacao", entidadeLocal.getIrrigante().getLocalizacao()));
		}else{
			if (entidadeLocal.getIrrigante().getArea().getId() != null) {
				c = c.createCriteria("area").add( Example.create(entidadeLocal.getIrrigante().getArea()));;
			}else{
				if (entidadeLocal.getIrrigante().getArea().getDistrito() != null) {
					c = c.createCriteria("area").createCriteria("distrito");
					c.add(Restrictions.eq("id", entidadeLocal.getIrrigante().getArea().getDistrito().getId()));
				}
			}
		}

		return c;
	}
	
}
