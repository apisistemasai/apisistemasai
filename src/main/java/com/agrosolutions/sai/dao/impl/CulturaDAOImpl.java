package com.agrosolutions.sai.dao.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.map.HashedMap;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.primefaces.model.SortOrder;
import org.springframework.stereotype.Repository;

import com.agrosolutions.sai.dao.CulturaDAO;
import com.agrosolutions.sai.model.Area;
import com.agrosolutions.sai.model.BaciaHidrografica;
import com.agrosolutions.sai.model.Cultura;
import com.agrosolutions.sai.model.GraficoPizza;
import com.agrosolutions.sai.model.GraficoPizzaIndicadores;
import com.agrosolutions.sai.model.Municipio;
import com.agrosolutions.sai.util.BigDecimalUtil;

@Repository
public class CulturaDAOImpl extends DAOImpl<Cultura> implements CulturaDAO {

	private static final long serialVersionUID = -4919834906533535707L;
	static Logger logger = Logger.getLogger(CulturaDAOImpl.class);
	
    @Override
    public void excluir(Cultura cultura) {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
    	parametros.put("pId",cultura.getId());
        executarComando("Cultura.excluir", parametros);
    }	

    @Override
    public List<Cultura> obterTodos() {
    	return executarQuery("Cultura.todos", null, null, null);
    }
     
    @Override
	@SuppressWarnings("unchecked")
	public List<GraficoPizza> culturaAtivaPorArea(Area a, Date data) {
		String jpql = "SELECT new "
				+ GraficoPizza.class
				+ " (cul.nome, count(*)) from Plantio p JOIN p.cultivo c JOIN c.cultura cult JOIN p.setor s JOIN s.irrigante i WHERE i.area = :pArea AND c.dataInicio <= :pData AND (p.dataFim IS NULL OR p.dataFim >= :pData) GROUP BY cul.nome ";
		return (List<GraficoPizza>) this.entityManager.createQuery(jpql)
				.setParameter("pArea", a).setParameter("pData", data)
				.getSingleResult();
    }
    
	@Override
	public Criteria criarCriteriaExample(Cultura entidade, MatchMode match) {

		Criteria criteria = super.criarCriteriaExample(entidade, match);
				
		criteria = prepararCriteria(entidade, criteria);		
		return criteria;
	}

	private Criteria prepararCriteria(Cultura entidadeLocal, Criteria criteria) {
		Criteria a = null;
		if (entidadeLocal.getAtividade() != null) {
			a = criteria.createCriteria("atividade");
			a.add(Restrictions.eq("id", entidadeLocal.getAtividade().getId()));
		}
		if (entidadeLocal.getSortField().equals("nome")) {
			if (entidadeLocal.getSortOrder().equals(SortOrder.DESCENDING)) {
				criteria.addOrder(Order.desc("nome"));
			}else{
				criteria.addOrder(Order.asc("nome"));
			}
		}
		return criteria;
		
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<GraficoPizzaIndicadores> totalHaPorCulturaPorMunicipio(Municipio m) {
		List<GraficoPizzaIndicadores> retorno = new ArrayList<GraficoPizzaIndicadores>();
		String jpql 	 = "SELECT distinct(cult.nome) as nome, sum(a.areatotal) as total from AtividadeEconomica a JOIN a.cultura cult WHERE a.municipio = :pMunicipio and a.dataFim IS NULL GROUP BY cult.nome ORDER BY total desc";
		String jpqlTotal = "SELECT sum(a.areatotal) as total from AtividadeEconomica a JOIN a.cultura cult WHERE a.municipio = :pMunicipio and a.dataFim IS NULL";
		BigDecimal total = this.entityManager.createQuery(jpqlTotal, BigDecimal.class).setParameter("pMunicipio", m).getSingleResult();
		List<Object[]> objs = (List<Object[]>) this.entityManager.createQuery(jpql).setParameter("pMunicipio", m).getResultList();
		for (Object[] p : objs) {
			String nome = (String) p[0];
			double qtd = new BigDecimal(p[1].toString(), BigDecimalUtil.MC).doubleValue();
			
			GraficoPizzaIndicadores g = new GraficoPizzaIndicadores( nome, qtd, total.doubleValue(), "ha");
			retorno.add(g);
		}
		
		return retorno;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<GraficoPizzaIndicadores> totalHaPorCulturaPorBacia(BaciaHidrografica b) {
		List<GraficoPizzaIndicadores> retorno = new ArrayList<GraficoPizzaIndicadores>();
		String jpql 	 = "SELECT distinct(cult.nome) as nome, sum(a.areatotal) as total from AtividadeEconomica a JOIN a.cultura cult JOIN a.municipio m WHERE m.bacia = :pBacia GROUP BY cult.nome ORDER BY total DESC";
		String jpqlTotal = "SELECT sum(a.areatotal) as total from AtividadeEconomica a JOIN a.cultura cult JOIN a.municipio m WHERE m.bacia = :pBacia";
		BigDecimal total = this.entityManager.createQuery(jpqlTotal, BigDecimal.class).setParameter("pBacia", b).getSingleResult();
		List<Object[]> objs = (List<Object[]>) this.entityManager.createQuery(jpql).setParameter("pBacia", b).getResultList();
		for (Object[] p : objs) {
			String nome = (String) p[0];
			double qtd = new BigDecimal(p[1].toString(), BigDecimalUtil.MC).doubleValue();
			
			GraficoPizzaIndicadores g = new GraficoPizzaIndicadores( nome, qtd, total.doubleValue(), "ha");
			retorno.add(g);
		}
		
		return retorno;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<GraficoPizzaIndicadores> totalCicloCulturaPorBacia(BaciaHidrografica b) {
		List<GraficoPizzaIndicadores> retorno = new ArrayList<GraficoPizzaIndicadores>();
		String jpql 	 = "SELECT distinct(ind.cicloCultura) as nome, count(*) as total from Indicadores ind JOIN ind.atividadeEconomica a JOIN a.cultura cult JOIN a.municipio m WHERE m.bacia = :pBacia AND a.dataFim IS NULL GROUP BY ind.cicloCultura ORDER BY total DESC";
		String jpqlTotal = "SELECT count(*) as total from Indicadores ind JOIN ind.atividadeEconomica a JOIN a.cultura cult JOIN a.municipio m WHERE m.bacia = :pBacia AND a.dataFim IS NULL";
		BigDecimal total = new BigDecimal((Long)this.entityManager.createQuery(jpqlTotal).setParameter("pBacia", b).getSingleResult());
		List<Object[]> objs = (List<Object[]>) this.entityManager.createQuery(jpql).setParameter("pBacia", b).getResultList();
		for (Object[] p : objs) {
			String nome = (p[0].toString().equals("0.50") ? "Permanente" : (p[0].toString().equals("0.75") ? "Tempo Long" : "Tempo Curto"));
			double qtd = new BigDecimal(p[1].toString(), BigDecimalUtil.MC).doubleValue();
			
			GraficoPizzaIndicadores g = new GraficoPizzaIndicadores( nome, qtd, total.doubleValue(), "un");
			retorno.add(g);
		}
		
		return retorno;
	}

}
