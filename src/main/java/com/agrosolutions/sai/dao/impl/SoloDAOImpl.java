package com.agrosolutions.sai.dao.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.collections.map.HashedMap;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.primefaces.model.SortOrder;
import org.springframework.stereotype.Repository;

import com.agrosolutions.sai.dao.SoloDAO;
import com.agrosolutions.sai.model.Solo;

@Repository
public class SoloDAOImpl extends DAOImpl<Solo> implements SoloDAO {

	private static final long serialVersionUID = 1959227279899335921L;
	static Logger logger = Logger.getLogger(SoloDAOImpl.class);

	@Override
	public List<Solo> pesquisar(String nome) {
		@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
		parametros.put("pNome", nome);

		return executarQuery("Solo.pesquisar", parametros, null, null);
	}

	@Override
    public void excluir(Solo solo) {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
    	parametros.put("pId",solo.getId());
        executarComando("Solo.excluir", parametros);
    }	

    @Override
    public List<Solo> obterTodos() {
    	return executarQuery("Solo.todos", null, null, null);
    }	
    @Override
	public Criteria criarCriteriaExample(Solo entidade, MatchMode match) {

		Criteria criteria = super.criarCriteriaExample(entidade, match);
				
		criteria = prepararCriteria(entidade, criteria);		
		return criteria;
	}

	private Criteria prepararCriteria(Solo entidadeLocal, Criteria criteria) {
		if (entidadeLocal.getSortField().equals("nome")) {
			if (entidadeLocal.getSortOrder().equals(SortOrder.DESCENDING)) {
				criteria.addOrder(Order.desc("nome"));
			}else{
				criteria.addOrder(Order.asc("nome"));
			}
		}
		return criteria;
		
	}
}
