package com.agrosolutions.sai.dao.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.collections.map.HashedMap;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.agrosolutions.sai.dao.UsuarioDAO;
import com.agrosolutions.sai.exception.SaiException;
import com.agrosolutions.sai.model.Usuario;

@Repository
public class UsuarioDAOImpl extends DAOImpl<Usuario> implements UsuarioDAO {
	
	private static final long serialVersionUID = 3515822517075460426L;
	static Logger logger = Logger.getLogger(UsuarioDAOImpl.class);

	@Override
	public Usuario pesquisar(String username) {
		@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
		parametros.put("pUsername", username);

		return executarQuery("Usuario.pesquisarPorNome", parametros);
	}

	@Override
	public Criteria criarCriteriaExample(Usuario entidade, MatchMode match) {

		Criteria criteria = super.criarCriteriaExample(entidade, match);
				
		criteria = prepararCriteria(entidade, criteria);		
		return criteria;
	}

	private Criteria prepararCriteria(Usuario entidadeLocal, Criteria criteria) {
		
		if (entidadeLocal.getPerfil() != null) {
			
			Criteria p = criteria.createCriteria("perfil");   
			p.add(Restrictions.ilike("descricao", entidadeLocal.getPerfil().getDescricao()));   
		}

		return criteria;
	}	
    @Override
    public void excluir(Usuario usuario) {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
    	parametros.put("pId",usuario.getId());
        executarComando("Usuario.excluir", parametros);
    }	
	
    @Override
    public List<Usuario> obterTodos() {
    	return executarQuery("Usuario.todos", null, null, null);
    }
    
	@SuppressWarnings("unchecked")
	@Override
	public int alterarSenha(Usuario u) throws SaiException {
			Map<String, Object> parametros = new HashedMap();
			parametros.put("pSenha",u.getPassword());
			parametros.put("pId",u.getId());
			int i = executarComando("Usuario.alterarSenha", parametros);
			
			return i;
	}

	@Override
	public void alterarDistritoPadrao(Usuario usuario) throws SaiException {
		@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
		parametros.put("pId", usuario.getId());
    	parametros.put("pDistrito", usuario.getDistritoPadrao());
        executarComando("Usuario.alterarDistritoPadrao", parametros);
	}

	@Override
	public List<Usuario> pesquisarPorEmail(String email) {
		@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
		parametros.put("pEmail", email);

		return executarQuery("Usuario.pesquisarPorEmail", parametros, 0, 100);
	}
	
}
