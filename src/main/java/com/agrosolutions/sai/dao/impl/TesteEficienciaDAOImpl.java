package com.agrosolutions.sai.dao.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.map.HashedMap;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.agrosolutions.sai.dao.TesteEficienciaDAO;
import com.agrosolutions.sai.model.Setor;
import com.agrosolutions.sai.model.TesteEficiencia;

@Repository
public class TesteEficienciaDAOImpl extends DAOImpl<TesteEficiencia> implements TesteEficienciaDAO {

	private static final long serialVersionUID = -4616531599416072886L;
	static Logger logger = Logger.getLogger(TesteEficienciaDAOImpl.class);
	
    @Override
    public void excluir(TesteEficiencia t) {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
    	parametros.put("pId",t.getId());
        executarComando("TesteEficiencia.excluir", parametros);
    }	
    
	@Override
	public List<TesteEficiencia> pesquisarPorSetor(Setor setor) {
		@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
		parametros.put("pSetor", setor);

		return executarQuery("TesteEficiencia.pesquisarPorSetor", parametros, null, null);
	}

	@Override
	public TesteEficiencia pesquisarPorSetorData(Setor setor, Date data) {
		@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
		parametros.put("pSetor", setor);
		parametros.put("pData", data);

		return executarQuery("TesteEficiencia.pesquisarPorSetorData", parametros);
	}
}
