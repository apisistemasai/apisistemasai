package com.agrosolutions.sai.dao.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.collections.map.HashedMap;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.primefaces.model.SortOrder;
import org.springframework.stereotype.Repository;

import com.agrosolutions.sai.dao.MunicipioDAO;
import com.agrosolutions.sai.model.BaciaHidrografica;
import com.agrosolutions.sai.model.Municipio;

@Repository
public class MunicipioDAOImpl extends DAOImpl<Municipio> implements MunicipioDAO {
	
	private static final long serialVersionUID = -4539925590006264211L;

	static Logger logger = Logger.getLogger(MunicipioDAOImpl.class);

	@Override
	public Criteria criarCriteriaExample(Municipio entidade, MatchMode match) {

		Criteria criteria = super.criarCriteriaExample(entidade, match);
				
		criteria = prepararCriteria(entidade, criteria);		
		return criteria;
	}

	private Criteria prepararCriteria(Municipio entidadeLocal, Criteria criteria) {
		
		if (entidadeLocal.getSortField().equals("nome")) {
			if (entidadeLocal.getSortOrder().equals(SortOrder.DESCENDING)) {
				criteria.addOrder(Order.desc("nome"));
			}else{
				criteria.addOrder(Order.asc("nome"));
			}
		}
		if (entidadeLocal.getSortField().equals("localizacao")) {
			if (entidadeLocal.getSortOrder().equals(SortOrder.DESCENDING)) {
				criteria.addOrder(Order.desc("localizacao"));
			}else{
				criteria.addOrder(Order.asc("localizacao"));
			}
		}

		criteria = criteria.add(Restrictions.eq("bacia", entidadeLocal.getBacia()));
		
		return criteria;
	}

    @Override
    public void excluir(Municipio municipio) {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
    	parametros.put("pId",municipio.getId());
        executarComando("Municipio.excluir", parametros);
    }	
    
    @Override
    @SuppressWarnings("unchecked")
	public List<Municipio> obterPorParametro(Municipio entidade, Integer indiceInicio, Integer quantidade) {
    	MatchMode match = MatchMode.START;
    	
    	if(entidade.getNome() != null) {
    		match = MatchMode.ANYWHERE;
    	}
    	
		Criteria criterio = criarCriteriaExample(entidade, match);
		if(indiceInicio!=null){
			criterio.setFirstResult(indiceInicio);
			criterio.setMaxResults(quantidade);
		}
		return criterio.list();
	}
   
    @Override
    public List<Municipio> pesquisarPorBacia(List<BaciaHidrografica> bacias) {
    	String jpql = "SELECT m FROM Municipio m WHERE m.bacia IN (:pBacia)";
    	return this.entityManager.createQuery(jpql, Municipio.class)
			.setParameter("pBacia", bacias)
			.getResultList();
    }
    
	@Override
	public List<Municipio> pesquisarPorBacia(List<BaciaHidrografica> bacias, Integer indiceInicio, Integer quantidade) {
		String jpql = "SELECT m FROM Municipio m WHERE m.bacia IN (:pBacia) ORDER BY m.nome";
		return this.entityManager.createQuery(jpql, Municipio.class)
				.setFirstResult(indiceInicio).setMaxResults(quantidade)
				.setParameter("pBacia", bacias).getResultList();
	}
	
	@Override
	public Long totalMunicipioPorBacia(List<BaciaHidrografica> bacias) {
		String jpql = "SELECT count(*) as total FROM Municipio m WHERE m.bacia IN (:pBacia)";
		return this.entityManager.createQuery(jpql, Long.class)
				.setParameter("pBacia", bacias).getSingleResult();
	}

}
