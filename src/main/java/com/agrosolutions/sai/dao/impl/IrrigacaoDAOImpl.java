package com.agrosolutions.sai.dao.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.map.HashedMap;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.primefaces.model.SortOrder;
import org.springframework.stereotype.Repository;

import com.agrosolutions.sai.dao.IrrigacaoDAO;
import com.agrosolutions.sai.model.Cultura;
import com.agrosolutions.sai.model.Irrigacao;
import com.agrosolutions.sai.model.Irrigante;
import com.agrosolutions.sai.model.Plantio;
import com.agrosolutions.sai.model.Variedade;

@Repository
public class IrrigacaoDAOImpl extends DAOImpl<Irrigacao> implements IrrigacaoDAO {

	private static final long serialVersionUID = 667010602324678065L;
	static Logger logger = Logger.getLogger(IrrigacaoDAOImpl.class);
	
	@Override
	public List<Irrigacao> pesquisarPorIrriganteData(Date d, Irrigante ir) {
		@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
		parametros.put("pData", d);
		parametros.put("pIrrigante", ir);

		return executarQuery("Irrigacao.pesquisarPorIrriganteData", parametros, null, null);
	}

	@Override
	public List<Irrigacao> pesquisarPorIrrigantePeriodo(Date dIni, Date dFim, Irrigante ir) {
		@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
		parametros.put("pDataIni", dIni);
		parametros.put("pDataFim", dFim);
		parametros.put("pIrrigante", ir);

		return executarQuery("Irrigacao.pesquisarPorIrrigantePeriodo", parametros, null, null);
	}

	@Override
	public List<Irrigacao> pesquisarPorPlantio(Plantio p) {
		@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
		parametros.put("pPlantio", p);

		return executarQuery("Irrigacao.pesquisarPorPlantio", parametros, null, null);
	}

	@Override
	public List<Irrigacao> pesquisarPorData(Date d) {
		@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
		parametros.put("pData", d);

		return executarQuery("Irrigacao.SMSTI", parametros, null, null);
	}

	@Override
	public Irrigacao pesquisar(Plantio p, Date data) {
		@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
		parametros.put("pPlantio", p);
		parametros.put("pData", data);

		return executarQuery("Irrigacao.pesquisar", parametros);
	}

    @Override
    public void excluir(Irrigacao irrigacao) {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
    	parametros.put("pId",irrigacao.getId());
    	parametros.put("pMotivo", irrigacao.getMotivo());
        executarComando("Irrigacao.excluir", parametros);
    }
    
    @Override
    public void deletar(Plantio p) {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
    	parametros.put("pPlantio",p);
        executarComando("Irrigacao.deletar", parametros);
    }

    @Override
    public void enviarSMS(Irrigacao irrigacao) {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
    	parametros.put("pId",irrigacao.getId());
        executarComando("Irrigacao.enviarSMS", parametros);
    }

    @Override
	public Criteria criarCriteriaExample(Irrigacao entidade, MatchMode match) {

		Criteria criteria = super.criarCriteriaExample(entidade, match);
				
		criteria = prepararCriteria(entidade, criteria);		
		return criteria;
	}

	@SuppressWarnings("unused")
	private Criteria prepararCriteria(Irrigacao entidadeLocal, Criteria criteria) {
		if (entidadeLocal.getSortField().equals("tempoIrrigacao")) {
			if (entidadeLocal.getSortOrder().equals(SortOrder.DESCENDING)) {
				criteria.addOrder(Order.desc("tempoIrrigacao"));
			}else{
				criteria.addOrder(Order.asc("tempoIrrigacao"));
			}
		}

		Criteria c = criteria.createCriteria("plantio").add( Example.create(entidadeLocal.getPlantio()));
		c = c.createCriteria("cultivo").add( Example.create(entidadeLocal.getPlantio().getCultivo()));
		
		if (entidadeLocal.getSortField().equals("plantio.cultivo.dataInicio")) {
			if (entidadeLocal.getSortOrder().equals(SortOrder.DESCENDING)) {
				c.addOrder(Order.desc("dataInicio"));
			}else{
				c.addOrder(Order.asc("dataInicio"));
			}
		}
		
		if (entidadeLocal.getPlantio().getCultivo().getVariedade() != null) {
			Criteria v = c.createCriteria("variedade").add( Example.create(entidadeLocal.getPlantio().getCultivo().getVariedade()));
		}else{
			if (entidadeLocal.getSortField().equals("plantio.cultivo.variedade.nome")) {
				Criteria v = c.createCriteria("variedade").add( Example.create(new Variedade()));
				if (entidadeLocal.getSortOrder().equals(SortOrder.DESCENDING)) {
					v.addOrder(Order.desc("nome"));
				}else{
					v.addOrder(Order.asc("nome"));
				}
			}
		}
		if (entidadeLocal.getPlantio().getCultivo().getCultura() != null) {
			Criteria cult = c.createCriteria("cultura").add( Example.create(entidadeLocal.getPlantio().getCultivo().getCultura()));
		}else{
			if (entidadeLocal.getSortField().equals("plantio.cultivo.cultura.nome")) {
				Criteria cult = c.createCriteria("cultura").add( Example.create(new Cultura()));
				if (entidadeLocal.getSortOrder().equals(SortOrder.DESCENDING)) {
					cult.addOrder(Order.desc("nome"));
				}else{
					cult.addOrder(Order.asc("nome"));
				}
			}
		}		
		c = c.createCriteria("irrigante").add( Example.create(entidadeLocal.getPlantio().getCultivo().getIrrigante()));
		if (entidadeLocal.getSortField().equals("plantio.cultivo.irrigante.nome")) {
			if (entidadeLocal.getSortOrder().equals(SortOrder.DESCENDING)) {
				c.addOrder(Order.desc("nome"));
			}else{
				c.addOrder(Order.asc("nome"));
			}
		}
		
		if (entidadeLocal.getSortField().equals("plantio.cultivo.irrigante.localizacao")) {
			if (entidadeLocal.getSortOrder().equals(SortOrder.DESCENDING)) {
				c.addOrder(Order.desc("localizacao"));
			}else{
				c.addOrder(Order.asc("localizacao"));
			}
		}

		if (entidadeLocal.getPlantio().getCultivo().getIrrigante().getLocalizacao() != null && !entidadeLocal.getPlantio().getCultivo().getIrrigante().getLocalizacao().isEmpty()) {
			c.add(Restrictions.eq("localizacao", entidadeLocal.getPlantio().getCultivo().getIrrigante().getLocalizacao()));
		}else{
			if (entidadeLocal.getPlantio().getCultivo().getIrrigante().getArea().getId() != null) {
				c = c.createCriteria("area").add( Example.create(entidadeLocal.getPlantio().getCultivo().getIrrigante().getArea()));;
			}else{
				if (entidadeLocal.getSortField().equals("plantio.cultivo.irrigante.area.nome")) {
					c = c.createCriteria("area").add( Example.create(entidadeLocal.getPlantio().getCultivo().getIrrigante().getArea()));;
					if (entidadeLocal.getSortOrder().equals(SortOrder.DESCENDING)) {
						c.addOrder(Order.desc("nome"));
					}else{
						c.addOrder(Order.asc("nome"));
					}
					c = c.createCriteria("distrito");
					c.add(Restrictions.eq("id", entidadeLocal.getPlantio().getCultivo().getIrrigante().getArea().getDistrito().getId()));
				}else{
					if (entidadeLocal.getPlantio().getCultivo().getIrrigante().getArea().getDistrito() != null) {
						c = c.createCriteria("area").createCriteria("distrito");
						c.add(Restrictions.eq("id", entidadeLocal.getPlantio().getCultivo().getIrrigante().getArea().getDistrito().getId()));
					}
				}
			}
		}

		return c;
	}
}
