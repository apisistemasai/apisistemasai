package com.agrosolutions.sai.dao.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.map.HashedMap;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.agrosolutions.sai.dao.PlantioDAO;
import com.agrosolutions.sai.model.Area;
import com.agrosolutions.sai.model.Cultivo;
import com.agrosolutions.sai.model.Distrito;
import com.agrosolutions.sai.model.GraficoPizza;
import com.agrosolutions.sai.model.Irrigante;
import com.agrosolutions.sai.model.Plantio;
import com.agrosolutions.sai.model.Setor;
import com.agrosolutions.sai.model.TipoIrrigante;
import com.agrosolutions.sai.model.TipoSolo;


@Repository
public class PlantioDAOImpl extends DAOImpl<Plantio> implements PlantioDAO {

	private static final long serialVersionUID = 6819691161882947140L;
	static Logger logger = Logger.getLogger(PlantioDAOImpl.class);
	
    @Override
    public void excluir(Plantio plantio) {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
    	parametros.put("pId",plantio.getId());
        executarComando("Plantio.excluir", parametros);
    }	

	@Override
	public List<Plantio> pesquisar(Cultivo cultivo) { 
		@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
		parametros.put("pCultivo", cultivo);

		return executarQuery("Plantio.pesquisar", parametros, null, null);
	}
    
	@Override
	public List<Plantio> ativos(Area area, Date data) {
		@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
		parametros.put("pArea", area);
		parametros.put("pData", data);

		return executarQuery("Plantio.ativos", parametros, null, null);
		
	}
	
	@Override
	public List<Plantio> ativos2(Area area, Date data) {
		@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
		parametros.put("pArea", area);
		parametros.put("pData", data);

		return executarQuery("Plantio.ativos2", parametros, null, null);
		
	}
	
	@Override
	public List<Plantio> pesquisarPorSetor(Setor setor) {
		@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
		parametros.put("pSetor", setor);

		return executarQuery("Plantio.pesquisarPorSetor", parametros, null, null);
	}

	@Override
	public List<Plantio> pesquisarPorIrrigante(Irrigante param) {
		@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
		parametros.put("pIrrigante", param);

		return executarQuery("Plantio.pesquisarPorIrrigante", parametros, null, null);
	}


	@Override
	@SuppressWarnings("unchecked")
	public List<GraficoPizza> emissorPorDistrito(Distrito a) {
		List<GraficoPizza> retorno = new ArrayList<GraficoPizza>();
		
		String jpql = "select f.nome, count(*) as total from Setor s inner join s.emissores e join e.fabricante f join s.irrigante i join i.area a WHERE a.distrito = :pDistrito Group by f.nome order by total desc";
		String jpqlTotal = "select count(*) from Setor s inner join s.emissores e join s.irrigante i join i.area a WHERE a.distrito = :pDistrito";
		Long total = this.entityManager.createQuery(jpqlTotal, Long.class).setParameter("pDistrito", a).getSingleResult();

		List<Object[]> objs = (List<Object[]>) this.entityManager.createQuery(jpql).setParameter("pDistrito", a).setFirstResult(0).setMaxResults(5).getResultList();
		for (Object[] t : objs) {
			GraficoPizza vpm = new GraficoPizza((String) t[0], (Long) t[1], total, "un");
			retorno.add(vpm);
		}

		return retorno;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<GraficoPizza> bombaPorDistrito(Distrito a) {
		List<GraficoPizza> retorno = new ArrayList<GraficoPizza>();
		String jpql = "select f.nome, count(*) as total from Irrigante  i inner join i.bombas b join b.fabricante f join i.area a WHERE a.distrito = :pDistrito Group by f.nome order by total desc";
		String jpqlTotal = "select count(*) from Irrigante  i inner join i.bombas b join i.area a WHERE a.distrito = :pDistrito";
		Long total = this.entityManager.createQuery(jpqlTotal, Long.class).setParameter("pDistrito", a).getSingleResult();

		List<Object[]> objs = (List<Object[]>) this.entityManager.createQuery(jpql).setParameter("pDistrito", a).setFirstResult(0).setMaxResults(5).getResultList();
		for (Object[] t : objs) {
			GraficoPizza vpm = new GraficoPizza((String) t[0], (Long) t[1], total, "un");
			retorno.add(vpm);
		}

		return retorno;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<GraficoPizza> filtroPorDistrito(Distrito a) {
		List<GraficoPizza> retorno = new ArrayList<GraficoPizza>();
		String jpql = "select fab.nome, count(*) as total from Irrigante  i inner join i.filtros f join f.fabricante fab join i.area a WHERE a.distrito = :pDistrito Group by fab.nome order by total desc";
		String jpqlTotal = "select count(*) from Irrigante  i inner join i.filtros f join i.area a WHERE a.distrito = :pDistrito";
		Long total = this.entityManager.createQuery(jpqlTotal, Long.class).setParameter("pDistrito", a).getSingleResult();
		List<Object[]> objs = (List<Object[]>) this.entityManager.createQuery(jpql).setParameter("pDistrito", a).setFirstResult(0).setMaxResults(5).getResultList();
		for (Object[] t : objs) {
			GraficoPizza vpm = new GraficoPizza((String) t[0], (Long) t[1], total, "un");
			retorno.add(vpm);
		}

		return retorno;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<GraficoPizza> valvulaPorDistrito(Distrito a) {
		List<GraficoPizza> retorno = new ArrayList<GraficoPizza>();
		String jpql = "select f.nome, count(*) as total from Irrigante  i inner join i.valvulas v join v.fabricante f join i.area a WHERE a.distrito = :pDistrito Group by f.nome order by total desc";
		String jpqlTotal = "select count(*) from Irrigante  i inner join i.valvulas v join i.area a WHERE a.distrito = :pDistrito";
		Long total = this.entityManager.createQuery(jpqlTotal, Long.class).setParameter("pDistrito", a).getSingleResult();
		List<Object[]> objs = (List<Object[]>) this.entityManager.createQuery(jpql).setParameter("pDistrito", a).setFirstResult(0).setMaxResults(5).getResultList();
		for (Object[] t : objs) {
			GraficoPizza vpm = new GraficoPizza((String) t[0], (Long) t[1], total, "un");
			retorno.add(vpm);
		}

		return retorno;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<GraficoPizza> tuboPorDistrito(Distrito a) {
		List<GraficoPizza> retorno = new ArrayList<GraficoPizza>();
		String jpql = "select f.nome, count(*) as total from Irrigante  i inner join i.tubos t join t.fabricante f join i.area a WHERE a.distrito = :pDistrito Group by f.nome order by total desc";
		String jpqlTotal = "select count(*) from Irrigante  i inner join i.tubos t join i.area a WHERE a.distrito = :pDistrito";
		Long total = this.entityManager.createQuery(jpqlTotal, Long.class).setParameter("pDistrito", a).getSingleResult();
		List<Object[]> objs = (List<Object[]>) this.entityManager.createQuery(jpql).setParameter("pDistrito", a).setFirstResult(0).setMaxResults(5).getResultList();
		for (Object[] t : objs) {
			GraficoPizza vpm = new GraficoPizza((String) t[0], (Long) t[1], total, "un");
			retorno.add(vpm);
		}

		return retorno;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<GraficoPizza> culturaAtivaPorDistrito(Distrito a, Date data) {
		List<GraficoPizza> retorno = new ArrayList<GraficoPizza>();
		String jpql = "select cult.nome, count(*) as total from Plantio p join p.cultivo c join c.cultura cult join p.setor s join s.irrigante i join i.area a WHERE a.distrito = :pDistrito AND c.dataInicio <= :pData AND (p.dataFim is null OR p.dataFim >= :pData) Group by cult.nome order by total desc";
		String jpqlTotal = "select count(*) from Plantio p join p.cultivo c join c.cultura cult join p.setor s join s.irrigante i join i.area a WHERE a.distrito = :pDistrito AND c.dataInicio <= :pData AND (p.dataFim is null OR p.dataFim >= :pData)";
		Long total = this.entityManager.createQuery(jpqlTotal, Long.class).setParameter("pDistrito", a).setParameter("pData", data).getSingleResult();
		List<Object[]> objs = (List<Object[]>) this.entityManager.createQuery(jpql).setParameter("pDistrito", a).setParameter("pData", data).setFirstResult(0).setMaxResults(5).getResultList();
		for (Object[] t : objs) {
			GraficoPizza vpm = new GraficoPizza((String) t[0], (Long) t[1], total, "un");
			retorno.add(vpm);
		}

		return retorno;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<GraficoPizza> culturaHecPorDistrito(Distrito a, Date data) {
		List<GraficoPizza> retorno = new ArrayList<GraficoPizza>();
		String jpql = "select cult.nome, sum(p.areatotal) as total from Plantio p join p.cultivo c join c.cultura cult join p.setor s join s.irrigante i join i.area a WHERE a.distrito = :pDistrito AND c.dataInicio <= :pData AND (p.dataFim is null OR p.dataFim >= :pData) Group by cult.nome order by total desc";
		String jpqlTotal = "select sum(s.areaTotal) from Plantio p join p.cultivo c join c.cultura cult join p.setor s join s.irrigante i join i.area a WHERE a.distrito = :pDistrito AND c.dataInicio <= :pData AND (p.dataFim is null OR p.dataFim >= :pData)";
		BigDecimal total = this.entityManager.createQuery(jpqlTotal, BigDecimal.class).setParameter("pDistrito", a).setParameter("pData", data).getSingleResult();
		List<Object[]> objs = (List<Object[]>) this.entityManager.createQuery(jpql).setParameter("pDistrito", a).setParameter("pData", data).setFirstResult(0).setMaxResults(5).getResultList();
		for (Object[] t : objs) {
			GraficoPizza vpm = new GraficoPizza((String) t[0], ((BigDecimal) t[1]).longValue(), total.longValue(), "ha");
			retorno.add(vpm);
		}

		return retorno;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<GraficoPizza> soloPorDistrito(Distrito a) {
		List<GraficoPizza> retorno = new ArrayList<GraficoPizza>();
		String jpql = "select s.tipoSolo, sum(s.areaTotal) from Setor s join s.irrigante i join i.area a WHERE a.distrito = :pDistrito and s.tipoSolo is not null Group by s.tipoSolo";
		List<Object[]> objs = (List<Object[]>) this.entityManager.createQuery(jpql).setParameter("pDistrito", a).getResultList();
		for (Object[] t : objs) {
			GraficoPizza vpm = new GraficoPizza((String)((TipoSolo)t[0]).getDescricao(), ((BigDecimal) t[1]).longValue(), null, "ha");
			retorno.add(vpm);
		}

		return retorno;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<GraficoPizza> irrigantePorDistrito(Distrito a) {
		List<GraficoPizza> retorno = new ArrayList<GraficoPizza>();
		String jpql = "select i.tipoIrrigante, count(*) from Irrigante i join i.area a WHERE a.distrito = :pDistrito  Group by i.tipoIrrigante";
		List<Object[]> objs = (List<Object[]>) this.entityManager.createQuery(jpql).setParameter("pDistrito", a).getResultList();
		for (Object[] t : objs) {
			GraficoPizza vpm = new GraficoPizza((String)((TipoIrrigante)t[0]).getDescricao(), (Long) t[1], null, "lotes");
			retorno.add(vpm);
		}

		return retorno;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<GraficoPizza> atividadePorDistrito(Distrito a, Date data) {
		List<GraficoPizza> retorno = new ArrayList<GraficoPizza>();
		String jpql = "select at.nome, sum(p.areatotal) as total from Plantio p join p.cultivo c join c.cultura cult join cult.atividade at join p.setor s join s.irrigante i join i.area a WHERE a.distrito = :pDistrito AND c.dataInicio <= :pData AND (p.dataFim is null OR p.dataFim >= :pData) Group by at.nome order by total desc";
		String jpqlTotal = "select sum(s.areaTotal) from Plantio p join p.cultivo c join c.cultura cult join cult.atividade at join p.setor s join s.irrigante i join i.area a WHERE a.distrito = :pDistrito AND c.dataInicio <= :pData AND (p.dataFim is null OR p.dataFim >= :pData)";
		BigDecimal total = this.entityManager.createQuery(jpqlTotal, BigDecimal.class).setParameter("pDistrito", a).setParameter("pData", data).getSingleResult();
		List<Object[]> objs = (List<Object[]>) this.entityManager.createQuery(jpql).setParameter("pDistrito", a).setParameter("pData", data).setFirstResult(0).setMaxResults(5).getResultList();
		for (Object[] t : objs) {
			GraficoPizza vpm = new GraficoPizza((String)t[0], ((BigDecimal) t[1]).longValue(), total.longValue(), "ha" );
			retorno.add(vpm);
		}

		return retorno;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<GraficoPizza> culturaAtivaPorArea(Area a, Date data) {
		List<GraficoPizza> retorno = new ArrayList<GraficoPizza>();
		String jpql = "select cult.nome, count(*) as total from Plantio p join p.cultivo c join c.cultura cult join p.setor s join s.irrigante i WHERE i.area = :pArea AND c.dataInicio <= :pData AND (p.dataFim is null OR p.dataFim >= :pData) Group by cult.nome order by total desc";
		String jpqlTotal = "select count(*) from Plantio p join p.cultivo c join c.cultura cult join p.setor s join s.irrigante i WHERE i.area = :pArea AND c.dataInicio <= :pData AND (p.dataFim is null OR p.dataFim >= :pData)";
		Long total = this.entityManager.createQuery(jpqlTotal, Long.class).setParameter("pArea", a).setParameter("pData", data).getSingleResult();
		List<Object[]> objs = (List<Object[]>) this.entityManager.createQuery(jpql).setParameter("pArea", a).setParameter("pData", data).setFirstResult(0).setMaxResults(5).getResultList();
		for (Object[] t : objs) {
			GraficoPizza vpm = new GraficoPizza((String) t[0], (Long) t[1], total, "un");
			retorno.add(vpm);
		}

		return retorno;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<GraficoPizza> culturaHecPorArea(Area a, Date data) {
		List<GraficoPizza> retorno = new ArrayList<GraficoPizza>();
		String jpql = "select cult.nome, sum(p.areatotal) as total from Plantio p join p.cultivo c join c.cultura cult join p.setor s join s.irrigante i WHERE i.area = :pArea AND c.dataInicio <= :pData AND (p.dataFim is null OR p.dataFim >= :pData) Group by cult.nome order by total desc";
		String jpqlTotal = "select sum(p.areatotal) from Plantio p join p.cultivo c join c.cultura cult join p.setor s join s.irrigante i WHERE i.area = :pArea AND c.dataInicio <= :pData AND (p.dataFim is null OR p.dataFim >= :pData)";
		BigDecimal total = this.entityManager.createQuery(jpqlTotal, BigDecimal.class).setParameter("pArea", a).setParameter("pData", data).getSingleResult();
		List<Object[]> objs = (List<Object[]>) this.entityManager.createQuery(jpql).setParameter("pArea", a).setParameter("pData", data).setFirstResult(0).setMaxResults(5).getResultList();
		for (Object[] t : objs) {
			GraficoPizza vpm = new GraficoPizza((String) t[0], ((BigDecimal) t[1]).longValue(), total.longValue(), "ha");
			retorno.add(vpm);
		}

		return retorno;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<GraficoPizza> soloPorArea(Area a) {
		List<GraficoPizza> retorno = new ArrayList<GraficoPizza>();
		String jpql = "select s.tipoSolo, sum(s.areaTotal) from Setor s join s.irrigante i WHERE i.area = :pArea and s.tipoSolo is not null Group by s.tipoSolo";
		List<Object[]> objs = (List<Object[]>) this.entityManager.createQuery(jpql).setParameter("pArea", a).getResultList();
		for (Object[] t : objs) {
			GraficoPizza vpm = new GraficoPizza((String)((TipoSolo)t[0]).getDescricao(), ((BigDecimal) t[1]).longValue(), null, "ha");
			retorno.add(vpm);
		}

		return retorno;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<GraficoPizza> irrigantePorArea(Area a) {
		List<GraficoPizza> retorno = new ArrayList<GraficoPizza>();
		String jpql = "select i.tipoIrrigante, count(*) from Irrigante i WHERE i.area = :pArea Group by i.tipoIrrigante ";
		List<Object[]> objs = (List<Object[]>) this.entityManager.createQuery(jpql).setParameter("pArea", a).getResultList();
		for (Object[] t : objs) {
			GraficoPizza vpm = new GraficoPizza((String)((TipoIrrigante)t[0]).getDescricao(), (Long) t[1], null, "lotes");
			retorno.add(vpm);
		}

		return retorno;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<GraficoPizza> atividadePorArea(Area a, Date data) {
		List<GraficoPizza> retorno = new ArrayList<GraficoPizza>();
		String jpql = "select a.nome, sum(p.areatotal) as total from Plantio p join p.cultivo c join c.cultura cult join cult.atividade a join p.setor s join s.irrigante i WHERE i.area = :pArea AND c.dataInicio <= :pData AND (p.dataFim is null OR p.dataFim >= :pData) Group by a.nome order by total desc";
		String jpqlTotal = "select sum(s.areaTotal) from Plantio p join p.cultivo c join c.cultura cult join cult.atividade a join p.setor s join s.irrigante i WHERE i.area = :pArea AND c.dataInicio <= :pData AND (p.dataFim is null OR p.dataFim >= :pData)";
		BigDecimal total = this.entityManager.createQuery(jpqlTotal, BigDecimal.class).setParameter("pArea", a).setParameter("pData", data).getSingleResult();
		List<Object[]> objs = (List<Object[]>) this.entityManager.createQuery(jpql).setParameter("pArea", a).setParameter("pData", data).setFirstResult(0).setMaxResults(5).getResultList();
		for (Object[] t : objs) {
			GraficoPizza vpm = new GraficoPizza((String)t[0], ((BigDecimal) t[1]).longValue(), total.longValue(), "ha");
			retorno.add(vpm);
		}

		return retorno;
	}

	@SuppressWarnings("unchecked")
	public List<GraficoPizza> emissorPorArea(Area a) {
		List<GraficoPizza> retorno = new ArrayList<GraficoPizza>();
		String jpql = "select f.nome, count(*) as total from Setor s inner join s.emissores e join e.fabricante f join s.irrigante i WHERE i.area = :pArea Group by f.nome order by total desc";
		String jpqlTotal = "select count(*) from Setor s inner join s.emissores e join s.irrigante i WHERE i.area = :pArea";
		Long total = this.entityManager.createQuery(jpqlTotal, Long.class).setParameter("pArea", a).getSingleResult();
		List<Object[]> objs = (List<Object[]>) this.entityManager.createQuery(jpql).setParameter("pArea", a).setFirstResult(0).setMaxResults(5).getResultList();
		for (Object[] t : objs) {
			GraficoPizza vpm = new GraficoPizza((String)t[0], (Long) t[1], total, "un");
			retorno.add(vpm);
		}

		return retorno;
	}

	@SuppressWarnings("unchecked")
	public List<GraficoPizza> bombaPorArea(Area a) {
		List<GraficoPizza> retorno = new ArrayList<GraficoPizza>();
		String jpql = "select f.nome, count(*) as total from Irrigante  i inner join i.bombas b join b.fabricante f WHERE i.area = :pArea Group by f.nome order by total desc";
		String jpqlTotal = "select count(*) from Irrigante  i inner join i.bombas b WHERE i.area = :pArea";
		Long total = this.entityManager.createQuery(jpqlTotal, Long.class).setParameter("pArea", a).getSingleResult();
		List<Object[]> objs = (List<Object[]>) this.entityManager.createQuery(jpql).setParameter("pArea", a).setFirstResult(0).setMaxResults(5).getResultList();
		for (Object[] t : objs) {
			GraficoPizza vpm = new GraficoPizza((String)t[0], (Long) t[1], total, "un");
			retorno.add(vpm);
		}

		return retorno;
	}

	@SuppressWarnings("unchecked")
	public List<GraficoPizza> filtroPorArea(Area a) {
		List<GraficoPizza> retorno = new ArrayList<GraficoPizza>();
		String jpql = "select fab.nome, count(*) as total from Irrigante  i inner join i.filtros f join f.fabricante fab WHERE i.area = :pArea Group by fab.nome order by total desc";
		String jpqlTotal = "select count(*) from Irrigante  i inner join i.filtros f WHERE i.area = :pArea";
		Long total = this.entityManager.createQuery(jpqlTotal, Long.class).setParameter("pArea", a).getSingleResult();
		List<Object[]> objs = (List<Object[]>) this.entityManager.createQuery(jpql).setParameter("pArea", a).setFirstResult(0).setMaxResults(5).getResultList();
		for (Object[] t : objs) {
			GraficoPizza vpm = new GraficoPizza((String)t[0], (Long) t[1], total, "un");
			retorno.add(vpm);
		}

		return retorno;
	}

	@SuppressWarnings("unchecked")
	public List<GraficoPizza> valvulaPorArea(Area a) {
		List<GraficoPizza> retorno = new ArrayList<GraficoPizza>();
		String jpql = "select f.nome, count(*) as total from Irrigante  i inner join i.valvulas v join v.fabricante f WHERE i.area = :pArea Group by f.nome order by total desc";
		String jpqlTotal = "select count(*) from Irrigante  i inner join i.valvulas v WHERE i.area = :pArea";
		Long total = this.entityManager.createQuery(jpqlTotal, Long.class).setParameter("pArea", a).getSingleResult();
		List<Object[]> objs = (List<Object[]>) this.entityManager.createQuery(jpql).setParameter("pArea", a).setFirstResult(0).setMaxResults(5).getResultList();
		for (Object[] t : objs) {
			GraficoPizza vpm = new GraficoPizza((String)t[0], (Long) t[1], total, "un");
			retorno.add(vpm);
		}

		return retorno;
	}

	@SuppressWarnings("unchecked")
	public List<GraficoPizza> tuboPorArea(Area a) {
		List<GraficoPizza> retorno = new ArrayList<GraficoPizza>();
		String jpql = "select f.nome, count(*) as total from Irrigante  i inner join i.tubos t join t.fabricante f WHERE i.area = :pArea Group by f.nome order by total desc";
		String jpqlTotal = "select count(*) from Irrigante  i inner join i.tubos t WHERE i.area = :pArea";
		Long total = this.entityManager.createQuery(jpqlTotal, Long.class).setParameter("pArea", a).getSingleResult();
		List<Object[]> objs = (List<Object[]>) this.entityManager.createQuery(jpql).setParameter("pArea", a).setFirstResult(0).setMaxResults(5).getResultList();
		for (Object[] t : objs) {
			GraficoPizza vpm = new GraficoPizza((String)t[0], (Long) t[1], total, "un");
			retorno.add(vpm);
		}

		return retorno;
	}

	@Override
	public BigDecimal totalHecSequeiro(Distrito d) {
		String jpql = "SELECT sum(p.areatotal) FROM Plantio p JOIN p.setor s JOIN s.irrigante i JOIN i.area a WHERE a.distrito = :pDistrito AND p.dataFim IS NULL AND p.sequeiro = true";
    	
    	return this.entityManager.createQuery(jpql, BigDecimal.class)
			.setParameter("pDistrito", d).getSingleResult();
	}

	@Override
	public BigDecimal totalHecConsorcio(Distrito d) {
		String jpql = "SELECT sum(p.areatotal) FROM Plantio p JOIN p.setor s JOIN s.irrigante i JOIN i.area a WHERE a.distrito = :pDistrito AND p.dataFim IS NULL AND p.consorcio = true";
    	
    	return this.entityManager.createQuery(jpql, BigDecimal.class)
			.setParameter("pDistrito", d).getSingleResult();
	}
	
	@Override
	public BigDecimal totalHecSequeiro(Area a) {
		String jpql = "SELECT sum(p.areatotal) FROM Plantio p JOIN p.setor s JOIN s.irrigante i JOIN i.area a WHERE a = :pArea AND p.dataFim IS NULL AND p.sequeiro = true";
    	
    	return this.entityManager.createQuery(jpql, BigDecimal.class)
			.setParameter("pArea", a).getSingleResult();
	}

	@Override
	public BigDecimal totalHecConsorcio(Area a) {
		String jpql = "SELECT sum(p.areatotal) FROM Plantio p JOIN p.setor s JOIN s.irrigante i JOIN i.area a WHERE a = :pArea AND p.dataFim IS NULL AND p.consorcio = true";
    	
    	return this.entityManager.createQuery(jpql, BigDecimal.class)
			.setParameter("pArea", a).getSingleResult();
	}

}
