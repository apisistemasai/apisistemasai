package com.agrosolutions.sai.dao.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.collections.map.HashedMap;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.primefaces.model.SortOrder;
import org.springframework.stereotype.Repository;

import com.agrosolutions.sai.dao.FabricanteDAO;
import com.agrosolutions.sai.model.Fabricante;
import com.agrosolutions.sai.model.TipoFabricante;

@Repository
public class FabricanteDAOImpl extends DAOImpl<Fabricante> implements FabricanteDAO {

	private static final long serialVersionUID = -1548935252850544692L;
	static Logger logger = Logger.getLogger(FabricanteDAOImpl.class);

	@Override
	public List<Fabricante> pesquisar(TipoFabricante tipo) {
		@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
		parametros.put("pTipo", tipo);
		parametros.put("pTipo2", TipoFabricante.Todos);

		return executarQuery("Fabricante.pesquisar", parametros, null, null);
	}

    @Override
    public void excluir(Fabricante fabricante) {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
    	parametros.put("pId",fabricante.getId());
        executarComando("Fabricante.excluir", parametros);
    }	

    @Override
	public Criteria criarCriteriaExample(Fabricante entidade, MatchMode match) {

		Criteria criteria = super.criarCriteriaExample(entidade, match);
				
		criteria = prepararCriteria(entidade, criteria);		
		return criteria;
	}

	private Criteria prepararCriteria(Fabricante entidadeLocal, Criteria criteria) {
		if (entidadeLocal.getSortField().equals("nome")) {
			if (entidadeLocal.getSortOrder().equals(SortOrder.DESCENDING)) {
				criteria.addOrder(Order.desc("nome"));
			}else{
				criteria.addOrder(Order.asc("nome"));
			}
		}
		return criteria;
		
	}
}
