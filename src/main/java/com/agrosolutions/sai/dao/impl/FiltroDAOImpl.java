package com.agrosolutions.sai.dao.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.collections.map.HashedMap;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.agrosolutions.sai.dao.FiltroDAO;
import com.agrosolutions.sai.model.Fabricante;
import com.agrosolutions.sai.model.Filtro;
import com.agrosolutions.sai.model.Irrigante;

@Repository
public class FiltroDAOImpl extends DAOImpl<Filtro> implements FiltroDAO {


	private static final long serialVersionUID = 4660974085575379198L;
	static Logger logger = Logger.getLogger(FiltroDAOImpl.class);

	@Override
	public List<Filtro> pesquisar(Fabricante fabricante) {
		@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
		parametros.put("pFabricante", fabricante);

		return executarQuery("Filtro.pesquisar", parametros, null, null);
	}

	@Override
	public List<Filtro> pesquisarPorIrrigante(Irrigante irrigante) {
		@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
		parametros.put("pIrrigante", irrigante);

		return executarQuery("Filtro.pesquisarPorIrrigante", parametros, null, null);
	}

	@Override
    public void excluir(Filtro filtro) {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
    	parametros.put("pId",filtro.getId());
        executarComando("Filtro.excluir", parametros);
    }	

    @Override
    public List<Filtro> obterTodos() {
    	return executarQuery("Filtro.todos", null, null, null);
    }	

    @Override
	public Criteria criarCriteriaExample(Filtro entidade, MatchMode match) {

		Criteria criteria = super.criarCriteriaExample(entidade, match);
				
		criteria = prepararCriteria(entidade, criteria);		
		return criteria;
	}

	private Criteria prepararCriteria(Filtro entidadeLocal, Criteria criteria) {
		Criteria a = null;
		if (entidadeLocal.getFabricante() != null) {
			a = criteria.createCriteria("fabricante");
			a.add(Restrictions.eq("id", entidadeLocal.getFabricante().getId()));
		}
		return criteria;
	}
}
