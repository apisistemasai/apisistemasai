package com.agrosolutions.sai.dao.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.collections.map.HashedMap;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.agrosolutions.sai.dao.FornecedorDAO;
import com.agrosolutions.sai.model.Fornecedor;

@Repository
public class FornecedorDAOImpl extends DAOImpl<Fornecedor> implements FornecedorDAO {
	
	private static final long serialVersionUID = -7666411754607654416L;
	static Logger logger = Logger.getLogger(FornecedorDAOImpl.class);
	
    @Override
    public void excluir(Fornecedor c) {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
    	parametros.put("pId",c.getId());
        executarComando("Fornecedor.excluir", parametros);
    }	

    @Override
    public List<Fornecedor> obterTodos() {
    	return executarQuery("Fornecedor.todos", null, null, null);
    }		
}
