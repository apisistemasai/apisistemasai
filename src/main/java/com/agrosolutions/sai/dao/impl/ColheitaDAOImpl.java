package com.agrosolutions.sai.dao.impl;

import java.util.Map;

import org.apache.commons.collections.map.HashedMap;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.agrosolutions.sai.dao.ColheitaDAO;
import com.agrosolutions.sai.model.Colheita;

@Repository
public class ColheitaDAOImpl extends DAOImpl<Colheita> implements ColheitaDAO {

	private static final long serialVersionUID = -2718281141342022976L;
	static Logger logger = Logger.getLogger(ColheitaDAOImpl.class);
	
    @Override
    public void excluir(Colheita colheita) {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
    	parametros.put("pId",colheita.getId());
        executarComando("Colheita.excluir", parametros);
    }	

}
