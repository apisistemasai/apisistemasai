package com.agrosolutions.sai.dao;

import java.util.List;

import com.agrosolutions.sai.model.Coordenada;
import com.agrosolutions.sai.model.Irrigante;

public interface CoordenadaDAO extends DAO<Coordenada>{

	List<Coordenada> pesquisarPorIrrigante(Irrigante irrigante);
	
}
