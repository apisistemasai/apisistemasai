package com.agrosolutions.sai.dao;

import java.util.Date;
import java.util.List;

import com.agrosolutions.sai.model.AtividadeEconomica;
import com.agrosolutions.sai.model.BaciaHidrografica;
import com.agrosolutions.sai.model.GraficoBarra;
import com.agrosolutions.sai.model.GraficoPizzaIndicadores;
import com.agrosolutions.sai.model.Indicadores;
import com.agrosolutions.sai.model.Municipio;

public interface IndicadoresDAO extends DAO<Indicadores> {

	Indicadores salvar(Indicadores indicadores);

	List<GraficoBarra> culturasHaPorMunicipio(Municipio m);

	List<GraficoBarra> graficoIndicadoresSegurancaKgM3(Municipio m);

	List<GraficoBarra> graficoIndicadoresEconomicoReceitaLiquidaHa(
			Municipio municipio);

	List<GraficoBarra> graficoIndicadoresEconomicoReceitaLiquidaM3(
			Municipio municipio);

	List<GraficoBarra> graficoIndicadoresSegSocialEmpregosHa(Municipio municipio);

	List<GraficoBarra> graficoIndicadoresSegSocialEmpregosM3(Municipio municipio);

	List<GraficoBarra> graficoIndicadoresSegHidricaM3Ha(Municipio municipio);

	List<GraficoBarra> graficoIndicadoresSegHidricaLitrosSegundoHa(Municipio municipio);

	List<GraficoBarra> graficoIndicadoresSegurancaKgM3Bacia(BaciaHidrografica bacia);

	Indicadores pesquisarPorAtividadeEconomica(AtividadeEconomica atividadeEconomica, Date data);

	List<Indicadores> pesquisarPorMunicipio(Municipio municipio, Date data);

	List<Indicadores> pesquisarPorBacia(BaciaHidrografica bacia, Date data);

	List<Date> pesquisarDatasPorMunicipio(Municipio municipio);

	List<Date> pesquisarDatasPorBacia(BaciaHidrografica bacia);

	List<GraficoBarra> graficoIndicadoresSegurancaKgHaBacia(BaciaHidrografica bacia);

	List<GraficoBarra> graficoIndicadoresSegurancaEconomicaReceitaHaBacia(BaciaHidrografica bacia);

	List<GraficoBarra> graficoIndicadoresSegurancaEconomicaReceitaM3Bacia(BaciaHidrografica bacia);

	List<GraficoBarra> graficoIndicadoresSegurancaSocialEmpregosHaBacia(BaciaHidrografica bacia);

	List<GraficoBarra> graficoIndicadoresSegurancaSocialEmpregosM3Bacia(BaciaHidrografica bacia);

	List<GraficoBarra> graficoIndicadoresSegurancaHidricaM3HaBacia(BaciaHidrografica bacia);

	List<GraficoBarra> graficoIndicadoresSegurancaHidricaLitrosSegundoHaBacia(BaciaHidrografica bacia);

	List<GraficoBarra> graficoIndicadoresCorteHidrico(Municipio municipio);

	List<GraficoBarra> graficoCorteHidricoBacia(BaciaHidrografica bacia);

	List<GraficoBarra> graficoIndicadoresSegurancaEconomicaVBPBacia(BaciaHidrografica bacia);

	List<GraficoBarra> graficoIndicadoresEconomicoVBP(Municipio m);

	List<GraficoBarra> graficoIndicadoresEconomicoReceitaLiquida(Municipio m);

	List<GraficoBarra> graficoIndicadoresSegurancaEconomicaReceitaBacia(BaciaHidrografica bacia);

	List<GraficoPizzaIndicadores> graficoIndicadoresCicloCultura(Municipio m);

	List<Date> pesquisarDatasPorAtividade(AtividadeEconomica atividade);

	List<Date> pesquisarDatas();

	List<Indicadores> pesquisarPorData(Date data);

}