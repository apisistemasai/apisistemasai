package com.agrosolutions.sai.dao;

import java.util.Date;
import java.util.List;

import com.agrosolutions.sai.model.Setor;
import com.agrosolutions.sai.model.TesteEficiencia;

public interface TesteEficienciaDAO extends DAO<TesteEficiencia> {

	void excluir(TesteEficiencia t);
	List<TesteEficiencia> pesquisarPorSetor(Setor setor);
	TesteEficiencia pesquisarPorSetorData(Setor setor, Date data);
}
