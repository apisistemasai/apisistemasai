package com.agrosolutions.sai.dao;

import com.agrosolutions.sai.model.Cliente;

public interface ClienteDAO extends DAO<Cliente> {

	void excluir(Cliente c); 
}
