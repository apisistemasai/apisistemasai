package com.agrosolutions.controller;

import com.agrosolutions.sai.dao.UsuarioDAO;
import com.agrosolutions.sai.model.Usuario;
import com.agrosolutions.sai.service.ServiceUser;
import com.agrosolutions.sai.util.ResponseJson;
import com.google.gson.Gson;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
public class Api {

    //esse trecho sai
    ResponseJson json;
    Gson gson = new Gson();
    UsuarioDAO dao;
    // fim
    
    ServiceUser serviceUser;
  
    @RequestMapping(value = "", method = RequestMethod.GET)
    public @ResponseBody
    String api() {
        this.json = new ResponseJson(true, "Teste", "alterado com sucesso");
        return gson.toJson(json);
    }

    @RequestMapping(value = "/teste", method = RequestMethod.GET)
    public @ResponseBody String teste() {
        this.json = new ResponseJson(false, "Teste", "barra teste ");
        return gson.toJson(json);
    }

    @RequestMapping(value = "/saveUser", method = RequestMethod.POST)
    public @ResponseBody String saveUser(Usuario user) {
        this.serviceUser = new ServiceUser();
        return this.serviceUser.merge(user);
    }
    
    @RequestMapping(value = "/getUsers", method = RequestMethod.GET)
    public @ResponseBody String getusers() {
        this.serviceUser = new ServiceUser();
        return this.serviceUser.getUsers();
    }
}
