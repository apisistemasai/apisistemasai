package com.agrosolutions.sai.service;

import java.util.List;

import com.agrosolutions.sai.model.Atividade;

public interface AtividadeService {
	
	Atividade salvar(Atividade atividade);
	List<Atividade> obterTodos();
	void excluir(Atividade atividade);
	List<Atividade> pesquisar(Atividade entidade, Integer first, Integer pageSize);
	
}
