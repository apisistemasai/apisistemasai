package com.agrosolutions.sai.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.agrosolutions.sai.dao.VariedadeDAO;
import com.agrosolutions.sai.exception.SaiException;
import com.agrosolutions.sai.model.Cultura;
import com.agrosolutions.sai.model.Variedade;
import com.agrosolutions.sai.service.VariedadeService;

@Service("variedadeService")
public class VariedadeServiceImpl implements VariedadeService {

	@Autowired
	private VariedadeDAO variedadeDAO;

	@Transactional
	public Variedade salvar(Variedade variedade) {
		return variedadeDAO.salvar(variedade);
	}

	public List<Variedade> obterTodos() {
		return variedadeDAO.obterTodos();
	}

	public List<Variedade> pesquisar(Cultura cultura) {
		return variedadeDAO.pesquisar(cultura);
	}
	
	@Transactional
	public void excluir(Variedade variedade) {
		variedadeDAO.excluir(variedade);
	}
	
	@Transactional
	@Override
	public List<Variedade> pesquisar(Variedade entidade, Integer first, Integer pageSize){
		List<Variedade> variedades = null;
		try {
		} catch (Exception e) {
			throw new SaiException(e.getMessage());
		}
		variedades = variedadeDAO.obterPorParametro(entidade, first, pageSize);	
		
		return variedades;
	}

	@Transactional
	public int quantidade(Variedade entidade) {
		int resultado;
		
		try {
			validarPesquisa(entidade);
		} catch (SaiException e) {
			throw new SaiException(e.getMessages());
		}
		entidade.setSortField("");
		resultado = variedadeDAO.quantidade(entidade);
			
		return resultado;
	}
	private void validarPesquisa(Variedade entidade) throws SaiException {
		List<String> messages = new ArrayList<String>();
		limparFiltros(entidade);

		if (!messages.isEmpty()) {
			throw new SaiException(messages);
		}
	}
	public void limparFiltros(Variedade entidade){
		if (entidade.getNome() !=null && entidade.getNome().equals("")) {
			entidade.setNome(null);
		}
	}
}
