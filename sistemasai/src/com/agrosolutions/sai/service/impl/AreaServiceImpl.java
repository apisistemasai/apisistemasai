package com.agrosolutions.sai.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.agrosolutions.sai.dao.AreaDAO;
import com.agrosolutions.sai.exception.SaiException;
import com.agrosolutions.sai.model.Area;
import com.agrosolutions.sai.model.Distrito;
import com.agrosolutions.sai.service.AreaService;

@Service("areaService")
public class AreaServiceImpl implements AreaService {

	@Autowired
	private AreaDAO areaDAO;

	@Transactional
	public Area salvar(Area area) {
		return areaDAO.salvar(area);
	}

	public List<Area> pesquisar(List<Distrito> distrito) {
		return areaDAO.pesquisar(distrito);
	}

	@Transactional
	@Override
	public void excluir(Area area) {
		areaDAO.excluir(area);
	}
	
	@Transactional
	@Override
	public List<Area> pesquisarArea(Area entidade, Integer first,
			Integer pageSize) {
		List<Area> areas = null;
		try {
		} catch (Exception e) {
			throw new SaiException(e.getMessage());
		}
		areas = areaDAO.obterPorParametro(entidade, first, pageSize);	
		
		return areas;
	}	

}
