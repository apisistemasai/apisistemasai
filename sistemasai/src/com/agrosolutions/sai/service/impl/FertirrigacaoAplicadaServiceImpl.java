package com.agrosolutions.sai.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.agrosolutions.sai.dao.FertirrigacaoAplicadaDAO;
import com.agrosolutions.sai.exception.SaiException;
import com.agrosolutions.sai.model.FertirrigacaoAplicada;
import com.agrosolutions.sai.service.FertirrigacaoAplicadaService;

@Service("fertirrigacaoAplicadaService")
public class FertirrigacaoAplicadaServiceImpl implements FertirrigacaoAplicadaService {

	@Autowired
	private FertirrigacaoAplicadaDAO fertirrigacaoAplicadaDAO;

	@Transactional
	public FertirrigacaoAplicada salvar(FertirrigacaoAplicada fertirrigacaoAplicada) {
		return fertirrigacaoAplicadaDAO.salvar(fertirrigacaoAplicada);
	}

	public List<FertirrigacaoAplicada> obterTodos() {
		return fertirrigacaoAplicadaDAO.obterTodos();
	}

	@Transactional
	@Override
	public void excluir(FertirrigacaoAplicada fertirrigacaoAplicada) {
		fertirrigacaoAplicadaDAO.excluir(fertirrigacaoAplicada);
	}
	
	@Transactional
	@Override
	public List<FertirrigacaoAplicada> pesquisar(FertirrigacaoAplicada entidade, Integer first, Integer pageSize) {
		List<FertirrigacaoAplicada> fertirrigacaoAplicadas = null;
		try {
		} catch (Exception e) {
			throw new SaiException(e.getMessage());
		}
		fertirrigacaoAplicadas = fertirrigacaoAplicadaDAO.obterPorParametro(entidade, first, pageSize);	
		for (FertirrigacaoAplicada fertirrigacaoAplicada : fertirrigacaoAplicadas) {
			String descricao = fertirrigacaoAplicada.getPlantio().getDescricao();
		}
		
		return fertirrigacaoAplicadas;
	}
}
