package com.agrosolutions.sai.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.agrosolutions.sai.dao.MensagemDAO;
import com.agrosolutions.sai.exception.SaiException;
import com.agrosolutions.sai.model.Mensagem;
import com.agrosolutions.sai.service.MensagemService;

@Service("mensagemService")
public class MensagemServiceImpl implements MensagemService {

	@Autowired
	private MensagemDAO mensagemDAO;

	@Transactional
	public Mensagem salvar(Mensagem msg) {
		Mensagem m = mensagemDAO.salvar(msg);
		return m;
	}
	
	@Transactional
	@Override
	public int quantidade(Mensagem entidade) {
		int resultado;
		try {
			validarPesquisa(entidade);
		} catch (SaiException e) {
			throw new SaiException(e.getMessages());
		}
		entidade.setSortField("");
		resultado = mensagemDAO.quantidade(entidade);
		return resultado;
	}

	@Transactional
	@Override
	public List<Mensagem> pesquisar(Mensagem entidade, Integer first, Integer pageSize) {
		List<Mensagem> mensagens = null;
		try {
			validarPesquisa(entidade);
		} catch (SaiException e) {
			throw new SaiException(e.getMessages());
		}
		try {
			mensagens = mensagemDAO.obterPorParametro(entidade, first, pageSize);
			for (Mensagem mensagem : mensagens) {
				mensagem.getDestinatarios().isEmpty();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mensagens;
	}
	
	private void validarPesquisa(Mensagem entidade) throws SaiException {
		List<String> messages = new ArrayList<String>();
		limparFiltros(entidade);
		if (entidade.getIrrigante() != null) {
			entidade.setDataEnvio(null); //erro aqui!
		}
		
		if (!messages.isEmpty()) {
			throw new SaiException(messages);
		}
	}

	// Limpando atributos vazio para nulos para n�o entrar na clausula where
	public void limparFiltros(Mensagem entidade){
		if (entidade.getAssunto()!=null && entidade.getAssunto().isEmpty() ) {
			entidade.setAssunto(null);
		}
	}

}
