package com.agrosolutions.sai.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.agrosolutions.sai.dao.DefensivoDAO;
import com.agrosolutions.sai.exception.SaiException;
import com.agrosolutions.sai.model.Defensivo;
import com.agrosolutions.sai.service.DefensivoService;

@Service("defensivoService")
public class DefensivoServiceImpl implements DefensivoService {

	@Autowired
	private DefensivoDAO defensivoDAO;

	@Transactional
	public Defensivo salvar(Defensivo defensivo) {
		return defensivoDAO.salvar(defensivo);
	}

	public List<Defensivo> obterTodos() {
		return defensivoDAO.obterTodos();
	}

	@Transactional
	@Override
	public void excluir(Defensivo defensivo) {
		defensivoDAO.excluir(defensivo);
	}

	@Transactional
	@Override
	public List<Defensivo> pesquisar(Defensivo entidade, Integer first,Integer pageSize) {
		List<Defensivo> defencivos = null;
		try {
		} catch (Exception e) {
			throw new SaiException(e.getMessage());
		}
		defencivos = defensivoDAO.obterPorParametro(entidade, first, pageSize);	
		
		return defencivos;
	}
}
