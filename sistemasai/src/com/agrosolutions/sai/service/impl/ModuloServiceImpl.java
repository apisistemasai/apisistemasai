package com.agrosolutions.sai.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.agrosolutions.sai.dao.ModuloDAO;
import com.agrosolutions.sai.model.Modulo;
import com.agrosolutions.sai.service.ModuloService;

@Service("moduloService")
public class ModuloServiceImpl implements ModuloService {

	@Autowired
	private ModuloDAO moduloDAO;

	@Transactional
	public Modulo salvar(Modulo funcionalidade) {
		return moduloDAO.salvar(funcionalidade);
	}

	public List<Modulo> obterTodos() {
		return moduloDAO.obterTodos();
	}
}
