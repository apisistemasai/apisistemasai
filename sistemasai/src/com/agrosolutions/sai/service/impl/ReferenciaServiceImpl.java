package com.agrosolutions.sai.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.agrosolutions.sai.dao.ReferenciaDAO;
import com.agrosolutions.sai.model.Cultura;
import com.agrosolutions.sai.model.Referencia;
import com.agrosolutions.sai.model.Variedade;
import com.agrosolutions.sai.service.ReferenciaService;

@Service("referenciaService")
public class ReferenciaServiceImpl implements ReferenciaService {

	@Autowired
	private ReferenciaDAO referenciaDAO;

	@Transactional
	public Referencia salvar(Referencia referencia) {
		return referenciaDAO.salvar(referencia);
	}
	
	public List<Referencia> obterTodos() {
		return referenciaDAO.obterTodos();
	}

	public List<Referencia> pesquisar(Variedade variedade) {
		return referenciaDAO.pesquisar(variedade);
	}
	@Override
	public List<Referencia> pesquisarCultura(Cultura c) {
		return referenciaDAO.pesquisarCultura(c);
	}

	@Transactional
	public void excluir(Referencia referencia) {
		referenciaDAO.excluir(referencia);
	}

}
