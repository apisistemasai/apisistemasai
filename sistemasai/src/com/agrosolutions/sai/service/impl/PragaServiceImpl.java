package com.agrosolutions.sai.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.agrosolutions.sai.dao.PragaDAO;
import com.agrosolutions.sai.exception.SaiException;
import com.agrosolutions.sai.model.Praga;
import com.agrosolutions.sai.service.PragaService;

@Service("pragaService")
public class PragaServiceImpl implements PragaService {

	@Autowired
	private PragaDAO pragaDAO;

	@Transactional
	public Praga salvar(Praga praga) {
		return pragaDAO.salvar(praga);
	}

	public List<Praga> obterTodos() {
		return pragaDAO.obterTodos();
	}

	@Transactional
	@Override
	public void excluir(Praga praga) {
		pragaDAO.excluir(praga);
	}
	
	@Transactional
	@Override
	public List<Praga> pesquisar(Praga entidade, Integer first, Integer pageSize) {
		List<Praga> pragas = null;
		try {
		} catch (Exception e) {
			throw new SaiException(e.getMessage());
		}
		pragas = pragaDAO.obterPorParametro(entidade, first, pageSize);	
		
		return pragas;
	}
}
