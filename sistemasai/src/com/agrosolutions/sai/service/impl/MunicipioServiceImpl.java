package com.agrosolutions.sai.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.agrosolutions.sai.dao.MunicipioDAO;
import com.agrosolutions.sai.exception.SaiException;
import com.agrosolutions.sai.model.BaciaHidrografica;
import com.agrosolutions.sai.model.Municipio;
import com.agrosolutions.sai.service.MunicipioService;
import com.agrosolutions.sai.util.ResourceBundle;

@Service("municipioService")
public class MunicipioServiceImpl implements MunicipioService {

	@Autowired
	private MunicipioDAO municipioDAO;

	@Transactional
	public Municipio salvar(Municipio municipio) {
		return municipioDAO.salvar(municipio);
	}

	@Transactional
	public int quantidade(Municipio entidade) {
		int resultado;
		try {
			validarPesquisa(entidade);
		} catch (SaiException e) {
			throw new SaiException(e.getMessages());
		}
		entidade.setSortField("");
		resultado = municipioDAO.quantidade(entidade);					
		
		return resultado;
	}

	@Transactional
	public List<Municipio> pesquisar(Municipio entidade, Integer first, Integer pageSize) {
		List<Municipio> municipios = new ArrayList<Municipio>();
		try {
			validarPesquisa(entidade);
		} catch (SaiException e) {
			throw new SaiException(e.getMessages());
		}
		municipios = municipioDAO.obterPorParametro(entidade, first, pageSize);					
		return municipios;
	}
	
	private void validarPesquisa(Municipio entidade) throws SaiException {
		List<String> messages = new ArrayList<String>();
		limparFiltros(entidade);

		if (!messages.isEmpty()) {
			throw new SaiException(messages);
		}
	}

	// Limpando atributos vazio para nulos para n�o entrar na clausula where
	public void limparFiltros(Municipio entidade){
		if (entidade.getNome() !=null && entidade.getNome().equals("")) {
			entidade.setNome(null);
		}
		if (entidade.getLocalizacao() !=null && entidade.getLocalizacao().equals("")) {
			entidade.setLocalizacao(null);
		}
	}

	@Transactional
	@Override
	public void excluir(Municipio municipio) {
		municipioDAO.excluir(municipio);
	}	

	@Override
	public List<Municipio> pesquisarPorBacia(List<BaciaHidrografica> bacias) {
		List<Municipio> municipios = null;
		try {
			municipios = municipioDAO.pesquisarPorBacia(bacias);
		} catch (EmptyResultDataAccessException e) {
			throw new SaiException(ResourceBundle.getMessage("erro.municipio.pesquisa"));
		}
		return municipios;
	}
	
	@Override
	public List<Municipio> pesquisarPorBacia(List<BaciaHidrografica> bacias, Integer indiceInicio, Integer quantidade) {
		List<Municipio> municipios = null;
		try {
			municipios = municipioDAO.pesquisarPorBacia(bacias, indiceInicio, quantidade);
		} catch (EmptyResultDataAccessException e) {
			throw new SaiException(ResourceBundle.getMessage("erro.municipio.pesquisa"));
		}
		return municipios;
	}
}
