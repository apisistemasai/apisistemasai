package com.agrosolutions.sai.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.agrosolutions.sai.dao.AduboDAO;
import com.agrosolutions.sai.exception.SaiException;
import com.agrosolutions.sai.model.Adubo;
import com.agrosolutions.sai.service.AduboService;

@Service("aduboService")
public class AduboServiceImpl implements AduboService {

	@Autowired
	private AduboDAO aduboDAO;

	@Transactional
	public Adubo salvar(Adubo adubo) {
		return aduboDAO.salvar(adubo);
	}

	public List<Adubo> obterTodos() {
		return aduboDAO.obterTodos();
	}

	@Transactional
	@Override
	public void excluir(Adubo adubo) {
		aduboDAO.excluir(adubo);
	}

	@Transactional
	@Override
	public List<Adubo> pesquisar(Adubo entidade, Integer first, Integer pageSize) {
		List<Adubo> adubos = null;
		try {
		} catch (Exception e) {
			throw new SaiException(e.getMessage());
		}
		adubos = aduboDAO.obterPorParametro(entidade, first, pageSize);	
		
		return adubos;
	}
}
