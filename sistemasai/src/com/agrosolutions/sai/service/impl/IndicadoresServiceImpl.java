package com.agrosolutions.sai.service.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.agrosolutions.sai.dao.AtividadeEconomicaDAO;
import com.agrosolutions.sai.dao.CulturaDAO;
import com.agrosolutions.sai.dao.IndicadoresDAO;
import com.agrosolutions.sai.dao.MunicipioDAO;
import com.agrosolutions.sai.dao.SetorDAO;
import com.agrosolutions.sai.exception.SaiException;
import com.agrosolutions.sai.model.AtividadeEconomica;
import com.agrosolutions.sai.model.BaciaHidrografica;
import com.agrosolutions.sai.model.GraficoBarra;
import com.agrosolutions.sai.model.GraficoPizzaIndicadores;
import com.agrosolutions.sai.model.Indicadores;
import com.agrosolutions.sai.model.Municipio;
import com.agrosolutions.sai.service.IndicadoresService;
import com.agrosolutions.sai.util.BigDecimalUtil;

@Service("indicadoresService")
public class IndicadoresServiceImpl implements IndicadoresService {

	@Autowired
	private MunicipioDAO municipioDAO;
	@Autowired
	private CulturaDAO culturaDAO;
	@Autowired
	private AtividadeEconomicaDAO atividadeEconomicaDAO;
	@Autowired
	private IndicadoresDAO indicadoresDAO;
	@Autowired
	private SetorDAO setorDAO;
	
	@Override
	public Long totalMunicipiosPorBacia(List<BaciaHidrografica> baciasPesquisa) {
		try {
			return municipioDAO.totalMunicipioPorBacia(baciasPesquisa);
		} catch (SaiException e) {
			throw new SaiException(e.getMessages());
		}
	}

	@Override
	public List<GraficoPizzaIndicadores> totalHaPorCulturaPorMunicipio(
			Municipio municipio) {
		try {
			return culturaDAO.totalHaPorCulturaPorMunicipio(municipio);
		} catch (SaiException e) {
			throw new SaiException(e.getMessages());
		}
	}

	@Override
	public List<GraficoPizzaIndicadores> totalHaPorCulturaPorBacia(
			BaciaHidrografica bacia) {
		try {
			return culturaDAO.totalHaPorCulturaPorBacia(bacia);
		} catch (SaiException e) {
			throw new SaiException(e.getMessages());
		}
	}
	
	@Override
	public List<GraficoPizzaIndicadores> totalCicloCulturaPorBacia(
			BaciaHidrografica bacia) {
		try {
			return culturaDAO.totalCicloCulturaPorBacia(bacia);
		} catch (SaiException e) {
			throw new SaiException(e.getMessages());
		}
	}

	@Transactional
	@Override
	public Indicadores salvar(Indicadores indicadores) {
		try {
			return indicadoresDAO.salvar(indicadores);
		} catch (SaiException e) {
			throw new SaiException(e.getMessages());
		}
	}
	
	@Override
	public Indicadores pesquisarPorAtividadeEconomica(AtividadeEconomica atividadeEconomica, Date data) {
		try {
			return indicadoresDAO.pesquisarPorAtividadeEconomica(atividadeEconomica, data);
		} catch (SaiException e) {
			throw new SaiException(e.getMessages());
		}
	}

	@Override
	public List<Indicadores> pesquisarPorData(Date data) {
		try {
			return indicadoresDAO.pesquisarPorData(data);
		} catch (SaiException e) {
			throw new SaiException(e.getMessages());
		}
	}

	@Override
	public List<Indicadores> pesquisarPorMunicipio(Municipio municipio, Date data) {
		try {
			return indicadoresDAO.pesquisarPorMunicipio(municipio, data);
		} catch (SaiException e) {
			throw new SaiException(e.getMessages());
		}
	}

	@Override
	public List<Indicadores> pesquisarPorBacia(BaciaHidrografica bacia, Date data) {
		try {
			return indicadoresDAO.pesquisarPorBacia(bacia, data);
		} catch (SaiException e) {
			throw new SaiException(e.getMessages());
		}
	}

	@Override
	public List<Date> pesquisarDatasPorAtividade(AtividadeEconomica atividade) {
		try {
			return indicadoresDAO.pesquisarDatasPorAtividade(atividade);
		} catch (SaiException e) {
			throw new SaiException(e.getMessages());
		}
	}

	@Override
	public List<Date> pesquisarDatasPorMunicipio(Municipio municipio) {
		try {
			return indicadoresDAO.pesquisarDatasPorMunicipio(municipio);
		} catch (SaiException e) {
			throw new SaiException(e.getMessages());
		}
	}

	@Override
	public List<Date> pesquisarDatasPorBacia(BaciaHidrografica bacia) {
		try {
			return indicadoresDAO.pesquisarDatasPorBacia(bacia);
		} catch (SaiException e) {
			throw new SaiException(e.getMessages());
		}
	}

	@Override
	public List<Date> pesquisarDatas() {
		try {
			return indicadoresDAO.pesquisarDatas();
		} catch (SaiException e) {
			throw new SaiException(e.getMessages());
		}
	}

	@Override
	public List<GraficoBarra> culturasHaPorMunicipio(Municipio municipio) {
		try {
			return indicadoresDAO.culturasHaPorMunicipio(municipio);
		} catch (SaiException e) {
			throw new SaiException(e.getMessages());
		}
	}
	
	@Override
	public List<GraficoBarra> graficoIndicadoresSegurancaKgM3(Municipio municipio) {
		try {
			return indicadoresDAO.graficoIndicadoresSegurancaKgM3(municipio);
		} catch (SaiException e) {
			throw new SaiException(e.getMessages());
		}
	}

	@Override
	public List<GraficoBarra> graficoIndicadoresEconomicoReceitaLiquida(Municipio municipio) {
		try {
			return indicadoresDAO.graficoIndicadoresEconomicoReceitaLiquida(municipio);
		} catch (SaiException e) {
			throw new SaiException(e.getMessages());
		}
	}

	@Override
	public List<GraficoBarra> graficoIndicadoresEconomicoReceitaLiquidaHa(Municipio municipio) {
		try {
			return indicadoresDAO.graficoIndicadoresEconomicoReceitaLiquidaHa(municipio);
		} catch (SaiException e) {
			throw new SaiException(e.getMessages());
		}
	}

	@Override
	public List<GraficoBarra> graficoIndicadoresEconomicoReceitaLiquidaM3(Municipio municipio) {
		try {
			return indicadoresDAO.graficoIndicadoresEconomicoReceitaLiquidaM3(municipio);
		} catch (SaiException e) {
			throw new SaiException(e.getMessages());
		}
	}
	
	@Override
	public List<GraficoBarra> graficoIndicadoresEconomicoVBP(Municipio municipio) {
		try {
			return indicadoresDAO.graficoIndicadoresEconomicoVBP(municipio);
		} catch (SaiException e) {
			throw new SaiException(e.getMessages());
		}
	}

	@Override
	public List<GraficoBarra> graficoIndicadoresSegSocialEmpregosHa(Municipio municipio) {
		try {
			return indicadoresDAO.graficoIndicadoresSegSocialEmpregosHa(municipio);
		} catch (SaiException e) {
			throw new SaiException(e.getMessages());
		}
	}
	
	@Override
	public List<GraficoBarra> graficoIndicadoresSegSocialEmpregosM3(Municipio municipio) {
		try {
			return indicadoresDAO.graficoIndicadoresSegSocialEmpregosM3(municipio);
		} catch (SaiException e) {
			throw new SaiException(e.getMessages());
		}
	}

	@Override
	public List<GraficoBarra> graficoIndicadoresSegHidricaM3Ha(Municipio municipio) {
		try {
			return indicadoresDAO.graficoIndicadoresSegHidricaM3Ha(municipio);
		} catch (SaiException e) {
			throw new SaiException(e.getMessages());
		}
	}

	@Override
	public List<GraficoBarra> graficoIndicadoresSegHidricaLitrosSegundoHa(Municipio municipio) {
		try {
			return indicadoresDAO.graficoIndicadoresSegHidricaLitrosSegundoHa(municipio);
		} catch (SaiException e) {
			throw new SaiException(e.getMessages());
		}
	}
	
	@Override
	public List<GraficoPizzaIndicadores> graficoIndicadoresCicloCultura(Municipio municipio) {
		try {
			return indicadoresDAO.graficoIndicadoresCicloCultura(municipio);
		} catch (SaiException e) {
			throw new SaiException(e.getMessages());
		}
	}

	@Override
	public List<GraficoBarra> graficoIndicadoresCorteHidrico(Municipio municipio) {
		try {
			return indicadoresDAO.graficoIndicadoresCorteHidrico(municipio);
		} catch (SaiException e) {
			throw new SaiException(e.getMessages());
		}
	}

	@Override
	public List<GraficoBarra> graficoIndicadoresSegurancaKgM3Bacia(BaciaHidrografica bacia) {
		try {
			return indicadoresDAO.graficoIndicadoresSegurancaKgM3Bacia(bacia);
		} catch (SaiException e) {
			throw new SaiException(e.getMessages());
		}
	}

	@Override
	public List<GraficoBarra> graficoIndicadoresSegurancaKgHaBacia(BaciaHidrografica bacia) {
		try {
			return indicadoresDAO.graficoIndicadoresSegurancaKgHaBacia(bacia);
		} catch (SaiException e) {
			throw new SaiException(e.getMessages());
		}
	}

	@Override
	public List<GraficoBarra> graficoIndicadoresSegurancaEconomicaReceitaHaBacia(BaciaHidrografica bacia) {
		try {
			return indicadoresDAO.graficoIndicadoresSegurancaEconomicaReceitaHaBacia(bacia);
		} catch (SaiException e) {
			throw new SaiException(e.getMessages());
		}
	}

	@Override
	public List<GraficoBarra> graficoIndicadoresSegurancaEconomicaReceitaBacia(BaciaHidrografica bacia) {
		try {
			return indicadoresDAO.graficoIndicadoresSegurancaEconomicaReceitaBacia(bacia);
		} catch (SaiException e) {
			throw new SaiException(e.getMessages());
		}
	}

	@Override
	public List<GraficoBarra> graficoIndicadoresSegurancaEconomicaReceitaM3Bacia(BaciaHidrografica bacia) {
		try {
			return indicadoresDAO.graficoIndicadoresSegurancaEconomicaReceitaM3Bacia(bacia);
		} catch (SaiException e) {
			throw new SaiException(e.getMessages());
		}
	}

	@Override
	public List<GraficoBarra> graficoIndicadoresSegurancaEconomicaVBPBacia(BaciaHidrografica bacia) {
		try {
			return indicadoresDAO.graficoIndicadoresSegurancaEconomicaVBPBacia(bacia);
		} catch (SaiException e) {
			throw new SaiException(e.getMessages());
		}
	}

	@Override
	public List<GraficoBarra> graficoIndicadoresSegurancaSocialEmpregosHaBacia(BaciaHidrografica bacia) {
		try {
			return indicadoresDAO.graficoIndicadoresSegurancaSocialEmpregosHaBacia(bacia);
		} catch (SaiException e) {
			throw new SaiException(e.getMessages());
		}
	}

	@Override
	public List<GraficoBarra> graficoIndicadoresSegurancaSocialEmpregosM3Bacia(BaciaHidrografica bacia) {
		try {
			return indicadoresDAO.graficoIndicadoresSegurancaSocialEmpregosM3Bacia(bacia);
		} catch (SaiException e) {
			throw new SaiException(e.getMessages());
		}
	}

	@Override
	public List<GraficoBarra> graficoIndicadoresSegurancaHidricaM3HaBacia(BaciaHidrografica bacia) {
		try {
			return indicadoresDAO.graficoIndicadoresSegurancaHidricaM3HaBacia(bacia);
		} catch (SaiException e) {
			throw new SaiException(e.getMessages());
		}
	}

	@Override
	public List<GraficoBarra> graficoIndicadoresSegurancaHidricaLitrosSegundoHaBacia(BaciaHidrografica bacia) {
		try {
			return indicadoresDAO.graficoIndicadoresSegurancaHidricaLitrosSegundoHaBacia(bacia);
		} catch (SaiException e) {
			throw new SaiException(e.getMessages());
		}
	}

	@Override
	public List<GraficoBarra> graficoCorteHidricoBacia(BaciaHidrografica bacia) {
		try {
			return indicadoresDAO.graficoCorteHidricoBacia(bacia);
		} catch (SaiException e) {
			throw new SaiException(e.getMessages());
		}
	}
	
}
