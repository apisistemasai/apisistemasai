package com.agrosolutions.sai.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.agrosolutions.sai.dao.EmissorDAO;
import com.agrosolutions.sai.dao.EstadioDAO;
import com.agrosolutions.sai.dao.PlantioDAO;
import com.agrosolutions.sai.exception.SaiException;
import com.agrosolutions.sai.model.Area;
import com.agrosolutions.sai.model.Cultivo;
import com.agrosolutions.sai.model.Distrito;
import com.agrosolutions.sai.model.Emissor;
import com.agrosolutions.sai.model.Estadio;
import com.agrosolutions.sai.model.FormulaPlantio;
import com.agrosolutions.sai.model.FormulasTI;
import com.agrosolutions.sai.model.GraficoPizza;
import com.agrosolutions.sai.model.Irrigante;
import com.agrosolutions.sai.model.Plantio;
import com.agrosolutions.sai.model.Setor;
import com.agrosolutions.sai.model.TipoCalculoKL;
import com.agrosolutions.sai.model.TipoEmissor;
import com.agrosolutions.sai.model.TipoFormulaGotejo;
import com.agrosolutions.sai.model.TipoFormulaMicro;
import com.agrosolutions.sai.service.PlantioService;
import com.agrosolutions.sai.util.BigDecimalUtil;

@Service("plantioService")
public class PlantioServiceImpl implements PlantioService {

	@Autowired
	private PlantioDAO plantioDAO;
	@Autowired
	private EmissorDAO emissorDAO;
	@Autowired
	private EstadioDAO estadioDAO;


	@Transactional
	public Plantio salvar(Plantio plantio) {
		try {
			if (plantio.getDataFim() == null){
				if (!plantio.getSequeiro()){
					validar(plantio);					
				}
			}
		} catch (SaiException e) {
			throw new SaiException(e.getMessages());
		}
	
		return plantioDAO.salvar(plantio);
	}

	private void validar(Plantio entidade) throws SaiException {
		List<String> messages = new ArrayList<String>();

		if (entidade.getTurnoRega() < 1) {
			messages.add("Turno de rega deve ser 1 ou mais.");
		}
		if (entidade.getFormula().getFormulaTI() == null) {
			messages.add("Plantio n�o tem formula identificada.");
		}

		if (entidade.getFormula().getTipoEmissor() == null) {
			messages.add("Plantio sem emissor cadastrado. Verifique o cadastro do Setor.");
		}

		if (entidade.getFormula().getTipoEmissor() != null) {
			if (entidade.getFormula().getTipoEmissor().equals(TipoEmissor.Gotejamento) && entidade.getTipoFormulaGotejo() == null) {
				messages.add("Plantio com gotejo requer um tipo de formula para gotejo.");
			}
			if (entidade.getFormula().getTipoEmissor().equals(TipoEmissor.Microaspersao) && entidade.getTipoFormulaMicro() == null) {
				messages.add("Plantio com micro aspersor requer um tipo de formula para micro.");
			}
		}
		if (entidade.getTipoFormulaGotejo() != null && entidade.getTipoFormulaGotejo().equals(TipoFormulaGotejo.AreaCoberturaKl) && entidade.getTipoCalculoKL() == null) {
			messages.add("Plantio com gotejo requer um tipo de calculo para o coeficiente de localiza��o (KL).");
		}
		
		if (!messages.isEmpty()) {
			throw new SaiException(messages);
		}
	}

	public List<Plantio> pesquisar(Cultivo cultivo) {
		return plantioDAO.pesquisar(cultivo);
	}

	public List<Plantio> pesquisarPorIrrigante(Irrigante irrigante) {
		return plantioDAO.pesquisarPorIrrigante(irrigante);
	}

	@Override
	public List<Plantio> pesquisarPorSetor(Setor setor) {
		return plantioDAO.pesquisarPorSetor(setor);
	}

	@Override
	public List<GraficoPizza> culturaAtivaPorArea(Area a, Date data) {
		return plantioDAO.culturaAtivaPorArea(a, data);
	}

	@Override
	public List<GraficoPizza> culturaHecPorArea(Area a, Date data) {
		return plantioDAO.culturaHecPorArea(a, data);
	}

	@Override
	public List<GraficoPizza> soloPorArea(Area a) {
		return plantioDAO.soloPorArea(a);
	}

	@Override
	public List<GraficoPizza> atividadePorArea(Area a, Date data) {
		return plantioDAO.atividadePorArea(a, data);
	}

	@Override
	public List<GraficoPizza> irrigantePorArea(Area a) {
		return plantioDAO.irrigantePorArea(a);
	}

	@Override
	public List<GraficoPizza> culturaAtivaPorDistrito(Distrito a, Date data) {
		return plantioDAO.culturaAtivaPorDistrito(a, data);
	}

	@Override
	public List<GraficoPizza> culturaHecPorDistrito(Distrito a, Date data) {
		return plantioDAO.culturaHecPorDistrito(a, data);
	}

	@Override
	public List<GraficoPizza> soloPorDistrito(Distrito a) {
		return plantioDAO.soloPorDistrito(a);
	}

	@Override
	public List<GraficoPizza> atividadePorDistrito(Distrito a, Date data) {
		return plantioDAO.atividadePorDistrito(a, data);
	}

	@Override
	public List<GraficoPizza> irrigantePorDistrito(Distrito a) {
		return plantioDAO.irrigantePorDistrito(a);
	}
	
	public List<GraficoPizza> emissorPorDistrito(Distrito area){
		return plantioDAO.emissorPorDistrito(area);
	}
	
	public List<GraficoPizza> bombaPorDistrito(Distrito area){
		return plantioDAO.bombaPorDistrito(area);
	}

	public List<GraficoPizza> filtroPorDistrito(Distrito area){
		return plantioDAO.filtroPorDistrito(area);
	}
	
	public List<GraficoPizza> valvulaPorDistrito(Distrito area){
		return plantioDAO.valvulaPorDistrito(area);
	}
	
	public List<GraficoPizza> tuboPorDistrito(Distrito area){
		return plantioDAO.tuboPorDistrito(area);
	}
	
	public List<GraficoPizza> emissorPorArea(Area a){
		return plantioDAO.emissorPorArea(a);
	}
	
	public List<GraficoPizza> bombaPorArea(Area a){
		return plantioDAO.bombaPorArea(a);
	}
	
	public List<GraficoPizza> filtroPorArea(Area a){
		return plantioDAO.filtroPorArea(a);
	}
	
	public List<GraficoPizza> valvulaPorArea(Area a){
		return plantioDAO.valvulaPorArea(a);
	}
	
	public List<GraficoPizza> tuboPorArea(Area a){
		return plantioDAO.tuboPorArea(a);
	}

	@Transactional
	public void excluir(Plantio plantio) {
		plantioDAO.excluir(plantio);
	}

	@Transactional
	public Plantio carregarDoenca(Plantio plantio) {
		Plantio p = plantioDAO.obterPorReferencia(plantio.getId());
		p.getDoencas().isEmpty();
		return p;
	}

	public FormulaPlantio verificarFormula(Plantio entidade) throws SaiException {
		BigDecimal diamCopa = new BigDecimal(0);
		FormulaPlantio retorno = new FormulaPlantio();
		TipoEmissor tipoEmissor = null;
		Boolean mudouTipo = false;
		List<Emissor> emissores;
		BigDecimal diametro = new BigDecimal(0);
		entidade.setEmissores(emissorDAO.pesquisarPorPlantio(entidade));
		entidade.getCultivo().getReferencia().setEstadios(estadioDAO.pesquisar(entidade.getCultivo().getReferencia()));

		Estadio estadioAtual = entidade.getCultivo().getEstadioAtual();
		if (estadioAtual != null) {
			diamCopa = entidade.getCultivo().getValorDc();
		}
		
		//Se o plantio n�o tiver emissores exclusivos, usamos os emissores do setor.
		if ( entidade.getEmissores() == null || entidade.getEmissores().isEmpty()) {
			emissores = emissorDAO.pesquisarPorSetor(entidade.getSetor());
		}else{
			emissores = entidade.getEmissores();
		}

		for (Emissor emissor : emissores) {
			retorno.setVazao(retorno.getVazao().add(emissor.getVazao()==null?new BigDecimal(0):emissor.getVazao()));
			if (emissor.getRaio().compareTo(diametro) > 0){
				diametro = emissor.getRaio();
			}
			if (tipoEmissor == null) {
				tipoEmissor = emissor.getTipoEmissor();
			}
			if (!tipoEmissor.equals(emissor.getTipoEmissor())) {
				mudouTipo = true;
				tipoEmissor = emissor.getTipoEmissor();
			}
		}

		if (tipoEmissor == null) {
			return retorno;
		}
		retorno.setDiametro(diametro);
		retorno.setTipoEmissor(tipoEmissor);
		//Se mudou o tipo de emissor � porque precisaremos checar se os tipos de formula est�o definidos.
		if (mudouTipo) {
			if(entidade.getTipoFormulaMicro() == null || entidade.getTipoFormulaMicro().equals(TipoFormulaMicro.HasteNormalPAM)){
				retorno.setFormulaTI(FormulasTI.MicroHasteNormalPAM);
			}
			if(entidade.getTipoFormulaMicro() != null && entidade.getTipoFormulaMicro().equals(TipoFormulaMicro.HasteNormalKL)){
				retorno.setFormulaTI(FormulasTI.MicroHasteNormalKL);
				retorno.setDiametro(diamCopa.equals(new BigDecimal(0))? new BigDecimal(2*entidade.getEspE()).setScale(2, BigDecimal.ROUND_UP) : diamCopa);
			}
			if(entidade.getTipoFormulaMicro() != null && entidade.getTipoFormulaMicro().equals(TipoFormulaMicro.HasteAlongada)){
				retorno.setFormulaTI(FormulasTI.MicroHasteAlongada);
			}			
			if(entidade.getTipoFormulaGotejo() == null || entidade.getTipoFormulaGotejo().equals(TipoFormulaGotejo.AreaCoberturaPam)){
				retorno.setFormulaTI(FormulasTI.GotejoAreaPam);
			}
			if(entidade.getTipoFormulaGotejo() != null && entidade.getTipoFormulaGotejo().equals(TipoFormulaGotejo.AreaCoberturaKl)){
				retorno.setFormulaTI(FormulasTI.GotejoAreaKl);
				retorno.setDiametro(diamCopa.equals(new BigDecimal(0))? new BigDecimal(2*entidade.getEspE()).setScale(2, BigDecimal.ROUND_UP) : diamCopa);
			}
			if(entidade.getTipoFormulaGotejo() != null && entidade.getTipoFormulaGotejo().equals(TipoFormulaGotejo.FaixaPam)){
				retorno.setFormulaTI(FormulasTI.GotejoFaixaPam);
			}
			if(entidade.getTipoFormulaGotejo() != null && entidade.getTipoFormulaGotejo().equals(TipoFormulaGotejo.FaixaKl)){
				retorno.setFormulaTI(FormulasTI.GotejoFaixaKl);
				retorno.setDiametro(diamCopa.equals(new BigDecimal(0))? new BigDecimal(2*entidade.getEspE()).setScale(2, BigDecimal.ROUND_UP) : diamCopa);
			}
		}else{
			if (tipoEmissor.equals(TipoEmissor.Pivo)) {
				retorno.setFormulaTI(FormulasTI.Aspercao);
			}
			if (tipoEmissor.equals(TipoEmissor.Aspersao)) {
				retorno.setFormulaTI(FormulasTI.Aspercao);
			}
			if (tipoEmissor.equals(TipoEmissor.Microaspersao)) {
				if(entidade.getTipoFormulaMicro() == null || entidade.getTipoFormulaMicro().equals(TipoFormulaMicro.HasteNormalPAM)){
					retorno.setFormulaTI(FormulasTI.MicroHasteNormalPAM);
				}
				if(entidade.getTipoFormulaMicro() != null && entidade.getTipoFormulaMicro().equals(TipoFormulaMicro.HasteNormalKL)){
					retorno.setFormulaTI(FormulasTI.MicroHasteNormalKL);
					retorno.setDiametro(diamCopa.equals(new BigDecimal(0))? new BigDecimal(2*entidade.getEspE()).setScale(2, BigDecimal.ROUND_UP) : diamCopa);
				}
				if(entidade.getTipoFormulaMicro() != null && entidade.getTipoFormulaMicro().equals(TipoFormulaMicro.HasteAlongada)){
					retorno.setFormulaTI(FormulasTI.MicroHasteAlongada);
				}
			}
			if (tipoEmissor.equals(TipoEmissor.Gotejamento)) {
				if(entidade.getTipoFormulaGotejo() == null || entidade.getTipoFormulaGotejo().equals(TipoFormulaGotejo.AreaCoberturaPam)){
					retorno.setFormulaTI(FormulasTI.GotejoAreaPam);
				}
				if(entidade.getTipoFormulaGotejo() != null && entidade.getTipoFormulaGotejo().equals(TipoFormulaGotejo.AreaCoberturaKl)){
					retorno.setFormulaTI(FormulasTI.GotejoAreaKl);
					try {
						retorno.setDiametro(diamCopa.equals(new BigDecimal(0))? new BigDecimal(2*entidade.getEspE()).setScale(2, BigDecimal.ROUND_UP) : diamCopa);
					} catch (NullPointerException e) {
						System.out.println("Awqui");
					}
				}
				if(entidade.getTipoFormulaGotejo() != null && entidade.getTipoFormulaGotejo().equals(TipoFormulaGotejo.FaixaPam)){
					retorno.setFormulaTI(FormulasTI.GotejoFaixaPam);
				}
				if(entidade.getTipoFormulaGotejo() != null && entidade.getTipoFormulaGotejo().equals(TipoFormulaGotejo.FaixaKl)){
					retorno.setFormulaTI(FormulasTI.GotejoFaixaKl);
					retorno.setDiametro(diamCopa.equals(new BigDecimal(0))? new BigDecimal(2*entidade.getEspE()).setScale(2, BigDecimal.ROUND_UP) : diamCopa);
				}
			}
		}
		
		return retorno;
	}
	
	public BigDecimal calculoPAM(Plantio plantio) {
		BigDecimal pam = new BigDecimal(0);
		//Calculo do Pam
		BigDecimal gotejoPlanta = (new BigDecimal(plantio.getTotalEmissores())).divide(new BigDecimal(plantio.getTotalPlanta()), BigDecimalUtil.MC_RUP);
		BigDecimal espacaoPlanta = (new BigDecimal(plantio.getEspacamento1())).multiply(new BigDecimal(plantio.getEspacamento2()));
		BigDecimal dw3 = (new BigDecimal(plantio.getEspE())).multiply(new BigDecimal(plantio.getEspE()));
		BigDecimal am = (new BigDecimal(Math.PI)).multiply(dw3);
		BigDecimal z = espacaoPlanta.multiply(new BigDecimal(4));
		BigDecimal pamParcial = am.divide(z, BigDecimalUtil.MC_RUP);
		BigDecimal pamParcial2 = pamParcial.multiply(new BigDecimal(100));
		pam = gotejoPlanta.multiply(pamParcial2);
		//Se PAM for menor que 70% considerar 70%.
		if(pam.compareTo(new BigDecimal(70)) < 0){
			pam = new BigDecimal(70);
		}
		//Se PAM for maior que 100% considerar 100%.
		if(pam.compareTo(new BigDecimal(100)) > 0){
			pam = new BigDecimal(100);
		}
		return pam;
	}
	

	@Override
	public Plantio definirTipoFormula(Plantio plantio) {
		TipoEmissor tipoEmissor = null;
		// Se n�o tem emissor no setor, ou o plantio � sequeiro, n�o define formula
		if ( plantio.getSequeiro() || plantio.getSetor().getEmissores().isEmpty() ) {
			return plantio;
		}
		if ( plantio.getFormula() == null ) {
			return null;
		}
		
		tipoEmissor = plantio.getFormula().getTipoEmissor();
		
		if ( tipoEmissor.equals(TipoEmissor.Gotejamento) ) {
			plantio.setTipoFormulaMicro( null );
			if ( plantio.getTipoFormulaGotejo() == null ) {
				plantio.setTipoFormulaGotejo( TipoFormulaGotejo.FaixaKl );
				plantio.setTipoCalculoKL(TipoCalculoKL.KellerBliesnet);
			}
		}
		
		if ( tipoEmissor.equals(TipoEmissor.Microaspersao) ) {
			plantio.setTipoFormulaGotejo( null );
			if ( plantio.getTipoFormulaMicro() == null ) {
				plantio.setTipoFormulaMicro(TipoFormulaMicro.HasteNormalKL);
				plantio.setTipoCalculoKL(TipoCalculoKL.KellerBliesnet);
			}
		}
		
		return plantio;
	}
}