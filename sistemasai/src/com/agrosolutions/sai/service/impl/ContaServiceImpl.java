package com.agrosolutions.sai.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.agrosolutions.sai.dao.ContaDAO;
import com.agrosolutions.sai.exception.SaiException;
import com.agrosolutions.sai.model.Conta;
import com.agrosolutions.sai.service.ContaService;

@Service("contaService")
public class ContaServiceImpl implements ContaService {

	@Autowired
	private ContaDAO contaDAO;

	@Transactional
	public Conta salvar(Conta c) {
		try {
			validar(c);
		} catch (SaiException e) {
			throw new SaiException(e.getMessages());
		}

		return contaDAO.salvar(c);
	}

	public List<Conta> obterTodos() {
		return contaDAO.obterTodos();
	}

	private void validar(Conta c) throws SaiException {
		List<String> messages = new ArrayList<String>();

		if (!messages.isEmpty()) {
			throw new SaiException(messages);
		}
	}

	@Transactional
	public void excluir(Conta c) {
		contaDAO.excluir(c);
	}
}
