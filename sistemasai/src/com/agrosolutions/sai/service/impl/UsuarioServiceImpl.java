package com.agrosolutions.sai.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.persistence.NoResultException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.agrosolutions.sai.dao.UsuarioDAO;
import com.agrosolutions.sai.exception.SaiException;
import com.agrosolutions.sai.model.Usuario;
import com.agrosolutions.sai.service.EmailService;
import com.agrosolutions.sai.service.UsuarioService;
import com.agrosolutions.sai.util.ResourceBundle;

@Service("usuarioService")
public class UsuarioServiceImpl implements UsuarioService, UserDetailsService {

	@Autowired
	private UsuarioDAO usuarioDAO;
	@Autowired
	private EmailService emailService;
	
	
	@Transactional
	public Usuario salvar(Usuario usuario) {
		try {
			validar(usuario);
		} catch (SaiException e) {
			throw new SaiException(e.getMessages());
		}
		
		if(usuario.getConfirmarSenha()!=null){
			codificarSenha(usuario);
		}
		
		return usuarioDAO.salvar(usuario);
	}

	private void codificarSenha(Usuario usuario) {
		Md5PasswordEncoder encoder = new Md5PasswordEncoder();
		usuario.setPassword(encoder.encodePassword(usuario.getPassword(), null));
	}
	
	@Transactional
	public int quantidade(Usuario entidade) {
		int resultado;
		try {
			validarPesquisa(entidade);
		} catch (SaiException e) {
			throw new SaiException(e.getMessages());
		}
		resultado = usuarioDAO.quantidade(entidade);
		return resultado;
	}

	@Transactional
	public List<Usuario> pesquisar(Usuario entidade, Integer first, Integer pageSize) {
		List<Usuario> usuarios = null;
		try {
			validarPesquisa(entidade);
		} catch (SaiException e) {
			throw new SaiException(e.getMessages());
		}
		usuarios = usuarioDAO.obterPorParametro(entidade, first, pageSize);
		return usuarios;
	}
	
	private void validar(Usuario usuario) throws SaiException {
		List<String> messages = new ArrayList<String>();
	
		if (usuario.getConfirmarSenha()!= null && !usuario.getPassword().equals(usuario.getConfirmarSenha())) {
			messages.add("Senha n�o confirma.");
		}
		if (usuario.getConfirmarEmail() !=null && !usuario.getEmail().equals(usuario.getConfirmarEmail())) {
			messages.add("Email n�o confirma.");
		}
		if (usuario.getDistritos() == null || usuario.getDistritos().isEmpty()) {
			messages.add("Usu�rio deve atuar em pelo menos um distrito.");
		}

		if (!messages.isEmpty()) {
			throw new SaiException(messages);
		}
	}

	private void validarPesquisa(Usuario entidade) throws SaiException {
		List<String> messages = new ArrayList<String>();

		if (!messages.isEmpty()) {
			throw new SaiException(messages);
		}
	}

	@Override
	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException {
		return findByUsername(username);
	}

	@Override
	public Usuario pesquisarPorLogin(String username) {
		return findByUsername(username);
	}

	private Usuario findByUsername(String username) {

		try {
			return usuarioDAO.pesquisar(username);

		} catch (NoResultException e) {
			throw new UsernameNotFoundException("Usuario nao encontrado.");
		}
	}

	@Transactional
	public void excluir(Usuario usuario) {
		usuarioDAO.excluir(usuario);
	}
	
	@Override
	public Usuario pesquisarPorId(String id) throws SaiException {
		try {
			return usuarioDAO.obterPorId(Long.parseLong(id));	
		} catch (EmptyResultDataAccessException e) {
			return null;
		}
	}
	
	@Override
	@Transactional
	public void enviarEmailSenha(Usuario usuario, String urlComplementacao) throws SaiException {
		List<String> listaEmail = new ArrayList<String>();
		listaEmail.add(usuario.getEmail());
		usuario.setHashCodigo(codificarCodigoContato(usuario.getId()));
		usuarioDAO.salvar(usuario);
		
		urlComplementacao = urlComplementacao.replace("USUARIO_ID", usuario.getId().toString());
		urlComplementacao = urlComplementacao.replace("USUARIO_HASH", usuario.getHashCodigo());

		String corpoEmail = "<div><img src=\"http://inovagri.org.br/sai/img/logoPrincipal.png\"><p> Ol� " + usuario.getNome() + ",</p><p>Para alterar sua senha clique no link abaixo.</p><p></p><div style=\"background:#f0f0f0;width:800px;border:1px solid;padding:12px\"><b>Para redefinir sua senha, clique no link abaixo:</b><br/><a href=\""+urlComplementacao+ "\" target=\"_blank\" avglsprocesses=\"1\">"+urlComplementacao+"</a></div><p> Em caso de qualquer d�vida, nossa equipe de suporte est� � sua disposi��o.</p><p>Atenciosamente,</p></div>";
		emailService.enviarEmail(listaEmail, "Link para redefinir senha", corpoEmail, null);
	}

	private String codificarCodigoContato(Long codigo){
		return String.valueOf(Long.toHexString(codigo.intValue()*new Random().nextLong()));
	}
	
	@Override
	@Transactional
	public void alterarSenha(Usuario usuario) {
		usuario.setPassword(codificarSenha(usuario.getPassword()));
		usuarioDAO.alterarSenha(usuario);
	}

	@Transactional
	public void alterarDistritoPadrao(Usuario usuario) {
		try {
			//usuarioDAO.salvar(usuario);
			usuarioDAO.alterarDistritoPadrao(usuario);			
		} catch (SaiException e) {
			throw new SaiException(e.getMessages());
		}
	}
	
	/**
	 * Encripta valores com hash md5
	 * 
	 * @param String valor
	 * @return String md5
	 */
	public String codificarSenha(String senha) {
		Md5PasswordEncoder encoder = new Md5PasswordEncoder();
		return encoder.encodePassword(senha, null);
	}

	@Transactional
	public List<Usuario> pesquisarPorEmail(String email) {
		try {
			
			return usuarioDAO.pesquisarPorEmail(email);
		} catch (EmptyResultDataAccessException e) {
			
			throw new SaiException(ResourceBundle.getMessage("emailNaoCadastrado2"));
		}
	}

	@Override
	public List<Usuario> enviarEmailRecuperacaoLogin(List<Usuario> usuarios) {
		String nome = usuarios.get(0).getNome();
		List<String> email = new ArrayList<String>();
		email.add(usuarios.get(0).getEmail());
		String usernames = "";
		//List<String> usernames = new ArrayList<String>();
		
		for (Usuario usuario : usuarios) {
			usernames += usuario.getUsername()+"<br/>";
		}
		
		String mensagem = "<div style='background: none repeat scroll 0 0 #f1f1f1; border: 1px solid #999; float: left; padding: 15px; width: 640px;'><img style='margin-bottom: 25px;' src=\"https://sistemasai.com.br:8443/imagens/logotipo.png\"><br/>";
		mensagem += "<p>Ol� " +nome+", </p><br /><p>Localizamos o(s) seguinte(s) usu�rios cadastrado nesse email:</p><br/> <div style='background: #CCC; padding: 10px;'>" + usernames + "</div>";
		mensagem += "<br/><p>Ol� " +nome+", </p><br /><p>Localizamos o(s) seguinte(s) usu�rios cadastrado nesse email:</p><br/> <div style='background: #CCC; padding: 10px;'>" + usernames + "</div>";
		mensagem += "<br/><p>Caso voc� n�o possua a senha de acesso, na p�gina de login existe a possibilidade de recuperar.";
		mensagem += "<div style='clear:both;'></div></div>";
		
		//String corpoEmail = "<div><img src=\"http://inovagri.org.br/sai/img/logoPrincipal.png\"><p> Ol� " + usuario.getNome() + ",</p><p>Para alterar sua senha clique no link abaixo.</p><p></p><div style=\"background:#f0f0f0;width:800px;border:1px solid;padding:12px\"><b>Para redefinir sua senha, clique no link abaixo:</b><br/><a href=\""+urlComplementacao+ "\" target=\"_blank\" avglsprocesses=\"1\">"+urlComplementacao+"</a></div><p> Em caso de qualquer d�vida, nossa equipe de suporte est� � sua disposi��o.</p><p>Atenciosamente,</p></div>";
		emailService.enviarEmail(email, "Recupera��o de login do sistema s@i", mensagem, null);
		
		return null;
	}

}

