package com.agrosolutions.sai.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.agrosolutions.sai.dao.BaciaHidrograficaDAO;
import com.agrosolutions.sai.model.BaciaHidrografica;
import com.agrosolutions.sai.service.BaciaHidrograficaService;

@Service("baciaHidrograficaService")
public class BaciaHidrograficaServiceImpl implements BaciaHidrograficaService {

	@Autowired
	private BaciaHidrograficaDAO baciaHidrograficaDAO;

	@Transactional
	public BaciaHidrografica salvar(BaciaHidrografica baciaHidrografica) {
		return baciaHidrograficaDAO.salvar(baciaHidrografica);
	}

	public List<BaciaHidrografica> obterTodos(List<BaciaHidrografica> baciaHidrograficas) {
		return baciaHidrograficaDAO.obterTodos(baciaHidrograficas);
	}
	
	@Transactional
	public void excluir(BaciaHidrografica baciaHidrografica) {
		baciaHidrograficaDAO.excluir(baciaHidrografica);
	}
	
	@Transactional
	@Override
	public List<BaciaHidrografica> pesquisar(BaciaHidrografica entidade, Integer first,
			Integer pageSize) {
		List<BaciaHidrografica> baciaHidrograficas = null;
		baciaHidrograficas = baciaHidrograficaDAO.obterPorParametro(entidade, first, pageSize);	
		
		return baciaHidrograficas;
	}
	
	@Override
	public List<BaciaHidrografica> pesquisarTodos() {
		return baciaHidrograficaDAO.pesquisarTodos();
	}
}
