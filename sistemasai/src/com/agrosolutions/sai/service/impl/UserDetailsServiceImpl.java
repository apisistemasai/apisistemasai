package com.agrosolutions.sai.service.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.Usuario;

@Component("userDetailsService")
public class UserDetailsServiceImpl implements UserDetailsService {

	@PersistenceContext
	private EntityManager entityManager;

	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		return findByUsername(username);
	}

	private Usuario findByUsername(String username) {

		try {

			Usuario usuario;

			// pegando o usuario
			List<Usuario> lista = entityManager.createNamedQuery("Usuario.findByUsername", Usuario.class)
                          				.setParameter("username", username).getResultList();

			// verificando a lista de usuarios
			if (lista == null || lista.size() == 0){
				return null;
			}
			usuario = lista.get(0);

			return usuario;

		} catch (NoResultException e) {
			throw new UsernameNotFoundException("Usuario nao encontrado");
		}

	}

}
