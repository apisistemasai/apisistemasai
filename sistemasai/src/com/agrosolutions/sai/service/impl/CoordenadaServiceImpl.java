package com.agrosolutions.sai.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.agrosolutions.sai.dao.CoordenadaDAO;
import com.agrosolutions.sai.model.Coordenada;
import com.agrosolutions.sai.model.Irrigante;
import com.agrosolutions.sai.service.CoordenadaService;

@Service("coordenadaService")
public class CoordenadaServiceImpl implements CoordenadaService {

	@Autowired
	private CoordenadaDAO coordenadaDAO;

	@Transactional
	public void salvar(List<Coordenada> coordenadas) {
		coordenadaDAO.salvar(coordenadas);
	}

	@Override
	public List<Coordenada> pesquisarPorIrrigante(Irrigante irrigante) {
		
		return coordenadaDAO.pesquisarPorIrrigante(irrigante);
	}

}