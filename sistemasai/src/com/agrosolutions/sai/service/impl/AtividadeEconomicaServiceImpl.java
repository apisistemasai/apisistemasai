package com.agrosolutions.sai.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.agrosolutions.sai.dao.AtividadeEconomicaDAO;
import com.agrosolutions.sai.model.AtividadeEconomica;
import com.agrosolutions.sai.model.BaciaHidrografica;
import com.agrosolutions.sai.model.Cultura;
import com.agrosolutions.sai.model.Municipio;
import com.agrosolutions.sai.model.Variedade;
import com.agrosolutions.sai.service.AtividadeEconomicaService;

@Service("atividadeEconomicaService")
public class AtividadeEconomicaServiceImpl implements AtividadeEconomicaService {

	@Autowired
	private AtividadeEconomicaDAO atividadeEconomicaDAO;

	@Transactional
	public AtividadeEconomica salvar(AtividadeEconomica atividadeEconomica) {
		return atividadeEconomicaDAO.salvar(atividadeEconomica);
	}

	public List<AtividadeEconomica> pesquisarPorMunicipio(Municipio municipio) {
		return atividadeEconomicaDAO.pesquisarPorMunicipio(municipio);
	}

	public List<AtividadeEconomica> pesquisarPorBacia(BaciaHidrografica bacia) {
		return atividadeEconomicaDAO.pesquisarPorBacia(bacia);
	}

	public List<AtividadeEconomica> pesquisarTodosDeMunicipio() {
		return atividadeEconomicaDAO.pesquisarTodosDeMunicipio();
	}

	public List<AtividadeEconomica> pesquisarTodosDaBacia() {
		return atividadeEconomicaDAO.pesquisarTodosDaBacia();
	}
	@Override
	public List<AtividadeEconomica> pesquisarPorVariedades(Variedade variedade) {
		return atividadeEconomicaDAO.pesquisarPorVariedades(variedade);
	}

	public List<Cultura> pesquisarCulturas(Municipio municipio) {
		return atividadeEconomicaDAO.pesquisarCulturas(municipio);
	}

	public List<Variedade> pesquisarVariedades(Municipio municipio) {
		return atividadeEconomicaDAO.pesquisarVariedades(municipio);
	}

	@Transactional
	public void excluir(AtividadeEconomica atividadeEconomica) {
		atividadeEconomicaDAO.excluir(atividadeEconomica);
	}


}
