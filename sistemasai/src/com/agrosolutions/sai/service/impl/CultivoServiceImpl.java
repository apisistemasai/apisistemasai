package com.agrosolutions.sai.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.agrosolutions.sai.dao.CultivoDAO;
import com.agrosolutions.sai.model.Cultivo;
import com.agrosolutions.sai.model.Cultura;
import com.agrosolutions.sai.model.Irrigante;
import com.agrosolutions.sai.model.Variedade;
import com.agrosolutions.sai.service.CultivoService;

@Service("cultivoService")
public class CultivoServiceImpl implements CultivoService {

	@Autowired
	private CultivoDAO cultivoDAO;

	@Transactional
	public Cultivo salvar(Cultivo cultivo) {
		return cultivoDAO.salvar(cultivo);
	}

	public List<Cultivo> pesquisar(Irrigante irrigante) {
		return cultivoDAO.pesquisar(irrigante);
	}

	public List<Cultura> pesquisarCulturas(Irrigante irrigante) {
		return cultivoDAO.pesquisarCulturas(irrigante);
	}

	public List<Variedade> pesquisarVariedades(Irrigante irrigante) {
		return cultivoDAO.pesquisarVariedades(irrigante);
	}

	@Transactional
	public void excluir(Cultivo cultivo) {
		cultivoDAO.excluir(cultivo);
	}


}
