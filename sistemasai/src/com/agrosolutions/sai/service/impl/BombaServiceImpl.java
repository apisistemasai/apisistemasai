package com.agrosolutions.sai.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.agrosolutions.sai.dao.BombaDAO;
import com.agrosolutions.sai.exception.SaiException;
import com.agrosolutions.sai.model.Bomba;
import com.agrosolutions.sai.model.Fabricante;
import com.agrosolutions.sai.model.Irrigante;
import com.agrosolutions.sai.service.BombaService;

@Service("bombaService")
public class BombaServiceImpl implements BombaService {

	@Autowired
	private BombaDAO bombaDAO;

	@Transactional
	public Bomba salvar(Bomba bomba) {
		return bombaDAO.salvar(bomba);
	}

	public List<Bomba> pesquisar(Fabricante fabricante) {
		return bombaDAO.pesquisar(fabricante);
	}
	@Override
	public List<Bomba> pesquisarPorIrrigante(Irrigante irrigante) {
		return bombaDAO.pesquisarPorIrrigante(irrigante);
	}

	@Override
	@Transactional
	public List<Bomba> pesquisar(Bomba bomba) {
		return bombaDAO.obterPorParametro(bomba, null, null);
	}

	public List<Bomba> obterTodos() {
		return bombaDAO.obterTodos();
	}

	@Transactional
	@Override
	public void excluir(Bomba bomba) {
		bombaDAO.excluir(bomba);
	}

	@Transactional
	@Override
	public List<Bomba> pesquisar(Bomba entidade, Integer first, Integer pageSize) {
		List<Bomba> bombas = null;
		try {
		} catch (SaiException e) {
			throw new SaiException(e.getMessages());
		}

				bombas = bombaDAO.obterPorParametro(entidade, first, pageSize);

		return bombas;
	}

	@Transactional
	public int quantidade(Bomba entidade) {
		int resultado;
		try {
		} catch (SaiException e) {
			throw new SaiException(e.getMessages());
		}
		entidade.setSortField("");
		
		resultado = bombaDAO.quantidade(entidade);
			
		
		return resultado;
	}


	}

	




