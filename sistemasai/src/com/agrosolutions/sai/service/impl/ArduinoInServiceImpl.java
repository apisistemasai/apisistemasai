package com.agrosolutions.sai.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import com.agrosolutions.sai.dao.ArduinoInDAO;
import com.agrosolutions.sai.exception.SaiException;
import com.agrosolutions.sai.model.ArduinoIn;
import com.agrosolutions.sai.service.ArduinoInService;
import com.agrosolutions.sai.util.ResourceBundle;

@Service("arduinoInService")
public class ArduinoInServiceImpl implements ArduinoInService {
	
	@Autowired
	private ArduinoInDAO arduinoInDAO;
	
	@Override
	public List<ArduinoIn> obterTodos() throws SaiException {
		try {
			return arduinoInDAO.obterTodos();
		} catch (EmptyResultDataAccessException e) {
			throw new SaiException(ResourceBundle.getMessage("arduinoInEmptyResult"));
		}
	}
	
}