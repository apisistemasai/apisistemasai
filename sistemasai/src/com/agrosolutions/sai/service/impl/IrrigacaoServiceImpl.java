package com.agrosolutions.sai.service.impl;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.persistence.NoResultException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.agrosolutions.sai.dao.AreaDAO;
import com.agrosolutions.sai.dao.ClimaDAO;
import com.agrosolutions.sai.dao.IrrigacaoDAO;
import com.agrosolutions.sai.dao.PlantioDAO;
import com.agrosolutions.sai.dao.TesteEficienciaDAO;
import com.agrosolutions.sai.exception.SaiException;
import com.agrosolutions.sai.model.Acesso;
import com.agrosolutions.sai.model.Area;
import com.agrosolutions.sai.model.Clima;
import com.agrosolutions.sai.model.Cultivo;
import com.agrosolutions.sai.model.Estadio;
import com.agrosolutions.sai.model.FormulasTI;
import com.agrosolutions.sai.model.Irrigacao;
import com.agrosolutions.sai.model.Irrigante;
import com.agrosolutions.sai.model.Mensagem;
import com.agrosolutions.sai.model.Plantio;
import com.agrosolutions.sai.model.TesteEficiencia;
import com.agrosolutions.sai.model.TipoCalculoKL;
import com.agrosolutions.sai.model.TipoEmissor;
import com.agrosolutions.sai.model.TipoMensagem;
import com.agrosolutions.sai.model.Usuario;
import com.agrosolutions.sai.service.AcessoService;
import com.agrosolutions.sai.service.EmailService;
import com.agrosolutions.sai.service.IrrigacaoService;
import com.agrosolutions.sai.service.MensagemService;
import com.agrosolutions.sai.service.PlantioService;
import com.agrosolutions.sai.service.SmsService;
import com.agrosolutions.sai.util.BigDecimalUtil;
import com.agrosolutions.sai.util.ResourceBundle;

@Service("irrigacaoService")
public class IrrigacaoServiceImpl implements IrrigacaoService {

	public static final Integer MAX_CARACTERES = 140;

	@Autowired
	private IrrigacaoDAO irrigacaoDAO;
	@Autowired
	private PlantioService plantioService;
	@Autowired
	private ClimaDAO climaDAO;
	@Autowired
	private TesteEficienciaDAO testeDAO;
	@Autowired
	private PlantioDAO plantioDAO;
	@Autowired
	private AreaDAO areaDAO;
	@Autowired
	private SmsService smsService;
	@Autowired
	private EmailService emailService;
	@Autowired
	private AcessoService acessoService;
	@Autowired
	private MensagemService mensagemService;

	@Override
	public Irrigacao pesquisar(Plantio p, Date data) {
		Irrigacao irrigacao = null;
		try {
			irrigacao = irrigacaoDAO.pesquisar(p, data);
		} catch (EmptyResultDataAccessException e) {
		}
		return irrigacao;
	}

	@Override
	public List<Irrigacao> pesquisarPorData(Date data) {
		List<Irrigacao> irrigacoes = null;
		try {
			irrigacoes = irrigacaoDAO.pesquisarPorData(data);
		} catch (EmptyResultDataAccessException e) {
		}
		return irrigacoes;
	}

	@Override
	public List<Irrigacao> pesquisarPorIrrigantePeriodo(Date dataIni, Date dataFim, Irrigante ir) {
		List<Irrigacao> irrigacoes = null;
		try {
			irrigacoes = irrigacaoDAO.pesquisarPorIrrigantePeriodo(dataIni, dataFim, ir);
		} catch (EmptyResultDataAccessException e) {
			new EmptyResultDataAccessException(e.getActualSize());
		} catch (SaiException e) {
			new SaiException(e.getMessages());
		}
		return irrigacoes;
	}

	@Override
	public List<Irrigacao> pesquisarPorIrriganteData(Date data, Irrigante ir) {
		List<Irrigacao> irrigacoes = null;
		try {
			irrigacoes = irrigacaoDAO.pesquisarPorIrriganteData(data, ir);
		} catch (EmptyResultDataAccessException e) {
		}
		return irrigacoes;
	}

	@Override
	public List<Irrigacao> pesquisarPorPlantio(Plantio p) {
		List<Irrigacao> irrigacoes = null;
		try {
			irrigacoes = irrigacaoDAO.pesquisarPorPlantio(p);
		} catch (EmptyResultDataAccessException e) {
		}
		return irrigacoes;
	}
	
	@Transactional
	@Override
	public int quantidade(Irrigacao entidade) {
		int resultado;
		try {
			validarPesquisa(entidade);
		} catch (SaiException e) {
			throw new SaiException(e.getMessages());
		}
		entidade.setSortField("");
		resultado = irrigacaoDAO.quantidade(entidade);
		return resultado;
	}

	@Transactional
	@Override
	public List<Irrigacao> pesquisar(Irrigacao entidade, Integer first, Integer pageSize) {
		List<Irrigacao> irrigacoes = null;
		try {
			validarPesquisa(entidade);
		} catch (SaiException e) {
			throw new SaiException(e.getMessages());
		}
		try {
			irrigacoes = irrigacaoDAO.obterPorParametro(entidade, first, pageSize);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return irrigacoes;
	}
	
	private void validarPesquisa(Irrigacao entidade) throws SaiException {
		List<String> messages = new ArrayList<String>();
		limparFiltros(entidade);

		if (!messages.isEmpty()) {
			throw new SaiException(messages);
		}
	}

	// Limpando atributos vazio para nulos para n�o entrar na clausula where
	public void limparFiltros(Irrigacao entidade){
		if (entidade.getPlantio().getCultivo().getIrrigante().getLocalizacao() != null && entidade.getPlantio().getCultivo().getIrrigante().getLocalizacao().equals("")) {
			entidade.getPlantio().getCultivo().getIrrigante().setLocalizacao(null);
		}

	}

	@Transactional
	@Override
	public void excluir(Irrigacao irrigacao) {
		irrigacaoDAO.excluir(irrigacao);
	}	
	
	@Transactional
	@Override
	public void enviarSMS(Irrigacao irrigacao) {
		irrigacaoDAO.enviarSMS(irrigacao);
	}	

	@Transactional
	@Override
	public void deletar(Plantio p) {
		irrigacaoDAO.deletar(p);
	}	

	@Transactional
	@Override
	public Irrigacao salvar(Irrigacao irrigacao) {
		return irrigacaoDAO.salvar(irrigacao);
	}

	@Transactional
	@Override
	public List<Irrigacao> recalcular(Plantio p) {
		List<Irrigacao> irrigacoes = new ArrayList<Irrigacao>();
		List<Clima> climas = climaDAO.pesquisarPorPeriodo(p.getSetor().getIrrigante().getArea(), p.getCultivo().getDataInicio(), p.getDataFim()==null?new Date():p.getDataFim());
		try {
			for (Clima clima : climas) {
				irrigacoes.add(checarIrrigacao(clima, p)); 
			}
			
		} catch (NullPointerException e) {
			throw new SaiException( ResourceBundle.getMessage("dadoNulo") );
		} catch (SaiException e) {
			throw new SaiException(e.getMessages());
		}
		return irrigacoes;
	}	

	@Transactional
	@Override
	public void recalcular(Usuario u) {
		List<Area> areas = areaDAO.pesquisar(u.getDistritos());
		for (Area area : areas) {
			List<Plantio> plantios = plantioDAO.ativos(area, new Date());
			for (Plantio plantio : plantios) {
				//apagar registro anteriores
				deletar(plantio);
				List<Clima> climas = climaDAO.pesquisarPorPeriodo(area, plantio.getCultivo().getDataInicio(), plantio.getDataFim()==null?new Date():plantio.getDataFim());
				for (Clima clima : climas) {
					Irrigacao irrigacao = checarIrrigacao(clima, plantio);
					if (irrigacao != null) {
						irrigacao = salvar(irrigacao);
					}
				}
			}
		}		
	}	
	
	@Transactional
	@Override
	public void calcularTempoIrrigacao(Clima clima) {
		List<Plantio> plantios = plantioDAO.ativos(clima.getArea(), clima.getData());
		for (Plantio plantio : plantios) {
			//Gerar o TI da irriga��o
			Irrigacao irrigacao = checarIrrigacao(clima, plantio);
			if (irrigacao != null) {
				configurarNotificacoes( plantio, plantio.getCultivo().getIrrigante(), irrigacao );
				irrigacaoDAO.salvar(irrigacao);
			}
		}
	}
	
	/**
	 * Habilita o SMS e Email para a irriga��o se o irrigante quer receber SMS ou Email de acordo com cada PLANTIO
	 * 
	 * @param plantio
	 * @param irrigante
	 * @param irrigacao
	 */
	private void configurarNotificacoes(Plantio plantio, Irrigante irrigante, Irrigacao irrigacao) {
		
		if ( irrigante.getEnviarSMS() ) {
			if ( plantio.getEnviarSMS() ) {
				irrigacao.setEnviarSms(true);
			}else{
				irrigacao.setEnviarSms(false);
			}
		} else {
			irrigacao.setEnviarSms(false);
		}
		
		if ( irrigante.getEnviarEmail() ) {
			if ( plantio.getEnviarEmail() ) {
				irrigacao.setEnviarEmail(true);
			}else{
				irrigacao.setEnviarEmail(false);
			}
		} else {
			irrigacao.setEnviarEmail(false);
		}
		
	}

	public Irrigacao checarIrrigacao(Clima clima, Plantio plantio) {
		
		plantio.setFormula(plantioService.verificarFormula(plantio));
		
		/*	N�o tem irriga��o se:
		 * 	N�o der para identificar a formula, a vaz�o for zero o raio do emissor for zero
		 * 	for sequeiro ou consorcio
		 */
		if (plantio.getSequeiro() || plantio.getConsorcio() || plantio.getFormula().getFormulaTI() == null || plantio.getFormula().getVazao().equals(new BigDecimal(0)) || plantio.getFormula().getDiametro().equals(new BigDecimal(0))){
			return null;
		}
		
		Irrigacao irrigacao = new Irrigacao(clima.getEtoCalculado(), clima.getChuvaEfetiva(), plantio, clima.getData(), true, plantio.getEnviarSMS(), true, plantio.getEnviarEmail() );
		irrigacao = criarTempoIrrigacao(irrigacao);
		return irrigacao;
	}

	private Irrigacao criarTempoIrrigacao(Irrigacao irrigacao) {
		GregorianCalendar data = new GregorianCalendar();
		data.setTime(irrigacao.getData());
		
		try {
			irrigacao.setZ( calcularProgressaoZ(irrigacao) );
		} catch (SaiException e) {
			throw new SaiException(e.getMessages());
		}
		
		if (irrigacao.getPlantio().getFormula().getTipoEmissor() != null) {
			
			TesteEficiencia teste = null;
			try{
				teste = testeDAO.pesquisarPorSetorData(irrigacao.getPlantio().getSetor(), irrigacao.getData());
			} catch (EmptyResultDataAccessException e) {
			}
			
			BigDecimal t = new BigDecimal(0);
			if (teste==null) {
				if (irrigacao.getPlantio().getFormula().getTipoEmissor().equals(TipoEmissor.Aspersao)) {
					t = new BigDecimal("0.7");
				}
				if (irrigacao.getPlantio().getFormula().getTipoEmissor().equals(TipoEmissor.Microaspersao)) {
					t = new BigDecimal("0.85");
				}
				if (irrigacao.getPlantio().getFormula().getTipoEmissor().equals(TipoEmissor.Gotejamento)) {
					t = new BigDecimal("0.9");
				}
			}else{
				t = teste.getEficiencia().divide(new BigDecimal(100), BigDecimalUtil.MC_RUP);
			}
			irrigacao.setEficiencia(t);
			
			if (irrigacao.getEtc()!= null) {
				if (irrigacao.getPlantio().getFormula().getFormulaTI().equals(FormulasTI.GotejoAreaPam)) {
					irrigacao = calculoGotejoAreaCoberturaPam(irrigacao);
				}
				if (irrigacao.getPlantio().getFormula().getFormulaTI().equals(FormulasTI.GotejoAreaKl)) {
					irrigacao = calculoGotejoAreaCoberturaKl(irrigacao);
				}
				if (irrigacao.getPlantio().getFormula().getFormulaTI().equals(FormulasTI.GotejoFaixaPam)) {
					irrigacao = calculoGotejoFaixaPam(irrigacao);
				}
				if (irrigacao.getPlantio().getFormula().getFormulaTI().equals(FormulasTI.GotejoFaixaKl)) {
					irrigacao = calculoGotejoFaixaKl(irrigacao);
				}
				if (irrigacao.getPlantio().getFormula().getFormulaTI().equals(FormulasTI.MicroHasteNormalPAM)) {
					irrigacao = calculoMicroHasteNormalPAM(irrigacao);
				}
				if (irrigacao.getPlantio().getFormula().getFormulaTI().equals(FormulasTI.MicroHasteNormalKL)) {
					irrigacao = calculoMicroHasteNormalKL(irrigacao);
				}
				if (irrigacao.getPlantio().getFormula().getFormulaTI().equals(FormulasTI.MicroHasteAlongada)) {
					irrigacao = calculoMicroHasteAlongada(irrigacao);
				}
				if (irrigacao.getPlantio().getFormula().getFormulaTI().equals(FormulasTI.Aspercao)) {
					irrigacao = calculoAspersao(irrigacao);
				}
			}else{
				irrigacao = null;
			}
		}
		
		return irrigacao;
	}
	
	
	/**
	 *  faixaDeCrescimentoRaiz = profRaizFim - profRaizIni
	 *  crescimentoRaizDiario = faixaDeCrescimentoRaiz / somaDiasEstadios
	 *  profRaiz = profRaizInicial + crescimentoRaizDiario * qtdDiasCultivo
	 */
	public BigDecimal calcularProgressaoZ(Irrigacao irrigacao){
		
		BigDecimal z = new BigDecimal(0);
		BigDecimal crescimentoRaizDiario = new BigDecimal(0);
		Long somaDiasEstadios = 0L;
		Cultivo cultivo = irrigacao.getPlantio().getCultivo();
		Estadio estadioAtual = irrigacao.getPlantio().getCultivo().getEstadioAtual();
		BigDecimal profRaizInicial = new BigDecimal(0);
		BigDecimal profRaizFinal = new BigDecimal(0);
		
		for (Estadio estadio : cultivo.getReferencia().getEstadios()) {
			if (estadio.getFase().equals("INICIAL")) profRaizInicial = estadio.getProfundidadeRaiz();
			if (estadio.getFase().equals("FLORACAO")) profRaizFinal = estadio.getProfundidadeRaiz();
		}
		
		if (profRaizInicial == null || profRaizFinal == null ) 
			throw new SaiException(ResourceBundle.getMessage("erroCalculoProgressaoRaiz"));
		
		if ( estadioAtual.getOrdem() >= 3 ) 
			return z = profRaizFinal;
		
		for (Estadio estadio : cultivo.getReferencia().getEstadios()) {
			if ( estadioAtual.getOrdem() == estadio.getOrdem() ) {
				somaDiasEstadios += estadio.getQtdDiasAtual();
				continue;
			} else {
				somaDiasEstadios += estadio.getQtdDias();
			}			
		}
		
		crescimentoRaizDiario = profRaizFinal.subtract(profRaizInicial).divide(new BigDecimal(somaDiasEstadios), BigDecimalUtil.MC_RDO);
		z = profRaizInicial.add( crescimentoRaizDiario.multiply(new BigDecimal(somaDiasEstadios)) );
		return z;
	}

	private Irrigacao calculoAspersao(Irrigacao irrigacao) {
		DecimalFormatSymbols symbols = new DecimalFormatSymbols();
	    symbols.setDecimalSeparator(',');
	    DecimalFormat formatter = new DecimalFormat("0.00", symbols);

	    String memoria = " Aspers�o \n "; 
		BigDecimal itn  = irrigacao.getEtc().subtract(irrigacao.getChuva());
		
		BigDecimal espE = irrigacao.getPlantio().getEspE().equals(0.0)?new BigDecimal(1):new BigDecimal(irrigacao.getPlantio().getEspE());
		BigDecimal espLE= irrigacao.getPlantio().getEspLE().equals(0.0)?new BigDecimal(1):new BigDecimal(irrigacao.getPlantio().getEspLE());
		//Calculo do Intesidade de Aplica��o (IA)
		BigDecimal area = espE.multiply(espLE);
		BigDecimal ia = irrigacao.getPlantio().getFormula().getVazao().divide(area, BigDecimalUtil.MC_RUP);
		memoria = memoria + " Ia = Qe / (EspE * EspLE) = " + formatter.format(irrigacao.getPlantio().getFormula().getVazao()) + " / (" + formatter.format(espE) + " * " + formatter.format(espLE) + ") = " + formatter.format(ia) + "mm/h \n "; 
		//Definir a l�mina bruta (lb)
		BigDecimal lb = itn.divide(irrigacao.getEficiencia(), BigDecimalUtil.MC_RUP);
		memoria = memoria + "Lb = ((Etc - Pe)/Ea) = ((" + formatter.format(irrigacao.getEtc()) + " - " + formatter.format(irrigacao.getChuva()) + ")/ " + formatter.format(irrigacao.getEficiencia()) + ") = " + formatter.format(lb) + "mm \n ";  
		//Calcular o Tempo de Irriga��o TI
		BigDecimal tih = lb.divide(ia, BigDecimalUtil.MC_RUP);
		//Em minutos
		irrigacao.setTempoIrrigacao(tih.multiply(new BigDecimal(60)));
		memoria = memoria + "TI = Lb/Ia = " + formatter.format(lb) + " / " + formatter.format(ia) + " = " + formatter.format(irrigacao.getTempoIrrigacao()) + "min \n ";
		//Legenda das Siglas
		memoria = memoria + "Legendas: \n Ea : Efici�ncia de aplica��o \n EspE : Espa�amento entre Emissores (m) \n EspLE : Espa�amento entre Linhas de Emissores (m) \n Etc : Evapotranspira��o da Cultura (mm.dia-1) \n Ia : Intensidade de aplica��o \n Lb : L�mina Bruta (mm.dia-1) \n Pe : Precipita��o Efetiva (mm.dia-1) \n Qe : Vaz�o do emissor \n TI : Tempo de Irriga��o  \n  ";
		irrigacao.setMemoriaCalculo(memoria);
		
		return irrigacao;
	}

	private Irrigacao calculoMicroHasteNormalPAM(Irrigacao irrigacao) {
		DecimalFormatSymbols symbols = new DecimalFormatSymbols();
	    symbols.setDecimalSeparator(',');
	    DecimalFormat formatter = new DecimalFormat("0.00", symbols);

	    String memoria =  "MicroAspers�o - Haste Normal PAM \n "; 
		BigDecimal itn  = irrigacao.getEtc().subtract(irrigacao.getChuva());
		BigDecimal espPlantas = irrigacao.getPlantio().getEspacamento1().equals(0.0)?new BigDecimal(1):new BigDecimal(irrigacao.getPlantio().getEspacamento1());
		BigDecimal espFilaPlantas = irrigacao.getPlantio().getEspacamento2().equals(0.0)?new BigDecimal(1):new BigDecimal(irrigacao.getPlantio().getEspacamento2());
		BigDecimal nEmissores = irrigacao.getPlantio().getTotalEmissores()==null || irrigacao.getPlantio().getTotalEmissores().intValue() == 0?new BigDecimal(1):new BigDecimal(irrigacao.getPlantio().getTotalEmissores());
		BigDecimal nPlantas = irrigacao.getPlantio().getTotalPlanta()==null || irrigacao.getPlantio().getTotalPlanta().intValue() == 0?new BigDecimal(1):new BigDecimal(irrigacao.getPlantio().getTotalPlanta());
		BigDecimal nEnP = nEmissores.divide(nPlantas, BigDecimalUtil.MC_RUP);
		
		BigDecimal diam = irrigacao.getPlantio().getFormula().getDiametro().pow(2);
		//Calculo do Intesidade de Aplica��o (IA)
		BigDecimal x = nEnP.multiply(irrigacao.getPlantio().getFormula().getVazao());
		BigDecimal ap = espPlantas.multiply(espFilaPlantas);
		BigDecimal ia = x.divide(ap, BigDecimalUtil.MC_RUP);
		memoria = memoria + "Ia = (Ne * q) / Ap = (" + formatter.format(nEnP) + " * " + formatter.format(irrigacao.getPlantio().getFormula().getVazao()) + ") / " + formatter.format(espPlantas.setScale(2, BigDecimal.ROUND_UP))+ " * " + formatter.format(espFilaPlantas.setScale(2, BigDecimal.ROUND_UP)) + " = " + formatter.format(ia) + "mm/h \n "; 
		//Porcentagem da �rea molhada - PAM
		BigDecimal am = (new BigDecimal(Math.PI).multiply(diam)).divide(new BigDecimal(4), BigDecimalUtil.MC_RUP);
		BigDecimal pam = am.divide(ap, BigDecimalUtil.MC_RUP);
		memoria = memoria + "PAM = AM / Ap = ((" + formatter.format(new BigDecimal(Math.PI).setScale(2, BigDecimal.ROUND_UP)) + " * " + formatter.format(diam.setScale(2, BigDecimal.ROUND_UP)) + ") / 4) / " + formatter.format(espPlantas.setScale(2, BigDecimal.ROUND_UP)) + " * " + formatter.format(espFilaPlantas.setScale(2, BigDecimal.ROUND_UP)) + " = " + formatter.format(pam) + " \n "; 
		//Se PAM for menor que 65% considerar 65%.
		if(pam.compareTo(new BigDecimal(0.65)) < 0){
			pam = new BigDecimal(0.65);
		}
		//Se PAM for maior que 100% considerar 100%.
		if(pam.compareTo(new BigDecimal(1)) > 0){
			pam = new BigDecimal(1);
		}
		//Definir a l�mina bruta (lb)
		BigDecimal x1 = itn.divide(irrigacao.getEficiencia(), BigDecimalUtil.MC_RUP);
		BigDecimal lb = x1.multiply(pam);
		memoria = memoria + "Lb = ((Etc - Pe)/Ea) * PAM = ((" + formatter.format(irrigacao.getEtc().setScale(2, BigDecimal.ROUND_UP)) + " - " + formatter.format(irrigacao.getChuva()) + ")/ " + formatter.format(irrigacao.getEficiencia().setScale(2, BigDecimal.ROUND_UP)) + ") * " + formatter.format(pam.setScale(2, BigDecimal.ROUND_UP)) + " = " + formatter.format(lb.setScale(2, BigDecimal.ROUND_UP)) + "mm \n ";  
		//Calcular o Tempo de Irriga��o TI
		BigDecimal tih = lb.divide(ia, BigDecimalUtil.MC_RUP);
		//Em minutos
		irrigacao.setTempoIrrigacao(tih.multiply(new BigDecimal(60)));
		memoria = memoria + "TI = Lb/Ia = " + formatter.format(lb.setScale(2, BigDecimal.ROUND_UP)) + " / " + formatter.format(ia.setScale(2, BigDecimal.ROUND_UP)) + " = " + formatter.format(irrigacao.getTempoIrrigacao()) + "min \n \n ";
		//Legenda das Siglas
		memoria = memoria + "Legendas: \n  AM : �rea Molhada \n  Ap : �rea da Planta \n  Ea : Efici�ncia de aplica��o \n  Etc : Evapotranspira��o da Cultura (mm.dia-1) \n  Ia : Intensidade de aplica��o \n  Lb : L�mina Bruta (mm.dia-1) \n  Ne : N�mero de Emissores (L.h-1) \n  Np : N�mero de Plantas \n  PAM : Porcentagem de �rea Molhada (%) \n  Pe : Precipita��o Efetiva (mm.dia-1) \n  q : Vaz�o do Emissor (L.h-1) \n  TI : Tempo de Irriga��o \n  ";
		irrigacao.setMemoriaCalculo(memoria);
		return irrigacao;
	}
	
	/**
	 *  d1 = cc - pmp
	 *	aguaDisponivel = d1 * z
	 *	aguaAplicada = vazao * nEnP * turnoRega
	 *
	 *	Ea = aguaDisponivel / aguaAplicada
	 *	Se Ea for igual 0, ele assume como 0,7
	 *
	 *	irn = etc - chuva
	 *	itn = irn / Ea
	 *
	 *	d = se * sl
	 *	la = vazao / d
	 *
	 *	ti = itn / la
	 *	//Em minutos
	 *	ti * 60
	 */
	private Irrigacao calculoMicroHasteNormalKL(Irrigacao irrigacao) {
		DecimalFormatSymbols symbols = new DecimalFormatSymbols();
	    symbols.setDecimalSeparator(',');
	    DecimalFormat formatter = new DecimalFormat("0.00", symbols);

	    irrigacao.setMemoriaCalculo("MicroAspers�o - Haste Normal KL \n ");
		BigDecimal kl = calculoKl(irrigacao);
		String memoria = irrigacao.getMemoriaCalculo();

		BigDecimal espPlantas = irrigacao.getPlantio().getEspacamento1().equals(0.0)?new BigDecimal(1):new BigDecimal(irrigacao.getPlantio().getEspacamento1());
		BigDecimal espFilaPlantas = irrigacao.getPlantio().getEspacamento2().equals(0.0)?new BigDecimal(1):new BigDecimal(irrigacao.getPlantio().getEspacamento2());
		BigDecimal nEmissores = irrigacao.getPlantio().getTotalEmissores()==null || irrigacao.getPlantio().getTotalEmissores().intValue() == 0?new BigDecimal(1):new BigDecimal(irrigacao.getPlantio().getTotalEmissores());
		BigDecimal nPlantas = irrigacao.getPlantio().getTotalPlanta()==null || irrigacao.getPlantio().getTotalPlanta().intValue() == 0?new BigDecimal(1):new BigDecimal(irrigacao.getPlantio().getTotalPlanta());
		BigDecimal nEnP = nEmissores.divide(nPlantas, BigDecimalUtil.MC_RUP);
		
		//Calculo do Intesidade de Aplica��o (IA)
		BigDecimal x = nEnP.multiply(irrigacao.getPlantio().getFormula().getVazao());
		BigDecimal ap = espPlantas.multiply(espFilaPlantas);
		BigDecimal ia = x.divide(ap, BigDecimalUtil.MC_RUP);
		memoria = memoria + " Ia = (Ne * q) / Ap = (" + formatter.format(nEnP) + " * " + formatter.format(irrigacao.getPlantio().getFormula().getVazao()) + ") / " + formatter.format(espPlantas.setScale(2, BigDecimal.ROUND_UP)) + " * " + formatter.format(espFilaPlantas.setScale(2, BigDecimal.ROUND_UP)) + " = " + formatter.format(ia) + "mm/h \n "; 
		//Definir a l�mina bruta (lb)
		BigDecimal irn1 = irrigacao.getEtc().multiply(kl);
		BigDecimal itn  = irn1.subtract(irrigacao.getChuva());
		BigDecimal irn = itn.divide(irrigacao.getEficiencia(), BigDecimalUtil.MC_RUP);
		BigDecimal lb  = irn.multiply(new BigDecimal(irrigacao.getPlantio().getTurnoRega()));

		memoria = memoria + "Lb = (((Etc * KL) - Pe)  /Ea) * Tr = (((" + formatter.format(irrigacao.getEtc().setScale(2, BigDecimal.ROUND_UP)) + " - " + formatter.format(irrigacao.getChuva().setScale(2, BigDecimal.ROUND_UP)) + ") * " + formatter.format(kl.setScale(2, BigDecimal.ROUND_UP)) + " / " + formatter.format(irrigacao.getEficiencia().setScale(2, BigDecimal.ROUND_UP)) + ") * " + formatter.format(irrigacao.getPlantio().getTurnoRega()) + " = " + formatter.format(lb.setScale(2, BigDecimal.ROUND_UP)) + "mm \n ";  
		//Calcular o Tempo de Irriga��o TI
		BigDecimal tih = lb.divide(ia, BigDecimalUtil.MC_RUP);
		//Em minutos
		irrigacao.setTempoIrrigacao(tih.multiply(new BigDecimal(60)));
		memoria = memoria + "TI = Lb/Ia = " + formatter.format(lb.setScale(2, BigDecimal.ROUND_UP)) + " / " + formatter.format(ia.setScale(2, BigDecimal.ROUND_UP)) + " = " + formatter.format(irrigacao.getTempoIrrigacao()) + "min \n \n ";
		//Legenda das Siglas
		memoria = memoria + "Legendas: \n  AM : �rea Molhada \n  Ap : �rea da Planta \n  Ea : Efici�ncia de aplica��o \n  Etc : Evapotranspira��o da Cultura (mm.dia-1) \n  Ia : Intensidade de aplica��o \n  Lb : L�mina Bruta (mm.dia-1) \n  Ne : N�mero de Emissores (L.h-1) \n  Np : N�mero de Plantas \n  PAM : Porcentagem de �rea Molhada (%) \n  Pe : Precipita��o Efetiva (mm.dia-1) \n  q : Vaz�o do Emissor (L.h-1) \n  TI : Tempo de Irriga��o \n  ";
		irrigacao.setMemoriaCalculo(memoria);
		return irrigacao;
	}

	private Irrigacao calculoMicroHasteAlongada(Irrigacao irrigacao) {
		DecimalFormatSymbols symbols = new DecimalFormatSymbols();
	    symbols.setDecimalSeparator(',');
	    DecimalFormat formatter = new DecimalFormat("0.00", symbols);

		String memoria =  " MicroAspers�o - Haste Alongada \n ";
		BigDecimal nEmissores = irrigacao.getPlantio().getTotalEmissores()==null || irrigacao.getPlantio().getTotalEmissores().intValue() == 0?new BigDecimal(1):new BigDecimal(irrigacao.getPlantio().getTotalEmissores());
		BigDecimal nPlantas = irrigacao.getPlantio().getTotalPlanta()==null || irrigacao.getPlantio().getTotalPlanta().intValue() == 0?new BigDecimal(1):new BigDecimal(irrigacao.getPlantio().getTotalPlanta());
		BigDecimal nEnP = nEmissores.divide(nPlantas, BigDecimalUtil.MC_RUP);
		BigDecimal se = irrigacao.getPlantio().getEspE().equals(0.0)?new BigDecimal(1):new BigDecimal(irrigacao.getPlantio().getEspE());
		BigDecimal sl = irrigacao.getPlantio().getEspLE().equals(0.0)?new BigDecimal(1):new BigDecimal(irrigacao.getPlantio().getEspLE());

		BigDecimal d1 = irrigacao.getPlantio().getSetor().getCc().subtract(irrigacao.getPlantio().getSetor().getPmp());
		BigDecimal aguaDisponivel = d1.multiply(irrigacao.getZ());
		BigDecimal aguaAplicada = irrigacao.getPlantio().getFormula().getVazao().multiply(nEnP).multiply(new BigDecimal(irrigacao.getPlantio().getTurnoRega()));
		try {
			BigDecimal Ea = aguaDisponivel.divide(aguaAplicada, BigDecimalUtil.MC_RUP);
			if (Ea == null || Ea.compareTo(new BigDecimal(0))==0) {
				Ea = new BigDecimal(0.7);
			}
			memoria = memoria + "Ea = Qtd H2O �til armazenada /Qtd H2O aplicada = ((Qcc - Qpmp) * Z )/ q * (Ne/Np) * Tr = " + formatter.format(aguaDisponivel.setScale(2, BigDecimal.ROUND_UP)) + " / " + formatter.format(aguaAplicada.setScale(2, BigDecimal.ROUND_UP)) + " = " + formatter.format(Ea.setScale(2, BigDecimal.ROUND_UP)) + " \n ";
			
			BigDecimal irn  = irrigacao.getEtc().subtract(irrigacao.getChuva());
			BigDecimal itn  = irn.divide(Ea, BigDecimalUtil.MC_RUP);
			memoria = memoria + "ITN = IRN / Ea = (Etc - Pe) / Ea = " + irn.setScale(2, BigDecimal.ROUND_UP).toString() + " / " + Ea.setScale(2, BigDecimal.ROUND_UP).toString() + " = " + itn.setScale(2, BigDecimal.ROUND_UP).toString() + "mm/d \n ";
			BigDecimal d = se.multiply(sl);
			BigDecimal la = irrigacao.getPlantio().getFormula().getVazao().divide(d, BigDecimalUtil.MC_RUP);
			memoria = memoria + "LA - L�mina Aplicada = q / Se * Sl = " + irrigacao.getPlantio().getFormula().getVazao().setScale(2, BigDecimal.ROUND_UP).toString() + " / " + se.setScale(2, BigDecimal.ROUND_UP) + " * " + sl.setScale(2, BigDecimal.ROUND_UP).toString() + " = " + la.setScale(2, BigDecimal.ROUND_UP).toString() + "mm/h \n ";
			
			BigDecimal ti = itn.divide(la, BigDecimalUtil.MC_RUP);
			//Em minutos
			irrigacao.setTempoIrrigacao(ti.multiply(new BigDecimal(60)));
			memoria = memoria + "TI = (ITN / LA) * 60 = (" + itn.setScale(2, BigDecimal.ROUND_UP).toString() + " / " + la.setScale(2, BigDecimal.ROUND_UP).toString() + ") * 60 = " + irrigacao.getTempoIrrigacao().setScale(2, BigDecimal.ROUND_UP).toString() + "min \n ";
			//Legenda das Siglas 
			memoria = memoria + "Legendas:  \n  ";
			irrigacao.setMemoriaCalculo(memoria);
		} catch (java.lang.ArithmeticException e) {
			System.out.println("Erro AE2 -" + e.getMessage());
		}
		
		return irrigacao;
	}
	
	@SuppressWarnings("unused")
	private Irrigacao calculoMicroAnterior(Irrigacao irrigacao) {
		DecimalFormatSymbols symbols = new DecimalFormatSymbols();
	    symbols.setDecimalSeparator(',');
	    DecimalFormat formatter = new DecimalFormat("0.00", symbols);

		String memoria = " MicroAspers�o \n "; 
		BigDecimal itn  = irrigacao.getEtc().subtract(irrigacao.getChuva());
		BigDecimal espPlantas = irrigacao.getPlantio().getEspacamento1().equals(0.0)?new BigDecimal(1):new BigDecimal(irrigacao.getPlantio().getEspacamento1());
		BigDecimal espFilaPlantas = irrigacao.getPlantio().getEspacamento2().equals(0.0)?new BigDecimal(1):new BigDecimal(irrigacao.getPlantio().getEspacamento2());
		BigDecimal nEmissores = irrigacao.getPlantio().getTotalEmissores()==null || irrigacao.getPlantio().getTotalEmissores().intValue() == 0?new BigDecimal(1):new BigDecimal(irrigacao.getPlantio().getTotalEmissores());
		BigDecimal nPlantas = irrigacao.getPlantio().getTotalPlanta()==null || irrigacao.getPlantio().getTotalPlanta().intValue() == 0?new BigDecimal(1):new BigDecimal(irrigacao.getPlantio().getTotalPlanta());
		
		BigDecimal gotejoPlanta = nEmissores.divide(nPlantas, BigDecimalUtil.MC_RUP);
		BigDecimal diam = irrigacao.getPlantio().getFormula().getDiametro().pow(2);
		//Calculo do Intesidade de Aplica��o (IA)
		BigDecimal x = gotejoPlanta.multiply(irrigacao.getPlantio().getFormula().getVazao());
		BigDecimal ap = espPlantas.multiply(espFilaPlantas);
		BigDecimal ia = x.divide(ap, BigDecimalUtil.MC_RUP);
		memoria = memoria + " Ia = ((Ne/Np) * Qe) / Ap = (" + formatter.format(gotejoPlanta) + " * " + formatter.format(irrigacao.getPlantio().getFormula().getVazao()) + ") / " + formatter.format(espPlantas) + " * " + formatter.format(espFilaPlantas) + " = " + formatter.format(ia) + "mm/h \n "; 
		//Porcentagem da �rea molhada - PAM
		BigDecimal am = (new BigDecimal(Math.PI).multiply(diam)).divide(new BigDecimal(4), BigDecimalUtil.MC_RUP);
		BigDecimal pam = am.divide(ap, BigDecimalUtil.MC_RUP);
		memoria = memoria + "PAM = AM / Ap = ((" + formatter.format(new BigDecimal(Math.PI).setScale(2, BigDecimal.ROUND_UP)) + " * " + formatter.format(diam) + ") / 4) / " + formatter.format(espPlantas) + " * " + formatter.format(espFilaPlantas) + " = " + formatter.format(pam) + " \n "; 
		//Definir a l�mina bruta (lb)
		BigDecimal x1 = itn.divide(irrigacao.getEficiencia(), BigDecimalUtil.MC_RUP);
		BigDecimal lb = x1.multiply(pam);
		memoria = memoria + "Lb = ((Etc - Pe)/Ea) * PAM = ((" + formatter.format(irrigacao.getEtc()) + " - " + formatter.format(irrigacao.getChuva()) + ")/ " + formatter.format(irrigacao.getEficiencia()) + ") * " + formatter.format(pam) + " = " + formatter.format(lb) + "mm \n ";  
		//Calcular o Tempo de Irriga��o TI
		BigDecimal tih = lb.divide(ia, BigDecimalUtil.MC_RUP);
		//Em minutos
		irrigacao.setTempoIrrigacao(tih.multiply(new BigDecimal(60)));
		memoria = memoria + "TI = Lb/Ia = " + formatter.format(lb) + " / " + formatter.format(ia) + " = " + formatter.format(irrigacao.getTempoIrrigacao()) + "min \n ";
		irrigacao.setMemoriaCalculo(memoria);
		return irrigacao;
	}

	private Irrigacao calculoGotejoAreaCoberturaPam(Irrigacao irrigacao) {
		DecimalFormatSymbols symbols = new DecimalFormatSymbols();
	    symbols.setDecimalSeparator(',');
	    DecimalFormat formatter = new DecimalFormat("0.00", symbols);

		String memoria = " Gotejo - �rea de Cobertura - PAM \n ";
		BigDecimal itn  = irrigacao.getEtc().subtract(irrigacao.getChuva());
		BigDecimal espPlantas = irrigacao.getPlantio().getEspacamento1().equals(0.0)?new BigDecimal(1):new BigDecimal(irrigacao.getPlantio().getEspacamento1());
		BigDecimal espFilaPlantas = irrigacao.getPlantio().getEspacamento2().equals(0.0)?new BigDecimal(1):new BigDecimal(irrigacao.getPlantio().getEspacamento2());
		BigDecimal nEmissores = irrigacao.getPlantio().getTotalEmissores()==null || irrigacao.getPlantio().getTotalEmissores().intValue() == 0?new BigDecimal(1):new BigDecimal(irrigacao.getPlantio().getTotalEmissores());
		BigDecimal nPlantas = irrigacao.getPlantio().getTotalPlanta()==null || irrigacao.getPlantio().getTotalPlanta().intValue() == 0?new BigDecimal(1):new BigDecimal(irrigacao.getPlantio().getTotalPlanta());
		BigDecimal espE = irrigacao.getPlantio().getEspE().equals(0.0)?new BigDecimal(1):new BigDecimal(irrigacao.getPlantio().getEspE());

		BigDecimal gotejoPlanta = nEmissores.divide(nPlantas, BigDecimalUtil.MC_RUP);
		BigDecimal x = itn.divide(irrigacao.getEficiencia(), BigDecimalUtil.MC_RUP);
		BigDecimal vazaoTotal = irrigacao.getPlantio().getFormula().getVazao().multiply(gotejoPlanta);
		BigDecimal espacoPlanta = espPlantas.multiply(espFilaPlantas);
		//Calculo do La - Lamina aplicada por hora
		BigDecimal la = vazaoTotal.divide(espacoPlanta, BigDecimalUtil.MC_RUP);
		memoria = memoria + " La = (qG * Np) / (Sp * Sr) = " + formatter.format(irrigacao.getPlantio().getFormula().getVazao()) + " * " + formatter.format(gotejoPlanta) + "  / (" + formatter.format(espPlantas) + " * " + formatter.format(espFilaPlantas) + ") = " + formatter.format(la) + "(La) \n ";  
		//Calculo do Pam
		BigDecimal dw2 = espE.multiply(espE);
		BigDecimal am = (new BigDecimal(Math.PI)).multiply(dw2);
		BigDecimal z = espacoPlanta.multiply(new BigDecimal(4));
		BigDecimal pamParcial = am.divide(z, BigDecimalUtil.MC_RUP);
		BigDecimal pamParcial2 = pamParcial.multiply(new BigDecimal(100));
		BigDecimal pam = gotejoPlanta.multiply(pamParcial2);
		memoria = memoria + "PAM = Np * ((Pi * (Eg * Eg)) / (4 * (Sp * Sr)) * 100 = " + formatter.format(gotejoPlanta) + " * (3,14 * (" + formatter.format(espE) + " * " + formatter.format(espE) + ") / (4 * " + formatter.format(espPlantas.setScale(2, BigDecimal.ROUND_UP)) + " * " + formatter.format(espFilaPlantas.setScale(2, BigDecimal.ROUND_UP)) + ")) * 100 =  "  + formatter.format(pam.setScale(2, BigDecimal.ROUND_UP)) + "mm \n ";  
		//Calculo do Lb -Lamina bruta por hora
		BigDecimal lbParcial = pam.divide(new BigDecimal(100), BigDecimalUtil.MC_RUP);
		BigDecimal lb = x.multiply(lbParcial);
		memoria = memoria + "Lb = ((Etc - Pe)/Ea) * PAM/100 = ((" + formatter.format(irrigacao.getEtc()) + " - " + formatter.format(irrigacao.getChuva()) + ")/ " + formatter.format(irrigacao.getEficiencia().setScale(2, BigDecimal.ROUND_UP)) + ") * " + formatter.format(pam.setScale(2, BigDecimal.ROUND_UP)) + "/100 = " + formatter.format(lb.setScale(2, BigDecimal.ROUND_UP)) + "mm \n ";  
		BigDecimal tih = lb.divide(la, BigDecimalUtil.MC_RUP);
		//Em minutos
		irrigacao.setTempoIrrigacao(tih.multiply(new BigDecimal(60)));
		memoria = memoria + "TI = (Lb/La) * 60 = (" + formatter.format(lb.setScale(2, BigDecimal.ROUND_UP)) + " / " + formatter.format(la.setScale(2, BigDecimal.ROUND_UP)) + ") * 60 = " + formatter.format(irrigacao.getTempoIrrigacao()) + "min \n ";
		//Legenda das Siglas 
		memoria = memoria + "Legendas: \n Ea : Efici�ncia de Aplica��o \n Etc : Evapotranspira��o da Cultura (mm.dia-1) \n La : L�mina Aplicada \n Lb : L�mina Bruta (mm.dia-1) \n Np : N�mero de Plantas \n PAM : Porcentagem de �rea Molhada (%) \n Pe : Precipita��o Efetiva (mm.dia-1) \n Pi : 3,14 \n qG : Vaz�o do Gotejador \n Se : Espa�amento entre emissores (m2) \n Sp : Espa�amento entre Plantas na mesma Fila de Plantio (m) \n Sr : Espa�amento entre Filas de Plantas (m) \n TI : Tempo de Irriga��o \n  ";
		irrigacao.setMemoriaCalculo(memoria);
		return irrigacao;
	}

	private Irrigacao calculoGotejoFaixaPam(Irrigacao irrigacao) {
		DecimalFormatSymbols symbols = new DecimalFormatSymbols();
	    symbols.setDecimalSeparator(',');
	    DecimalFormat formatter = new DecimalFormat("0.00", symbols);

	    String memoria = " Gotejo - Faixa Molhada - PAM \n "; 
		BigDecimal itn  = irrigacao.getEtc().subtract(irrigacao.getChuva());
		BigDecimal espPlantas = irrigacao.getPlantio().getEspacamento1().equals(0.0)?new BigDecimal(1):new BigDecimal(irrigacao.getPlantio().getEspacamento1());
		BigDecimal espFilaPlantas = irrigacao.getPlantio().getEspacamento2().equals(0.0)?new BigDecimal(1):new BigDecimal(irrigacao.getPlantio().getEspacamento2());
		BigDecimal nEmissores = irrigacao.getPlantio().getTotalEmissores()==null || irrigacao.getPlantio().getTotalEmissores().intValue() == 0?new BigDecimal(1):new BigDecimal(irrigacao.getPlantio().getTotalEmissores());
		BigDecimal nPlantas = irrigacao.getPlantio().getTotalPlanta()==null || irrigacao.getPlantio().getTotalPlanta().intValue() == 0?new BigDecimal(1):new BigDecimal(irrigacao.getPlantio().getTotalPlanta());
		
		BigDecimal espE = irrigacao.getPlantio().getEspE().equals(0.0)?new BigDecimal(1):new BigDecimal(irrigacao.getPlantio().getEspE());
		BigDecimal espLE= irrigacao.getPlantio().getEspLE().equals(0.0)?new BigDecimal(1):new BigDecimal(irrigacao.getPlantio().getEspLE());
		BigDecimal gotejoPlanta = nEmissores.divide(nPlantas, BigDecimalUtil.MC_RUP);
		BigDecimal x = itn.divide(irrigacao.getEficiencia(), BigDecimalUtil.MC_RUP);
		BigDecimal vazaoTotal = irrigacao.getPlantio().getFormula().getVazao().multiply(gotejoPlanta);
		BigDecimal espacaoPlanta = espPlantas.multiply(espFilaPlantas);
		//Calculo do La - Lamina aplicada por hora
		BigDecimal dw2 = espE.multiply(espLE);
		BigDecimal la = vazaoTotal.divide(dw2, BigDecimalUtil.MC_RUP);
		memoria = memoria + " La = (qG * Np) / (El * Eg) = (" + formatter.format(irrigacao.getPlantio().getFormula().getVazao()) + " * " + formatter.format(gotejoPlanta) + ") / (" + formatter.format(espLE) + " * " + formatter.format(espE) + ") = " + formatter.format(la) + "(La) \n ";  
		//Calculo do Pam
		BigDecimal dw3 = espE.multiply(espE);
		BigDecimal am = (new BigDecimal(Math.PI)).multiply(dw3);
		BigDecimal z = espacaoPlanta.multiply(new BigDecimal(4));
		BigDecimal pamParcial = am.divide(z, BigDecimalUtil.MC_RUP);
		BigDecimal pamParcial2 = pamParcial.multiply(new BigDecimal(100));
		BigDecimal pam = gotejoPlanta.multiply(pamParcial2);
		memoria = memoria + "PAM = Np * ((Pi * (Eg * Eg)) / (4 * (Sp * Sr)) * 100 = " + formatter.format(gotejoPlanta) + " * (3,14 * (" + formatter.format(espE) + " * " + formatter.format(espE) + ") / (4 * " + formatter.format(espPlantas) + " * " + formatter.format(espFilaPlantas) + ")) * 100 =  "  + formatter.format(pam) + "mm \n ";  
		//Calculo do Lb -Lamina bruta por hora
		BigDecimal lbParcial = pam.divide(new BigDecimal(100), BigDecimalUtil.MC_RUP);
		BigDecimal lb = x.multiply(lbParcial);
		memoria = memoria + "Lb = ((Etc - Pe)/Ea) * PAM/100 = ((" + formatter.format(irrigacao.getEtc()) + " - " + formatter.format(irrigacao.getChuva()) + ")/ " + formatter.format(irrigacao.getEficiencia()) + ") * " + formatter.format(pam) + "/100 = " + formatter.format(lb) + "mm \n ";  
		BigDecimal tih = lb.divide(la, BigDecimalUtil.MC_RUP);
		//Em minutos
		irrigacao.setTempoIrrigacao(tih.multiply(new BigDecimal(60)));
		memoria = memoria + "TI = (Lb/La) * 60 = (" + formatter.format(lb) + " / " + formatter.format(la) + ") * 60 = " + formatter.format(irrigacao.getTempoIrrigacao()) + "min \n ";
		//Legenda das Siglas 
		memoria = memoria + "Legendas: \n Ea : Efici�ncia de Aplica��o \n Etc : Evapotranspira��o da Cultura (mm.dia-1) \n La : L�mina Aplicada (mm.dia-1) \n Lb : L�mina Bruta (mm.dia-1) \n Np : N�mero de Plantas \n PAM : Porcentagem de �rea Molhada (%) \n Pe : Precipita��o Efetiva (mm.dia-1) \n Pi : 3,14 \n qG : Vaz�o do Gotejador \n Se : Espa�amento entre emissores (m2) \n Sle : Espa�amento entre Linhas de Emissores (m) \n Sp : Espa�amento entre Plantas da mesma Fila de Plantio (m) \n Sr : Espa�amento entre Filas de Plantas (m) \n TI : Tempo de Irriga��o \n  ";
		irrigacao.setMemoriaCalculo(memoria);
		return irrigacao;
	}

	private Irrigacao calculoGotejoFaixaKl(Irrigacao irrigacao) {
		DecimalFormatSymbols symbols = new DecimalFormatSymbols();
	    symbols.setDecimalSeparator(',');
	    DecimalFormat formatter = new DecimalFormat("0.00", symbols);

	    irrigacao.setMemoriaCalculo("Gotejo - Faixa Molhada - KL \n ");
		BigDecimal kl = calculoKl(irrigacao);
		String memoria = irrigacao.getMemoriaCalculo();
		
		BigDecimal irn1 = irrigacao.getEtc().multiply(kl);
		BigDecimal irn2  = irn1.subtract(irrigacao.getChuva());
		BigDecimal irn  = irn2.multiply(new BigDecimal(irrigacao.getPlantio().getTurnoRega()));
		memoria = memoria + " \n IRN = ((Kl * Etc) - Pe) * Tr = (("  
		+ formatter.format(kl.setScale(2, BigDecimal.ROUND_UP)) + " * " + formatter.format(irrigacao.getEtc().setScale(2, BigDecimal.ROUND_UP))+ ") - " + formatter.format(irrigacao.getChuva().setScale(2, BigDecimal.ROUND_UP)) + ") * " + formatter.format(irrigacao.getPlantio().getTurnoRega()) + 
		" = (" + formatter.format(irn1.setScale(2, BigDecimal.ROUND_UP)) + " - " + formatter.format(irrigacao.getChuva()) + ") * " + formatter.format(irrigacao.getPlantio().getTurnoRega()) + " = " + formatter.format(irn.setScale(2, BigDecimal.ROUND_UP)) + " mm/dia \n" ;

		BigDecimal itn = irn.divide(irrigacao.getEficiencia(), BigDecimalUtil.MC_RUP);
		memoria = memoria + "ITN = IRN / UE = " + formatter.format(irn.setScale(2, BigDecimal.ROUND_UP)) + " / " + formatter.format(irrigacao.getEficiencia()) + " = " + formatter.format(itn) + " \n" ;
		
		BigDecimal sp = irrigacao.getPlantio().getEspacamento2().equals(0.0)?new BigDecimal(1):new BigDecimal(irrigacao.getPlantio().getEspacamento2());
		BigDecimal sr = irrigacao.getPlantio().getEspacamento1().equals(0.0)?new BigDecimal(1):new BigDecimal(irrigacao.getPlantio().getEspacamento1());
		BigDecimal npm = new BigDecimal(1).divide(sp, BigDecimalUtil.MC_RUP);
		BigDecimal vtn = itn.multiply(sp).multiply(sr).multiply(npm);
		memoria = memoria + "VTN = ITN * Sp * Sr * No.Pl/m = " + formatter.format(itn) + " * " + formatter.format(sp) + " * " + formatter.format(sr) + " * " + formatter.format(npm) + " = " + formatter.format(vtn) + " L/pl.dia \n" ;
		
		BigDecimal se = irrigacao.getPlantio().getEspE().equals(0.0)?new BigDecimal(1):new BigDecimal(irrigacao.getPlantio().getEspE());
		BigDecimal nem = new BigDecimal(1).divide(se, BigDecimalUtil.MC_RUP);
		BigDecimal vazaoTotal = irrigacao.getPlantio().getFormula().getVazao().multiply(nem);
		BigDecimal ti = vtn.divide(vazaoTotal, BigDecimalUtil.MC_RUP);
		//Em minutos
		irrigacao.setTempoIrrigacao(ti.multiply(new BigDecimal(60)));
		memoria = memoria + "TI = (VTN / (Ne/m) * q) * 60 = (" + formatter.format(vtn) + " / " + formatter.format(nem) + " * " + formatter.format(irrigacao.getPlantio().getFormula().getVazao()) + ") * 60 = " + formatter.format(irrigacao.getTempoIrrigacao()) + "min/dia \n ";
		//Legenda das Siglas 
		memoria = memoria + "Legendas: \n Etc : Evapotranspira��o de Cultura \n IRN : L�mina Real Necess�ria (mm.dia-1) \n ITN : L�mina de Irriga��o Total Necess�ria \n Kl : Coeficiente de Localiza��o \n Ne/m : N�mero de Emissor por metro \n No.Pl/m : N�mero de Plantas por metro \n Pe : Precipita��o Efetiva (mm.dia-1) \n q : Vaz�o do Emissor (L.h-1) \n Sp : Espa�amento entre Plantas \n Sr : Espa�amento entre Filas de Plantas \n TI : Tempo de Irriga��o \n Tr : Turno de Rega (dia) \n UE : Uniformidade de Aplica��o \n VTN : Volume Total Necess�rio \n  ";
		irrigacao.setMemoriaCalculo(memoria);
		return irrigacao;
	}

	private Irrigacao calculoGotejoAreaCoberturaKl(Irrigacao irrigacao) {
		DecimalFormatSymbols symbols = new DecimalFormatSymbols();
	    symbols.setDecimalSeparator(',');
	    DecimalFormat formatter = new DecimalFormat("0.00", symbols);

	    irrigacao.setMemoriaCalculo("Gotejo - �rea de Cobertura - KL \n ");
		BigDecimal kl = calculoKl(irrigacao);
		String memoria = irrigacao.getMemoriaCalculo();
		
		BigDecimal irn1 = irrigacao.getEtc().multiply(kl);
		BigDecimal irn2  = irn1.subtract(irrigacao.getChuva());
		BigDecimal irn  = irn2.multiply(new BigDecimal(irrigacao.getPlantio().getTurnoRega()));
		memoria = memoria + " \n IRN = ((Kl * Etc) - Pe) * Tr = (" + formatter.format(irn1.setScale(2, BigDecimal.ROUND_UP)) + " - " + formatter.format(irrigacao.getChuva()) + ") * " + formatter.format(irrigacao.getPlantio().getTurnoRega()) + " = " + formatter.format(irn.setScale(2, BigDecimal.ROUND_UP)) + " mm/dia \n" ;

		BigDecimal itn = irn.divide(irrigacao.getEficiencia(), BigDecimalUtil.MC_RUP);
		memoria = memoria + "ITN = IRN / UE = " + formatter.format(irn.setScale(2, BigDecimal.ROUND_UP)) + " / " + formatter.format(irrigacao.getEficiencia()) + " = " + formatter.format(itn) + " \n" ;
		
		BigDecimal sp = irrigacao.getPlantio().getEspacamento2().equals(0.0)?new BigDecimal(1):new BigDecimal(irrigacao.getPlantio().getEspacamento2());
		BigDecimal sr = irrigacao.getPlantio().getEspacamento1().equals(0.0)?new BigDecimal(1):new BigDecimal(irrigacao.getPlantio().getEspacamento1());
		BigDecimal vtn = itn.multiply(sp).multiply(sr);
		memoria = memoria + "VTN = ITN * Sp * Sr = " + formatter.format(itn) + " * " + formatter.format(sp) + " * " + formatter.format(sr) + " = " + formatter.format(vtn) + " L/pl.dia \n" ;
		
		BigDecimal nEmissores = irrigacao.getPlantio().getTotalEmissores()==null || irrigacao.getPlantio().getTotalEmissores().intValue() == 0?new BigDecimal(1):new BigDecimal(irrigacao.getPlantio().getTotalEmissores());
		BigDecimal nPlantas = irrigacao.getPlantio().getTotalPlanta()==null || irrigacao.getPlantio().getTotalPlanta().intValue() == 0?new BigDecimal(1):new BigDecimal(irrigacao.getPlantio().getTotalPlanta());
		BigDecimal gotejoPlanta = nEmissores.divide(nPlantas, BigDecimalUtil.MC_RUP);
		BigDecimal vazaoTotal = irrigacao.getPlantio().getFormula().getVazao().multiply(gotejoPlanta);
		BigDecimal ti = vtn.divide(vazaoTotal, BigDecimalUtil.MC_RUP);
		//Em minutos
		irrigacao.setTempoIrrigacao(ti.multiply(new BigDecimal(60)));
		memoria = memoria + "TI = (VTN / (Ng/Pl) * q) * 60 = (" + formatter.format(vtn) + " / " + formatter.format(gotejoPlanta) + " * " + formatter.format(irrigacao.getPlantio().getFormula().getVazao()) + ") * 60 = " + formatter.format(irrigacao.getTempoIrrigacao()) + "min/dia \n ";
		//Legenda das Siglas 
		memoria = memoria + "Legendas: \n Etc : Evapotranspira��o de Cultura \n IRN : L�mina Real Necess�ria (mm.dia-1) \n ITN : L�mina de Irriga��o Total Necess�ria (mm.dia-1) \n Kl : Coeficiente de Localiza��o \n Ne/Pl : Numero de emissores por Planta \n Pe : Precipita Efetiva (mm.dia-1) \n q : Vaz�o do emissor (L.h-1) \n Sp : Espa�amento entre Plantas \n Sr : Espa�amento entre Filas de Plantas \n TI : Tempo de Irriga��o \n Tr : Turno de Rega (dia) \n UE : Uniformidade de Aplica��o \n VTN : Volume Total Necess�rio (L.Planta-1.dia) \n  ";
		irrigacao.setMemoriaCalculo(memoria);
		return irrigacao;
	}

	private BigDecimal calculoKl(Irrigacao irrigacao) {
		DecimalFormatSymbols symbols = new DecimalFormatSymbols();
	    symbols.setDecimalSeparator(',');
	    DecimalFormat formatter = new DecimalFormat("0.00", symbols);
	      
		String memoria = irrigacao.getMemoriaCalculo();
		BigDecimal Kl = new BigDecimal(0);
		//Calculo da Porcentagem de �rea Coberta
		memoria = memoria + "PC-Porcentagem de �rea Coberta: \n "; 
		BigDecimal diametro = irrigacao.getPlantio().getFormula().getDiametro()==null || irrigacao.getPlantio().getFormula().getDiametro().equals(new BigDecimal(0)) ? new BigDecimal(1) : irrigacao.getPlantio().getFormula().getDiametro();

		BigDecimal d2 = diametro.pow(2);
		BigDecimal area = d2.divide(new BigDecimal(4), BigDecimalUtil.MC_RUP);
		BigDecimal dividendo = (new BigDecimal(Math.PI)).multiply(area);
		BigDecimal divisor = new BigDecimal(irrigacao.getPlantio().getEspacamento1()).multiply(new BigDecimal(irrigacao.getPlantio().getEspacamento2()));
		try {
			BigDecimal PC = dividendo.divide(divisor, BigDecimalUtil.MC_RUP);
			BigDecimal PCperc = PC.multiply(new BigDecimal(100));
			memoria = memoria + "PC = ((PI * (D2/4)) / Sr * Sp) * 100 = ((" + formatter.format(new BigDecimal(Math.PI).setScale(2, BigDecimal.ROUND_UP)) + " * (" + formatter.format(d2.setScale(2, BigDecimal.ROUND_UP)) + "/4)) / " + formatter.format(irrigacao.getPlantio().getEspacamento1()) + " * " + formatter.format(irrigacao.getPlantio().getEspacamento2()) + ") * 100  = (" + formatter.format(dividendo.setScale(5, BigDecimal.ROUND_UP)) + " / " + formatter.format(divisor.setScale(2, BigDecimal.ROUND_UP)) + " ) * 100 = " + formatter.format(PCperc.setScale(5, BigDecimal.ROUND_UP)) + " \n "; 
			
			if (irrigacao.getPlantio().getTipoCalculoKL() == null || irrigacao.getPlantio().getTipoCalculoKL().equals(TipoCalculoKL.KellerBliesnet)) {
				memoria = memoria + "KL-Coeficiente de Localiza��o K&B: \n ";
				double sqrt = Math.sqrt(PCperc.doubleValue());
				Kl = new BigDecimal(0.1).multiply(new BigDecimal(sqrt));
				memoria = memoria + "KL = 0,1 	* SQRT(PC) = 0,1 * " + formatter.format(sqrt) + " = " + formatter.format(Kl.setScale(2, BigDecimal.ROUND_UP)) +  " \n";
			} else {
				memoria = memoria + "KL-Coeficiente de Localiza��o F&G: \n ";
				BigDecimal a = PCperc.divide(new BigDecimal(100), BigDecimalUtil.MC_RUP);
				BigDecimal b = new BigDecimal(1).subtract(a);
				BigDecimal c = new BigDecimal(0.5).multiply(b);
				Kl = a.add(c); 
				memoria = memoria + "KL = PC/100 + 0,5 * (1 - PC/100) = " + formatter.format(a.setScale(2, BigDecimal.ROUND_UP)) + " + 0,5 * (1 - " + formatter.format(a.setScale(2, BigDecimal.ROUND_UP)) + ") = " + formatter.format(Kl.setScale(2, BigDecimal.ROUND_UP)) +  " \n" ;
			}
			irrigacao.setMemoriaCalculo(memoria);
			
		} catch (ArithmeticException e) {
			System.out.println("Erro AE:" + e.getMessage());
		}
		return Kl;
	}
	
	@Override
	public int enviarTIGeral(){
	
		List<Irrigacao> irrigacoes = pesquisarPorData(new Date());
		List<Irrigacao> irrigacoesIrrigante = null;
		String celularLido = "";
		String celular = "";
		Long idIrriganteLido = null;
		Long idIrrigante = 0L;
		Long idVariedadeLido = null;
		Long idVariedade = 0L;
		String msg = "";
		int qtdMsg=0;
		Irrigante irrigante = null;
		String tempo = "";
		String tempoLido = "";
		boolean temIrrigacao = false;
		boolean primeiroSetor = true;
		
		//Percorrendo as irriga��es que est�o em ordem de telefone, seguido pelo lote, tempo de irrigacao, variedade e setor
		for (Irrigacao i : irrigacoes) {
			//Iniciando variaveis de checagem
			celularLido = i.getPlantio().getSetor().getIrrigante().getCelular().replace("(", "").replace(")", "").replace("-", "");
			//celularLido = "8588186358";
			tempoLido = i.getTempo();
			if (!tempoLido.equals("N�o irrigar")) {
				temIrrigacao = true;
			}			
			idIrriganteLido = i.getPlantio().getSetor().getIrrigante().getId();
			idVariedadeLido = i.getPlantio().getCultivo().getVariedade().getId();
			
			//Se o irrigante n�o deseja receber email, n�o gera mensagem
			//Se n�o tiver celular, ou se n�o for para enviarSMS ou se o tempo for superior a 240min, n�o gerar mensagem. 
			if ( !i.getPlantio().getSetor().getIrrigante().getEnviarSMS() || !i.getPlantio().getEnviarSMS() || celularLido.isEmpty() || !i.getTemIrrigacao()) {
				continue;
			}
			
			//Se mudar o celular
			if (!celular.equals(celularLido)) {
				//limpar o tempo
				tempo = "";
				//Se ja tiver mensagem preparada deve enviar ela e preparar um nova.
				if (!msg.isEmpty()) {
					//Se n�o tiver irriga��o, terminar de montar a mensagem.
					if (!temIrrigacao) {
					 	Acesso acesso = null;
					 	try {
							acesso = acessoService.pesquisarUltimo(i.getPlantio().getSetor().getIrrigante().getUsuario().getUsername());
						} catch (NoResultException e) {
						}
						String msgAcesso = "";
						if (acesso != null ) {
							Calendar data = new GregorianCalendar();
							data.setTime(acesso.getData());
							msgAcesso = "Seu �ltimo acesso foi em:" + data.get(Calendar.DAY_OF_MONTH) + "/" + (data.get(Calendar.MONTH)+1) + "/" + data.get(Calendar.YEAR) + " as " + data.get(Calendar.HOUR_OF_DAY) + ":" + data.get(Calendar.MINUTE);
						} else {
							msgAcesso = "Voc� ainda n�o visitou sua p�gina.";
						}
						if ((msg + " n�o precisa irrigar. " + msgAcesso).length() > MAX_CARACTERES) {
							msg = msg + " n�o precisa irrigar.";
						}else{
							msg = msg + " n�o precisa irrigar. " + msgAcesso;
						}
					}
					qtdMsg++;
					enviar(msg, celular, irrigante, irrigacoesIrrigante);
				}
			   	//tel = "558588186358";
				//Pegando celular Lido para depois verificar se mudou.
				celular = celularLido;
				//Mensagem inicial
				msg = "SAI-INFORMA:";
				//Iniciar lista com as irriga��es desse irrigante.
				irrigacoesIrrigante = new ArrayList<Irrigacao>();

			}
			
			//Adicionando irriga��o para depois alterar o indicador que o sms foi enviado na hora de enviar a mensagem.
			irrigacoesIrrigante.add(i);
			//Verificar se mudou o lote
			if (idIrrigante != idIrriganteLido) {
				//Pegando o irrigante em quest�o para gravar na hora de enviar a mensagem
				irrigante = i.getPlantio().getSetor().getIrrigante();
				idVariedade = 0L;
				idIrrigante = irrigante.getId();
				if (temIrrigacao) {
					//Se o tamanho da mensagem for ultrapassar os maxCaracteres caracteres ent�o devemos enviar a msg
					if ((msg + " Lote:" + (i.getPlantio().getSetor().getIrrigante().getLocalizacao().length()>6?i.getPlantio().getSetor().getIrrigante().getLocalizacao().substring(0,6):i.getPlantio().getSetor().getIrrigante().getLocalizacao()) + ".").length() > MAX_CARACTERES) {
						qtdMsg++;
						enviar(msg, celular, irrigante, irrigacoesIrrigante);
						irrigacoesIrrigante = new ArrayList<Irrigacao>();
						msg = "SAI-INFORMA(cont.):";
					}
					//Acrescenta na mensagem o lote
					msg = msg + " Lote:" + (i.getPlantio().getSetor().getIrrigante().getLocalizacao().length()>6?i.getPlantio().getSetor().getIrrigante().getLocalizacao().substring(0,6):i.getPlantio().getSetor().getIrrigante().getLocalizacao() + ".");
				}else{
					msg = msg + i.getPlantio().getSetor().getIrrigante().getLocalizacao()+ ",";
				}
			}
			//Verifica se mudou o tempo
			if (temIrrigacao && !tempo.equals(tempoLido)){
				tempo = tempoLido;
				idVariedade = 0L;
				if ((msg  + " TI:" + tempo).length() > MAX_CARACTERES) {
					qtdMsg++;
					enviar(msg, celular, irrigante, irrigacoesIrrigante);
					irrigacoesIrrigante = new ArrayList<Irrigacao>();
					msg = "SAI-INFORMA(cont.): Lote:" + (i.getPlantio().getSetor().getIrrigante().getLocalizacao().length()>6?i.getPlantio().getSetor().getIrrigante().getLocalizacao().substring(0,6):i.getPlantio().getSetor().getIrrigante().getLocalizacao());
				}
				msg = msg + " TI:" + tempo;
			}

			if (temIrrigacao) {
				if (idVariedade != idVariedadeLido) {
					primeiroSetor = true;
					idVariedade = idVariedadeLido;
					if ((msg + " " + trata((i.getPlantio().getCultivo().getCultura().getNome().split(" "))[0] + " " + (i.getPlantio().getCultivo().getVariedade().getNome().split(" "))[0]) + ":" ).length() > MAX_CARACTERES) {
						qtdMsg++;
						enviar(msg, celular, irrigante, irrigacoesIrrigante);
						irrigacoesIrrigante = new ArrayList<Irrigacao>();
						msg = "SAI-INFORMA(cont.): Lote:" + (i.getPlantio().getSetor().getIrrigante().getLocalizacao().length()>6?i.getPlantio().getSetor().getIrrigante().getLocalizacao().substring(0,6):i.getPlantio().getSetor().getIrrigante().getLocalizacao()+ "\n");
						msg = msg + tempo + ".";
					}
					msg = msg + " " + trata((i.getPlantio().getCultivo().getCultura().getNome().split(" "))[0] + " " + (i.getPlantio().getCultivo().getVariedade().getNome().split(" "))[0] + ":") ;
				}
				if(primeiroSetor){
					primeiroSetor = false;
					if ((msg + "S-" + i.getPlantio().getSetor().getIdentificacao()).length() > MAX_CARACTERES) {
						qtdMsg++;
						enviar(msg, celular, irrigante, irrigacoesIrrigante);
						irrigacoesIrrigante = new ArrayList<Irrigacao>();
						msg = "SAI-INFORMA(cont.): Lote:" + (i.getPlantio().getSetor().getIrrigante().getLocalizacao().length()>6?i.getPlantio().getSetor().getIrrigante().getLocalizacao().substring(0,6):i.getPlantio().getSetor().getIrrigante().getLocalizacao())+ "\n";
						msg = msg + tempo + ".";
						msg = msg + " " + trata((i.getPlantio().getCultivo().getCultura().getNome().split(" "))[0] + " " + (i.getPlantio().getCultivo().getVariedade().getNome().split(" "))[0] + ":");
					}
					msg = msg + "S-" + i.getPlantio().getSetor().getIdentificacao();
				}else{
					if ((msg + "," + i.getPlantio().getSetor().getIdentificacao()).length() > MAX_CARACTERES) {
						qtdMsg++;
						enviar(msg, celular, irrigante, irrigacoesIrrigante);
						irrigacoesIrrigante = new ArrayList<Irrigacao>();
						msg = "SAI-INFORMA(cont.): Lote:" + (i.getPlantio().getSetor().getIrrigante().getLocalizacao().length()>6?i.getPlantio().getSetor().getIrrigante().getLocalizacao().substring(0,6):i.getPlantio().getSetor().getIrrigante().getLocalizacao()) + "\n";
						msg = msg + tempo + ".";
						msg = msg + " " + trata((i.getPlantio().getCultivo().getCultura().getNome().split(" "))[0] + " " + (i.getPlantio().getCultivo().getVariedade().getNome().split(" "))[0] + ":");
					}
					msg = msg + "," + i.getPlantio().getSetor().getIdentificacao();
				}
			}
			qtdMsg++;
		}
		
		System.out.println("Total msg = " + qtdMsg);
		if(!msg.isEmpty()){
			enviar(msg, celular, irrigante, irrigacoesIrrigante);
			irrigacoesIrrigante = new ArrayList<Irrigacao>();
		}
		
		return qtdMsg;
	}		
	
	@Override
	public int enviarEmailTI() {
		
		List<Irrigacao> irrigacoes = pesquisarPorData(new Date());
		List<Irrigacao> irrigacoesIrrigante = null;
		String emailLido = "";
		String email = "";
		Long idIrriganteLido = null;
		Long idIrrigante = 0L;
		Long idVariedadeLido = null;
		Long idVariedade = 0L;
		String msg = "";
		int qtdMsg=0;
		Irrigante irrigante = null;
		String tempo = "";
		String tempoLido = "";
		boolean temIrrigacao = false;
		boolean primeiroSetor = true;
		
		//Percorrendo as irriga��es que est�o em ordem de telefone, seguido pelo lote, tempo de irrigacao, variedade e setor
		for (Irrigacao i : irrigacoes) {
			//Iniciando variaveis de checagem
			emailLido = i.getPlantio().getSetor().getIrrigante().getUsuario().getEmail();
			tempoLido = i.getTempo();
			if (!tempoLido.equals("N�o irrigar")) {
				temIrrigacao = true;
			}
			idIrriganteLido = i.getPlantio().getSetor().getIrrigante().getId();
			idVariedadeLido = i.getPlantio().getCultivo().getVariedade().getId();
			
			//Se o irrigante n�o deseja receber email, n�o gera mensagem
			//Se n�o tiver celular, ou se n�o for para enviarSMS ou se o tempo for superior a 240min, n�o gerar mensagem. 
			if ( !i.getPlantio().getSetor().getIrrigante().getEnviarEmail() || !i.getPlantio().getEnviarEmail() || emailLido.isEmpty() || !i.getTemIrrigacao()) {
				continue;
			}
			
			//Se mudar o celular
			if (!email.equals(emailLido)) {
				//limpar o tempo
				tempo = "";
				//Se ja tiver mensagem preparada deve enviar ela e preparar um nova.
				if (!msg.isEmpty()) {
					//Se n�o tiver irriga��o, terminar de montar a mensagem.
					if (!temIrrigacao) {
					 	Acesso acesso = null;
					 	try {
							acesso = acessoService.pesquisarUltimo(i.getPlantio().getSetor().getIrrigante().getUsuario().getUsername());
						} catch (NoResultException e) {
						}
						String msgAcesso = "";
						if (acesso != null ) {
							Calendar data = new GregorianCalendar();
							data.setTime(acesso.getData());
							msgAcesso = "Seu �ltimo acesso foi em:" + data.get(Calendar.DAY_OF_MONTH) + "/" + (data.get(Calendar.MONTH)+1) + "/" + data.get(Calendar.YEAR) + " as " + data.get(Calendar.HOUR_OF_DAY) + ":" + data.get(Calendar.MINUTE);
						} else {
							msgAcesso = "Voc� ainda n�o visitou sua p�gina.";
						}
						
						msg = msg + " n�o precisa irrigar. " + msgAcesso + " \n";
					}
					qtdMsg++;
					enviarEmail(msg, email, irrigante, irrigacoesIrrigante);
				}
			   	//email = "rpf1404@gmail.com";
				//Pegando celular Lido para depois verificar se mudou.
				email = emailLido;
				//Mensagem inicial
				msg = "SAI-INFORMA:";
				//Iniciar lista com as irriga��es desse irrigante.
				irrigacoesIrrigante = new ArrayList<Irrigacao>();

			}
			
			//Adicionando irriga��o para depois alterar o indicador que o sms foi enviado na hora de enviar a mensagem.
			irrigacoesIrrigante.add(i);
			//Verificar se mudou o lote
			if (idIrrigante != idIrriganteLido) {
				//Pegando o irrigante em quest�o para gravar na hora de enviar a mensagem
				irrigante = i.getPlantio().getSetor().getIrrigante();
				idVariedade = 0L;
				idIrrigante = irrigante.getId();
				if (temIrrigacao) {
					msg = msg + "\nLote:" + (i.getPlantio().getSetor().getIrrigante().getLocalizacao().length()>6?i.getPlantio().getSetor().getIrrigante().getLocalizacao().substring(0,6):i.getPlantio().getSetor().getIrrigante().getLocalizacao());
				}else{
					msg = msg + i.getPlantio().getSetor().getIrrigante().getLocalizacao()+ ",";
				}
			}
			//Verifica se mudou o tempo
			if (temIrrigacao && !tempo.equals(tempoLido)){
				tempo = tempoLido;
				idVariedade = 0L;
				msg = msg + "\n" + tempo;
			}

			if (temIrrigacao) {
				if (idVariedade != idVariedadeLido) {
					primeiroSetor = true;
					idVariedade = idVariedadeLido;
					msg = msg + "\n" + " " + (i.getPlantio().getCultivo().getCultura().getNome().split(" "))[0] + " " + (i.getPlantio().getCultivo().getVariedade().getNome().split(" "))[0] + ":" ;
				}
				if(primeiroSetor){
					primeiroSetor = false;
					msg = msg + "S-" + i.getPlantio().getSetor().getIdentificacao();
				}else{
					msg = msg + "," + i.getPlantio().getSetor().getIdentificacao();
				}
			}
		}
		qtdMsg++;
		System.out.println("Total email = " + qtdMsg);
		if(!msg.isEmpty()){
			enviarEmail(msg, email, irrigante, irrigacoesIrrigante);
			irrigacoesIrrigante = new ArrayList<Irrigacao>();
		}
		
		return qtdMsg;
	}
	

	public void enviarEmail(String msg, String dest, Irrigante i, List<Irrigacao> irrigacoesIrrigante) {
		List<String> destinatarios = new ArrayList<String>();
		destinatarios.add(dest);
		try {
			emailService.enviarEmail(destinatarios,"S@I - Tempo Irriga��o do dia", msg, null);
			//Gerar Mensagem
			List<Irrigante> irrigantes = new ArrayList<Irrigante>();
			irrigantes.add(i);
			Mensagem m = new Mensagem(TipoMensagem.Email, "TI", msg, null, new Date(), null, irrigantes, i.getArea().getDistrito());
			mensagemService.salvar(m);
			//Atualizar status das irriga��es do irrigante
			for (Irrigacao irr : irrigacoesIrrigante) {
				irr.setEmailEnviado(true);
				salvar(irr);
			}
		} catch (SaiException e) {
			System.out.println("Erro na hora de enviar email para:" + dest);
		}
	}

	public void enviar(String msg, String dest, Irrigante i, List<Irrigacao> irrigacoesIrrigante) {
		Long resultado = smsService.enviar(msg, dest);
		if (resultado != null) {
			//Gerar Mensagem
			List<Irrigante> irrigantes = new ArrayList<Irrigante>();
			irrigantes.add(i);
			Mensagem m = new Mensagem(TipoMensagem.SMS, "TI:"+resultado, msg, null, new Date(), null, irrigantes, i.getArea().getDistrito());
			mensagemService.salvar(m);
			//Atualizar status das irriga��es do irrigante
			for (Irrigacao irr : irrigacoesIrrigante) {
				irr.setSmsEnviado(true);
				salvar(irr);
			}
		}else{
			System.out.println("Erro ao enviar para:" + dest);
			System.out.println("Mensagem Erro:" + resultado);
		}
	}

	public String trata (String passa){  
	      passa = passa.replaceAll("[�����]","A");  
	      passa = passa.replaceAll("[�����]","a");  
	      passa = passa.replaceAll("[����]","E");  
	      passa = passa.replaceAll("[����]","e");  
	      passa = passa.replaceAll("����","I");  
	      passa = passa.replaceAll("����","i");  
	      passa = passa.replaceAll("[�����]","O");  
	      passa = passa.replaceAll("[�����]","o");  
	      passa = passa.replaceAll("[����]","U");  
	      passa = passa.replaceAll("[����]","u");  
	      passa = passa.replaceAll("�","C");  
	      passa = passa.replaceAll("�","c");   
	      passa = passa.replaceAll("[��]","y");  
	      passa = passa.replaceAll("�","Y");  
	      passa = passa.replaceAll("�","n");  
	      passa = passa.replaceAll("�","N");  
	      passa = passa.replaceAll("['<>\\|/]","");  
	      return passa;  
 }	
	
}
