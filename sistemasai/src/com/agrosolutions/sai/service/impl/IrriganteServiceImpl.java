package com.agrosolutions.sai.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.agrosolutions.sai.dao.IrriganteDAO;
import com.agrosolutions.sai.dao.SetorDAO;
import com.agrosolutions.sai.exception.SaiException;
import com.agrosolutions.sai.model.Distrito;
import com.agrosolutions.sai.model.Irrigante;
import com.agrosolutions.sai.model.Setor;
import com.agrosolutions.sai.model.Usuario;
import com.agrosolutions.sai.service.IrriganteService;
import com.agrosolutions.sai.service.UsuarioService;
import com.agrosolutions.sai.util.ResourceBundle;

@Service("irriganteService")
public class IrriganteServiceImpl implements IrriganteService {

	@Autowired
	private IrriganteDAO irriganteDAO;
	@Autowired
	private SetorDAO setorDAO;
	@Autowired
	private UsuarioService usuarioService;

	@Transactional
	public Irrigante salvar(Irrigante irrigante) {
		if (irrigante.getId() != null) {
			return irriganteDAO.salvar(irrigante);
		} else {			
			irrigante.getUsuario().setNome(irrigante.getNome());
	
			ArrayList<Distrito> ds = new ArrayList<Distrito>();
			ds.add(irrigante.getArea().getDistrito());
			irrigante.getUsuario().setDistritos(ds);
	
			irrigante.setUsuario(usuarioService.salvar(irrigante.getUsuario()));
			Irrigante i = irriganteDAO.salvar(irrigante);
			List<Setor> setores = setorDAO.pesquisar(i);
			if (irrigante.getQtdSetor()!=null && setores.isEmpty()) {
				for (int j = 1; j <= irrigante.getQtdSetor(); j++) {
					Setor setor = new Setor();
					setor.setIrrigante(i);
					setor.setIdentificacao(j);
					
					setorDAO.salvar(setor);
				}
			}
			
			return i;
		}
	}

	@Transactional
	public int quantidade(Irrigante entidade) {
		int resultado;
		try {
			validarPesquisa(entidade);
		} catch (SaiException e) {
			throw new SaiException(e.getMessages());
		}
		entidade.setSortField("");
		if (entidade.getLocalizacao() == null && entidade.getVariedade() != null) {
			resultado = irriganteDAO.qtdPorVariedade(entidade.getVariedade(), entidade.getTipoIrrigante(), entidade.getArea(), entidade.getDistritos(), null, null).intValue();
		} else {
			if (entidade.getLocalizacao() == null && entidade.getCultura() != null) {
				resultado = irriganteDAO.qtdPorCultura(entidade.getCultura(), entidade.getTipoIrrigante(), entidade.getArea(), entidade.getDistritos(), null, null).intValue();
			} else {
				if (entidade.getLotes() != null && !entidade.getLotes().isEmpty()) {
					resultado = entidade.getLotes().size();
				} else {
					resultado = irriganteDAO.quantidade(entidade);					
				}
			}
		}
		
		return resultado;
	}

	@Transactional
	public List<Irrigante> pesquisar(Irrigante entidade, Integer first, Integer pageSize) {
		List<Irrigante> irrigantes = new ArrayList<Irrigante>();
		try {
			validarPesquisa(entidade);
		} catch (SaiException e) {
			throw new SaiException(e.getMessages());
		}
		if (entidade.getLocalizacao() == null && entidade.getVariedade() != null) {
			irrigantes = irriganteDAO.pesquisarPorVariedade(entidade, first, pageSize);
		} else {
			if (entidade.getLocalizacao() == null && entidade.getCultura() != null) {
				irrigantes = irriganteDAO.pesquisarPorCultura(entidade, first, pageSize);
			} else {
				if (entidade.getLotes() != null && !entidade.getLotes().isEmpty()) {
					for (String lote : entidade.getLotes()) {
						irrigantes.add( irriganteDAO.pesquisarPorLote(lote, entidade.getDistritos()) );
					}
				} else {
					irrigantes = irriganteDAO.obterPorParametro(entidade, first, pageSize);					
				}
			}
		}
		return irrigantes;
	}
	
	private void validarPesquisa(Irrigante entidade) throws SaiException {
		List<String> messages = new ArrayList<String>();
		limparFiltros(entidade);

		if (!messages.isEmpty()) {
			throw new SaiException(messages);
		}
	}

	// Limpando atributos vazio para nulos para n�o entrar na clausula where
	public void limparFiltros(Irrigante entidade){
		if (entidade.getNome() !=null && entidade.getNome().equals("")) {
			entidade.setNome(null);
		}
		if (entidade.getCelular() !=null && entidade.getCelular().equals("")) {
			entidade.setCelular(null);
		}
		if (entidade.getLocalizacao() !=null && entidade.getLocalizacao().equals("")) {
			entidade.setLocalizacao(null);
			entidade.setLotes(null);
		}


	}

	@Transactional
	@Override
	public void excluir(Irrigante irrigante) {
		irriganteDAO.excluir(irrigante);
	}	

	@Override
	public Irrigante pesquisarPorUsuario(Usuario usu) {
		return irriganteDAO.pesquisarPorUsuario(usu);
	}
	
	@Override
	public Irrigante pesquisarPorLote(String lote, List<Distrito> distritos) {
		Irrigante irrigante = null;
		try {
			irrigante = irriganteDAO.pesquisarPorLote(lote, distritos);
		} catch (EmptyResultDataAccessException e) {
			throw new SaiException(ResourceBundle.getMessage("demandaLote")+ ".");
		}
		return irrigante;
	}

	@Override
	public List<Irrigante> pesquisarPorDistrito(List<Distrito> distritos) {
		List<Irrigante> irrigantes = null;
		try {
			irrigantes = irriganteDAO.pesquisarPorDistrito(distritos);
		} catch (EmptyResultDataAccessException e) {
			throw new SaiException(ResourceBundle.getMessage("Erro ao pesquisar irrigantes por distrito")+ ".");
		}
		return irrigantes;
	}
	
	@Override
	public List<Irrigante> pesquisarPorDistrito(List<Distrito> distritos, Integer indiceInicio, Integer quantidade) {
		List<Irrigante> irrigantes = null;
		try {
			irrigantes = irriganteDAO.pesquisarPorDistrito(distritos, indiceInicio, quantidade);
		} catch (EmptyResultDataAccessException e) {
			throw new SaiException(ResourceBundle.getMessage("Erro ao pesquisar irrigantes por distrito")+ ".");
		}
		return irrigantes;
	}
}
