package com.agrosolutions.sai.service.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.agrosolutions.sai.dao.AreaDAO;
import com.agrosolutions.sai.dao.ClimaDAO;
import com.agrosolutions.sai.dao.DemandaHidricaDAO;
import com.agrosolutions.sai.dao.IrriganteDAO;
import com.agrosolutions.sai.dao.PlantioDAO;
import com.agrosolutions.sai.model.Area;
import com.agrosolutions.sai.model.Clima;
import com.agrosolutions.sai.model.DemandaHidrica;
import com.agrosolutions.sai.model.Estadio;
import com.agrosolutions.sai.model.Plantio;
import com.agrosolutions.sai.service.DemandaHidricaService;
import com.agrosolutions.sai.util.BigDecimalUtil;

@Service("demandaHidricaService")
public class DemandaHidricaServiceImpl implements DemandaHidricaService {
	
	@Autowired
	PlantioDAO plantioDAO;
	@Autowired
	ClimaDAO climaDAO;
	@Autowired
	AreaDAO areaDAO;
	@Autowired
	DemandaHidricaDAO demandaHidricaDAO;
	@Autowired
	IrriganteDAO irriganteDAO;
	
	/**
	 * 	Percorre os plantios, calcula o kc, e monta objeto da demanda hidrica 
	 *  
	 */
	@Override
	@Transactional
	public List<DemandaHidrica> salvar(Area area) {
		List<Clima> climas = new ArrayList<Clima>();
		//List<Irrigante> irrigantes = new ArrayList<Irrigante>();
		List<DemandaHidrica> listaDemandaHidrica = new ArrayList<DemandaHidrica>();
		
		climas = climaDAO.pesquisarPorArea(area);
		for (Clima clima : climas) {
			List<Plantio> plantios = plantioDAO.ativos2(clima.getArea(), clima.getData());
			for (Plantio plantio : plantios) {
				System.out.println(plantio.getCultivo().getId());
				System.out.println(plantio.getCultivo().getIrrigante().getNome());
				System.out.println(plantio.getCultivo().getCultura().getNome()+"--"+plantio.getCultivo().getVariedade().getNome());
				
				BigDecimal kc;
				//if (plantio.getCultivo().getCultura().getNome().equalsIgnoreCase("Goiaba") || plantio.getCultivo().getCultura().getNome().equalsIgnoreCase("Manga") || plantio.getCultivo().getCultura().getNome().equalsIgnoreCase("Palma ") || plantio.getCultivo().getCultura().getNome().equalsIgnoreCase("Laranja") || plantio.getCultivo().getCultura().getNome().equalsIgnoreCase("Tamarindo") || plantio.getCultivo().getCultura().getNome().equalsIgnoreCase("Camar�o") || plantio.getCultivo().getCultura().getNome().equalsIgnoreCase("Uva")) {
				if (plantio.getCultivo().getCultura().getNome().equalsIgnoreCase("Melancia") || plantio.getCultivo().getCultura().getNome().equalsIgnoreCase("Uva") || plantio.getCultivo().getCultura().getNome().equalsIgnoreCase("Tamarindo") || plantio.getCultivo().getCultura().getNome().equalsIgnoreCase("Camar�o") ) {
					continue;
					//kc = new BigDecimal(0.84).setScale(2, RoundingMode.FLOOR);				
				} else {
					kc = calculaKc(plantio, clima).setScale(2, RoundingMode.FLOOR);						
				}
				
				DemandaHidrica demandaHidrica = new DemandaHidrica(clima.getData(), clima, plantio, clima.getEtoCalculado(), kc, null, plantio.getCultivo().getIrrigante());
				demandaHidrica.setEtc(demandaHidrica.getEtc().setScale(2, RoundingMode.FLOOR));
				demandaHidricaDAO.salvar(demandaHidrica);
				listaDemandaHidrica.add(demandaHidrica);
			}
//		List<Distrito> distritos = new ArrayList<Distrito>();
//		distritos.add( climas.getArea().getDistrito() );
//		//areas = areaDAO.pesquisar(distritos);
/*
			irrigantes = irriganteDAO.pesquisarPorArea(area);
 			for (Irrigante irrigante : irrigantes) {
				List<Plantio> plantios = plantioDAO.pesquisarPorIrrigante(irrigante);
				if ( plantios != null && !plantios.isEmpty() ) {
				}
			}*/
		}
		
		return listaDemandaHidrica;
	}


	/**
	 * 	Percorre os plantios, calcula o kc, e monta objeto da demanda hidrica 
	 *  
	 */
	@Override
	@Transactional
	public void salvarDemandaHidrica(Clima clima) {
		List<Plantio> plantios = plantioDAO.ativos(clima.getArea(), clima.getData());
		for (Plantio plantio : plantios) {
			BigDecimal kc;
			if (plantio.getCultivo().getCultura().getNome().equalsIgnoreCase("Tamarindo") || plantio.getCultivo().getCultura().getNome().equalsIgnoreCase("Camar�o") ) {
				continue;
			} else {
				kc = calculaKc(plantio, clima).setScale(2, RoundingMode.FLOOR);						
			}
			
			DemandaHidrica demandaHidrica = new DemandaHidrica(clima.getData(), clima, plantio, clima.getEtoCalculado(), kc, null, plantio.getCultivo().getIrrigante());
			demandaHidricaDAO.salvar(demandaHidrica);
		}
	}
	
	private BigDecimal calculaKc(Plantio plantio, Clima clima) {
		plantio.getCultivo().setDataClima(clima.getData());
		plantio.getCultivo().setUsarDataClimaParaCalcKc(true);
		Estadio estadioAtual = plantio.getCultivo().getEstadioAtual();
		Estadio estadioAnterior = plantio.getCultivo(). getEstadioAnteriorKc();
		Estadio estadioProximo = plantio.getCultivo().getEstadioProximoKc();
		if (estadioAtual == null){
			System.out.println("plantio:" + plantio.getId());
		}
		Long qtdDiasEstadio = estadioAtual.getQtdDiasAtual();
		
		
		if (estadioAtual.getKc() == null || estadioAtual.getKc().equals(new BigDecimal(0))) {
			return estadioAnterior.getKc().add((estadioProximo.getKc().subtract(estadioAnterior.getKc()).divide(new BigDecimal(estadioAtual.getQtdDias()), BigDecimalUtil.MC_RDO)).multiply(new BigDecimal(qtdDiasEstadio)));
		} else {
			return estadioAtual.getKc();
		}
	}

}
