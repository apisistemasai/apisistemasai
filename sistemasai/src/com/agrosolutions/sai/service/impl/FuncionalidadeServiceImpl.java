package com.agrosolutions.sai.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.agrosolutions.sai.dao.FuncionalidadeDAO;
import com.agrosolutions.sai.model.Funcionalidade;
import com.agrosolutions.sai.service.FuncionalidadeService;

@Service("funcionalidadeService")
public class FuncionalidadeServiceImpl implements FuncionalidadeService {

	@Autowired
	private FuncionalidadeDAO funcionalidadeDAO;

	@Transactional
	public Funcionalidade salvar(Funcionalidade funcionalidade) {
		return funcionalidadeDAO.salvar(funcionalidade);
	}

	public List<Funcionalidade> obterTodos() {
		return funcionalidadeDAO.obterTodos();
	}
}
