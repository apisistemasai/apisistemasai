package com.agrosolutions.sai.service.impl;

import java.io.InputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Scanner;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.agrosolutions.sai.dao.ClimaDAO;
import com.agrosolutions.sai.exception.SaiException;
import com.agrosolutions.sai.model.Area;
import com.agrosolutions.sai.model.Clima;
import com.agrosolutions.sai.model.Distrito;
import com.agrosolutions.sai.model.Estacao;
import com.agrosolutions.sai.service.ClimaService;
import com.agrosolutions.sai.service.DemandaHidricaService;
import com.agrosolutions.sai.service.IrrigacaoService;
import com.agrosolutions.sai.util.BigDecimalUtil;

@Service("climaService")
public class ClimaServiceImpl implements ClimaService {

	@Autowired
	private IrrigacaoService irrigacaoService;
	@Autowired
	private DemandaHidricaService demandaHidricaService;
	@Autowired
	private ClimaDAO climaDAO;

	public static Logger logger = Logger.getLogger(ClimaServiceImpl.class);

	@Override
	@Transactional
	public List<Clima> importarArquivoInmet(InputStream in, Estacao estacao) {
		List<Clima> climas = new ArrayList<Clima>();
		Clima clima = null;
		Calendar dataClima = new GregorianCalendar();
        Scanner sc = new Scanner(in);  
        sc.useDelimiter("<br>"); 
        int l = 0;  
        int qtdHora = 0;
        while (sc.hasNext()) {
        	l++;
	        String linha = sc.next();  
	        if (l>1) {
		        String [] campos = linha.replace("////", "0").split(",");
		        //Final de arquivo
		        if (linha.length() < 20) {
			        continue;
				}
	        	//Data e Hora do arquivo
				Calendar data = extraindoDataHoraInmet(campos);
				Calendar dataAnterior = new GregorianCalendar();
				dataAnterior.setTime(dataClima.getTime());
				dataAnterior.add(Calendar.DAY_OF_MONTH, -1);
				//Data do Dia vai das 16hrs do dias anterior at� as 16hrs de hoje
				if (dataClima.get(Calendar.YEAR) == data.get(Calendar.YEAR) &&
				   (dataClima.get(Calendar.MONTH) == data.get(Calendar.MONTH) || 
				    dataAnterior.get(Calendar.MONTH) == data.get(Calendar.MONTH)) &&
				   (dataClima.get(Calendar.DAY_OF_MONTH)    == data.get(Calendar.DAY_OF_MONTH) && data.get(Calendar.HOUR_OF_DAY) < 16 ||
				    dataAnterior.get(Calendar.DAY_OF_MONTH) == data.get(Calendar.DAY_OF_MONTH) && data.get(Calendar.HOUR_OF_DAY) > 14)) {
		        	qtdHora ++;
					if (clima == null) {
			        	qtdHora ++;
			        	clima = inicializaClimaInmet(dataClima, campos, data, estacao);
					} else {
						System.out.println("Acumula-" + data.get(Calendar.DAY_OF_MONTH) + "/" + data.get(Calendar.MONTH) + "/" + data.get(Calendar.YEAR) + " " + data.get(Calendar.HOUR_OF_DAY) + ":" + data.get(Calendar.MINUTE));
						acumulaClimaInmet(clima, campos);
					}
				}else{
					if (qtdHora == 24){
						System.out.println("Finaliza-" + data.get(Calendar.DAY_OF_MONTH) + "/" + data.get(Calendar.MONTH) + "/" + data.get(Calendar.YEAR) + " " + data.get(Calendar.HOUR_OF_DAY) + ":" + data.get(Calendar.MINUTE));
						clima = finalizaClimaInmet(clima, qtdHora);
						climas.add(clima);
					}
					clima = null;
					qtdHora = 1;
		        	clima = inicializaClimaInmet(dataClima, campos, data, estacao);
				}
	        }
        }
		if (qtdHora == 24){
			clima = finalizaClimaInmet(clima, qtdHora);
			climas.add(clima);
		}
		sc.close();
		return climas;
	}

	private Calendar extraindoDataHoraInmet(String[] campos) {
		Calendar data = new GregorianCalendar();
		data.set(Calendar.YEAR, Integer.parseInt(campos[1].substring(6, 10)));	
		data.set(Calendar.MONTH, Integer.parseInt(campos[1].substring(3, 5))-1);
		data.set(Calendar.DAY_OF_MONTH, Integer.parseInt(campos[1].substring(0, 2)));
		data.set(Calendar.HOUR_OF_DAY, Integer.parseInt(campos[2]));
		data.set(Calendar.MINUTE, 0);
		data.set(Calendar.SECOND, 0);
		data.add(Calendar.HOUR_OF_DAY, -3);
		System.out.println(data.get(Calendar.DAY_OF_MONTH) + "/" + (data.get(Calendar.MONTH)+1) + "/" + data.get(Calendar.YEAR) + " " + data.get(Calendar.HOUR_OF_DAY) + ":" + data.get(Calendar.MINUTE));
		return data;
	}
	private void acumulaClimaInmet(Clima clima, String[] campos) {
		clima.setTempMax(clima.getTempMax().add(new BigDecimal(Double.parseDouble(campos[4]))));
		clima.setTempMin(clima.getTempMin().add(new BigDecimal(Double.parseDouble(campos[5]))));
		clima.setUmidadeMax(clima.getUmidadeMax().add(new BigDecimal(Double.parseDouble(campos[7]))));
		clima.setUmidadeMin(clima.getUmidadeMin().add(new BigDecimal(Double.parseDouble(campos[8]))));
		clima.setOrvalhoMax(clima.getOrvalhoMax().add(new BigDecimal(Double.parseDouble(campos[10]))));
		clima.setOrvalhoMin(clima.getOrvalhoMin().add(new BigDecimal(Double.parseDouble(campos[11]))));
		clima.setPressaoMax(clima.getPressaoMax().add(new BigDecimal(Double.parseDouble(campos[13]))));
		clima.setPressaoMin(clima.getPressaoMin ().add(new BigDecimal(Double.parseDouble(campos[14]))));
		clima.setVento(clima.getVento().add(new BigDecimal(Double.parseDouble(campos[15]))));
		clima.setVentoRajada(clima.getVentoRajada().add(new BigDecimal(Double.parseDouble(campos[17]))));
		clima.setRadiacao(clima.getRadiacao().add(new BigDecimal(Double.parseDouble(campos[18]))));
		clima.setChuva(clima.getChuva().add(new BigDecimal(Double.parseDouble(campos[19]))));
	}
	private Clima inicializaClimaInmet(Calendar dataClima, String[] campos, Calendar data, Estacao estacao) {
		Clima clima;
		dataClima.setTime(data.getTime());
		dataClima.add(Calendar.DAY_OF_MONTH, 1);
		clima = new Clima(dataClima.getTime(), null, estacao);
		System.out.println("Inicio-" + data.get(Calendar.DAY_OF_MONTH) + "/" + data.get(Calendar.MONTH) + "/" + data.get(Calendar.YEAR) + " " + data.get(Calendar.HOUR_OF_DAY) + ":" + data.get(Calendar.MINUTE));

		clima.setEto(new BigDecimal(0));
		clima.setEtoCalculado(new BigDecimal(0));
		clima.setTempMax(new BigDecimal(Double.parseDouble(campos[4])));
		clima.setTempMin(new BigDecimal(Double.parseDouble(campos[5])));
		clima.setUmidadeMax(new BigDecimal(Double.parseDouble(campos[7])));
		clima.setUmidadeMin(new BigDecimal(Double.parseDouble(campos[8])));
		clima.setOrvalhoMax(new BigDecimal(Double.parseDouble(campos[10])));
		clima.setOrvalhoMin(new BigDecimal(Double.parseDouble(campos[11])));
		clima.setPressaoMax(new BigDecimal(Double.parseDouble(campos[13])));
		clima.setPressaoMin(new BigDecimal(Double.parseDouble(campos[14])));
		
		clima.setVento(new BigDecimal(Double.parseDouble(campos[15])));
		clima.setVentoRajada(new BigDecimal(Double.parseDouble(campos[17])));
		clima.setRadiacao(new BigDecimal(Double.parseDouble(campos[18])));
		clima.setChuva(new BigDecimal(Double.parseDouble(campos[19])));
		
		return clima;
	}

	private Clima finalizaClimaInmet(Clima clima, int qtdDia) {
		clima.setTempMax(clima.getTempMax().divide(new BigDecimal(qtdDia), BigDecimalUtil.MC_RUP));
		clima.setTempMin(clima.getTempMin().divide(new BigDecimal(qtdDia), BigDecimalUtil.MC_RUP));
		clima.setUmidadeMax(clima.getUmidadeMax().divide(new BigDecimal(qtdDia), BigDecimalUtil.MC_RUP));
		clima.setUmidadeMin(clima.getUmidadeMin().divide(new BigDecimal(qtdDia), BigDecimalUtil.MC_RUP));
		clima.setOrvalhoMax(clima.getOrvalhoMax().divide(new BigDecimal(qtdDia), BigDecimalUtil.MC_RUP));
		clima.setOrvalhoMin(clima.getOrvalhoMin().divide(new BigDecimal(qtdDia), BigDecimalUtil.MC_RUP));
		clima.setPressaoMax(clima.getPressaoMax().divide(new BigDecimal(qtdDia), BigDecimalUtil.MC_RUP).setScale(2, RoundingMode.FLOOR));
		clima.setPressaoMin(clima.getPressaoMin().divide(new BigDecimal(qtdDia), BigDecimalUtil.MC_RUP).setScale(2, RoundingMode.FLOOR));
		clima.setTempMed((clima.getTempMax().add(clima.getTempMin())).divide(new BigDecimal(2), BigDecimalUtil.MC_RUP));
		clima.setUmidadeMed((clima.getUmidadeMax().add(clima.getUmidadeMin())).divide(new BigDecimal(2), BigDecimalUtil.MC_RUP));
		clima.setOrvalhoMed((clima.getOrvalhoMax().add(clima.getOrvalhoMin())).divide(new BigDecimal(2), BigDecimalUtil.MC_RUP));
		clima.setPressaoMed((clima.getPressaoMax().add(clima.getPressaoMin())).divide(new BigDecimal(2), BigDecimalUtil.MC_RUP).setScale(2, RoundingMode.FLOOR));
		clima.setRadiacao(clima.getRadiacao().divide(new BigDecimal(1000), BigDecimalUtil.MC_RUP));
		clima.setVento(clima.getVento().divide(new BigDecimal(qtdDia), BigDecimalUtil.MC_RUP));
		clima.setVentoRajada(clima.getVentoRajada().divide(new BigDecimal(qtdDia), BigDecimalUtil.MC_RUP));
		clima.setRadLiquida( calcularRadLiquida(clima) );
		clima.setEtoCalculado( calcularETo(clima) );
		clima.setChuvaEfetiva(calcularChuvaEfetiva(clima));
		
		return clima;
	}
	
	private BigDecimal calcularRadLiquida(Clima clima) {
		Calendar data = new GregorianCalendar();
		data.setTime(clima.getData());
		System.out.println("Inicio cal Radia��o-" + data.get(Calendar.DAY_OF_MONTH) + "/" + (data.get(Calendar.MONTH)+1) + "/" + data.get(Calendar.YEAR) + " " + data.get(Calendar.HOUR_OF_DAY) + ":" + data.get(Calendar.MINUTE));
		
		//Calculo da radia��o liquida
		BigDecimal rns = new BigDecimal(0.77).multiply(clima.getRadiacao());
		System.out.println("rns="+rns);
		//Trocar essa latitude -3.12083 para o que ta no cadastro da esta��o
		BigDecimal latRadiano = new BigDecimal(-3.12083).multiply(new BigDecimal(0.01745));
		System.out.println("latRadiano="+latRadiano);
		//Troca a altitude para o que ta no cadastro
		BigDecimal altitude = new BigDecimal(76);
		System.out.println("altitude="+altitude);

		//Calculo do Juliano
		BigDecimal j1 = new BigDecimal(data.get(Calendar.MONTH) + 1).divide(new BigDecimal(9), BigDecimalUtil.MC_RUP);
		BigDecimal j2 = new BigDecimal(275).multiply(j1);
		BigDecimal j3 = j2.subtract(new BigDecimal(30));
		BigDecimal j4 = j3.add(new BigDecimal(data.get(Calendar.DAY_OF_MONTH)));
		Integer j = j4.intValue() - 2;
		
		if ((data.get(Calendar.MONTH) + 1) < 3){
			j = j + 2;
		}else{
			if (isAnoBissexto(data.get(Calendar.YEAR)) && (data.get(Calendar.MONTH) + 1) > 2) {
				j = j + 1;
			}
		}
		System.out.println("j="+j);
		//Calculo da distancia relativa do sol / terra
		double Dr = 1 + 0.033 * Math.cos(0.0172 * j);
		System.out.println("Dr="+Dr);
		
		//Calculo da inclina��o Solar
		BigDecimal inclinacaoSolar = new BigDecimal(0.409).multiply(new BigDecimal(Math.sin((0.0172 * j) - 1.39)));
		System.out.println("inclinacaoSolar="+inclinacaoSolar);

		//Calculo do angulo solar
		double tan1 = Math.tan(latRadiano.doubleValue())*-1;
		double tan2 = Math.tan(inclinacaoSolar.doubleValue());
		double anguloSolar = Math.acos(tan1 * tan2);  
		System.out.println("anguloSolar="+anguloSolar);

		//Calculo da Radia��o Extraterrestre
		double d1 = Math.sin(latRadiano.doubleValue());
		System.out.println("d1="+d1);
		double d2 = Math.sin(inclinacaoSolar.doubleValue());
		System.out.println("d2="+d2);

		double d3 = Math.cos(latRadiano.doubleValue());
		System.out.println("d3="+d3);
		double d4 = Math.cos(inclinacaoSolar.doubleValue());
		System.out.println("d4="+d4);
		double d5 = Math.cos(anguloSolar);
		System.out.println("d5="+d5);
		
		double p1 = anguloSolar * d1 * d2;
		System.out.println("p1="+p1);
		double p2 = d3 * d4 * d5;
		System.out.println("p2="+p2);
		
		double p = p1 + p2;
		System.out.println("p="+p);
		
		BigDecimal Ra = new BigDecimal(37.6).multiply(new BigDecimal(Dr).multiply(new BigDecimal(p)));
		System.out.println("Ra="+Ra);

		//Calculo da radia��o solar de curto e comprimento de ondas
		double Rso2 = 0.75 + (0.00002 + altitude.doubleValue()); 
		BigDecimal Rso = new BigDecimal(Rso2).multiply(Ra);
		System.out.println("Rso="+Rso);
		
		//Calculo da Nebulosidade
		BigDecimal f1 = clima.getRadiacao().divide(Rso, BigDecimalUtil.MC_RUP);
		BigDecimal f = (new BigDecimal(1.35).multiply(f1)).add(new BigDecimal(0.35));
		System.out.println("f="+f);
		
		//Calculo da EaMax
		BigDecimal divEaMax1 = new BigDecimal(17.27).multiply(clima.getTempMax());
		BigDecimal divEaMax2 = new BigDecimal(237.3).add(clima.getTempMax());
		BigDecimal eaMax = new BigDecimal(0.611).multiply(new BigDecimal(10)).pow((divEaMax1.divide(divEaMax2, BigDecimalUtil.MC_RUP)).intValue());
		
		//Calculo da EaMin
		BigDecimal divEaMin1 = new BigDecimal(17.27).multiply(clima.getTempMin());
		BigDecimal divEaMin2 = new BigDecimal(237.3).add(clima.getTempMin());
		BigDecimal eaMin = new BigDecimal(0.611).multiply(new BigDecimal(10)).pow((divEaMin1.divide(divEaMin2, BigDecimalUtil.MC_RUP)).intValue());
		

		//Calculo da Ed
		//BigDecimal divOrv1 = new BigDecimal(17.27).multiply(clima.getOrvalhoMed());
		//BigDecimal divOrv2 = new BigDecimal(237.3).add(clima.getOrvalhoMed());
		//BigDecimal ed = new BigDecimal(0.611).multiply(new BigDecimal(10)).pow((divOrv1.divide(divOrv2, BigDecimalUtil.MC_RUP)).intValue());
		//System.out.println("ed="+ed);
		BigDecimal ed1 = eaMin.multiply(clima.getUmidadeMax(), BigDecimalUtil.MC_RUP).divide(new BigDecimal(100));
		BigDecimal ed2 = eaMax.multiply(clima.getUmidadeMin(), BigDecimalUtil.MC_RUP).divide(new BigDecimal(100));
		BigDecimal ed = ed1.add(ed2).divide(new BigDecimal(2));

		//Calculo Emissividade l�quida da superficie
		double sqrt = Math.sqrt(ed.doubleValue());
		BigDecimal el1 = new BigDecimal(0.14).multiply(new BigDecimal(sqrt));
		BigDecimal el = new BigDecimal(0.34).subtract(el1);
		System.out.println("el="+el);
		
		//Calculo da temperatura absoluta m�xima de �rea
		BigDecimal tMax = clima.getTempMax().add(new BigDecimal(273));
		
		//Calculo da temperatura absoluta minima de �rea
		BigDecimal tMin = clima.getTempMin().add(new BigDecimal(273));
		
		//Calculo da Radia��o Global Liquida
		BigDecimal rnl1 = tMax.pow(4);
		BigDecimal rnl2 = tMin.pow(4);
		double stefanBoltzmann = 0.0000000049;
		BigDecimal rnl3 = (rnl1.add(rnl2)).divide(new BigDecimal(2), BigDecimalUtil.MC_RUP); 
		System.out.println("rnl1="+rnl1);
		System.out.println("rnl2="+rnl2);
		System.out.println("rnl3="+rnl3);

		BigDecimal rnl = f.multiply(el).multiply(new BigDecimal(stefanBoltzmann)).multiply(rnl3);
		BigDecimal radL = rnl.add(rns);
		System.out.println("rnl="+rnl);
		//Se a radi��o liquida for maior que a global, devemos usar a radi��o global.
		if(clima.getRadiacao().compareTo(radL) <= 0){
			radL =  clima.getRadiacao();
		}else{
			BigDecimal x = new BigDecimal(100).multiply(radL);
			BigDecimal y = x.divide(clima.getRadiacao(), BigDecimalUtil.MC_RUP);
			BigDecimal percDif = new BigDecimal(100).subtract(y);
			if (percDif.compareTo(new BigDecimal(25))> 0) {
				BigDecimal x1 = clima.getRadiacao().multiply(new BigDecimal(18));
				BigDecimal y1 = x1.divide(new BigDecimal(100), BigDecimalUtil.MC_RUP);
				radL = clima.getRadiacao().subtract(y1);
			}
		}
		System.out.println("rnlFinal="+rnl);
		return radL;
	}

	@Transactional
	public Clima salvar(Clima clima) {
		try {
			validarClima(clima);
		} catch (SaiException e) {
			throw new SaiException(e.getMessages());
		}
		
		if ( clima.getArea() != null && clima.getArea().getGerarTI() ) {
			irrigacaoService.calcularTempoIrrigacao(clima);
		}
		
		return climaDAO.salvar(clima);
	}
	
	@Transactional
	@Override
	public Clima salvarBacia(Clima clima) {
		try {
			validarClima(clima);
		} catch (SaiException e) {
			throw new SaiException(e.getMessages());
		}
		
		if ( clima.getArea() != null ) {
			demandaHidricaService.salvarDemandaHidrica(clima);
		}
		
		return climaDAO.salvar(clima);
	}

	private void validarClima(Clima clima) throws SaiException {
		List<String> messages = new ArrayList<String>();
		
		if (clima.getId() == null){
			try {
				Clima c = climaDAO.pesquisarPorData(clima.getArea(), clima.getData());
				if (c != null){
					messages.add("Clima j� existe nessa data!");
				}
			} catch (EmptyResultDataAccessException e) {
			}
		}

		if (clima.getEtoCalculado()== null){
			messages.add("Eto deve ser informada!");
		}
		
		if (!messages.isEmpty()) {
			throw new SaiException(messages);
		}
	}
	
	public List<Clima> pesquisarPorArea(Area area) {
		return climaDAO.pesquisarPorArea(area);
	}

	public Clima ultimoPorArea(Area area) {
		Clima ultimoPorArea = null;
		try {
			ultimoPorArea = climaDAO.ultimoPorArea(area);
		} catch (EmptyResultDataAccessException e) {
		}
		return ultimoPorArea;
	}

	@Override
	public Clima ultimoPorDistrito(Distrito distrito) {
		Clima clima = new Clima();
		List<Clima> ultimos = new ArrayList<Clima>();
		try {
			ultimos = climaDAO.ultimoPorDistrito(distrito);
			if (ultimos.size() > 0) {
				for (Clima c : ultimos) {
					clima.setChuva(clima.getChuva().add(c.getChuva()));
					clima.setData(c.getData());
					clima.setEtoCalculado(clima.getEtoCalculado().add(c.getEtoCalculado()));
					clima.setUmidadeMed(clima.getUmidadeMed().add(c.getUmidadeMed()));
					clima.setRadiacao(clima.getRadiacao().add(c.getRadiacao()));
					clima.setTempMax(clima.getTempMax().add(c.getTempMax()));
					clima.setTempMin(clima.getTempMin().add(c.getTempMin()));
					clima.setVento(clima.getVento().add(c.getVento()));
				}
				clima.setEtoCalculado(clima.getEtoCalculado().divide(new BigDecimal(ultimos.size()), BigDecimalUtil.MC_RUP));
				clima.setUmidadeMed(clima.getUmidadeMed().divide(new BigDecimal(ultimos.size()), BigDecimalUtil.MC_RUP));
				clima.setRadiacao(clima.getRadiacao().divide(new BigDecimal(ultimos.size()), BigDecimalUtil.MC_RUP));
				clima.setTempMax(clima.getTempMax().divide(new BigDecimal(ultimos.size()), BigDecimalUtil.MC_RUP));
				clima.setTempMin(clima.getTempMin().divide(new BigDecimal(ultimos.size()), BigDecimalUtil.MC_RUP));
				clima.setVento(clima.getVento().divide(new BigDecimal(ultimos.size()), BigDecimalUtil.MC_RUP));
			}
		} catch (EmptyResultDataAccessException e) {
		}
		return clima;
	}

	public Clima pesquisarPorData(Area area, Date data) {
		return climaDAO.pesquisarPorData(area, data);
	}

	public List<Clima> pesquisarPorPeriodo(Area area, Date dataIni, Date dataFim) {
		return climaDAO.pesquisarPorPeriodo(area, dataIni, dataFim);
	}

	@Override
	public List<Clima> estacaoPorPeriodo(Estacao estacao, Date dataIni, Date dataFim) {
		return climaDAO.estacaoPorPeriodo(estacao, dataIni, dataFim);
	}
	public static boolean isAnoBissexto(int ano) {  
        if ( ( ano % 4 == 0 && ano % 100 != 0 ) || ( ano % 400 == 0 ) ){  
            return true;  
        }  
        else{  
            return false;  
        }  
	}
	
	@Override
	@Transactional
	public List<Clima> importarArquivoCampbell(InputStream in, Estacao estacao) {
		List<Clima> climas = new ArrayList<Clima>();
		Clima clima = null;
		Calendar dataClima = new GregorianCalendar();
        Scanner sc = new Scanner(in);  
        int l = 0;  
        int qtdDia = 0;
        while (sc.hasNext()) {
        	l++;
	        String linha = sc.nextLine();  
	        String [] campos = linha.split(",");
	        if (l>4) {
	        	//Data e Hora
				Calendar data = extraindoDataHora(campos);
				Calendar dataAnterior = new GregorianCalendar();
				dataAnterior.setTime(dataClima.getTime());
				dataAnterior.add(Calendar.DAY_OF_MONTH, -1);
				//Data do Dia vai das 16hrs do dias anterior at� as 16hrs de hoje
				if (dataClima.get(Calendar.YEAR) == data.get(Calendar.YEAR) &&
				   (dataClima.get(Calendar.MONTH) == data.get(Calendar.MONTH) || 
				    dataAnterior.get(Calendar.MONTH) == data.get(Calendar.MONTH)) &&
				   ((dataClima.get(Calendar.DAY_OF_MONTH) == data.get(Calendar.DAY_OF_MONTH) && data.get(Calendar.HOUR_OF_DAY) < 15 || (data.get(Calendar.HOUR_OF_DAY) == 15 && data.get(Calendar.MINUTE) == 0)) ||
				    (dataAnterior.get(Calendar.DAY_OF_MONTH) == data.get(Calendar.DAY_OF_MONTH) && data.get(Calendar.HOUR_OF_DAY) > 15))) {
					qtdDia ++;
					if (clima == null) {
						qtdDia ++;
			        	clima = inicializaClima(dataClima, campos, data, estacao);
					} else {
						System.out.println("Acumula-" + data.get(Calendar.DAY_OF_MONTH) + "/" + data.get(Calendar.MONTH) + "/" + data.get(Calendar.YEAR) + " " + data.get(Calendar.HOUR_OF_DAY) + ":" + data.get(Calendar.MINUTE));
						acumulaClima(clima, campos);
					}
				}else{
					if (qtdDia == 24){
						System.out.println("Finaliza-" + data.get(Calendar.DAY_OF_MONTH) + "/" + data.get(Calendar.MONTH) + "/" + data.get(Calendar.YEAR) + " " + data.get(Calendar.HOUR_OF_DAY) + ":" + data.get(Calendar.MINUTE));
						clima = finalizaClima(clima, qtdDia);
						climas.add(clima);
					}
					clima = null;
					qtdDia = 1;
		        	clima = inicializaClima(dataClima, campos, data, estacao);
				}
			}
        }
		if (qtdDia == 24){
			clima = finalizaClima(clima, qtdDia);
			climas.add(clima);
		}
		sc.close();
		return climas;
	}
	
	private Clima finalizaClima(Clima clima, int qtdDia) {
		clima.setTempMed(clima.getTempMed().divide(new BigDecimal(qtdDia), BigDecimalUtil.MC_RUP));
		clima.setUmidadeMed(clima.getUmidadeMed().divide(new BigDecimal(qtdDia), BigDecimalUtil.MC_RUP));
		clima.setRadiacao(clima.getRadiacao().divide(new BigDecimal(1000), BigDecimalUtil.MC_RUP));
		clima.setVento(clima.getVento().divide(new BigDecimal(qtdDia), BigDecimalUtil.MC_RUP));
		clima.setVentoRajada(clima.getVentoRajada().divide(new BigDecimal(qtdDia), BigDecimalUtil.MC_RUP));
		clima.setEtoCalculado(clima.getEto());
		BigDecimal dividendo = new BigDecimal(100).subtract(clima.getUmidadeMed());
		BigDecimal resultado = dividendo.divide(new BigDecimal(5), BigDecimalUtil.MC_RUP);
		clima.setOrvalhoMed(clima.getTempMed().subtract(resultado));
		clima.setRadLiquida(calcularRadLiquida(clima));
		clima.setEtoCalculado(calcularETo(clima));

		return clima;
	}
	private Clima inicializaClima(Calendar dataClima, String[] campos, Calendar data, Estacao estacao) {
		Clima clima;
		dataClima.setTime(data.getTime());
		dataClima.add(Calendar.DAY_OF_MONTH, +1);

		clima = new Clima(dataClima.getTime(), null, estacao);
		System.out.println("Inicio-" + data.get(Calendar.DAY_OF_MONTH) + "/" + data.get(Calendar.MONTH) + "/" + data.get(Calendar.YEAR) + " " + data.get(Calendar.HOUR_OF_DAY) + ":" + data.get(Calendar.MINUTE));

		if (!campos[13].equals("\"NAN\"")) {
			clima.setEto(new BigDecimal(Double.parseDouble(campos[13])));
		}else{
			clima.setEto(new BigDecimal(0));
		}

		clima.setEtoCalculado(new BigDecimal(0));

		if (!campos[9].equals("\"NAN\"")) {
			clima.setTempMed(new BigDecimal(Double.parseDouble(campos[9])));
		}else{
			clima.setTempMed(new BigDecimal(0));
		}
		if (!campos[2].equals("\"NAN\"")) {
			clima.setTempMax(new BigDecimal(Double.parseDouble(campos[2])));
			clima.setTempMin(new BigDecimal(Double.parseDouble(campos[2])));
		}else{
			clima.setTempMin(new BigDecimal(0));
			clima.setTempMax(new BigDecimal(0));
		}

		clima.setRadLiquida(new BigDecimal(0));

		if (!campos[7].equals("\"NAN\"")) {
			clima.setRadiacao(new BigDecimal(Double.parseDouble(campos[7])));
		}else{
			clima.setRadiacao(new BigDecimal(0));
		}
		if (!campos[10].equals("\"NAN\"")) {
			clima.setUmidadeMed(new BigDecimal(Double.parseDouble(campos[10])));
			clima.setUmidadeMax(new BigDecimal(Double.parseDouble(campos[10])));
			clima.setUmidadeMin(new BigDecimal(Double.parseDouble(campos[10])));
		}else{
			clima.setUmidadeMed(new BigDecimal(0));
			clima.setUmidadeMax(new BigDecimal(0));
			clima.setUmidadeMin(new BigDecimal(0));
		}
		if (!campos[11].equals("\"NAN\"")) {
			clima.setVento(new BigDecimal(Double.parseDouble(campos[11])));
		}else{
			clima.setVento(new BigDecimal(0));
		}
		if (!campos[14].equals("\"NAN\"")) {
			clima.setVentoRajada(new BigDecimal(Double.parseDouble(campos[14])));
		}else{
			clima.setVentoRajada(new BigDecimal(0));
		}
		if (!campos[8].equals("\"NAN\"")) {
			clima.setChuva(new BigDecimal(Double.parseDouble(campos[8])));
		}else{
			clima.setChuva(new BigDecimal(0));
		}
		
		return clima;
	}

	private void acumulaClima(Clima clima, String[] campos) {
		if (!campos[13].equals("\"NAN\"")) {
			clima.setEto(clima.getEto().add(new BigDecimal(Double.parseDouble(campos[13]))));
		}
		if (!campos[9].equals("\"NAN\"")) {
			clima.setTempMed(clima.getTempMed().add(new BigDecimal(Double.parseDouble(campos[9]))));
		}
		if (!campos[2].equals("\"NAN\"")) {
			if (clima.getTempMax().compareTo(new BigDecimal(Double.parseDouble(campos[2]))) < 0 ) {
				clima.setTempMax(new BigDecimal(Double.parseDouble(campos[2])));
			}
			if (clima.getTempMin().compareTo(new BigDecimal(Double.parseDouble(campos[2]))) > 0 ) {
				clima.setTempMin(new BigDecimal(Double.parseDouble(campos[2])));
			}
		}
		if (!campos[7].equals("\"NAN\"")) {
			clima.setRadiacao(clima.getRadiacao().add(new BigDecimal(Double.parseDouble(campos[7]))));
		}
		if (!campos[10].equals("\"NAN\"")) {
			clima.setUmidadeMed(clima.getUmidadeMed().add(new BigDecimal(Double.parseDouble(campos[10]))));
			if (clima.getUmidadeMax().compareTo(new BigDecimal(Double.parseDouble(campos[10]))) < 0 ) {
				clima.setUmidadeMax(new BigDecimal(Double.parseDouble(campos[10])));
			}
			if (clima.getUmidadeMin().compareTo(new BigDecimal(Double.parseDouble(campos[10]))) > 0 ) {
				clima.setUmidadeMin(new BigDecimal(Double.parseDouble(campos[10])));
			}
		}
		if (!campos[11].equals("\"NAN\"")) {
			clima.setVento(clima.getVento().add(new BigDecimal(Double.parseDouble(campos[11]))));
		}
		if (!campos[14].equals("\"NAN\"")) {
			clima.setVentoRajada(clima.getVentoRajada().add(new BigDecimal(Double.parseDouble(campos[14]))));
		}
		if (!campos[8].equals("\"NAN\"")) {
			clima.setChuva(clima.getChuva().add(new BigDecimal(Double.parseDouble(campos[8]))));
		}
	}
	private Calendar extraindoDataHora(String[] campos) {
		Calendar data = new GregorianCalendar();
		data.set(Calendar.YEAR, Integer.parseInt(campos[0].substring(1, 5)));
		data.set(Calendar.MONTH, Integer.parseInt(campos[0].substring(6, 8))-1);
		data.set(Calendar.DAY_OF_MONTH, Integer.parseInt(campos[0].substring(9, 11)));
		data.set(Calendar.HOUR_OF_DAY, Integer.parseInt(campos[0].substring(12, 14)));
		data.set(Calendar.MINUTE, Integer.parseInt(campos[0].substring(15, 17)));
		data.set(Calendar.SECOND, Integer.parseInt(campos[0].substring(18, 20)));
		return data;
	}
	private BigDecimal calcularETo(Clima clima) {
		//Calculo da Ea
		BigDecimal divEa1 = new BigDecimal(17.27).multiply(clima.getTempMed());
		BigDecimal divEa2 = new BigDecimal(237.3).add(clima.getTempMed());
		BigDecimal ea = new BigDecimal(0.611).multiply(new BigDecimal(10)).pow((divEa1.divide(divEa2, BigDecimalUtil.MC_RUP)).intValue());

		//Calculo do declive da curva de press�o de vapor
		BigDecimal divA1 = new BigDecimal(17.27).multiply(clima.getTempMed());
		BigDecimal divA2 = new BigDecimal(237.3).add(clima.getTempMed());
		BigDecimal divA3 = (clima.getTempMed().add(new BigDecimal(237.3))).pow(2);
		BigDecimal declive = (new BigDecimal(2504).multiply(new BigDecimal(10)).pow((divA1.divide(divA2, BigDecimalUtil.MC_RUP)).intValue())).divide(divA3, BigDecimalUtil.MC_RUP);
		
		//Calculo da constante psicrom�trica (y)
		BigDecimal p = clima.getPressaoMed().divide(new BigDecimal(10), BigDecimalUtil.MC_RUP);
		BigDecimal y = new BigDecimal(0.00163).multiply(p.divide(new BigDecimal(2.45), BigDecimalUtil.MC_RUP));
		
		//Calculo da Ed
		BigDecimal divOrv1 = new BigDecimal(17.27).multiply(clima.getOrvalhoMed());
		BigDecimal divOrv2 = new BigDecimal(237.3).add(clima.getOrvalhoMed());
		BigDecimal ed = new BigDecimal(0.611).multiply(new BigDecimal(10)).pow((divOrv1.divide(divOrv2, BigDecimalUtil.MC_RUP)).intValue());

		//Calculo da Eto
		BigDecimal deficitPressao = ea.subtract(ed);
		BigDecimal Eto1 = new BigDecimal(0.408).multiply(declive.multiply(clima.getRadLiquida()));
		BigDecimal Eto2 = y.multiply((new BigDecimal(900)).divide(clima.getTempMed().add(new BigDecimal(273)), BigDecimalUtil.MC_RUP));
		BigDecimal Eto3 = clima.getVento().multiply(deficitPressao);
		BigDecimal EtoDividendo = Eto1.add(Eto2.multiply(Eto3));
		BigDecimal Eto4 = new BigDecimal(1).add(new BigDecimal(0.34).multiply(clima.getVento()));
		BigDecimal EtoDivisor = declive.add(y.multiply(Eto4));
		BigDecimal Eto = EtoDividendo.divide(EtoDivisor, BigDecimalUtil.MC_RUP);
		return Eto;
	}
	
	@Override
	@Transactional
	public List<Clima> importarArquivoCampbellDibau(InputStream in, Estacao estacao) {
		List<Clima> climas = new ArrayList<Clima>();
		Clima clima = null;
		Calendar dataClima = new GregorianCalendar();
        Scanner sc = new Scanner(in);  
        int l = 0;  
        int qtdDia = 0;
        int qtdHora = 0;
        while (sc.hasNext()) {
        	l++;
	        String linha = sc.nextLine();  
	        String [] campos = linha.split(",");
	        if (l>4) {
	        	//Data e Hora do arquivo
				Calendar data = extraindoDataHoraDibau(campos);
				Calendar dataAnterior = new GregorianCalendar();
				dataAnterior.setTime(dataClima.getTime());
				dataAnterior.add(Calendar.DAY_OF_MONTH, -1);
				//Data do Dia vai das 16hrs do dias anterior at� as 16hrs de hoje
				if (dataClima.get(Calendar.YEAR) == data.get(Calendar.YEAR) &&
				   (dataClima.get(Calendar.MONTH) == data.get(Calendar.MONTH) || 
				    dataAnterior.get(Calendar.MONTH) == data.get(Calendar.MONTH)) &&
				   (dataClima.get(Calendar.DAY_OF_MONTH)    == data.get(Calendar.DAY_OF_MONTH) && data.get(Calendar.HOUR_OF_DAY) < 16 ||
				    dataAnterior.get(Calendar.DAY_OF_MONTH) == data.get(Calendar.DAY_OF_MONTH) && data.get(Calendar.HOUR_OF_DAY) > 14)) {
		        	qtdHora ++;
					if (clima == null) {
			        	qtdHora ++;
			        	clima = inicializaClimaDibau(dataClima, campos, data, estacao);
					} else {
						System.out.println("Acumula-" + data.get(Calendar.DAY_OF_MONTH) + "/" + data.get(Calendar.MONTH) + "/" + data.get(Calendar.YEAR) + " " + data.get(Calendar.HOUR_OF_DAY) + ":" + data.get(Calendar.MINUTE));
						acumulaClimaDibau(clima, campos);
					}
				}else{
					if (qtdHora == 24){
						System.out.println("Finaliza-" + data.get(Calendar.DAY_OF_MONTH) + "/" + data.get(Calendar.MONTH) + "/" + data.get(Calendar.YEAR) + " " + data.get(Calendar.HOUR_OF_DAY) + ":" + data.get(Calendar.MINUTE));
						clima = finalizaClimaDibau(clima, qtdHora, estacao);
						climas.add(clima);
					}
					clima = null;
					qtdHora = 1;
		        	clima = inicializaClimaDibau(dataClima, campos, data, estacao);
				}
			}
        }
		if (qtdDia == 24){
			clima = finalizaClimaDibau(clima, qtdDia, estacao);
			climas.add(clima);
		}
		sc.close();
		return climas;
	}

	@Override
	@Transactional
	public List<Clima> importarArquivoFunceme(InputStream in, Estacao estacao) {
		List<Clima> climas = new ArrayList<Clima>();
		Clima clima = null;
		Calendar dataClima = new GregorianCalendar();
        Scanner sc = new Scanner(in);  
        int qtdHora = 0;
        while (sc.hasNext()) {
	        String linha = sc.nextLine();  
			logger.debug("importarArquivoFunceme:" + linha);

	        String [] campos = linha.split(";");
        	//Data e Hora do arquivo
			Calendar data = extraindoDataHoraFunceme(campos);
			//Data do Dia vai das 16hrs do dias anterior at� as 16hrs de hoje
			if (dataClima.get(Calendar.YEAR) == data.get(Calendar.YEAR) &&
			    dataClima.get(Calendar.MONTH) == data.get(Calendar.MONTH) &&
			    dataClima.get(Calendar.DAY_OF_MONTH)    == data.get(Calendar.DAY_OF_MONTH)) {
	        	qtdHora ++;
				if (clima == null) {
		        	clima = inicializaClimaFunceme(dataClima, campos, data, estacao);
				} else {
					System.out.println("Acumula-" + data.get(Calendar.DAY_OF_MONTH) + "/" + (data.get(Calendar.MONTH)+1) + "/" + data.get(Calendar.YEAR) + " " + data.get(Calendar.HOUR_OF_DAY) + ":" + data.get(Calendar.MINUTE));
					acumulaClimaFunceme(clima, campos);
				}
			}else{
				if (qtdHora == 24){
					System.out.println("Finaliza-" + data.get(Calendar.DAY_OF_MONTH) + "/" + (data.get(Calendar.MONTH)+1) + "/" + data.get(Calendar.YEAR) + " " + data.get(Calendar.HOUR_OF_DAY) + ":" + data.get(Calendar.MINUTE));
					clima = finalizaClimaInmet(clima, qtdHora);
					climas.add(clima);
				}
				clima = null;
				qtdHora = 1;
	        	clima = inicializaClimaFunceme(dataClima, campos, data, estacao);
			}
        }
		if (qtdHora == 24){
			clima = finalizaClimaInmet(clima, qtdHora);
			climas.add(clima);
		}
		sc.close();
		return climas;
	}

	private Clima inicializaClimaDibau(Calendar dataClima, String[] campos, Calendar data, Estacao estacao) {
		Clima clima;
		dataClima.setTime(data.getTime());
		dataClima.add(Calendar.DAY_OF_MONTH, +1);

		clima = new Clima(dataClima.getTime(), null, estacao);
		System.out.println("Inicio-" + data.get(Calendar.DAY_OF_MONTH) + "/" + data.get(Calendar.MONTH) + "/" + data.get(Calendar.YEAR) + " " + data.get(Calendar.HOUR_OF_DAY) + ":" + data.get(Calendar.MINUTE));
		
		clima.setEtoCalculado(new BigDecimal(0));
		clima.setRadLiquida(new BigDecimal(0));
		clima.setChuva(new BigDecimal(Double.parseDouble(campos[4])));
		clima.setTempMax(new BigDecimal(Double.parseDouble(campos[5])));
		clima.setTempMin(new BigDecimal(Double.parseDouble(campos[6])));
		clima.setUmidadeMax(new BigDecimal(Double.parseDouble(campos[7])));
		clima.setUmidadeMin(new BigDecimal(Double.parseDouble(campos[8])));
		clima.setRadiacao(new BigDecimal(Double.parseDouble(campos[9])));
		clima.setTempMed(new BigDecimal(Double.parseDouble(campos[12])));
		clima.setUmidadeMed(new BigDecimal(Double.parseDouble(campos[13])));
		clima.setVento(new BigDecimal(Double.parseDouble(campos[14])));
		clima.setEto(new BigDecimal(Double.parseDouble(campos[16])));
		
		return clima;
	}

	private Clima inicializaClimaFunceme(Calendar dataClima, String[] campos, Calendar data, Estacao estacao) {
		Clima clima;
		dataClima.setTime(data.getTime());

		clima = new Clima(dataClima.getTime(), null, estacao);
		System.out.println("Inicio-" + data.get(Calendar.DAY_OF_MONTH) + "/" + (data.get(Calendar.MONTH)+1) + "/" + data.get(Calendar.YEAR) + " " + data.get(Calendar.HOUR_OF_DAY) + ":" + data.get(Calendar.MINUTE));
		
		clima.setEto(new BigDecimal(0));
		clima.setEtoCalculado(new BigDecimal(0));
		clima.setTempMax(new BigDecimal(Double.parseDouble(campos[2])));
		clima.setTempMin(new BigDecimal(Double.parseDouble(campos[3])));
		clima.setUmidadeMax(new BigDecimal(Double.parseDouble(campos[5])));
		clima.setUmidadeMin(new BigDecimal(Double.parseDouble(campos[6])));
		clima.setOrvalhoMax(new BigDecimal(Double.parseDouble(campos[8])));
		clima.setOrvalhoMin(new BigDecimal(Double.parseDouble(campos[9])));
		if (campos[11].split("\\.").length == 3){
			String[] numeros = campos[11].split("\\.");
			campos[11] = numeros[0].concat(numeros[1])+ "." + numeros[2];
		}
		clima.setPressaoMax(new BigDecimal(Double.parseDouble(campos[11])));
		
		if (campos[12].split("\\.").length == 3){
			String[] numeros = campos[12].split("\\.");
			campos[12] = numeros[0].concat(numeros[1])+ "." + numeros[2];
		}
		clima.setPressaoMin(new BigDecimal(Double.parseDouble(campos[12])));
		clima.setVento(new BigDecimal(Double.parseDouble(campos[13])));
		clima.setVentoRajada(new BigDecimal(Double.parseDouble(campos[15])));

		if (campos[16].split("\\.").length == 3){
			String[] numeros = campos[16].split("\\.");
			campos[16] = numeros[0].concat(numeros[1])+ "." + numeros[2];
		}
		clima.setRadiacao(new BigDecimal(Double.parseDouble(campos[16])));
		clima.setChuva(new BigDecimal(Double.parseDouble(campos[17])));

		return clima;
	}
	private void acumulaClimaFunceme(Clima clima, String[] campos) {
		clima.setTempMax(clima.getTempMax().add(new BigDecimal(Double.parseDouble(campos[2]))));
		clima.setTempMin(clima.getTempMin().add(new BigDecimal(Double.parseDouble(campos[3]))));
		clima.setUmidadeMax(clima.getUmidadeMax().add(new BigDecimal(Double.parseDouble(campos[5]))));
		clima.setUmidadeMin(clima.getUmidadeMin().add(new BigDecimal(Double.parseDouble(campos[6]))));
		clima.setOrvalhoMax(clima.getOrvalhoMax().add(new BigDecimal(Double.parseDouble(campos[8]))));
		clima.setOrvalhoMin(clima.getOrvalhoMin().add(new BigDecimal(Double.parseDouble(campos[9]))));
		if (campos[11].split("\\.").length == 3){
			String[] numeros = campos[11].split("\\.");
			campos[11] = numeros[0].concat(numeros[1])+ "." + numeros[2];
		}
		clima.setPressaoMax(clima.getPressaoMax().add(new BigDecimal(Double.parseDouble(campos[11]))));

		if (campos[12].split("\\.").length == 3){
			String[] numeros = campos[12].split("\\.");
			campos[12] = numeros[0].concat(numeros[1])+ "." + numeros[2];
		}
		clima.setPressaoMin(clima.getPressaoMin ().add(new BigDecimal(Double.parseDouble(campos[12]))));
		clima.setVento(clima.getVento().add(new BigDecimal(Double.parseDouble(campos[13]))));
		clima.setVentoRajada(clima.getVentoRajada().add(new BigDecimal(Double.parseDouble(campos[15]))));
		if (campos[16].split("\\.").length == 3){
			String[] numeros = campos[16].split("\\.");
			campos[16] = numeros[0].concat(numeros[1])+ "." + numeros[2];
		}
		clima.setRadiacao(clima.getRadiacao().add(new BigDecimal(Double.parseDouble(campos[16]))));
		clima.setChuva(clima.getChuva().add(new BigDecimal(Double.parseDouble(campos[17]))));
	}
	
	private void acumulaClimaDibau(Clima clima, String[] campos) {
		clima.setChuva(clima.getChuva().add(new BigDecimal(Double.parseDouble(campos[4]))));
		clima.setTempMax(clima.getTempMax().add(new BigDecimal(Double.parseDouble(campos[5]))));
		clima.setTempMin(clima.getTempMin().add(new BigDecimal(Double.parseDouble(campos[6]))));
		clima.setUmidadeMax(clima.getUmidadeMax().add(new BigDecimal(Double.parseDouble(campos[7]))));
		clima.setUmidadeMin(clima.getUmidadeMin().add(new BigDecimal(Double.parseDouble(campos[8]))));
		clima.setRadiacao(clima.getRadiacao().add(new BigDecimal(Double.parseDouble(campos[9]))));
		clima.setVento(clima.getVento().add(new BigDecimal(Double.parseDouble(campos[14]))));
	}
	
	private Clima finalizaClimaDibau(Clima clima, int qtdDia, Estacao estacao) {
		
		//clima.setPressaoMed(new BigDecimal(101.1));
		clima.setPressaoMed( calcularPressao(estacao.getAltitude()) );
		clima.setTempMax(clima.getTempMax().divide(new BigDecimal(qtdDia), BigDecimalUtil.MC_RUP));
		clima.setTempMin(clima.getTempMin().divide(new BigDecimal(qtdDia), BigDecimalUtil.MC_RUP));
		clima.setUmidadeMax(clima.getUmidadeMax().divide(new BigDecimal(qtdDia), BigDecimalUtil.MC_RUP));
		clima.setUmidadeMin(clima.getUmidadeMin().divide(new BigDecimal(qtdDia), BigDecimalUtil.MC_RUP));
		clima.setTempMed((clima.getTempMax().add(clima.getTempMin())).divide(new BigDecimal(2), BigDecimalUtil.MC_RUP));
		clima.setUmidadeMed((clima.getUmidadeMax().add(clima.getUmidadeMin())).divide(new BigDecimal(2), BigDecimalUtil.MC_RUP));
		clima.setRadiacao(clima.getRadiacao().divide(new BigDecimal(1000), BigDecimalUtil.MC_RUP));
		clima.setVento(clima.getVento().divide(new BigDecimal(qtdDia), BigDecimalUtil.MC_RUP));
		
		BigDecimal radL = calcularRadLiquida(clima);
		
		clima.setRadLiquida(radL);
		System.out.println("radLiquida="+radL);
		
		BigDecimal Eto = calcularEToDibau(clima);
		clima.setEtoCalculado(Eto);
		
		return clima;
	}
	
	
	private Calendar extraindoDataHoraDibau(String[] campos) {
		Calendar data = new GregorianCalendar();
		data.set(Calendar.YEAR, Integer.parseInt(campos[0].substring(1, 5)));
		data.set(Calendar.MONTH, Integer.parseInt(campos[0].substring(6, 8))-1);
		data.set(Calendar.DAY_OF_MONTH, Integer.parseInt(campos[0].substring(9, 11)));
		data.set(Calendar.HOUR_OF_DAY, Integer.parseInt(campos[0].substring(12, 14)));
		data.set(Calendar.MINUTE, 0);
		data.set(Calendar.SECOND, 0);
		data.add(Calendar.HOUR_OF_DAY, -3);
		System.out.println(data.get(Calendar.DAY_OF_MONTH) + "/" + (data.get(Calendar.MONTH)+1) + "/" + data.get(Calendar.YEAR) + " " + data.get(Calendar.HOUR_OF_DAY) + ":" + data.get(Calendar.MINUTE));
		
		return data;
	}
	
	private Calendar extraindoDataHoraFunceme(String[] campos) {
		Calendar data = new GregorianCalendar();
		data.set(Calendar.YEAR, Integer.parseInt(campos[0].substring(6, 10)));
		data.set(Calendar.MONTH, Integer.parseInt(campos[0].substring(3, 5))-1);
		data.set(Calendar.DAY_OF_MONTH, Integer.parseInt(campos[0].substring(0, 2)));
		data.set(Calendar.HOUR_OF_DAY, Integer.parseInt(campos[0].substring(11, 13)));
		data.set(Calendar.MINUTE, 0);
		data.set(Calendar.SECOND, 0);
		System.out.println(data.get(Calendar.DAY_OF_MONTH) + "/" + (data.get(Calendar.MONTH)+1) + "/" + data.get(Calendar.YEAR) + " " + data.get(Calendar.HOUR_OF_DAY) + ":" + data.get(Calendar.MINUTE));
		
		return data;
	}
	
	private BigDecimal calcularEToDibau(Clima clima) {
		
		//Calculo do declive da curva de press�o de vapor
		BigDecimal divA1 = new BigDecimal(17.27).multiply(clima.getTempMed());
		BigDecimal divA2 = new BigDecimal(237.3).add(clima.getTempMed());
		BigDecimal divA3 = (clima.getTempMed().add(new BigDecimal(237.3))).pow(2);
		BigDecimal declive = (new BigDecimal(2504).multiply(new BigDecimal(10)).pow((divA1.divide(divA2, BigDecimalUtil.MC_RUP)).intValue())).divide(divA3, BigDecimalUtil.MC_RUP);
		
		//Calculo da constante psicrom�trica (y)
		//Transforma em Kilo Pascal
		//BigDecimal p = clima.getPressaoMed().divide(new BigDecimal(10), BigDecimalUtil.MC_RUP);
		BigDecimal y = new BigDecimal(0.00163).multiply(clima.getPressaoMed().divide(new BigDecimal(2.45), BigDecimalUtil.MC_RUP));
		
		//Calculo da EaMax
		BigDecimal divEaMax1 = new BigDecimal(17.27).multiply(clima.getTempMax());
		BigDecimal divEaMax2 = new BigDecimal(237.3).add(clima.getTempMax());
		BigDecimal eaMax = new BigDecimal(0.611).multiply(new BigDecimal(10)).pow((divEaMax1.divide(divEaMax2, BigDecimalUtil.MC_RUP)).intValue());
		
		//Calculo da EaMin
		BigDecimal divEaMin1 = new BigDecimal(17.27).multiply(clima.getTempMin());
		BigDecimal divEaMin2 = new BigDecimal(237.3).add(clima.getTempMin());
		BigDecimal eaMin = new BigDecimal(0.611).multiply(new BigDecimal(10)).pow((divEaMin1.divide(divEaMin2, BigDecimalUtil.MC_RUP)).intValue());
		
		//Ea
		BigDecimal ea = eaMax.add(eaMin).divide(new BigDecimal(2));
		
		//Calculo da Ed
		//BigDecimal divOrv1 = new BigDecimal(17.27).multiply(clima.getOrvalhoMed());
		//BigDecimal divOrv2 = new BigDecimal(237.3).add(clima.getOrvalhoMed());
		//BigDecimal ed = new BigDecimal(0.611).multiply(new BigDecimal(10)).pow((divOrv1.divide(divOrv2, BigDecimalUtil.MC_RUP)).intValue());
		BigDecimal ed1 = eaMin.multiply(clima.getUmidadeMax(), BigDecimalUtil.MC_RUP).divide(new BigDecimal(100));
		BigDecimal ed2 = eaMax.multiply(clima.getUmidadeMin(), BigDecimalUtil.MC_RUP).divide(new BigDecimal(100));
		BigDecimal ed = ed1.add(ed2).divide(new BigDecimal(2));

		//Calculo do deficit
		BigDecimal deficitPressao = ea.subtract(ed);
		
		BigDecimal Eto1 = new BigDecimal(0.408).multiply(declive.multiply(clima.getRadLiquida()));
		BigDecimal Eto2 = y.multiply((new BigDecimal(900)).divide(clima.getTempMed().add(new BigDecimal(273)), BigDecimalUtil.MC_RUP));
		BigDecimal Eto3 = clima.getVento().multiply(deficitPressao);
		BigDecimal EtoDividendo = Eto1.add(Eto2.multiply(Eto3));
		BigDecimal Eto4 = new BigDecimal(1).add(new BigDecimal(0.34).multiply(clima.getVento()));
		BigDecimal EtoDivisor = declive.add(y.multiply(Eto4));
		BigDecimal Eto = EtoDividendo.divide(EtoDivisor, BigDecimalUtil.MC_RUP);
		
		return Eto;
	}
	
	/**
	 * F�rmula usada:
	 * 101,3.[(293-0,0065.z)/293]^5,26
	 * 
	 * Legenda da f�rmula: 
	 * 		z = altura da esta��o em rela��o ao n�vel do mar
	 * 
	 * @param altura
	 * @return
	 */
	public BigDecimal calcularPressao(BigDecimal altitudeEstacao){
		BigDecimal v1 = new BigDecimal(101.3);
		BigDecimal v2 = new BigDecimal(293);
		BigDecimal v3 = new BigDecimal(0.0065);
		BigDecimal pot = new BigDecimal(5.26);
		BigDecimal r1 = new BigDecimal(0);
		BigDecimal r2 = new BigDecimal(0);
		BigDecimal r3 = new BigDecimal(0);
		
		// (293-0,0065.z)
		r1 = v2.subtract(v3.multiply(altitudeEstacao));
		// (293-0,0065.z)/293
		r2 = r1.divide(v2, BigDecimalUtil.MC_RUP);
		// x^5,26
		r3 = new BigDecimal(Math.pow( r2.doubleValue(), pot.doubleValue() ), BigDecimalUtil.MC_RUP );
		// 101,3.(x^5,26)
		BigDecimal pressao = v1.multiply(r3);
		
		//BigDecimal pressao = v1.multiply( new BigDecimal ( Math.pow( (v2.subtract(v3).multiply(altitudeEstacao)).divide(v2, BigDecimalUtil.MC_RUP).doubleValue() , pot.doubleValue() ) ) );
		//BigDecimal pressao = v1.multiply(  ( v2.subtract(v3).multiply(altura) ).divide(v2)  );
		
		return pressao;
	}
	
	/**
	 * Chuva-(Chuva-0,2*254*(100/FatorCorr-1))^2/(Chuva+0,8*254*(100/FatorCorr-1)
	 * 
	 * @param clima
	 * @return
	 */
	private BigDecimal calcularChuvaEfetiva(Clima clima) {
		// Fator de corre��o
		BigDecimal fator =  new BigDecimal(85);
		BigDecimal chuva = clima.getChuva();
		
		// 100/FatorCorr-1
		BigDecimal v1 = new BigDecimal(100).divide( fator.subtract(new BigDecimal(1)), BigDecimalUtil.MC_RUP );
		BigDecimal v2 = new BigDecimal(0.2).multiply(new BigDecimal(254)).multiply(v1, BigDecimalUtil.MC_RUP );
		BigDecimal v3 = chuva.subtract(v2, BigDecimalUtil.MC_RUP ).pow(2);
		BigDecimal v4 = new BigDecimal(0.8).multiply(new BigDecimal(254)).multiply(v1, BigDecimalUtil.MC_RUP );
		BigDecimal v5 = chuva.add(v4, BigDecimalUtil.MC_RUP );
		BigDecimal v6 = v3.divide(v5, BigDecimalUtil.MC_RUP );
		BigDecimal precipitacaoEfetiva = chuva.subtract(v6, BigDecimalUtil.MC_RUP );
		
		if ( precipitacaoEfetiva.compareTo(new BigDecimal(0)) > 0){  
            return precipitacaoEfetiva;
        } else {
        	return new BigDecimal(0);
        }
		
	}
	
	public Clima prepararClimaCalcularEto(Clima clima) {
		clima.setTempMed((clima.getTempMax().add(clima.getTempMin())).divide(new BigDecimal(2), BigDecimalUtil.MC_RUP));
		clima.setUmidadeMed((clima.getUmidadeMax().add(clima.getUmidadeMin())).divide(new BigDecimal(2), BigDecimalUtil.MC_RUP));
		clima.setPressaoMed((clima.getPressaoMax().add(clima.getPressaoMin())).divide(new BigDecimal(2), BigDecimalUtil.MC_RUP));
		clima.setOrvalhoMed((clima.getOrvalhoMax().add(clima.getOrvalhoMin())).divide(new BigDecimal(2), BigDecimalUtil.MC_RUP));
		clima.setRadLiquida(calcularRadLiquida(clima));
		clima.setEtoCalculado(calcularETo(clima));
		clima.setChuvaEfetiva(calcularChuvaEfetiva(clima));
		
		return clima;
	}
	
}