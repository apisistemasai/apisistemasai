/*package com.agrosolutions.sai.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.agrosolutions.sai.dao.LoteDAO;
import com.agrosolutions.sai.dao.SetorDAO;
import com.agrosolutions.sai.model.Irrigante;
import com.agrosolutions.sai.model.Lote;
import com.agrosolutions.sai.model.Setor;
import com.agrosolutions.sai.service.LoteService;

@Service("loteService")
public class LoteServiceImpl implements LoteService {

	@Autowired
	private LoteDAO loteDAO;
	@Autowired
	private SetorDAO setorDAO;

	@Transactional
	public Lote salvar(Lote lote) {
		Lote l = loteDAO.salvar(lote);
		List<Setor> setores = setorDAO.pesquisar(l);
		if(lote.getQtdSetor() != null && setores.isEmpty()){
			for (int i = 1; i <= lote.getQtdSetor(); i++) {
				Setor setor = new Setor();
				setor.setLote(lote);
				setor.setIdentificacao("Setor " + i);
				
				setorDAO.salvar(setor);
			}
		}
		return l;
	}

	public List<Lote> pesquisar(Irrigante irrigante) {
		return loteDAO.pesquisar(irrigante);
	}

	@Transactional
	@Override
	public void excluir(Lote lote) {
		loteDAO.excluir(lote);
	}
}
*/