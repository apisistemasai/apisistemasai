package com.agrosolutions.sai.service.impl;

import org.hibernate.envers.RevisionListener;
import org.springframework.security.core.context.SecurityContextHolder;

import com.agrosolutions.sai.model.Revisao;
import com.agrosolutions.sai.model.Usuario;

public class RevisaoListener implements RevisionListener {

	@Override
	public void newRevision(Object revisaoObj) {
		if(SecurityContextHolder.getContext().getAuthentication() != null && SecurityContextHolder.getContext().getAuthentication().getPrincipal() != null) {
			Revisao revisao = (Revisao) revisaoObj;
			revisao.setUsuario((Usuario) SecurityContextHolder.getContext().getAuthentication().getPrincipal());
		}
	}

}
