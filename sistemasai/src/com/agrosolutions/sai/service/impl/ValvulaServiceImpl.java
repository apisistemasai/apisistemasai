package com.agrosolutions.sai.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.agrosolutions.sai.dao.ValvulaDAO;
import com.agrosolutions.sai.exception.SaiException;
import com.agrosolutions.sai.model.Fabricante;
import com.agrosolutions.sai.model.Irrigante;
import com.agrosolutions.sai.model.Valvula;
import com.agrosolutions.sai.service.ValvulaService;

@Service("valvulaService")
public class ValvulaServiceImpl implements ValvulaService {

	@Autowired
	private ValvulaDAO valvulaDAO;

	@Transactional
	public Valvula salvar(Valvula valvula) {
		return valvulaDAO.salvar(valvula);
	}

	public List<Valvula> pesquisar(Fabricante fabricante) {
		return valvulaDAO.pesquisar(fabricante);
	}
	@Override
	public List<Valvula> pesquisarPorIrrigante(Irrigante irrigante) {
		return valvulaDAO.pesquisarPorIrrigante(irrigante);
	}
	@Override
	@Transactional
	public List<Valvula> pesquisar(Valvula valvula) {
		return valvulaDAO.obterPorParametro(valvula, null, null);
	}
		
	public List<Valvula> obterTodos() {
		return valvulaDAO.obterTodos();
	}

	@Transactional
	@Override
	public void excluir(Valvula valvula) {
		valvulaDAO.excluir(valvula);
	}

	@Transactional
	@Override
	public List<Valvula> pesquisar(Valvula entidade, int first, int pageSize) {
		List<Valvula> valvulas = null;
		try {
			validarPesquisa(entidade);
		} catch (SaiException e) {
			throw new SaiException(e.getMessages());
		}
		valvulas = valvulaDAO.obterPorParametro(entidade, first, pageSize);
		return valvulas;
	}
	
	private void validarPesquisa(Valvula entidade) throws SaiException {
		List<String> messages = new ArrayList<String>();

		if (!messages.isEmpty()) {
			throw new SaiException(messages);
		}
	}

	@Transactional
	public int quantidade(Valvula entidade) {
		int resultado;
		try {
			validarPesquisa(entidade);
		} catch (SaiException e) {
			throw new SaiException(e.getMessages());
		}
		entidade.setSortField("");
		resultado = valvulaDAO.quantidade(entidade);
		return resultado;
	}
}
