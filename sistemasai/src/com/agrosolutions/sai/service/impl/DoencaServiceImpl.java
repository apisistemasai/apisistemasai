package com.agrosolutions.sai.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.agrosolutions.sai.dao.DoencaDAO;
import com.agrosolutions.sai.exception.SaiException;
import com.agrosolutions.sai.model.Doenca;
import com.agrosolutions.sai.service.DoencaService;

@Service("doencaService")
public class DoencaServiceImpl implements DoencaService {

	@Autowired
	private DoencaDAO doencaDAO;

	@Transactional
	public Doenca salvar(Doenca doenca) {
		return doencaDAO.salvar(doenca);
	}

	public List<Doenca> obterTodos() {
		return doencaDAO.obterTodos();
	}

	@Transactional
	@Override
	public void excluir(Doenca doenca) {
		doencaDAO.excluir(doenca);
	}

	@Transactional
	@Override
	public List<Doenca> pesquisar(Doenca entidade, Integer first, Integer pageSize) {
		List<Doenca> doencas = null;
		try {
		} catch (Exception e) {
			throw new SaiException(e.getMessage());
		}
		doencas = doencaDAO.obterPorParametro(entidade, first, pageSize);	
		
		return doencas;
	}
}
