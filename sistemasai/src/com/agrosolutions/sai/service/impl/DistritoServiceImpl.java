package com.agrosolutions.sai.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.agrosolutions.sai.dao.DistritoDAO;
import com.agrosolutions.sai.exception.SaiException;
import com.agrosolutions.sai.model.Distrito;
import com.agrosolutions.sai.service.DistritoService;

@Service("distritoService")
public class DistritoServiceImpl implements DistritoService {

	@Autowired
	private DistritoDAO distritoDAO;

	@Transactional
	public Distrito salvar(Distrito distrito) {
		return distritoDAO.salvar(distrito);
	}

	public List<Distrito> obterTodos(List<Distrito> distritos) {
		return distritoDAO.obterTodos(distritos);
	}
	
	@Transactional
	public void excluir(Distrito distrito) {
		distritoDAO.excluir(distrito);
	}
	
	@Transactional
	@Override
	public List<Distrito> pesquisar(Distrito entidade, Integer first,
			Integer pageSize) {
		List<Distrito> distritos = null;
		try {
		} catch (Exception e) {
			throw new SaiException(e.getMessage());
		}
		distritos = distritoDAO.obterPorParametro(entidade, first, pageSize);	
		
		return distritos;
	}
	
	@Override
	public List<Distrito> pesquisarTodos() {
		return distritoDAO.pesquisarTodos();
	}
}
