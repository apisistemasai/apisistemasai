package com.agrosolutions.sai.service;

import java.util.List;

import com.agrosolutions.sai.model.Cultura;
import com.agrosolutions.sai.model.Referencia;
import com.agrosolutions.sai.model.Variedade;

public interface ReferenciaService {
	
	Referencia salvar(Referencia variedade);
	List<Referencia> obterTodos();
	List<Referencia> pesquisar(Variedade variedade);
	void excluir(Referencia referencia);
	List<Referencia> pesquisarCultura(Cultura c);
}
