package com.agrosolutions.sai.service;

import java.util.List;

import com.agrosolutions.sai.model.Categoria;

public interface CategoriaService {
	
	Categoria salvar(Categoria c);
	List<Categoria> obterTodos();
	void excluir(Categoria c);
	List<Categoria> obterSuperiores();
	
}
