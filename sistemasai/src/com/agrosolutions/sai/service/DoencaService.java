package com.agrosolutions.sai.service;

import java.util.List;

import com.agrosolutions.sai.model.Doenca;

public interface DoencaService {
	
	Doenca salvar(Doenca doenca);
	List<Doenca> obterTodos();
	void excluir(Doenca doenca);
	List<Doenca> pesquisar(Doenca entidade, Integer first, Integer pageSize);
}
