package com.agrosolutions.sai.service;

import com.agrosolutions.sai.model.Parametro;

public interface ParametroService {
	
	Parametro pesquisarPorCodigo(String codigo);

}
