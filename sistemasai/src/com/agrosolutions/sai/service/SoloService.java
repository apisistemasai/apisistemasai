package com.agrosolutions.sai.service;

import java.util.List;

import com.agrosolutions.sai.model.Solo;

public interface SoloService {
	
	Solo salvar(Solo solo);
	List<Solo> pesquisar(String nome);
	void excluir(Solo solo);
	List<Solo> obterTodos();
	int quantidade(Solo soloBusca);
	List<Solo> pesquisar(Solo entidade, Integer first, Integer pageSize);
}
