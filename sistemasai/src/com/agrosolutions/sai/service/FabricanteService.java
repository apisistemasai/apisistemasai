package com.agrosolutions.sai.service;

import java.util.List;

import com.agrosolutions.sai.model.Fabricante;
import com.agrosolutions.sai.model.TipoFabricante;

public interface FabricanteService {
	
	Fabricante salvar(Fabricante fabricante);
	List<Fabricante> pesquisar(TipoFabricante tipo);
	void excluir(Fabricante atividade);
	List<Fabricante> pesquisar(Fabricante entidade, Integer first,Integer pageSize);
	int quantidade(Fabricante entidade);
}
