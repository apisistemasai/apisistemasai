package com.agrosolutions.sai.service;

import java.util.List;

import com.agrosolutions.sai.model.Fabricante;
import com.agrosolutions.sai.model.Irrigante;
import com.agrosolutions.sai.model.Tubo;

public interface TuboService {
	
	Tubo salvar(Tubo tubo);
	List<Tubo> pesquisar(Fabricante entidade);
	void excluir(Tubo tubo);
	List<Tubo> obterTodos();
	List<Tubo> pesquisarPorIrrigante(Irrigante irrigante);
	List<Tubo> pesquisar(Tubo tubo);
	List<Tubo> pesquisar(Tubo entidade, Integer first, Integer pageSize);
	int quantidade(Tubo tuboBusca);
}
