package com.agrosolutions.sai.service;

import java.util.List;

import com.agrosolutions.sai.model.Area;
import com.agrosolutions.sai.model.Distrito;
import com.agrosolutions.sai.model.GraficoPizza;
import com.agrosolutions.sai.model.Irrigante;
import com.agrosolutions.sai.model.Setor;
 
public interface SetorService {
	
	Setor salvar(Setor setor);
	List<Setor> pesquisar(Irrigante param);
	void excluir(Setor setor);
	List<GraficoPizza> irrigacaoPorDistrito(Distrito a);
	List<GraficoPizza> irrigacaoPorArea(Area a);
}
