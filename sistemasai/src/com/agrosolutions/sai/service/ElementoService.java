package com.agrosolutions.sai.service;

import java.util.List;

import com.agrosolutions.sai.model.Elemento;

public interface ElementoService {
	
	Elemento salvar(Elemento atividade);
	List<Elemento> obterTodos();
	void excluir(Elemento atividade);
	List<Elemento> pesquisar(Elemento entidade, Integer first, Integer pageSize);
	
}
