package com.agrosolutions.sai.service;

import java.util.List;

import com.agrosolutions.sai.model.BaciaHidrografica;

public interface BaciaHidrograficaService {
	
	BaciaHidrografica salvar(BaciaHidrografica baciaHidrografica);
	List<BaciaHidrografica> obterTodos(List<BaciaHidrografica> baciaHidrograficas);
	void excluir(BaciaHidrografica baciaHidrografica);
	List<BaciaHidrografica> pesquisar(BaciaHidrografica entidade, Integer first, Integer pageSize);
	List<BaciaHidrografica> pesquisarTodos();
}
