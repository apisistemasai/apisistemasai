package com.agrosolutions.sai.service;

import java.util.Date;
import java.util.List;

import com.agrosolutions.sai.model.Clima;
import com.agrosolutions.sai.model.Irrigacao;
import com.agrosolutions.sai.model.Irrigante;
import com.agrosolutions.sai.model.Plantio;
import com.agrosolutions.sai.model.Usuario;

public interface IrrigacaoService {
	
	Irrigacao pesquisar(Plantio plantio, Date data);

	List<Irrigacao> pesquisarPorData(Date data);

	List<Irrigacao> pesquisarPorPlantio(Plantio p);

	List<Irrigacao> pesquisarPorIrriganteData(Date data, Irrigante ir);

	void excluir(Irrigacao irrigacao);

	int quantidade(Irrigacao entidade);

	List<Irrigacao> pesquisar(Irrigacao entidade, Integer first,
			Integer pageSize);

	Irrigacao salvar(Irrigacao irrigacao);

	void enviarSMS(Irrigacao irrigacao);

	List<Irrigacao> pesquisarPorIrrigantePeriodo(Date dataIni, Date dataFim,
			Irrigante ir);

	void calcularTempoIrrigacao(Clima clima);
	
	Irrigacao checarIrrigacao(Clima clima, Plantio plantio);

	List<Irrigacao> recalcular(Plantio p);

	void deletar(Plantio p);

	void recalcular(Usuario u);

	int enviarTIGeral();

	int enviarEmailTI(); 
	
}
