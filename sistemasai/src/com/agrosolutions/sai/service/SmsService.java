package com.agrosolutions.sai.service;

import org.marre.sms.SmsException;

import com.agrosolutions.sai.exception.SaiException;


public interface SmsService {
	
	void enviarSMS(String destinatario, String msg) throws SaiException, SmsException;

	void enviarSMS2(String destinatario, String msg) throws SaiException,
			SmsException;

	String consultarSaldo() throws SaiException, SmsException;

	void enviarSMSInfoBip(String to, String msg) throws SaiException,
			SmsException;

	Long enviar(String msg, String dest);

}
