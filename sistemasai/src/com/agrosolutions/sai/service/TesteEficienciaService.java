package com.agrosolutions.sai.service;

import java.util.Date;
import java.util.List;

import com.agrosolutions.sai.model.Setor;
import com.agrosolutions.sai.model.TesteEficiencia;

public interface TesteEficienciaService {
	
	void salvar(List<TesteEficiencia> t);
	TesteEficiencia salvar(TesteEficiencia t);
	void excluir(TesteEficiencia t);
	List<TesteEficiencia> pesquisarPorSetor(Setor setor);
	TesteEficiencia pesquisarPorSetorData(Setor setor, Date data);

}
