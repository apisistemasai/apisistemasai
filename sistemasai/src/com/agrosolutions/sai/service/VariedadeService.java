package com.agrosolutions.sai.service;

import java.util.List;

import com.agrosolutions.sai.model.Cultura;
import com.agrosolutions.sai.model.Variedade;

public interface VariedadeService {
	
	Variedade salvar(Variedade cultura);
	List<Variedade> obterTodos();
	void excluir(Variedade variedade);
	List<Variedade> pesquisar(Variedade entidade,  Integer first, Integer pageSize);
	int quantidade(Variedade entidade);
	List<Variedade> pesquisar(Cultura cultura);
}
