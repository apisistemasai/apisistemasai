package com.agrosolutions.sai.service;

import java.util.Date;
import java.util.List;


import com.agrosolutions.sai.model.Area;
import com.agrosolutions.sai.model.Cultura;
import com.agrosolutions.sai.model.GraficoPizza;

public interface CulturaService {
	
	Cultura salvar(Cultura cultura); 
	List<Cultura> obterTodos();
	void excluir(Cultura cultura);
	List<GraficoPizza> culturaAtivaPorArea(Area a, Date data);
	List<Cultura> pesquisar(Cultura culturaBusca, Integer first, Integer pageSize);
	int quantidade(Cultura entidade);
 
}
