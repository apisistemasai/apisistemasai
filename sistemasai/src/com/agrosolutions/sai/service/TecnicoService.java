package com.agrosolutions.sai.service;

import java.util.List;

import com.agrosolutions.sai.model.Distrito;
import com.agrosolutions.sai.model.Tecnico;

public interface TecnicoService {
	
	Tecnico salvar(Tecnico tecnico);
	List<Tecnico> pesquisar(List<Distrito> distrito);
	List<Tecnico> obterTodos();
	void excluir(Tecnico tecnico);
	List<Tecnico> pesquisar(Tecnico tecnicoBusca, Integer first, Integer pageSize);
	int quantidade(Tecnico tecnicoBusca);
	Tecnico editar(Tecnico tecnico);
}
