package com.agrosolutions.sai.service;

import java.util.List;

import com.agrosolutions.sai.model.Perfil;
import com.agrosolutions.sai.model.TipoPerfil;

public interface PerfilService {
	
	Perfil salvar(Perfil perfil);
	List<Perfil> obterTodos();
	void excluir(Perfil perfil);
	List<Perfil> porTipo(TipoPerfil tipo);
	List<Perfil> pesquisar(Perfil entidade, Integer first, Integer pageSize);
	int quantidade(Perfil perfilBusca);
}
