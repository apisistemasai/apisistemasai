package com.agrosolutions.sai.service;

import java.io.InputStream;
import java.util.List;
import java.util.Map;

import com.agrosolutions.sai.exception.SaiException;


public interface EmailService {
	
	void enviarEmail(List<String> destinatarios, String assunto, String conteudo, Map<String, InputStream> anexos) throws SaiException;

}
