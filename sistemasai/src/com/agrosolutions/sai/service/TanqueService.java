package com.agrosolutions.sai.service;

import java.util.List;

import com.agrosolutions.sai.model.Tanque;

public interface TanqueService {
	
	Tanque salvar(Tanque tanque);
	List<Tanque> obterTodos();
	void excluir(Tanque tanque);
	List<Tanque> pesquisar(Tanque entidade, Integer first, Integer pageSize);
	
}
