package com.agrosolutions.sai.service;

import java.util.List;

import com.agrosolutions.sai.model.BaciaHidrografica;
import com.agrosolutions.sai.model.Municipio;

public interface MunicipioService {
	
	Municipio salvar(Municipio municipio);
	int quantidade(Municipio entidade);
	List<Municipio> pesquisar(Municipio entidade, Integer first, Integer pageSize);
	void excluir(Municipio municipio);
	List<Municipio> pesquisarPorBacia(List<BaciaHidrografica> distritos);
	List<Municipio> pesquisarPorBacia(List<BaciaHidrografica> distritos,
			Integer indiceInicio, Integer quantidade);
}
