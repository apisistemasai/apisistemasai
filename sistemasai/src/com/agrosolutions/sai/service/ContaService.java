package com.agrosolutions.sai.service;

import java.util.List;

import com.agrosolutions.sai.model.Conta;

public interface ContaService {
	Conta salvar(Conta c);

	List<Conta> obterTodos();

	void excluir(Conta c);

}
