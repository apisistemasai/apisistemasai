package com.agrosolutions.sai.service;

import java.util.List;

import com.agrosolutions.sai.model.Emissor;
import com.agrosolutions.sai.model.Plantio;
import com.agrosolutions.sai.model.Setor;

public interface EmissorService {
	
	Emissor salvar(Emissor emissor);
	void excluir(Emissor emissor);
	List<Emissor> pesquisarPorSetor(Setor setor);
	List<Emissor> pesquisarPorPlantio(Plantio p);
	List<Emissor> pesquisar(Emissor entidade, Integer first, Integer pageSize);
	List<Emissor> obterTodos();
	int quantidade(Emissor emissorBusca);
	List<Emissor> pesquisar(Emissor emissor);
}
