package com.agrosolutions.sai.service;

import java.util.List;

import com.agrosolutions.sai.exception.SaiException;
import com.agrosolutions.sai.model.Usuario;

public interface UsuarioService {
	
	Usuario salvar(Usuario usuario);
	
	int quantidade(Usuario usuario);
	
	List<Usuario> pesquisar(Usuario entidade, Integer first, Integer pageSize);
	
	void excluir(Usuario usuario);
	
	Usuario pesquisarPorLogin(String username);
	
	void enviarEmailSenha(Usuario usuario, String urlComplementacao) throws SaiException;
	
	public void alterarSenha(Usuario usuario);
	
	public Usuario pesquisarPorId(String id) throws SaiException;
	
	public void alterarDistritoPadrao(Usuario usuario);

	List<Usuario> pesquisarPorEmail(String email);

	List<Usuario> enviarEmailRecuperacaoLogin(List<Usuario> usuarios);

}
