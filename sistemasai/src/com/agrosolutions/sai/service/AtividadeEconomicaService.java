package com.agrosolutions.sai.service;

import java.util.List;

import com.agrosolutions.sai.model.AtividadeEconomica;
import com.agrosolutions.sai.model.BaciaHidrografica;
import com.agrosolutions.sai.model.Cultura;
import com.agrosolutions.sai.model.Municipio;
import com.agrosolutions.sai.model.Variedade;

public interface AtividadeEconomicaService {
	
	AtividadeEconomica salvar(AtividadeEconomica atividadeEconomica);
	List<AtividadeEconomica> pesquisarPorMunicipio(Municipio municipio);
	List<AtividadeEconomica> pesquisarPorBacia(BaciaHidrografica bacia);
	List<AtividadeEconomica> pesquisarTodosDeMunicipio();
	List<AtividadeEconomica> pesquisarTodosDaBacia();
	List<Variedade> pesquisarVariedades(Municipio municipio);
	List<Cultura> pesquisarCulturas(Municipio municipio);
	void excluir(AtividadeEconomica atividadeEconomica);
	List<AtividadeEconomica> pesquisarPorVariedades(Variedade variedade);
}
