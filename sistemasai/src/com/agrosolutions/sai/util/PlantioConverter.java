package com.agrosolutions.sai.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import com.agrosolutions.sai.model.Plantio;
/**
 * Converter espec�fico para sele��o com {@link Plantio}, onde o primeiro item selecionado � vazio
 * 
 * @author Raphael.Ferreira
 */
@FacesConverter(value = "plantioConverter")
public class PlantioConverter implements Converter {

		private static final Map<Plantio, String> PLANTIOS = new HashMap<Plantio, String>();

		@Override
		public Object getAsObject(FacesContext arg0, UIComponent arg1, String stringValor) {

			if ("".equals(stringValor)) {
				return null;
			}
			// Procura pelo plantio correspondente ao select item selecionado:
			for (Entry<Plantio, String> entry : PLANTIOS.entrySet()) {
				if (entry.getValue().equals(stringValor)) {
					return entry.getKey();
				}
			}

			throw new RuntimeException("Erro inesperado no converter " + getClass().getName()
					+ ": Refer�ncia para uma entidade foi perdida.");
		}

		@Override
		public String getAsString(FacesContext arg0, UIComponent arg1, Object plantio) {
			// Se o objeto selecionado for o valor vazio retorna
			// a string contendo a descri��o.
			if ("".equals(plantio.toString())) {
				return "";
			}

			// Adiciona os demais plantioes, que s�o Bomba ao Map de PLANTIOS, id
			// do mesmo.
			synchronized (PLANTIOS) {
				if (PLANTIOS.containsKey(plantio)) {
					PLANTIOS.remove(plantio);
				}
				PLANTIOS.put((Plantio) plantio, ((Plantio) plantio).getId().toString());
				return PLANTIOS.get(plantio);
			}
		}
	}
