package com.agrosolutions.sai.util;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 * Converter espec�fico para sele��o com {@link Date}, onde o primeiro item selecionado � vazio
 * 
 * @author Raphael.Ferreira
 */
@FacesConverter(value = "dateConverter")
public class DateConverter implements Converter {

	private static final Map<Date, String> DATES = new HashMap<Date, String>();

	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String stringValor) {

		if ("".equals(stringValor)) {
			return null;
		}
		// Procura pela data correspondente ao select item selecionado:
		for (Entry<Date, String> entry : DATES.entrySet()) {
			if (entry.getValue().equals(stringValor)) {
				return entry.getKey();
			}
		}

		throw new RuntimeException("Erro inesperado no converter " + getClass().getName()
				+ ": Refer�ncia para uma entidade foi perdida.");
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object data) {
		// Se o objeto selecionado for o valor vazio retorna
		// a string contendo a descri��o.
		if ("".equals(data.toString())) {
			return "";
		}

		synchronized (DATES) {
			if (DATES.containsKey(data)) {
				DATES.remove(data);
			}
			DATES.put((Date) data, ((Date) data).toString());
			return DATES.get(data);
		}
	}
}
