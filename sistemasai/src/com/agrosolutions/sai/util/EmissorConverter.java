package com.agrosolutions.sai.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import com.agrosolutions.sai.model.Emissor;
/**
 * Converter espec�fico para sele��o com {@link Emissor}, onde o primeiro item selecionado � vazio
 * 
 * @author Raphael.Ferreira
 */
@FacesConverter(value = "emissorConverter")
public class EmissorConverter implements Converter {

		private static final Map<Emissor, String> EMISSORES = new HashMap<Emissor, String>();

		@Override
		public Object getAsObject(FacesContext arg0, UIComponent arg1, String stringValor) {

			if ("".equals(stringValor)) {
				return null;
			}
			// Procurar pelo emissor correspondente ao select item selecionado:
			for (Entry<Emissor, String> entry : EMISSORES.entrySet()) {
				if (entry.getValue().equals(stringValor)) {
					return entry.getKey();
				}
			}

			throw new RuntimeException("Erro inesperado no converter " + getClass().getName()
					+ ": Refer�ncia para uma entidade foi perdida.");
		}

		@Override
		public String getAsString(FacesContext arg0, UIComponent arg1, Object emissor) {
			// Se o objeto selecionado for o valor vazio retorna
			// a string contendo a descri��o.
			if ("".equals(emissor.toString())) {
				return "";
			}

			// Adiciona os demais emissores, que s�o Bomba ao Map de EMISSORES, id
			// do mesmo.
			synchronized (EMISSORES) {
				if (EMISSORES.containsKey(emissor)) {
					EMISSORES.remove(emissor);
				}
				EMISSORES.put((Emissor) emissor, ((Emissor) emissor).getId().toString());
				return EMISSORES.get(emissor);
			}
		}
	}
