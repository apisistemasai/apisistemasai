package com.agrosolutions.sai.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import com.agrosolutions.sai.model.Adubo;

/**
 * Converter espec�fico para sele��o com {@link Adubo}, onde o primeiro item selecionado � vazio
 * 
 * @author Raphael.Ferreira
 */
@FacesConverter(value = "aduboConverter")
public class AduboConverter implements Converter {

	private static final Map<Adubo, String> PRAGAS = new HashMap<Adubo, String>();

	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String stringValor) {

		if ("".equals(stringValor)) {
			return null;
		}
		// Procura pela adubo correspondente ao select item selecionado:
		for (Entry<Adubo, String> entry : PRAGAS.entrySet()) {
			if (entry.getValue().equals(stringValor)) {
				return entry.getKey();
			}
		}

		throw new RuntimeException("Erro inesperado no converter " + getClass().getName()
				+ ": Refer�ncia para uma entidade foi perdida.");
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object adubo) {
		// Se o objeto selecionado for o valor vazio retorna
		// a string contendo a descri��o.
		if ("".equals(adubo.toString())) {
			return "";
		}

		synchronized (PRAGAS) {
			if (!PRAGAS.containsKey(adubo)) {
				PRAGAS.put((Adubo) adubo, ((Adubo) adubo).getId().toString());
				return ((Adubo) adubo).getId().toString();
			}
			return PRAGAS.get(adubo);
		}
	}
}
