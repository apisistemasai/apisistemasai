package com.agrosolutions.sai.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import com.agrosolutions.sai.model.Cultura;

/**
 * Converter espec�fico para sele��o com {@link Cultura}, onde o primeiro item selecionado � vazio
 * 
 * @author Raphael.Ferreira
 */
@FacesConverter(value = "culturaConverter")
public class CulturaConverter implements Converter {

	private static final Map<Cultura, String> CULTURAS = new HashMap<Cultura, String>();

	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String stringValor) {

		if ("".equals(stringValor)) {
			return null;
		}
		// Procura pela cultura correspondente ao select item selecionado:
		for (Entry<Cultura, String> entry : CULTURAS.entrySet()) {
			if (entry.getValue().equals(stringValor)) {
				return entry.getKey();
			}
		}

		throw new RuntimeException("Erro inesperado no converter " + getClass().getName()
				+ ": Refer�ncia para uma entidade foi perdida.");
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object cultura) {
		// Se o objeto selecionado for o valor vazio retorna
		// a string contendo a descri��o.
		if ("".equals(cultura.toString())) {
			return "";
		}

		synchronized (CULTURAS) {
			if (CULTURAS.containsKey(cultura)) {
				CULTURAS.remove(cultura);
			}
			CULTURAS.put((Cultura) cultura, ((Cultura) cultura).getId().toString());
			return CULTURAS.get(cultura);
		}
	}
}
