package com.agrosolutions.sai.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import com.agrosolutions.sai.model.Atividade;

/**
 * Converter espec�fico para sele��o com {@link Atividade}, onde o primeiro item selecionado � vazio
 * 
 * @author Raphael.Ferreira
 */
@FacesConverter(value = "atividadeConverter")
public class AtividadeConverter implements Converter {

	private static final Map<Atividade, String> ATIVIDADES = new HashMap<Atividade, String>();

	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String stringValor) {

		if ("".equals(stringValor)) {
			return null;
		}
		// Procura pela atividade correspondente ao select item selecionado:
		for (Entry<Atividade, String> entry : ATIVIDADES.entrySet()) {
			if (entry.getValue().equals(stringValor)) {
				return entry.getKey();
			}
		}

		throw new RuntimeException("Erro inesperado no converter " + getClass().getName()
				+ ": Refer�ncia para uma entidade foi perdida.");
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object atividade) {
		// Se o objeto selecionado for o valor vazio retorna
		// a string contendo a descri��o.
		if ("".equals(atividade.toString())) {
			return "";
		}

		// Adiciona as demais atividades, que s�o Atividade ao Map de ATIVIDADES, id
		// do mesmo.
		synchronized (ATIVIDADES) {
			if (!ATIVIDADES.containsKey(atividade)) {
				ATIVIDADES.put((Atividade) atividade, ((Atividade) atividade).getId().toString());
				return ((Atividade) atividade).getId().toString();
			}
			return ATIVIDADES.get(atividade);
		}
	}
}
