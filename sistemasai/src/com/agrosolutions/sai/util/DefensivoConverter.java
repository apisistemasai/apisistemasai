package com.agrosolutions.sai.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import com.agrosolutions.sai.model.Defensivo;

/**
 * Converter espec�fico para sele��o com {@link Defensivo}, onde o primeiro item selecionado � vazio
 * 
 * @author Raphael.Ferreira
 */
@FacesConverter(value = "defensivoConverter")
public class DefensivoConverter implements Converter {

	private static final Map<Defensivo, String> PRAGAS = new HashMap<Defensivo, String>();

	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String stringValor) {

		if ("".equals(stringValor)) {
			return null;
		}
		// Procura pela defensivo correspondente ao select item selecionado:
		for (Entry<Defensivo, String> entry : PRAGAS.entrySet()) {
			if (entry.getValue().equals(stringValor)) {
				return entry.getKey();
			}
		}

		throw new RuntimeException("Erro inesperado no converter " + getClass().getName()
				+ ": Refer�ncia para uma entidade foi perdida.");
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object defensivo) {
		// Se o objeto selecionado for o valor vazio retorna
		// a string contendo a descri��o.
		if ("".equals(defensivo.toString())) {
			return "";
		}

		synchronized (PRAGAS) {
			if (!PRAGAS.containsKey(defensivo)) {
				PRAGAS.put((Defensivo) defensivo, ((Defensivo) defensivo).getId().toString());
				return ((Defensivo) defensivo).getId().toString();
			}
			return PRAGAS.get(defensivo);
		}
	}
}
