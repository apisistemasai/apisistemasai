package com.agrosolutions.sai.util;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Locale;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JRXlsExporter;

import org.apache.log4j.Logger;
import org.primefaces.model.DefaultStreamedContent;
import org.springframework.web.context.WebApplicationContext;

public class RelatorioUtil {
	
	static Logger logger = Logger.getLogger(RelatorioUtil.class);

	public DefaultStreamedContent relatorio(String arquivoRelatorio, Map<String, Object> parametros, String nomeArquivo) throws Exception {  

		// pegando o servlet context
		FacesContext facesContext = FacesContext.getCurrentInstance();  
		ServletContext servletContext = (ServletContext) facesContext.getExternalContext().getContext();

		// pegando o ds do spring
		String springVar = "org.springframework.web.context.WebApplicationContext.ROOT";
		WebApplicationContext wctx = (WebApplicationContext) servletContext.getAttribute(springVar);
		DataSource dataSource = (DataSource) wctx.getBean("dataSourceRel");

		String logo = servletContext.getRealPath("//imagens/" + "logo.png");
		String pathRel = servletContext.getRealPath("//relatorios/" + arquivoRelatorio);  

		//parametros  
		parametros.put("LOGO", logo);
		parametros.put(JRParameter.REPORT_LOCALE, Locale.getDefault());		

		//Fill the report using an empty data source
		JasperPrint print;

		print = JasperFillManager.fillReport(pathRel, parametros, dataSource.getConnection());
		byte[] bytes = JasperExportManager.exportReportToPdf(print);
		//writeBytesAsAttachedTextFile(bytes, nomeArquivo);

		InputStream stream = new ByteArrayInputStream(bytes);		
        
        return new DefaultStreamedContent(stream, "application/pdf", nomeArquivo);
	}

	public DefaultStreamedContent relatorioXLS(String arquivoRelatorio, Map<String, Object> parametros, String nomeArquivo) throws Exception {

		// pegando o servlet context
		FacesContext facesContext = FacesContext.getCurrentInstance();
		ServletContext servletContext = (ServletContext) facesContext.getExternalContext().getContext();

		// pegando o ds do spring
		String springVar = "org.springframework.web.context.WebApplicationContext.ROOT";
		WebApplicationContext wctx = (WebApplicationContext) servletContext.getAttribute(springVar);
		DataSource dataSource = (DataSource) wctx.getBean("dataSourceRel");

		String logo = servletContext.getRealPath("//imagens/" + "logo.png");
		String pathRel = servletContext.getRealPath("//relatorios/" + arquivoRelatorio);

		//parametros
		parametros.put("LOGO", logo);
		parametros.put(JRParameter.REPORT_LOCALE, Locale.getDefault());

		// impressora jasper
		JasperPrint print = JasperFillManager.fillReport(pathRel, parametros, dataSource.getConnection());

		JRExporter tipoArquivoExportado = new JRXlsExporter();
		File arquivoGerado = new java.io.File(nomeArquivo);

		tipoArquivoExportado.setParameter(JRExporterParameter.JASPER_PRINT, print);
		tipoArquivoExportado.setParameter(JRExporterParameter.OUTPUT_FILE, arquivoGerado);
		tipoArquivoExportado.exportReport();
		arquivoGerado.deleteOnExit();

		InputStream conteudoRelatorio = new FileInputStream(arquivoGerado);

		return new DefaultStreamedContent(conteudoRelatorio, "application/xls", nomeArquivo);
	}
	
	protected void writeBytesAsAttachedTextFile(byte[] bytes, String fileName) throws Exception {  

		if (bytes == null)  
			throw new Exception("Array de bytes nulo.");  

		if (fileName == null)  
			throw new Exception("Nome do arquivo � nulo.");  

		FacesContext facesContext = FacesContext.getCurrentInstance();  
		HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();  
		response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\";");
		response.setContentLength(bytes.length);
		ServletOutputStream ouputStream = response.getOutputStream();
		ouputStream.write(bytes, 0, bytes.length);
		ouputStream.flush();
		ouputStream.close();
		facesContext.responseComplete();

	}  

}