package com.agrosolutions.sai.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import com.agrosolutions.sai.model.Fornecedor;

/**
 * Converter espec�fico para sele��o com {@link Fornecedor}, onde o primeiro item selecionado � vazio
 * 
 * @author mateus.gabriel
 */
@FacesConverter(value = "fornecedorConverter")
public class FornecedorConverter implements Converter {

	private static final Map<Fornecedor, String> FORNECEDORES = new HashMap<Fornecedor, String>();

	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String stringValor) {

		if ("".equals(stringValor)) {
			return null;
		}
		// Procura pelo fornecedor correspondente ao select item selecionado:
		for (Entry<Fornecedor, String> entry : FORNECEDORES.entrySet()) {
			if (entry.getValue().equals(stringValor)) {
				return entry.getKey();
			}
		}

		throw new RuntimeException("Erro inesperado no converter " + getClass().getName()
				+ ": Refer�ncia para uma entidade foi perdida.");
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object fornecedor) {
		// Se o objeto selecionado for o valor vazio retorna
		// a string contendo a descri��o.
		if ("".equals(fornecedor.toString())) {
			return "";
		}

		// Adiciona os demais Fornecedores, que s�o Fornecedor ao Map de FORNECEDORES, id
		// do mesmo.
		synchronized (FORNECEDORES) {
			if (FORNECEDORES.containsKey(fornecedor)) {
				FORNECEDORES.remove(fornecedor);
			}
			FORNECEDORES.put((Fornecedor) fornecedor, ((Fornecedor) fornecedor).getId().toString());
			return FORNECEDORES.get(fornecedor);
		}
	}
}
