package com.agrosolutions.sai.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import com.agrosolutions.sai.model.Categoria;

/**
 * Converter espec�fico para sele��o com {@link Categoria}, onde o primeiro item selecionado � vazio
 * 
 * @author Raphael.Ferreira
 */
@FacesConverter(value = "categoriaConverter")
public class CategoriaConverter implements Converter {

	private static final Map<Categoria, String> CATEGORIAS = new HashMap<Categoria, String>();

	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String stringValor) {

		if ("".equals(stringValor)) {
			return null;
		}
		// Procura pela bomba correspondente ao select item selecionado:
		for (Entry<Categoria, String> entry : CATEGORIAS.entrySet()) {
			if (entry.getValue().equals(stringValor)) {
				return entry.getKey();
			}
		}

		throw new RuntimeException("Erro inesperado no converter " + getClass().getName()
				+ ": Refer�ncia para uma entidade foi perdida.");
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object categoria) {
		// Se o objeto selecionado for o valor vazio retorna
		// a string contendo a descri��o.
		if ("".equals(categoria.toString())) {
			return "";
		}

		// Adiciona as demais Categorias, que s�o Categoria ao Map de CATEGORIAS, id
		// do mesmo.
		synchronized (CATEGORIAS) {
			if (CATEGORIAS.containsKey(categoria)) {
				CATEGORIAS.remove(categoria);
			}
			CATEGORIAS.put((Categoria) categoria, ((Categoria) categoria).getId().toString());
			return CATEGORIAS.get(categoria);
		}
	}
}
