package com.agrosolutions.sai.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import com.agrosolutions.sai.model.Perfil;

/**
 * Converter espec�fico para sele��o com {@link Perfil}, onde o primeiro item selecionado � vazio
 * 
 * @author Raphael.Ferreira
 */
@FacesConverter(value = "perfilConverter")
public class PerfilConverter implements Converter {

	private static final Map<Perfil, String> PERFIS = new HashMap<Perfil, String>();

	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String stringValor) {

		if ("".equals(stringValor)) {
			return null;
		}
		// Procura pela perfil correspondente ao select item selecionado:
		for (Entry<Perfil, String> entry : PERFIS.entrySet()) {
			if (entry.getValue().equals(stringValor)) {
				return entry.getKey();
			}
		}

		throw new RuntimeException("Erro inesperado no converter " + getClass().getName()
				+ ": Refer�ncia para uma entidade foi perdida.");
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object perfil) {
		// Se o objeto selecionado for o valor vazio retorna
		// a string contendo a descri��o.
		if ("".equals(perfil.toString())) {
			return "";
		}

		synchronized (PERFIS) {
			if (PERFIS.containsKey(perfil)) {
				PERFIS.remove(perfil);
			}
			
			PERFIS.put((Perfil) perfil, ((Perfil) perfil).getId().toString());
			return PERFIS.get(perfil);
		}
	}
}
