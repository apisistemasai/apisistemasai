package com.agrosolutions.sai.util;

import javax.faces.context.ExceptionHandler;
import javax.faces.context.ExceptionHandlerFactory;

public class SaiExceptionHandlerFactory extends ExceptionHandlerFactory {

	private final ExceptionHandlerFactory delegateFactory;

	public SaiExceptionHandlerFactory(ExceptionHandlerFactory delegateFactory) {
		this.delegateFactory = delegateFactory;
	}

	@Override
	public ExceptionHandler getExceptionHandler() {
		return new SaiExceptionHandler(delegateFactory.getExceptionHandler());
	}
}