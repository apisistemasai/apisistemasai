package com.agrosolutions.sai.util;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import com.agrosolutions.sai.model.Coordenada;

import de.micromata.opengis.kml.v_2_2_0.Coordinate;
import de.micromata.opengis.kml.v_2_2_0.Document;
import de.micromata.opengis.kml.v_2_2_0.Feature;
import de.micromata.opengis.kml.v_2_2_0.Geometry;
import de.micromata.opengis.kml.v_2_2_0.Kml;
import de.micromata.opengis.kml.v_2_2_0.LineString;
import de.micromata.opengis.kml.v_2_2_0.Placemark;

public class KmlUtil {
	
	private List<Coordenada> coordenadas = new ArrayList<Coordenada>();
	int ordem = 0;
	
	public List<Coordenada> getCoordenadas(InputStream arquivo){
		try {
			Kml kml = Kml.unmarshal(arquivo);
			Feature feature = kml.getFeature();
			parseFeature(feature);
			ordem = 0;
		} catch (Exception e) {
			e.getStackTrace();
		}
		
		return coordenadas;
	}
	
	private void parseFeature(Feature feature) {
		try {
			if(feature != null) {
		        if(feature instanceof Document) {
		            Document document = (Document) feature;
		            List<Feature> featureList = document.getFeature();
		            for(Feature documentFeature : featureList) {
		            	Placemark placemark = (Placemark) documentFeature;
		            	Geometry geometry = placemark.getGeometry();
		            	parseGeometry(geometry);
		            }
		        }
		    }
		} catch (Exception e) {
			e.getStackTrace();
		}
	}

	private void parseGeometry(Geometry geometry) {
	    if(geometry != null) {
	        if(geometry instanceof LineString) {
                List<Coordinate> coordinates = ((LineString) geometry).getCoordinates();
                if(coordinates != null) {
                    for(Coordinate coordinate : coordinates) {
                        parseCoordinate(coordinate);
                    }
                }
	        }
	    }
	}

	private void parseCoordinate(Coordinate coordinate) {
	    if(coordinate != null) {
	    	Coordenada coordenada = new Coordenada();
	    	coordenada.setLatitude(coordinate.getLatitude());
	    	coordenada.setLongitude(coordinate.getLongitude());	    	
	    	coordenada.setAltitude(coordinate.getAltitude());
	    	coordenada.setOrdem(ordem);
	    	
	    	System.out.println("Ordem : " +  ordem);
	    	System.out.println("Latitude : " +  coordinate.getLatitude());
	    	System.out.println("Longitude : " +  coordinate.getLongitude());
	    	System.out.println("Altitude : " +  coordinate.getAltitude());
	    	System.out.println("");
	    	ordem++;
	    	this.coordenadas.add(coordenada);
	    }
	}

	public void setCoordenadas(List<Coordenada> coordenadas) {
		this.coordenadas = coordenadas;
	}
}
