package com.agrosolutions.sai.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import com.agrosolutions.sai.model.Distrito;

/**
 * Converter espec�fico para sele��o com {@link Distrito}, onde o primeiro item selecionado � vazio
 * 
 * @author Raphael.Ferreira
 */
@FacesConverter(value = "distritoConverter")
public class DistritoConverter implements Converter {

	private static final Map<Distrito, String> DISTRITOS = new HashMap<Distrito, String>();

	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String stringValor) {

		if ("".equals(stringValor)) {
			return null;
		}
		// Procura pela distrito correspondente ao select item selecionado:
		for (Entry<Distrito, String> entry : DISTRITOS.entrySet()) {
			if (entry.getValue().equals(stringValor)) {
				return entry.getKey();
			}
		}

		throw new RuntimeException("Erro inesperado no converter " + getClass().getName()
				+ ": Refer�ncia para uma entidade foi perdida.");
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object distrito) {
		// Se o objeto selecionado for o valor vazio retorna
		// a string contendo a descri��o.
		if ("".equals(distrito.toString())) {
			return "";
		}

		synchronized (DISTRITOS) {
			if (!DISTRITOS.containsKey(distrito)) {
				DISTRITOS.put((Distrito) distrito, ((Distrito) distrito).getId().toString());
				return ((Distrito) distrito).getId().toString();
			}
			return DISTRITOS.get(distrito);
		}
	}
}
