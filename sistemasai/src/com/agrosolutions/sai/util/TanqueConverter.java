package com.agrosolutions.sai.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import com.agrosolutions.sai.model.Tanque;

/**
 * Converter espec�fico para sele��o com {@link Tanque}, onde o primeiro item selecionado � vazio
 * 
 * @author Raphael.Ferreira
 */
@FacesConverter(value = "tanqueConverter")
public class TanqueConverter implements Converter {

	private static final Map<Tanque, String> TANQUES = new HashMap<Tanque, String>();

	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String stringValor) {

		if ("".equals(stringValor)) {
			return null;
		}
		// Procura pela tanque correspondente ao select item selecionado:
		for (Entry<Tanque, String> entry : TANQUES.entrySet()) {
			if (entry.getValue().equals(stringValor)) {
				return entry.getKey();
			}
		}

		throw new RuntimeException("Erro inesperado no converter " + getClass().getName()
				+ ": Refer�ncia para uma entidade foi perdida.");
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object tanque) {
		// Se o objeto selecionado for o valor vazio retorna
		// a string contendo a descri��o.
		if ("".equals(tanque.toString())) {
			return "";
		}

		// Adiciona as demais tanques, que s�o Tanque ao Map de TANQUES, id
		// do mesmo.
		synchronized (TANQUES) {
			if (TANQUES.containsKey(tanque)) {
				TANQUES.remove(tanque);
			}
			TANQUES.put((Tanque) tanque, ((Tanque) tanque).getId().toString());
			return TANQUES.get(tanque);
		}
	}
}
