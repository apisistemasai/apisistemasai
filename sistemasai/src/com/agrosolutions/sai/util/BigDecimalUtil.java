package com.agrosolutions.sai.util;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

public class BigDecimalUtil {

	//http://docs.oracle.com/javase/1.4.2/docs/api/java/math/BigDecimal.html#ROUND_CEILING
	//Note that this is the rounding mode that minimizes cumulative error when applied repeatedly over a sequence of calculations.
	private static final RoundingMode RM = RoundingMode.HALF_EVEN;
	private static final RoundingMode HD = RoundingMode.HALF_DOWN;
	private static final RoundingMode UN = RoundingMode.UNNECESSARY;
	public static final MathContext MC = MathContext.DECIMAL128;
	public static final MathContext MC_RUP = new MathContext(2, RM);
	public static final MathContext MC_RDO = new MathContext(2, HD);
	public static final MathContext MC_UN = new MathContext(2, UN);
	public static final BigDecimal _100 = new BigDecimal(100);

}
