package com.agrosolutions.sai.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.BatchSize;
import org.hibernate.envers.Audited;
import org.primefaces.model.SortOrder;

@Entity
@SequenceGenerator(name = "entity_sequence", sequenceName = "sq_Mensagem", allocationSize = 1)
@Table
@Audited
public class Mensagem extends Model implements Serializable {

	private static final long serialVersionUID = 3634187567924955074L;
	private TipoMensagem tipoMensagem;
	private String assunto;
	private String texto;
	private String anexo;
	@Temporal(TemporalType.DATE)
	private Date dataEnvio;
	@ManyToOne
	private Usuario usuario;
	@ManyToMany
	@BatchSize(size=30)
	private List<Irrigante> destinatarios;
	@ManyToOne
	private Distrito distrito;
	@Transient
	private String sortField;
	@Transient
	private SortOrder sortOrder;
	@Transient
	private Irrigante irrigante;
	@Transient
	private List<Distrito> distritos;

	public Mensagem() {
		super();
	}

	public Mensagem(TipoMensagem tipoMensagem, String assunto, String texto,
			String anexo, Date dataEnvio, Usuario usuario,
			List<Irrigante> destinatarios, Distrito distrito) {
		super();
		this.tipoMensagem = tipoMensagem;
		this.assunto = assunto;
		this.texto = texto;
		this.anexo = anexo;
		this.dataEnvio = dataEnvio;
		this.usuario = usuario;
		this.destinatarios = destinatarios;
		this.distrito = distrito;
	}
	
	public String getAssunto() {
		return assunto;
	}
	public void setAssunto(String assunto) {
		this.assunto = assunto;
	}
	public String getTexto() {
		return texto;
	}
	public void setTexto(String texto) {
		this.texto = texto;
	}
	public String getAnexo() {
		return anexo;
	}
	public void setAnexo(String anexo) {
		this.anexo = anexo;
	}
	public Date getDataEnvio() {
		return dataEnvio;
	}
	public void setDataEnvio(Date dataEnvio) {
		this.dataEnvio = dataEnvio;
	}
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public List<Irrigante> getDestinatarios() {
		return destinatarios;
	}
	public TipoMensagem getTipoMensagem() {
		return tipoMensagem;
	}
	public void setTipoMensagem(TipoMensagem tipoMensagem) {
		this.tipoMensagem = tipoMensagem;
	}
	public void setDestinatarios(List<Irrigante> destinatarios) {
		this.destinatarios = destinatarios;
	}

	public Distrito getDistrito() {
		return distrito;
	}

	public void setDistrito(Distrito distrito) {
		this.distrito = distrito;
	}

	public String getSortField() {
		return sortField;
	}

	public void setSortField(String sortField) {
		this.sortField = sortField;
	}

	public SortOrder getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(SortOrder sortOrder) {
		this.sortOrder = sortOrder;
	}

	public Irrigante getIrrigante() {
		return irrigante;
	}

	public void setIrrigante(Irrigante irrigante) {
		this.irrigante = irrigante;
	}

	public List<String> getDestinos() {
		List<String> dest = new ArrayList<String>();
		for (Irrigante i : destinatarios) {
			if (!dest.contains(i.getLocalizacao() + "-" + i.getNome())) {
				dest.add(i.getLocalizacao() + "-" + i.getNome());
			}
		}
		return dest;
	}

	public List<Distrito> getDistritos() {
		return distritos;
	}

	public void setDistritos(List<Distrito> distritos) {
		this.distritos = distritos;
	}

}