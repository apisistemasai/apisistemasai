package com.agrosolutions.sai.model;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;
import org.primefaces.model.SortOrder;
import org.primefaces.model.chart.PieChartModel;

@Entity
@SequenceGenerator(name = "entity_sequence", sequenceName = "sq_Distrito", allocationSize = 1)
@Table
@Audited
@NamedQueries({
		@NamedQuery(name = "Distrito.todos", query = "SELECT d FROM Distrito d WHERE d.id IN (:pDistrito) ORDER BY d.nome ASC"),
		@NamedQuery(name = "Distrito.excluir", query = "DELETE FROM Distrito d WHERE d.id = :pId"),
		@NamedQuery(name = "Distrito.pesquisarTodos", query = "SELECT d FROM Distrito d ORDER BY d.nome ASC")
})
public class Distrito extends Model {

	private static final long serialVersionUID = -551843934096516797L;

	private String nome;

	private String endereco;

	private String cidade;

	private String cep;

	private String uf;

	private String telefoneContato;
	
	private String telefoneCelular;

	private String localizacao;

	private String latitude;

	private String longitude;

	private BigDecimal areaTotal;

	private BigDecimal areaPreservada;

	private Integer zoommapa;

	@Column(length = 800)
	private String observacao;

	@Temporal(TemporalType.DATE)
	private Date dataCadastro;

	@Temporal(TemporalType.DATE)
	private Date dataInatividade;

	@Transient
	private Clima clima;
	@Transient
	private PieChartModel graficoAreaCultivada;
	@Transient
	private PieChartModel graficoIrrigacao;
	@Transient
	private PieChartModel graficoCultura;
	@Transient
	private PieChartModel graficoSolo;
	@Transient
	private PieChartModel graficoAtividade;
	@Transient
	private PieChartModel graficoIrrigante;
	@Transient
	private PieChartModel graficoEmissor;
	@Transient
	private PieChartModel graficoBomba;
	@Transient
	private PieChartModel graficoFiltro;
	@Transient
	private PieChartModel graficoValvula;
	@Transient
	private PieChartModel graficoTubo;
	@Transient
	private PieChartModel graficoAreaIrrigada;
	@Transient
	private PieChartModel graficoCulturasPorHa;
	
	@Transient
	List<Distrito> distritos;
	@Transient
	private String sortField;
	@Transient
	private SortOrder sortOrder;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getUf() {
		return uf;
	}

	public void setUf(String uf) {
		this.uf = uf;
	}

	public String getTelefoneContato() {
		return telefoneContato;
	}

	public void setTelefoneContato(String telefoneContato) {
		this.telefoneContato = telefoneContato;
	}
	
	public String getTelefoneCelular() {
		return telefoneContato;
	}

	public void setTelefoneCelular(String telefoneCelular) {
		this.telefoneCelular = telefoneCelular;
	}

	public String getLocalizacao() {
		return localizacao;
	}

	public void setLocalizacao(String localizacao) {
		this.localizacao = localizacao;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public BigDecimal getAreaTotal() {
		return areaTotal;
	}

	public PieChartModel getGraficoAreaCultivada() {
		return graficoAreaCultivada;
	}

	public void setGraficoAreaCultivada(PieChartModel graficoAreaCultivada) {
		this.graficoAreaCultivada = graficoAreaCultivada;
	}

	public void setAreaTotal(BigDecimal areaTotal) {
		this.areaTotal = areaTotal;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public Date getDataInatividade() {
		return dataInatividade;
	}

	public void setDataInatividade(Date dataInatividade) {
		this.dataInatividade = dataInatividade;
	}

	public Clima getClima() {
		return clima;
	}

	public void setClima(Clima clima) {
		this.clima = clima;
	}

	public PieChartModel getGraficoIrrigacao() {
		return graficoIrrigacao;
	}

	public void setGraficoIrrigacao(PieChartModel graficoIrrigacao) {
		this.graficoIrrigacao = graficoIrrigacao;
	}

	public PieChartModel getGraficoCultura() {
		return graficoCultura;
	}

	public void setGraficoCultura(PieChartModel graficoCultura) {
		this.graficoCultura = graficoCultura;
	}

	public PieChartModel getGraficoSolo() {
		return graficoSolo;
	}

	public void setGraficoSolo(PieChartModel graficoSolo) {
		this.graficoSolo = graficoSolo;
	}

	public PieChartModel getGraficoBomba() {
		return graficoBomba;
	}

	public void setGraficoBomba(PieChartModel graficoBomba) {
		this.graficoBomba = graficoBomba;
	}

	public PieChartModel getGraficoEmissor() {
		return graficoEmissor;
	}

	public void setGraficoEmissor(PieChartModel graficoEmissor) {
		this.graficoEmissor = graficoEmissor;
	}

	public PieChartModel getGraficoFiltro() {
		return graficoFiltro;
	}

	public void setGraficoFiltro(PieChartModel graficoFiltro) {
		this.graficoFiltro = graficoFiltro;
	}

	public PieChartModel getGraficoValvula() {
		return graficoValvula;
	}

	public void setGraficoValvula(PieChartModel graficoValvula) {
		this.graficoValvula = graficoValvula;
	}

	public PieChartModel getGraficoTubo() {
		return graficoTubo;
	}

	public void setGraficoTubo(PieChartModel graficoTubo) {
		this.graficoTubo = graficoTubo;
	}

	public PieChartModel getGraficoAtividade() {
		return graficoAtividade;
	}

	public void setGraficoAtividade(PieChartModel graficoAtividade) {
		this.graficoAtividade = graficoAtividade;
	}

	public PieChartModel getGraficoIrrigante() {
		return graficoIrrigante;
	}

	public void setGraficoIrrigante(PieChartModel graficoIrrigante) {
		this.graficoIrrigante = graficoIrrigante;
	}

	public void setSortField(String sortField) {
		this.sortField = sortField;
	}

	public String getSortField() {
		return sortField;
	}

	public void setSortOrder(SortOrder sortOrder) {
		this.sortOrder = sortOrder;
	}

	public SortOrder getSortOrder() {
		return sortOrder;
	}

	public List<Distrito> getDistritos() {
		return distritos;
	}

	public void setDistritos(List<Distrito> distritos) {
		this.distritos = distritos;
	}

	public Integer getZoommapa() {
		return zoommapa;
	}

	public void setZoommapa(Integer zoommapa) {
		this.zoommapa = zoommapa;
	}

	public PieChartModel getGraficoAreaIrrigada() {
		return graficoAreaIrrigada;
	}

	public void setGraficoAreaIrrigada(PieChartModel graficoAreaIrrigada) {
		this.graficoAreaIrrigada = graficoAreaIrrigada;
	}

	public PieChartModel getGraficoCulturasPorHa() {
		return graficoCulturasPorHa;
	}

	public void setGraficoCulturasPorHa(PieChartModel graficoCulturasPorHa) {
		this.graficoCulturasPorHa = graficoCulturasPorHa;
	}

	public BigDecimal getAreaPreservada() {
		return areaPreservada;
	}

	public void setAreaPreservada(BigDecimal areaPreservada) {
		this.areaPreservada = areaPreservada;
	}
}