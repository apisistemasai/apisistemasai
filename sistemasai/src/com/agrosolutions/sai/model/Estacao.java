package com.agrosolutions.sai.model;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.envers.Audited;
import org.primefaces.model.SortOrder;

@Entity
@SequenceGenerator(name = "entity_sequence", sequenceName = "sq_Estacao", allocationSize = 1)
@Table
@Audited
@NamedQueries({
	@NamedQuery(name = "Estacao.excluir", 
			   query = "DELETE FROM Estacao e WHERE e.id = :pId"), 			  
	@NamedQuery(name = "Estacao.pesquisar", 
			   query = "SELECT e FROM Estacao e WHERE e.distrito IN (:pDistrito) ORDER BY nome ASC") 			  
   })
public class Estacao extends Model {

	private static final long serialVersionUID = 180384738175735411L;
	private String nome;
	private String localizacao;
	private String latitude;
	private String longitude;
	private BigDecimal altitude;
	@Column(length=800)
	private String observacao;
	
	@Temporal(TemporalType.DATE)
	private Date dataCadastro;
	@Temporal(TemporalType.DATE)
	private Date dataInatividade;
	@ManyToOne
	private Distrito distrito;
	@ManyToMany(fetch = FetchType.EAGER)
	@Fetch(FetchMode.SUBSELECT)
	@BatchSize(size=10)
	@JoinTable(name = "estacao_area", schema="sai" , 
			joinColumns = @JoinColumn(name = "id_estacao"),
			inverseJoinColumns = @JoinColumn(name = "id_area"))
	private List<Area> areas;

	@OneToMany(cascade = CascadeType.ALL, mappedBy="estacao")
	@BatchSize(size=20)
	private List<Clima> climas;
	
	@Transient
	private String sortField;
	@Transient
	private SortOrder sortOrder;
	@Transient
	List<Distrito> distritos;
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getLocalizacao() {
		return localizacao;
	}
	public void setLocalizacao(String localizacao) {
		this.localizacao = localizacao;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getObservacao() {
		return observacao;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public Date getDataCadastro() {
		return dataCadastro;
	}
	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}
	public Date getDataInatividade() {
		return dataInatividade;
	}
	public void setDataInatividade(Date dataInatividade) {
		this.dataInatividade = dataInatividade;
	}
	public Distrito getDistrito() {
		return distrito;
	}
	public void setDistrito(Distrito distrito) {
		this.distrito = distrito;
	}
	public List<Area> getAreas() {
		return areas;
	}
	public void setAreas(List<Area> areas) {
		this.areas = areas;
	}
	public List<Clima> getClimas() {
		return climas;
	}
	public void setClimas(List<Clima> climas) {
		this.climas = climas;
	}
	public void setSortField(String sortField) {
		this.sortField = sortField;
	}
	public String getSortField() {
		return sortField;
	}
	public void setSortOrder(SortOrder sortOrder) {
		this.sortOrder = sortOrder;
	}
	public SortOrder getSortOrder() {
		return sortOrder;
	}
	public List<Distrito> getDistritos() {
		return distritos;
	}
	public void setDistritos(List<Distrito> distritos) {
		this.distritos = distritos;
	}
	public BigDecimal getAltitude() {
		return altitude;
	}
	public void setAltitude(BigDecimal altitude) {
		this.altitude = altitude;
	}

}
