package com.agrosolutions.sai.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.BatchSize;
import org.hibernate.envers.Audited;

@Entity
@SequenceGenerator(name = "entity_sequence", sequenceName = "sq_Energia", allocationSize = 1)
@Table
@Audited
@BatchSize(size=30)
@NamedQueries({
	@NamedQuery(name = "Energia.excluir", 
			   query = "DELETE FROM Energia e WHERE e.id = :pId"), 	 			  
	@NamedQuery(name = "Energia.pesquisar", 
			   query = "SELECT e FROM Energia e WHERE e.irrigante = :pIrrigante ORDER BY e.data") 			  
   })
public class Energia extends Model {

	private static final long serialVersionUID = 3692145422098635565L;
	@Temporal(TemporalType.DATE)
	private Date data;
	private BigDecimal quantidade; 	
	private BigDecimal valor;	
	private BigDecimal faturadop;
	private BigDecimal faturadofp;
	private BigDecimal reativop;
	private BigDecimal reativofp;
	private BigDecimal demandap;
	private BigDecimal outros;
	
	@ManyToOne
	private Irrigante irrigante;
	
	public Energia() {
		super();
	}

	public Energia(Date data) {
		super();
		this.data = data;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public BigDecimal getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(BigDecimal quantidade) {
		this.quantidade = quantidade;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public Irrigante getIrrigante() {
		return irrigante;
	}

	public void setIrrigante(Irrigante irrigante) {
		this.irrigante = irrigante;
	}

	public BigDecimal getFaturadop() {
		return faturadop;
	}

	public void setFaturadop(BigDecimal faturadop) {
		this.faturadop = faturadop;
	}

	public BigDecimal getFaturadofp() {
		return faturadofp;
	}

	public void setFaturadofp(BigDecimal faturadofp) {
		this.faturadofp = faturadofp;
	}

	public BigDecimal getReativop() {
		return reativop;
	}

	public void setReativop(BigDecimal reativop) {
		this.reativop = reativop;
	}

	public BigDecimal getReativofp() {
		return reativofp;
	}

	public void setReativofp(BigDecimal reativofp) {
		this.reativofp = reativofp;
	}

	public BigDecimal getDemandap() {
		return demandap;
	}

	public void setDemandap(BigDecimal demandap) {
		this.demandap = demandap;
	}

	public BigDecimal getOutros() {
		return outros;
	}

	public void setOutros(BigDecimal outros) {
		this.outros = outros;
	}
	
}
