package com.agrosolutions.sai.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;

@Entity
@SequenceGenerator(name = "entity_sequence", sequenceName = "sq_Estadio", allocationSize = 1)
@Table
@Audited
@NamedQueries({
	@NamedQuery(name = "Estadio.excluir", 
			   query = "DELETE FROM Estadio e WHERE e.id = :pId"), 	 			  
	@NamedQuery(name = "Estadio.pesquisar", 
			   query = "SELECT e FROM Estadio e WHERE e.referencia = :pReferencia ORDER BY e.ordem") 			  
   })

public class Estadio extends Model implements Serializable {


	private static final long serialVersionUID = 2841589851165184470L;
	private Integer ordem; 
	private EstadioCultivo fase;
	private BigDecimal kc;
	private Long qtdDias; 
	private BigDecimal diametroCopa;
	private BigDecimal profundidadeRaiz;
	private BigDecimal esgotamento;
	private BigDecimal fatorRendimento;
	private BigDecimal alturaFruta;
	private Boolean poda;
	private Boolean posPoda;
	
	@ManyToOne
	private Referencia referencia;
	
	@Transient
	private Long qtdDiasAtual;

	public Estadio() {
	}

	public Estadio(int i, Referencia ref) {
		setOrdem(i);
		setReferencia(ref);
	}
	public Estadio(Estadio e, Referencia referencia) {

		ordem = e.getOrdem(); 
		fase = e.getFase();
		kc = e.getKc();
		qtdDias = e.getQtdDias(); 
		diametroCopa = e.getDiametroCopa();
		profundidadeRaiz = e.getProfundidadeRaiz();
		esgotamento = e.getEsgotamento();
		fatorRendimento = e.getFatorRendimento();
		alturaFruta = e.getAlturaFruta();
		poda = e.getPoda();
		posPoda = e.getPosPoda();
		this.referencia = referencia;;
	}

	public EstadioCultivo getFase() {
		return fase;
	}
	public void setFase(EstadioCultivo fase) {
		this.fase = fase;
	}
	public BigDecimal getKc() {
		return kc;
	}
	public void setKc(BigDecimal kc) {
		this.kc = kc;
	}
	public Long getQtdDias() {
		return qtdDias;
	}
	public void setQtdDias(Long qtdDias) {
		this.qtdDias = qtdDias;
	}
	public BigDecimal getDiametroCopa() {
		return diametroCopa;
	}
	public void setDiametroCopa(BigDecimal diametroCopa) {
		this.diametroCopa = diametroCopa;
	}
	public BigDecimal getProfundidadeRaiz() {
		return profundidadeRaiz;
	}
	public void setProfundidadeRaiz(BigDecimal profundidadeRaiz) {
		this.profundidadeRaiz = profundidadeRaiz;
	}
	public BigDecimal getEsgotamento() {
		return esgotamento;
	}
	public void setEsgotamento(BigDecimal esgotamento) {
		this.esgotamento = esgotamento;
	}
	public BigDecimal getFatorRendimento() {
		return fatorRendimento;
	}
	public void setFatorRendimento(BigDecimal fatorRendimento) {
		this.fatorRendimento = fatorRendimento;
	}
	public BigDecimal getAlturaFruta() {
		return alturaFruta;
	}
	public void setAlturaFruta(BigDecimal alturaFruta) {
		this.alturaFruta = alturaFruta;
	}
	public Referencia getReferencia() {
		return referencia;
	}
	public void setReferencia(Referencia referencia) {
		this.referencia = referencia;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public Boolean getPoda() {
		return poda;
	}
	public void setPoda(Boolean poda) {
		this.poda = poda;
	}
	public Boolean getPosPoda() {
		return posPoda;
	}
	public void setPosPoda(Boolean posPoda) {
		this.posPoda = posPoda;
	}
	public Integer getOrdem() {
		return ordem;
	}
	public void setOrdem(Integer ordem) {
		this.ordem = ordem;
	}

	public Long getQtdDiasAtual() {
		return qtdDiasAtual;
	}

	public void setQtdDiasAtual(Long qtdDiasAtual) {
		this.qtdDiasAtual = qtdDiasAtual;
	}
}