package com.agrosolutions.sai.model;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

@Entity
@SequenceGenerator(name = "entity_sequence", sequenceName = "sq_Coordenadas", allocationSize = 1)
@Table
@Audited
public class Coordenada extends Model {
	
	private static final long serialVersionUID = -6042437422736911627L;
	
	private String local;
	
	private double latitude;
	
	private double longitude;
	
	private double altitude;
	
	private double ordem;

	@ManyToOne
	private Irrigante irrigante;

	@ManyToOne
	private Municipio municipio;
	
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	public double getOrdem() {
		return ordem;
	}
	public void setOrdem(double ordem) {
		this.ordem = ordem;
	}
	public double getAltitude() {
		return altitude;
	}
	public void setAltitude(double altitude) {
		this.altitude = altitude;
	}
	public String getLocal() {
		return local;
	}
	public void setLocal(String local) {
		this.local = local;
	}
	public Irrigante getIrrigante() {
		return irrigante;
	}
	public void setIrrigante(Irrigante irrigante) {
		this.irrigante = irrigante;
	}
	public Municipio getMunicipio() {
		return municipio;
	}
	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}

}
