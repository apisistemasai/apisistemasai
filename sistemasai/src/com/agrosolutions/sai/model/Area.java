package com.agrosolutions.sai.model;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;
import org.primefaces.model.SortOrder;
import org.primefaces.model.chart.PieChartModel;

@Entity
@SequenceGenerator(name = "entity_sequence", sequenceName = "sq_Area", allocationSize = 1)
@Table
@Audited
@NamedQueries({
	@NamedQuery(name = "Area.excluir", 
			   query = "DELETE FROM Area a WHERE a.id = :pId"), 			  
	@NamedQuery(name = "Area.pesquisar", 
			   query = "SELECT a FROM Area a WHERE a.distrito IN (:pDistrito) ORDER BY nome ASC") 			  
   })
public class Area extends Model {

	private static final long serialVersionUID = -7355076840734522336L;
	private String nome;
	private String localizacao;
	private String latitude;
	private String longitude;
	private Integer altitude;
	private BigDecimal areaTotal;
	private BigDecimal areaIrrigada;
	@Column(length=800)
	private String observacao;
	
	@Temporal(TemporalType.DATE)
	private Date dataCadastro;
	@Temporal(TemporalType.DATE)
	private Date dataInatividade;
	@ManyToOne
	private Distrito distrito;
	
	@Transient
	private Clima clima;
	@Transient
	private PieChartModel graficoAreaCultivada;
	@Transient
	private PieChartModel graficoIrrigacao;
	@Transient
	private PieChartModel graficoCultura;
	@Transient
	private PieChartModel graficoSolo;
	@Transient
	private PieChartModel graficoAtividade;
	@Transient
	private PieChartModel graficoIrrigante;
	@Transient
	private PieChartModel graficoEmissor;
	@Transient
	private PieChartModel graficoBomba;
	@Transient
	private PieChartModel graficoFiltro;
	@Transient
	private PieChartModel graficoValvula;
	@Transient
	private PieChartModel graficoTubo;
	@Transient
	private PieChartModel graficoCulturasPorHa;
	
	@Transient
	private String sortField;
	@Transient
	private SortOrder sortOrder;
	@Transient
	List<Distrito> distritos;
	
	@Column(nullable = false,  columnDefinition = "boolean default true")
	private Boolean gerarTI;
	
	
	public PieChartModel getGraficoEmissor() {
		return graficoEmissor;
	}
	public void setGraficoEmissor(PieChartModel graficoEmissor) {
		this.graficoEmissor = graficoEmissor;
	}
	public PieChartModel getGraficoBomba() {
		return graficoBomba;
	}
	public void setGraficoBomba(PieChartModel graficoBomba) {
		this.graficoBomba = graficoBomba;
	}
	public PieChartModel getGraficoFiltro() {
		return graficoFiltro;
	}
	public void setGraficoFiltro(PieChartModel graficoFiltro) {
		this.graficoFiltro = graficoFiltro;
	}
	public PieChartModel getGraficoValvula() {
		return graficoValvula;
	}
	public void setGraficoValvula(PieChartModel graficoValvula) {
		this.graficoValvula = graficoValvula;
	}
	public PieChartModel getGraficoTubo() {
		return graficoTubo;
	}
	public void setGraficoTubo(PieChartModel graficoTubo) {
		this.graficoTubo = graficoTubo;
	}
	public PieChartModel getGraficoAtividade() {
		return graficoAtividade;
	}
	public void setGraficoAtividade(PieChartModel graficoAtividade) {
		this.graficoAtividade = graficoAtividade;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getLocalizacao() {
		return localizacao;
	}
	public void setLocalizacao(String localizacao) {
		this.localizacao = localizacao;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getLongitude() {
		return longitude;
	}
	public PieChartModel getGraficoAreaCultivada() {
		return graficoAreaCultivada;
	}
	public void setGraficoAreaCultivada(PieChartModel graficoAreaCultivada) {
		this.graficoAreaCultivada = graficoAreaCultivada;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public BigDecimal getAreaTotal() {
		return areaTotal;
	}
	public void setAreaTotal(BigDecimal areaTotal) {
		this.areaTotal = areaTotal;
	}
	public BigDecimal getAreaIrrigada() {
		return areaIrrigada;
	}
	public void setAreaIrrigada(BigDecimal areaIrrigada) {
		this.areaIrrigada = areaIrrigada;
	}
	public String getObservacao() {
		return observacao;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public Date getDataCadastro() {
		return dataCadastro;
	}
	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}
	public Date getDataInatividade() {
		return dataInatividade;
	}
	public void setDataInatividade(Date dataInatividade) {
		this.dataInatividade = dataInatividade;
	}
	public Distrito getDistrito() {
		return distrito;
	}
	public void setDistrito(Distrito distrito) {
		this.distrito = distrito;
	}
	public Integer getAltitude() {
		return altitude;
	}
	public void setAltitude(Integer altitude) {
		this.altitude = altitude;
	}
	public Clima getClima() {
		return clima;
	}
	public void setClima(Clima clima) {
		this.clima = clima;
	}
	public PieChartModel getGraficoIrrigacao() {
        return graficoIrrigacao;
	}
	public void setGraficoIrrigacao(PieChartModel graficoIrrigacao) {
		this.graficoIrrigacao = graficoIrrigacao;
	}
	public PieChartModel getGraficoCultura() {
		return graficoCultura;
	}
	public void setGraficoCultura(PieChartModel graficoCultura) {
		this.graficoCultura = graficoCultura;
	}
	public PieChartModel getGraficoIrrigante() {
		return graficoIrrigante;
	}
	public void setGraficoIrrigante(PieChartModel graficoIrrigante) {
		this.graficoIrrigante = graficoIrrigante;
	}
	public PieChartModel getGraficoSolo() {
		return graficoSolo;
	}
	public void setGraficoSolo(PieChartModel graficoSolo) {
		this.graficoSolo = graficoSolo;
	}
	public void setSortField(String sortField) {
		this.sortField = sortField;
	}
	public String getSortField() {
		return sortField;
	}
	public void setSortOrder(SortOrder sortOrder) {
		this.sortOrder = sortOrder;
	}
	public SortOrder getSortOrder() {
		return sortOrder;
	}
	public List<Distrito> getDistritos() {
		return distritos;
	}
	public void setDistritos(List<Distrito> distritos) {
		this.distritos = distritos;
	}
	public PieChartModel getGraficoCulturasPorHa() {
		return graficoCulturasPorHa;
	}
	public void setGraficoCulturasPorHa(PieChartModel graficoCulturasPorHa) {
		this.graficoCulturasPorHa = graficoCulturasPorHa;
	}
	public Boolean getGerarTI() {
		return gerarTI;
	}
	public void setGerarTI(Boolean gerarTI) {
		this.gerarTI = gerarTI;
	}
}