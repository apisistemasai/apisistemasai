package com.agrosolutions.sai.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;

@Entity
@SequenceGenerator(name = "entity_sequence", sequenceName = "sq_Colheita", allocationSize = 1)
@Table
@Audited
public class Colheita extends Model {

	private static final long serialVersionUID = 2258825927128281629L;
	@ManyToOne
	private Plantio plantio;
	@Temporal(TemporalType.DATE)
	private Date dataColheita;
	private Integer qtdColhida;
	private Integer qtdPerdida;
	public Plantio getPlantio() {
		return plantio;
	}
	public void setPlantio(Plantio plantio) {
		this.plantio = plantio;
	}
	public Date getDataColheita() {
		return dataColheita;
	}
	public void setDataColheita(Date dataColheita) {
		this.dataColheita = dataColheita;
	}
	public Integer getQtdColhida() {
		return qtdColhida;
	}
	public void setQtdColhida(Integer qtdColhida) {
		this.qtdColhida = qtdColhida;
	}
	public Integer getQtdPerdida() {
		return qtdPerdida;
	}
	public void setQtdPerdida(Integer qtdPerdida) {
		this.qtdPerdida = qtdPerdida;
	}

}
