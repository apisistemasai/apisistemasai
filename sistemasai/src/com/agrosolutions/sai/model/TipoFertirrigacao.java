package com.agrosolutions.sai.model;

import com.agrosolutions.sai.util.ResourceBundle;

public enum TipoFertirrigacao {

	NENHUM(0, "nenhum"),
	BOMBA(1, "bombaInjetora"),
	VENTURI(2,"venturi");
	
	private Integer id;
	
	private String descricao;

	private TipoFertirrigacao(Integer id, String descricao){
		this.id = id;
		this.descricao = descricao;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the descricao
	 */
	public String getDescricao() {
		return ResourceBundle.getMessage(descricao);
	}

	/**
	 * @param descricao the descricao to set
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}	
	
	/**
	 * M�todo que retorna o valor do enum correspondente a descri��o
	 * 
	 * @param descri��o
	 * @return
	 */
	public static TipoFertirrigacao fromDescricao(String descricao) {
		for (TipoFertirrigacao tipo : values()) {
			if (tipo.descricao.toLowerCase().equals(descricao.toLowerCase())) {
				return tipo;
			}
		}
		throw new RuntimeException(TipoIrrigante.class.getName() + ResourceBundle.getMessage("valorIdentificador") + " " + ResourceBundle.getMessage(descricao));
	}	
}
