package com.agrosolutions.sai.model;

import java.math.BigDecimal;


public enum PesoIndicadores {
	
	ALTA(new BigDecimal(1), "Alta" ),
	MEDIA(new BigDecimal(0.75), "M�dia" ),
	BAIXA(new BigDecimal(0.5), "Baixa" );
	
	private BigDecimal valor;
	private String descricao;

	private PesoIndicadores(BigDecimal valor, String descricao){
		this.setValor(valor);
		this.setDescricao(descricao);
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
}
