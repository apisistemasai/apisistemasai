package com.agrosolutions.sai.model;

import com.agrosolutions.sai.util.ResourceBundle;

public enum TipoRevisao {
	INCLUSAO("inclusao"), ALTERACAO("alteracao"), EXCLUSAO("exclusao");
	
	private TipoRevisao(String descricao) {
		this.descricao = descricao;
	}
	
	private String descricao;

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return ResourceBundle.getMessage(descricao);
	}
}
