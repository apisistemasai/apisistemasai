package com.agrosolutions.sai.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


/**
 * Classe que representa um modulo do sistema. 
 * 
 * @author Raphael Ferreira 
 *
 */
@Entity
@SequenceGenerator(name = "entity_sequence", sequenceName = "sq_Modulo")
@Table
@NamedQueries({
	@NamedQuery(name = "Modulo.todos", 
			   query = "SELECT m FROM Modulo m ORDER BY siglas ASC") 			  
   })
public class Modulo extends Model {
	
	private static final long serialVersionUID = 9059327627186317940L;

	@Column(unique = true)
	private String siglas;
	private String descricao;
	private String url;
	private String imagem;

	public String getSiglas() {
		return siglas;
	}
	public void setSiglas(String siglas) {
		this.siglas = siglas;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getImagem() {
		return imagem;
	}
	public void setImagem(String imagem) {
		this.imagem = imagem;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}

}
