package com.agrosolutions.sai.model;


public class GraficoPizzaIndicadores {
	private final String nome;
	private final double qtd;
	private final double total;
	private final String und;

	public GraficoPizzaIndicadores(String nome, double qtd, double total, String und){
		this.nome = nome;
		this.qtd = qtd;
		this.total = total;
		this.und = und;
	}
	public String getNome() {
		return nome;
	}
	public double getQtd() {
		return qtd;
	}
	public double getTotal() {
		return total;
	}
	public String getUnd() {
		return und;
	}
}
