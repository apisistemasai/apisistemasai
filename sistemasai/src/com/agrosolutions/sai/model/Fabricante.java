package com.agrosolutions.sai.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;
import org.primefaces.model.SortOrder;

@Entity
@SequenceGenerator(name = "entity_sequence", sequenceName = "sq_Fabricante", allocationSize = 1)
@Table
@Audited
@NamedQueries({
	@NamedQuery(name = "Fabricante.excluir", 
			   query = "DELETE FROM Fabricante f WHERE f.id = :pId"),
	@NamedQuery(name = "Fabricante.pesquisar", 
			   query = "SELECT f FROM Fabricante f WHERE f.tipoFabricante = :pTipo OR f.tipoFabricante = 5 ORDER BY nome ASC") 			  
   })
public class Fabricante extends Model {

	private static final long serialVersionUID = 1220001178594702231L;
	@Column(unique=true)
	private String nome;
	private TipoFabricante tipoFabricante;
	@Column(length=800)
	private String observacao;
	
	@Transient
	private String sortField;
	@Transient
	private SortOrder sortOrder;

	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public TipoFabricante getTipoFabricante() {
		return tipoFabricante;
	}
	public void setTipoFabricante(TipoFabricante tipoFabricante) {
		this.tipoFabricante = tipoFabricante;
	}
	public String getObservacao() {
		return observacao;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public void setSortField(String sortField) {
		this.sortField = sortField;
	}
	public String getSortField() {
		return sortField;
	}
	public void setSortOrder(SortOrder sortOrder) {
		this.sortOrder = sortOrder;
	}
	public SortOrder getSortOrder() {
		return sortOrder;
	}
	
}
