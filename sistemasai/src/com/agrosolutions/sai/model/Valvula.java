package com.agrosolutions.sai.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;
import org.primefaces.model.SortOrder;

@Entity
@SequenceGenerator(name = "entity_sequence", sequenceName = "sq_Valvula", allocationSize = 1)
@Table
@Audited
@NamedQueries({
	@NamedQuery(name = "Valvula.excluir", 
			   query = "DELETE FROM Valvula v WHERE v.id = :pId"), 			  
	@NamedQuery(name = "Valvula.todos", 
			   query = "SELECT v FROM Valvula v ORDER BY v.modelo ASC"), 			  
	@NamedQuery(name = "Valvula.pesquisar", 
			   query = "SELECT v FROM Valvula v WHERE v.fabricante = :pFabricante ORDER BY v.modelo ASC"), 			  
	@NamedQuery(name = "Valvula.pesquisarPorIrrigante", 
			   query = "SELECT v FROM Irrigante i join i.valvulas v WHERE i = :pIrrigante ORDER BY v.modelo ASC") 			  
   })
public class Valvula extends Model {

	private static final long serialVersionUID = -248128901919755952L;
	@ManyToOne
	private Fabricante fabricante;
	@Column(unique=true)
	private String modelo;
	private TipoValvula tipo;
	private BigDecimal pressao;
	private BigDecimal vazao;
	@Transient
	private String sortField;
	@Transient
	private SortOrder sortOrder;

	public Fabricante getFabricante() {
		return fabricante;
	}
	public void setFabricante(Fabricante fabricante) {
		this.fabricante = fabricante;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public TipoValvula getTipo() {
		return tipo;
	}
	public void setTipo(TipoValvula tipo) {
		this.tipo = tipo;
	}
	public BigDecimal getPressao() {
		return pressao;
	}
	public void setPressao(BigDecimal pressao) {
		this.pressao = pressao;
	}
	public BigDecimal getVazao() {
		return vazao;
	}
	public void setVazao(BigDecimal vazao) {
		this.vazao = vazao;
	}
	public String getSortField() {
		return sortField;
	}
	public void setSortField(String sortField) {
		this.sortField = sortField;
	}
	public SortOrder getSortOrder() {
		return sortOrder;
	}
	public void setSortOrder(SortOrder sortOrder) {
		this.sortOrder = sortOrder;
	}
	
}
