package com.agrosolutions.sai.model;

public class Retorno {
	private String ind_sucesso;  
    private String nro_retorno;  
	private String dispatcher_id;  
    private String dta_hora_envio;  
    private String msg_retorno;

    public String getInd_sucesso() {
		return ind_sucesso;
	}
	public void setInd_sucesso(String ind_sucesso) {
		this.ind_sucesso = ind_sucesso;
	}
	public String getNro_retorno() {
		return nro_retorno;
	}
	public void setNro_retorno(String nro_retorno) {
		this.nro_retorno = nro_retorno;
	}
	public String getDispatcher_id() {
		return dispatcher_id;
	}
	public void setDispatcher_id(String dispatcher_id) {
		this.dispatcher_id = dispatcher_id;
	}
	public String getDta_hora_envio() {
		return dta_hora_envio;
	}
	public void setDta_hora_envio(String dta_hora_envio) {
		this.dta_hora_envio = dta_hora_envio;
	}
	public String getMsg_retorno() {
		return msg_retorno;
	}
	public void setMsg_retorno(String msg_retorno) {
		this.msg_retorno = msg_retorno;
	}

    
}
