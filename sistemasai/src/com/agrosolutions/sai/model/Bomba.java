package com.agrosolutions.sai.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;
import org.primefaces.model.SortOrder;

@Entity
@SequenceGenerator(name = "entity_sequence", sequenceName = "sq_Bomba", allocationSize = 1)
@Table
@Audited
@NamedQueries({
	@NamedQuery(name = "Bomba.excluir", 
			   query = "DELETE FROM Bomba b WHERE b.id = :pId"), 			  
	@NamedQuery(name = "Bomba.todos", 
			   query = "SELECT b FROM Bomba b ORDER BY b.modelo ASC"), 			  
	@NamedQuery(name = "Bomba.pesquisar", 
			   query = "SELECT b FROM Bomba b WHERE b.fabricante = :pFabricante ORDER BY b.modelo ASC"), 			  
	@NamedQuery(name = "Bomba.pesquisarPorIrrigante", 
			   query = "SELECT b FROM Irrigante i join i.bombas b WHERE i = :pIrrigante ORDER BY b.modelo ASC") 			  
   })
public class Bomba extends Model {

	private static final long serialVersionUID = -745575208109687187L;
	@ManyToOne
	private Fabricante fabricante;
	@Column(unique=true)
	private String modelo;
	private TipoBomba tipo;
	private AcionamentoBomba acionamentoBomba;
	private BigDecimal vazao;
	private BigDecimal potencia;
	private BigDecimal rendimento;
	
	@Transient
	private String sortField;
	@Transient
	private SortOrder sortOrder;
		
	public Fabricante getFabricante() {
		return fabricante;
	}
	public void setFabricante(Fabricante fabricante) {
		this.fabricante = fabricante;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public BigDecimal getVazao() {
		return vazao;
	}
	public void setVazao(BigDecimal vazao) {
		this.vazao = vazao;
	}
	public BigDecimal getPotencia() {
		return potencia;
	}
	public void setPotencia(BigDecimal potencia) {
		this.potencia = potencia;
	}
	public BigDecimal getRendimento() {
		return rendimento;
	}
	public void setRendimento(BigDecimal rendimento) {
		this.rendimento = rendimento;
	}
	public TipoBomba getTipo() {
		return tipo;
	}
	public void setTipo(TipoBomba tipo) {
		this.tipo = tipo;
	}
	public AcionamentoBomba getAcionamentoBomba() {
		return acionamentoBomba;
	}
	public void setAcionamentoBomba(AcionamentoBomba acionamentoBomba) {
		this.acionamentoBomba = acionamentoBomba;
	}
	
	public String getSortField() {
		return sortField;
	}
	public void setSortField(String sortField) {
		this.sortField = sortField;
	}
	public SortOrder getSortOrder() {
		return sortOrder;
	}
	public void setSortOrder(SortOrder sortOrder) {
		this.sortOrder = sortOrder;
	}

}
