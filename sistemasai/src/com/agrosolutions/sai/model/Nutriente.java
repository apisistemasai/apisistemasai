package com.agrosolutions.sai.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;
import org.primefaces.model.SortOrder;


@Entity
@SequenceGenerator(name = "entity_sequence", sequenceName = "sq_Nutriente", allocationSize = 1)
@Table
@Audited
@NamedQueries({
	@NamedQuery(name = "Nutriente.todos", 
			   query = "SELECT n FROM Nutriente n ORDER BY n.nome ASC"), 			  
	@NamedQuery(name = "Nutriente.excluir", 
			   query = "DELETE FROM Nutriente n WHERE n.id = :pId") 			  
   })
public class Nutriente extends Model {

	private static final long serialVersionUID = -4017552929581575731L;

	@Column(unique=true)
	private String nome;
	@ManyToOne
	private Elemento elemento;
	@Column
	private Integer percentual;
	@Column
	private TipoNutriente tipoNutriente;
	
	@Transient
	private String sortField;
	@Transient
	private SortOrder sortOrder;

	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setSortField(String sortField) {
		this.sortField = sortField;
	}
	public String getSortField() {
		return sortField;
	}
	public void setSortOrder(SortOrder sortOrder) {
		this.sortOrder = sortOrder;
	}
	public SortOrder getSortOrder() {
		return sortOrder;
	}
	public Elemento getElemento() {
		return elemento;
	}
	public void setElemento(Elemento elemento) {
		this.elemento = elemento;
	}
	public Integer getPercentual() {
		return percentual;
	}
	public void setPercentual(Integer percentual) {
		this.percentual = percentual;
	}
	public TipoNutriente getTipoNutriente() {
		return tipoNutriente;
	}
	public void setTipoNutriente(TipoNutriente tipoNutriente) {
		this.tipoNutriente = tipoNutriente;
	}
	
}
