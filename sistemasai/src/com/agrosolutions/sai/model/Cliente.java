package com.agrosolutions.sai.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Classe que representa os clientes 
 * @author janine.freitas 
 *
 */
@Entity
@Table
@SequenceGenerator(name = "entity_sequence", sequenceName = "sq_Cliente", allocationSize = 1)
@NamedQueries({ 			  
	@NamedQuery(name = "Cliente.todos", 
			   query = "SELECT c FROM Cliente c ORDER BY c.nome"), 			  
	@NamedQuery(name = "Cliente.excluir", 
			   query = "DELETE FROM Cliente c WHERE c.id = :pId") 			  
   })
public class Cliente extends Model implements Serializable {

	private static final long serialVersionUID = -879307323066140764L;
	private String codigo;
	private String nome;
	private String cpf;
	private String cnpj;
	private String telefone;
	private String celular;
	private String contato;
	private String observacao;
	
	@OneToOne(cascade = CascadeType.ALL)
	private Endereco endereco = new Endereco();
	
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getCnpj() {
		return cnpj;
	}
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	public String getCelular() {
		return celular;
	}
	public void setCelular(String celular) {
		this.celular = celular;
	}
	public String getContato() {
		return contato;
	}
	public void setContato(String contato) {
		this.contato = contato;
	}
	public String getObservacao() {
		return observacao;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public Endereco getEndereco() {
		return endereco;
	}
	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}
	
}

