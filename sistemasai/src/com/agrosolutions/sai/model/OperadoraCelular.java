package com.agrosolutions.sai.model;

import com.agrosolutions.sai.util.ResourceBundle;

public enum OperadoraCelular {

	NENHUM(0, "nenhum"),
	TIM(1, "tim"),
	CLARO(2, "claro"),
	VIVO(3, "vivo"),
	NEXTEL(4, "nextel"),
	OI(5, "oi");
	
	private Integer id;
	
	private String descricao;

	private OperadoraCelular(Integer id, String descricao){
		this.id = id;
		this.descricao = descricao;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the descricao
	 */
	public String getDescricao() {
		return ResourceBundle.getMessage(descricao);
	}

	/**
	 * @param descricao the descricao to set
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}	
}
