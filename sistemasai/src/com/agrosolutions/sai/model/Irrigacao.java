package com.agrosolutions.sai.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;
import org.primefaces.model.SortOrder;

@Entity
@SequenceGenerator(name = "entity_sequence", sequenceName = "sq_Irrigacao", allocationSize = 1)
@Table
@Audited
@NamedQueries({
	@NamedQuery(name = "Irrigacao.pesquisarPorIrrigantePeriodo", 
			   query = "SELECT i FROM Irrigacao i join i.plantio p join p.setor s join s.irrigante ir WHERE i.data >= :pDataIni AND i.data <= :pDataFim AND ir = :pIrrigante ORDER BY i.data ASC"),
	@NamedQuery(name = "Irrigacao.pesquisarPorIrriganteData", 
			   query = "SELECT i FROM Irrigacao i join i.plantio p join p.setor s join s.irrigante ir WHERE i.data = :pData AND ir = :pIrrigante ORDER BY ir.id, s.id ASC"),
	@NamedQuery(name = "Irrigacao.SMSTI", 
			   query = "SELECT i FROM Irrigacao i join i.plantio p join p.setor s join s.irrigante ir join p.cultivo c WHERE i.data = :pData and ir.celular is not null and ir.celular <> '' ORDER BY ir.celular, ir.localizacao, i.tempoIrrigacao DESC, c.variedade, s.identificacao ASC"),
	@NamedQuery(name = "Irrigacao.EmailTI", 
			   query = "SELECT i FROM Irrigacao i join i.plantio p join p.setor s join s.irrigante ir join p.cultivo c join ir.usuario u WHERE i.data = :pData and u.email is not null and u.email <> '' ORDER BY u.email, ir.localizacao, i.tempoIrrigacao DESC, c.variedade, s.identificacao ASC"),
	@NamedQuery(name = "Irrigacao.pesquisar", 
			   query = "SELECT i FROM Irrigacao i join i.plantio p WHERE p = :pPlantio AND i.data = :pData"),
	@NamedQuery(name = "Irrigacao.pesquisarPorPlantio", 
			   query = "SELECT i FROM Irrigacao i join i.plantio p WHERE p = :pPlantio ORDER BY i.data DESC"),
	@NamedQuery(name = "Irrigacao.deletar", 
			   query = "DELETE Irrigacao i WHERE i.plantio = :pPlantio"),
	@NamedQuery(name = "Irrigacao.excluir", 
			   query = "UPDATE FROM Irrigacao i SET i.enviarSms = false, i.enviarEmail = false, i.motivo = :pMotivo WHERE i.id = :pId"),
	@NamedQuery(name = "Irrigacao.enviarSMS", 
			   query = "UPDATE FROM Irrigacao i SET i.enviarSms = true WHERE i.id = :pId"),
   })
public class Irrigacao extends Model {

	private static final long serialVersionUID = 8996583522513942096L;
	@Temporal(TemporalType.DATE)
	private Date data;
	private EstadioCultivo estadio;
	private BigDecimal kc;
	private BigDecimal eto;
	private BigDecimal etc;
	private BigDecimal chuva;
	private BigDecimal eficiencia;
	private BigDecimal tempoIrrigacao;
	private BigDecimal z; // Profundidade de enraizamento
	@ManyToOne
	private Plantio plantio;
	private String motivo;
	private Boolean smsEnviado;
	private Boolean emailEnviado;
	@Column(nullable = false,  columnDefinition = "boolean default false")
	private Boolean enviarSms;
	@Column(nullable = false,  columnDefinition = "boolean default false")
	private Boolean enviarEmail;
	
	@Column(length=800)
	private String memoriaCalculo;
	
	@Transient
	private BigDecimal irrigacao;
	@Transient
	private Boolean temIrrigacao;
	@Transient
	private String sortField;
	@Transient
	private SortOrder sortOrder;
	
	public Irrigacao() {
		super();
	}
	public Irrigacao(BigDecimal eto, BigDecimal chuvaEfetiva, Plantio p, Date date, boolean enviarSms, boolean smsEnviado, boolean enviarEmail, boolean emailEnviado) {
		super();
		this.eto = eto;
		chuva = chuvaEfetiva;
		plantio = p;
		data = date;
		this.smsEnviado = smsEnviado;
		this.enviarSms = enviarSms;
		this.emailEnviado = emailEnviado;
		this.enviarEmail = enviarEmail;
}
	public Date getData() {
		return data;
	}
	public void setData(Date data) {
		this.data = data;
	}
	public EstadioCultivo getEstadio() {
		return estadio;
	}
	public void setEstadio(EstadioCultivo estadio) {
		this.estadio = estadio;
	}
	public BigDecimal getKc() {
		return kc;
	}
	public void setKc(BigDecimal kc) {
		this.kc = kc;
	}
	public BigDecimal getEto() {
		return eto;
	}
	public void setEto(BigDecimal eto) {
		this.eto = eto;
	}
	public BigDecimal getEtc() {
		if (kc != null && eto != null) {
			etc = kc.multiply(eto);
		}
		return etc;
	}
	public void setEtc(BigDecimal etc) {
		this.etc = etc;
	}
	public BigDecimal getChuva() {
		return chuva;
	}
	public void setChuva(BigDecimal chuva) {
		this.chuva = chuva;
	}
	public BigDecimal getIrrigacao() {
		if (etc != null && chuva != null) {
			irrigacao = etc.subtract(chuva);
		}
		return irrigacao;
	}
	public void setIrrigacao(BigDecimal irrigacao) {
		this.irrigacao = irrigacao;
	}
	public BigDecimal getTempoIrrigacao() {
		return tempoIrrigacao;
	}
	public void setTempoIrrigacao(BigDecimal tempoIrrigacao) {
		this.tempoIrrigacao = tempoIrrigacao;
	}
	public BigDecimal getEficiencia() {
		return eficiencia;
	}
	public void setEficiencia(BigDecimal eficiencia) {
		this.eficiencia = eficiencia;
	}
	public Boolean getTemIrrigacao() {
		if (tempoIrrigacao.compareTo(new BigDecimal(240)) < 0) {
			temIrrigacao = true;
		}else{
			temIrrigacao = false;
		}
		return temIrrigacao;
	}
	public void setPlantio(Plantio plantio) {
		this.plantio = plantio;
	}
	public Plantio getPlantio() {
		return plantio;
	}
	public String getSortField() {
		return sortField;
	}
	public void setSortField(String sortField) {
		this.sortField = sortField;
	}
	public Boolean getSmsEnviado() {
		return smsEnviado;
	}
	public void setSmsEnviado(Boolean smsEnviado) {
		this.smsEnviado = smsEnviado;
	}
	public Boolean getEnviarSms() {
		return enviarSms==null?true:enviarSms;
	}
	public void setEnviarSms(Boolean enviarSms) {
		this.enviarSms = enviarSms;
	}
	public Boolean getEmailEnviado() {
		return emailEnviado;
	}
	public void setEmailEnviado(Boolean emailEnviado) {
		this.emailEnviado = emailEnviado;
	}
	public Boolean getEnviarEmail() {
		return enviarEmail==null?true:enviarEmail;
	}
	public void setEnviarEmail(Boolean enviarEmail) {
		this.enviarEmail = enviarEmail;
	}
	public SortOrder getSortOrder() {
		return sortOrder;
	}
	public void setSortOrder(SortOrder sortOrder) {
		this.sortOrder = sortOrder;
	}
	public String getTempo(){
		if (tempoIrrigacao.intValue()<0) {
			return "N�o irrigar";
		}
		int minutos = tempoIrrigacao.intValue();   
		int minuto = minutos % 60;   
		int hora = minutos / 60;   
		String hms = String.format ("%02dh:%02dm", hora, minuto);   
		return (hms);  
	}
	public String getMemoriaCalculo() {
		return memoriaCalculo;
	}
	public void setMemoriaCalculo(String memoriaCalculo) {
		this.memoriaCalculo = memoriaCalculo;
	}
	public BigDecimal getZ() {
		return z;
	}
	public void setZ(BigDecimal z) {
		this.z = z;
	}
	
	public String getMotivo() {
		return motivo;
	}
	
	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}

}
