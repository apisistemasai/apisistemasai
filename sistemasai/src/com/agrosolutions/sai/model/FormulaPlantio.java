package com.agrosolutions.sai.model;

import java.math.BigDecimal;

public class FormulaPlantio {

	private BigDecimal vazao;
	private BigDecimal diametro;
	private TipoEmissor tipoEmissor;
	private FormulasTI formulaTI;

	
	public FormulaPlantio() {
		super();
		this.vazao = new BigDecimal(0);
		this.diametro = new BigDecimal(0);
	}
	public BigDecimal getVazao() {
		return vazao;
	}
	public void setVazao(BigDecimal vazao) {
		this.vazao = vazao;
	}
	public BigDecimal getDiametro() {
		return diametro;
	}
	public void setDiametro(BigDecimal raio) {
		this.diametro = raio;
	}
	public TipoEmissor getTipoEmissor() {
		return tipoEmissor;
	}
	public void setTipoEmissor(TipoEmissor tipoEmissor) {
		this.tipoEmissor = tipoEmissor;
	}
	public FormulasTI getFormulaTI() {
		return formulaTI;
	}
	public void setFormulaTI(FormulasTI formulaTI) {
		this.formulaTI = formulaTI;
	}

}
