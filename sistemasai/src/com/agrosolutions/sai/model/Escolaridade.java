package com.agrosolutions.sai.model;

import com.agrosolutions.sai.util.ResourceBundle;

public enum Escolaridade {

	SEM_ESCOLARIDADE(0, "semEscolaridade"),
	NIVEL_FUNDAMENTAL_INCOMPLETO(1, "nivelFundamentalIncompleto"),
	NIVEL_FUNDAMENTAL_COMPLETO(2, "nivelFundamentalCompleto"),
	NIVEL_MEDIO_INCOMPLETO(3, "nivelMedioIncompleto"),
	NIVEL_MEDIO_COMPLETO(4, "nivelMedioCompleto"),
	NIVEL_SUPERIOR_INCOMPLETO(5, "nivelSuperiorIncompleto"),
	NIVEL_SUPERIOR_COMPLETO(6, "nivelSuperiorCompleto"),
	POS_GRADUACAO(7, "posGraduacao"),
	MESTRADO(8, "mestrado"),
	DOUTORADO(9, "doutorado"),
	PHD(10, "phd");
	
	private Integer id;
	
	private String descricao;

	private Escolaridade(Integer id, String descricao){
		this.id = id;
		this.descricao = descricao;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the descricao
	 */
	public String getDescricao() {
		return ResourceBundle.getMessage(descricao);
	}

	/**
	 * @param descricao the descricao to set
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}	
}
