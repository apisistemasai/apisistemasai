package com.agrosolutions.sai.model;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.envers.Audited;

import com.agrosolutions.sai.util.BigDecimalUtil;
import com.agrosolutions.sai.util.DateUtils;

@Entity
@SequenceGenerator(name = "entity_sequence", sequenceName = "sq_Cultivo", allocationSize = 1)
@Table
@Audited
@BatchSize(size=40)
@NamedQueries({
	@NamedQuery(name = "Cultivo.excluir", 
			   query = "DELETE FROM Cultivo c WHERE c.id = :pId"), 			  
	@NamedQuery(name = "Cultivo.pesquisar", 
			   query = "SELECT DISTINCT c FROM Cultivo c join c.plantio p WHERE c.irrigante = :pIrrigante AND p.dataFim is null ORDER BY c.dataInicio ASC"), 			  
	@NamedQuery(name = "Cultivo.pesquisarCulturas", 
			   query = "SELECT c.cultura FROM Cultivo c WHERE c.irrigante = :pIrrigante ORDER BY c.dataInicio ASC"), 			  
	@NamedQuery(name = "Cultivo.pesquisarVariedades", 
			   query = "SELECT c.variedade FROM Cultivo c WHERE c.irrigante = :pIrrigante ORDER BY c.dataInicio ASC") 			  
   })
public class Cultivo extends Model {

	private static final long serialVersionUID = 2488137953471709359L;
	@ManyToOne
	private Irrigante irrigante;
	@ManyToOne
	private Cultura cultura;
	@ManyToOne
	private Variedade variedade;
	@ManyToOne(fetch=FetchType.EAGER)
	private Referencia referencia;
	@Temporal(TemporalType.DATE)
	private Date dataInicio;
	@Temporal(TemporalType.DATE)
	private Date dataFim;
	@Column(length=800)
	private String observacao;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy="cultivo")
	@Fetch(FetchMode.SUBSELECT)
	@BatchSize(size=30)
	private List<Plantio> plantio;
	
	private Long qtdDiasCultivo;
	@Transient
	private Boolean usarDataClimaParaCalcKc;
	@Transient
	private Date dataClima;
	
	public Cultivo() {
		super();
	}
	
	public Cultivo(Irrigante irrigante) {
		super();
		this.irrigante = irrigante;
	}

	public Irrigante getIrrigante() {
		return irrigante;
	}

	public void setIrrigante(Irrigante irrigante) {
		this.irrigante = irrigante;
	}

	public Cultura getCultura() {
		return cultura;
	}

	public void setCultura(Cultura cultura) {
		this.cultura = cultura;
	}

	public Variedade getVariedade() {
		return variedade;
	}

	public void setVariedade(Variedade variedade) {
		this.variedade = variedade;
	}

	public Date getDataInicio() {
		return dataInicio;
	}

	public String getDataInicioFormatada() {
		Calendar data = new GregorianCalendar();
		data.setTime(dataInicio);
		
		return data.get(Calendar.DAY_OF_MONTH) + "/" + (data.get(Calendar.MONTH)+1) + "/" + data.get(Calendar.YEAR);
	}

	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public List<Plantio> getPlantio() {
		return plantio;
	}

	public void setPlantio(List<Plantio> plantio) {
		this.plantio = plantio;
	}

	public Date getDataFim() {
		return dataFim;
	}

	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}
	
	public Referencia getReferencia() {
		return referencia;
	}

	public void setReferencia(Referencia referencia) {
		this.referencia = referencia;
	}
	//Fun��o que verificar se � estadio atual quando os dias lido ultrapassa o dia atual.
	private boolean verificarEstadio(Long dia, Long diasLido){
		if (dia <= diasLido ) {
			return true;
		}else{
			return false;
		}
	}
	
	public Estadio getEstadioAtual() {
		Long diaAtual;
		if ( usarDataClimaParaCalcKc != null && usarDataClimaParaCalcKc) {
			diaAtual = DateUtils.diferencaDeDias(getDataInicio(), this.dataClima);
		} else {
			diaAtual = DateUtils.diferencaDeDias(getDataInicio(), new Date());
		}
		
		boolean poda = false;
		boolean posPoda = false;
		int ordemPosPoda = 0;
		int indexPosPoda = 0;
		
		Estadio estadioAtual = null;
		Long totalDiasLido = 0L;
		for (int i = 0; i < getReferencia().getEstadios().size(); i++) {
			Estadio e = getReferencia().getEstadios().get(i);
			 e.setQtdDiasAtual(diaAtual - totalDiasLido);
			 totalDiasLido = totalDiasLido + e.getQtdDias();

			 //Se o estadio for pos poda devemos pegar o index dele
			 if (e.getPosPoda()) {
				 indexPosPoda = i;
			 }
			 //Se a poda tiver sido encontrada
			 if (poda) {
				 if (e.getPosPoda()) {
					 ordemPosPoda = e.getOrdem();			 
					 posPoda = true;
					 if (verificarEstadio(diaAtual, totalDiasLido)){
						 estadioAtual = e;
						 break;
					 }else{
						 continue;
					 }
				 }else{
					 if (posPoda && e.getOrdem() > ordemPosPoda){
						 if (verificarEstadio(diaAtual, totalDiasLido)){
							 estadioAtual = e;
							 break;
						 }else{
							 continue;
						 }
					 }else{
						 continue;
					 }
				 }
			  }else{
				 //Se o estadio for poda devemos marcar que a poda foi encontrada e pegar o indice dela
				 if (e.getPoda()) {
					 poda = true;
					 i = indexPosPoda;
					 if (verificarEstadio(diaAtual, totalDiasLido)){
						 estadioAtual = e;
						 break;
					 }else{
						 continue;
					 }
				 }else{
					 if (i == getReferencia().getEstadios().size()-1 && !e.getPoda()){
						 estadioAtual = e;
						 break;
					 }else{
						 if (verificarEstadio(diaAtual, totalDiasLido)){
							 estadioAtual = e;
							 break;
						 }else{
							 continue;
						 }
					 }
				 }
			  }
		}
		return estadioAtual;
	}
	
	public Estadio getEstadioProximo() {
		Estadio estadioAtual = getEstadioAtual();
		Estadio estadioProximo = null;
		
		for (Estadio e : getReferencia().getEstadios()) {
			if (e.getOrdem() > estadioAtual.getOrdem()){
				estadioProximo = e;
			}	
		}
		return estadioProximo;
	}
		
	public Estadio getEstadioAnterior() {
		Estadio estadioAtual = getEstadioAtual();
		Estadio estadioAnterior = null;
		
		for (Estadio e : getReferencia().getEstadios()) {
			if (e.getOrdem() < estadioAtual.getOrdem()){
				estadioAnterior = e;
			}	
			if (e.getOrdem() == estadioAtual.getOrdem()) {
				break;
			}
		}
		return estadioAnterior;
	}

	public Estadio getEstadioProximoKc() {
		Estadio estadioAtual = getEstadioAtual();
		Estadio estadioProximo = null;
		
		for (Estadio e : getReferencia().getEstadios()) {
			if (e.getOrdem() > estadioAtual.getOrdem() && e.getKc() != null){
				estadioProximo = e;
				break;
			}	
		}
		return estadioProximo;
	}
		
	public Estadio getEstadioAnteriorKc() {
		Estadio estadioAtual = getEstadioAtual();
		Estadio estadioAnterior = null;
		for (Estadio e : getReferencia().getEstadios()) {
			if (e.getOrdem() < estadioAtual.getOrdem() && e.getKc() != null){
				estadioAnterior = e;
			}	
			if (e.getOrdem() == estadioAtual.getOrdem()) {
				break;
			}
		}
		return estadioAnterior;
	}
	
	public Estadio getEstadioAnteriorRaiz() {
		Estadio estadioAtual = getEstadioAtual();
		Estadio estadioAnterior = null;
		
		for (Estadio e : getReferencia().getEstadios()) {
			if (e.getOrdem() < estadioAtual.getOrdem() && e.getProfundidadeRaiz() != null){
				estadioAnterior = e;
				break;
			}	
			if (e.getOrdem() == estadioAtual.getOrdem()) {
				estadioAnterior = e;
				break;
			}
		}
		return estadioAnterior;
	}
	
	public Estadio getEstadioProximoRaiz() {
		Estadio estadioAtual = getEstadioAtual();
		Estadio estadioProximo = null;
		
		for (Estadio e : getReferencia().getEstadios()) {
			if (e.getOrdem() > estadioAtual.getOrdem() && e.getProfundidadeRaiz() != null && (e.getProfundidadeRaiz().compareTo(BigDecimal.ZERO) != 0) ){
				estadioProximo = e;
				break;
			}
		}
		
		return estadioProximo;
	}

	public BigDecimal getValorDc() {
		Estadio atual = getEstadioAtual();
		BigDecimal dc = new BigDecimal(0);
		BigDecimal dcAnterior = new BigDecimal(0);
		BigDecimal dcProximo = new BigDecimal(0);
		Integer ordem = 0;
		
		if (atual.getDiametroCopa() != null && !atual.getDiametroCopa().equals(new BigDecimal(0))) {
			dc = atual.getDiametroCopa();
		}else{
			for (Estadio e : getReferencia().getEstadios()) {
				if (e.getOrdem() <= atual.getOrdem() && e.getDiametroCopa() != null){
					dcAnterior = e.getDiametroCopa();
					ordem = e.getOrdem(); 
				}
				if (e.getOrdem() == atual.getOrdem()) {
					break;
				}else{
					if (e.getDiametroCopa() == null && atual.getOrdem() - ordem > 1) {
						atual.setQtdDiasAtual(atual.getQtdDiasAtual()+ e.getQtdDias());
					}
				}
			}
			int qtd = 1;
			//Calcular diferen�a do Diametro de Copa
			for (Estadio e : getReferencia().getEstadios()) {
				if (e.getOrdem() > ordem) {
					if (e.getDiametroCopa() != null) {
						dcProximo = e.getDiametroCopa();
						break;
					}else{
						qtd = qtd + e.getQtdDias().intValue();
					}
				}
			}
			BigDecimal diferenca = dcProximo.subtract(dcAnterior);
			BigDecimal divide = diferenca.divide(new BigDecimal(qtd), BigDecimalUtil.MC_RUP);
			dc = divide.multiply(new BigDecimal(atual.getQtdDiasAtual())).add(dcAnterior);			
		}
	
		return dc;
	}


	public Long getQtdDiasCultivo() {
		if ( getDataInicio() != null ) {			
			this.qtdDiasCultivo = DateUtils.diferencaDeDias(getDataInicio(), new Date());
		}
		return qtdDiasCultivo;
	}

	public void setQtdDiasCultivo(Long qtdDiasCultivo) {
		this.qtdDiasCultivo = qtdDiasCultivo;
	}

	public Boolean getUsarDataClimaParaCalcKc() {
		return usarDataClimaParaCalcKc;
	}

	public void setUsarDataClimaParaCalcKc(Boolean usarDataClimaParaCalcKc) {
		this.usarDataClimaParaCalcKc = usarDataClimaParaCalcKc;
	}

	public Date getDataClima() {
		return dataClima;
	}

	public void setDataClima(Date dataClima) {
		this.dataClima = dataClima;
	}
}
