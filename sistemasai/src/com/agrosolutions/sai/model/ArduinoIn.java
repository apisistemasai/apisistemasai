package com.agrosolutions.sai.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.envers.Audited;

@Entity
@SequenceGenerator(name = "entity_sequence", sequenceName = "sq_ArduinoIn", allocationSize = 1)
@Table
@Audited
public class ArduinoIn extends Model implements Serializable {
	
	private static final long serialVersionUID = 9193795614031747020L;

	@ManyToOne(cascade = CascadeType.ALL)
	@Fetch(FetchMode.JOIN)
	private Irrigante irrigante;

	@Temporal(TemporalType.TIMESTAMP)
	private Date data;
	
	private BigDecimal volume;
	
	
	public Irrigante getIrrigante() {
		return irrigante;
	}

	public void setIrrigante(Irrigante irrigante) {
		this.irrigante = irrigante;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public BigDecimal getVolume() {
		return volume;
	}

	public void setVolume(BigDecimal volume) {
		this.volume = volume;
	}

}