package com.agrosolutions.sai.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;
import org.primefaces.model.SortOrder;

@Entity
@SequenceGenerator(name = "entity_sequence", sequenceName = "sq_Tubo", allocationSize = 1)
@Table
@Audited
@NamedQueries({
	@NamedQuery(name = "Tubo.excluir", 
			   query = "DELETE FROM Tubo t WHERE t.id = :pId"), 			  
	@NamedQuery(name = "Tubo.todos", 
			   query = "SELECT t FROM Tubo t ORDER BY t.modelo ASC"), 			  
	@NamedQuery(name = "Tubo.pesquisar", 
			   query = "SELECT t FROM Tubo t WHERE t.fabricante = :pFabricante ORDER BY t.modelo ASC"), 			  
	@NamedQuery(name = "Tubo.pesquisarPorIrrigante", 
			   query = "SELECT t FROM Irrigante i join i.tubos t WHERE i = :pIrrigante ORDER BY t.modelo ASC") 			  
   })
public class Tubo extends Model {

	private static final long serialVersionUID = -5241609665194676538L;
	@ManyToOne
	private Fabricante fabricante;
	@Column(unique=true)
	private String modelo;
	private TipoTubo tipo;
	private BigDecimal vazao;
	private BigDecimal pressao;
	private BigDecimal diametrointerno;
	private BigDecimal diametroexterno;
	@Transient
	private String sortField;
	@Transient
	private SortOrder sortOrder;

	public Fabricante getFabricante() {
		return fabricante;
	}
	public void setFabricante(Fabricante fabricante) {
		this.fabricante = fabricante;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public TipoTubo getTipo() {
		return tipo;
	}
	public void setTipo(TipoTubo tipo) {
		this.tipo = tipo;
	}
	public BigDecimal getVazao() {
		return vazao;
	}
	public void setVazao(BigDecimal vazao) {
		this.vazao = vazao;
	}
	public BigDecimal getPressao() {
		return pressao;
	}
	public void setPressao(BigDecimal pressao) {
		this.pressao = pressao;
	}
	public BigDecimal getDiametrointerno() {
		return diametrointerno;
	}
	public void setDiametrointerno(BigDecimal diametrointerno) {
		this.diametrointerno = diametrointerno;
	}
	public BigDecimal getDiametroexterno() {
		return diametroexterno;
	}
	public String getSortField() {
		return sortField;
	}
	public void setSortField(String sortField) {
		this.sortField = sortField;
	}
	public SortOrder getSortOrder() {
		return sortOrder;
	}
	public void setSortOrder(SortOrder sortOrder) {
		this.sortOrder = sortOrder;
	}
	public void setDiametroexterno(BigDecimal diametroexterno) {
		this.diametroexterno = diametroexterno;
	}	

}
