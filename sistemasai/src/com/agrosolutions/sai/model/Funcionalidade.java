package com.agrosolutions.sai.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.BatchSize;
import org.hibernate.envers.Audited;
import org.springframework.security.core.GrantedAuthority;


/**
 * Classe que representa uma funcionalidade do sistema. 
 * 
 * @author Raphael Ferreira 
 *
 */
@Entity
@SequenceGenerator(name = "entity_sequence", sequenceName = "sq_Funcionalidade")
@Table
@Audited
@BatchSize(size=20)
@NamedQueries({
	@NamedQuery(name = "Funcionalidade.todos", 
			   query = "SELECT f FROM Funcionalidade f ORDER BY ordem ASC") 			  
   })
public class Funcionalidade extends Model implements GrantedAuthority {
	
	private static final long serialVersionUID = 5159543555133136465L;

	private String descricao;
	private Integer ordem;
	@Column(unique = true)
	private String mnemonico;

	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public Integer getOrdem() {
		return ordem;
	}
	public void setOrdem(Integer ordem) {
		this.ordem = ordem;
	}
	public String getMnemonico() {
		return mnemonico;
	}
	public void setMnemonico(String mnemonico) {
		this.mnemonico = mnemonico;
	}

	/**
	 * Retorna o c�digo da permiss�o, que � utilizado no sistema.
	 */
	@Override
	public String getAuthority() {
		return mnemonico;
	}
}
