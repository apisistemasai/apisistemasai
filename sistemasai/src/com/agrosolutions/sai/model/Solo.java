package com.agrosolutions.sai.model;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;
import org.primefaces.model.SortOrder;

@Entity
@SequenceGenerator(name = "entity_sequence", sequenceName = "sq_Solo", allocationSize = 1)
@Table
@Audited
@NamedQueries({
	@NamedQuery(name = "Solo.excluir", 
			   query = "DELETE FROM Solo s WHERE s.id = :pId"),
	@NamedQuery(name = "Solo.todos", 
			   query = "SELECT s FROM Solo s ORDER BY s.nome ASC"), 			  
	@NamedQuery(name = "Solo.pesquisar", 
			   query = "SELECT s FROM Solo s WHERE s.nome = :pNome") 			  
   })
public class Solo extends Model {

	private static final long serialVersionUID = -2356833291832677976L;
	private String nome;
	private TipoSolo tipoSolo;
	private BigDecimal cc;						//Capacidade de campo
	private BigDecimal pmp;						//Ponto de mucha permanente
	private BigDecimal maxTaxaInfiltracaoChuva;	//Maxima taxa de infiltração da chuva
	private BigDecimal massaSolo;				//Massa do SOlo
	private BigDecimal esgotamento;				//Esgotamento de umidade inicial do solo
	private BigDecimal umidadeInicial;			//Umidade inicial do solo disponíveis
	
	@Transient
	private String sortField;
	@Transient
	private SortOrder sortOrder;
	
	public TipoSolo getTipoSolo() {
		return tipoSolo;
	}
	public void setTipoSolo(TipoSolo tipoSolo) {
		this.tipoSolo = tipoSolo;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public BigDecimal getCc() {
		return cc;
	}
	public void setCc(BigDecimal cc) {
		this.cc = cc;
	}
	public BigDecimal getPmp() {
		return pmp;
	}
	public void setPmp(BigDecimal pmp) {
		this.pmp = pmp;
	}
	public BigDecimal getMaxTaxaInfiltracaoChuva() {
		return maxTaxaInfiltracaoChuva;
	}
	public void setMaxTaxaInfiltracaoChuva(BigDecimal maxTaxaInfiltracaoChuva) {
		this.maxTaxaInfiltracaoChuva = maxTaxaInfiltracaoChuva;
	}
	public BigDecimal getMassaSolo() {
		return massaSolo;
	}
	public void setMassaSolo(BigDecimal massaSolo) {
		this.massaSolo = massaSolo;
	}
	public BigDecimal getEsgotamento() {
		return esgotamento;
	}
	public void setEsgotamento(BigDecimal esgotamento) {
		this.esgotamento = esgotamento;
	}
	public BigDecimal getUmidadeInicial() {
		return umidadeInicial;
	}
	public void setUmidadeInicial(BigDecimal umidadeInicial) {
		this.umidadeInicial = umidadeInicial;
	}
	public void setSortField(String sortField) {
		this.sortField = sortField;
	}
	public String getSortField() {
		return sortField;
	}
	public void setSortOrder(SortOrder sortOrder) {
		this.sortOrder = sortOrder;
	}
	public SortOrder getSortOrder() {
		return sortOrder;
	}
}
