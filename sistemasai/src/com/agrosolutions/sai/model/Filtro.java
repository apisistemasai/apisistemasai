package com.agrosolutions.sai.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;
import org.primefaces.model.SortOrder;

@Entity
@SequenceGenerator(name = "entity_sequence", sequenceName = "sq_Filtro", allocationSize = 1)
@Table
@Audited
@NamedQueries({
	@NamedQuery(name = "Filtro.excluir", 
			   query = "DELETE FROM Filtro f WHERE f.id = :pId"), 			  
	@NamedQuery(name = "Filtro.todos", 
			   query = "SELECT f FROM Filtro f ORDER BY f.modelo ASC"), 			  
	@NamedQuery(name = "Filtro.pesquisar", 
			   query = "SELECT f FROM Filtro f WHERE f.fabricante = :pFabricante ORDER BY f.modelo ASC"), 			  
	@NamedQuery(name = "Filtro.pesquisarPorIrrigante", 
			   query = "SELECT f FROM Irrigante i join i.filtros f WHERE i = :pIrrigante ORDER BY f.modelo ASC") 			  
   })
public class Filtro extends Model {

	private static final long serialVersionUID = -7923442385450460292L;
	@ManyToOne
	private Fabricante fabricante;
	@Column(unique=true)
	private String modelo;
	private TipoFiltro tipoFiltro;
	private BigDecimal filtragem;
	private BigDecimal pressao;
	private Integer elemento;
	
	@Transient
	private String sortField;
	@Transient
	private SortOrder sortOrder;
	
	public Fabricante getFabricante() {
		return fabricante;
	}
	public void setFabricante(Fabricante fabricante) {
		this.fabricante = fabricante;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public BigDecimal getFiltragem() {
		return filtragem;
	}
	public void setFiltragem(BigDecimal filtragem) {
		this.filtragem = filtragem;
	}
	public BigDecimal getPressao() {
		return pressao;
	}
	public void setPressao(BigDecimal pressao) {
		this.pressao = pressao;
	}
	public Integer getElemento() {
		return elemento;
	}
	public void setElemento(Integer elemento) {
		this.elemento = elemento;
	}
	public void setSortField(String sortField) {
		this.sortField = sortField;
	}
	public String getSortField() {
		return sortField;
	}
	public void setSortOrder(SortOrder sortOrder) {
		this.sortOrder = sortOrder;
	}
	public SortOrder getSortOrder() {
		return sortOrder;
	}
	public void setTipoFiltro(TipoFiltro tipoFiltro) {
		this.tipoFiltro = tipoFiltro;
	}
	public TipoFiltro getTipoFiltro() {
		return tipoFiltro;
	}
	
}
