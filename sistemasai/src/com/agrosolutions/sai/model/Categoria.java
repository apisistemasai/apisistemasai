package com.agrosolutions.sai.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Classe que representa as categorias de contas 
 * @author raphael.ferreira
 *
 */
@Entity
@Table
@SequenceGenerator(name = "entity_sequence", sequenceName = "sq_Categoria", allocationSize = 1)
@NamedQueries({
	@NamedQuery(name = "Categoria.pai", 
			   query = "SELECT c FROM Categoria c WHERE c.tipo.id = 2 ORDER BY c.nome"), 			  
	@NamedQuery(name = "Categoria.todos", 
			   query = "SELECT c FROM Categoria c ORDER BY c.nome"), 			  
	@NamedQuery(name = "Categoria.excluir", 
			   query = "DELETE FROM Categoria c WHERE c.id = :pId") 			  
   })
public class Categoria extends Model implements Serializable {

	private static final long serialVersionUID = -879307323066140764L;
	private String codigo;
	private String nome;
	private Integer ordem;
	private Categoria pai;
	private TipoCategoria tipo;
	
	
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Integer getOrdem() {
		return ordem;
	}
	public void setOrdem(Integer ordem) {
		this.ordem = ordem;
	}
	public Categoria getPai() {
		return pai;
	}
	public void setPai(Categoria pai) {
		this.pai = pai;
	}
	public TipoCategoria getTipo() {
		return tipo;
	}
	public void setTipo(TipoCategoria tipo) {
		this.tipo = tipo;
	}
}
