package com.agrosolutions.sai.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.BatchSize;
import org.hibernate.envers.Audited;
import org.primefaces.model.SortOrder;

@Entity
@SequenceGenerator(name = "entity_sequence", sequenceName = "sq_Cultura", allocationSize = 1)
@Table
@BatchSize(size=40)
@Audited
@NamedQueries({
	@NamedQuery(name = "Cultura.todos",  
			   query = "SELECT c FROM Cultura c ORDER BY c.nome ASC"), 			  
	@NamedQuery(name = "Cultura.excluir", 
			   query = "DELETE FROM Cultura c WHERE c.id = :pId")
   })
public class Cultura extends Model {

	private static final long serialVersionUID = -5301045033069126100L;
	@Column(unique=true)
	private String nome;
	@ManyToOne
	private Atividade atividade;
	private String image;

	@Transient
	private String sortField;
	@Transient
	private SortOrder sortOrder;
	
	public Cultura() {
		super();
		image = "cultura.jpg";
	}
 
	@OneToMany(cascade = CascadeType.ALL, mappedBy="cultura")
	@BatchSize(size=10)
	private List<Variedade> variedades;
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public List<Variedade> getVariedades() {
		return variedades;
	}
	public void setVariedades(List<Variedade> variedades) {
		this.variedades = variedades;
	}
	public Atividade getAtividade() {
		return atividade;
	}
	public void setAtividade(Atividade atividade) {
		this.atividade = atividade;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getImage() {
		return image;
	}
	public String getSortField() {
		return sortField;
	}
	public void setSortField(String sortField) {
		this.sortField = sortField;
	}
	public SortOrder getSortOrder() {
		return sortOrder;
	}
	public void setSortOrder(SortOrder sortOrder) {
		this.sortOrder = sortOrder;
	}

}
