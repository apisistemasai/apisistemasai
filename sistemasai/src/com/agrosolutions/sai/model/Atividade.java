package com.agrosolutions.sai.model;

import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;
import org.primefaces.model.SortOrder;

@Entity
@SequenceGenerator(name = "entity_sequence", sequenceName = "sq_Atividade", allocationSize = 1)
@Table
@Audited
@NamedQueries({
	@NamedQuery(name = "Atividade.todos", 
			   query = "SELECT a FROM Atividade a ORDER BY NOME ASC"), 			  
	@NamedQuery(name = "Atividade.excluir", 
			   query = "DELETE FROM Atividade a WHERE a.id = :pId") 			  
   })
public class Atividade extends Model {

	private static final long serialVersionUID = 1566315074224178619L;
	private String nome;
	
	@Transient
	private String sortField;
	@Transient
	private SortOrder sortOrder;
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setSortField(String sortField) {
		this.sortField = sortField;
	}
	public String getSortField() {
		return sortField;
	}
	public void setSortOrder(SortOrder sortOrder) {
		this.sortOrder = sortOrder;
	}
	public SortOrder getSortOrder() {
		return sortOrder;
	}
}
