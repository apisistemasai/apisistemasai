package com.agrosolutions.sai.model;

import java.math.BigDecimal;


public enum PesoIndicadorCiclo {
	
	PERMANENTE(new BigDecimal(0.5), "Permanente" ),
	TEMPOLONGO(new BigDecimal(0.75), "Tempo Longo" ),
	TEMPOCURTO(new BigDecimal(1), "Tempo Curto" );
	
	private BigDecimal valor;
	private String descricao;

	private PesoIndicadorCiclo(BigDecimal valor, String descricao){
		this.setValor(valor);
		this.setDescricao(descricao);
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
}
