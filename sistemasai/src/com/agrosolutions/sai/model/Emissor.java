package com.agrosolutions.sai.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.BatchSize;
import org.hibernate.envers.Audited;
import org.primefaces.model.SortOrder;

@Entity
@SequenceGenerator(name = "entity_sequence", sequenceName = "sq_Emissor", allocationSize = 1)
@Table
@Audited
@BatchSize(size=30)
@NamedQueries({
	@NamedQuery(name = "Emissor.pesquisarPorPlantio", 
			   query = "SELECT e FROM Plantio p join p.emissores e WHERE p = :pPlantio ORDER BY e.modelo ASC"), 			  
	@NamedQuery(name = "Emissor.excluir", 
			   query = "DELETE FROM Emissor e WHERE e.id = :pId"), 			  
	@NamedQuery(name = "Emissor.todos", 
			   query = "SELECT e FROM Emissor e ORDER BY e.modelo ASC"), 			  
	@NamedQuery(name = "Emissor.pesquisarPorSetor", 
			   query = "SELECT e FROM Setor s join s.emissores e WHERE s = :pSetor ORDER BY e.modelo ASC"), 			  
	@NamedQuery(name = "Emissor.pesquisar", 
			   query = "SELECT e FROM Emissor e WHERE e.fabricante = :pFabricante ORDER BY e.modelo ASC") 			  
   })
public class Emissor extends Model {

	private static final long serialVersionUID = 992029385972110765L;
	private TipoEmissor tipoEmissor;
	@ManyToOne
	private Fabricante fabricante;
	@Column(unique=true)
	private String modelo;
	private BigDecimal vazao;
	private BigDecimal pressao;
	private BigDecimal raio;
	@Transient
	private String sortField;
	@Transient
	private SortOrder sortOrder;
		
	public TipoEmissor getTipoEmissor() {
		return tipoEmissor;
	}
	public void setTipoEmissor(TipoEmissor tipoEmissor) {
		this.tipoEmissor = tipoEmissor;
	}
	public Fabricante getFabricante() {
		return fabricante;
	}
	public void setFabricante(Fabricante fabricante) {
		this.fabricante = fabricante;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public BigDecimal getVazao() {
		return vazao;
	}
	public void setVazao(BigDecimal vazao) {
		this.vazao = vazao;
	}
	public BigDecimal getPressao() {
		return pressao;
	}
	public void setPressao(BigDecimal pressao) {
		this.pressao = pressao;
	}
	public BigDecimal getRaio() {
		return raio==null?new BigDecimal(0):raio;
	}
	public void setRaio(BigDecimal raio) {
		this.raio = raio;
	}
	public String getSortField() {
		return sortField;
	}
	public void setSortField(String sortField) {
		this.sortField = sortField;
	}
	public SortOrder getSortOrder() {
		return sortOrder;
	}
	public void setSortOrder(SortOrder sortOrder) {
		this.sortOrder = sortOrder;
	}
}
