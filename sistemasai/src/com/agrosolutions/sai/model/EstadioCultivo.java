package com.agrosolutions.sai.model;

import com.agrosolutions.sai.util.ResourceBundle;

public enum EstadioCultivo {

	INICIAL(0, "inicial"),
	VEGETATIVO(1, "vegetativo"),
	FLORACAO(2, "floracao"),
	FRUTIFICACAO(3,"frutificacao"),
	COLHEITA(4,"colheita"),
	BROTACAO(5, "brotacao"),
	PrimABORTO(6, "primaborto"),
	SegABORTO(7, "segaborto"),
	ENCHIMENTOFRUTO(8, "enchimento"),
	MATURACAO(9, "maturacao"),
	REPOUSO(10, "repouso"),
	STRESS(11, "stress");

	private Integer id;
	
	private String descricao;

	private EstadioCultivo(Integer id, String descricao){
		this.id = id;
		this.descricao = descricao;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the descricao
	 */
	public String getDescricao() {
		return ResourceBundle.getMessage(descricao);
	}

	/**
	 * @param descricao the descricao to set
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}	
	
	/**
	 * M�todo que retorna o valor do enum correspondente a descri��o
	 * 
	 * @param descri��o
	 * @return
	 */
	public static EstadioCultivo fromDescricao(String descricao) {
		for (EstadioCultivo tipo : values()) {
			if (tipo.descricao.toLowerCase().equals(descricao.toLowerCase())) {
				return tipo;
			}
		}
		throw new RuntimeException(EstadioCultivo.class.getName() + " valorIdentificador " +" "
				+ descricao);
	}	
}
