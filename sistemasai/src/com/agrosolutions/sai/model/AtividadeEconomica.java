package com.agrosolutions.sai.model;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.BatchSize;
import org.hibernate.envers.Audited;

@Entity
@SequenceGenerator(name = "entity_sequence", sequenceName = "sq_AtividadeEconomica", allocationSize = 1)
@Table
@Audited
@BatchSize(size = 30)
@NamedQueries({
		@NamedQuery(name = "AtividadeEconomica.excluir", query = "DELETE FROM AtividadeEconomica p WHERE p.id = :pId"),
		@NamedQuery(name = "AtividadeEconomica.ativos", query = "SELECT a FROM AtividadeEconomica a JOIN a.municipio m WHERE m.bacia = :pBacia AND a.dataInicio <= :pData AND (a.dataFim is null OR a.dataFim >= :pData) ORDER BY a.dataInicio ASC"),
		@NamedQuery(name = "AtividadeEconomica.ativos2", query = "SELECT a FROM AtividadeEconomica a JOIN a.municipio m WHERE m.bacia = :pBacia AND a.dataInicio <= :pData AND a.dataFim is null ORDER BY a.dataInicio ASC"),
		@NamedQuery(name = "AtividadeEconomica.pesquisarTodosDaBacia", query = "SELECT a FROM AtividadeEconomica a WHERE a.bacia is not null AND a.dataFim is null ORDER BY a.dataInicio ASC"),
		@NamedQuery(name = "AtividadeEconomica.pesquisarTodosDeMunicipio", query = "SELECT a FROM AtividadeEconomica a WHERE a.municipio is not null AND a.dataFim is null ORDER BY a.dataInicio ASC"),
		@NamedQuery(name = "AtividadeEconomica.pesquisarPorBacia", query = "SELECT DISTINCT a FROM AtividadeEconomica a WHERE a.bacia = :pBacia AND a.dataFim is null ORDER BY a.dataInicio ASC"),
		@NamedQuery(name = "AtividadeEconomica.pesquisarPorMunicipio", query = "SELECT DISTINCT a FROM AtividadeEconomica a WHERE a.municipio = :pMunicipio AND a.dataFim is null ORDER BY a.dataInicio ASC"), 			  
		@NamedQuery(name = "AtividadeEconomica.pesquisarPorVariedades", query = "SELECT a FROM AtividadeEconomica a INNER JOIN a.municipio m WHERE a.municipio is not null AND a.variedade = :pVariedade ORDER BY m.nome ASC"), 			  
		@NamedQuery(name = "AtividadeEconomica.pesquisarCulturas", query = "SELECT a.cultura FROM AtividadeEconomica a WHERE a.municipio = :pMunicipio ORDER BY a.dataInicio ASC"), 			  
		@NamedQuery(name = "AtividadeEconomica.pesquisarVariedades", query = "SELECT a.variedade FROM AtividadeEconomica a WHERE a.municipio = :pMunicipio ORDER BY a.dataInicio ASC") 			  
		})
public class AtividadeEconomica extends Model {

	private static final long serialVersionUID = -6319713566716515458L;

	@ManyToOne
	@BatchSize(size = 30)
	private Municipio municipio;

	@ManyToOne
	@BatchSize(size = 30)
	private BaciaHidrografica bacia;

	@ManyToOne
	private Cultura cultura;

	@ManyToOne
	private Variedade variedade;

	@ManyToOne(fetch=FetchType.EAGER)
	private Referencia referencia;

	@Temporal(TemporalType.DATE)
	private Date dataInicio;

	@Temporal(TemporalType.DATE)
	private Date dataFim;

	@Column(length=800)
	private String observacao;
	
	private BigDecimal areatotal;
	
	private Long qtdDiasCultivo;
	
	@Transient
	private List<Referencia> listaReferencias;

	@Transient
	private List<Variedade> listaVariedades;

	@Transient
	private Boolean incluido; 
	
	public AtividadeEconomica() {
		super();
	}

	public AtividadeEconomica(Municipio municipio) {
		super();
		this.municipio = municipio;
	}

	public AtividadeEconomica(BaciaHidrografica bacia) {
		super();
		this.bacia = bacia;
	}

	public AtividadeEconomica(AtividadeEconomica atividadeEconomica) {
		this.setCultura(atividadeEconomica.getCultura());
		this.setVariedade(atividadeEconomica.getVariedade());
		this.setReferencia(atividadeEconomica.getReferencia());
		this.setDataInicio(atividadeEconomica.getDataInicio());
		this.setQtdDiasCultivo(atividadeEconomica.getQtdDiasCultivo());
		this.setBacia(atividadeEconomica.getMunicipio().getBacia());
	}

	public Municipio getMunicipio() {
		return municipio;
	}

	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}

	public Cultura getCultura() {
		return cultura;
	}

	public void setCultura(Cultura cultura) {
		this.cultura = cultura;
	}

	public Variedade getVariedade() {
		return variedade;
	}

	public void setVariedade(Variedade variedade) {
		this.variedade = variedade;
	}

	public Date getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}

	public Date getDataFim() {
		return dataFim;
	}

	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public BigDecimal getAreatotal() {
		return areatotal;
	}

	public void setAreatotal(BigDecimal areatotal) {
		this.areatotal = areatotal;
	}

	public Referencia getReferencia() {
		return referencia;
	}

	public void setReferencia(Referencia referencia) {
		this.referencia = referencia;
	}

	public Long getQtdDiasCultivo() {
		return qtdDiasCultivo;
	}

	public void setQtdDiasCultivo(Long qtdDiasCultivo) {
		this.qtdDiasCultivo = qtdDiasCultivo;
	}

	public BaciaHidrografica getBacia() {
		return bacia;
	}

	public void setBacia(BaciaHidrografica bacia) {
		this.bacia = bacia;
	}

	public List<Referencia> getListaReferencias() {
		return listaReferencias;
	}

	public void setListaReferencias(List<Referencia> listaReferencias) {
		this.listaReferencias = listaReferencias;
	}

	public List<Variedade> getListaVariedades() {
		return listaVariedades;
	}

	public void setListaVariedades(List<Variedade> listaVariedades) {
		this.listaVariedades = listaVariedades;
	}

	public Boolean getIncluido() {
		return incluido;
	}

	public void setIncluido(Boolean incluido) {
		this.incluido = incluido;
	}
}
