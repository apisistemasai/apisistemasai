package com.agrosolutions.sai.model;

import com.agrosolutions.sai.util.ResourceBundle;

public enum TipoMetodologia {

	LISIMETRIA(0, "lisimetria"),
	BHIDRICO(1, "balancoHidrico"),
	PENMAN(2, "penmanMonteithFAO"),
	OUTROS(3, "especificarModelo");
	
	private Integer id;
	
	private String descricao;

	private TipoMetodologia(Integer id, String descricao){
		this.id = id;
		this.descricao = descricao;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the descricao
	 */
	public String getDescricao() {
		return ResourceBundle.getMessage(descricao);
	}

	/**
	 * @param descricao the descricao to set
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}	
	
	/**
	 * M�todo que retorna o valor do enum correspondente a descri��o
	 * 
	 * @param descri��o
	 * @return
	 */
	public static TipoMetodologia fromDescricao(String descricao) {
		for (TipoMetodologia tipo : values()) {
			if (tipo.descricao.toLowerCase().equals(descricao.toLowerCase())) {
				return tipo;
			}
		}
		throw new RuntimeException(TipoMetodologia.class.getName() + "valorIdentificador" + " "
				+ descricao);
	}	
}
