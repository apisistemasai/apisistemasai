package com.agrosolutions.sai.model;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.BatchSize;
import org.hibernate.envers.Audited;

import com.agrosolutions.sai.util.BigDecimalUtil;

@Entity
@SequenceGenerator(name = "entity_sequence", sequenceName = "sq_TesteEficiencia", allocationSize = 1)
@Table
@Audited
@BatchSize(size=40)
@NamedQueries({
	@NamedQuery(name = "TesteEficiencia.excluir", 
			   query = "DELETE FROM TesteEficiencia t WHERE t.id = :pId"), 			  
	@NamedQuery(name = "TesteEficiencia.pesquisarPorSetor", 
			   query = "SELECT t FROM TesteEficiencia t WHERE t.setor = :pSetor ORDER BY t.data ASC"),
	@NamedQuery(name = "TesteEficiencia.pesquisarPorSetorData", 
			   query = "SELECT t FROM TesteEficiencia t WHERE t.setor = :pSetor AND t.data = (SELECT MAX(t1.data)FROM TesteEficiencia t1 WHERE t1.setor = :pSetor AND t1.data <= :pData AND t1.eficiencia > 0)") 			  
   })
public class TesteEficiencia extends Model {

	private static final long serialVersionUID = 6599459039966274472L;
	@ManyToOne
	private Setor setor;
	@Temporal(TemporalType.DATE)
	private Date data;
	private Integer tempoTeste;   //Tempo em min da realização do teste
	@ManyToOne
	private Usuario responsavel;
	@Column(length=2000)
	private String observacao;
	
	private BigDecimal linha1Emissor1Rep1;
	private BigDecimal linha1Emissor1Rep2;
	private BigDecimal linha1Emissor1Rep3;
	private String linha1Emissor2Id;
	private BigDecimal linha1Emissor2Rep1;
	private BigDecimal linha1Emissor2Rep2;
	private BigDecimal linha1Emissor2Rep3;
	private String linha1Emissor3Id;
	private BigDecimal linha1Emissor3Rep1;
	private BigDecimal linha1Emissor3Rep2;
	private BigDecimal linha1Emissor3Rep3;
	private BigDecimal linha1Emissor4Rep1;
	private BigDecimal linha1Emissor4Rep2;
	private BigDecimal linha1Emissor4Rep3;
	private BigDecimal pressaoFinalLinha1;

	private BigDecimal linha2Emissor1Rep1;
	private BigDecimal linha2Emissor1Rep2;
	private BigDecimal linha2Emissor1Rep3;
	private String linha2Emissor2Id;
	private BigDecimal linha2Emissor2Rep1;
	private BigDecimal linha2Emissor2Rep2;
	private BigDecimal linha2Emissor2Rep3;
	private String linha2Emissor3Id;
	private BigDecimal linha2Emissor3Rep1;
	private BigDecimal linha2Emissor3Rep2;
	private BigDecimal linha2Emissor3Rep3;
	private BigDecimal linha2Emissor4Rep1;
	private BigDecimal linha2Emissor4Rep2;
	private BigDecimal linha2Emissor4Rep3;
	private BigDecimal pressaoFinalLinha2;
	
	private BigDecimal linha3Emissor1Rep1;
	private BigDecimal linha3Emissor1Rep2;
	private BigDecimal linha3Emissor1Rep3;
	private String linha3Emissor2Id;
	private BigDecimal linha3Emissor2Rep1;
	private BigDecimal linha3Emissor2Rep2;
	private BigDecimal linha3Emissor2Rep3;
	private String linha3Emissor3Id;
	private BigDecimal linha3Emissor3Rep1;
	private BigDecimal linha3Emissor3Rep2;
	private BigDecimal linha3Emissor3Rep3;
	private BigDecimal linha3Emissor4Rep1;
	private BigDecimal linha3Emissor4Rep2;
	private BigDecimal linha3Emissor4Rep3;
	private BigDecimal pressaoFinalLinha3;

	private BigDecimal linha4Emissor1Rep1;
	private BigDecimal linha4Emissor1Rep2;
	private BigDecimal linha4Emissor1Rep3;
	private String linha4Emissor2Id;
	private BigDecimal linha4Emissor2Rep1;
	private BigDecimal linha4Emissor2Rep2;
	private BigDecimal linha4Emissor2Rep3;
	private String linha4Emissor3Id;
	private BigDecimal linha4Emissor3Rep1;
	private BigDecimal linha4Emissor3Rep2;
	private BigDecimal linha4Emissor3Rep3;
	private BigDecimal linha4Emissor4Rep1;
	private BigDecimal linha4Emissor4Rep2;
	private BigDecimal linha4Emissor4Rep3;
	private BigDecimal pressaoFinalLinha4;
	
	private BigDecimal eficiencia;

	@Transient
	private BigDecimal linha1Emissor1Media;
	@Transient
	private BigDecimal linha1Emissor2Media;
	@Transient
	private BigDecimal linha1Emissor3Media;
	@Transient
	private BigDecimal linha1Emissor4Media;

	@Transient
	private BigDecimal linha2Emissor1Media;
	@Transient
	private BigDecimal linha2Emissor2Media;
	@Transient
	private BigDecimal linha2Emissor3Media;
	@Transient
	private BigDecimal linha2Emissor4Media;
	
	@Transient
	private BigDecimal linha3Emissor1Media;
	@Transient
	private BigDecimal linha3Emissor2Media;
	@Transient
	private BigDecimal linha3Emissor3Media;
	@Transient
	private BigDecimal linha3Emissor4Media;

	@Transient
	private BigDecimal linha4Emissor1Media;
	@Transient
	private BigDecimal linha4Emissor2Media;
	@Transient
	private BigDecimal linha4Emissor3Media;
	@Transient
	private BigDecimal linha4Emissor4Media;

	public TesteEficiencia() {
		super();
	}

	public TesteEficiencia(Setor setor, Usuario responsavel) {
		super();
		this.setor = setor;
		this.responsavel = responsavel;
	}
	public Setor getSetor() {
		return setor;
	}
	public void setSetor(Setor setor) {
		this.setor = setor;
	}
	public Date getData() {
		return data;
	}
	public void setData(Date data) {
		this.data = data;
	}
	public Integer getTempoTeste() {
		return tempoTeste;
	}
	public void setTempoTeste(Integer tempoTeste) {
		this.tempoTeste = tempoTeste;
	}
	public Usuario getResponsavel() {
		return responsavel;
	}
	public void setResponsavel(Usuario responsavel) {
		this.responsavel = responsavel;
	}
	public BigDecimal getLinha1Emissor1Rep1() {
		return linha1Emissor1Rep1!=null?linha1Emissor1Rep1.setScale(0):null;
	}
	public void setLinha1Emissor1Rep1(BigDecimal linha1Emissor1Rep1) {
		this.linha1Emissor1Rep1 = linha1Emissor1Rep1;
	}
	public BigDecimal getLinha1Emissor1Rep2() {
		return linha1Emissor1Rep2!=null?linha1Emissor1Rep2.setScale(0):null;
	}
	public void setLinha1Emissor1Rep2(BigDecimal linha1Emissor1Rep2) {
		this.linha1Emissor1Rep2 = linha1Emissor1Rep2;
	}
	public BigDecimal getLinha1Emissor1Rep3() {
		return linha1Emissor1Rep3!=null?linha1Emissor1Rep3.setScale(0):null;
	}
	public void setLinha1Emissor1Rep3(BigDecimal linha1Emissor1Rep3) {
		this.linha1Emissor1Rep3 = linha1Emissor1Rep3;
	}
	public String getLinha1Emissor2Id() {
		return linha1Emissor2Id;
	}

	public void setLinha1Emissor2Id(String linha1Emissor2Id) {
		this.linha1Emissor2Id = linha1Emissor2Id;
	}

	public String getLinha1Emissor3Id() {
		return linha1Emissor3Id;
	}

	public void setLinha1Emissor3Id(String linha1Emissor3Id) {
		this.linha1Emissor3Id = linha1Emissor3Id;
	}

	public String getLinha2Emissor2Id() {
		return linha2Emissor2Id;
	}

	public void setLinha2Emissor2Id(String linha2Emissor2Id) {
		this.linha2Emissor2Id = linha2Emissor2Id;
	}

	public String getLinha2Emissor3Id() {
		return linha2Emissor3Id;
	}

	public void setLinha2Emissor3Id(String linha2Emissor3Id) {
		this.linha2Emissor3Id = linha2Emissor3Id;
	}

	public String getLinha3Emissor2Id() {
		return linha3Emissor2Id;
	}

	public void setLinha3Emissor2Id(String linha3Emissor2Id) {
		this.linha3Emissor2Id = linha3Emissor2Id;
	}

	public String getLinha3Emissor3Id() {
		return linha3Emissor3Id;
	}

	public void setLinha3Emissor3Id(String linha3Emissor3Id) {
		this.linha3Emissor3Id = linha3Emissor3Id;
	}

	public String getLinha4Emissor2Id() {
		return linha4Emissor2Id;
	}

	public void setLinha4Emissor2Id(String linha4Emissor2Id) {
		this.linha4Emissor2Id = linha4Emissor2Id;
	}

	public String getLinha4Emissor3Id() {
		return linha4Emissor3Id;
	}

	public void setLinha4Emissor3Id(String linha4Emissor3Id) {
		this.linha4Emissor3Id = linha4Emissor3Id;
	}

	public BigDecimal getLinha1Emissor2Rep1() {
		return linha1Emissor2Rep1!=null?linha1Emissor2Rep1.setScale(0):null;
	}
	public void setLinha1Emissor2Rep1(BigDecimal linha1Emissor2Rep1) {
		this.linha1Emissor2Rep1 = linha1Emissor2Rep1;
	}
	public BigDecimal getLinha1Emissor2Rep2() {
		return linha1Emissor2Rep2!=null?linha1Emissor2Rep2.setScale(0):null;
	}
	public void setLinha1Emissor2Rep2(BigDecimal linha1Emissor2Rep2) {
		this.linha1Emissor2Rep2 = linha1Emissor2Rep2;
	}
	public BigDecimal getLinha1Emissor2Rep3() {
		return linha1Emissor2Rep3!=null?linha1Emissor2Rep3.setScale(0):null;
	}
	public void setLinha1Emissor2Rep3(BigDecimal linha1Emissor2Rep3) {
		this.linha1Emissor2Rep3 = linha1Emissor2Rep3;
	}
	public BigDecimal getLinha1Emissor3Rep1() {
		return linha1Emissor3Rep1!=null?linha1Emissor3Rep1.setScale(0):null;
	}
	public void setLinha1Emissor3Rep1(BigDecimal linha1Emissor3Rep1) {
		this.linha1Emissor3Rep1 = linha1Emissor3Rep1;
	}
	public BigDecimal getLinha1Emissor3Rep2() {
		return linha1Emissor3Rep2!=null?linha1Emissor3Rep2.setScale(0):null;
	}
	public void setLinha1Emissor3Rep2(BigDecimal linha1Emissor3Rep2) {
		this.linha1Emissor3Rep2 = linha1Emissor3Rep2;
	}
	public BigDecimal getLinha1Emissor3Rep3() {
		return linha1Emissor3Rep3!=null?linha1Emissor3Rep3.setScale(0):null;
	}
	public void setLinha1Emissor3Rep3(BigDecimal linha1Emissor3Rep3) {
		this.linha1Emissor3Rep3 = linha1Emissor3Rep3;
	}
	public BigDecimal getLinha1Emissor4Rep1() {
		return linha1Emissor4Rep1!=null?linha1Emissor4Rep1.setScale(0):null;
	}
	public void setLinha1Emissor4Rep1(BigDecimal linha1Emissor4Rep1) {
		this.linha1Emissor4Rep1 = linha1Emissor4Rep1;
	}
	public BigDecimal getLinha1Emissor4Rep2() {
		return linha1Emissor4Rep2!=null?linha1Emissor4Rep2.setScale(0):null;
	}
	public void setLinha1Emissor4Rep2(BigDecimal linha1Emissor4Rep2) {
		this.linha1Emissor4Rep2 = linha1Emissor4Rep2;
	}
	public BigDecimal getLinha1Emissor4Rep3() {
		return linha1Emissor4Rep3!=null?linha1Emissor4Rep3.setScale(0):null;
	}
	public void setLinha1Emissor4Rep3(BigDecimal linha1Emissor4Rep3) {
		this.linha1Emissor4Rep3 = linha1Emissor4Rep3;
	}
	public BigDecimal getLinha2Emissor1Rep1() {
		return linha2Emissor1Rep1!=null?linha2Emissor1Rep1.setScale(0):null;
	}
	public void setLinha2Emissor1Rep1(BigDecimal linha2Emissor1Rep1) {
		this.linha2Emissor1Rep1 = linha2Emissor1Rep1;
	}
	public BigDecimal getLinha2Emissor1Rep2() {
		return linha2Emissor1Rep2!=null?linha2Emissor1Rep2.setScale(0):null;
	}
	public void setLinha2Emissor1Rep2(BigDecimal linha2Emissor1Rep2) {
		this.linha2Emissor1Rep2 = linha2Emissor1Rep2;
	}
	public BigDecimal getLinha2Emissor1Rep3() {
		return linha2Emissor1Rep3!=null?linha2Emissor1Rep3.setScale(0):null;
	}
	public void setLinha2Emissor1Rep3(BigDecimal linha2Emissor1Rep3) {
		this.linha2Emissor1Rep3 = linha2Emissor1Rep3;
	}
	public BigDecimal getLinha2Emissor2Rep1() {
		return linha2Emissor2Rep1!=null?linha2Emissor2Rep1.setScale(0):null;
	}
	public void setLinha2Emissor2Rep1(BigDecimal linha2Emissor2Rep1) {
		this.linha2Emissor2Rep1 = linha2Emissor2Rep1;
	}
	public BigDecimal getLinha2Emissor2Rep2() {
		return linha2Emissor2Rep2!=null?linha2Emissor2Rep2.setScale(0):null;
	}
	public void setLinha2Emissor2Rep2(BigDecimal linha2Emissor2Rep2) {
		this.linha2Emissor2Rep2 = linha2Emissor2Rep2;
	}
	public BigDecimal getLinha2Emissor2Rep3() {
		return linha2Emissor2Rep3!=null?linha2Emissor2Rep3.setScale(0):null;
	}
	public void setLinha2Emissor2Rep3(BigDecimal linha2Emissor2Rep3) {
		this.linha2Emissor2Rep3 = linha2Emissor2Rep3;
	}
	public BigDecimal getLinha2Emissor3Rep1() {
		return linha2Emissor3Rep1!=null?linha2Emissor3Rep1.setScale(0):null;
	}
	public void setLinha2Emissor3Rep1(BigDecimal linha2Emissor3Rep1) {
		this.linha2Emissor3Rep1 = linha2Emissor3Rep1;
	}
	public BigDecimal getLinha2Emissor3Rep2() {
		return linha2Emissor3Rep2!=null?linha2Emissor3Rep2.setScale(0):null;
	}
	public void setLinha2Emissor3Rep2(BigDecimal linha2Emissor3Rep2) {
		this.linha2Emissor3Rep2 = linha2Emissor3Rep2;
	}
	public BigDecimal getLinha2Emissor3Rep3() {
		return linha2Emissor3Rep3!=null?linha2Emissor3Rep3.setScale(0):null;
	}
	public void setLinha2Emissor3Rep3(BigDecimal linha2Emissor3Rep3) {
		this.linha2Emissor3Rep3 = linha2Emissor3Rep3;
	}
	public BigDecimal getLinha2Emissor4Rep1() {
		return linha2Emissor4Rep1!=null?linha2Emissor4Rep1.setScale(0):null;
	}
	public void setLinha2Emissor4Rep1(BigDecimal linha2Emissor4Rep1) {
		this.linha2Emissor4Rep1 = linha2Emissor4Rep1;
	}
	public BigDecimal getLinha2Emissor4Rep2() {
		return linha2Emissor4Rep2!=null?linha2Emissor4Rep2.setScale(0):null;
	}
	public void setLinha2Emissor4Rep2(BigDecimal linha2Emissor4Rep2) {
		this.linha2Emissor4Rep2 = linha2Emissor4Rep2;
	}
	public BigDecimal getLinha2Emissor4Rep3() {
		return linha2Emissor4Rep3!=null?linha2Emissor4Rep3.setScale(0):null;
	}
	public void setLinha2Emissor4Rep3(BigDecimal linha2Emissor4Rep3) {
		this.linha2Emissor4Rep3 = linha2Emissor4Rep3;
	}
	public BigDecimal getLinha3Emissor1Rep1() {
		return linha3Emissor1Rep1!=null?linha3Emissor1Rep1.setScale(0):null;
	}
	public void setLinha3Emissor1Rep1(BigDecimal linha3Emissor1Rep1) {
		this.linha3Emissor1Rep1 = linha3Emissor1Rep1;
	}
	public BigDecimal getLinha3Emissor1Rep2() {
		return linha3Emissor1Rep2!=null?linha3Emissor1Rep2.setScale(0):null;
	}
	public void setLinha3Emissor1Rep2(BigDecimal linha3Emissor1Rep2) {
		this.linha3Emissor1Rep2 = linha3Emissor1Rep2;
	}
	public BigDecimal getLinha3Emissor1Rep3() {
		return linha3Emissor1Rep3!=null?linha3Emissor1Rep3.setScale(0):null;
	}
	public void setLinha3Emissor1Rep3(BigDecimal linha3Emissor1Rep3) {
		this.linha3Emissor1Rep3 = linha3Emissor1Rep3;
	}
	public BigDecimal getLinha3Emissor2Rep1() {
		return linha3Emissor2Rep1!=null?linha3Emissor2Rep1.setScale(0):null;
	}
	public void setLinha3Emissor2Rep1(BigDecimal linha3Emissor2Rep1) {
		this.linha3Emissor2Rep1 = linha3Emissor2Rep1;
	}
	public BigDecimal getLinha3Emissor2Rep2() {
		return linha3Emissor2Rep2!=null?linha3Emissor2Rep2.setScale(0):null;
	}
	public void setLinha3Emissor2Rep2(BigDecimal linha3Emissor2Rep2) {
		this.linha3Emissor2Rep2 = linha3Emissor2Rep2;
	}
	public BigDecimal getLinha3Emissor2Rep3() {
		return linha3Emissor2Rep3!=null?linha3Emissor2Rep3.setScale(0):null;
	}
	public void setLinha3Emissor2Rep3(BigDecimal linha3Emissor2Rep3) {
		this.linha3Emissor2Rep3 = linha3Emissor2Rep3;
	}
	public BigDecimal getLinha3Emissor3Rep1() {
		return linha3Emissor3Rep1!=null?linha3Emissor3Rep1.setScale(0):null;
	}
	public void setLinha3Emissor3Rep1(BigDecimal linha3Emissor3Rep1) {
		this.linha3Emissor3Rep1 = linha3Emissor3Rep1;
	}
	public BigDecimal getLinha3Emissor3Rep2() {
		return linha3Emissor3Rep2!=null?linha3Emissor3Rep2.setScale(0):null;
	}
	public void setLinha3Emissor3Rep2(BigDecimal linha3Emissor3Rep2) {
		this.linha3Emissor3Rep2 = linha3Emissor3Rep2;
	}
	public BigDecimal getLinha3Emissor3Rep3() {
		return linha3Emissor3Rep3!=null?linha3Emissor3Rep3.setScale(0):null;
	}
	public void setLinha3Emissor3Rep3(BigDecimal linha3Emissor3Rep3) {
		this.linha3Emissor3Rep3 = linha3Emissor3Rep3;
	}
	public BigDecimal getLinha3Emissor4Rep1() {
		return linha3Emissor4Rep1!=null?linha3Emissor4Rep1.setScale(0):null;
	}
	public void setLinha3Emissor4Rep1(BigDecimal linha3Emissor4Rep1) {
		this.linha3Emissor4Rep1 = linha3Emissor4Rep1;
	}
	public BigDecimal getLinha3Emissor4Rep2() {
		return linha3Emissor4Rep2!=null?linha3Emissor4Rep2.setScale(0):null;
	}
	public void setLinha3Emissor4Rep2(BigDecimal linha3Emissor4Rep2) {
		this.linha3Emissor4Rep2 = linha3Emissor4Rep2;
	}
	public BigDecimal getLinha3Emissor4Rep3() {
		return linha3Emissor4Rep3!=null?linha3Emissor4Rep3.setScale(0):null;
	}
	public void setLinha3Emissor4Rep3(BigDecimal linha3Emissor4Rep3) {
		this.linha3Emissor4Rep3 = linha3Emissor4Rep3;
	}
	public BigDecimal getLinha4Emissor1Rep1() {
		return linha4Emissor1Rep1!=null?linha4Emissor1Rep1.setScale(0):null;
	}
	public void setLinha4Emissor1Rep1(BigDecimal linha4Emissor1Rep1) {
		this.linha4Emissor1Rep1 = linha4Emissor1Rep1;
	}
	public BigDecimal getLinha4Emissor1Rep2() {
		return linha4Emissor1Rep2!=null?linha4Emissor1Rep2.setScale(0):null;
	}
	public void setLinha4Emissor1Rep2(BigDecimal linha4Emissor1Rep2) {
		this.linha4Emissor1Rep2 = linha4Emissor1Rep2;
	}
	public BigDecimal getLinha4Emissor1Rep3() {
		return linha4Emissor1Rep3!=null?linha4Emissor1Rep3.setScale(0):null;
	}
	public void setLinha4Emissor1Rep3(BigDecimal linha4Emissor1Rep3) {
		this.linha4Emissor1Rep3 = linha4Emissor1Rep3;
	}
	public BigDecimal getLinha4Emissor2Rep1() {
		return linha4Emissor2Rep1!=null?linha4Emissor2Rep1.setScale(0):null;
	}
	public void setLinha4Emissor2Rep1(BigDecimal linha4Emissor2Rep1) {
		this.linha4Emissor2Rep1 = linha4Emissor2Rep1;
	}
	public BigDecimal getLinha4Emissor2Rep2() {
		return linha4Emissor2Rep2!=null?linha4Emissor2Rep2.setScale(0):null;
	}
	public void setLinha4Emissor2Rep2(BigDecimal linha4Emissor2Rep2) {
		this.linha4Emissor2Rep2 = linha4Emissor2Rep2;
	}
	public BigDecimal getLinha4Emissor2Rep3() {
		return linha4Emissor2Rep3!=null?linha4Emissor2Rep3.setScale(0):null;
	}
	public void setLinha4Emissor2Rep3(BigDecimal linha4Emissor2Rep3) {
		this.linha4Emissor2Rep3 = linha4Emissor2Rep3;
	}
	public BigDecimal getLinha4Emissor3Rep1() {
		return linha4Emissor3Rep1!=null?linha4Emissor3Rep1.setScale(0):null;
	}
	public void setLinha4Emissor3Rep1(BigDecimal linha4Emissor3Rep1) {
		this.linha4Emissor3Rep1 = linha4Emissor3Rep1;
	}
	public BigDecimal getLinha4Emissor3Rep2() {
		return linha4Emissor3Rep2!=null?linha4Emissor3Rep2.setScale(0):null;
	}
	public void setLinha4Emissor3Rep2(BigDecimal linha4Emissor3Rep2) {
		this.linha4Emissor3Rep2 = linha4Emissor3Rep2;
	}
	public BigDecimal getLinha4Emissor3Rep3() {
		return linha4Emissor3Rep3!=null?linha4Emissor3Rep3.setScale(0):null;
	}
	public void setLinha4Emissor3Rep3(BigDecimal linha4Emissor3Rep3) {
		this.linha4Emissor3Rep3 = linha4Emissor3Rep3;
	}
	public BigDecimal getLinha4Emissor4Rep1() {
		return linha4Emissor4Rep1!=null?linha4Emissor4Rep1.setScale(0):null;
	}
	public void setLinha4Emissor4Rep1(BigDecimal linha4Emissor4Rep1) {
		this.linha4Emissor4Rep1 = linha4Emissor4Rep1;
	}
	public BigDecimal getLinha4Emissor4Rep2() {
		return linha4Emissor4Rep2!=null?linha4Emissor4Rep2.setScale(0):null;
	}
	public void setLinha4Emissor4Rep2(BigDecimal linha4Emissor4Rep2) {
		this.linha4Emissor4Rep2 = linha4Emissor4Rep2;
	}
	public BigDecimal getLinha4Emissor4Rep3() {
		return linha4Emissor4Rep3!=null?linha4Emissor4Rep3.setScale(0):null;
	}
	public void setLinha4Emissor4Rep3(BigDecimal linha4Emissor4Rep3) {
		this.linha4Emissor4Rep3 = linha4Emissor4Rep3;
	}
	public BigDecimal getLinha1Emissor1Media() {
		if (linha1Emissor1Rep1 != null && linha1Emissor1Rep2 != null && linha1Emissor1Rep3 != null && tempoTeste != null && tempoTeste != 0) {
			linha1Emissor1Media = new BigDecimal(0);
			linha1Emissor1Media = linha1Emissor1Rep1.add(linha1Emissor1Rep2).add(linha1Emissor1Rep3).divide(new BigDecimal(3), BigDecimalUtil.MC);
			linha1Emissor1Media = linha1Emissor1Media.divide(new BigDecimal(1000)).multiply(new BigDecimal(60).divide(new BigDecimal(tempoTeste), BigDecimalUtil.MC).multiply(new BigDecimal(60)), BigDecimalUtil.MC);
		}
		return linha1Emissor1Media;
	}
	public void setLinha1Emissor1Media(BigDecimal linha1Emissor1Media) {
		this.linha1Emissor1Media = linha1Emissor1Media;
	}
	public BigDecimal getLinha1Emissor2Media() {
		if (linha1Emissor2Rep1 != null && linha1Emissor2Rep2 != null && linha1Emissor2Rep3 != null && tempoTeste != null && tempoTeste != 0) {
			linha1Emissor2Media = new BigDecimal(0);
			linha1Emissor2Media = linha1Emissor2Rep1.add(linha1Emissor2Rep2).add(linha1Emissor2Rep3).divide(new BigDecimal(3), BigDecimalUtil.MC);
			linha1Emissor2Media = linha1Emissor2Media.divide(new BigDecimal(1000)).multiply(new BigDecimal(60).divide(new BigDecimal(tempoTeste), BigDecimalUtil.MC).multiply(new BigDecimal(60)), BigDecimalUtil.MC);
		}
		return linha1Emissor2Media;
	}
	public void setLinha1Emissor2Media(BigDecimal linha1Emissor2Media) {
		this.linha1Emissor2Media = linha1Emissor2Media;
	}
	public BigDecimal getLinha1Emissor3Media() {
		if (linha1Emissor3Rep1 != null && linha1Emissor3Rep2 != null && linha1Emissor3Rep3 != null && tempoTeste != null && tempoTeste != 0) {
			linha1Emissor3Media = new BigDecimal(0);
			linha1Emissor3Media = linha1Emissor3Rep1.add(linha1Emissor3Rep2).add(linha1Emissor3Rep3).divide(new BigDecimal(3), BigDecimalUtil.MC);
			linha1Emissor3Media = linha1Emissor3Media.divide(new BigDecimal(1000)).multiply(new BigDecimal(60).divide(new BigDecimal(tempoTeste), BigDecimalUtil.MC).multiply(new BigDecimal(60)), BigDecimalUtil.MC);
		}
		return linha1Emissor3Media;
	}
	public void setLinha1Emissor3Media(BigDecimal linha1Emissor3Media) {
		this.linha1Emissor3Media = linha1Emissor3Media;
	}
	public BigDecimal getLinha1Emissor4Media() {
		if (linha1Emissor4Rep1 != null && linha1Emissor4Rep2 != null && linha1Emissor4Rep3 != null && tempoTeste != null && tempoTeste != 0) {
			linha1Emissor4Media = new BigDecimal(0);
			linha1Emissor4Media = linha1Emissor4Rep1.add(linha1Emissor4Rep2).add(linha1Emissor4Rep3).divide(new BigDecimal(3), BigDecimalUtil.MC);
			linha1Emissor4Media = linha1Emissor4Media.divide(new BigDecimal(1000)).multiply(new BigDecimal(60).divide(new BigDecimal(tempoTeste), BigDecimalUtil.MC).multiply(new BigDecimal(60)), BigDecimalUtil.MC);
		}
		return linha1Emissor4Media;
	}
	public void setLinha1Emissor4Media(BigDecimal linha1Emissor4Media) {
		this.linha1Emissor4Media = linha1Emissor4Media;
	}
	public BigDecimal getLinha2Emissor1Media() {
		if (linha2Emissor1Rep1 != null && linha2Emissor1Rep2 != null && linha2Emissor1Rep3 != null && tempoTeste != null && tempoTeste != 0) {
			linha2Emissor1Media = new BigDecimal(0);
			linha2Emissor1Media = linha2Emissor1Rep1.add(linha2Emissor1Rep2).add(linha2Emissor1Rep3).divide(new BigDecimal(3), BigDecimalUtil.MC);
			linha2Emissor1Media = linha2Emissor1Media.divide(new BigDecimal(1000)).multiply(new BigDecimal(60).divide(new BigDecimal(tempoTeste), BigDecimalUtil.MC).multiply(new BigDecimal(60)), BigDecimalUtil.MC);
		}
		return linha2Emissor1Media;
	}
	public void setLinha2Emissor1Media(BigDecimal linha2Emissor1Media) {
		this.linha2Emissor1Media = linha2Emissor1Media;
	}
	public BigDecimal getLinha2Emissor2Media() {
		if (linha2Emissor2Rep1 != null && linha2Emissor2Rep2 != null && linha2Emissor2Rep3 != null && tempoTeste != null && tempoTeste != 0) {
			linha2Emissor2Media = new BigDecimal(0);
			linha2Emissor2Media = linha2Emissor2Rep1.add(linha2Emissor2Rep2).add(linha2Emissor2Rep3).divide(new BigDecimal(3), BigDecimalUtil.MC);
			linha2Emissor2Media = linha2Emissor2Media.divide(new BigDecimal(1000)).multiply(new BigDecimal(60).divide(new BigDecimal(tempoTeste), BigDecimalUtil.MC).multiply(new BigDecimal(60)), BigDecimalUtil.MC);
		}
		return linha2Emissor2Media;
	}
	public void setLinha2Emissor2Media(BigDecimal linha2Emissor2Media) {
		this.linha2Emissor2Media = linha2Emissor2Media;
	}
	public BigDecimal getLinha2Emissor3Media() {
		if (linha2Emissor3Rep1 != null && linha2Emissor3Rep2 != null && linha2Emissor3Rep3 != null && tempoTeste != null && tempoTeste != 0) {
			linha2Emissor3Media = new BigDecimal(0);
			linha2Emissor3Media = linha2Emissor3Rep1.add(linha2Emissor3Rep2).add(linha2Emissor3Rep3).divide(new BigDecimal(3), BigDecimalUtil.MC);
			linha2Emissor3Media = linha2Emissor3Media.divide(new BigDecimal(1000)).multiply(new BigDecimal(60).divide(new BigDecimal(tempoTeste), BigDecimalUtil.MC).multiply(new BigDecimal(60)), BigDecimalUtil.MC);
		}
		return linha2Emissor3Media;
	}
	public void setLinha2Emissor3Media(BigDecimal linha2Emissor3Media) {
		this.linha2Emissor3Media = linha2Emissor3Media;
	}
	public BigDecimal getLinha2Emissor4Media() {
		if (linha2Emissor4Rep1 != null && linha2Emissor4Rep2 != null && linha2Emissor4Rep3 != null && tempoTeste != null && tempoTeste != 0) {
			linha2Emissor4Media = new BigDecimal(0);
			linha2Emissor4Media = linha2Emissor4Rep1.add(linha2Emissor4Rep2).add(linha2Emissor4Rep3).divide(new BigDecimal(3), BigDecimalUtil.MC);
			linha2Emissor4Media = linha2Emissor4Media.divide(new BigDecimal(1000)).multiply(new BigDecimal(60).divide(new BigDecimal(tempoTeste), BigDecimalUtil.MC).multiply(new BigDecimal(60)), BigDecimalUtil.MC);
		}
		return linha2Emissor4Media;
	}
	public void setLinha2Emissor4Media(BigDecimal linha2Emissor4Media) {
		this.linha2Emissor4Media = linha2Emissor4Media;
	}
	public BigDecimal getLinha3Emissor1Media() {
		if (linha3Emissor1Rep1 != null && linha3Emissor1Rep2 != null && linha3Emissor1Rep3 != null && tempoTeste != null && tempoTeste != 0) {
			linha3Emissor1Media = new BigDecimal(0);
			linha3Emissor1Media = linha3Emissor1Rep1.add(linha3Emissor1Rep2).add(linha3Emissor1Rep3).divide(new BigDecimal(3), BigDecimalUtil.MC);
			linha3Emissor1Media = linha3Emissor1Media.divide(new BigDecimal(1000)).multiply(new BigDecimal(60).divide(new BigDecimal(tempoTeste), BigDecimalUtil.MC).multiply(new BigDecimal(60)), BigDecimalUtil.MC);
		}
		return linha3Emissor1Media;
	}
	public void setLinha3Emissor1Media(BigDecimal linha3Emissor1Media) {
		this.linha3Emissor1Media = linha3Emissor1Media;
	}
	public BigDecimal getLinha3Emissor2Media() {
		if (linha3Emissor2Rep1 != null && linha3Emissor2Rep2 != null && linha3Emissor2Rep3 != null && tempoTeste != null && tempoTeste != 0) {
			linha3Emissor2Media = new BigDecimal(0);
			linha3Emissor2Media = linha3Emissor2Rep1.add(linha3Emissor2Rep2).add(linha3Emissor2Rep3).divide(new BigDecimal(3), BigDecimalUtil.MC);
			linha3Emissor2Media = linha3Emissor2Media.divide(new BigDecimal(1000)).multiply(new BigDecimal(60).divide(new BigDecimal(tempoTeste), BigDecimalUtil.MC).multiply(new BigDecimal(60)), BigDecimalUtil.MC);
		}
		return linha3Emissor2Media;
	}
	public void setLinha3Emissor2Media(BigDecimal linha3Emissor2Media) {
		this.linha3Emissor2Media = linha3Emissor2Media;
	}
	public BigDecimal getLinha3Emissor3Media() {
		if (linha3Emissor3Rep1 != null && linha3Emissor3Rep2 != null && linha3Emissor3Rep3 != null && tempoTeste != null && tempoTeste != 0) {
			linha3Emissor3Media = new BigDecimal(0);
			linha3Emissor3Media = linha3Emissor3Rep1.add(linha3Emissor3Rep2).add(linha3Emissor3Rep3).divide(new BigDecimal(3), BigDecimalUtil.MC);
			linha3Emissor3Media = linha3Emissor3Media.divide(new BigDecimal(1000)).multiply(new BigDecimal(60).divide(new BigDecimal(tempoTeste), BigDecimalUtil.MC).multiply(new BigDecimal(60)), BigDecimalUtil.MC);
		}
		return linha3Emissor3Media;
	}
	public void setLinha3Emissor3Media(BigDecimal linha3Emissor3Media) {
		this.linha3Emissor3Media = linha3Emissor3Media;
	}
	public BigDecimal getLinha3Emissor4Media() {
		if (linha3Emissor4Rep1 != null && linha3Emissor4Rep2 != null && linha3Emissor4Rep3 != null && tempoTeste != null && tempoTeste != 0) {
			linha3Emissor4Media = new BigDecimal(0);
			linha3Emissor4Media = linha3Emissor4Rep1.add(linha3Emissor4Rep2).add(linha3Emissor4Rep3).divide(new BigDecimal(3), BigDecimalUtil.MC);
			linha3Emissor4Media = linha3Emissor4Media.divide(new BigDecimal(1000)).multiply(new BigDecimal(60).divide(new BigDecimal(tempoTeste), BigDecimalUtil.MC).multiply(new BigDecimal(60)), BigDecimalUtil.MC);
		}
		return linha3Emissor4Media;
	}
	public void setLinha3Emissor4Media(BigDecimal linha3Emissor4Media) {
		this.linha3Emissor4Media = linha3Emissor4Media;
	}
	public BigDecimal getLinha4Emissor1Media() {
		if (linha4Emissor1Rep1 != null && linha4Emissor1Rep2 != null && linha4Emissor1Rep3 != null && tempoTeste != null && tempoTeste != 0) {
			linha4Emissor1Media = new BigDecimal(0);
			linha4Emissor1Media = linha4Emissor1Rep1.add(linha4Emissor1Rep2).add(linha4Emissor1Rep3).divide(new BigDecimal(3), BigDecimalUtil.MC);
			linha4Emissor1Media = linha4Emissor1Media.divide(new BigDecimal(1000)).multiply(new BigDecimal(60).divide(new BigDecimal(tempoTeste), BigDecimalUtil.MC).multiply(new BigDecimal(60)), BigDecimalUtil.MC);
		}
		return linha4Emissor1Media;
	}
	public void setLinha4Emissor1Media(BigDecimal linha4Emissor1Media) {
		this.linha4Emissor1Media = linha4Emissor1Media;
	}
	public BigDecimal getLinha4Emissor2Media() {
		if (linha4Emissor2Rep1 != null && linha4Emissor2Rep2 != null && linha4Emissor2Rep3 != null && tempoTeste != null && tempoTeste != 0) {
			linha4Emissor2Media = new BigDecimal(0);
			linha4Emissor2Media = linha4Emissor2Rep1.add(linha4Emissor2Rep2).add(linha4Emissor2Rep3).divide(new BigDecimal(3), BigDecimalUtil.MC);
			linha4Emissor2Media = linha4Emissor2Media.divide(new BigDecimal(1000)).multiply(new BigDecimal(60).divide(new BigDecimal(tempoTeste), BigDecimalUtil.MC).multiply(new BigDecimal(60)), BigDecimalUtil.MC);
		}
		return linha4Emissor2Media;
	}
	public void setLinha4Emissor2Media(BigDecimal linha4Emissor2Media) {
		this.linha4Emissor2Media = linha4Emissor2Media;
	}
	public BigDecimal getLinha4Emissor3Media() {
		if (linha4Emissor3Rep1 != null && linha4Emissor3Rep2 != null && linha4Emissor3Rep3 != null && tempoTeste != null && tempoTeste != 0) {
			linha4Emissor3Media = new BigDecimal(0);
			linha4Emissor3Media = linha4Emissor3Rep1.add(linha4Emissor3Rep2).add(linha4Emissor3Rep3).divide(new BigDecimal(3), BigDecimalUtil.MC);
			linha4Emissor3Media = linha4Emissor3Media.divide(new BigDecimal(1000)).multiply(new BigDecimal(60).divide(new BigDecimal(tempoTeste), BigDecimalUtil.MC).multiply(new BigDecimal(60)), BigDecimalUtil.MC);
		}
		return linha4Emissor3Media;
	}
	public void setLinha4Emissor3Media(BigDecimal linha4Emissor3Media) {
		this.linha4Emissor3Media = linha4Emissor3Media;
	}
	public BigDecimal getLinha4Emissor4Media() {
		if (linha4Emissor4Rep1 != null && linha4Emissor4Rep2 != null && linha4Emissor4Rep3 != null && tempoTeste != null && tempoTeste != 0) {
			linha4Emissor4Media = new BigDecimal(0);
			linha4Emissor4Media = linha4Emissor4Rep1.add(linha4Emissor4Rep2).add(linha4Emissor4Rep3).divide(new BigDecimal(3), BigDecimalUtil.MC);
			linha4Emissor4Media = linha4Emissor4Media.divide(new BigDecimal(1000)).multiply(new BigDecimal(60).divide(new BigDecimal(tempoTeste), BigDecimalUtil.MC).multiply(new BigDecimal(60)), BigDecimalUtil.MC);
		}
		return linha4Emissor4Media;
	}
	public void setLinha4Emissor4Media(BigDecimal linha4Emissor4Media) {
		this.linha4Emissor4Media = linha4Emissor4Media;
	}
	public BigDecimal getPressaoFinalLinha1() {
		return pressaoFinalLinha1;
	}
	public void setPressaoFinalLinha1(BigDecimal pressaoFinalLinha1) {
		this.pressaoFinalLinha1 = pressaoFinalLinha1;
	}
	public BigDecimal getPressaoFinalLinha2() {
		return pressaoFinalLinha2;
	}
	public void setPressaoFinalLinha2(BigDecimal pressaoFinalLinha2) {
		this.pressaoFinalLinha2 = pressaoFinalLinha2;
	}
	public BigDecimal getPressaoFinalLinha3() {
		return pressaoFinalLinha3;
	}
	public void setPressaoFinalLinha3(BigDecimal pressaoFinalLinha3) {
		this.pressaoFinalLinha3 = pressaoFinalLinha3;
	}
	public BigDecimal getPressaoFinalLinha4() {
		return pressaoFinalLinha4;
	}
	public void setPressaoFinalLinha4(BigDecimal pressaoFinalLinha4) {
		this.pressaoFinalLinha4 = pressaoFinalLinha4;
	}
	 
	public BigDecimal getEficiencia() {
		EficienciaDTO[] lista = new EficienciaDTO[16];
		BigDecimal mediaTotal = new BigDecimal(0);
		BigDecimal media25 = new BigDecimal(0);
		eficiencia = new BigDecimal(0);
		eficiencia.setScale(2);
		boolean terminou = true;
		
		if (getLinha1Emissor1Media() != null) {
			lista[0] = new EficienciaDTO(getLinha1Emissor1Media());
			mediaTotal = mediaTotal.add(getLinha1Emissor1Media());
		}else{
			terminou = false;
		}
		if (getLinha1Emissor2Media() != null) {
			lista[1] = new EficienciaDTO(getLinha1Emissor2Media());
			mediaTotal = mediaTotal.add(getLinha1Emissor2Media());
		}else{
			terminou = false;
		}
		if (getLinha1Emissor3Media() != null) {
			lista[2] = new EficienciaDTO(getLinha1Emissor3Media());
			mediaTotal = mediaTotal.add(getLinha1Emissor3Media());
		}else{
			terminou = false;
		}
		if (getLinha1Emissor4Media() != null) {
			lista[3] = new EficienciaDTO(getLinha1Emissor4Media());
			mediaTotal = mediaTotal.add(getLinha1Emissor4Media());
		}else{
			terminou = false;
		}
		
		if (getLinha2Emissor1Media() != null) {
			lista[4] = new EficienciaDTO(getLinha2Emissor1Media());
			mediaTotal = mediaTotal.add(getLinha2Emissor1Media());
		}else{
			terminou = false;
		}
		if (getLinha2Emissor2Media() != null) {
			lista[5] = new EficienciaDTO(getLinha2Emissor2Media());
			mediaTotal = mediaTotal.add(getLinha2Emissor2Media());
		}else{
			terminou = false;
		}
		if (getLinha2Emissor3Media() != null) {
			lista[6] = new EficienciaDTO(getLinha2Emissor3Media());
			mediaTotal = mediaTotal.add(getLinha2Emissor3Media());
		}else{
			terminou = false;
		}
		if (getLinha2Emissor4Media() != null) {
			lista[7] = new EficienciaDTO(getLinha2Emissor4Media());
			mediaTotal = mediaTotal.add(getLinha2Emissor4Media());
		}else{
			terminou = false;
		}

		if (getLinha3Emissor1Media() != null) {
			lista[8] = new EficienciaDTO(getLinha3Emissor1Media());
			mediaTotal = mediaTotal.add(getLinha3Emissor1Media());
		}else{
			terminou = false;
		}
		if (getLinha3Emissor2Media() != null) {
			lista[9] = new EficienciaDTO(getLinha3Emissor2Media());
			mediaTotal = mediaTotal.add(getLinha3Emissor2Media());
		}else{
			terminou = false;
		}
		if (getLinha3Emissor3Media() != null) {
			lista[10] = new EficienciaDTO(getLinha3Emissor3Media());
			mediaTotal = mediaTotal.add(getLinha3Emissor3Media());
		}else{
			terminou = false;
		}
		if (getLinha3Emissor4Media() != null) {
			lista[11] = new EficienciaDTO(getLinha3Emissor4Media());
			mediaTotal = mediaTotal.add(getLinha3Emissor4Media());
		}else{
			terminou = false;
		}

		if (getLinha4Emissor1Media() != null) {
			lista[12] = new EficienciaDTO(getLinha4Emissor1Media());
			mediaTotal = mediaTotal.add(getLinha4Emissor1Media());
		}else{
			terminou = false;
		}
		if (getLinha4Emissor2Media() != null) {
			lista[13] = new EficienciaDTO(getLinha4Emissor2Media());
			mediaTotal = mediaTotal.add(getLinha4Emissor2Media());
		}else{
			terminou = false;
		}
		if (getLinha4Emissor3Media() != null) {
			lista[14] = new EficienciaDTO(getLinha4Emissor3Media());
			mediaTotal = mediaTotal.add(getLinha4Emissor3Media());
		}else{
			terminou = false;
		}
		if (getLinha4Emissor4Media() != null) {
			lista[15] = new EficienciaDTO(getLinha4Emissor4Media());
			mediaTotal = mediaTotal.add(getLinha4Emissor4Media());
		}else{
			terminou = false;
		}
		
		if (terminou) {
			Arrays.sort(lista);
			for(int i=0;i < lista.length;i++){
				if (i<4) {
					media25 = media25.add(lista[i].getValorClasse());
				}
			}
			media25 = media25.divide(new BigDecimal(4), BigDecimalUtil.MC);
			mediaTotal = mediaTotal.divide(new BigDecimal(16), BigDecimalUtil.MC);
			eficiencia = media25.divide(mediaTotal, BigDecimalUtil.MC).multiply(new BigDecimal(100));
		}
		
		return eficiencia;
	}
	public void setEficiencia(BigDecimal eficiencia) {
		this.eficiencia = eficiencia;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	
}

