package com.agrosolutions.sai.model;


public class GraficoPizza {
	private final String nome;
	private final Long qtd;
	private final Long total;
	private final String und;

	public GraficoPizza(String nome, Long qtd, Long total, String und){
		this.nome = nome;
		this.qtd = qtd;
		this.total = total;
		this.und = und;
	}
	public String getNome() {
		return nome;
	}
	public Long getQtd() {
		return qtd;
	}
	public Long getTotal() {
		return total;
	}
	public String getUnd() {
		return und;
	}
}
