package com.agrosolutions.sai.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.Where;
import org.hibernate.envers.Audited;
import org.primefaces.model.SortOrder;

@Entity
@SequenceGenerator(name = "entity_sequence", sequenceName = "sq_Tecnico", allocationSize = 1)
@Table
@Audited
@NamedQueries({
	@NamedQuery(name = "Tecnico.excluir", 
			   query = "DELETE FROM Tecnico t WHERE t.id = :pId"), 			  
	@NamedQuery(name = "Tecnico.pesquisar", 
			   query = "SELECT t FROM Tecnico t WHERE t.distrito IN (:pDistrito) ORDER BY t.nome ASC")
   })
public class Tecnico extends Model implements Serializable {

	private static final long serialVersionUID = 7627003472606016615L;
	private String nome;
	private String foto;
	private String endereco;
	private String cidade;
	private String cep;
	private String uf;
	private String telefoneContato;
	private String celular;
	private OperadoraCelular operadora;
	private Escolaridade escolaridade;
	@Column(length=800)
	private String observacao;
	
	@Temporal(TemporalType.DATE)
	private Date dataCadastro;
	@Where(clause = "dataInatividade is null") 
	@Temporal(TemporalType.DATE)
	private Date dataInatividade;

	@ManyToOne
	private Distrito distrito;

	@Transient
	private String sortField;
	@Transient
	private SortOrder sortOrder;
	
	@OneToOne
	private Usuario usuario;

	public Tecnico() {
		super();
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getEndereco() {
		return endereco;
	}
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	public String getCidade() {
		return cidade;
	}
	public void setCidade(String cidade) {
		this.cidade = cidade;
	}
	public String getCep() {
		return cep;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}
	public String getUf() {
		return uf;
	}
	public void setUf(String uf) {
		this.uf = uf;
	}
	public String getTelefoneContato() {
		return telefoneContato;
	}
	public void setTelefoneContato(String telefoneContato) {
		this.telefoneContato = telefoneContato;
	}
	public String getCelular() {
		return celular;
	}
	public void setCelular(String celular) {
		this.celular = celular;
	}
	public OperadoraCelular getOperadora() {
		return operadora;
	}
	public void setOperadora(OperadoraCelular operadora) {
		this.operadora = operadora;
	}
	public Escolaridade getEscolaridade() {
		return escolaridade;
	}
	public void setEscolaridade(Escolaridade escolaridade) {
		this.escolaridade = escolaridade;
	}
	public String getObservacao() {
		return observacao;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public String getFoto() {
		return foto;
	}
	public void setFoto(String foto) {
		this.foto = foto;
	}
	public Date getDataCadastro() {
		return dataCadastro;
	}
	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}
	public Date getDataInatividade() {
		return dataInatividade;
	}
	public void setDataInatividade(Date dataInatividade) {
		this.dataInatividade = dataInatividade;
	}
	public Distrito getDistrito() {
		return distrito;
	}
	public void setDistrito(Distrito distrito) {
		this.distrito = distrito;
	}
	public String getSortField() {
		return sortField;
	}
	public void setSortField(String sortField) {
		this.sortField = sortField;
	}
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public void setSortOrder(SortOrder ascending) {
		this.sortOrder = ascending;
	}
	public SortOrder getSortOrder() {
		return sortOrder;
	}
}