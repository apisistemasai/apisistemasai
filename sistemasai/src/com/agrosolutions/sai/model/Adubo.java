package com.agrosolutions.sai.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;
import org.primefaces.model.SortOrder;


@Entity
@SequenceGenerator(name = "entity_sequence", sequenceName = "sq_Adubo", allocationSize = 1)
@Table
@Audited
@NamedQueries({
	@NamedQuery(name = "Adubo.todos", 
			   query = "SELECT a FROM Adubo a ORDER BY a.nome ASC"), 			  
	@NamedQuery(name = "Adubo.excluir", 
			   query = "DELETE FROM Adubo a WHERE a.id = :pId") 			  
   })
public class Adubo extends Model {

	private static final long serialVersionUID = 4572409364741224164L;
	
	@Column(unique=true)
	private String nome;
	@Column(length=800)
	private String observacao;
	
	@Transient
	private String sortField;
	@Transient
	private SortOrder sortOrder;

	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getObservacao() {
		return observacao;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public void setSortField(String sortField) {
		this.sortField = sortField;
	}
	public String getSortField() {
		return sortField;
	}
	public void setSortOrder(SortOrder sortOrder) {
		this.sortOrder = sortOrder;
	}
	public SortOrder getSortOrder() {
		return sortOrder;
	}
	
}
