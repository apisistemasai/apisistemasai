package com.agrosolutions.sai.model;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.BatchSize;
import org.hibernate.envers.Audited;

@Entity
@SequenceGenerator(name = "entity_sequence", sequenceName = "sq_Clima", allocationSize = 1)
@Table
@BatchSize(size=20)
@Audited
@NamedQueries({
	@NamedQuery(name = "Clima.ultimoPorDistrito", 
			   query = "SELECT c FROM Clima c join c.area a WHERE a.distrito = :pDistrito AND c.data = (SELECT MAX(c1.data) FROM Clima c1 join c1.area a1 WHERE a1.distrito = :pDistrito)"), 			  
	@NamedQuery(name = "Clima.ultimoPorArea", 
			   query = "SELECT c FROM Clima c WHERE c.area = :pArea AND c.data = (SELECT MAX(c1.data) FROM Clima c1 WHERE c1.area = :pArea)"), 			  
	@NamedQuery(name = "Clima.pesquisarPorArea", 
			   query = "SELECT c FROM Clima c WHERE c.area = :pArea ORDER BY c.data ASC"), 			  
	@NamedQuery(name = "Clima.pesquisarPorData", 
			   query = "SELECT c FROM Clima c WHERE c.area = :pArea AND c.data = :pData"), 			  
	@NamedQuery(name = "Clima.pesquisarPorPeriodo", 
			   query = "SELECT c FROM Clima c WHERE c.area = :pArea AND c.data >= :pDataIni AND c.data < :pDataFim ORDER BY c.data ASC"), 			  
	@NamedQuery(name = "Clima.estacaoPorPeriodo", 
			   query = "SELECT c FROM Clima c WHERE c.estacao = :pEstacao AND c.data >= :pDataIni AND c.data < :pDataFim ORDER BY c.data ASC") 			  
   })
public class Clima extends Model {

	private static final long serialVersionUID = -455312344809840473L;
	@Temporal(TemporalType.DATE)
	private Date data;
	@ManyToOne
	private Area area;
	@ManyToOne
	private Estacao estacao;
	private BigDecimal tempMin; 	//Temperatura Minima (Celsius)
	private BigDecimal tempMax;		//Temperatura Maxima (Celsius)
	private BigDecimal tempMed;		//Temperatura M�dia (Celsius)
	private BigDecimal umidadeMin;	//Humidade Minima (%)
	private BigDecimal umidadeMax;	//Humidade M�xima (%)
	private BigDecimal umidadeMed;	//Humidade M�dia (%)

	private BigDecimal orvalhoMin;	//Temperatura no ponto de orvalho Minima (Celsius)
	private BigDecimal orvalhoMax;	//Temperatura no ponto de orvalho M�xima (Celsius)
	private BigDecimal orvalhoMed;	//Temperatura no ponto de orvalho M�dia (Celsius)
	
	private BigDecimal pressaoMin;	//Temperatura no ponto de orvalho Minima (Celsius)
	private BigDecimal pressaoMax;	//Temperatura no ponto de orvalho M�xima (Celsius)
	private BigDecimal pressaoMed;	//Temperatura no ponto de orvalho M�dia (Celsius)

	private BigDecimal vento;		//Vento (m/s)
	private BigDecimal ventoRajada;	//Vento (m/s)

	private BigDecimal radiacao;	//Radia��o Solar (Mj/m2/dia)
	private BigDecimal radLiquida;	//Radia��o Solar Liquida (Mj/m2/dia)
	
	private BigDecimal chuva;		//Precipita��o em mm
	private BigDecimal chuvaEfetiva;//Precipita��o efetiva em mm
	private BigDecimal eto;			//Evapotranspira��o (mm/dia)
	private BigDecimal etoCalculado;//Evapotranspira��o (mm/dia) - calculada pela A EQUA��O DE PENMAN-MONTEITH/FAO, DESCRITA POR ALLEN et al. 1998
	
	@Transient
	private Integer dia;

	public Clima() {
		super();
		this.tempMin = new BigDecimal(0); 		//Temperatura Minima (Celsius)
		this.tempMax = new BigDecimal(0);		//Temperatura Maxima (Celsius)
		this.tempMed = new BigDecimal(0);		//Temperatura Maxima (Celsius)
		this.orvalhoMin = new BigDecimal(0);	//Temperatura do Ponto de Orvalho Minima (Celsius)
		this.orvalhoMax = new BigDecimal(0);	//Temperatura do Ponto de Orvalho Maxima (Celsius)
		this.orvalhoMed = new BigDecimal(0);	//Temperatura do Ponto de Orvalho Maxima (Celsius)
		this.umidadeMax = new BigDecimal(0);	//Humidade M�xima (%)
		this.umidadeMin = new BigDecimal(0);	//Humidade Minima (%)
		this.umidadeMed = new BigDecimal(0);	//Humidade M�dia (%)
		this.pressaoMax = new BigDecimal(0);	//Press�o M�xima (%)
		this.pressaoMin = new BigDecimal(0);	//Press�o Minima (%)
		this.pressaoMed = new BigDecimal(0);	//Press�o M�dia (%)
		this.vento = new BigDecimal(0);			//Vento (km/dia)
		this.ventoRajada = new BigDecimal(0);	//Vento (km/dia)
		this.radiacao = new BigDecimal(0);		//Radia��o Solar (Mj/m2/dia)
		this.radLiquida = new BigDecimal(0);	//Radia��o Solar (Mj/m2/dia)
		this.chuva = new BigDecimal(0);			//Precipita��o em mm
		this.chuvaEfetiva = new BigDecimal(0);	//Precipita��o efetiva em mm
		this.eto = new BigDecimal(0);			//Evapotranspira��o (mm/dia)
		this.etoCalculado = new BigDecimal(0);	//Evapotranspira��o Calculado
		
	}

	public Clima(Date data, Area area, Estacao estacao) {
		super();
		this.data = data;
		this.area = area;
		this.estacao = estacao;
	}

	
	
	public Clima(Clima c) {
		super();
		this.data = c.getData();
		this.area = c.getArea();
		this.estacao = c.getEstacao();
		this.tempMin = c.getTempMin();
		this.tempMax = c.getTempMax();
		this.tempMed = c.getTempMed();
		this.umidadeMin = c.getUmidadeMin();
		this.umidadeMax = c.getUmidadeMax();
		this.umidadeMed = c.getUmidadeMed();
		this.orvalhoMin = c.getOrvalhoMin();
		this.orvalhoMax = c.getOrvalhoMax();
		this.orvalhoMed = c.getOrvalhoMed();
		this.pressaoMin = c.getPressaoMin();
		this.pressaoMax = c.getPressaoMax();
		this.pressaoMed = c.getPressaoMed();
		this.vento = c.getVento();
		this.ventoRajada = c.getVentoRajada();
		this.radiacao = c.getRadiacao();
		this.radLiquida = c.getRadLiquida();
		this.chuva = c.getChuva();
		this.chuvaEfetiva = c.getChuvaEfetiva();
		this.eto = c.getEto();
		this.etoCalculado = c.getEtoCalculado();
	}

	public Area getArea() {
		return area;
	}
	public void setArea(Area area) {
		this.area = area;
	}
	public Estacao getEstacao() {
		return estacao;
	}
	public void setEstacao(Estacao estacao) {
		this.estacao = estacao;
	}
	public BigDecimal getTempMin() {
		return tempMin;
	}
	public void setTempMin(BigDecimal tempMin) {
		this.tempMin = tempMin;
	}
	public BigDecimal getTempMax() {
		return tempMax;
	}
	public void setTempMax(BigDecimal tempMax) {
		this.tempMax = tempMax;
	}
	public BigDecimal getVento() {
		return vento;
	}
	public void setVento(BigDecimal vento) {
		this.vento = vento;
	}
	public BigDecimal getRadiacao() {
		return radiacao;
	}
	public void setRadiacao(BigDecimal radiacao) {
		this.radiacao = radiacao;
	}
	public BigDecimal getEto() {
		return eto;
	}
	public void setEto(BigDecimal eto) {
		this.eto = eto;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public BigDecimal getChuva() {
		return chuva;
	}

	public void setChuva(BigDecimal chuva) {
		this.chuva = chuva;
	}

	public BigDecimal getChuvaEfetiva() {
		if (chuva.compareTo(new BigDecimal(16)) > 0 ) {
			chuvaEfetiva =  new BigDecimal(16);
		} else {
			chuvaEfetiva = chuva;
		}
		return chuvaEfetiva;
	}

	public BigDecimal getTempMed() {
		return tempMed;
	}

	public void setTempMed(BigDecimal tempMed) {
		this.tempMed = tempMed;
	}

	public BigDecimal getRadLiquida() {
		return radLiquida;
	}

	public void setRadLiquida(BigDecimal radLiquida) {
		this.radLiquida = radLiquida;
	}

	public BigDecimal getEtoCalculado() {
		return etoCalculado;
	}

	public void setEtoCalculado(BigDecimal etoCalculado) {
		this.etoCalculado = etoCalculado;
	}

	public void setChuvaEfetiva(BigDecimal chuvaEfetiva) {
		this.chuvaEfetiva = chuvaEfetiva;
	}

	public Integer getDia() {
		if (data != null) {
			Calendar d = new GregorianCalendar();
			d.setTime(data);
			dia = d.get(Calendar.DAY_OF_MONTH);
		}
		return dia;
	}

	public void setDia(Integer dia) {
		this.dia = dia;
	}

	public BigDecimal getUmidadeMin() {
		return umidadeMin;
	}

	public void setUmidadeMin(BigDecimal umidadeMin) {
		this.umidadeMin = umidadeMin;
	}

	public BigDecimal getUmidadeMax() {
		return umidadeMax;
	}

	public void setUmidadeMax(BigDecimal umidadeMax) {
		this.umidadeMax = umidadeMax;
	}

	public BigDecimal getUmidadeMed() {
		return umidadeMed;
	}

	public void setUmidadeMed(BigDecimal umidadeMed) {
		this.umidadeMed = umidadeMed;
	}

	public BigDecimal getOrvalhoMin() {
		return orvalhoMin;
	}

	public void setOrvalhoMin(BigDecimal orvalhoMin) {
		this.orvalhoMin = orvalhoMin;
	}

	public BigDecimal getOrvalhoMax() {
		return orvalhoMax;
	}

	public void setOrvalhoMax(BigDecimal orvalhoMax) {
		this.orvalhoMax = orvalhoMax;
	}

	public BigDecimal getOrvalhoMed() {
		return orvalhoMed;
	}

	public void setOrvalhoMed(BigDecimal orvalhoMed) {
		this.orvalhoMed = orvalhoMed;
	}

	public BigDecimal getPressaoMin() {
		return pressaoMin;
	}

	public void setPressaoMin(BigDecimal pressaoMin) {
		this.pressaoMin = pressaoMin;
	}

	public BigDecimal getPressaoMax() {
		return pressaoMax;
	}

	public void setPressaoMax(BigDecimal pressaoMax) {
		this.pressaoMax = pressaoMax;
	}

	public BigDecimal getPressaoMed() {
		return pressaoMed;
	}

	public void setPressaoMed(BigDecimal pressaoMed) {
		this.pressaoMed = pressaoMed;
	}

	public BigDecimal getVentoRajada() {
		return ventoRajada;
	}

	public void setVentoRajada(BigDecimal ventoRajada) {
		this.ventoRajada = ventoRajada;
	}
}
