package com.agrosolutions.sai.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;


/**
 *
 * @author Raphael
 */

@Entity
@Table
@Audited
@SequenceGenerator(name = "entity_sequence", sequenceName="sq_Parametro", allocationSize = 1)
@NamedQueries({
	@NamedQuery(name = "Parametro.todos", 
			   query = "SELECT p FROM Parametro p ORDER BY p.codigo ASC"),			  
	@NamedQuery(name = "Parametro.porCodigo", 
			   query = "SELECT p FROM Parametro p WHERE p.codigo = :pCodigo")			  
		   })
public class Parametro extends Model {

	private static final long serialVersionUID = -5128254706635563534L;

	@Column(length=20, nullable=false)
	private String codigo;

	@Column(length=80)
	private String descricao;

	@Column(length=50, nullable=false)
	private String valor;

	public Parametro() {
		super();
	}

	public Parametro(String codigo, String descricao, String valor) {
		super();
		this.codigo = codigo;
		this.descricao = descricao;
		this.valor = valor;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

}
