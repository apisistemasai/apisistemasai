package com.agrosolutions.sai.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@SequenceGenerator(name = "entity_sequence", sequenceName = "sq_Acesso", allocationSize = 1)
@Table
@NamedQueries({
	@NamedQuery(name = "Acesso.pesquisarPorUsuario", 
		   query = "SELECT a FROM Acesso a JOIN a.usuario u WHERE u.username = :pUsuario ORDER BY a.data ASC"),
	@NamedQuery(name = "Acesso.ultimoPorUsuario", 
		   query = "SELECT a FROM Acesso a JOIN a.usuario u WHERE u.username = :pUsuario and a.data = (select max(a2.data) from Acesso a2 where a2.usuario = a.usuario)")
   })
public class Acesso extends Model {

	private static final long serialVersionUID = 430867894003666975L;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date data;
	@ManyToOne
	private Usuario usuario;
	
	public Acesso() {
		super();
	}
	public Acesso(Date data, Usuario usuario) {
		super();
		this.data = data;
		this.usuario = usuario;
}
	public Date getData() {
		return data;
	}
	public void setData(Date data) {
		this.data = data;
	}
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

}
