package com.agrosolutions.sai.model;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.BatchSize;
import org.hibernate.envers.Audited;
import org.primefaces.model.SortOrder;


@Entity
@SequenceGenerator(name = "entity_sequence", sequenceName = "sq_FertirrigacaoAplicada", allocationSize = 1)
@Table
@Audited
@NamedQueries({
	@NamedQuery(name = "FertirrigacaoAplicada.todos", 
			   query = "SELECT f FROM FertirrigacaoAplicada f ORDER BY f.data ASC"), 			  
	@NamedQuery(name = "FertirrigacaoAplicada.excluir", 
			   query = "DELETE FROM FertirrigacaoAplicada f WHERE f.id = :pId") 			  
   })
public class FertirrigacaoAplicada extends Model {

	private static final long serialVersionUID = 1913142805022594697L;

	private Boolean base;

	@OneToMany(fetch = FetchType.EAGER)
	@BatchSize(size=10)
	private List<Nutriente> nutrientes;

	@Temporal(TemporalType.DATE)
	private Date data;

	@ManyToOne
	private Tanque tanque;

	@ManyToOne(fetch = FetchType.LAZY)
	private Plantio plantio;

	@Temporal(TemporalType.TIMESTAMP)
	private Date tempoInicial;

	@Temporal(TemporalType.TIMESTAMP)
	private Date tempoFinal;

	@Column
	private BigDecimal volume;

	@Column
	private BigDecimal custo;
	
	@Transient
	private String sortField;
	@Transient
	private SortOrder sortOrder;
	@Transient
	private Date dataIni;
	@Transient
	private Date dataFim;


	public void setSortField(String sortField) {
		this.sortField = sortField;
	}
	public String getSortField() {
		return sortField;
	}
	public void setSortOrder(SortOrder sortOrder) {
		this.sortOrder = sortOrder;
	}
	public SortOrder getSortOrder() {
		return sortOrder;
	}
	public BigDecimal getVolume() {
		return volume;
	}
	public void setVolume(BigDecimal volume) {
		this.volume = volume;
	}
	public Boolean isBase() {
		return base;
	}
	public void setBase(Boolean base) {
		this.base = base;
	}
	public Date getData() {
		return data;
	}
	public void setData(Date data) {
		this.data = data;
	}
	public Tanque getTanque() {
		return tanque;
	}
	public void setTanque(Tanque tanque) {
		this.tanque = tanque;
	}
	public Plantio getPlantio() {
		return plantio;
	}
	public void setPlantio(Plantio plantio) {
		this.plantio = plantio;
	}
	public Date getTempoInicial() {
		return tempoInicial;
	}
	public void setTempoInicial(Date tempoInicial) {
		this.tempoInicial = tempoInicial;
	}
	public Date getTempoFinal() {
		return tempoFinal;
	}
	public void setTempoFinal(Date tempoFinal) {
		this.tempoFinal = tempoFinal;
	}
	public BigDecimal getCusto() {
		return custo;
	}
	public void setCusto(BigDecimal custo) {
		this.custo = custo;
	}
	public List<Nutriente> getNutrientes() {
		return nutrientes;
	}
	public void setNutrientes(List<Nutriente> nutrientes) {
		this.nutrientes = nutrientes;
	}
	public Date getDataIni() {
		return dataIni;
	}
	public void setDataIni(Date dataIni) {
		this.dataIni = dataIni;
	}
	public Date getDataFim() {
		return dataFim;
	}
	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}
	public String getNutrientesDescricao(){
		String retorno = "";
		if (nutrientes != null && !nutrientes.isEmpty()){
			for (Nutriente nutriente : nutrientes) {
				retorno += nutriente.getNome() + ",";
			}
		}
		return retorno.substring(0, retorno.length()-1);
		
	}
	
}
