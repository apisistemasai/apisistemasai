package com.agrosolutions.sai.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.envers.Audited;
import org.primefaces.model.SortOrder;
import org.primefaces.model.chart.CartesianChartModel;
import org.primefaces.model.chart.PieChartModel;

@Entity
@SequenceGenerator(name = "entity_sequence", sequenceName = "sq_Irrigante", allocationSize = 1)
@Table
@Audited
@NamedQueries({
		@NamedQuery(name = "Irrigante.pesquisarPorLote", query = "SELECT i FROM Irrigante i JOIN i.area a WHERE i.localizacao = :pLote AND a.distrito IN (:pDistrito)"),
		@NamedQuery(name = "Irrigante.pesquisarPorCultura", query = "SELECT DISTINCT i FROM Irrigante i JOIN i.area a JOIN i.setores s JOIN s.plantio p JOIN p.cultivo c JOIN c.cultura cult WHERE cult.id = :pCultura AND p.dataFim is null AND (i.tipoIrrigante = :pTipo OR :pTipo is null) AND (i.area = :pArea OR :pArea is null) AND a.distrito IN (:pDistrito)"),
		@NamedQuery(name = "Irrigante.pesquisarPorVariedade", query = "SELECT DISTINCT i FROM Irrigante i JOIN i.area a JOIN i.setores s JOIN s.plantio p JOIN p.cultivo c JOIN c.variedade v WHERE v.id = :pVariedade AND p.dataFim is null AND (i.tipoIrrigante = :pTipo OR :pTipo is null) AND (i.area = :pArea OR :pArea is null) AND a.distrito IN (:pDistrito)"),
		@NamedQuery(name = "Irrigante.pesquisarPorUsuario", query = "SELECT i FROM Irrigante i JOIN i.usuario u WHERE u = :pUsuario"),
		@NamedQuery(name = "Irrigante.excluir", query = "DELETE FROM Irrigante i WHERE i.id = :pId"),
		@NamedQuery(name = "Irrigante.pesquisar", query = "SELECT i FROM Irrigante i JOIN i.area a JOIN i.bombas b WHERE a.distrito IN (:pDistrito) ORDER BY nome ASC"),
		@NamedQuery(name = "Irrigante.pesquisarPorDistrito", query = "SELECT i FROM Irrigante i JOIN i.area a WHERE a.distrito IN (:pDistrito)"), })
public class Irrigante extends Model implements Serializable {

	private static final long serialVersionUID = -7839801116171529808L;
	private String nome;
	private String foto;
	private TipoIrrigante tipoIrrigante;
	private String cpf;
	private String cnpj;
	private String rg;
	private String endereco;
	private String cidade;
	private String cep;
	private String uf;
	private String telefoneContato;
	private String celular;
	private OperadoraCelular operadora;
	private Escolaridade escolaridade;
	private Boolean possuiInternet;
	private String localizacao;
	private String latitude;
	private String longitude;
	private BigDecimal areaTotal;
	private BigDecimal areaIrrigada;
	private BigDecimal pressao;
	private Boolean sistemaAutomatico;
	private Boolean piloto;
	private Integer classificacao;
	@Column(nullable = false, columnDefinition = "boolean default false")
	private Boolean enviarEmail;
	@Column(nullable = false, columnDefinition = "boolean default false")
	private Boolean enviarSMS;
	private String lacreHidrometro;
	private String codigo;
	@Column(length = 800)
	private String observacao;
	@Temporal(TemporalType.DATE)
	private Date dataCadastro;
	@Temporal(TemporalType.DATE)
	private Date dataInatividade;
	@ManyToOne
	private Usuario usuario;
	@ManyToOne
	private Area area;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "irrigante")
	@Fetch(FetchMode.SUBSELECT)
	@BatchSize(size = 30)
	private List<Setor> setores;
	@ManyToMany
	@BatchSize(size = 10)
	private List<Bomba> bombas;
	@ManyToMany
	@BatchSize(size = 10)
	private List<Filtro> filtros;
	@ManyToMany
	@BatchSize(size = 10)
	private List<Valvula> valvulas;
	@ManyToMany
	@BatchSize(size = 10)
	private List<Tubo> tubos;
	@Transient
	private Cultura cultura;
	@Transient
	private Variedade variedade;
	@Transient
	private String sortField;
	@Transient
	private SortOrder sortOrder;
	@Transient
	private Integer qtdSetor;
	@Transient
	List<Distrito> distritos;
	@Transient
	private List<String> lotes;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "irrigante")
	@Fetch(FetchMode.SUBSELECT)
	@BatchSize(size = 30)
	private List<Coordenada> coordenadasPoligono;
	@Transient
	private Integer alturaGrafico;
	@Transient
	private PieChartModel graficoIndicadorAreaIrrigada;
	@Transient
	private CartesianChartModel graficoIndicadoresSegurancaKgHa;
	@Transient
	private CartesianChartModel graficoIndicadoresSegurancaKgM3;
	@Transient
	private CartesianChartModel graficoIndicadoresEconomicoReceitaLiquida;
	@Transient
	private CartesianChartModel graficoIndicadoresVBP;
	@Transient
	private CartesianChartModel graficoIndicadoresEconomicoReceitaLiquidaHa;
	@Transient
	private CartesianChartModel graficoIndicadoresEconomicoReceitaLiquidaM3;
	@Transient
	private CartesianChartModel graficoIndicadoresSegSocialEmpregosHa;
	@Transient
	private CartesianChartModel graficoIndicadoresSegSocialEmpregosM3;
	@Transient
	private CartesianChartModel graficoIndicadoresSegHidricaM3Ha;
	@Transient
	private CartesianChartModel graficoIndicadoresSegHidricaLitrosSegundoHa;
	@Transient
	private CartesianChartModel graficoIndicadoresCorteHidrico;
	@Transient
	private PieChartModel graficoIndicadorCicloCultura; 
	
	public Irrigante() {
		super();
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public TipoIrrigante getTipoIrrigante() {
		return tipoIrrigante;
	}

	public void setTipoIrrigante(TipoIrrigante tipoIrrigante) {
		this.tipoIrrigante = tipoIrrigante;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public Boolean getSistemaAutomatico() {
		return sistemaAutomatico;
	}

	public void setSistemaAutomatico(Boolean sistemaAutomatico) {
		this.sistemaAutomatico = sistemaAutomatico;
	}

	public String getUf() {
		return uf;
	}

	public void setUf(String uf) {
		this.uf = uf;
	}

	public String getTelefoneContato() {
		return telefoneContato;
	}

	public void setTelefoneContato(String telefoneContato) {
		this.telefoneContato = telefoneContato;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public OperadoraCelular getOperadora() {
		return operadora;
	}

	public Cultura getCultura() {
		return cultura;
	}

	public void setCultura(Cultura cultura) {
		this.cultura = cultura;
	}

	public Variedade getVariedade() {
		return variedade;
	}

	public void setVariedade(Variedade variedade) {
		this.variedade = variedade;
	}

	public void setOperadora(OperadoraCelular operadora) {
		this.operadora = operadora;
	}

	public Escolaridade getEscolaridade() {
		return escolaridade;
	}

	public void setEscolaridade(Escolaridade escolaridade) {
		this.escolaridade = escolaridade;
	}

	public List<Filtro> getFiltros() {
		return filtros;
	}

	public Boolean getPossuiInternet() {
		return possuiInternet;
	}

	public Integer getQtdSetor() {
		return qtdSetor;
	}

	public void setQtdSetor(Integer qtdSetor) {
		this.qtdSetor = qtdSetor;
	}

	public void setPossuiInternet(Boolean possuiInternet) {
		this.possuiInternet = possuiInternet;
	}

	public String getLocalizacao() {
		return localizacao;
	}

	public void setLocalizacao(String localizacao) {
		this.localizacao = localizacao;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public BigDecimal getAreaTotal() {
		return areaTotal;
	}

	public void setAreaTotal(BigDecimal areaTotal) {
		this.areaTotal = areaTotal;
	}

	public BigDecimal getAreaIrrigada() {
		return areaIrrigada;
	}

	public void setAreaIrrigada(BigDecimal areaIrrigada) {
		this.areaIrrigada = areaIrrigada;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public String getFoto() {
		return foto;
	}

	public void setFoto(String foto) {
		this.foto = foto;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public Date getDataInatividade() {
		return dataInatividade;
	}

	public void setDataInatividade(Date dataInatividade) {
		this.dataInatividade = dataInatividade;
	}

	public Area getArea() {
		return area;
	}

	public void setArea(Area area) {
		this.area = area;
	}

	public String getSortField() {
		return sortField;
	}

	public void setSortField(String sortField) {
		this.sortField = sortField;
	}

	public SortOrder getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(SortOrder sortOrder) {
		this.sortOrder = sortOrder;
	}

	public List<Setor> getSetores() {
		return setores;
	}

	public List<Bomba> getBombas() {
		return bombas;
	}

	public void setBombas(List<Bomba> bombas) {
		this.bombas = bombas;
	}

	public void setFiltros(List<Filtro> filtros) {
		this.filtros = filtros;
	}

	public void setSetores(List<Setor> setores) {
		this.setores = setores;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public BigDecimal getPressao() {
		return pressao;
	}

	public void setPressao(BigDecimal pressao) {
		this.pressao = pressao;
	}

	public Boolean getPiloto() {
		return piloto;
	}

	public void setPiloto(Boolean piloto) {
		this.piloto = piloto;
	}

	public List<Valvula> getValvulas() {
		return valvulas;
	}

	public void setValvulas(List<Valvula> valvulas) {
		this.valvulas = valvulas;
	}

	public List<Tubo> getTubos() {
		return tubos;
	}

	public void setTubos(List<Tubo> tubos) {
		this.tubos = tubos;
	}

	public Integer getClassificacao() {
		return classificacao;
	}

	public void setClassificacao(Integer classificacao) {
		this.classificacao = classificacao;
	}

	public Boolean getEnviarEmail() {
		return enviarEmail;
	}

	public void setEnviarEmail(Boolean enviarEmail) {
		this.enviarEmail = enviarEmail;
	}

	public Boolean getEnviarSMS() {
		return enviarSMS;
	}

	public void setEnviarSMS(Boolean enviarSMS) {
		this.enviarSMS = enviarSMS;
	}

	public List<Distrito> getDistritos() {
		return distritos;
	}

	public void setDistritos(List<Distrito> distritos) {
		this.distritos = distritos;
	}

	public void setLacreHidrometro(String lacreHidrometro) {
		this.lacreHidrometro = lacreHidrometro;
	}

	public String getLacreHidrometro() {
		return lacreHidrometro;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getCodigo() {
		return codigo;
	}

	public List<String> getLotes() {
		return lotes;
	}

	public void setLotes(List<String> lotes) {
		this.lotes = lotes;
	}

	public PieChartModel getGraficoIndicadorAreaIrrigada() {
		return graficoIndicadorAreaIrrigada;
	}

	public void setGraficoIndicadorAreaIrrigada(
			PieChartModel graficoIndicadorAreaIrrigada) {
		this.graficoIndicadorAreaIrrigada = graficoIndicadorAreaIrrigada;
	}

	public CartesianChartModel getGraficoIndicadoresSegurancaKgHa() {
		return graficoIndicadoresSegurancaKgHa;
	}

	public void setGraficoIndicadoresSegurancaKgHa(
			CartesianChartModel graficoIndicadoresSegurancaKgHa) {
		this.graficoIndicadoresSegurancaKgHa = graficoIndicadoresSegurancaKgHa;
	}

	public CartesianChartModel getGraficoIndicadoresSegurancaKgM3() {
		return graficoIndicadoresSegurancaKgM3;
	}

	public void setGraficoIndicadoresSegurancaKgM3(
			CartesianChartModel graficoIndicadoresSegurancaKgM3) {
		this.graficoIndicadoresSegurancaKgM3 = graficoIndicadoresSegurancaKgM3;
	}

	public CartesianChartModel getGraficoIndicadoresEconomicoReceitaLiquidaHa() {
		return graficoIndicadoresEconomicoReceitaLiquidaHa;
	}

	public void setGraficoIndicadoresEconomicoReceitaLiquidaHa(
			CartesianChartModel graficoIndicadoresEconomicoReceitaLiquidaHa) {
		this.graficoIndicadoresEconomicoReceitaLiquidaHa = graficoIndicadoresEconomicoReceitaLiquidaHa;
	}

	public CartesianChartModel getGraficoIndicadoresEconomicoReceitaLiquidaM3() {
		return graficoIndicadoresEconomicoReceitaLiquidaM3;
	}

	public void setGraficoIndicadoresEconomicoReceitaLiquidaM3(
			CartesianChartModel graficoIndicadoresEconomicoReceitaLiquidaM3) {
		this.graficoIndicadoresEconomicoReceitaLiquidaM3 = graficoIndicadoresEconomicoReceitaLiquidaM3;
	}

	public CartesianChartModel getGraficoIndicadoresSegSocialEmpregosHa() {
		return graficoIndicadoresSegSocialEmpregosHa;
	}

	public void setGraficoIndicadoresSegSocialEmpregosHa(
			CartesianChartModel graficoIndicadoresSegSocialEmpregosHa) {
		this.graficoIndicadoresSegSocialEmpregosHa = graficoIndicadoresSegSocialEmpregosHa;
	}

	public CartesianChartModel getGraficoIndicadoresSegSocialEmpregosM3() {
		return graficoIndicadoresSegSocialEmpregosM3;
	}

	public void setGraficoIndicadoresSegSocialEmpregosM3(
			CartesianChartModel graficoIndicadoresSegSocialEmpregosM3) {
		this.graficoIndicadoresSegSocialEmpregosM3 = graficoIndicadoresSegSocialEmpregosM3;
	}

	public CartesianChartModel getGraficoIndicadoresSegHidricaM3Ha() {
		return graficoIndicadoresSegHidricaM3Ha;
	}

	public void setGraficoIndicadoresSegHidricaM3Ha(
			CartesianChartModel graficoIndicadoresSegHidricaM3Ha) {
		this.graficoIndicadoresSegHidricaM3Ha = graficoIndicadoresSegHidricaM3Ha;
	}

	public CartesianChartModel getGraficoIndicadoresSegHidricaLitrosSegundoHa() {
		return graficoIndicadoresSegHidricaLitrosSegundoHa;
	}

	public void setGraficoIndicadoresSegHidricaLitrosSegundoHa(
			CartesianChartModel graficoIndicadoresSegHidricaLitrosSegundoHa) {
		this.graficoIndicadoresSegHidricaLitrosSegundoHa = graficoIndicadoresSegHidricaLitrosSegundoHa;
	}

	public List<Coordenada> getCoordenadasPoligono() {
		return coordenadasPoligono;
	}

	public void setCoordenadasPoligono(List<Coordenada> coordenadasPoligono) {
		this.coordenadasPoligono = coordenadasPoligono;
	}

	public CartesianChartModel getGraficoIndicadoresCorteHidrico() {
		return graficoIndicadoresCorteHidrico;
	}

	public void setGraficoIndicadoresCorteHidrico(CartesianChartModel graficoIndicadoresCorteHidrico) {
		this.graficoIndicadoresCorteHidrico = graficoIndicadoresCorteHidrico;
	}

	public Integer getAlturaGrafico() {
		return alturaGrafico;
	}

	public void setAlturaGrafico(Integer alturaGrafico) {
		this.alturaGrafico = alturaGrafico;
	}

	public CartesianChartModel getGraficoIndicadoresVBP() {
		return graficoIndicadoresVBP;
	}

	public void setGraficoIndicadoresVBP(CartesianChartModel graficoIndicadoresVBP) {
		this.graficoIndicadoresVBP = graficoIndicadoresVBP;
	}

	public CartesianChartModel getGraficoIndicadoresEconomicoReceitaLiquida() {
		return graficoIndicadoresEconomicoReceitaLiquida;
	}

	public void setGraficoIndicadoresEconomicoReceitaLiquida(
			CartesianChartModel graficoIndicadoresEconomicoReceitaLiquida) {
		this.graficoIndicadoresEconomicoReceitaLiquida = graficoIndicadoresEconomicoReceitaLiquida;
	}

	public PieChartModel getGraficoIndicadorCicloCultura() {
		return graficoIndicadorCicloCultura;
	}

	public void setGraficoIndicadorCicloCultura(
			PieChartModel graficoIndicadorCicloCultura) {
		this.graficoIndicadorCicloCultura = graficoIndicadorCicloCultura;
	}
}