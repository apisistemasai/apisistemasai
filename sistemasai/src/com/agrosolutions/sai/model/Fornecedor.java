package com.agrosolutions.sai.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Classe que representa as Fornecedors de contas 
 * @author raphael.ferreira
 *
 */
@Entity
@Table
@SequenceGenerator(name = "entity_sequence", sequenceName = "sq_Fornecedor", allocationSize = 1)
@NamedQueries({ 			  
	@NamedQuery(name = "Fornecedor.todos", 
			   query = "SELECT c FROM Fornecedor c ORDER BY c.nome"), 			  
	@NamedQuery(name = "Fornecedor.excluir", 
			   query = "DELETE FROM Fornecedor c WHERE c.id = :pId") 			  
   })
public class Fornecedor extends Model implements Serializable {

	private static final long serialVersionUID = -879307323066140764L;
	private Long codigo;
	private String nome;
	private String cpf;
	private String cnpj;
	private String telefone;
	private String celular;
	private String contato;
	private String observacao;
	
	@OneToOne(cascade = CascadeType.ALL)
	private Endereco endereco;
	
	public Long getCodigo() {
		return codigo;
	}
	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getCnpj() {
		return cnpj;
	}
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	public String getCelular() {
		return celular;
	}
	public void setCelular(String celular) {
		this.celular = celular;
	}
	public String getContato() {
		return contato;
	}
	public void setContato(String contato) {
		this.contato = contato;
	}
	public String getObservacao() {
		return observacao;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}
	public Endereco getEndereco() {
		return endereco;
	}
	
}