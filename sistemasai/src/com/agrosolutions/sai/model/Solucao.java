package com.agrosolutions.sai.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.BatchSize;
import org.hibernate.envers.Audited;
import org.primefaces.model.SortOrder;


@Entity
@SequenceGenerator(name = "entity_sequence", sequenceName = "sq_Solucao", allocationSize = 1)
@Table
@Audited
@NamedQueries({
	@NamedQuery(name = "Solucao.todos", 
			   query = "SELECT s FROM Solucao s ORDER BY s.id ASC"), 			  
	@NamedQuery(name = "Solucao.excluir", 
			   query = "DELETE FROM Solucao s WHERE s.id = :pId") 			  
   })
public class Solucao extends Model {

	private static final long serialVersionUID = 7678210881580244378L;

	@OneToMany(fetch = FetchType.EAGER)
	@BatchSize(size=10)
	private List<Nutriente> nutrientes;
	
	@Transient
	private String sortField;
	@Transient
	private SortOrder sortOrder;


	public void setSortField(String sortField) {
		this.sortField = sortField;
	}
	public String getSortField() {
		return sortField;
	}
	public void setSortOrder(SortOrder sortOrder) {
		this.sortOrder = sortOrder;
	}
	public SortOrder getSortOrder() {
		return sortOrder;
	}
	public List<Nutriente> getNutrientes() {
		return nutrientes;
	}
	public void setNutrientes(List<Nutriente> nutrientes) {
		this.nutrientes = nutrientes;
	}
	
}
