package com.agrosolutions.sai.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;
import org.primefaces.model.SortOrder;


@Entity
@SequenceGenerator(name = "entity_sequence", sequenceName = "sq_Elemento", allocationSize = 1)
@Table
@Audited
@NamedQueries({
	@NamedQuery(name = "Elemento.todos", 
			   query = "SELECT e FROM Elemento e ORDER BY e.nome ASC"), 			  
	@NamedQuery(name = "Elemento.excluir", 
			   query = "DELETE FROM Elemento e WHERE e.id = :pId") 			  
   })
public class Elemento extends Model {

	private static final long serialVersionUID = 1487344758189484024L;

	@Column(unique=true)
	private String nome;
	
	@Transient
	private String sortField;
	@Transient
	private SortOrder sortOrder;

	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setSortField(String sortField) {
		this.sortField = sortField;
	}
	public String getSortField() {
		return sortField;
	}
	public void setSortOrder(SortOrder sortOrder) {
		this.sortOrder = sortOrder;
	}
	public SortOrder getSortOrder() {
		return sortOrder;
	}
	
}
