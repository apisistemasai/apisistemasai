package com.agrosolutions.sai.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Classe que representa as contas 
 * 
 * @author carlos.torres
 *
 */
@Entity
@Table
@SequenceGenerator(name = "entity_sequence", sequenceName = "sq_Conta", allocationSize = 1)
@NamedQueries({ 			  
	@NamedQuery(name = "Conta.todos", 
			   query = "SELECT c FROM Conta c ORDER BY c.descricao"), 			  
	@NamedQuery(name = "Conta.excluir", 
			   query = "DELETE FROM Conta c WHERE c.id = :pId") 			  
   })
public class Conta extends Model implements Serializable {
	
	private static final long serialVersionUID = -3267277985352823062L;
	
	private long codigo;
	private String descricao;
	private BigDecimal saldoInicial;
	private TipoConta tipo;
	private BigDecimal saldoAtual;
	private Date dataInicial;
	private Date dataFinal;
	
	public long getCodigo() {
		return codigo;
	}
	public void setCodigo(long codigo) {
		this.codigo = codigo;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public BigDecimal getSaldoInicial() {
		return saldoInicial;
	}
	public void setSaldoInicial(BigDecimal saldoInicial) {
		this.saldoInicial = saldoInicial;
	}
	public TipoConta getTipo() {
		return tipo;
	}
	public void setTipo(TipoConta tipo) {
		this.tipo = tipo;
	}
	public BigDecimal getSaldoAtual() {
		return saldoAtual;
	}
	public void setSaldoAtual(BigDecimal saldoAtual) {
		this.saldoAtual = saldoAtual;
	}
	public Date getDataInicial() {
		return dataInicial;
	}
	public void setDataInicial(Date dataInicial) {
		this.dataInicial = dataInicial;
	}
	public Date getDataFinal() {
		return dataFinal;
	}
	public void setDataFinal(Date dataFinal) {
		this.dataFinal = dataFinal;
	}
}
