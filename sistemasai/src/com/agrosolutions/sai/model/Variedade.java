package com.agrosolutions.sai.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;
import org.primefaces.model.SortOrder;

@Entity
@SequenceGenerator(name = "entity_sequence", sequenceName = "sq_Variedade", allocationSize = 1)
@Table
@Audited
@NamedQueries({
	@NamedQuery(name = "Variedade.todos", 
			   query = "SELECT v FROM Variedade v inner join v.cultura c ORDER BY c.nome, v.nome ASC"), 			  
	@NamedQuery(name = "Variedade.excluir", 
			   query = "DELETE FROM Variedade v WHERE v.id = :pId"), 			  
	@NamedQuery(name = "Variedade.pesquisar", 
			   query = "SELECT v FROM Variedade v WHERE v.cultura = :pCultura ORDER BY v.nome ASC") 			  
   })
public class Variedade extends Model {

	private static final long serialVersionUID = 376853931367270342L;
	@Column(unique=true)
	private String nome;
	@ManyToOne
	private Cultura cultura;
	
	@Transient
	private String sortField;
	@Transient
	private SortOrder sortOrder;
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Cultura getCultura() {
		return cultura;
	}
	public void setCultura(Cultura cultura) {
		this.cultura = cultura;
	}
	public void setSortField(String sortField) {
		this.sortField = sortField;
	}
	public String getSortField() {
		return sortField;
	}
	public void setSortOrder(SortOrder sortOrder) {
		this.sortOrder = sortOrder;
	}
	public SortOrder getSortOrder() {
		return sortOrder;
	}
}
