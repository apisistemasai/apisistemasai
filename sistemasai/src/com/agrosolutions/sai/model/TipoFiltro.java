package com.agrosolutions.sai.model;

import com.agrosolutions.sai.util.ResourceBundle;

public enum TipoFiltro {

	Areia(0, "areia"),
	Disco(1, "disco"),
	Tela(2, "tela"),
	Centrifuga(3, "centrifuga"); 
	
	private Integer id;
	
	private String descricao;

	private TipoFiltro(Integer id, String descricao){
		this.id = id;
		this.descricao = descricao;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the descricao
	 */
	public String getDescricao() {
		return ResourceBundle.getMessage(descricao);
	}

	/**
	 * @param descricao the descricao to set
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}	
}
