package com.agrosolutions.sai.view;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.faces.model.SelectItem;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.AtividadeEconomica;
import com.agrosolutions.sai.model.CicloCultura;
import com.agrosolutions.sai.model.Indicadores;
import com.agrosolutions.sai.model.PesoIndicadores;
import com.agrosolutions.sai.model.PesoIndicadoresSegHidrica;
import com.agrosolutions.sai.service.IndicadoresService;
import com.agrosolutions.sai.util.BigDecimalUtil;
import com.agrosolutions.sai.util.ResourceBundle;

@Component("cadastrarIndicadorBean")
@Scope("session")
public class CadastrarIndicadorBean extends ManagerBean implements
		Serializable {

	private static final long serialVersionUID = 5029315015913771521L;

	public static Logger logger = Logger.getLogger(CadastrarIndicadorBean.class);
	@Autowired
	private IndicadoresService indicadoresService;
	private AtividadeEconomica atividadeEconomica;
	private Boolean calculoAutomatico = true;
	private Boolean calculoManual = false;
	private Indicadores indicador;
	private Boolean calAuto1 = true;
	private Boolean calAuto2 = true;
	private Boolean calAuto3 = true;
	private Boolean calAuto4 = true;
	private Boolean calVazao = false;
	private Date data;
	private List<Date> dates;
	private SelectItem[] listaDatas;

	
	

	public void carregarDadosIniciais() {
		atividadeEconomica = getAtividadeEconomica();
		dates = indicadoresService.pesquisarDatasPorAtividade(atividadeEconomica);
		listaDatas = criarFiltroData(dates);
		if (indicador == null){
			if(dates != null && !dates.isEmpty()){
				indicador = indicadoresService.pesquisarPorAtividadeEconomica(atividadeEconomica, dates.get(0));
			}else{
				novo();
			}
		}
	}

	public void consulta() {
		indicador = indicadoresService.pesquisarPorAtividadeEconomica(atividadeEconomica, data);
	}

	private SelectItem[] criarFiltroData(List<Date> indicadores)  {  
        SelectItem[] options = new SelectItem[indicadores.size()];  
  
        for(int i = 0; i < indicadores.size(); i++) {  
        	Date data = indicadores.get(i);
        	Calendar dia = new GregorianCalendar();
        	dia.setTime(data);
        	String dataFormatada = dia.get(Calendar.DAY_OF_MONTH) + "/" + (dia.get(Calendar.MONTH)+1) + "/" + dia.get(Calendar.YEAR);
            options[i] = new SelectItem(data, dataFormatada);  
        }  
  
        return options;  
    }  
	public void novo() {
		indicador = new Indicadores();
		data = new Date();
		if (verificarData(data)){
			dates = new ArrayList<Date>();
			dates.add(data);
			listaDatas = criarFiltroData(dates);
			indicador.setData(data);
		}else{
			errorMsg(ResourceBundle.getMessage("dataExiste"), null);
		}
	}
	
	public boolean verificarData(Date data){
		Calendar dataHoje = new GregorianCalendar();
		dataHoje.setTime(data);
		for (Date date : dates) {
			Calendar d = new GregorianCalendar();
			d.setTime(date);
			if (d.get(Calendar.DAY_OF_MONTH) == dataHoje.get(Calendar.DAY_OF_MONTH) &&
					d.get(Calendar.MONTH) == dataHoje.get(Calendar.MONTH) &&
							d.get(Calendar.YEAR) == dataHoje.get(Calendar.YEAR)){
				return false;
			}
		}
		return true;
	}
	
	public void salvar() {
		boolean valido = true;
		try {
			if(calVazao){
				indicador.setCicloCultura(null);
			}else{
				if (indicador.getCicloCultura().getValor().compareTo(new BigDecimal(0)) == 0){
					errorMsg(ResourceBundle.getMessage("cicloCulturaNaoInformado"), null);
					valido = false;
				}else{
					indicador.setPesoLitrosSegundoHa(null);
					indicador.setLitrosSegundoHa(null);
				}
			}
			if (valido) {
				indicador = indicadoresService.salvar(indicador);
				infoMsg(ResourceBundle.getMessage("indicadoresSalvarSucesso"), null);
			}
		} catch (Exception e) {
			errorMsg(ResourceBundle.getMessage("indicadoresSalvarErro"), null);
		}
	}
	
	public void calcularKgHa(){
		BigDecimal kgHa = indicador.getProdutividade().divide(indicador.getAtividadeEconomica().getAreatotal(), BigDecimalUtil.MC).setScale(2, RoundingMode.FLOOR);
		indicador.setKgHa(kgHa);
	}
	
	public void calcularKgM3(){
		BigDecimal kgM3 = indicador.getProdutividade().divide(indicador.getLaminaAplicada(), BigDecimalUtil.MC).setScale(2, RoundingMode.FLOOR);
		indicador.setKgM3(kgM3);
		
		// Calculo Seg Hidrica
		
		if (indicador.getReceitaLiquidaM3() != null)
			calcularReceitaLiquidaM3();
		
		if (indicador.getEmpregosM3() != null)
			calcularEmpregosM3();
		
		if (indicador.getAtividadeEconomica().getAreatotal() != null)
			calcularM3Ha();
	}

	public void calcularSegEco(){
		BigDecimal receitaLiquidaHa = indicador.getReceitaLiquida().divide(indicador.getAtividadeEconomica().getAreatotal(), BigDecimalUtil.MC).setScale(2, RoundingMode.FLOOR);
		
		indicador.setReceitaLiquidaHa(receitaLiquidaHa);
		calcularReceitaLiquidaM3();
	}
	
	public void calcularReceitaLiquidaM3(){		
		BigDecimal receitaLiquidaM3 = indicador.getReceitaLiquida().divide(indicador.getLaminaAplicada(), BigDecimalUtil.MC).setScale(2, RoundingMode.FLOOR);
		indicador.setReceitaLiquidaM3(receitaLiquidaM3);
	}
	
	public void calcularSegSoc(){
		BigDecimal empregosHa = indicador.getEmpregos().divide(indicador.getAtividadeEconomica().getAreatotal(), BigDecimalUtil.MC).setScale(2, RoundingMode.FLOOR);		
		indicador.setEmpregosHa(empregosHa);
		calcularEmpregosM3();
	}
	
	public void calcularEmpregosM3(){
		BigDecimal empregosM3 = indicador.getEmpregos().divide(indicador.getLaminaAplicada(), BigDecimalUtil.MC).multiply(new BigDecimal(1000), BigDecimalUtil.MC).setScale(3, RoundingMode.FLOOR);
		indicador.setEmpregosM3(empregosM3);
	}
	
	public void calcularSegHid(){
		BigDecimal litrosSegHa = indicador.getLitrosSegundo().divide(indicador.getAtividadeEconomica().getAreatotal(), BigDecimalUtil.MC).setScale(2, RoundingMode.FLOOR);
		
		indicador.setLitrosSegundoHa(litrosSegHa);
	}
	
	private void calcularM3Ha() {
		BigDecimal m3Ha = indicador.getLaminaAplicada().divide(indicador.getAtividadeEconomica().getAreatotal(), BigDecimalUtil.MC).setScale(2, RoundingMode.FLOOR);
		indicador.setM3Ha(m3Ha);
	}
	
	public PesoIndicadores[] getPesos() {
		return PesoIndicadores.values();
	}
	
	public PesoIndicadoresSegHidrica[] getPesosSegHid() {
		return PesoIndicadoresSegHidrica.values();
	}

	public CicloCultura[] getCicloCulturas() {
		return CicloCultura.values();
	}

	public String voltar() {
		return "consultarAtividadesEconomica?faces-redirect=true";
	}

	public AtividadeEconomica getAtividadeEconomica() {
		return atividadeEconomica;
	}

	public void setAtividadeEconomica(AtividadeEconomica atividadeEconomica) {
		this.atividadeEconomica = atividadeEconomica;
	}
	
	public Boolean getCalculoAutomatico() {
		return calculoAutomatico;
	}

	public void setCalculoAutomatico(Boolean calculoAutomatico) {
		this.calculoAutomatico = calculoAutomatico;
	}

	public Boolean getCalculoManual() {
		return calculoManual;
	}

	public void setCalculoManual(Boolean calculoManual) {
		this.calculoManual = calculoManual;
	}

	public Indicadores getIndicador() {
		return indicador;
	}

	public void setIndicador(Indicadores indicador) {
		this.indicador = indicador;
	}
	public Boolean getCalAuto1() {
		return calAuto1;
	}

	public void setCalAuto1(Boolean calAuto1) {
		this.calAuto1 = calAuto1;
	}

	public Boolean getCalAuto2() {
		return calAuto2;
	}

	public void setCalAuto2(Boolean calAuto2) {
		this.calAuto2 = calAuto2;
	}

	public Boolean getCalAuto3() {
		return calAuto3;
	}

	public void setCalAuto3(Boolean calAuto3) {
		this.calAuto3 = calAuto3;
	}

	public Boolean getCalAuto4() {
		return calAuto4;
	}

	public void setCalAuto4(Boolean calAuto4) {
		this.calAuto4 = calAuto4;
	}

	public Boolean getCalVazao() {
		return calVazao;
	}

	public void setCalVazao(Boolean calVazao) {
		this.calVazao = calVazao;
	}

	public SelectItem[] getListaDatas() {
		return listaDatas;
	}

	public void setListaDatas(SelectItem[] listaDatas) {
		this.listaDatas = listaDatas;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

}
