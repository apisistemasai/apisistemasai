package com.agrosolutions.sai.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.Distrito;
import com.agrosolutions.sai.model.Perfil;
import com.agrosolutions.sai.model.Usuario;
import com.agrosolutions.sai.service.PerfilService;
import com.agrosolutions.sai.service.UsuarioService;
import com.agrosolutions.sai.util.ResourceBundle;

/**
 * Use case : Consultar Usu�rio
 * 
 * @since : Jan 05, 2011, 20:23:20 PM
 * @author : raphael.ferreira@ivia.com.br
 */

@Component("consultarUsuarioBean")
@Scope("session")
public class ConsultarUsuarioBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = 9214653707763104608L;

	public static Logger logger = Logger.getLogger(ConsultarUsuarioBean.class);

	public static final String MSG_TITULO = "Consultar Usu�rio";

	@Autowired
	private UsuarioService usuarioService;
	@Autowired
	private PerfilService perfilService;

	private LazyDataModel<Usuario> usuarios;
	private List<Perfil> perfis;
	private Usuario usuarioBusca;
	private Usuario usuarioSelecionado;
	private List<Distrito> distritosPesquisa = new ArrayList<Distrito>();
	private HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
	private Boolean multidistrito = (Boolean) getRequest().getSession(true).getAttribute("multidistrito");

	public void carregarDadosIniciais() {
		
		if (getMultidistrito()){
			this.distritosPesquisa.clear();
			this.distritosPesquisa.add(getUsuarioLogado().getDistritoPadrao());
		}else{
			this.distritosPesquisa.clear();
			this.distritosPesquisa = getUsuarioLogado().getDistritos();
		}


		if (usuarioBusca == null) {
			usuarioBusca = new Usuario();
			usuarioBusca.setDistritos(this.distritosPesquisa);

			consultar();

			// Popular combobox de filtros
			perfis = perfilService.obterTodos();
		}
	}

	public String cadastro() {
		return "cadastrarUsuario?faces-redirect=true";
	}

	// Desabilita o usu�rio
	public void excluir() {
		usuarioSelecionado.setAtivo(false);
		usuarioService.salvar(usuarioSelecionado);
		usuarioBusca = null;
		infoMsg(ResourceBundle.getMessage("usuarioExcluir") + ".", MSG_TITULO);
	}

	public void consultar() {
		usuarios = new LazyDataModel<Usuario>() {

			private static final long serialVersionUID = -8272901981253486943L;

			@Override
			public List<Usuario> load(int first, int pageSize,
					String sortField, SortOrder sortOrder,
					Map<String, String> filters) {
				List<Usuario> usuarios = new ArrayList<Usuario>();

				usuarioBusca.setSortField(sortField);
				usuarioBusca.setSortOrder(sortOrder);

				usuarioBusca.setAtivo(true);

				usuarios = usuarioService.pesquisar(usuarioBusca, first,
						pageSize);

				return usuarios;
			}
		};
		usuarios.setRowCount(usuarioService.quantidade(usuarioBusca));
	}

	public LazyDataModel<Usuario> getUsuarios() {
		return usuarios;
	}

	public void setUsuarios(LazyDataModel<Usuario> usuarios) {
		this.usuarios = usuarios;
	}

	public Usuario getUsuarioBusca() {
		return usuarioBusca;
	}

	public void setUsuarioBusca(Usuario usuarioBusca) {
		this.usuarioBusca = usuarioBusca;
	}

	public Usuario getUsuarioSelecionado() {
		return usuarioSelecionado;
	}

	public void setUsuarioSelecionado(Usuario usuarioSelecionado) {
		this.usuarioSelecionado = usuarioSelecionado;
	}

	public List<Perfil> getPerfis() {
		return perfis;
	}

	public void setPerfis(List<Perfil> perfis) {
		this.perfis = perfis;
	}

	public HttpServletRequest getRequest() {
		return request;
	}

	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}

	public Boolean getMultidistrito() {
		return multidistrito;
	}

	public void setMultidistrito(Boolean multidistrito) {
		this.multidistrito = multidistrito;
	}

	public List<Distrito> getDistritosPesquisa() {
		return distritosPesquisa;
	}

	public void setDistritosPesquisa(List<Distrito> distritosPesquisa) {
		this.distritosPesquisa = distritosPesquisa;
	}
}
