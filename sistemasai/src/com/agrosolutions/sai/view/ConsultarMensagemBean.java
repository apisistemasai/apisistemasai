package com.agrosolutions.sai.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.Mensagem;
import com.agrosolutions.sai.model.TipoMensagem;
import com.agrosolutions.sai.service.IrriganteService;
import com.agrosolutions.sai.service.MensagemService;
import com.agrosolutions.sai.service.SmsService;

/**
* Use case : Consultar Mensagem
* 
* @since   : Ago 03, 2012, 20:23:20 PM
* @author  : rpf1404@gmail.com
*/

@Component("consultarMensagemBean")
@Scope("view")
public class ConsultarMensagemBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = 6142454950893070729L;

	public static Logger logger = Logger.getLogger(ConsultarMensagemBean.class);

	@Autowired
	private MensagemService mensagemService;
	@Autowired
	private IrriganteService irriganteService;
	@Autowired
	private SmsService smsService;
	
	private LazyDataModel<Mensagem> mensagens;
	private Mensagem mensagemBusca;
	private String lote;
	private String saldo;
	   
   public void carregarDadosIniciais() {
		
		if(mensagemBusca == null){
			mensagemBusca = new Mensagem();
			mensagemBusca.setSortField("dataEnvio");
			mensagemBusca.setSortOrder(SortOrder.ASCENDING);
			mensagemBusca.setDataEnvio(new Date());
			mensagemBusca.setDistritos(getDistritosPesquisa());
			consultar();
		}
		
	}
   
	public void consultar() {
		if (lote != null && !lote.isEmpty()) {
			mensagemBusca.setIrrigante(irriganteService.pesquisarPorLote(lote, mensagemBusca.getDistritos()));
		}
		
		mensagens = new LazyDataModel<Mensagem>() {

			private static final long serialVersionUID = 3788859659067356473L;

			@Override
			public List<Mensagem> load(int first, int pageSize, String sortField,
					SortOrder sortOrder, Map<String, String> filters) {
				List<Mensagem> mensagens = new ArrayList<Mensagem>();

		        if(sortField == null){
					sortField = "dataEnvio";
					sortOrder = SortOrder.ASCENDING;
				}
		        
				mensagemBusca.setSortField(sortField);
				mensagemBusca.setSortOrder(sortOrder);
				mensagens = mensagemService.pesquisar(mensagemBusca, first, pageSize);

				return mensagens;
			}
        };
        mensagens.setRowCount(mensagemService.quantidade(mensagemBusca));
        /*
        try {
			saldo = smsService.consultarSaldo();
		} catch (SaiException e) {
			e.printStackTrace();
		} catch (SmsException e) {
			e.printStackTrace();
		}
		*/
	}

	public LazyDataModel<Mensagem> getMensagens() {
		return mensagens;
	}

	public void setMensagens(LazyDataModel<Mensagem> mensagens) {
		this.mensagens = mensagens;
	}

	public Mensagem getMensagemBusca() {
		return mensagemBusca;
	}

	public void setMensagemBusca(Mensagem mensagemBusca) {
		this.mensagemBusca = mensagemBusca;
	}

	public String getLote() {
		return lote;
	}

	public void setLote(String lote) {
		this.lote = lote;
	}

	public TipoMensagem[] getTiposMensagem() {
		return TipoMensagem.values();
	}

	public String getSaldo() {
		return saldo;
	}

	public void setSaldo(String saldo) {
		this.saldo = saldo;
	}
}
