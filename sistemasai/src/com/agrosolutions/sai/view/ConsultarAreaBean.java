package com.agrosolutions.sai.view;

import java.io.Serializable;
import java.util.List;

import org.apache.log4j.Logger;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.Area;
import com.agrosolutions.sai.service.AreaService;
import com.agrosolutions.sai.util.ResourceBundle;

/**
* Use case : Consultar �reas
* 
* @since   : Jan 06, 2011, 14:23:20 AM
* @author  : raphael.ferreira@ivia.com.br
*/

@Component("consultarAreaBean")
@Scope("view")
public class ConsultarAreaBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = 7256705566321446081L;

	public static Logger logger = Logger.getLogger(ConsultarAreaBean.class);

	@Autowired
	private AreaService areaService;
	
	private List<Area> areas;
	private Area areaSelecionado;
	private Area areaBusca;

	
	public void carregarDadosIniciais() {
		if (areaBusca == null){
			areaBusca = new Area();
			areaBusca.setSortField("nome");
    		areaBusca.setSortOrder(SortOrder.ASCENDING);
    		areaBusca.setDistritos(getDistritosPesquisa());
    		consultar();
		}
	}
	
	public void consultar(){
		areas = areaService.pesquisarArea(areaBusca, null, null);
	}

	public String clima() {
		return "consultarClima?faces-redirect=true";
	}

	public void excluir(){
	   	areaService.excluir(areaSelecionado);
		infoMsg(ResourceBundle.getMessage("areaExcluir")+ ".","");
    }
	
	public String cadastro() {
		return "cadastrarArea?faces-redirect=true";
	}

	public List<Area> getAreas() {
		return areas;
	}

	public void setAreas(List<Area> areas) {
		this.areas = areas;
	}

	public Area getAreaSelecionado() {
		return areaSelecionado;
	}

	public void setAreaSelecionado(Area areaSelecionado) {
		this.areaSelecionado = areaSelecionado;
	}

	public void setAreaBusca(Area areaBusca) {
		this.areaBusca = areaBusca;
	}

	public Area getAreaBusca() {
		return areaBusca;
	}

}
