package com.agrosolutions.sai.view;

import java.io.Serializable;

import javax.faces.event.ActionEvent;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.Fornecedor;
import com.agrosolutions.sai.service.FornecedorService;
import com.agrosolutions.sai.util.ResourceBundle;

/**
* Use case : Cadastrar Fornecedor
* 
* @since   : Nov 26, 2012, 22:10:20 AM
* @author  : mateus.gabriel@ivia.com.br
*/

@Component("cadastrarFornecedorBean")
@Scope("session")
public class CadastrarFornecedorBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = -1335171156978434238L;

	public static Logger logger = Logger.getLogger(CadastrarFornecedorBean.class);

	public static final String MSG_TITULO = "Cadastro de Fornecedor";

	@Autowired
	private FornecedorService fornecedorService;
	
	private Fornecedor fornecedor;
	private String operacao;
	
	public void carregarDadosIniciais() {
		if (operacao.equals("NOVO") && fornecedor == null) {
			fornecedor = new Fornecedor();
		}
	}
	
	public void gravar(ActionEvent actionEvent) {
		fornecedor = fornecedorService.salvar(fornecedor);
		if (operacao.equals("NOVO")) {
			fornecedor = null;
			infoMsg(ResourceBundle.getMessage("fornecedorSalvar")+ ".", MSG_TITULO);
		}else{
			infoMsg(ResourceBundle.getMessage("fornecedorAlterar")+ ".", MSG_TITULO);
		}
    }

	public String voltar() {
		return "consultarFornecedores?faces-redirect=true";
	}

	public FornecedorService getFornecedorService() {
		return fornecedorService;
	}

	public void setFornecedorService(FornecedorService fornecedorService) {
		this.fornecedorService = fornecedorService;
	}

	public Fornecedor getFornecedor() {
		return fornecedor;
	}

	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}

	public String getOperacao() {
		return operacao;
	}

	public void setOperacao(String operacao) {
		this.operacao = operacao;
	}

	
}
