package com.agrosolutions.sai.view;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.FertirrigacaoAplicada;
import com.agrosolutions.sai.model.Irrigante;
import com.agrosolutions.sai.model.Nutriente;
import com.agrosolutions.sai.model.Plantio;
import com.agrosolutions.sai.model.Tanque;
import com.agrosolutions.sai.model.TipoPerfil;
import com.agrosolutions.sai.service.FertirrigacaoAplicadaService;
import com.agrosolutions.sai.service.IrriganteService;
import com.agrosolutions.sai.service.NutrienteService;
import com.agrosolutions.sai.service.PlantioService;
import com.agrosolutions.sai.service.TanqueService;
import com.agrosolutions.sai.util.ResourceBundle;

/**
* Use case : Consultar Aplica��o
* 
* @since   : Dez 18, 2015, 22:23:20 AM
* @author  : raphael@iasoft.me
*/

@Component("consultarAplicacaoBean")
@Scope("view")
public class ConsultarAplicacaoBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = -6674416492636049088L;

	public static Logger logger = Logger.getLogger(ConsultarAplicacaoBean.class);

	@Autowired
	private FertirrigacaoAplicadaService aplicacaoService;
	@Autowired
	private TanqueService tanqueService;
	@Autowired
	private PlantioService plantioService;
	@Autowired
	private NutrienteService nutrienteService;
	@Autowired
	private IrriganteService irriganteService;
	
	private List<FertirrigacaoAplicada> aplicacoes;
	private FertirrigacaoAplicada aplicacaoSelecionada;
	private List<Tanque> tanques;
	private List<Plantio> plantios;
	private List<Nutriente> nutrientes;
	private Irrigante irrigante;
	private Date dataIni;
	private Date dataFim;

	
	private FertirrigacaoAplicada aplicacaoBusca;

	public void carregarDadosIniciais() {
		if (getUsuarioLogado().getPerfil().getTipo().equals(TipoPerfil.Irrigante)) {
			setIrrigante(irriganteService.pesquisarPorUsuario(getUsuarioLogado()));
			if(aplicacaoBusca == null){
				aplicacaoBusca = new FertirrigacaoAplicada();
				aplicacaoBusca.setSortField("data");
				aplicacaoBusca.setSortOrder(SortOrder.ASCENDING);

				this.setTanques(tanqueService.obterTodos());
				this.setNutrientes(nutrienteService.obterTodos());
				this.setPlantios(plantioService.pesquisarPorIrrigante(getIrrigante()));

				consultar();
			}
		}else{
			errorMsg(ResourceBundle.getMessage("aplicacaoIrrigante")+ ".", null);
		}
		
		
	}

	public void consultar(){	
		if (getDataIni()!= null){
			aplicacaoBusca.setDataIni(getDataIni());
		}
		if (getDataFim()!= null){
			aplicacaoBusca.setDataFim(getDataFim());
		}
		aplicacoes = aplicacaoService.pesquisar(aplicacaoBusca, null, null);
	}
	
	public void excluir(){
	   aplicacaoService.excluir(aplicacaoSelecionada);
		infoMsg(ResourceBundle.getMessage("aplicacaoExcluir")+ ".", null);
   }

   public String cadastro() {
		return "cadastrarAplicacao?faces-redirect=true";
	}
	
	public List<FertirrigacaoAplicada> getAplicacoes() {
		return aplicacoes;
	}

	public void setAplicacoes(List<FertirrigacaoAplicada> aplicacoes) {
		this.aplicacoes = aplicacoes;
	}

	public FertirrigacaoAplicada getAplicacaoSelecionada() {
		return aplicacaoSelecionada;
	}

	public void setAplicacaoSelecionada(FertirrigacaoAplicada aplicacaoSelecionada) {
		this.aplicacaoSelecionada = aplicacaoSelecionada;
	}

	public void setAplicacaoBusca(FertirrigacaoAplicada aplicacaoBusca) {
		this.aplicacaoBusca = aplicacaoBusca;
	}

	public FertirrigacaoAplicada getAplicacaoBusca() {
		return aplicacaoBusca;
	}

	public List<Tanque> getTanques() {
		return tanques;
	}

	public void setTanques(List<Tanque> tanques) {
		this.tanques = tanques;
	}

	public List<Plantio> getPlantios() {
		return plantios;
	}

	public void setPlantios(List<Plantio> plantios) {
		this.plantios = plantios;
	}

	public List<Nutriente> getNutrientes() {
		return nutrientes;
	}

	public void setNutrientes(List<Nutriente> nutrientes) {
		this.nutrientes = nutrientes;
	}

	public Irrigante getIrrigante() {
		return irrigante;
	}

	public void setIrrigante(Irrigante irrigante) {
		this.irrigante = irrigante;
	}

	public Date getDataIni() {
		return dataIni;
	}

	public void setDataIni(Date dataIni) {
		this.dataIni = dataIni;
	}

	public Date getDataFim() {
		return dataFim;
	}

	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}

}
