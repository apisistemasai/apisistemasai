package com.agrosolutions.sai.view;

import java.io.Serializable;
import java.util.List;

import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.Doenca;
import com.agrosolutions.sai.service.DoencaService;
import com.agrosolutions.sai.util.ResourceBundle;

/**
* Use case : Consultar Doença
* 
* @since   : Jan 27, 2012, 23:23:20 AM
* @author  : raphael.ferreira@ivia.com.br
*/

@Component("consultarDoencaBean")
@Scope("view")
public class ConsultarDoencaBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = -8165336592906347423L;

	public static final String MSG_TITULO = "Consultar Doenças";

	@Autowired
	private DoencaService doencaService;
	
	private List<Doenca> doencas;
	private Doenca doencaSelecionado;
	private Doenca doencaBusca;
	
	public void carregarDadosIniciais() {
		if (doencaBusca == null){
			doencaBusca = new Doenca();
			doencaBusca.setSortField("nome");
			doencaBusca.setSortOrder(SortOrder.ASCENDING);
			
			consultar();
		}
	}
	public void consultar(){
		doencas = doencaService.pesquisar(doencaBusca, null, null);
	}

	public String cadastro() {
		return "cadastrarDoenca?faces-redirect=true";
	}

   public void excluir(){
	    doencaService.excluir(doencaSelecionado);
	    doencaSelecionado = null;
	    consultar();

	    infoMsg(ResourceBundle.getMessage("doencaExcluir")+ ".","");
   }

	public List<Doenca> getDoencas() {
		return doencas;
	}
	
	public void setDoencas(List<Doenca> doencas) {
		this.doencas = doencas;
	}
	
	public Doenca getDoencaSelecionado() {
		return doencaSelecionado;
	}
	
	public void setDoencaSelecionado(Doenca doencaSelecionado) {
		this.doencaSelecionado = doencaSelecionado;
	}

	public void setDoencaBusca(Doenca doencaBusca) {
		this.doencaBusca = doencaBusca;
	}

	public Doenca getDoencaBusca() {
		return doencaBusca;
	}  	

}
