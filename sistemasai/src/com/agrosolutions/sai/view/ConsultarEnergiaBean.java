package com.agrosolutions.sai.view;

import java.io.Serializable;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.Energia;
import com.agrosolutions.sai.model.Irrigante;
import com.agrosolutions.sai.model.TipoPerfil;
import com.agrosolutions.sai.service.EnergiaService;
import com.agrosolutions.sai.service.IrriganteService;
import com.agrosolutions.sai.util.ResourceBundle;

/**
* Use case : Consultar Energia
* 
* @since   : Maio 24, 2012, 10:00:20 AM
* @author  : walljr@gmail.com
*/

@Component("consultarEnergiaBean")
@Scope("session")
public class ConsultarEnergiaBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = -9163301828239605509L;

	public static Logger logger = Logger.getLogger(ConsultarEnergiaBean.class);

	public static final String MSG_TITULO = "Consultar Energia";

	@Autowired
	private EnergiaService energiaService;
	@Autowired
	private IrriganteService irriganteService;
	
	private List<Energia> energias;
	private Energia energiaSelecionado;
	private Irrigante irrigante;
		
	
	public void carregarDadosIniciais() {
		if (getUsuarioLogado().getPerfil().getTipo().equals(TipoPerfil.Irrigante)) {
			irrigante = irriganteService.pesquisarPorUsuario(getUsuarioLogado());
		}
		
		energias = energiaService.pesquisar(irrigante);
	}

	public void excluir(){
		energiaService.excluir(energiaSelecionado);
		energiaSelecionado = null;
		infoMsg(ResourceBundle.getMessage("energiaExcluir")+ ".", MSG_TITULO);
	}
	
	public String cadastro() {
		return "cadastrarEnergia?faces-redirect=true";
	}
	
	public String voltar() {
		if (getUsuarioLogado().getPerfil().getTipo().equals(TipoPerfil.Irrigante)) {
			return "/homeIrrigante?faces-redirect=true";
		} else {
			return "consultarIrrigante?faces-redirect=true";
		}
	}

	public List<Energia> getEnergias() {
		return energias;
	}

	public void setEnergias(List<Energia> energias) {
		this.energias = energias;
	}

	public Irrigante getIrrigante() {
		return irrigante;
	}

	public void setIrrigante(Irrigante irrigante) {
		this.irrigante = irrigante;
	}

	public Energia getEnergiaSelecionado() {
		return energiaSelecionado;
	}

	public void setEnergiaSelecionado(Energia energiaSelecionado) {
		this.energiaSelecionado = energiaSelecionado;
	}

}
