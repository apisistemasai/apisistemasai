package com.agrosolutions.sai.view;

import java.awt.Color;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.marre.sms.SmsException;
import org.primefaces.event.map.OverlaySelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;
import org.primefaces.model.map.DefaultMapModel;
import org.primefaces.model.map.LatLng;
import org.primefaces.model.map.MapModel;
import org.primefaces.model.map.Marker;
import org.primefaces.model.map.Polyline;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.exception.SaiException;
import com.agrosolutions.sai.model.Area;
import com.agrosolutions.sai.model.Coordenada;
import com.agrosolutions.sai.model.Cultura;
import com.agrosolutions.sai.model.Irrigante;
import com.agrosolutions.sai.model.Mensagem;
import com.agrosolutions.sai.model.TipoIrrigante;
import com.agrosolutions.sai.model.TipoMensagem;
import com.agrosolutions.sai.model.Variedade;
import com.agrosolutions.sai.service.AreaService;
import com.agrosolutions.sai.service.CoordenadaService;
import com.agrosolutions.sai.service.CulturaService;
import com.agrosolutions.sai.service.EmailService;
import com.agrosolutions.sai.service.IrrigacaoService;
import com.agrosolutions.sai.service.IrriganteService;
import com.agrosolutions.sai.service.MensagemService;
import com.agrosolutions.sai.service.SmsService;
import com.agrosolutions.sai.service.VariedadeService;
import com.agrosolutions.sai.util.ResourceBundle;

/**
 * Use case : Consultar Irrigante
 * 
 * @since : Dez 30, 2011, 20:23:20 PM
 * @author : raphael.ferreira@ivia.com.br
 */

@Component("consultarIrriganteBean")
@Scope("session")
public class ConsultarIrriganteBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = 2000210947938981045L;

	public static Logger logger = Logger
			.getLogger(ConsultarIrriganteBean.class);

	public static final String MSG_TITULO = "Consultar Irrigantes";

	@Autowired
	private IrriganteService irriganteService;
	@Autowired
	private AreaService areaService;
	@Autowired
	private SmsService smsService;
	@Autowired
	private EmailService emailService;
	@Autowired
	private MensagemService mensagemService;
	@Autowired
	private CulturaService culturaService;
	@Autowired
	private VariedadeService variedadeService;
	@Autowired
	private IrrigacaoService irrigacaoService;
	@Autowired
	private CoordenadaService coordenadaService;

	private LazyDataModel<Irrigante> irrigantes;
	private Irrigante irriganteBusca;
	private Irrigante irriganteSelecionado;
	private List<Area> areas;
	private List<Cultura> culturas;
	private List<Variedade> variedades;

	private Boolean modoSatelite = false;
	private MapModel irriganteMarcado;
	private Marker marker;
	private String central;
	private String msgEmail;
	private String msgSMS;
	private String assuntoEmail;
	private UploadedFile uploadAnexo;
	private Boolean piloto;
	private String localizacao;
	private StreamedContent file;
	private Integer zoomMapa;
	private MapModel model;
	
	public void carregarDadosIniciais() {
		
		//	Reseta os par�metros de pesquisa do Irrigante quando muda de distrito
		if ( super.getMultidistrito() ) {
			if ( (this.irriganteBusca != null) && (super.getDistritoPadrao().getId() != this.irriganteBusca.getDistritos().get(0).getId()) )
				this.irriganteBusca = null;
		}

		if (irriganteBusca == null) {
			irriganteBusca = new Irrigante();
			irriganteBusca.setSortField("nome");
			irriganteBusca.setSortOrder(SortOrder.ASCENDING);
			
			setZoomMapa(super.getDistritosPesquisa().get(0).getZoommapa());
			
			central = super.getDistritosPesquisa().get(0).getLatitude()+ ","+ super.getDistritosPesquisa().get(0).getLongitude();

			irriganteBusca.setDistritos(super.getDistritosPesquisa());

			// Popular combobox de filtros
			this.areas = areaService.pesquisar( super.getDistritosPesquisa() );
			this.culturas = culturaService.obterTodos();

			// Fazer consulta inicial
			consultar();
		}

		if (localizacao != null && !localizacao.isEmpty()) {
			irriganteBusca.setLocalizacao(localizacao);
			localizacao = null;
			// Fazer consulta inicial
			consultar();
		}
	}

	public void enviarTIGeral() throws SaiException, SmsException {
		int qtdMsg = irrigacaoService.enviarTIGeral();
		int qtdEmail = irrigacaoService.enviarEmailTI();
		
		if (qtdMsg > 0 && qtdEmail > 0) {
			infoMsg("TI Enviado com sucesso.", null);
		}else {
			errorMsg("O TI de hoje n�o foi gerado ou todos os irrigantes est�o desabilitados para receber SMS.", null);
		}
	}

	public void carregarVariedades() {
		if (irriganteBusca.getCultura() == null) {
			variedades = null;
		} else {
			variedades = variedadeService
					.pesquisar(irriganteBusca.getCultura());
		}
		irriganteBusca.setVariedade(null);
	}

	public void consultar() {
		preparaBuscaIrrigante();

		irrigantes = new LazyDataModel<Irrigante>() {
			private static final long serialVersionUID = -7291827228017454370L;

			@Override
			public List<Irrigante> load(int first, int pageSize,
					String sortField, SortOrder sortOrder,
					Map<String, String> filters) {
				List<Irrigante> irrigantes = new ArrayList<Irrigante>();
				String lote = irriganteBusca.getLocalizacao();

				if (lote != null) {
					preparaConsultaPorLote();
				}

				if (sortField == null) {
					sortField = "nome";
					sortOrder = SortOrder.ASCENDING;
				}

				irriganteBusca.setSortField(sortField);
				irriganteBusca.setSortOrder(sortOrder);

				irrigantes = irriganteService.pesquisar(irriganteBusca, first, pageSize);

				return irrigantes;
			}
		};
		int quantidade = irriganteService.quantidade(irriganteBusca);
		irrigantes.setRowCount(quantidade);
		if (modoSatelite) {
			modoSatelite();
		}
	}

	public void preparaConsultaPorLote() {
		String lote = irriganteBusca.getLocalizacao();
		if (!lote.isEmpty()) {
			irriganteBusca = new Irrigante();
			irriganteBusca.setLocalizacao(lote);
			irriganteBusca.setDistritos(super.getDistritosPesquisa());
		}
		
		String[] lotes = lote.split(",");
		
		if ( lotes.length > 1) {
			irriganteBusca.setLotes(new ArrayList<String>());
			for (String lotePesquisa : lotes) {
				irriganteBusca.getLotes().add(lotePesquisa);
			}
		}
	}

	public void modoSatelite() {
		
		modoSatelite = true;
		LatLng coord;
		irriganteMarcado = new DefaultMapModel();
		List<Irrigante> irrigantes = recuperarIrrigantes();
		
		for (Irrigante i : irrigantes) {
			if (i.getLatitude() != null && i.getLongitude() != null && !i.getLatitude().isEmpty() && !i.getLongitude().isEmpty()) {
				// Coordenadas
				coord = new LatLng(Double.parseDouble(i.getLatitude().replaceAll("\\s","")),
						Double.parseDouble(i.getLongitude().replaceAll("\\s","")));
				// Icones e Dados
				irriganteMarcado
						.addOverlay(new Marker(coord, i.getNome(), i,
								"http://maps.google.com/mapfiles/ms/micons/blue-dot.png"));
			} else {
				warnMsg(ResourceBundle.getMessage("preenchaCoordenadas")+i.getNome(), null);
			}
			
			// Desenha poligono
			List<Coordenada> coordenadas = new ArrayList<Coordenada>();
			coordenadas = coordenadaService.pesquisarPorIrrigante(i);
			Polyline polyline = new Polyline();
			if (!coordenadas.isEmpty()) {	
				for (Coordenada coor : coordenadas) {
					polyline.getPaths().add(new LatLng(coor.getLatitude(),coor.getLongitude()));							
				}
			}
			polyline.setStrokeWeight(5);
			polyline.setStrokeColor("#"+gerarHexCor());
			polyline.setStrokeOpacity(1);
			irriganteMarcado.addOverlay(polyline);
		}
	}
	
	public String gerarHexCor(){
		int red = (int) (( Math.random()*255)+1);
		int green = (int) (( Math.random()*255)+1);
		int blue = (int) (( Math.random()*255)+1);
		
		Color RandomC = new Color(red,green,blue);
		int RandomRGB = (RandomC.getRGB());
		String RandomRGB2Hex = Integer.toHexString(RandomRGB);
		if (RandomRGB2Hex.length() < 6) {
			RandomRGB2Hex = "000000".substring(0, 6 - RandomRGB2Hex.length()) + RandomRGB2Hex;
	   }
		
		return RandomRGB2Hex.substring(0, 6);
	}

	public void modoTabela() {
		modoSatelite = false;
	}

	public void marcadorSelecionado(OverlaySelectEvent event) {
		marker = (Marker) event.getOverlay();
		irriganteSelecionado = (Irrigante) marker.getData();
	}

	public void excluir() {
		irriganteService.excluir(irriganteSelecionado);
		irriganteBusca = null;
		irriganteSelecionado = null;
		infoMsg("Irrigante exclu�do com sucesso.", null);
	}

	public void enviarSMS() {
		boolean erro = false;
		String ierro = "";
		if (!msgSMS.equals("")) {
			List<String> telefone = new ArrayList<String>();
			List<Irrigante> dest = new ArrayList<Irrigante>();
			List<Irrigante> irrigantes = recuperarIrrigantes();
			for (Irrigante i : irrigantes) {
				String tel = i.getCelular().replace("(", "").replace(")", "")
						.replace("-", "");
				if (i.getCelular() != null && !i.getCelular().isEmpty()
						&& !telefone.contains(tel)) {
					try {
						smsService.enviarSMS2(tel, msgSMS);
						telefone.add(tel);
						dest.add(i);
					} catch (SmsException e) {
						erro = true;
						ierro = ierro + i.getLocalizacao() + "/"
								+ i.getCelular() + "; ";
						e.printStackTrace();
					} catch (SaiException e) {
						erro = true;
						ierro = ierro + i.getLocalizacao() + "-"
								+ i.getCelular() + " - " + e.getMessage();
						e.printStackTrace();
					}
				}
			}
			if (!dest.isEmpty()) {
				Mensagem msg = criarMensagem(dest, null, msgSMS, null,
						TipoMensagem.SMS);
				mensagemService.salvar(msg);
			}
			msgSMS = null;

			if (erro) {
				errorMsg(ResourceBundle.getMessage("irriganteErro") + ":"
						+ ierro, null);
			} else {
				infoMsg(ResourceBundle.getMessage("smsEnviado") + ".", null);
			}
		} else {
			infoMsg(ResourceBundle.getMessage("irriganteInfo") + ".", null);
		}
	}

	public void enviarInfoBibSMS() {
		boolean erro = false;
		String ierro = "";
		if (!msgSMS.equals("")) {
			List<String> telefone = new ArrayList<String>();
			List<Irrigante> dest = new ArrayList<Irrigante>();
			List<Irrigante> irrigantes = recuperarIrrigantes();
			
			for (Irrigante i : irrigantes) {
				String tel = "55"
						+ i.getCelular().replace("(", "").replace(")", "")
								.replace("-", "");
				if (i.getCelular() != null && !i.getCelular().isEmpty()
						&& !telefone.contains(tel)) {
					try {
						smsService.enviarSMSInfoBip(tel, msgSMS);
						telefone.add(tel);
						dest.add(i);
					} catch (SmsException e) {
						erro = true;
						ierro = ierro + i.getLocalizacao() + "/"
								+ i.getCelular() + "; ";
						e.printStackTrace();
					} catch (SaiException e) {
						erro = true;
						ierro = ierro + i.getLocalizacao() + "-"
								+ i.getCelular() + " - " + e.getMessage();
						e.printStackTrace();
					}
				}
			}
			if (!dest.isEmpty()) {
				Mensagem msg = criarMensagem(dest, null, msgSMS, null,
						TipoMensagem.SMS);
				mensagemService.salvar(msg);
			}
			msgSMS = null;

			if (erro) {
				errorMsg(ResourceBundle.getMessage("irriganteErro") + ":"
						+ ierro, null);
			} else {
				infoMsg(ResourceBundle.getMessage("smsEnviado") + ".", null);
			}
		} else {
			infoMsg(ResourceBundle.getMessage("irriganteInfo") + ".", null);
		}
	}

	private List<Irrigante> recuperarIrrigantes() {
		List<Irrigante> irrigantes = null;
		if (this.irrigantes.getRowCount() > this.irrigantes.getPageSize()) {
			irrigantes = irriganteService.pesquisar(irriganteBusca, 0,
					this.irrigantes.getRowCount());
		} else {
			irrigantes = new ArrayList<Irrigante>();
			for (Irrigante i : this.irrigantes) {
				irrigantes.add(i);
			}
		}
		return irrigantes;
	}

	public Mensagem criarMensagem(List<Irrigante> dest, String assunto,
			String texto, String anexo, TipoMensagem tipo) {
		if (irriganteBusca.getArea() == null) {
			Area area = new Area();
			area.setDistrito(super.getDistritosPesquisa().get(0));
			irriganteBusca.setArea(area);
		}

		Mensagem msg = new Mensagem(tipo, assunto, texto, anexo, new Date(),
				getUsuarioLogado(), dest, irriganteBusca.getArea()
						.getDistrito());
		return msg;
	}

	public void enviarEmail() {
		if (!msgEmail.equals("") && !assuntoEmail.equals("")) {
			List<Irrigante> dest = new ArrayList<Irrigante>();
			Map<String, InputStream> anexos = null;
			if (uploadAnexo != null) {
				UploadedFile arq = uploadAnexo;
				InputStream in = null;
				try {
					in = new BufferedInputStream(arq.getInputstream());
				} catch (IOException e) {
					e.printStackTrace();
				}
				anexos = new HashMap<String, InputStream>();
				anexos.put(arq.getFileName(), in);
			}

			List<String> destinatarios = new ArrayList<String>();
			List<Irrigante> irrigantes = recuperarIrrigantes();

			for (Irrigante i : irrigantes) {
				if (i.getUsuario().getEmail() != null
						&& !i.getUsuario().getEmail().isEmpty()
						&& !destinatarios.contains(i.getUsuario().getEmail())) {
					destinatarios.add(i.getUsuario().getEmail());
					dest.add(i);
				}
			}
			Mensagem msg = criarMensagem(dest, assuntoEmail, msgEmail,
					uploadAnexo == null ? null : uploadAnexo.getFileName(),
					TipoMensagem.Email);
			emailService.enviarEmail(destinatarios, assuntoEmail, msgEmail,
					anexos);
			mensagemService.salvar(msg);
			msgEmail = null;
			assuntoEmail = null;
			infoMsg("Email enviados com sucesso.", MSG_TITULO);
		} else {
			if (msgEmail.equals("")) {
				errorMsg("Texto do email deve ser informado.", MSG_TITULO);
			}
			if (assuntoEmail.equals("")) {
				errorMsg("Assunto do email deve ser informado.", MSG_TITULO);
			}
		}
	}

	public String cadastro() {
		return "cadastrarIrrigante?faces-redirect=true";
	}

	public String fazenda() {
		return "consultarFazenda?faces-redirect=true";
	}

	public String plantio() {
		return "consultarPlantio?faces-redirect=true";
	}

	public String energia() {
		return "consultarEnergia?faces-redirect=true";
	}

	public String agua() {
		return "consultarAgua?faces-redirect=true";
	}

	public String setor() {
		return "consultarSetor?faces-redirect=true";
	}

	public String sistema() {
		return "cadastrarSistema?faces-redirect=true";
	}

	public String bomba() {
		return "consultarBomba?faces-redirect=true";
	}

	public String cultivo() {
		return "consultarCultivo?faces-redirect=true";
	}

	private void preparaBuscaIrrigante() {
		if (piloto != null && piloto) {
			irriganteBusca.setPiloto(true);
		} else {
			irriganteBusca.setPiloto(null);
		}
	}

	public LazyDataModel<Irrigante> getIrrigantes() {
		return irrigantes;
	}

	public void setIrrigantes(LazyDataModel<Irrigante> irrigantes) {
		this.irrigantes = irrigantes;
	}

	public Irrigante getIrriganteBusca() {
		return irriganteBusca;
	}

	public void setIrriganteBusca(Irrigante irriganteBusca) {
		this.irriganteBusca = irriganteBusca;
	}
	
	public List<Area> getAreas() {
		return areas;
	}

	public void setAreas(List<Area> areas) {
		this.areas = areas;
	}

	public TipoIrrigante[] getTipoIrrigante() {
		return TipoIrrigante.values();
	}

	public Irrigante getIrriganteSelecionado() {
		return irriganteSelecionado;
	}

	public void setIrriganteSelecionado(Irrigante irriganteSelecionado) {
		this.irriganteSelecionado = irriganteSelecionado;
	}

	public Boolean getModoSatelite() {
		return modoSatelite;
	}

	public void setModoSatelite(Boolean modoSatelite) {
		this.modoSatelite = modoSatelite;
	}

	public MapModel getIrriganteMarcado() {
		return irriganteMarcado;
	}

	public void setIrriganteMarcado(MapModel irriganteMarcado) {
		this.irriganteMarcado = irriganteMarcado;
	}

	public Marker getMarker() {
		return marker;
	}

	public void setMarker(Marker marker) {
		this.marker = marker;
	}

	public String getCentral() {
		return central;
	}

	public void setCentral(String central) {
		this.central = central;
	}

	public String getMsgEmail() {
		return msgEmail;
	}

	public void setMsgEmail(String msgEmail) {
		this.msgEmail = msgEmail;
	}

	public String getMsgSMS() {
		return msgSMS;
	}

	public void setMsgSMS(String msgSMS) {
		this.msgSMS = msgSMS;
	}

	public String getAssuntoEmail() {
		return assuntoEmail;
	}

	public void setAssuntoEmail(String assuntoEmail) {
		this.assuntoEmail = assuntoEmail;
	}

	public UploadedFile getUploadAnexo() {
		return uploadAnexo;
	}

	public List<Cultura> getCulturas() {
		return culturas;
	}

	public void setCulturas(List<Cultura> culturas) {
		this.culturas = culturas;
	}

	public List<Variedade> getVariedades() {
		return variedades;
	}

	public void setVariedades(List<Variedade> variedades) {
		this.variedades = variedades;
	}

	public void setUploadAnexo(UploadedFile uploadAnexo) {
		this.uploadAnexo = uploadAnexo;
	}

	public Boolean getPiloto() {
		return piloto;
	}

	public void setPiloto(Boolean piloto) {
		this.piloto = piloto;
	}

	public String getLocalizacao() {
		return localizacao;
	}

	public void setLocalizacao(String localizacao) {
		this.localizacao = localizacao;
	}

	public StreamedContent getFile() {
		return file;
	}

	public void setFile(StreamedContent file) {
		this.file = file;
	}

	public Integer getZoomMapa() {
		return zoomMapa;
	}

	public void setZoomMapa(Integer zoomMapa) {
		this.zoomMapa = zoomMapa;
	}

	public MapModel getModel() {
		return model;
	}

	public void setModel(MapModel model) {
		this.model = model;
	}	
}
