package com.agrosolutions.sai.view;

import java.io.Serializable;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.Fabricante;
import com.agrosolutions.sai.model.TipoFabricante;
import com.agrosolutions.sai.service.FabricanteService;
import com.agrosolutions.sai.util.ResourceBundle;

/**
* Use case : Cadastrar Fabricante
* 
* @since   : Jan 27, 2012, 03:23:20 AM
* @author  : raphael.ferreira@ivia.com.br
*/

@Component("cadastrarFabricanteBean")
@Scope("session")
public class CadastrarFabricanteBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = -5744191357952648418L;

	public static Logger logger = Logger.getLogger(CadastrarFabricanteBean.class);

	public static final String MSG_TITULO = "Cadastro de Fabricante";

	@Autowired
	private FabricanteService fabricanteService;
	
	private Fabricante fabricante;
	private Boolean cadastroBomba;
	private Boolean cadastroEmissor;
	private Boolean cadastroFiltro;
	private Boolean cadastroValvula;
	private Boolean cadastroTubo;
	private String operacao;
	
	public void carregarDadosIniciais() {
		operacao = getOperacao();
		fabricante = getFabricante();
		cadastroBomba = getCadastroBomba();
		cadastroEmissor = getCadastroEmissor();
		
		if (operacao.equals("NOVO") && fabricante == null) {
			fabricante = new Fabricante();
			if (cadastroBomba != null && cadastroBomba) {
				fabricante.setTipoFabricante(TipoFabricante.Bomba);
				}
			if (cadastroEmissor != null && cadastroEmissor) {
				fabricante.setTipoFabricante(TipoFabricante.Emissor);
			}
			if (cadastroFiltro != null && cadastroFiltro) {
				fabricante.setTipoFabricante(TipoFabricante.Filtro);
			}
			if (cadastroValvula != null && cadastroValvula) {
				fabricante.setTipoFabricante(TipoFabricante.Valvula);
			}
			if (cadastroTubo != null && cadastroTubo) {
				fabricante.setTipoFabricante(TipoFabricante.Tubo);
			}
		}
	}
	
	public String gravar() {
		String volta = null;
		fabricante = fabricanteService.salvar(fabricante); 
		if (operacao.equals("NOVO")) {
			fabricante = null;
			infoMsg(ResourceBundle.getMessage("fabricanteSalvar")+ ".", MSG_TITULO);
		}else{
			infoMsg(ResourceBundle.getMessage("fabricanteAlterar")+ ".", MSG_TITULO);
		}

		if (cadastroBomba != null && cadastroBomba) {
			cadastroBomba = false;
			volta = "cadastrarBomba?faces-redirect=true";
		}
		if (cadastroEmissor != null && cadastroEmissor) {
			cadastroEmissor = false;
			volta = "cadastrarEmissor?faces-redirect=true";
		}
		
		if (cadastroFiltro != null && cadastroFiltro) {
			cadastroFiltro = false;
			volta = "cadastrarFiltro?faces-redirect=true";
		}
		if (cadastroValvula != null && cadastroValvula) {
			cadastroValvula = false;
			volta = "cadastrarValvula?faces-redirect=true";
		}
		if (cadastroTubo != null && cadastroTubo) {
			cadastroTubo = false;
			volta = "cadastrarTubo?faces-redirect=true";
		}
		return volta;
    }

	public String voltar() {
		String volta;
		volta = "consultarFabricante?faces-redirect=true";

		if (cadastroBomba != null && cadastroBomba) {
			cadastroBomba = false;
			volta = "cadastrarBomba?faces-redirect=true";
		}
		if (cadastroEmissor != null && cadastroEmissor) {
			cadastroEmissor = false;
			volta = "cadastrarEmissor?faces-redirect=true";
		}
		
		if (cadastroFiltro != null && cadastroFiltro) {
			cadastroFiltro = false;
			volta = "cadastrarFiltro?faces-redirect=true";
		}
		if (cadastroValvula != null && cadastroValvula) {
			cadastroValvula = false;
			volta = "cadastrarValvula?faces-redirect=true";
		}
		if (cadastroTubo != null && cadastroTubo) {
			cadastroTubo = false;
			volta = "cadastrarTubo?faces-redirect=true";
		}
		
		return volta;
	}

	public Fabricante getFabricante() {
		if (fabricante == null) {
			fabricante = (Fabricante) getFlashScope().get("fabricante");
		}
		return fabricante;
	}

	public void setFabricante(Fabricante fabricante) {
		this.fabricante = fabricante;
		getFlashScope().put("fabricante", fabricante);
	}

	public TipoFabricante[] getTipos() {
		return TipoFabricante.values();
	}

	public Boolean getCadastroBomba() {
		if (cadastroBomba == null) {
			cadastroBomba = (Boolean) getFlashScope().get("cadastroBomba");
		}
		return cadastroBomba;
	}

	public void setCadastroBomba(Boolean cadastroBomba) {
		this.cadastroBomba = cadastroBomba;
		getFlashScope().put("cadastroBomba", cadastroBomba);
	}

	public Boolean getCadastroEmissor() {
		if (cadastroEmissor == null) {
			cadastroEmissor = (Boolean) getFlashScope().get("cadastroEmissor");
		}
		return cadastroEmissor;
	}

	public void setCadastroEmissor(Boolean cadastroEmissor) {
		this.cadastroEmissor = cadastroEmissor;
		getFlashScope().put("cadastroEmissor", cadastroEmissor);
	}
	
	public String getOperacao() {
		if (operacao == null) {
			operacao = (String) getFlashScope().get("operacao");
		}
		return operacao;
	}

	public void setOperacao(String operacao) {
		this.operacao = operacao;
		getFlashScope().put("operacao", operacao);
	}

	public Boolean getCadastroFiltro() {
		return cadastroFiltro;
	}

	public void setCadastroFiltro(Boolean cadastroFiltro) {
		this.cadastroFiltro = cadastroFiltro;
	}

	public Boolean getCadastroValvula() {
		return cadastroValvula;
	}

	public void setCadastroValvula(Boolean cadastroValvula) {
		this.cadastroValvula = cadastroValvula;
	}

	public Boolean getCadastroTubo() {
		return cadastroTubo;
	}

	public void setCadastroTubo(Boolean cadastroTubo) {
		this.cadastroTubo = cadastroTubo;
	}

}
