package com.agrosolutions.sai.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.Fabricante;
import com.agrosolutions.sai.model.TipoFabricante;
import com.agrosolutions.sai.model.TipoValvula;
import com.agrosolutions.sai.model.Valvula;
import com.agrosolutions.sai.service.FabricanteService;
import com.agrosolutions.sai.service.ValvulaService;
import com.agrosolutions.sai.util.ResourceBundle;

/**
* Use case : Consultar Valvula
* 
* @since   : Maio 14, 2012, 01:49:20 AM
* @author  : walljr@gmail.com
*/

@Component("consultarValvulaBean")
@Scope("view")
public class ConsultarValvulaBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = -1849275221652412065L;

	public static Logger logger = Logger.getLogger(ConsultarValvulaBean.class);

	public static final String MSG_TITULO = "Consultar Valvulas";

	@Autowired
	private ValvulaService valvulaService;
	@Autowired
	private FabricanteService fabricanteService;
	
	private LazyDataModel<Valvula> valvulas;
	private Valvula valvulaSelecionada;
	private Valvula valvulaBusca;
	private List<Fabricante> fabricantes;

	public void carregarDadosIniciais() {
		if (valvulaBusca == null){
			valvulaBusca = new Valvula();
			valvulaBusca.setSortField("modelo");
			valvulaBusca.setSortOrder(SortOrder.ASCENDING);
			this.setFabricantes(fabricanteService.pesquisar(TipoFabricante.Valvula));
			consultar();
		}
	}

	public void consultar(){
		valvulas = new LazyDataModel<Valvula>() {

			private static final long serialVersionUID = 1581476336358042722L;

			@Override
			public List<Valvula> load(int first, int pageSize, String sortField,
					SortOrder sortOrder, Map<String, String> filters) {
				List<Valvula> valvulas = new ArrayList<Valvula>();

				if (sortField == null) {
					sortField = "modelo";
					sortOrder = SortOrder.ASCENDING;
				}

				valvulaBusca.setSortField(sortField);
				valvulaBusca.setSortOrder(sortOrder);
				valvulas = valvulaService.pesquisar(valvulaBusca, first, pageSize);

				return valvulas;
			}
		};
		int quantidade = valvulaService.quantidade(valvulaBusca);
		valvulas.setRowCount(quantidade == 0 ? 1 : quantidade);
	}

	public void excluir(){
		valvulaService.excluir(valvulaSelecionada);
		consultar();
		infoMsg(ResourceBundle.getMessage("valvulaExcluir")+ ".", null);
   }

   public String cadastro() {
		return "cadastrarValvula?faces-redirect=true";
	}
	
	public Valvula getValvulaSelecionada() {
		return valvulaSelecionada;
	}
	
	public void setValvulaSelecionada(Valvula valvulaSelecionada) {
		this.valvulaSelecionada = valvulaSelecionada;
	}

	public void setValvulas(LazyDataModel<Valvula> valvulas) {
		this.valvulas = valvulas;
	}

	public LazyDataModel<Valvula> getValvulas() {
		return valvulas;
	}

	public void setValvulaBusca(Valvula valvulaBusca) {
		this.valvulaBusca = valvulaBusca;
	}

	public Valvula getValvulaBusca() {
		return valvulaBusca;
	}

	public void setFabricantes(List<Fabricante> fabricantes) {
		this.fabricantes = fabricantes;
	}

	public List<Fabricante> getFabricantes() {
		return fabricantes;
	}
	
	public TipoValvula[] getTipos() {
		return TipoValvula.values();
	}
}
