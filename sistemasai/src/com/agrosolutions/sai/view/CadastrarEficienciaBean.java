package com.agrosolutions.sai.view;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.faces.event.ActionEvent;

import org.apache.log4j.Logger;
import org.primefaces.component.datagrid.DataGrid;
import org.primefaces.event.ItemSelectEvent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.chart.CartesianChartModel;
import org.primefaces.model.chart.LineChartSeries;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.exception.SaiException;
import com.agrosolutions.sai.model.Setor;
import com.agrosolutions.sai.model.TesteEficiencia;
import com.agrosolutions.sai.service.TesteEficienciaService;
import com.agrosolutions.sai.util.RelatorioUtil;
import com.agrosolutions.sai.util.ResourceBundle;

/**
* Use case : Cadastrar Efici�ncia
* 
* @since   : Mar 25, 2012, 03:45:20 AM
* @author  : rpf1404@gmail.com
*/

@Component("cadastrarEficienciaBean")
@Scope("session")
public class CadastrarEficienciaBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = 808360499843110937L;

	public static Logger logger = Logger.getLogger(CadastrarEficienciaBean.class);

	@Autowired
	private TesteEficienciaService testeEficienciaService;
	@Autowired
	private RelatorioUtil relatorioUtil;

	private List<TesteEficiencia> testes;
	private TesteEficiencia testeSelecionado;
	private Setor setor;
	private String operacao;
	private StreamedContent file;
	private CartesianChartModel grafico;
	private DataGrid grid;
	
	public DataGrid getGrid() {
		return grid;
	}
	public void setGrid(DataGrid grid) {
		this.grid = grid;
	}
	public void carregarDadosIniciais() {
		if (testes == null && setor != null) {
			testes = testeEficienciaService.pesquisarPorSetor(setor);
		}
		if(setor!=null){
			if (testes == null) {
				testes = new ArrayList<TesteEficiencia>();
			}
			grafico = new CartesianChartModel();
			carregarGrafico();
		}
		
	}
	public void itemGrafico(ItemSelectEvent event) {  
		infoMsg(grafico.getSeries().get(event.getSeriesIndex()).getLabel() + "=" + grafico.getSeries().get(event.getSeriesIndex()).getData().toString(), null);
    }
	
	private void carregarGrafico() {
	    LineChartSeries efic = new LineChartSeries();

		for (TesteEficiencia t : testes) {
			Calendar data = new GregorianCalendar();
			if (t.getData() !=null) {
				data.setTime(t.getData());
				efic.set(data.get(Calendar.DAY_OF_MONTH) + "/" + data.getDisplayName(Calendar.MONTH, 0, Locale.getDefault()), t.getEficiencia()==null?new BigDecimal(0):t.getEficiencia().setScale(2, BigDecimal.ROUND_UP));
			}
		}
		if (!testes.isEmpty()) {
			grafico.addSeries(efic);
		}else{
			Calendar data = new GregorianCalendar();
			efic.set(data.get(Calendar.DAY_OF_MONTH) + "/" + data.getDisplayName(Calendar.MONTH, 0, Locale.getDefault()), new BigDecimal(0));
			grafico.addSeries(efic);
		}
	}

	@SuppressWarnings("unchecked")
	public void gravar(ActionEvent actionEvent) {
		testeSelecionado = ((List<TesteEficiencia>) grid.getValue()).get(grid.getPage());
		testeEficienciaService.salvar(testeSelecionado);
		operacao = "VER";
		testes = null;
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("id", testeSelecionado.getId());

		try {
			file = relatorioUtil.relatorio("rptParecerTesteEficiencia.jasper", parametros, "testeEficiencia.pdf");
			infoMsg(ResourceBundle.getMessage("eficienciaSalvar")+ ".", null);
		} catch (SaiException e) {
			errorMsg(ResourceBundle.getMessage("ocorreuErro")+ ":" + e.getMessage(), null);
			logger.warn("Ocorreu o seguinte erro: " + e.getMessage());
		} catch (Exception e) {
			errorMsg(ResourceBundle.getMessage("ocorreuErro")+ ":" + e.getMessage(), null);
			logger.fatal("Ocorreu o seguinte erro: " + e.getMessage());
		}
	}

	@SuppressWarnings("unchecked")
	public void excluir() {
		testeSelecionado = ((List<TesteEficiencia>) grid.getValue()).get(grid.getPage());
		testeEficienciaService.excluir(testeSelecionado);
		testes = null;
		infoMsg(ResourceBundle.getMessage("eficienciaExcluir")+ ".", null);
		
    }

	public void adicionarTeste(ActionEvent actionEvent) {
		//Cria novo teste
		TesteEficiencia teste = new TesteEficiencia(setor, getUsuarioLogado());
		teste.setData(new Date());
		testes.add(0,teste);
		
		operacao = "NOVO";
    }

	public void alterarTeste(ActionEvent actionEvent) {
		//Permitir altera��o nos testes
		operacao = "ALTERAR";
    }

	@SuppressWarnings("unchecked")
	public String voltar() {
		if (operacao.equals("NOVO")) {
			testeSelecionado = ((List<TesteEficiencia>) grid.getValue()).get(grid.getPage());
			testes.remove(testeSelecionado);
			operacao = "VER";
			return null;
		}
		if (operacao.equals("ALTERAR")) {
			operacao = "VER";
			return null;
		}
		return "consultarFazenda?faces-redirect=true";
	}
	public List<TesteEficiencia> getTestes() {
		return testes;
	}
	public void setTestes(List<TesteEficiencia> testes) {
		this.testes = testes;
	}
	public TesteEficiencia getTesteSelecionado() {
		return testeSelecionado;
	}
	public void setTesteSelecionado(TesteEficiencia testeSelecionado) {
		this.testeSelecionado = testeSelecionado;
	}
	public Setor getSetor() {
		return setor;
	}
	public void setSetor(Setor setor) {
		this.setor = setor;
		
	}
	public String getOperacao() {
		return operacao;
	}

	public void setOperacao(String operacao) {
		this.operacao = operacao;
	}
	public CartesianChartModel getGrafico() {
		return grafico;
	}
	public void setGrafico(CartesianChartModel grafico) {
		this.grafico = grafico;
	}
	public StreamedContent getFile() {
		return file;
	}
	public void setFile(StreamedContent file) {
		this.file = file;
	}
	
}
