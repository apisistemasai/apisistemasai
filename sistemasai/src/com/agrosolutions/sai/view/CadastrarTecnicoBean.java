package com.agrosolutions.sai.view;

import java.io.Serializable;
import java.util.List;

import javax.faces.event.ActionEvent;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.Distrito;
import com.agrosolutions.sai.model.Escolaridade;
import com.agrosolutions.sai.model.OperadoraCelular;
import com.agrosolutions.sai.model.Perfil;
import com.agrosolutions.sai.model.Tecnico;
import com.agrosolutions.sai.model.TipoPerfil;
import com.agrosolutions.sai.model.Usuario;
import com.agrosolutions.sai.service.PerfilService;
import com.agrosolutions.sai.service.TecnicoService;
import com.agrosolutions.sai.util.ResourceBundle;

/**
* Use case : Cadastrar Tecnico
* 
* @since   : Jan 11, 2012, 03:23:20 AM
* @author  : raphael.ferreira@ivia.com.br
*/	

@Component("cadastrarTecnicoBean")
@Scope("session")
public class CadastrarTecnicoBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = -3456232235861349235L;

	public static Logger logger = Logger.getLogger(CadastrarTecnicoBean.class);

	public static final String MSG_TITULO = "Cadastro de Tecnico";

	@Autowired
	private TecnicoService tecnicoService;
	@Autowired
	private PerfilService perfilService;
	
	private Tecnico tecnico;
	private List<Distrito> distritos;
	private boolean multiDistrito;
	private List<Perfil> perfis;
	private String operacao;
	
	public void carregarDadosIniciais() {
		operacao = getOperacao();
		tecnico = getTecnico();
		
		if (operacao.equals("NOVO") && tecnico == null) {
			tecnico = new Tecnico();
			tecnico.setUsuario(new Usuario());
			tecnico.getUsuario().setAtivo(true);
			tecnico.getUsuario().setDistritos(super.getDistritosPesquisa());
		}
		if ((operacao.equals("ALTERAR") || operacao.equals("VER")) && tecnico != null) {
			if(tecnico.getUsuario().getConfirmarEmail() == null) {
				tecnico.getUsuario().setConfirmarEmail(tecnico.getUsuario().getEmail());
				tecnico.getUsuario().setConfirmarSenha(tecnico.getUsuario().getPassword());
			}
		}
		if (operacao.equals("ALTERAR") || operacao.equals("NOVO")){		
			distritos = super.getDistritosPesquisa();
			perfis = perfilService.porTipo(TipoPerfil.Tecnico);
		}
	}
	
	public void gravar(ActionEvent actionEvent) {
		if (operacao.equals("NOVO")) {
			tecnico = tecnicoService.salvar(tecnico);
			tecnico = null;
			infoMsg(ResourceBundle.getMessage("tecnicoSalvar")+ ".", MSG_TITULO);
		}else{
			tecnico = tecnicoService.editar(tecnico);
			infoMsg(ResourceBundle.getMessage("tecnicoAlterar")+ ".", MSG_TITULO);
		}	
    }

	public String voltar() {
		return "consultarTecnico?faces-redirect=true";
	}
	
	public Escolaridade[] getEscolaridades() {
		return Escolaridade.values();
	}

	public OperadoraCelular[] getOperadoras() {
		return OperadoraCelular.values();
	}

	public Tecnico getTecnico() {
		if (tecnico == null) {
			tecnico = (Tecnico) getFlashScope().get("tecnico");
		}
		return tecnico;
	}

	public void setTecnico(Tecnico tecnico) {
		this.tecnico = tecnico;
		getFlashScope().put("tecnico", tecnico);
	}

	public List<Distrito> getDistritos() {
		return distritos;
	}

	public void setDistritos(List<Distrito> regioes) {
		this.distritos = regioes;
	}

	public boolean isMultiDistrito() {
		return multiDistrito;
	}

	public void setMultiDistrito(boolean multiDistrito) {
		this.multiDistrito = multiDistrito;
	}

	public List<Perfil> getPerfis() {
		return perfis;
	}

	public void setPerfis(List<Perfil> perfis) {
		this.perfis = perfis;
	}

	public String getOperacao() {
		if (operacao == null) {
			operacao = (String) getFlashScope().get("operacao");
		}
		return operacao;
	}

	public void setOperacao(String operacao) {
		this.operacao = operacao;
		getFlashScope().put("operacao", operacao);
	}
}
