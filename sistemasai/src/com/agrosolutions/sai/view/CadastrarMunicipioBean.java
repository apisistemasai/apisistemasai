package com.agrosolutions.sai.view;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.event.ActionEvent;

import org.apache.log4j.Logger;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.BaciaHidrografica;
import com.agrosolutions.sai.model.Coordenada;
import com.agrosolutions.sai.model.Municipio;
import com.agrosolutions.sai.service.CoordenadaService;
import com.agrosolutions.sai.service.MunicipioService;
import com.agrosolutions.sai.util.KmlUtil;
import com.agrosolutions.sai.util.ResourceBundle;

/**
* Use case : Cadastrar Municipio
* 
* @since   : Jan 21, 2016, 20:23:20 PM
* @author  : raphael@iasoft.me
*/

@Component("cadastrarMunicipioBean")
@Scope("session")
public class CadastrarMunicipioBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = -8816125708463930745L;

	public static Logger logger = Logger.getLogger(CadastrarMunicipioBean.class);

	@Autowired
	private MunicipioService municipioService;
	@Autowired
	private CoordenadaService coordenadaService;
	
	private Municipio municipio;
	private List<BaciaHidrografica> bacias;
	private BaciaHidrografica bacia;
	private String operacao;
	
	public void carregarDadosIniciais() {
		//Se for um novo e ainda tiver nulo criar objeto.
		if (getOperacao().equals("NOVO") && getMunicipio() == null) {
			setMunicipio(new Municipio());
			getMunicipio().setDataCadastro(new Date());
		}
		if ((getOperacao().equals("ALTERAR") || getOperacao().equals("VER")) && getMunicipio() != null) {
			if(getMunicipio().getBacia()!= null){
				setBacia(getMunicipio().getBacia());
			}
		}
		
		//	Reseta a listagem dos distritos quando mudar o distrito.
		if (getBacias() == null){
			setBacias(getUsuarioLogado().getBacias());
		}
	}
	

    public void uploadCoordenadas(FileUploadEvent event) {
		try{
			UploadedFile arq = event.getFile();
			InputStream in = new BufferedInputStream(arq.getInputstream());
			
			KmlUtil kmlUtil = new KmlUtil();
			List<Coordenada> coordenadas = new ArrayList<Coordenada>();
			coordenadas = kmlUtil.getCoordenadas(in);
			coordenadaService.salvar(coordenadas);
			getMunicipio().setCoordenadasPoligono(coordenadas);
			municipioService.salvar(getMunicipio());
			
    		infoMsg(ResourceBundle.getMessage("sucessoImportarCoordenadas"), null);
		} catch(Exception e){
			e.getStackTrace();
			errorMsg("erroImportarCoordenadas", null);
		}
    }
	
    public void uploadFoto(FileUploadEvent event) {
    	final String caminho = System.getProperty("imagem_sai");

		try{
	    	UploadedFile arq = event.getFile();
			InputStream in = new BufferedInputStream(arq.getInputstream());
			File file = new File(caminho + File.separator + arq.getFileName());
			FileOutputStream fout = new FileOutputStream(file);
			 
			while(in.available() != 0){
				fout.write(in.read());
			}
			fout.close();
    		getMunicipio().setFoto(file.getName());
    		infoMsg("Foto foi carregada.", null);
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
    }

	public void gravar(ActionEvent actionEvent) {
		setMunicipio(municipioService.salvar(getMunicipio())); 
		if (getOperacao().equals("NOVO")) {
			setMunicipio(null);
			infoMsg(ResourceBundle.getMessage("municipioSalvar")+ ".", null);
		}else{
			infoMsg(ResourceBundle.getMessage("municipioAlterar")+ ".", null);
		}
    }

	public String voltar() {
		return "consultarMunicipio?faces-redirect=true";
	}


	public BaciaHidrografica getBacia() {
		return bacia;
	}


	public void setBacia(BaciaHidrografica bacia) {
		this.bacia = bacia;
	}


	public Municipio getMunicipio() {
		return municipio;
	}


	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}


	public List<BaciaHidrografica> getBacias() {
		return bacias;
	}


	public void setBacias(List<BaciaHidrografica> bacias) {
		this.bacias = bacias;
	}


	public String getOperacao() {
		return operacao;
	}


	public void setOperacao(String operacao) {
		this.operacao = operacao;
	}
	
}
