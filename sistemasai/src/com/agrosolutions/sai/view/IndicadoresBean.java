package com.agrosolutions.sai.view;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.primefaces.event.TabChangeEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.chart.CartesianChartModel;
import org.primefaces.model.chart.ChartSeries;
import org.primefaces.model.chart.PieChartModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.BaciaHidrografica;
import com.agrosolutions.sai.model.GraficoBarra;
import com.agrosolutions.sai.model.GraficoPizzaIndicadores;
import com.agrosolutions.sai.model.Municipio;
import com.agrosolutions.sai.model.Parametro;
import com.agrosolutions.sai.service.IndicadoresService;
import com.agrosolutions.sai.service.MunicipioService;
import com.agrosolutions.sai.service.ParametroService;
import com.agrosolutions.sai.service.PlantioService;
import com.agrosolutions.sai.util.BigDecimalUtil;
import com.agrosolutions.sai.util.ResourceBundle;

@Component("indicadoresBean")
@Scope("view")
public class IndicadoresBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = -6107206677236418037L;

	public static Logger logger = Logger.getLogger(IndicadoresBean.class);

	@Autowired
	private ParametroService parametroService;
	@Autowired
	private MunicipioService MunicipioService;
	@Autowired
	private PlantioService plantioService;
	@Autowired
	private IndicadoresService indicadoresService;
//	private Boolean filtrarPorMunicipio = true;
	private LazyDataModel<Municipio> municipios;
	private Long qtd = 0L;
	private PieChartModel graficoIndicadoresCulturasBacia;
	private PieChartModel graficoIndicadorCicloCultura;
	private CartesianChartModel graficoIndicadoresSegProdutivaKgHaBacia;
	private CartesianChartModel graficoIndicadoresSegProdutivaKgM3Bacia;
	private CartesianChartModel graficoIndicadoresSegEconomicaReceitaHaBacia;
	private CartesianChartModel graficoIndicadoresSegEconomicaReceitaM3Bacia;
	private CartesianChartModel graficoIndicadoresSegSocialEmpregosHaBacia;
	private CartesianChartModel graficoIndicadoresSegSocialEmpregosM3Bacia;
	private CartesianChartModel graficoIndicadoresSegHidricaM3HaBacia;
	private CartesianChartModel graficoIndicadoresSegHidricaLitrosSegundoHaBacia;
	private CartesianChartModel graficoIndicadoresCorteHidricoBacia;
	private CartesianChartModel graficoIndicadoresSegEconomicaVBPBacia;
	private CartesianChartModel graficoIndicadoresSegEconomicaReceitaBacia;
	private BigDecimal areaIrrigadaBacia = new BigDecimal(0);

	private Double graficoIndicadoresSegProdutivaKgHaBaciaMax;
	private Double graficoIndicadoresSegProdutivaKgM3BaciaMax;
	private Double graficoIndicadoresSegEconomicaReceitaHaBaciaMax;
	private Double graficoIndicadoresSegEconomicaReceitaM3BaciaMax;
	private Double graficoIndicadoresSegEconomicaReceitaBaciaMax;
	private Double graficoIndicadoresSegSocialEmpregosHaBaciaMax;
	private Double graficoIndicadoresSegSocialEmpregosM3BaciaMax;
	private Double graficoIndicadoresSegHidricaM3HaBaciaMax;
	private Integer percentualCorte;
	private List<BaciaHidrografica> baciasPesquisa = new ArrayList<BaciaHidrografica>();
		
	public void carregarDadosIniciais() {
		if (percentualCorte == null){
			Parametro pesquisarPorCodigo = parametroService.pesquisarPorCodigo("percentualCorte");
			percentualCorte = Integer.valueOf(pesquisarPorCodigo.getValor());
		}
		
//		if ( filtrarPorMunicipio ) {
//			if ( municipios == null ) {
//				municipios = new LazyDataModel<Municipio>() {
//					private static final long serialVersionUID = -7291827228017454370L;
//					
//					@Override
//					public List<Municipio> load(int first, int pageSize,
//							String sortField, SortOrder sortOrder,
//							Map<String, String> filters) {
//						
//						List<Municipio> Municipios = new ArrayList<Municipio>();
//						
////						if (sortField == null) {
////							sortField = "nome";
////							sortOrder = SortOrder.ASCENDING;
////						}
//						
//						Municipios = MunicipioService.pesquisarPorBacia(getBaciasPesquisa(), first, pageSize);
//						// Cria/Popula o gr�fico
//						for (Municipio Municipio : Municipios) {
//							Municipio.setGraficoIndicadorAreaIrrigada( criarGraficoAreaIrrigada(Municipio) );
//							
//							Municipio.setGraficoIndicadoresSegurancaKgHa( criarGraficoIndicadoresSegurancaKgHa(Municipio) );
//							Municipio.setGraficoIndicadoresSegurancaKgM3( criarGraficoIndicadoresSegurancaKgM3(Municipio) );
//							
//							Municipio.setGraficoIndicadoresEconomicoReceitaLiquidaHa( criarGraficoIndicadoresEconomicoReceitaLiquidaHa(Municipio) );
//							Municipio.setGraficoIndicadoresEconomicoReceitaLiquidaM3( criarGraficoIndicadoresEconomicoReceitaLiquidaM3(Municipio) );
//							Municipio.setGraficoIndicadoresEconomicoReceitaLiquida( criarGraficoIndicadoresEconomicoReceitaLiquida(Municipio) );
//							Municipio.setGraficoIndicadoresVBP( criarGraficoIndicadoresEconomicoVBP(Municipio) );
//							
//							Municipio.setGraficoIndicadoresSegSocialEmpregosHa( criarGraficoIndicadoresSegSocialEmpregosHa(Municipio) );
//							Municipio.setGraficoIndicadoresSegSocialEmpregosM3( criarGraficoIndicadoresSegSocialEmpregosM3(Municipio) );
//							
//							Municipio.setGraficoIndicadoresSegHidricaM3Ha( criarGraficoIndicadoresSegHidricaM3Ha(Municipio) );
//							//Municipio.setGraficoIndicadoresSegHidricaLitrosSegundoHa( criarGraficoIndicadoresSegHidricaLitrosSegundoHa(Municipio) );
//							Municipio.setGraficoIndicadorCicloCultura( criarGraficoCicloCultura(Municipio) );
//							
//							Municipio.setGraficoIndicadoresCorteHidrico( criarGraficoIndicadoresCorteHidrico(Municipio) );
//						}
//						
//						return Municipios;
//					}
//
//				};
//				
//				if ( qtd == 0L) {
//					qtd = indicadoresService.totalMunicipiosPorBacia(getBaciasPesquisa());
//					municipios.setRowCount(qtd.intValue());
//				}
//			}
//		} else {
			if ( graficoIndicadoresCulturasBacia == null ) {				
				graficoIndicadoresCulturasBacia = criarGraficoCulturaBacia( getBaciasPesquisa().get(0) );

				graficoIndicadoresSegProdutivaKgHaBacia = criarGraficoIndicadoresSegurancaProdutivaKgHaBacia( getBaciasPesquisa().get(0) );
				graficoIndicadoresSegProdutivaKgM3Bacia = criarGraficoIndicadoresSegurancaProdutivaKgM3Bacia( getBaciasPesquisa().get(0) );
				
				graficoIndicadoresSegEconomicaReceitaBacia = criarGraficoIndicadoresSegurancaEconomicaReceitaBacia( getBaciasPesquisa().get(0) ); 
				graficoIndicadoresSegEconomicaReceitaHaBacia = criarGraficoIndicadoresSegurancaEconomicaReceitaHaBacia( getBaciasPesquisa().get(0) ); 
				graficoIndicadoresSegEconomicaReceitaM3Bacia = criarGraficoIndicadoresSegurancaEconomicaReceitaM3Bacia( getBaciasPesquisa().get(0) );
				graficoIndicadoresSegEconomicaVBPBacia = criarGraficoIndicadoresSegurancaEconomicaVBPBacia( getBaciasPesquisa().get(0) );
				
				graficoIndicadoresSegSocialEmpregosHaBacia = criarGraficoIndicadoresSegurancaSocialEmpregosHaBacia( getBaciasPesquisa().get(0) );
				graficoIndicadoresSegSocialEmpregosM3Bacia = criarGraficoIndicadoresSegurancaSocialEmpregosM3Bacia( getBaciasPesquisa().get(0) );
				
				graficoIndicadoresSegHidricaM3HaBacia = criarGraficoIndicadoresSegurancaHidricaM3HaBacia( getBaciasPesquisa().get(0) );
				//graficoIndicadoresSegHidricaLitrosSegundoHaBacia = criarGraficoIndicadoresSegurancaHidricaLitrosSegundoHaBacia( getBaciasPesquisa().get(0) );
				graficoIndicadorCicloCultura = criarGraficoCicloCultura( getBaciasPesquisa().get(0) );
				
				graficoIndicadoresCorteHidricoBacia = criarGraficoCorteHidricoBacia( getBaciasPesquisa().get(0) );
			}
//		}
	}
	
	public void onChange(TabChangeEvent event) {
		municipios = null;
		carregarDadosIniciais();
	}

//	private CartesianChartModel criarGraficoIndicadoresSegurancaHidricaLitrosSegundoHaBacia(BaciaHidrografica distrito) {
//		List<GraficoBarra> plantios = indicadoresService.graficoIndicadoresSegurancaHidricaLitrosSegundoHaBacia(distrito);
//		CartesianChartModel grafico = new CartesianChartModel();
//		montarGraficoBarra(grafico, plantios, ResourceBundle.getMessage("litrosSegundoHa"));
//		
//		return grafico;
//	}
	
	private CartesianChartModel criarGraficoIndicadoresSegurancaHidricaM3HaBacia(BaciaHidrografica bacia) {
		List<GraficoBarra> plantios = indicadoresService.graficoIndicadoresSegurancaHidricaM3HaBacia(bacia);
		CartesianChartModel grafico = new CartesianChartModel();
		montarGraficoBarra(grafico, plantios, ResourceBundle.getMessage("m3Ha"));
		
		graficoIndicadoresSegHidricaM3HaBaciaMax = calcularValorMaximoGrafico(plantios); 
		return grafico;
	}
	
	private CartesianChartModel criarGraficoIndicadoresSegurancaSocialEmpregosM3Bacia(BaciaHidrografica distrito) {
		List<GraficoBarra> plantios = indicadoresService.graficoIndicadoresSegurancaSocialEmpregosM3Bacia(distrito);
		CartesianChartModel grafico = new CartesianChartModel();
		montarGraficoBarra(grafico, plantios, ResourceBundle.getMessage("empregosM3"));
		
		graficoIndicadoresSegSocialEmpregosM3BaciaMax = calcularValorMaximoGrafico(plantios); 
		return grafico;
	}
	
	private CartesianChartModel criarGraficoIndicadoresSegurancaSocialEmpregosHaBacia(BaciaHidrografica distrito) {
		List<GraficoBarra> plantios = indicadoresService.graficoIndicadoresSegurancaSocialEmpregosHaBacia(distrito);
		CartesianChartModel grafico = new CartesianChartModel();
		montarGraficoBarra(grafico, plantios, ResourceBundle.getMessage("empregosHa"));
		
		graficoIndicadoresSegSocialEmpregosHaBaciaMax = calcularValorMaximoGrafico(plantios); 
		return grafico;
	}
	
	private CartesianChartModel criarGraficoIndicadoresSegurancaEconomicaReceitaM3Bacia(BaciaHidrografica distrito) {
		List<GraficoBarra> plantios = indicadoresService.graficoIndicadoresSegurancaEconomicaReceitaM3Bacia(distrito);
		CartesianChartModel grafico = new CartesianChartModel();
		montarGraficoBarra(grafico, plantios, ResourceBundle.getMessage("receitaLiquidaM3"));
		
		graficoIndicadoresSegEconomicaReceitaM3BaciaMax = calcularValorMaximoGrafico(plantios); 
		return grafico;
	}
	
	private CartesianChartModel criarGraficoIndicadoresSegurancaEconomicaVBPBacia(BaciaHidrografica distrito) {
		List<GraficoBarra> plantios = indicadoresService.graficoIndicadoresSegurancaEconomicaVBPBacia(distrito);
		CartesianChartModel grafico = new CartesianChartModel();
		montarGraficoBarra(grafico, plantios, ResourceBundle.getMessage("vbp"));
		
		return grafico;
	}

	private CartesianChartModel criarGraficoIndicadoresSegurancaEconomicaReceitaHaBacia(BaciaHidrografica distrito) {
		List<GraficoBarra> plantios = indicadoresService.graficoIndicadoresSegurancaEconomicaReceitaHaBacia(distrito);
		CartesianChartModel grafico = new CartesianChartModel();
		montarGraficoBarra(grafico, plantios, ResourceBundle.getMessage("receitaLiquidaHa"));
		
		graficoIndicadoresSegEconomicaReceitaHaBaciaMax = calcularValorMaximoGrafico(plantios); 
		return grafico;
	}

	private CartesianChartModel criarGraficoIndicadoresSegurancaEconomicaReceitaBacia(BaciaHidrografica distrito) {
		List<GraficoBarra> plantios = indicadoresService.graficoIndicadoresSegurancaEconomicaReceitaBacia(distrito);
		CartesianChartModel grafico = new CartesianChartModel();
		montarGraficoBarra(grafico, plantios, ResourceBundle.getMessage("receitaLiquida"));
		
		graficoIndicadoresSegEconomicaReceitaBaciaMax = calcularValorMaximoGrafico(plantios); 
		return grafico;
	}

	private PieChartModel criarGraficoCulturaBacia(BaciaHidrografica distrito) {
		PieChartModel grafico = new PieChartModel();
		List<GraficoPizzaIndicadores> plantios = indicadoresService.totalHaPorCulturaPorBacia(distrito);
		areaIrrigadaBacia = new BigDecimal(plantios.get(0).getTotal(), BigDecimalUtil.MC).setScale(2, RoundingMode.UP);

		montarGrafico(grafico, plantios, null);
		
		return grafico;
	}
	
	private PieChartModel criarGraficoCicloCultura(BaciaHidrografica distrito) {
		PieChartModel grafico = new PieChartModel();
		List<GraficoPizzaIndicadores> plantios = indicadoresService.totalCicloCulturaPorBacia(distrito);
		montarGrafico(grafico, plantios, null);
		
		return grafico;
	}

	private CartesianChartModel criarGraficoIndicadoresSegurancaProdutivaKgHaBacia(BaciaHidrografica distrito) {
		List<GraficoBarra> plantios = indicadoresService.graficoIndicadoresSegurancaKgHaBacia(distrito);
		CartesianChartModel grafico = new CartesianChartModel();
		montarGraficoBarra(grafico, plantios, ResourceBundle.getMessage("kgHa"));
		
		graficoIndicadoresSegProdutivaKgHaBaciaMax = calcularValorMaximoGrafico(plantios); 
		return grafico;
	}

	private Double calcularValorMaximoGrafico(List<GraficoBarra> plantios) {
		double valorMaximo = 0;
		double segundoMaximo= 0;
		for (GraficoBarra graficoBarra : plantios) {
			if (valorMaximo == 0 || graficoBarra.getValorEixoY() > valorMaximo){
				valorMaximo = graficoBarra.getValorEixoY();
			}
			if (graficoBarra.getValorEixoY() > segundoMaximo && graficoBarra.getValorEixoY() < valorMaximo){
				segundoMaximo = graficoBarra.getValorEixoY();
			} 
		}
		return segundoMaximo + ((segundoMaximo * percentualCorte)/100);
	}
	
	private CartesianChartModel criarGraficoIndicadoresSegurancaProdutivaKgM3Bacia(BaciaHidrografica distrito) {
		List<GraficoBarra> plantios = indicadoresService.graficoIndicadoresSegurancaKgM3Bacia(distrito);
		CartesianChartModel grafico = new CartesianChartModel();
		montarGraficoBarra(grafico, plantios, ResourceBundle.getMessage("kgM3"));
		
		graficoIndicadoresSegProdutivaKgM3BaciaMax = calcularValorMaximoGrafico(plantios); 
		return grafico;
	}
	
	private CartesianChartModel criarGraficoCorteHidricoBacia(BaciaHidrografica distrito) {
		List<GraficoBarra> plantios = indicadoresService.graficoCorteHidricoBacia(distrito);
		CartesianChartModel grafico = new CartesianChartModel();
		montarGraficoBarraHorizontal(grafico, plantios, ResourceBundle.getMessage("corteHidrico"));
		
		return grafico;
	}
	
	private PieChartModel criarGraficoAreaIrrigada(Municipio municipio) {
		PieChartModel grafico = new PieChartModel();
		List<GraficoPizzaIndicadores> plantios = indicadoresService.totalHaPorCulturaPorMunicipio(municipio);

		for (GraficoPizzaIndicadores graficoPizza : plantios) {
			municipio.setAreaIrrigada(municipio.getAreaIrrigada().add(new BigDecimal(graficoPizza.getQtd()).setScale(2, RoundingMode.UP)));
		}

		montarGrafico(grafico, plantios, municipio);
		
		return grafico;
	}

	private CartesianChartModel criarGraficoIndicadoresSegurancaKgHa(Municipio municipio) {
		List<GraficoBarra> plantios = indicadoresService.culturasHaPorMunicipio(municipio);
		CartesianChartModel grafico = new CartesianChartModel();
		montarGraficoBarra(grafico, plantios, ResourceBundle.getMessage("kgHa"));
		
		return grafico;
	}
	
	private CartesianChartModel criarGraficoIndicadoresSegurancaKgM3(Municipio Municipio) {
		List<GraficoBarra> plantios = indicadoresService.graficoIndicadoresSegurancaKgM3(Municipio);
		CartesianChartModel grafico = new CartesianChartModel();
	
		montarGraficoBarra(grafico, plantios, ResourceBundle.getMessage("kgM3"));
		
		return grafico;
	}
	
	private CartesianChartModel criarGraficoIndicadoresEconomicoReceitaLiquida(Municipio Municipio) {
		List<GraficoBarra> plantios = indicadoresService.graficoIndicadoresEconomicoReceitaLiquida(Municipio);
		CartesianChartModel grafico = new CartesianChartModel();
		montarGraficoBarra(grafico, plantios, ResourceBundle.getMessage("receitaLiquida"));
		
		return grafico;
	}

	private CartesianChartModel criarGraficoIndicadoresEconomicoReceitaLiquidaHa(Municipio Municipio) {
		List<GraficoBarra> plantios = indicadoresService.graficoIndicadoresEconomicoReceitaLiquidaHa(Municipio);
		CartesianChartModel grafico = new CartesianChartModel();
		montarGraficoBarra(grafico, plantios, ResourceBundle.getMessage("receitaLiquidaHa"));
		
		return grafico;
	}
	
	private CartesianChartModel criarGraficoIndicadoresEconomicoReceitaLiquidaM3(Municipio Municipio) {
		List<GraficoBarra> plantios = indicadoresService.graficoIndicadoresEconomicoReceitaLiquidaM3(Municipio);
		CartesianChartModel grafico = new CartesianChartModel();
		montarGraficoBarra(grafico, plantios, ResourceBundle.getMessage("receitaLiquidaM3"));
		
		return grafico;
	}
	
	private CartesianChartModel criarGraficoIndicadoresEconomicoVBP(Municipio Municipio) {
		List<GraficoBarra> plantios = indicadoresService.graficoIndicadoresEconomicoVBP(Municipio);
		CartesianChartModel grafico = new CartesianChartModel();
		montarGraficoBarra(grafico, plantios, ResourceBundle.getMessage("vbp"));
		
		return grafico;
	}

	private CartesianChartModel criarGraficoIndicadoresSegSocialEmpregosHa(Municipio Municipio) {
		List<GraficoBarra> plantios = indicadoresService.graficoIndicadoresSegSocialEmpregosHa(Municipio);
		CartesianChartModel grafico = new CartesianChartModel();
		montarGraficoBarra(grafico, plantios, ResourceBundle.getMessage("empregosHa"));
		
		return grafico;
	}
	
	private CartesianChartModel criarGraficoIndicadoresSegSocialEmpregosM3(Municipio Municipio) {
		List<GraficoBarra> plantios = indicadoresService.graficoIndicadoresSegSocialEmpregosM3(Municipio);
		CartesianChartModel grafico = new CartesianChartModel();
		montarGraficoBarra(grafico, plantios, ResourceBundle.getMessage("empregosM3"));
		
		return grafico;
	}
	
	private CartesianChartModel criarGraficoIndicadoresSegHidricaM3Ha(Municipio Municipio) {
		List<GraficoBarra> plantios = indicadoresService.graficoIndicadoresSegHidricaM3Ha(Municipio);
		CartesianChartModel grafico = new CartesianChartModel();
		montarGraficoBarra(grafico, plantios, ResourceBundle.getMessage("m3Ha"));
		
		return grafico;
	}
	
//	private CartesianChartModel criarGraficoIndicadoresSegHidricaLitrosSegundoHa(Municipio Municipio) {
//		List<GraficoBarra> plantios = indicadoresService.graficoIndicadoresSegHidricaLitrosSegundoHa(Municipio);
//		CartesianChartModel grafico = new CartesianChartModel();
//		montarGraficoBarra(grafico, plantios, ResourceBundle.getMessage("litrosSegundoHa"));
//		
//		return grafico;
//	}
	
	private PieChartModel criarGraficoCicloCultura(Municipio Municipio) {
		List<GraficoPizzaIndicadores> plantios = indicadoresService.graficoIndicadoresCicloCultura(Municipio);
		PieChartModel grafico = new PieChartModel();

		montarGrafico(grafico, plantios, Municipio);
		
		return grafico;
	}

	private CartesianChartModel criarGraficoIndicadoresCorteHidrico(Municipio Municipio) {
		List<GraficoBarra> graficos = indicadoresService.graficoIndicadoresCorteHidrico(Municipio);
		Municipio.setAlturaGrafico(graficos.size() * 60);
		CartesianChartModel grafico = new CartesianChartModel();
		montarGraficoBarraHorizontal(grafico, graficos, ResourceBundle.getMessage("corteHidrico"));
																																																																																		
		return grafico;
	}
	
	private void montarGraficoBarra(CartesianChartModel grafico, List<GraficoBarra> graficosBarra, String titulo) {
		ChartSeries culturas = new ChartSeries();
		if ( graficosBarra != null && !graficosBarra.isEmpty() ) {
	        culturas.setLabel(titulo);
	        for (GraficoBarra graficoBarra : graficosBarra) {
	        	culturas.set(graficoBarra.getValorEixoX() + " - " + graficoBarra.getValorEixoY() + " " + graficoBarra.getUnd(), graficoBarra.getValorEixoY());
			}
	        
	        grafico.addSeries(culturas);
		} else {
			culturas.setLabel(ResourceBundle.getMessage("semDados"));
			culturas.set(0, 0);
	        grafico.addSeries(culturas);
		}
	}
	
	private void montarGraficoBarraHorizontal(CartesianChartModel graficoModel, List<GraficoBarra> graficosBarra, String titulo) {
		if ( !graficosBarra.isEmpty() ) {
			ChartSeries chartSeries = new ChartSeries();
			chartSeries.setLabel( ResourceBundle.getMessage("corteHidrico") );
			
			for (GraficoBarra grafico : graficosBarra) {
				//Calculo Percentual
				double percent = grafico.getValorEixoY();
				BigDecimal percentual = new BigDecimal(percent).setScale(2, RoundingMode.FLOOR);
				
				chartSeries.set( grafico.getValorEixoX() +" - Corte: "+ percentual + "%", grafico.getValorEixoY() );
			}
			graficoModel.addSeries(chartSeries);
		} else {
			ChartSeries chartSeries = new ChartSeries();
			chartSeries.setLabel(ResourceBundle.getMessage("semDados"));
			chartSeries.set(0, 0);
	        graficoModel.addSeries(chartSeries);
		}
	}
	
	private void montarGrafico(PieChartModel grafico, List<GraficoPizzaIndicadores> graficoPizza, Municipio Municipio) {
		BigDecimal perc100 = new BigDecimal(100);
		BigDecimal percParcial = new BigDecimal(0);
		
		if (graficoPizza.isEmpty()) {
			grafico.set("N�o possui plantios", 0);
		} else {
			int qtd = graficoPizza.size();
			int i = 0;
			for (GraficoPizzaIndicadores v : graficoPizza) {
				BigDecimal perc;
				i = i + 1;
				if (i == qtd && v.getTotal() == 0) {
					perc = perc100.subtract(percParcial);
				} else {
					perc = (new BigDecimal(v.getQtd()).multiply(perc100)).divide(
							new BigDecimal(v.getTotal(), BigDecimalUtil.MC).setScale(2, RoundingMode.UP), BigDecimalUtil.MC).setScale(2, RoundingMode.UP);
				}
				percParcial = percParcial.add(new BigDecimal(
						perc.doubleValue() == 0 ? 1 : perc.doubleValue()));
				grafico.set(
						v.getNome() + ": "
								+ perc.doubleValue()+ "%" + " - " + v.getQtd() + " " + v.getUnd(),
								v.getQtd());
			}
			
		}
	}

//	public Boolean getFiltrarPorLote() {
//		return getFiltrarPorMunicipio();
//	}
//
//	public void setFiltrarPorLote(Boolean filtrarPorLote) {
//		this.setFiltrarPorMunicipio(filtrarPorLote);
//	}

//	public Boolean getFiltrarPorMunicipio() {
//		return filtrarPorMunicipio;
//	}
//
//	public void setFiltrarPorMunicipio(Boolean filtrarPorMunicipio) {
//		this.filtrarPorMunicipio = filtrarPorMunicipio;
//	}

	public LazyDataModel<Municipio> getMunicipios() {
		return municipios;
	}

	public void setMunicipios(LazyDataModel<Municipio> municipios) {
		this.municipios = municipios;
	}

	public PieChartModel getGraficoIndicadoresCulturasBacia() {
		return graficoIndicadoresCulturasBacia;
	}

	public void setGraficoIndicadoresCulturasBacia(
			PieChartModel graficoIndicadoresCulturasBacia) {
		this.graficoIndicadoresCulturasBacia = graficoIndicadoresCulturasBacia;
	}


	public CartesianChartModel getGraficoIndicadoresSegProdutivaKgHaBacia() {
		return graficoIndicadoresSegProdutivaKgHaBacia;
	}


	public void setGraficoIndicadoresSegProdutivaKgHaBacia(
			CartesianChartModel graficoIndicadoresSegProdutivaKgHaBacia) {
		this.graficoIndicadoresSegProdutivaKgHaBacia = graficoIndicadoresSegProdutivaKgHaBacia;
	}


	public CartesianChartModel getGraficoIndicadoresSegProdutivaKgM3Bacia() {
		return graficoIndicadoresSegProdutivaKgM3Bacia;
	}


	public void setGraficoIndicadoresSegProdutivaKgM3Bacia(
			CartesianChartModel graficoIndicadoresSegProdutivaKgM3Bacia) {
		this.graficoIndicadoresSegProdutivaKgM3Bacia = graficoIndicadoresSegProdutivaKgM3Bacia;
	}


	public CartesianChartModel getGraficoIndicadoresSegEconomicaReceitaHaBacia() {
		return graficoIndicadoresSegEconomicaReceitaHaBacia;
	}


	public void setGraficoIndicadoresSegEconomicaReceitaHaBacia(
			CartesianChartModel graficoIndicadoresSegEconomicaReceitaHaBacia) {
		this.graficoIndicadoresSegEconomicaReceitaHaBacia = graficoIndicadoresSegEconomicaReceitaHaBacia;
	}


	public CartesianChartModel getGraficoIndicadoresSegEconomicaReceitaM3Bacia() {
		return graficoIndicadoresSegEconomicaReceitaM3Bacia;
	}


	public void setGraficoIndicadoresSegEconomicaReceitaM3Bacia(
			CartesianChartModel graficoIndicadoresSegEconomicaReceitaM3Bacia) {
		this.graficoIndicadoresSegEconomicaReceitaM3Bacia = graficoIndicadoresSegEconomicaReceitaM3Bacia;
	}


	public CartesianChartModel getGraficoIndicadoresSegSocialEmpregosHaBacia() {
		return graficoIndicadoresSegSocialEmpregosHaBacia;
	}


	public void setGraficoIndicadoresSegSocialEmpregosHaBacia(
			CartesianChartModel graficoIndicadoresSegSocialEmpregosHaBacia) {
		this.graficoIndicadoresSegSocialEmpregosHaBacia = graficoIndicadoresSegSocialEmpregosHaBacia;
	}


	public CartesianChartModel getGraficoIndicadoresSegSocialEmpregosM3Bacia() {
		return graficoIndicadoresSegSocialEmpregosM3Bacia;
	}


	public void setGraficoIndicadoresSegSocialEmpregosM3Bacia(
			CartesianChartModel graficoIndicadoresSegSocialEmpregosM3Bacia) {
		this.graficoIndicadoresSegSocialEmpregosM3Bacia = graficoIndicadoresSegSocialEmpregosM3Bacia;
	}


	public CartesianChartModel getGraficoIndicadoresSegHidricaM3HaBacia() {
		return graficoIndicadoresSegHidricaM3HaBacia;
	}


	public void setGraficoIndicadoresSegHidricaM3HaBacia(
			CartesianChartModel graficoIndicadoresSegHidricaM3HaBacia) {
		this.graficoIndicadoresSegHidricaM3HaBacia = graficoIndicadoresSegHidricaM3HaBacia;
	}


	public CartesianChartModel getGraficoIndicadoresSegHidricaLitrosSegundoHaBacia() {
		return graficoIndicadoresSegHidricaLitrosSegundoHaBacia;
	}


	public void setGraficoIndicadoresSegHidricaLitrosSegundoHaBacia(
			CartesianChartModel graficoIndicadoresSegHidricaLitrosSegundoHaBacia) {
		this.graficoIndicadoresSegHidricaLitrosSegundoHaBacia = graficoIndicadoresSegHidricaLitrosSegundoHaBacia;
	}


	public BigDecimal getAreaIrrigadaBacia() {
		return areaIrrigadaBacia;
	}


	public void setAreaIrrigadaBacia(BigDecimal areaIrrigadaBacia) {
		this.areaIrrigadaBacia = areaIrrigadaBacia;
	}

	public CartesianChartModel getGraficoIndicadoresCorteHidricoBacia() {
		return graficoIndicadoresCorteHidricoBacia;
	}

	public void setGraficoIndicadoresCorteHidricoBacia(
			CartesianChartModel graficoIndicadoresCorteHidricoBacia) {
		this.graficoIndicadoresCorteHidricoBacia = graficoIndicadoresCorteHidricoBacia;
	}

	public CartesianChartModel getGraficoIndicadoresSegEconomicaVBPBacia() {
		return graficoIndicadoresSegEconomicaVBPBacia;
	}

	public void setGraficoIndicadoresSegEconomicaVBPBacia(
			CartesianChartModel graficoIndicadoresSegEconomicaVBPBacia) {
		this.graficoIndicadoresSegEconomicaVBPBacia = graficoIndicadoresSegEconomicaVBPBacia;
	}

	public CartesianChartModel getGraficoIndicadoresSegEconomicaReceitaBacia() {
		return graficoIndicadoresSegEconomicaReceitaBacia;
	}

	public void setGraficoIndicadoresSegEconomicaReceitaBacia(
			CartesianChartModel graficoIndicadoresSegEconomicaReceitaBacia) {
		this.graficoIndicadoresSegEconomicaReceitaBacia = graficoIndicadoresSegEconomicaReceitaBacia;
	}

	public PieChartModel getGraficoIndicadorCicloCultura() {
		return graficoIndicadorCicloCultura;
	}

	public void setGraficoIndicadorCicloCultura(
			PieChartModel graficoIndicadorCicloCultura) {
		this.graficoIndicadorCicloCultura = graficoIndicadorCicloCultura;
	}

	public Double getGraficoIndicadoresSegProdutivaKgHaBaciaMax() {
		return graficoIndicadoresSegProdutivaKgHaBaciaMax;
	}

	public void setGraficoIndicadoresSegProdutivaKgHaBaciaMax(
			Double graficoIndicadoresSegProdutivaKgHaBaciaMax) {
		this.graficoIndicadoresSegProdutivaKgHaBaciaMax = graficoIndicadoresSegProdutivaKgHaBaciaMax;
	}

	public Double getGraficoIndicadoresSegProdutivaKgM3BaciaMax() {
		return graficoIndicadoresSegProdutivaKgM3BaciaMax;
	}

	public void setGraficoIndicadoresSegProdutivaKgM3BaciaMax(
			Double graficoIndicadoresSegProdutivaKgM3BaciaMax) {
		this.graficoIndicadoresSegProdutivaKgM3BaciaMax = graficoIndicadoresSegProdutivaKgM3BaciaMax;
	}

	public Double getGraficoIndicadoresSegEconomicaReceitaHaBaciaMax() {
		return graficoIndicadoresSegEconomicaReceitaHaBaciaMax;
	}

	public void setGraficoIndicadoresSegEconomicaReceitaHaBaciaMax(
			Double graficoIndicadoresSegEconomicaReceitaHaBaciaMax) {
		this.graficoIndicadoresSegEconomicaReceitaHaBaciaMax = graficoIndicadoresSegEconomicaReceitaHaBaciaMax;
	}

	public Double getGraficoIndicadoresSegEconomicaReceitaM3BaciaMax() {
		return graficoIndicadoresSegEconomicaReceitaM3BaciaMax;
	}

	public void setGraficoIndicadoresSegEconomicaReceitaM3BaciaMax(
			Double graficoIndicadoresSegEconomicaReceitaM3BaciaMax) {
		this.graficoIndicadoresSegEconomicaReceitaM3BaciaMax = graficoIndicadoresSegEconomicaReceitaM3BaciaMax;
	}

	public Double getGraficoIndicadoresSegEconomicaReceitaBaciaMax() {
		return graficoIndicadoresSegEconomicaReceitaBaciaMax;
	}

	public void setGraficoIndicadoresSegEconomicaReceitaBaciaMax(
			Double graficoIndicadoresSegEconomicaReceitaBaciaMax) {
		this.graficoIndicadoresSegEconomicaReceitaBaciaMax = graficoIndicadoresSegEconomicaReceitaBaciaMax;
	}

	public Double getGraficoIndicadoresSegSocialEmpregosHaBaciaMax() {
		return graficoIndicadoresSegSocialEmpregosHaBaciaMax;
	}

	public void setGraficoIndicadoresSegSocialEmpregosHaBaciaMax(
			Double graficoIndicadoresSegSocialEmpregosHaBaciaMax) {
		this.graficoIndicadoresSegSocialEmpregosHaBaciaMax = graficoIndicadoresSegSocialEmpregosHaBaciaMax;
	}

	public Double getGraficoIndicadoresSegSocialEmpregosM3BaciaMax() {
		return graficoIndicadoresSegSocialEmpregosM3BaciaMax;
	}

	public void setGraficoIndicadoresSegSocialEmpregosM3BaciaMax(
			Double graficoIndicadoresSegSocialEmpregosM3BaciaMax) {
		this.graficoIndicadoresSegSocialEmpregosM3BaciaMax = graficoIndicadoresSegSocialEmpregosM3BaciaMax;
	}

	public Double getGraficoIndicadoresSegHidricaM3HaBaciaMax() {
		return graficoIndicadoresSegHidricaM3HaBaciaMax;
	}

	public void setGraficoIndicadoresSegHidricaM3HaBaciaMax(
			Double graficoIndicadoresSegHidricaM3HaBaciaMax) {
		this.graficoIndicadoresSegHidricaM3HaBaciaMax = graficoIndicadoresSegHidricaM3HaBaciaMax;
	}
	public List<BaciaHidrografica> getBaciasPesquisa() {
		baciasPesquisa.clear();
		
		if (getMultibacia()){
			baciasPesquisa.addAll(getUsuarioLogado().getBacias());
		}else{
			baciasPesquisa.add( getUsuarioLogado().getBaciaPadrao());
		}
		
		return baciasPesquisa;
	}

	public void setBaciasPesquisa(List<BaciaHidrografica> baciasPesquisa) {
		this.baciasPesquisa = baciasPesquisa;
	}

	public Boolean getMultibacia() {
		if (getUsuarioLogado().getBacias() != null && getUsuarioLogado().getBacias().size() > 1 ){
			return true;
		}else{
			return false;
		}
	}
}