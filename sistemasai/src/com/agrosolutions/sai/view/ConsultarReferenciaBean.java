package com.agrosolutions.sai.view;

import java.io.Serializable;
import java.util.List;

import javax.faces.model.SelectItem;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.Cultura;
import com.agrosolutions.sai.model.Referencia;
import com.agrosolutions.sai.model.Variedade;
import com.agrosolutions.sai.service.CulturaService;
import com.agrosolutions.sai.service.ReferenciaService;
import com.agrosolutions.sai.service.VariedadeService;
import com.agrosolutions.sai.util.ResourceBundle;

/**
* Use case : Consultar Referencia
* 
* @since   : julho 27, 2011, 14323:20 AM
* @author  : walljr@gmail.com
*/

@Component("consultarReferenciaBean")
@Scope("session")
public class ConsultarReferenciaBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = -5291678326645164690L;

	public static Logger logger = Logger.getLogger(ConsultarReferenciaBean.class);

	public static final String MSG_TITULO = "Consultar Referencias";

	@Autowired
	private ReferenciaService referenciaService;
	@Autowired
	private CulturaService culturaService;
	@Autowired
	private VariedadeService variedadeService;
								  
	private List<Referencia> referencias;
	private Referencia referenciaSelecionado;
	private SelectItem[] culturas;
	private SelectItem[] variedades;
	
	public void carregarDadosIniciais() {
		referencias = referenciaService.obterTodos();
	
	
		//Popular combobox de filtros
		List<Cultura> culturas = culturaService.obterTodos();
		this.culturas = criarFiltroCulturas(culturas);
		List<Variedade> variedades = variedadeService.obterTodos();
		this.variedades = criarFiltroVariedades(variedades);
	}
	
	private SelectItem[] criarFiltroCulturas(List<Cultura> culturas)  {  
        SelectItem[] options = new SelectItem[culturas.size() + 1];  
  
        options[0] = new SelectItem("", "Todas");  
        for(int i = 0; i < culturas.size(); i++) {  
            options[i + 1] = new SelectItem(culturas.get(i).getNome(), culturas.get(i).getNome());  
        }  
  
        return options;  
    }  

	private SelectItem[] criarFiltroVariedades(List<Variedade> variedades)  {  
        SelectItem[] options = new SelectItem[variedades.size() + 1];  
  
        options[0] = new SelectItem("", "Todas");  
        for(int i = 0; i < variedades.size(); i++) {  
            options[i + 1] = new SelectItem(variedades.get(i).getNome(), variedades.get(i).getNome());  
        }  
  
        return options;  
    }  

	public String cadastro() {
		return "cadastrarReferencia?faces-redirect=true";
	}
	
	public String voltar() {
		return "consultarReferencia?faces-redirect=true";
	}

	public void excluir(){
	    referenciaService.excluir(referenciaSelecionado);
	    referencias = null;
	    referenciaSelecionado = null;
		infoMsg(ResourceBundle.getMessage("referenciaExcluir")+ ".", MSG_TITULO);
   }  	
	
	public SelectItem[] getCulturas() {
		return culturas;
	}

	public void setCulturas(SelectItem[] culturas) {
		this.culturas = culturas;
	}

	public SelectItem[] getVariedades() {
		return variedades;
	}

	public void setVariedades(SelectItem[] variedades) {
		this.variedades = variedades;
	}
	public List<Referencia> getReferencias() {
		return referencias;
	}
	public void setReferencias(List<Referencia> referencias) {
		this.referencias = referencias;
	}
	public Referencia getReferenciaSelecionado() {
		return referenciaSelecionado;
	}
	public void setReferenciaSelecionado(Referencia referenciaSelecionado) {
		this.referenciaSelecionado = referenciaSelecionado;
	}

}
