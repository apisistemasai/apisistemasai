package com.agrosolutions.sai.view;

import java.util.List;

import javax.faces.model.ListDataModel;

import org.primefaces.model.SelectableDataModel;

import com.agrosolutions.sai.model.Funcionalidade;
  
public class FuncionalidadeDataModel extends ListDataModel<Funcionalidade> implements SelectableDataModel<Funcionalidade> {    
  
    public FuncionalidadeDataModel() {  
    }  
  
    public FuncionalidadeDataModel(List<Funcionalidade> data) {  
        super(data);  
    }  
      
    @Override  
    public Funcionalidade getRowData(String rowKey) {  
          
        @SuppressWarnings("unchecked")
		List<Funcionalidade> datas = (List<Funcionalidade>) getWrappedData();  
          
        for(Funcionalidade data : datas) {  
            if(data.getId().equals(rowKey))  
                return data;  
        }  
          
        return null;  
    }  
  
    @Override  
    public Object getRowKey(Funcionalidade data) {  
        return data.getId();  
    }  
}  
                     