package com.agrosolutions.sai.view;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.primefaces.event.TabChangeEvent;
import org.primefaces.model.chart.PieChartModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.Area;
import com.agrosolutions.sai.model.Distrito;
import com.agrosolutions.sai.model.GraficoPizza;
import com.agrosolutions.sai.service.AreaService;
import com.agrosolutions.sai.service.ClimaService;
import com.agrosolutions.sai.service.PlantioService;
import com.agrosolutions.sai.service.SetorService;
import com.agrosolutions.sai.util.BigDecimalUtil;

/**
* Use case : Consultar EDistrito
* 
* @since   : Mar 20, 2012, 14:23:20 AM
* @author  : rpf1404@gmail.com
*/

@Component("consultarEDistritoBean")
@Scope("view")
public class ConsultarEDistritoBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = 3407645880653466152L;

	public static Logger logger = Logger.getLogger(ConsultarEDistritoBean.class);

	public static final String MSG_TITULO = "Consultar �reas";

	@Autowired
	private AreaService areaService;
	@Autowired
	private ClimaService climaService;
	@Autowired
	private PlantioService plantioService;
	@Autowired
	private SetorService setorService;
	
	private List<Area> areas;
	private List<Distrito> distritos;
	private Boolean porArea;

	
	public void carregarDadosIniciais() {
		if (porArea == null) {
			porArea = true;
		}
		
		if (porArea) {
			areas = areaService.pesquisar(super.getDistritosPesquisa());
			for (Area a : areas) {
				a.setClima(climaService.ultimoPorArea(a));
				a.setGraficoCultura(criarGraficoCultura(a));
				a.setGraficoSolo(criarGraficoSolo(a));
				a.setGraficoAtividade(criarGraficoAtividade(a));
				a.setGraficoIrrigante(criarGraficoIrrigante(a));
				a.setGraficoIrrigacao(criarGraficoIrrigacao(a));
				a.setGraficoCulturasPorHa(criarGraficoCulturasPorHa(a));
			}
		} else {
			distritos = super.getDistritosPesquisa();
			for (Distrito d : distritos) {
				d.setClima(climaService.ultimoPorDistrito(d));
				d.setGraficoCultura(criarGraficoCultura(d));
				d.setGraficoSolo(criarGraficoSolo(d));
				d.setGraficoAtividade(criarGraficoAtividade(d));
				d.setGraficoIrrigante(criarGraficoIrrigante(d));
				d.setGraficoIrrigacao(criarGraficoIrrigacao(d));
				d.setGraficoCulturasPorHa(criarGraficoCulturasPorHa(d));
			}

		}
	}

   public void mudancaTabDistrito(TabChangeEvent event) {  
        infoMsg("Active Tab: " + event.getTab().getTitle(), event.getTab().getId());  
    }  
	   
   public void mudancaTabArea(TabChangeEvent event) {  
	      infoMsg("Active Tab: " + event.getTab().getTitle(), event.getTab().getId());  
    }
   
	private PieChartModel criarGraficoCultura(Area area) {
		PieChartModel grafico = new PieChartModel();
		List<GraficoPizza> culturas = plantioService.culturaAtivaPorArea(area, new Date());
		montarGrafico(grafico, culturas);
		
		return grafico;
	}

	private PieChartModel criarGraficoSolo(Area area) {
		PieChartModel grafico = new PieChartModel();
		List<GraficoPizza> solos = plantioService.soloPorArea(area);
		montarGrafico(grafico, solos);
		
		return grafico;
	}

	private PieChartModel criarGraficoIrrigante(Area area) {
		PieChartModel grafico = new PieChartModel();
		List<GraficoPizza> irrigantes = plantioService.irrigantePorArea(area);
		montarGrafico(grafico, irrigantes);
		
		return grafico;
	}

	private PieChartModel criarGraficoAtividade(Distrito d) {
		PieChartModel grafico = new PieChartModel();
		List<GraficoPizza> solos = plantioService.atividadePorDistrito(d, new Date());
		montarGrafico(grafico, solos);
		
		return grafico;
	}
	private PieChartModel criarGraficoCultura(Distrito area) {
		PieChartModel grafico = new PieChartModel();
		List<GraficoPizza> culturas = plantioService.culturaAtivaPorDistrito(area, new Date());
		montarGrafico(grafico, culturas);
		
		return grafico;
	}

	private PieChartModel criarGraficoSolo(Distrito area) {
		PieChartModel grafico = new PieChartModel();
		List<GraficoPizza> solos = plantioService.soloPorDistrito(area);
		montarGrafico(grafico, solos);
		
		return grafico;
	}

	private PieChartModel criarGraficoIrrigante(Distrito area) {
		PieChartModel grafico = new PieChartModel();
		List<GraficoPizza> irrigantes = plantioService.irrigantePorDistrito(area);
		montarGrafico(grafico, irrigantes);
		
		return grafico;
	}

	private PieChartModel criarGraficoIrrigacao(Distrito d) {
		PieChartModel grafico = new PieChartModel();
		List<GraficoPizza> irrigacoes = setorService.irrigacaoPorDistrito(d);
		montarGrafico(grafico, irrigacoes);
		
		return grafico;
	}

	private PieChartModel criarGraficoCulturasPorHa(Distrito d) {
		PieChartModel grafico = new PieChartModel();
		List<GraficoPizza> irrigacoes = plantioService.culturaHecPorDistrito(d, new Date());
		montarGrafico(grafico, irrigacoes);
		
		return grafico;
	}
	
	private PieChartModel criarGraficoCulturasPorHa(Area a) {
		PieChartModel grafico = new PieChartModel();
		List<GraficoPizza> irrigacoes = plantioService.culturaHecPorArea(a, new Date());
		montarGrafico(grafico, irrigacoes);
		
		return grafico;
	}

	private PieChartModel criarGraficoIrrigacao(Area area) {
		PieChartModel grafico = new PieChartModel();
		List<GraficoPizza> irrigacoes = setorService.irrigacaoPorArea(area);
		montarGrafico(grafico, irrigacoes);
		
		return grafico;
	}

	private PieChartModel criarGraficoAtividade(Area area) {
		PieChartModel grafico = new PieChartModel();
		List<GraficoPizza> solos = plantioService.atividadePorArea(area, new Date());
		montarGrafico(grafico, solos);
		
		return grafico;
	}

	private void montarGrafico(PieChartModel grafico, List<GraficoPizza> graficoPizza) {
		BigDecimal totalParcial = new BigDecimal(0);
		BigDecimal total = new BigDecimal(0);
		BigDecimal perc100 = new BigDecimal(100);
		BigDecimal percParcial = new BigDecimal(0); 
		String und = "";
		
		if(graficoPizza.isEmpty() || graficoPizza.get(0).getTotal()==null){
			for (GraficoPizza v : graficoPizza) {
				total = total.add(new BigDecimal(v.getQtd()));
			}
		}else{
			total = new BigDecimal(graficoPizza.get(0).getTotal());
		}
		int qtd = graficoPizza.size();
		int i = 0;
		for (GraficoPizza v : graficoPizza) {
			BigDecimal perc;
			i = i + 1;
			if (i == qtd && v.getTotal()==null){
				perc = perc100.subtract(percParcial);
			}else{
				perc = (new BigDecimal(v.getQtd()).multiply(perc100)).divide(total, BigDecimalUtil.MC_RUP);
				und = v.getUnd();
			}
			percParcial = percParcial.add(new BigDecimal(perc.intValue()==0?1:perc.intValue()));
			grafico.set(v.getNome() + ": " + (perc.intValue()==0?1:perc.intValue()) + "%" + " - " + v.getQtd() + " " + v.getUnd() , v.getQtd());
			totalParcial = totalParcial.add(new BigDecimal(v.getQtd()));
		}
		if (totalParcial.compareTo(total) != 0) {
			BigDecimal perc = perc100.subtract(percParcial);
			grafico.set("Outros" + ": " + perc + "%" + " - " + total.subtract(totalParcial) + " " + und, total.subtract(totalParcial));
		}
	}
	public List<Area> getAreas() {
		return areas;
	}

	public void setAreas(List<Area> areas) {
		this.areas = areas;
	}

	public List<Distrito> getDistritos() {
		return distritos;
	}

	public void setDistritos(List<Distrito> distritos) {
		this.distritos = distritos;
	}

	public Boolean getPorArea() {
		return porArea;
	}

	public void setPorArea(Boolean porArea) {
		this.porArea = porArea;
	}
}
