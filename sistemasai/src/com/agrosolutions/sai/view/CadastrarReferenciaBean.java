package com.agrosolutions.sai.view;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.faces.event.ActionEvent;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.Cultura;
import com.agrosolutions.sai.model.Estadio;
import com.agrosolutions.sai.model.EstadioCultivo;
import com.agrosolutions.sai.model.Referencia;
import com.agrosolutions.sai.model.TipoMetodologia;
import com.agrosolutions.sai.model.Variedade;
import com.agrosolutions.sai.service.CulturaService;
import com.agrosolutions.sai.service.EstadioService;
import com.agrosolutions.sai.service.ReferenciaService;
import com.agrosolutions.sai.service.VariedadeService;
import com.agrosolutions.sai.util.ResourceBundle;

/**
* Use case : Cadastrar Referencia
* 
* @since   : Julho 26, 2012, 14:59:20 pM
* @author  : walljr@gmail.com
*/

@Component("cadastrarReferenciaBean")
@Scope("session")
public class CadastrarReferenciaBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = 1877519992449582332L;

	public static Logger logger = Logger.getLogger(CadastrarReferenciaBean.class);

	@Autowired
	private ReferenciaService referenciaService;
	@Autowired
	private CulturaService culturaService;
	@Autowired
	private VariedadeService variedadeService;
	@Autowired
	private EstadioService estadioService;
	
	private Referencia referencia;
	private Referencia refCopia;
	private Cultura cultura;
	private List<Cultura> culturas;
	private List<Variedade> variedades;
	private String operacao;
	private Estadio estadioSelecionado;
	private List<Referencia> referencias;
	
	public void carregarDadosIniciais() {
		if (operacao.equals("NOVO") && referencia == null) {
			referencia = new Referencia();
			referencia.setEstadios(new ArrayList<Estadio>());
			referencia.getEstadios().add(new Estadio(1, referencia));
		}
		if ((operacao.equals("ALTERAR") || operacao.equals("VER")) && referencia != null) {
			referencia.setEstadios(estadioService.pesquisar(referencia));
			setCultura(referencia.getVariedade().getCultura());
			variedades = variedadeService.pesquisar(getCultura());
		}
		culturas = culturaService.obterTodos();
	}

	public void carregarReferencia() {
		if (refCopia != null) {
			referencia.setAno(refCopia.getAno());
			referencia.setAutor(refCopia.getAutor());
			referencia.setCidadep(refCopia.getCidadep());
			referencia.setCoautor(refCopia.getCoautor());
			referencia.setEstadop(refCopia.getEstadop());
			referencia.setObservacao(refCopia.getObservacao());
			referencia.setPaisp(refCopia.getPaisp());
			referencia.setTipoMetodologiaETc(refCopia.getTipoMetodologiaETc());
			referencia.setTipoMetodologiaETo(refCopia.getTipoMetodologiaETo());
			
			List<Estadio> estadiosNovo = new ArrayList<Estadio>();
			for (Estadio e : refCopia.getEstadios()) {
				Estadio estadio = new Estadio(e, referencia);
				estadiosNovo.add(estadio);
			}
			referencia.setEstadios(estadiosNovo);
		}
	}

	
	
	public void carregarVariedades() {
		if(getCultura()==null){
			variedades = null;
			referencias = null;
		}else{
			variedades = variedadeService.pesquisar(getCultura());
			referencias = referenciaService.pesquisarCultura(getCultura());
		}
		referencia.setVariedade(null);
	}

	public void gravar(ActionEvent actionEvent) {
		prepararReferencia(referencia);
		referencia = referenciaService.salvar(referencia);
		if (operacao.equals("NOVO")) {
			referencia = null;
			infoMsg(ResourceBundle.getMessage("referenciaSalvar")+ ".", null);
		}else{
			infoMsg(ResourceBundle.getMessage("referenciaAlterar")+ ".", null);
		}
    }
	
	private void prepararReferencia(Referencia referencia) {
		for (Estadio e : referencia.getEstadios()) {
			if (e.getKc() != null && (e.getKc().equals(new BigDecimal("0.00"))|| e.getKc().equals(new BigDecimal("0")))) {
				e.setKc(null);
			}
			
			if (e.getProfundidadeRaiz() != null && (e.getProfundidadeRaiz().equals(new BigDecimal("0.00")) || e.getProfundidadeRaiz().equals(new BigDecimal("0")))) {
				e.setProfundidadeRaiz(null);
			}
			
			if (e.getDiametroCopa() != null && (e.getDiametroCopa().equals(new BigDecimal("0.00")) || e.getDiametroCopa().equals(new BigDecimal("0")))) {
				e.setDiametroCopa(null);
			}
		}
		
	}

	public void excluir() {
		referencia.getEstadios().remove(estadioSelecionado);
		if (estadioSelecionado.getId()!= null) {
			estadioService.excluir(estadioSelecionado);
		}
		estadioSelecionado = null;
		infoMsg(ResourceBundle.getMessage("estadioRemovido")+ ".", null);
    }

	public void maisEstadio(ActionEvent actionEvent) {
		referencia.getEstadios().add(new Estadio(referencia.getEstadios().size()+1, referencia));
    }

	public String voltar() {
		return "consultarReferencia?faces-redirect=true";
	}

	public ReferenciaService getReferenciaService() {
		return referenciaService;
	}

	public void setReferenciaService(ReferenciaService referenciaService) {
		this.referenciaService = referenciaService;
	}
	
	public TipoMetodologia[] getTipos() {
		return TipoMetodologia.values();
	}

	public Referencia getReferencia() {
		return referencia;
	}

	public void setReferencia(Referencia referencia) {
		this.referencia = referencia;
	}

	public String getOperacao() {
		return operacao;
	}

	public void setOperacao(String operacao) {
		this.operacao = operacao;
	}

	public List<Cultura> getCulturas() {
		return culturas;
	}

	public void setCulturas(List<Cultura> culturas) {
		this.culturas = culturas;
	}

	public List<Variedade> getVariedades() {
		return variedades;
	}

	public void setVariedades(List<Variedade> variedades) {
		this.variedades = variedades;
	}
	
	public EstadioCultivo[] getFases(){
		return EstadioCultivo.values();
	}

	public Cultura getCultura() {
		return cultura;
	}

	public void setCultura(Cultura cultura) {
		this.cultura = cultura;
	}
	public Estadio getEstadioSelecionado() {
		return estadioSelecionado;
	}

	public void setEstadioSelecionado(Estadio estadioSelecionado) {
		this.estadioSelecionado = estadioSelecionado;
	}




	public List<Referencia> getReferencias() {
		return referencias;
	}




	public void setReferencias(List<Referencia> referencias) {
		this.referencias = referencias;
	}

	public Referencia getRefCopia() {
		return refCopia;
	}

	public void setRefCopia(Referencia refCopia) {
		this.refCopia = refCopia;
	}
}
