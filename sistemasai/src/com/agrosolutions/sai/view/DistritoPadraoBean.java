package com.agrosolutions.sai.view;

import java.io.Serializable;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.Distrito;
import com.agrosolutions.sai.service.UsuarioService;

/**
 * 
 * @author Kleben Ribeiro
 * @since	Aug 29, 2014, 10:46:35 AM
 *
 */


@Component("distritoPadraoBean")
@Scope("session")
public class DistritoPadraoBean extends ManagerBean implements Serializable{	
	
	@Autowired	
	private UsuarioService usuarioService;
	
	private static final long serialVersionUID = -52755771059638767L;
	public static Logger logger = Logger.getLogger(DistritoPadraoBean.class);
	private Distrito distritoSelecionado = new Distrito();
	
	
	public String selecionarDistritoPadrao(){
		getRequestSession().getSession().setAttribute("distritoPadrao", distritoSelecionado );
		getUsuarioLogado().setDistritoPadrao(distritoSelecionado);
		usuarioService.alterarDistritoPadrao(getUsuarioLogado());
		
		return "/home?redirect-faces=true";
	}
	
	public Distrito getDistritoSelecionado() {
		if (super.getUsuarioLogado().getDistritoPadrao() != null ){
			distritoSelecionado = super.getUsuarioLogado().getDistritoPadrao();
		}
		
		return distritoSelecionado;
	}

	public void setDistritoSelecionado(Distrito distritoSelecionado) {
		setDistritoPadrao(distritoSelecionado);
		this.distritoSelecionado = distritoSelecionado;
	}

}
