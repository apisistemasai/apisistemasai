package com.agrosolutions.sai.view;

import java.io.Serializable;
import java.util.List; 

import javax.faces.model.SelectItem;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.Adubo;
import com.agrosolutions.sai.model.Cultura;
import com.agrosolutions.sai.model.Defensivo;
import com.agrosolutions.sai.model.Doenca;
import com.agrosolutions.sai.model.Irrigante;
import com.agrosolutions.sai.model.Plantio;
import com.agrosolutions.sai.model.Praga;
import com.agrosolutions.sai.model.Setor;
import com.agrosolutions.sai.model.Variedade;
import com.agrosolutions.sai.service.AduboService;
import com.agrosolutions.sai.service.CultivoService;
import com.agrosolutions.sai.service.DefensivoService;
import com.agrosolutions.sai.service.DoencaService;
import com.agrosolutions.sai.service.PlantioService;
import com.agrosolutions.sai.service.PragaService;
import com.agrosolutions.sai.service.SetorService;

/**
* Use case : Consultar Plantio
* 
* @since   : Jan 19, 2011, 03:23:20 AM
* @author  : raphael.ferreira@ivia.com.br
*/

@Component("consultarPlantioBean")
@Scope("session")
public class ConsultarPlantioBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = -4189814820528324137L;

	public static Logger logger = Logger.getLogger(ConsultarPlantioBean.class);

	public static final String MSG_TITULO = "Consultar Plantios";

	@Autowired
	private PlantioService plantioService;
	@Autowired
	private CultivoService cultivoService;
	@Autowired
	private SetorService setorService;
/*	@Autowired
	private LoteService loteService;
*/	@Autowired
	private DoencaService doencaService;
	@Autowired
	private PragaService pragaService;
	@Autowired
	private AduboService aduboService;
	@Autowired
	private DefensivoService defensivoService;

	
	private Irrigante irrigante;
	private List<Plantio> plantios;
	private Plantio doencaSelecionado;
	private Plantio pragaSelecionado;
	private Plantio aduboSelecionado;
	private Plantio defensivoSelecionado;
	private SelectItem[] culturas;
	private SelectItem[] variedades;
	private SelectItem[] setores;
	private SelectItem[] lotes;
	
	private Boolean incluirDoenca;
	private Boolean novaDoenca;
	private Doenca doenca;
	private Doenca doencaSelecionada;
	private List<Doenca> doencas;
	private List<Praga> pragas;
	private List<Adubo> adubos;
	private List<Defensivo> defensivos;
	
	public void carregarDadosIniciais() {
		plantios = plantioService.pesquisarPorIrrigante(irrigante);

		//Popular combobox de filtros
/*		List<Lote> lotes = loteService.pesquisar(irrigante);
		this.lotes = criarFiltroLotes(lotes);
*/		List<Cultura> culturas = cultivoService.pesquisarCulturas(irrigante);
		this.culturas = criarFiltroCulturas(culturas);
		List<Variedade> variedades = cultivoService.pesquisarVariedades(irrigante);
		this.variedades = criarFiltroVariedades(variedades);
		List<Setor> setores = setorService.pesquisar(irrigante);
		this.setores = criarFiltroSetores(setores);
		
		doencaSelecionado = new Plantio();
		pragaSelecionado = new Plantio();
		aduboSelecionado = new Plantio();
		defensivoSelecionado = new Plantio();
		
	}

	private SelectItem[] criarFiltroSetores(List<Setor> setores)  {  
        SelectItem[] options = new SelectItem[setores.size() + 1];  
  
        options[0] = new SelectItem("", "Todas");  
        for(int i = 0; i < setores.size(); i++) {  
            options[i + 1] = new SelectItem(setores.get(i).getIdentificacao().toString(), setores.get(i).getIdentificacao().toString());  
        }  
  
        return options;  
    }  

/*	private SelectItem[] criarFiltroLotes(List<Lote> lotes)  {  
        SelectItem[] options = new SelectItem[lotes.size() + 1];  
  
        options[0] = new SelectItem("", "Todas");  
        for(int i = 0; i < lotes.size(); i++) {  
            options[i + 1] = new SelectItem(lotes.get(i).getIdentificacao(), lotes.get(i).getIdentificacao());  
        }  
  
        return options;  
    }  
*/
	private SelectItem[] criarFiltroCulturas(List<Cultura> culturas)  {  
        SelectItem[] options = new SelectItem[culturas.size() + 1];  
  
        options[0] = new SelectItem("", "Todas");  
        for(int i = 0; i < culturas.size(); i++) {  
            options[i + 1] = new SelectItem(culturas.get(i).getNome(), culturas.get(i).getNome());  
        }  
  
        return options;  
    }  

	private SelectItem[] criarFiltroVariedades(List<Variedade> variedades)  {  
        SelectItem[] options = new SelectItem[variedades.size() + 1];  
  
        options[0] = new SelectItem("", "Todas");  
        for(int i = 0; i < variedades.size(); i++) {  
            options[i + 1] = new SelectItem(variedades.get(i).getNome(), variedades.get(i).getNome());  
        }  
  
        return options;  
    }  

	public String cadastro() {
		return "manterPlantio?faces-redirect=true";
	}

	public void carregarDoencas(){
		doencaSelecionado = plantioService.carregarDoenca(doencaSelecionado);
		doencas = doencaService.obterTodos();
		if (doencas.isEmpty()) {
			novaDoenca = true;
		}else{
			novaDoenca = false;
		}

	}

	public void incluirDoenca(){
		incluirDoenca = true;
		doenca = new Doenca();

	}

	public void prepararDoenca() { 
		novaDoenca = true;
	}  

	public void salvarDoenca() {
		if (novaDoenca) {
			doencaSelecionado.getDoencas().add(doencaService.salvar(doenca));
			doencas = doencaService.obterTodos();
		}else{
			doencaSelecionado.getDoencas().add(doencaSelecionada);
		}
		novaDoenca = false;
		plantioService.salvar(doencaSelecionado);
    }	
	
	public String voltar() {
		return "consultarIrrigante?faces-redirect=true";
	}
	
	public String irrigacao() {
		return "consultarIrrigacao?faces-redirect=true";
	}
	
	public String cadastrarIndicadores() {
		return "cadastrarIndicador?faces-redirect=true";
	}

	public Irrigante getIrrigante() {
		return irrigante;
	}

	public void setIrrigante(Irrigante irrigante) {
		this.irrigante = irrigante;
	}

	public SelectItem[] getCulturas() {
		return culturas;
	}

	public void setCulturas(SelectItem[] culturas) {
		this.culturas = culturas;
	}

	public SelectItem[] getVariedades() {
		return variedades;
	}

	public void setVariedades(SelectItem[] variedades) {
		this.variedades = variedades;
	}

	public List<Plantio> getPlantios() {
		return plantios;
	}

	public void setPlantios(List<Plantio> plantios) {
		this.plantios = plantios;
	}

	public SelectItem[] getSetores() {
		return setores;
	}

	public void setSetores(SelectItem[] setores) {
		this.setores = setores;
	}

	public Boolean getNovaDoenca() {
		return novaDoenca;
	}

	public void setNovaDoenca(Boolean novaDoenca) {
		this.novaDoenca = novaDoenca;
	}

	public SelectItem[] getLotes() {
		return lotes;
	}

	public void setLotes(SelectItem[] lotes) {
		this.lotes = lotes;
	}

	public Doenca getDoenca() {
		return doenca;
	}

	public void setDoenca(Doenca doenca) {
		this.doenca = doenca;
	}

	public List<Doenca> getDoencas() {
		return doencas;
	}

	public void setDoencas(List<Doenca> doencas) {
		this.doencas = doencas;
	}

	public List<Praga> getPragas() {
		return pragas;
	}

	public void setPragas(List<Praga> pragas) {
		this.pragas = pragas;
	}

	public List<Adubo> getAdubos() {
		return adubos;
	}

	public void setAdubos(List<Adubo> adubos) {
		this.adubos = adubos;
	}

	public List<Defensivo> getDefensivos() {
		return defensivos;
	}

	public void setDefensivos(List<Defensivo> defensivos) {
		this.defensivos = defensivos;
	}

	public Boolean getIncluirDoenca() {
		return incluirDoenca;
	}

	public void setIncluirDoenca(Boolean incluirDoenca) {
		this.incluirDoenca = incluirDoenca;
	}

	public Plantio getDoencaSelecionado() {
		return doencaSelecionado;
	}

	public void setDoencaSelecionado(Plantio doencaSelecionado) {
		this.doencaSelecionado = doencaSelecionado;
	}

	public Plantio getPragaSelecionado() {
		return pragaSelecionado;
	}

	public void setPragaSelecionado(Plantio pragaSelecionado) {
		this.pragaSelecionado = pragaSelecionado;
	}

	public Plantio getAduboSelecionado() {
		return aduboSelecionado;
	}

	public void setAduboSelecionado(Plantio aduboSelecionado) {
		this.aduboSelecionado = aduboSelecionado;
	}

	public Plantio getDefensivoSelecionado() {
		return defensivoSelecionado;
	}

	public void setDefensivoSelecionado(Plantio defensivoSelecionado) {
		this.defensivoSelecionado = defensivoSelecionado;
	}

	public Doenca getDoencaSelecionada() {
		return doencaSelecionada;
	}

	public void setDoencaSelecionada(Doenca doencaSelecionada) {
		this.doencaSelecionada = doencaSelecionada;
	}

}
