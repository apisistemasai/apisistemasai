package com.agrosolutions.sai.view;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.primefaces.model.StreamedContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.exception.SaiException;
import com.agrosolutions.sai.model.Emissor;
import com.agrosolutions.sai.service.EmissorService;
import com.agrosolutions.sai.service.IrriganteService;
import com.agrosolutions.sai.util.DateUtils;
import com.agrosolutions.sai.util.RelatorioUtil;
import com.agrosolutions.sai.util.ResourceBundle;

/**
* Use case : Relatorio de Teste de EficiÍncia
* 
* @since   : Jun 27, 2013, 20:23:20 PM
* @author  : rpf1404@gmail.com
*/

@Component("relEficienciaBean")
@Scope("session")
public class RelEficienciaBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = 2000210947938981045L;

	public static Logger logger = Logger.getLogger(RelEficienciaBean.class);

	@Autowired
	private RelatorioUtil relatorioUtil;
	@Autowired
	private EmissorService emissorService;
	@Autowired
	private IrriganteService irriganteService;

	private Date dataIni;
	private Date dataFim;
	private List<Emissor> emissores;
	private String localizacao;
	private Emissor emissor;
	private StreamedContent file;
	
	public String carregar(){
		this.emissores = emissorService.obterTodos();
		return null;
	}
	   
	public String imprimir() {
		boolean erro = false;
		try {

			// validando campos da entidade
			if ( getDataIni() == null ){
				errorMsg(ResourceBundle.getMessage("demandaDataI")+ ".", null);
				erro = true;
			}
			if ( getDataFim() == null ){
				errorMsg(ResourceBundle.getMessage("demandaDataF")+ ".", null);
				erro = true;
			}
			if (!erro && getDataIni().after(getDataFim())){
				errorMsg(ResourceBundle.getMessage("demandaDataM")+ ".", null);
				erro = true;
			}
			if ( !localizacao.isEmpty() ){
				try {
					irriganteService.pesquisarPorLote(localizacao, super.getDistritosPesquisa());
				} catch (Exception e) {
					errorMsg(ResourceBundle.getMessage("demandaLote")+ ".", null);
					erro = true;
				}
			}

			if (erro) {
				return null;
			}

			Map<String, Object> parametros = new HashMap<String, Object>();
			
			parametros.put("dtIni", "'" + DateUtils.toString(getDataIni()) + "'");
			parametros.put("dtFim", "'" + DateUtils.toString(getDataFim()) + "'");
			parametros.put("lote", localizacao.isEmpty()?null:"'" + localizacao + "'");
			parametros.put("emissor", emissor==null?null:emissor.getId());
			parametros.put("emissorNome", emissor==null?"Todas":emissor.getFabricante() + " - " + emissor.getModelo() + " - " + emissor.getVazao());

			file = relatorioUtil.relatorio("rptTesteEficiencia.jasper", parametros, "rptTesteEficiencia.pdf");

		} catch (SaiException e) {
			errorMsg(ResourceBundle.getMessage("ocorreuErro")+ ":" + e.getMessage(), null);
			logger.warn("Ocorreu o seguinte erro: " + e.getMessage());
			return null;
		} catch (Exception e) {
			errorMsg(ResourceBundle.getMessage("ocorreuErro")+ ":" + e.getMessage(), null);
			logger.fatal("Ocorreu o seguinte erro: " + e.getMessage());
			return null;
		}
		infoMsg(ResourceBundle.getMessage("demandaRelatorio")+ ".", null);
		return null;
	}


	public Date getDataIni() {
		return dataIni;
	}


	public void setDataIni(Date dataIni) {
		this.dataIni = dataIni;
	}


	public Date getDataFim() {
		return dataFim;
	}


	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}

	public List<Emissor> getEmissores() {
		return emissores;
	}


	public void setEmissores(List<Emissor> emissores) {
		this.emissores = emissores;
	}


	public String getLocalizacao() {
		return localizacao;
	}


	public void setLocalizacao(String localizacao) {
		this.localizacao = localizacao;
	}

	public Emissor getEmissor() {
		return emissor;
	}

	public void setEmissor(Emissor emissor) {
		this.emissor = emissor;
	}

	public StreamedContent getFile() {
		return file;
	}

	public void setFile(StreamedContent file) {
		this.file = file;
	}
}
