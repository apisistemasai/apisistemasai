package com.agrosolutions.sai.view;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.util.List;

import org.apache.log4j.Logger;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.Atividade;
import com.agrosolutions.sai.model.Cultura;
import com.agrosolutions.sai.service.AtividadeService;
import com.agrosolutions.sai.service.CulturaService;
import com.agrosolutions.sai.util.ResourceBundle;

/**
* Use case : Cadastrar Cultura
* 
* @since   : Jan 17, 2012, 22:00:20 AM
* @author  : rpf1404@gmail.com
*/

@Component("cadastrarCulturaBean")
@Scope("session")
public class CadastrarCulturaBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = 4524560196224731215L;

	public static Logger logger = Logger.getLogger(CadastrarCulturaBean.class);

	public static final String MSG_TITULO = "Cadastro de Cultura";

	@Autowired
	private CulturaService culturaService;
	@Autowired
	private AtividadeService atividadeService;
	
	private Cultura cultura;
	private Boolean cadastroVariedade;
	private Boolean cadastroCultivo;
	private List<Atividade> atividades;
	private Atividade atividade;
	
	private String operacao;

	public void carregarDadosIniciais() {
		operacao = getOperacao();
		cultura = getCultura();
		cadastroCultivo = getCadastroCultivo();
		cadastroVariedade = getCadastroVariedade();
		atividade = getAtividade();
		
		if (operacao.equals("NOVO") && cultura == null) {
			cultura = new Cultura();
		}
		if (operacao.equals("ALTERAR") || operacao.equals("NOVO")){		
			//Se tiver atividade preenchida foi recem cadastrada e deve ser setada no objeto.
			if (atividade != null) {
				cultura.setAtividade(atividade);
				atividade = null;
			}
		}
		atividades = atividadeService.obterTodos();
		
	}

	public String novaAtividade() {
		return "cadastrarAtividade";
	}

	public String gravar() {
		String volta = null;
		cultura = culturaService.salvar(cultura); 

		if (operacao.equals("NOVO")) {
			cultura = null;
			infoMsg(ResourceBundle.getMessage("culturaSalvar")+ ".", MSG_TITULO);
		}else{
			infoMsg(ResourceBundle.getMessage("culturaAlterar")+ ".", MSG_TITULO);
		}
		
		if (cadastroCultivo != null && cadastroCultivo) {
			cadastroCultivo = false;
			volta = "cadastrarCultivo?faces-redirect=true";
		}
		if (cadastroVariedade != null && cadastroVariedade) {
			cadastroVariedade = false;
			volta = "cadastrarVariedade?faces-redirect=true";
		}
		
		return volta;
		
    }
    public void uploadImage(FileUploadEvent event) {
    	final String caminho = System.getProperty("imagem_sai");

		try{
	    	UploadedFile arq = event.getFile();
			InputStream in = new BufferedInputStream(arq.getInputstream());
			File file = new File(caminho + File.separator + arq.getFileName());
			FileOutputStream fout = new FileOutputStream(file);
			 
			while(in.available() != 0){
				fout.write(in.read());
			}
			fout.close();
    		cultura.setImage(file.getName());
    		infoMsg("Image foi carregada.", null);
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
    }
	public String voltar() {
		String volta = "consultarCultura?faces-redirect=true";

		if (cadastroCultivo != null && cadastroCultivo) {
			cadastroCultivo = false;
			volta = "cadastrarCultivo?faces-redirect=true";
		}
		if (cadastroVariedade != null && cadastroVariedade) {
			cadastroVariedade = false;
			volta = "cadastrarVariedade?faces-redirect=true";
		}
		
		return volta;
	}

	public Cultura getCultura() {
		if (cultura == null) {
			cultura = (Cultura) getFlashScope().get("cultura");
		}
		return cultura;
	}

	public void setCultura(Cultura cultura) {
		this.cultura = cultura;
		getFlashScope().put("cultura", cultura);
	}

	public Boolean getCadastroVariedade() {
		if (cadastroVariedade == null) {
			cadastroVariedade = (Boolean) getFlashScope().get("cadastroVariedade");
		}
		return cadastroVariedade;
	}

	public void setCadastroVariedade(Boolean cadastroVariedade) {
		this.cadastroVariedade = cadastroVariedade;
		getFlashScope().put("cadastroVariedade", cadastroVariedade);
	}

	public Boolean getCadastroCultivo() {
		if (cadastroCultivo == null) {
			cadastroCultivo = (Boolean) getFlashScope().get("cadastroCultivo");
		}
		return cadastroCultivo;
	}

	public void setCadastroCultivo(Boolean cadastroCultivo) {
		this.cadastroCultivo = cadastroCultivo;
		getFlashScope().put("cadastroCultivo", cadastroCultivo);
	}

	public String getOperacao() {
		if (operacao == null) {
			operacao = (String) getFlashScope().get("operacao");
		}
		return operacao;
	}

	public void setOperacao(String operacao) {
		this.operacao = operacao;
		getFlashScope().put("operacao", operacao);
	}

	public List<Atividade> getAtividades() {
		return atividades;
	}

	public void setAtividades(List<Atividade> atividades) {
		this.atividades = atividades;
	}

	public Atividade getAtividade() {
		if (atividade == null) {
			atividade = (Atividade) getFlashScope().get("atividade");
		}
		return atividade;
	}

	public void setAtividade(Atividade atividade) {
		this.atividade = atividade;
		getFlashScope().put("atividade", atividade);
	}

}
