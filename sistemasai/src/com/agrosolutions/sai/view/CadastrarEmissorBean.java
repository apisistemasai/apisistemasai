package com.agrosolutions.sai.view;

import java.io.Serializable;
import java.util.List;

import javax.faces.event.ActionEvent;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.Emissor;
import com.agrosolutions.sai.model.Fabricante;
import com.agrosolutions.sai.model.TipoEmissor;
import com.agrosolutions.sai.model.TipoFabricante;
import com.agrosolutions.sai.service.EmissorService;
import com.agrosolutions.sai.service.FabricanteService;
import com.agrosolutions.sai.util.ResourceBundle;

/**
* Use case : Cadastrar Emissor
* 
* @since   : Jan 07, 2012, 02:10:20 AM
* @author  : raphael.ferreira@ivia.com.br
*/

@Component("cadastrarEmissorBean")
@Scope("session")
public class CadastrarEmissorBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = 5200682480428364611L;

	public static Logger logger = Logger.getLogger(CadastrarEmissorBean.class);

	public static final String MSG_TITULO = "Cadastro de Emissor";

	@Autowired
	private EmissorService emissorService;
	@Autowired
	private FabricanteService fabricanteService;
	
	private Emissor emissor;
	private List<Fabricante> fabricantes;
	private Fabricante fabricante;
	private String operacao;

	public void carregarDadosIniciais() {
		operacao = getOperacao();
		emissor = getEmissor();
		fabricante = getFabricante();
		
		if (operacao.equals("NOVO") && emissor == null) {
			emissor = new Emissor();
		}
		fabricantes = fabricanteService.pesquisar(TipoFabricante.Emissor);
		if (fabricante != null) {
			emissor.setFabricante(fabricante);
			fabricante = null;
		}
	}
	
	public void gravar(ActionEvent actionEvent) {
		emissor = emissorService.salvar(emissor); 
		if (operacao.equals("NOVO")) {
			emissor = null;
			infoMsg(ResourceBundle.getMessage("emissorSalvar")+ ".", MSG_TITULO);
		}else{
			infoMsg(ResourceBundle.getMessage("emissorAlterar")+ ".", MSG_TITULO);
		}
    }

	public String novoFabricante() {
		return "cadastrarFabricante?faces-redirect=true";
	}

	public String voltar() {
		return "consultarEmissor?faces-redirect=true";
	}

	public Emissor getEmissor() {
		if (emissor == null) {
			emissor = (Emissor) getFlashScope().get("emissor");
		}
		return emissor;
	}

	public void setEmissor(Emissor emissor) {
		this.emissor = emissor;
		getFlashScope().put("emissor", emissor);
	}

	public List<Fabricante> getFabricantes() {
		return fabricantes;
	}

	public void setFabricantes(List<Fabricante> fabricantes) {
		this.fabricantes = fabricantes;
	}

	public TipoEmissor[] getTipos() {
		return TipoEmissor.values();
	}

	public Fabricante getFabricante() {
		if (fabricante == null) {
			fabricante = (Fabricante) getFlashScope().get("fabricante");
		}
		return fabricante;
	}

	public void setFabricante(Fabricante fabricante) {
		this.fabricante = fabricante;
		getFlashScope().put("fabricante", fabricante);
	}

	public String getOperacao() {
		if (operacao == null) {
			operacao = (String) getFlashScope().get("operacao");
		}
		return operacao;
	}

	public void setOperacao(String operacao) {
		this.operacao = operacao;
		getFlashScope().put("operacao", operacao);
	}
}
