package com.agrosolutions.sai.view;

import java.io.Serializable;
import java.util.List;

import javax.faces.event.ActionEvent;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.Bomba;
import com.agrosolutions.sai.model.Tanque;
import com.agrosolutions.sai.model.TipoBomba;
import com.agrosolutions.sai.service.BombaService;
import com.agrosolutions.sai.service.TanqueService;
import com.agrosolutions.sai.util.ResourceBundle;

/**
* Use case : Cadastrar Tanque
* 
* @since   : Dez 10, 2015, 22:10:20 AM
* @author  : raphael.ferreira@ivia.com.br
*/

@Component("cadastrarTanqueBean")
@Scope("session")
public class CadastrarTanqueBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = -8533449348784583L;

	public static Logger logger = Logger.getLogger(CadastrarTanqueBean.class);

	@Autowired
	private TanqueService tanqueService;
	@Autowired
	private BombaService bombaService;
	
	private Tanque tanque;
	private List<Tanque> tanques;
	private List<Bomba> bombas;
	private String operacao;
	private Boolean temBomba;
	
	public void carregarDadosIniciais() {
		operacao = getOperacao();
		tanque = getTanque();
		
		if (operacao.equals("NOVO") && tanque == null) {
			tanque = new Tanque();
		}
		if (tanques == null) {
			tanques = tanqueService.obterTodos();
			Bomba bombaBusca = new Bomba();
			bombaBusca.setTipo(TipoBomba.Fertirrigacao);
			setBombas(bombaService.pesquisar(bombaBusca));
		}
		if (temBomba == null) {
			temBomba = false;
		}
	}
	
	public void gravar(ActionEvent actionEvent) {
		tanque = tanqueService.salvar(tanque);
		if (operacao.equals("NOVO")) {
			tanque = null;
			infoMsg(ResourceBundle.getMessage("tanqueSalvar"), null);
		}else{
			infoMsg(ResourceBundle.getMessage("tanqueAlterar"), null);
		}
    }

	public String voltar() {
		return "consultarTanques?faces-redirect=true";
	}
	
	public Tanque getTanque() {
		if (tanque == null) {
			tanque = (Tanque) getFlashScope().get("tanque");
		}
		return tanque;
	}

	public void setTanque(Tanque tanque) {
		this.tanque = tanque;
		getFlashScope().put("tanque", tanque);
	}

	public List<Tanque> getTanques() {
		return tanques;
	}

	public void setTanques(List<Tanque> tanques) {
		this.tanques = tanques;
	}

	public String getOperacao() {
		if (operacao == null) {
			operacao = (String) getFlashScope().get("operacao");
		}
		return operacao;
	}

	public void setOperacao(String operacao) {
		this.operacao = operacao;
		getFlashScope().put("operacao", operacao);
	}

	public List<Bomba> getBombas() {
		return bombas;
	}

	public void setBombas(List<Bomba> bombas) {
		this.bombas = bombas;
	}

	public Boolean getTemBomba() {
		return temBomba;
	}

	public void setTemBomba(Boolean temBomba) {
		this.temBomba = temBomba;
	}
}
