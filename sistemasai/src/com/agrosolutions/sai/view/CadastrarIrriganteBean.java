package com.agrosolutions.sai.view;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.event.ActionEvent;

import org.apache.log4j.Logger;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.Area;
import com.agrosolutions.sai.model.Bomba;
import com.agrosolutions.sai.model.Coordenada;
import com.agrosolutions.sai.model.Distrito;
import com.agrosolutions.sai.model.Escolaridade;
import com.agrosolutions.sai.model.Irrigante;
import com.agrosolutions.sai.model.OperadoraCelular;
import com.agrosolutions.sai.model.Perfil;
import com.agrosolutions.sai.model.TipoIrrigante;
import com.agrosolutions.sai.model.TipoPerfil;
import com.agrosolutions.sai.model.Usuario;
import com.agrosolutions.sai.service.AreaService;
import com.agrosolutions.sai.service.BombaService;
import com.agrosolutions.sai.service.CoordenadaService;
import com.agrosolutions.sai.service.IrriganteService;
import com.agrosolutions.sai.service.PerfilService;
import com.agrosolutions.sai.util.KmlUtil;
import com.agrosolutions.sai.util.ResourceBundle;

/**
* Use case : Cadastrar Irrigante
* 
* @since   : Dez 24, 2011, 20:23:20 PM
* @author  : raphael.ferreira@ivia.com.br
*/

@Component("cadastrarIrriganteBean")
@Scope("session")
public class CadastrarIrriganteBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = -3539217161253634343L;
	public static Logger logger = Logger.getLogger(CadastrarIrriganteBean.class);

	public static final String MSG_TITULO = "Cadastro de Irrigante";

	@Autowired
	private IrriganteService irriganteService;
	@Autowired
	private AreaService areaService;
	@Autowired
	private PerfilService perfilService;
	@Autowired
	private BombaService bombaService;
	@Autowired
	private CoordenadaService coordenadaService;
	
	private Irrigante irrigante;
	private List<Distrito> distritos;
	private Distrito distrito;
	private List<Area> areas;
	private List<Perfil> perfis;
	private List<Bomba> bombas;
	private Bomba[] bombasSelecionadas;
	private String operacao;
	private Boolean isIrrigante;
	
	public void carregarDadosIniciais() {
		if (irrigante == null && getUsuarioLogado().getPerfil().getTipo().equals(TipoPerfil.Irrigante)) {
			irrigante = irriganteService.pesquisarPorUsuario(getUsuarioLogado());
			operacao = "ALTERAR";
			isIrrigante = true;
		}
		
		//Se for um novo e ainda tiver nulo criar objeto.
		if (operacao.equals("NOVO") && irrigante == null) {
			irrigante = new Irrigante();
			irrigante.setDataCadastro(new Date());
			irrigante.setUsuario(new Usuario());
			irrigante.setArea(new Area());
			//O padrão dos checkboxs marcados
			irrigante.setEnviarEmail(true);
			irrigante.setEnviarSMS(true);
		}
		if ((operacao.equals("ALTERAR") || operacao.equals("VER")) && irrigante != null) {
			if(irrigante.getUsuario().getConfirmarEmail() == null) {
				irrigante.getUsuario().setConfirmarEmail(irrigante.getUsuario().getEmail());
				irrigante.getUsuario().setConfirmarSenha(null);
			}
			if(irrigante.getArea()!= null && irrigante.getArea().getDistrito() != null){
				distrito = irrigante.getArea().getDistrito();
			}
		}
		
		//	Reseta a listagem dos distritos quando mudar o distrito.
		if ( super.getMultidistrito() && distritos != null && (distritos.get(0).getId() != super.getDistritoPadrao().getId()) ) distritos = null;
		
		if (distritos == null){
			bombas = bombaService.obterTodos();
			distritos = super.getDistritosPesquisa();
			perfis = perfilService.porTipo(TipoPerfil.Irrigante);
			if (multidistrito) {
				distritos.clear();
				distritos.add(getUsuarioLogado().getDistritoPadrao());
			}
			areas = areaService.pesquisar(distritos);
		}
	}
	

    public void uploadCoordenadas(FileUploadEvent event) {
		try{
			UploadedFile arq = event.getFile();
			InputStream in = new BufferedInputStream(arq.getInputstream());
			
			KmlUtil kmlUtil = new KmlUtil();
			List<Coordenada> coordenadas = new ArrayList<Coordenada>();
			coordenadas = kmlUtil.getCoordenadas(in);
			coordenadaService.salvar(coordenadas);
			irrigante.setCoordenadasPoligono(coordenadas);
			irriganteService.salvar(irrigante);
			
    		infoMsg(ResourceBundle.getMessage("sucessoImportarCoordenadas"), null);
		} catch(Exception e){
			e.getStackTrace();
			errorMsg("erroImportarCoordenadas", null);
		}
    }
	
    public void uploadFoto(FileUploadEvent event) {
    	final String caminho =""; System.getProperty("imagem_sai");
    	

		try{
	    	UploadedFile arq = event.getFile();
			InputStream in = new BufferedInputStream(arq.getInputstream());
			File file = new File(caminho + File.separator + arq.getFileName());
			FileOutputStream fout = new FileOutputStream(file);
			 
			while(in.available() >= 0){
				fout.write(in.read());
			}
			fout.close();
    		irrigante.setFoto(file.getName());
    		infoMsg("Foto foi carregada.", null);
    		
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
    }

    public void incluirBomba(){
		List<Bomba> bombas = new ArrayList<Bomba>();
		for (Bomba b : bombasSelecionadas) {
			bombas.add(b);
		}
		irrigante.setBombas(bombas);
	}
	
	public void carregarAreas() {
		if(distrito==null){
			areas = null;
		}else{
			ArrayList<Distrito> dists = new ArrayList<Distrito>();
			dists.add(distrito);
			irrigante.getArea().setDistrito(distrito);
			areas = areaService.pesquisar(dists);
		}
	}  

	public void gravar(ActionEvent actionEvent) {
		irrigante = irriganteService.salvar(irrigante); 
		if (operacao.equals("NOVO")) {
			irrigante = null;
			infoMsg(ResourceBundle.getMessage("irriganteSalvar")+ ".", MSG_TITULO);
		}else{
			infoMsg(ResourceBundle.getMessage("irriganteAlterar")+ ".", MSG_TITULO);
		}
    }

	public String voltar() {
		if (getUsuarioLogado().getPerfil().getTipo().equals(TipoPerfil.Irrigante)) {
			return "/homeIrrigante?faces-redirect=true";
		} else {
			return "consultarIrrigante?faces-redirect=true";
		}
	}
	
	public Irrigante getIrrigante() {
		return irrigante;
	}

	public void setIrrigante(Irrigante irrigante) {
		this.irrigante = irrigante;
	}

	public TipoIrrigante[] getTipos() {
		return TipoIrrigante.values();
	}

	public Escolaridade[] getEscolaridades() {
		return Escolaridade.values();
	}

	public OperadoraCelular[] getOperadoras() {
		return OperadoraCelular.values();
	}

	public List<Distrito> getDistritos() {
		return distritos;
	}

	public void setDistritos(List<Distrito> distritos) {
		this.distritos = distritos;
	}

	public List<Area> getAreas() {
		return areas;
	}

	public void setAreas(List<Area> areas) {
		this.areas = areas;
	}
	
	public List<Perfil> getPerfis() {
		return perfis;
	}

	public void setPerfis(List<Perfil> perfis) {
		this.perfis = perfis;
	}

	public Distrito getDistrito() {
		return distrito;
	}

	public void setDistrito(Distrito distrito) {
		this.distrito = distrito;
	}

	public List<Bomba> getBombas() {
		return bombas;
	}

	public void setBombas(List<Bomba> bombas) {
		this.bombas = bombas;
	}

	public Bomba[] getBombasSelecionadas() {
		return bombasSelecionadas;
	}

	public void setBombasSelecionadas(Bomba[] bombasSelecionadas) {
		this.bombasSelecionadas = bombasSelecionadas;
	}

	public String getOperacao() {
		return operacao;
	}

	public void setOperacao(String operacao) {
		this.operacao = operacao;
	}

	public Boolean getIsIrrigante() {
		return isIrrigante;
	}

	public void setIsIrrigante(Boolean isIrrigante) {
		this.isIrrigante = isIrrigante;
	}
}
