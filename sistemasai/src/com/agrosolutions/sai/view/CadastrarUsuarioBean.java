package com.agrosolutions.sai.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.event.ActionEvent;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.Distrito;
import com.agrosolutions.sai.model.Perfil;
import com.agrosolutions.sai.model.TipoPerfil;
import com.agrosolutions.sai.model.Usuario;
import com.agrosolutions.sai.service.DistritoService;
import com.agrosolutions.sai.service.PerfilService;
import com.agrosolutions.sai.service.UsuarioService;
import com.agrosolutions.sai.util.ResourceBundle;

/**
* Use case : Cadastrar Usuario
* 
* @since   : Jan 05, 2012, 06:23:20 PM
* @author  : raphael.ferreira@ivia.com.br
*/

@Component("cadastrarUsuarioBean")
@Scope("session")
public class CadastrarUsuarioBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = -821466037704377491L;

	public static Logger logger = Logger.getLogger(CadastrarUsuarioBean.class);

	public static final String MSG_TITULO = "Cadastro de Usu�rio";

	@Autowired
	private UsuarioService usuarioService;
	@Autowired
	private PerfilService perfilService;
	@Autowired
	private DistritoService distritoService;
	
	private Usuario usuario;
	private List<Distrito> distritos;
	private List<Perfil> perfis;
	private Distrito[] distritosSelecionadas;
	private String operacao;
	
	public void carregarDadosIniciais() {
		operacao = getOperacao();
		usuario = getUsuario();
		//Se for um novo e ainda tiver nulo criar objeto.
		if (operacao.equals("NOVO") && usuario == null) {
			usuario = new Usuario();
		}
		
		if ((operacao.equals("ALTERAR") || operacao.equals("VER")) && usuario != null) {
			if(usuario.getDistritos() != null){
				int i = 0;
				distritosSelecionadas = new Distrito[usuario.getDistritos().size()];
				for (Distrito reg : usuario.getDistritos()) {
					distritosSelecionadas[i++] = reg;
				}
			}
			if(usuario.getConfirmarEmail() == null) {
				usuario.setConfirmarEmail(usuario.getEmail());
			}
		}

		if (operacao.equals("ALTERAR") || operacao.equals("NOVO")){		
			distritos = distritoService.obterTodos(super.getDistritosPesquisa());
			perfis = perfilService.porTipo(TipoPerfil.Administrativo);
		}
	}
	
	public void gravar(ActionEvent actionEvent) {
		if (operacao.equals("NOVO")) usuario.setAtivo(true);
		preparaDistritos();
		usuario = usuarioService.salvar(usuario);
		
		if (operacao.equals("NOVO")) {
			usuario = null;
			distritosSelecionadas = null;
			infoMsg(ResourceBundle.getMessage("usuarioSalvar")+ ".", MSG_TITULO);
		}else{
			infoMsg(ResourceBundle.getMessage("usuarioAlterar")+ ".", MSG_TITULO);
		}
    }

	private void preparaDistritos() {
		if (distritosSelecionadas.length > 0) {
			List<Distrito> regs  = new ArrayList<Distrito>();
			for (int i = 0; i < distritosSelecionadas.length; i++) {
				regs.add(distritosSelecionadas[i]);
			}
			usuario.setDistritos(regs);
		}
	}

	public String voltar() {
		return "consultarUsuario?faces-redirect=true";
	}
	
	public Usuario getUsuario() {
		if (usuario == null) {
			usuario = (Usuario) getFlashScope().get("usuario");
		}
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
		getFlashScope().put("usuario", usuario);

	}

	public List<Distrito> getDistritos() {
		return distritos;
	}

	public void setDistritos(List<Distrito> distritos) {
		this.distritos = distritos;
	}

	public Distrito[] getDistritosSelecionadas() {
		return distritosSelecionadas;
	}

	public void setDistritosSelecionadas(Distrito[] distritosSelecionadas) {
		this.distritosSelecionadas = distritosSelecionadas;
	}

	public List<Perfil> getPerfis() {
		return perfis;
	}

	public void setPerfis(List<Perfil> perfis) {
		this.perfis = perfis;
	}

	public String getOperacao() {
		if (operacao == null) {
			operacao = (String) getFlashScope().get("operacao");
		}
		return operacao;
	}

	public void setOperacao(String operacao) {
		this.operacao = operacao;
		getFlashScope().put("operacao", operacao);
	}
}
