package com.agrosolutions.sai.view;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.faces.model.SelectItem;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.AtividadeEconomica;
import com.agrosolutions.sai.model.BaciaHidrografica;
import com.agrosolutions.sai.model.Cultura;
import com.agrosolutions.sai.model.Municipio;
import com.agrosolutions.sai.model.Variedade;
import com.agrosolutions.sai.service.AtividadeEconomicaService;
import com.agrosolutions.sai.service.CulturaService;
import com.agrosolutions.sai.service.EstadioService;
import com.agrosolutions.sai.service.ReferenciaService;
import com.agrosolutions.sai.service.VariedadeService;
import com.agrosolutions.sai.util.ResourceBundle;

/**
* Use case : Consultar AtividadeEconomica
* 
* @since   : Jan 26, 2016, 03:23:20 AM
* @author  : raphael@iasoft.me
*/

@Component("consultarAtividadeEconomicaBean")
@Scope("session")
public class ConsultarAtividadeEconomicaBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = -8858888777453532539L;

	public static Logger logger = Logger.getLogger(ConsultarAtividadeEconomicaBean.class);

	@Autowired
	private AtividadeEconomicaService atividadeEconomicaService;
	@Autowired
	private CulturaService culturaService;
	@Autowired
	private VariedadeService variedadeService;
	@Autowired
	private ReferenciaService referenciaService;
	@Autowired
	private EstadioService estadioService;
	
	private List<Cultura> listaCulturas;
	private Municipio municipio;
	private BaciaHidrografica bacia;
	private List<AtividadeEconomica> atividadeEconomicas;
	private AtividadeEconomica atividadeEconomicaSelecionado;
	private SelectItem[] culturas;
	private SelectItem[] variedades;
	private String operacao;
	private String nome;
	private Boolean modoPlanilha;
	
	public void carregarDadosIniciais() {
		if (modoPlanilha == null){
			modoPlanilha= false;
		}
		if (atividadeEconomicas ==  null){
			if (operacao.equals("MUNICIPIO") && municipio != null){
				atividadeEconomicas = atividadeEconomicaService.pesquisarPorMunicipio(municipio);
				nome = municipio.getNome();
			}else{
				atividadeEconomicas = atividadeEconomicaService.pesquisarPorBacia(bacia);
				nome = bacia.getNome();
			}
		}
		
		//Popular combobox de filtros
		List<Cultura> culturas = atividadeEconomicaService.pesquisarCulturas(municipio);
		this.culturas = criarFiltroCulturas(culturas);
		List<Variedade> variedades = atividadeEconomicaService.pesquisarVariedades(municipio);
		this.variedades = criarFiltroVariedades(variedades);
		listaCulturas = culturaService.obterTodos();

	}

	public void adicionarLinhas() {
		if (operacao.equals("BACIA")) {
			atividadeEconomicas.add(new AtividadeEconomica(bacia));
		} else {
			atividadeEconomicas.add(new AtividadeEconomica(municipio));
		}
	}

	public String cadastrarIndicadores() {
		return "cadastrarIndicador?faces-redirect=true";
	}

	public void carregarVariedades(AtividadeEconomica a) {
		if(a.getCultura()==null){
			a.setListaVariedades(null);
		}else{
			a.setListaVariedades(variedadeService.pesquisar(a.getCultura()));
		}
	}

	public void gravar() {
		boolean valido = true;
		List<AtividadeEconomica> listaAtividadeInvalida = new ArrayList<AtividadeEconomica>();
		for (AtividadeEconomica atividadeEconomica : atividadeEconomicas) {
			if (atividadeEconomica.getCultura()== null && atividadeEconomica.getVariedade()== null && atividadeEconomica.getReferencia()== null && atividadeEconomica.getDataInicio() == null && atividadeEconomica.getAreatotal().equals(new BigDecimal(0)) && atividadeEconomica.getQtdDiasCultivo()== 0L){
				listaAtividadeInvalida.add(atividadeEconomica);
			}else{
				if (atividadeEconomica.getCultura()== null){
					errorMsg(ResourceBundle.getMessage("culturaNaoInformada"), null);
					valido = false;
				}	
				if (atividadeEconomica.getVariedade()== null){
					errorMsg(ResourceBundle.getMessage("variedadeNaoInformada"), null);
					valido = false;
				}	
				if (atividadeEconomica.getAreatotal().equals(new BigDecimal(0))){
					errorMsg(ResourceBundle.getMessage("areaTotalNaoInformada"), null);
					valido = false;
				}	
				if (atividadeEconomica.getQtdDiasCultivo() == 0L){
					errorMsg(ResourceBundle.getMessage("cicloNaoInformada"), null);
					valido = false;
				}	
				if (atividadeEconomica.getDataInicio() == null){
					errorMsg(ResourceBundle.getMessage("dataInicioNaoInformada"), null);
					valido = false;
				}
				if (valido){
					atividadeEconomica = atividadeEconomicaService.salvar(atividadeEconomica);
				}

			}
		}
		if (valido){
			infoMsg(ResourceBundle.getMessage("atividadesSalva"), null);
			modoPlanilha = false;
			atividadeEconomicas =  null;
		}
	}

	public void carregarReferencias(AtividadeEconomica a) {
		if(a.getVariedade()==null){
			a.setListaReferencias(null);
		}else{
			a.setListaReferencias(referenciaService.pesquisar(a.getVariedade()));
		}
	}

	private SelectItem[] criarFiltroCulturas(List<Cultura> culturas)  {  
        SelectItem[] options = new SelectItem[culturas.size() + 1];  
  
        options[0] = new SelectItem("", "Todas");  
        for(int i = 0; i < culturas.size(); i++) {  
            options[i + 1] = new SelectItem(culturas.get(i).getNome(), culturas.get(i).getNome());  
        }  
  
        return options;  
    }  

	private SelectItem[] criarFiltroVariedades(List<Variedade> variedades)  {  
        SelectItem[] options = new SelectItem[variedades.size() + 1];  
  
        options[0] = new SelectItem("", "Todas");  
        for(int i = 0; i < variedades.size(); i++) {  
            options[i + 1] = new SelectItem(variedades.get(i).getNome(), variedades.get(i).getNome());  
        }  
  
        return options;  
    }  

	public String cadastro() {
		return "cadastrarAtividadeEconomica?faces-redirect=true";
	}
	
	
	public void planilha() {
		logger.debug("inicio do modo planilha" + modoPlanilha );
		modoPlanilha = true;
		if (!atividadeEconomicas.isEmpty()) {
			for (AtividadeEconomica atividadeEconomica : atividadeEconomicas) {
				if(atividadeEconomica.getListaVariedades() == null){
					atividadeEconomica.setListaVariedades(variedadeService.pesquisar(atividadeEconomica.getCultura()));
				}
				if(atividadeEconomica.getListaReferencias() == null){
					atividadeEconomica.setListaReferencias(referenciaService.pesquisar(atividadeEconomica.getVariedade()));
				}

			}
			
		}
		if (atividadeEconomicas.size() < 20){
			for (int i = 0; i < (20-atividadeEconomicas.size()); i++) {
				if (operacao.equals("BACIA")) {
					atividadeEconomicas.add(new AtividadeEconomica(bacia));
				} else {
					atividadeEconomicas.add(new AtividadeEconomica(municipio));
				}
			}
		}
	}

	public void normal() {
		logger.debug("inicio do modo normal" + modoPlanilha );
		modoPlanilha = false;
		atividadeEconomicas = null;
	}

	public String voltar() {
		modoPlanilha = false;
		if (operacao.equals("BACIA")){
			return "consultarBaciaHidrografica?faces-redirect=true";
		}else{
			return "consultarMunicipio?faces-redirect=true";
		}
	}

	public void excluir(){
	    atividadeEconomicaService.excluir(atividadeEconomicaSelecionado);
		if (modoPlanilha){
			atividadeEconomicas.remove(atividadeEconomicaSelecionado);
		}else{
		    atividadeEconomicas = null;
		}
	    atividadeEconomicaSelecionado = null;
		infoMsg(ResourceBundle.getMessage("atividadeEconomicaExcluir")+ ".", null);
   }  	
	
	public Municipio getMunicipio() {
		return municipio;
	}

	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}

	public List<AtividadeEconomica> getAtividadeEconomicas() {
		return atividadeEconomicas;
	}

	public void setAtividadeEconomicas(List<AtividadeEconomica> atividadeEconomicas) {
		this.atividadeEconomicas = atividadeEconomicas;
	}

	public SelectItem[] getCulturas() {
		return culturas;
	}

	public void setCulturas(SelectItem[] culturas) {
		this.culturas = culturas;
	}

	public SelectItem[] getVariedades() {
		return variedades;
	}

	public void setVariedades(SelectItem[] variedades) {
		this.variedades = variedades;
	}

	public AtividadeEconomica getAtividadeEconomicaSelecionado() {
		return atividadeEconomicaSelecionado;
	}

	public void setAtividadeEconomicaSelecionado(AtividadeEconomica atividadeEconomicaSelecionado) {
		this.atividadeEconomicaSelecionado = atividadeEconomicaSelecionado;
	}

	public BaciaHidrografica getBacia() {
		return bacia;
	}

	public void setBacia(BaciaHidrografica bacia) {
		this.bacia = bacia;
	}

	public String getOperacao() {
		return operacao;
	}

	public void setOperacao(String operacao) {
		this.operacao = operacao;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Boolean getModoPlanilha() {
		return modoPlanilha;
	}

	public void setModoPlanilha(Boolean modoPlanilha) {
		this.modoPlanilha = modoPlanilha;
	}

	public List<Cultura> getListaCulturas() {
		return listaCulturas;
	}

	public void setListaCulturas(List<Cultura> listaCulturas) {
		this.listaCulturas = listaCulturas;
	}

}
