
package com.agrosolutions.sai.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.faces.bean.RequestScoped;

import org.apache.log4j.Logger;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.Fabricante;
import com.agrosolutions.sai.model.Filtro;
import com.agrosolutions.sai.model.TipoFiltro;
import com.agrosolutions.sai.service.FabricanteService;
import com.agrosolutions.sai.service.FiltroService;
import com.agrosolutions.sai.util.ResourceBundle;

/**
* Use case : Consultar Filtro
* 
* @since   : Abr 12, 2012, 10:23:20 AM
* @author  : rpf1404@gmail.com
*/

@Component("consultarFiltroBean")
@RequestScoped
public class ConsultarFiltroBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = 2230366396353590501L;

	public static Logger logger = Logger.getLogger(ConsultarFiltroBean.class);

	public static final String MSG_TITULO = "Consultar Filtros";

	@Autowired
	private FiltroService filtroService;
	@Autowired
	private FabricanteService fabricanteService;
	
	private LazyDataModel<Filtro> filtros;
	private Filtro filtroSelecionada;
	private Filtro filtroBusca;
	private List<Fabricante> fabricantes;

	public void carregarDadosIniciais() {
		if(filtroBusca == null){
			filtroBusca = new Filtro();
			filtroBusca.setSortField("modelo");
			filtroBusca.setSortOrder(SortOrder.ASCENDING);
			
			consultar();
		}
	}

	public void consultar(){
		filtros = new LazyDataModel<Filtro>() {
			private static final long serialVersionUID = -832336520014337834L;

			@Override
			public List<Filtro> load(int first, int pageSize, String sortField,
					SortOrder sortOrder, Map<String, String> filters){ 
				List<Filtro> filtros = new ArrayList<Filtro>();

				if (sortField == null) {
					sortField = "modelo";
					sortOrder = SortOrder.ASCENDING;
				}

				filtroBusca.setSortField(sortField);
				filtroBusca.setSortOrder(sortOrder);
				filtros = filtroService.pesquisar(filtroBusca, first, pageSize);

				return filtros;
			}
		};
		int quantidade = filtroService.quantidade(filtroBusca);
		filtros.setRowCount(quantidade == 0 ? 1 : quantidade);
	}	
	
	public void excluir(){
	   filtroService.excluir(filtroSelecionada);
	   //setFiltros(filtroService.obterTodos());
		infoMsg(ResourceBundle.getMessage("filtroExcluir")+ ".","");
   }

   public String cadastro() {
		return "cadastrarFiltro?faces-redirect=true";
	}

	public Filtro getFiltroSelecionada() {
		return filtroSelecionada;
	}
	
	public void setFiltroSelecionada(Filtro filtroSelecionada) {
		this.filtroSelecionada = filtroSelecionada;
	}
	
	public void setFiltros(LazyDataModel<Filtro> filtros) {
		this.filtros = filtros;
	}

	public LazyDataModel<Filtro> getFiltros() {
		return filtros;
	}

	public void setFiltroBusca(Filtro filtroBusca) {
		this.filtroBusca = filtroBusca;
	}

	public Filtro getFiltroBusca() {
		return filtroBusca;
	}
	
	public TipoFiltro[] getTipos() {
		return TipoFiltro.values();
	}

	public void setFabricantes(List<Fabricante> fabricantes) {
		this.fabricantes = fabricantes;
	}

	public List<Fabricante> getFabricantes() {
		return fabricantes;
	}
}
