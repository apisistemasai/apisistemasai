package com.agrosolutions.sai.view;

import java.util.List;

import javax.faces.model.ListDataModel;

import org.primefaces.model.SelectableDataModel;

import com.agrosolutions.sai.model.Filtro;
  
public class FiltroDataModel extends ListDataModel<Filtro> implements SelectableDataModel<Filtro> {    
  
    public FiltroDataModel() {  
    }  
  
    public FiltroDataModel(List<Filtro> data) {  
        super(data);  
    }  
      
    @Override  
    public Filtro getRowData(String rowKey) {  
          
        @SuppressWarnings("unchecked")
		List<Filtro> datas = (List<Filtro>) getWrappedData();  
          
        for(Filtro filtro : datas) {  
            if(filtro.getId().equals(rowKey))  
                return filtro;  
        }  
          
        return null;  
    }  
  
    @Override  
    public Object getRowKey(Filtro filtro) {  
        return filtro.getId();  
    }  
}  
                     