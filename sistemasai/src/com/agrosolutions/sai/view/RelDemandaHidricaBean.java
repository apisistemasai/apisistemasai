package com.agrosolutions.sai.view;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.primefaces.model.StreamedContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.exception.SaiException;
import com.agrosolutions.sai.model.Cultura;
import com.agrosolutions.sai.service.CulturaService;
import com.agrosolutions.sai.service.IrriganteService;
import com.agrosolutions.sai.util.DateUtils;
import com.agrosolutions.sai.util.RelatorioUtil;
import com.agrosolutions.sai.util.ResourceBundle;

/**
* Use case : Relatorio de Demanda Hidrica
* 
* @since   : Mar 19, 2013, 20:23:20 PM
* @author  : rpf1404@gmail.com
*/

@Component("relDemandaHidricaBean")
@Scope("session")
public class RelDemandaHidricaBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = 2000210947938981045L;

	public static Logger logger = Logger.getLogger(RelDemandaHidricaBean.class);

	@Autowired
	private RelatorioUtil relatorioUtil;
	@Autowired
	private CulturaService culturaService;
	@Autowired
	private IrriganteService irriganteService;

	private Date dataIni;
	private Date dataFim;
	private String opcao = new String("S");
	private List<Cultura> culturas;
	private String localizacao;
	private Cultura cultura;
	private StreamedContent file;
	
	public String carregar(){
		this.culturas = culturaService.obterTodos();
		return null;
	}
	   
	public String imprimir() {
		boolean erro = false;
		try {

			// validando campos da entidade
			if ( getDataIni() == null ){
				errorMsg(ResourceBundle.getMessage("demandaDataI")+ ".", null);
				erro = true;
			}
			if ( getDataFim() == null ){
				errorMsg(ResourceBundle.getMessage("demandaDataF")+ ".", null);
				erro = true;
			}
			if (!erro && getDataIni().after(getDataFim())){
				errorMsg(ResourceBundle.getMessage("demandaDataM")+ ".", null);
				erro = true;
			}
			if ( !localizacao.isEmpty() ){
				try {
					irriganteService.pesquisarPorLote(localizacao, super.getDistritosPesquisa());
				} catch (SaiException e) {
					errorMsg(e.getMessage(), null);
					erro = true;
				}
			}

			if (erro) {
				return null;
			}

			Map<String, Object> parametros = new HashMap<String, Object>();
			
			parametros.put("dtIni", "'" + DateUtils.toString(getDataIni()) + "'");
			parametros.put("dtFim", "'" + DateUtils.toString(getDataFim()) + "'");
			parametros.put("cultura", cultura==null?null:cultura.getId());
			parametros.put("culturaNome", cultura==null?"Todas":cultura.getNome());
			parametros.put("listaDistrito", prepararDistritos());
			
			if (opcao.equals("S")) {
				file = relatorioUtil.relatorio("rptIrrigacao_HidricoSintetico.jasper", parametros, "DemandaHidricaSintetico.pdf");
			}else{
				parametros.put("lote", localizacao.isEmpty()?null:"'" + localizacao + "'");
				file = relatorioUtil.relatorio("rptIrrigacao_Hidrico.jasper", parametros, "DemandaHidrica.pdf");
			}

		} catch (SaiException e) {
			errorMsg(ResourceBundle.getMessage("ocorreuErro")+ ":" + e.getMessage(), null);
			logger.warn("Ocorreu o seguinte erro: " + e.getMessage() + e.getCause().getMessage());
			return null;
		} catch (Exception e) {
			errorMsg(ResourceBundle.getMessage("ocorreuErro")+ ":" + e.getMessage(), null);
			logger.fatal("Ocorreu o seguinte erro: " + e.getMessage() + e.getCause().getMessage());
			return null;
		}
		infoMsg(ResourceBundle.getMessage("demandaRelatorio")+ ".", null);
		return null;
	}

	public Date getDataIni() {
		return dataIni;
	}


	public void setDataIni(Date dataIni) {
		this.dataIni = dataIni;
	}


	public Date getDataFim() {
		return dataFim;
	}


	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}

	public String getOpcao() {
		return opcao;
	}


	public void setOpcao(String opcao) {
		this.opcao = opcao;
	}


	public List<Cultura> getCulturas() {
		return culturas;
	}


	public void setCulturas(List<Cultura> culturas) {
		this.culturas = culturas;
	}


	public String getLocalizacao() {
		return localizacao;
	}


	public void setLocalizacao(String localizacao) {
		this.localizacao = localizacao;
	}

	public Cultura getCultura() {
		return cultura;
	}

	public void setCultura(Cultura cultura) {
		this.cultura = cultura;
	}

	public StreamedContent getFile() {
		return file;
	}

	public void setFile(StreamedContent file) {
		this.file = file;
	}
}
