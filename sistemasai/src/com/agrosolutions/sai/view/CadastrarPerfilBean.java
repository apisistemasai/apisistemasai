package com.agrosolutions.sai.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.event.ActionEvent;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.Funcionalidade;
import com.agrosolutions.sai.model.Perfil;
import com.agrosolutions.sai.model.TipoPerfil;
import com.agrosolutions.sai.service.FuncionalidadeService;
import com.agrosolutions.sai.service.PerfilService;
import com.agrosolutions.sai.util.ResourceBundle;

/**
* Use case : Cadastrar Perfil
* 
* @since   : Jan 11, 2012, 22:00:20 AM
* @author  : rpf1404@gmail.com
*/

@Component("cadastrarPerfilBean")
@Scope("session")
public class CadastrarPerfilBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = -6473172613173927761L;

	public static Logger logger = Logger.getLogger(CadastrarPerfilBean.class);

	public static final String MSG_TITULO = "Cadastro de Perfil";

	@Autowired
	private PerfilService perfilService;
	@Autowired
	private FuncionalidadeService funcionalidadeService;
	
	private Perfil perfil;
	private FuncionalidadeDataModel funcionalidades;
	private Funcionalidade[] funcionalidadesSelecionadas;
	private String operacao;

	public void carregarDadosIniciais() {
		operacao = getOperacao();
		perfil = getPerfil();
		
		if (operacao.equals("NOVO") && perfil == null) {
			perfil = new Perfil();
		}else{
			if(funcionalidadesSelecionadas == null && perfil.getFuncionalidades() !=null){
				int i = 0;
				funcionalidadesSelecionadas = new Funcionalidade[perfil.getFuncionalidades().size()];
				for (Funcionalidade func : perfil.getFuncionalidades()) {
					funcionalidadesSelecionadas[i++] = func;
				}
			}
		}
		if (funcionalidades == null){
			funcionalidades = new FuncionalidadeDataModel(funcionalidadeService.obterTodos());
		}
	}
	
	public void gravar(ActionEvent actionEvent) {
		prepararFuncionalidades();
		perfil = perfilService.salvar(perfil);
		if (operacao.equals("NOVO")) {
			perfil = null;
			funcionalidadesSelecionadas = null;
			infoMsg(ResourceBundle.getMessage("perfilSalvar")+ ".", MSG_TITULO);
		}else{
			infoMsg(ResourceBundle.getMessage("perfilAlterar")+ ".", MSG_TITULO);
		}
    }

	private void prepararFuncionalidades() {
		if (funcionalidadesSelecionadas.length > 0) {
			List<Funcionalidade> funcs  = new ArrayList<Funcionalidade>();
			for (int i = 0; i < funcionalidadesSelecionadas.length; i++) {
				funcs.add(funcionalidadesSelecionadas[i]);
			}
			perfil.setFuncionalidades(funcs);
		}
	}

	public String voltar() {
		return "consultarPerfil?faces-redirect=true";
	}

	public Perfil getPerfil() {
		if (perfil == null) {
			perfil = (Perfil) getFlashScope().get("perfil");
		}
		return perfil;
	}

	public void setPerfil(Perfil perfil) {
		this.perfil = perfil;
		getFlashScope().put("perfil", perfil);
	}

	public FuncionalidadeDataModel getFuncionalidades() {
		return funcionalidades;
	}

	public void setFuncionalidades(FuncionalidadeDataModel funcionalidades) {
		this.funcionalidades = funcionalidades;
	}

	public Funcionalidade[] getFuncionalidadesSelecionadas() {
		return funcionalidadesSelecionadas;
	}

	public void setFuncionalidadesSelecionadas(
			Funcionalidade[] funcionalidadesSelecionadas) {
		this.funcionalidadesSelecionadas = funcionalidadesSelecionadas;
	}

	public TipoPerfil[] getTipos() {
		return TipoPerfil.values();
	}
	public String getOperacao() {
		if (operacao == null) {
			operacao = (String) getFlashScope().get("operacao");
		}
		return operacao;
	}

	public void setOperacao(String operacao) {
		this.operacao = operacao;
		getFlashScope().put("operacao", operacao);
	}
}
