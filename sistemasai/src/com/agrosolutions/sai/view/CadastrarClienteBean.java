package com.agrosolutions.sai.view;

import java.io.Serializable;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.Cliente;
import com.agrosolutions.sai.model.Endereco;
import com.agrosolutions.sai.service.ClienteService;
import com.agrosolutions.sai.util.ResourceBundle;

/**
* Use case : Cadastrar Cliente
* 
* @since   : Nov 28, 2012, 14:20:20 AM
* @author  : janine.freitas@ivia.com.br 
*/

@Component("cadastrarClienteBean")
@Scope("session")
public class CadastrarClienteBean extends ManagerBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1702678886293490678L;

	public static Logger logger = Logger.getLogger(CadastrarClienteBean.class);

	public static final String MSG_TITULO = "Cadastro de Cliente";

	@Autowired
	private ClienteService clienteService;
	
	private Cliente cliente;
	private String operacao;
	
	public void carregarDadosIniciais() {
		if (operacao.equals("NOVO") && cliente == null) {
			cliente = new Cliente();
			
			cliente.setEndereco(new Endereco());
		}
	}
	
	public void gravar() {
		cliente = clienteService.salvar(cliente);
		
		if (operacao.equals("NOVO")) {
			cliente = null;
			infoMsg(ResourceBundle.getMessage("clienteSalvar")+ ".", MSG_TITULO);
		}else{
			infoMsg(ResourceBundle.getMessage("clienteAlterar")+ ".", MSG_TITULO);
		}
    }

	public String voltar() {
		return "consultarClientes?faces-redirect=true";
	}
	
	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public String getOperacao() {
		return operacao;
	}

	public void setOperacao(String operacao) {
		this.operacao = operacao;
	}
		
}
