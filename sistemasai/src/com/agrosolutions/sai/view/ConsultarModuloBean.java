package com.agrosolutions.sai.view;

import java.io.Serializable;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.Modulo;
import com.agrosolutions.sai.service.ModuloService;

/**
* Use case : Consultar Modulo
* 
* @since   : Jan 03, 2016, 03:23:20 AM
* @author  : rpf1404@gmail.com
*/

@Component("consultarModuloBean")
@Scope("view")
public class ConsultarModuloBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = -6955501279442795804L;

	public static Logger logger = Logger.getLogger(ConsultarModuloBean.class);

	@Autowired
	private ModuloService moduloService;
	
	private List<Modulo> modulos;
    
	public void carregarDadosIniciais() {
		setModulos(getUsuarioLogado().getModulos());
	}

	public List<Modulo> getModulos() {
		return modulos;
	}

	public void setModulos(List<Modulo> modulos) {
		this.modulos = modulos;
	}

	public Boolean getTemSAI() {
		return verificarModulo("S@I");
	}

	public Boolean getTemSAIBH() {
		return verificarModulo("S@BH");
	}

	private boolean verificarModulo(String sigla) {
		boolean tem = false;
		for (Modulo modulo : modulos) {
			if (modulo.getSiglas().equals(sigla)){
				tem = true;
				continue;
			}
		}
		return tem;
	}
}
