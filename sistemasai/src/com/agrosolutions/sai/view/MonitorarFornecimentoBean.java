package com.agrosolutions.sai.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.Agua;
import com.agrosolutions.sai.model.Area;
import com.agrosolutions.sai.model.Distrito;
import com.agrosolutions.sai.model.Irrigante;
import com.agrosolutions.sai.service.AguaService;
import com.agrosolutions.sai.service.AreaService;
import com.agrosolutions.sai.util.ResourceBundle;

/**
* Use case : Monitorar Fornecimento de Agua
* 
* @since   : Set 20, 2013, 20:23:20 PM
* @author  : rpf1404@gmail.com
*/

@Component("monitorarFornecimentoBean")
@Scope("session")
public class MonitorarFornecimentoBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = -5797014986349659616L;

	public static Logger logger = Logger.getLogger(MonitorarFornecimentoBean.class);

	@Autowired
	private AguaService aguaService;
	@Autowired
	private AreaService areaService;
	
	
	private LazyDataModel<Agua> aguas;
	private Agua aguaBusca;
	private Agua aguaSelecionada;
	private List<Area> areas;

	
   public void carregarDadosIniciais() {
	   
	   // Reseta o objeto da busca quando muda de distrito
	   if (super.getMultidistrito()) {
		   if( (aguaBusca != null) && (this.aguaBusca.getIrrigante().getArea().getDistrito().getId() != super.getDistritoPadrao().getId()) )
			   aguaBusca = null;
	   }
	   
	   if(aguaBusca == null){
		   aguaBusca = new Agua();
		   aguaBusca.setIrrigante(new Irrigante());
		   
		   //Popular combobox de filtros
		   this.areas = areaService.pesquisar(super.getDistritosPesquisa());
		   consultar();
	   }
	}

	private void preparaBuscaDistrito() {
		Distrito distrito = new Distrito();
		distrito.setId(super.getDistritosPesquisa().get(0).getId());
		Area area = new Area();
		area.setDistrito(distrito);
		aguaBusca.getIrrigante().setArea(area);
	}
   
	public void consultar() {
		if (aguaBusca.getIrrigante().getArea() == null) {
			preparaBuscaDistrito();
		}
		aguas = new LazyDataModel<Agua>() {

			private static final long serialVersionUID = -3352970271239830806L;

			@Override
			public List<Agua> load(int first, int pageSize, String sortField,
					SortOrder sortOrder, Map<String, String> filters) {
				List<Agua> aguas = new ArrayList<Agua>();

		        if(sortField == null){
					sortField = "data";
					sortOrder = SortOrder.ASCENDING;
				}
				if (aguaBusca.getIrrigante().getArea() == null) {
					preparaBuscaDistrito();
				}
		        
				aguaBusca.setSortField(sortField);
				aguaBusca.setSortOrder(sortOrder);
				aguas = aguaService.pesquisar(aguaBusca, first, pageSize);

				return aguas;
			}
        };
        aguas.setRowCount(aguaService.quantidade(aguaBusca));
	}
	
	public void excluir() {
		if (getAguaSelecionada() == null) {
			infoMsg(ResourceBundle.getMessage("aguaSelecionada") + ".", null);
		} else {
			aguaService.excluir(getAguaSelecionada());
			infoMsg(ResourceBundle.getMessage("aguaExcluir") + ".", null);
		}
	}

	public String cadastro() {
		return "cadastrarFornecimento?faces-redirect=true";
	}

	public LazyDataModel<Agua> getAguas() {
		return aguas;
	}
	
	public void setAguas(LazyDataModel<Agua> aguas) {
		this.aguas = aguas;
	}
	
	public Agua getAguaBusca() {
		return aguaBusca;
	}
	
	public void setAguaBusca(Agua aguaBusca) {
		this.aguaBusca = aguaBusca;
	}
	
	public Agua getAguaSelecionada() {
		return aguaSelecionada;
	}
	
	public void setIrrigacaoSelecionado(Agua aguaSelecionada) {
		this.setAguaSelecionada(aguaSelecionada);
	}

	public void setAreas(List<Area> areas) {
		this.areas = areas;
	}

	public List<Area> getAreas() {
		return areas;
	}

	public void setAguaSelecionada(Agua aguaSelecionada) {
		this.aguaSelecionada = aguaSelecionada;
	}
	
}
