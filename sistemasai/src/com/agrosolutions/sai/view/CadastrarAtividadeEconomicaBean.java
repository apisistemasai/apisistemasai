package com.agrosolutions.sai.view;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.faces.event.ActionEvent;

import org.apache.log4j.Logger;
import org.primefaces.event.DateSelectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.AtividadeEconomica;
import com.agrosolutions.sai.model.Cultura;
import com.agrosolutions.sai.model.Municipio;
import com.agrosolutions.sai.model.Referencia;
import com.agrosolutions.sai.model.Variedade;
import com.agrosolutions.sai.service.AtividadeEconomicaService;
import com.agrosolutions.sai.service.CulturaService;
import com.agrosolutions.sai.service.EstadioService;
import com.agrosolutions.sai.service.ReferenciaService;
import com.agrosolutions.sai.service.VariedadeService;
import com.agrosolutions.sai.util.ResourceBundle;

/**
* Use case : Cadastrar AtividadeEconomica
* 
* @since   : Jan 26, 2016, 03:45:20 AM
* @author  : raphael@iasoft.me
*/

@Component("cadastrarAtividadeEconomicaBean")
@Scope("session")
public class CadastrarAtividadeEconomicaBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = 7185168325145790563L;

	public static Logger logger = Logger.getLogger(CadastrarAtividadeEconomicaBean.class);

	@Autowired
	private AtividadeEconomicaService atividadeEconomicaService;
	@Autowired
	private CulturaService culturaService;
	@Autowired
	private VariedadeService variedadeService;
	@Autowired
	private ReferenciaService referenciaService;
	@Autowired
	private EstadioService estadioService;
	
	private AtividadeEconomica atividadeEconomica;
	private Municipio municipio;
	private List<Cultura> culturas;
	private List<Variedade> variedades;
	private List<Referencia> referencias;
	private String operacao;

	public void carregarDadosIniciais() {
		if (operacao.equals("NOVO") && atividadeEconomica == null) {
			atividadeEconomica = new AtividadeEconomica(municipio);
		}
		
		if ((operacao.equals("ALTERAR") || operacao.equals("VER")) && atividadeEconomica != null) {
			if(variedades == null){
				variedades = variedadeService.pesquisar(atividadeEconomica.getCultura());
			}
			if(referencias == null){
				referencias = referenciaService.pesquisar(atividadeEconomica.getVariedade());
				atividadeEconomica.getReferencia().setEstadios(estadioService.pesquisar(atividadeEconomica.getReferencia()));
			}
		}
		
		culturas = culturaService.obterTodos();
		
	}
	
	public void carregarVariedades() {
		if(atividadeEconomica.getCultura()==null){
			variedades = null;
		}else{
			variedades = variedadeService.pesquisar(atividadeEconomica.getCultura());
		}
		atividadeEconomica.setVariedade(null);
	}
	
	public void carregarReferencias() {
		if(atividadeEconomica.getVariedade()==null){
			referencias = null;
		}else{
			referencias = referenciaService.pesquisar(atividadeEconomica.getVariedade());
		}
		atividadeEconomica.setReferencia(null);
	}

	
	public void mudarData(DateSelectEvent event) {
		atividadeEconomica.setDataInicio(event.getDate());
	}

	public void gravar(ActionEvent actionEvent) {
		boolean valido = true;
		if (atividadeEconomica.getCultura()== null){
			errorMsg(ResourceBundle.getMessage("culturaNaoInformada"), null);
			valido = false;
		}	
		if (atividadeEconomica.getVariedade()== null){
			errorMsg(ResourceBundle.getMessage("variedadeNaoInformada"), null);
			valido = false;
		}	
		if (atividadeEconomica.getAreatotal().equals(new BigDecimal(0))){
			errorMsg(ResourceBundle.getMessage("areaTotalNaoInformada"), null);
			valido = false;
		}	
		if (atividadeEconomica.getQtdDiasCultivo() == 0L){
			errorMsg(ResourceBundle.getMessage("cicloNaoInformada"), null);
			valido = false;
		}	
		if (atividadeEconomica.getDataInicio() == null){
			errorMsg(ResourceBundle.getMessage("dataInicioNaoInformada"), null);
			valido = false;
		}
		if (valido){
			atividadeEconomica = atividadeEconomicaService.salvar(atividadeEconomica);
			if (operacao.equals("NOVO")) {
				atividadeEconomica = null;
				infoMsg(ResourceBundle.getMessage("atividadeEconomicaSalvar")+ ".", null);
			}else{
				infoMsg(ResourceBundle.getMessage("atividadeEconomicaAlterar")+ ".", null);
			}
		}
    }

	public String voltar() {
		return "consultarAtividadesEconomica?faces-redirect=true";
	}
	
	public Municipio getMunicipio() {
		return municipio;
	}

	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}

	public AtividadeEconomica getAtividadeEconomica() {
		return atividadeEconomica;
	}

	public void setAtividadeEconomica(AtividadeEconomica atividadeEconomica) {
		this.atividadeEconomica = atividadeEconomica;
	}

	public List<Cultura> getCulturas() {
		return culturas;
	}

	public void setCulturas(List<Cultura> culturas) {
		this.culturas = culturas;
	}

	public List<Variedade> getVariedades() {
		return variedades;
	}

	public void setVariedades(List<Variedade> variedades) {
		this.variedades = variedades;
	}

	public String getOperacao() {
		return operacao;
	}

	public void setOperacao(String operacao) {
		this.operacao = operacao;
	}

	public List<Referencia> getReferencias() {
		return referencias;
	}

	public void setReferencias(List<Referencia> referencias) {
		this.referencias = referencias;
	}

}
