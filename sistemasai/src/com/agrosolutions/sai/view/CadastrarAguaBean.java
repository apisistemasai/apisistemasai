package com.agrosolutions.sai.view;

import java.io.Serializable;
import java.util.Date;

import javax.faces.event.ActionEvent;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.Agua;
import com.agrosolutions.sai.model.Irrigante;
import com.agrosolutions.sai.service.AguaService;
import com.agrosolutions.sai.util.ResourceBundle;

/**
* Use case : Cadastrar Agua
* 
* @since   : Julho 05, 2012, 14:00:00 PM
* @author  : walljr@gmail.com
*/

@Component("cadastrarAguaBean")
@Scope("session")
public class CadastrarAguaBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = 9169747384998115749L;

	public static Logger logger = Logger.getLogger(CadastrarAguaBean.class);

	public static final String MSG_TITULO = "Cadastro de Agua";

	@Autowired
	private AguaService aguaService;
	
	private Agua agua;
	private Irrigante irrigante;
	private String operacao;
	
	public void carregarDadosIniciais() {
		operacao = getOperacao();
		
		if (agua == null) {
			agua = new Agua(new Date());	
			agua.setIrrigante(irrigante);
		}
	}
	
   public void gravar(ActionEvent actionEvent) {
	   agua = aguaService.salvar(agua); 
	   agua = null;
	   infoMsg(ResourceBundle.getMessage("aguaSalvar")+ ".", null);
    }

	public String voltar() {
		return "consultarAgua?faces-redirect=true";
	}

	public String getOperacao() {
		if (operacao == null) {
			operacao = (String) getFlashScope().get("operacao");
		}
		return operacao;
	}

	public void setOperacao(String operacao) {
		this.operacao = operacao;
		getFlashScope().put("operacao", operacao);
	}

	public AguaService getAguaService() {
		return aguaService;
	}

	public void setAguaService(AguaService aguaService) {
		this.aguaService = aguaService;
	}

	public Agua getAgua() {
		return agua;
	}

	public void setAgua(Agua agua) {
		this.agua = agua;
	}

	public Irrigante getIrrigante() {
		return irrigante;
	}

	public void setIrrigante(Irrigante irrigante) {
		this.irrigante = irrigante;
	}

}
