package com.agrosolutions.sai.view;

import java.io.Serializable;
import java.util.List;

import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.Praga;
import com.agrosolutions.sai.service.PragaService;
import com.agrosolutions.sai.util.ResourceBundle;

/**
* Use case : Consultar Praga
* 
* @since   : Jan 27, 2012, 23:23:20 AM
* @author  : raphael.ferreira@ivia.com.br
*/

@Component("consultarPragaBean")
@Scope("view")
public class ConsultarPragaBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = -921419392669459739L;

	public static final String MSG_TITULO = "Consultar Pragas";

	@Autowired
	private PragaService pragaService;
	
	private List<Praga> pragas;
	private Praga pragaSelecionado;
	private Praga pragaBusca;
	
	public void carregarDadosIniciais() {
		if (pragaBusca == null){
			pragaBusca = new Praga();
			pragaBusca.setSortField("nome");
			pragaBusca.setSortOrder(SortOrder.ASCENDING);
			
			consultar();
		}
	}
	
	public void consultar(){
		pragas = pragaService.pesquisar(pragaBusca, null, null);
	}

	public String cadastro() {
		return "cadastrarPraga?faces-redirect=true";
	}

	public void excluir(){
		pragaService.excluir(pragaSelecionado);
		consultar();
		infoMsg(ResourceBundle.getMessage("pragaExcluir")+ ".","");
   	}

	public List<Praga> getPragas() {
		return pragas;
	}
	
	public void setPragas(List<Praga> pragas) {
		this.pragas = pragas;
	}
	
	public Praga getPragaSelecionado() {
		return pragaSelecionado;
	}
	
	public void setPragaSelecionado(Praga pragaSelecionado) {
		this.pragaSelecionado = pragaSelecionado;
	}

	public void setPragaBusca(Praga pragaBusca) {
		this.pragaBusca = pragaBusca;
	}

	public Praga getPragaBusca() {
		return pragaBusca;
	}  	

}
