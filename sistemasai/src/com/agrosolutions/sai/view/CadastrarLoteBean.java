/*package com.agrosolutions.sai.view;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.RequestScoped;
import javax.faces.event.ActionEvent;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.Atividade;
import com.agrosolutions.sai.model.Irrigante;
import com.agrosolutions.sai.model.Lote;
import com.agrosolutions.sai.service.AtividadeService;
import com.agrosolutions.sai.service.LoteService;

*//**
* Use case : Cadastrar Lote
* 
* @since   : Jan 06, 2012, 02:00:20 AM
* @author  : raphael.ferreira@ivia.com.br
*//*

@Component("cadastrarLoteBean")
@RequestScoped
public class CadastrarLoteBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = -1744759643193752819L;

	public static Logger logger = Logger.getLogger(CadastrarLoteBean.class);

	public static final String MSG_TITULO = "Cadastro de Lote";

	@Autowired
	private LoteService loteService;
	@Autowired
	private AtividadeService atividadeService;
	
	private Lote lote;
	private Irrigante irrigante;
	private List<Atividade> atividades;
	private Atividade atividade;
	private String operacao;

	public void carregarDadosIniciais() {
		if (operacao.equals("NOVO") && lote == null) {
			lote = new Lote();
			lote.setIrrigante(irrigante);
		}
		if (operacao.equals("ALTERAR") || operacao.equals("NOVO")){		
			//Se tiver atividade preenchida foi recem cadastrada e deve ser setada no objeto.
			if (atividade != null) {
				lote.setAtividade(atividade);
				atividade = null;
			}
		}
		atividades = atividadeService.obterTodos();
	}
	
	public String novaAtividade() {
		return "cadastrarAtividade";
	}

	public void gravar(ActionEvent actionEvent) {
		lote = loteService.salvar(lote);
		if (operacao.equals("NOVO")) {
			lote = null;
			infoMsg("Lote criado com sucesso.", MSG_TITULO);
		}else{
			infoMsg("Lote alterado com sucesso.", MSG_TITULO);
		}
    }

	public String voltar() {
		return "consultarLote?faces-redirect=true";
	}
	
	public Irrigante getIrrigante() {
		return irrigante;
	}

	public void setIrrigante(Irrigante irrigante) {
		this.irrigante = irrigante;
	}

	public Lote getLote() {
		return lote;
	}

	public void setLote(Lote lote) {
		this.lote = lote;
	}

	public List<Atividade> getAtividades() {
		return atividades;
	}

	public void setAtividades(List<Atividade> atividades) {
		this.atividades = atividades;
	}

	public Atividade getAtividade() {
		return atividade;
	}

	public void setAtividade(Atividade atividade) {
		this.atividade = atividade;
	}

	public String getOperacao() {
		return operacao;
	}

	public void setOperacao(String operacao) {
		this.operacao = operacao;
	}
}
*/