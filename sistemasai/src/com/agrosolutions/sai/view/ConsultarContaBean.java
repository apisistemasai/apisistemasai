package com.agrosolutions.sai.view;

import java.io.Serializable;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.Conta;
import com.agrosolutions.sai.service.ContaService;
import com.agrosolutions.sai.util.ResourceBundle;

/**
 * Use case : Consultar Conta
 * 
 * @since : Nov 28, 2012, 22:23:20 AM
 * @author : carlos.torres@ivia.com.br
 */

@Component("consultarContaBean")
@Scope("view")
public class ConsultarContaBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = -5224457405555086564L;

	public static Logger logger = Logger
			.getLogger(ConsultarContaBean.class);

	public static final String MSG_TITULO = "Consultar Conta";

	@Autowired
	private ContaService contaService;

	private List<Conta> contas;
	private Conta contaSelecionada;

	public void carregarDadosIniciais() {
		contas = contaService.obterTodos();
	}

	public void excluir() {
		contaService.excluir(contaSelecionada);
		contas = contaService.obterTodos();
		infoMsg(ResourceBundle.getMessage("contaExcluir")+ ".", MSG_TITULO);
	}

	public String cadastro() {
		return "cadastrarConta?faces-redirect=true";
	}

	public List<Conta> getContas() {
		return contas;
	}

	public void setContas(List<Conta> contas) {
		this.contas = contas;
	}

	public Conta getContaSelecionada() {
		return contaSelecionada;
	}

	public void setContaSelecionada(Conta contaSelecionada) {
		this.contaSelecionada = contaSelecionada;
	}

}
