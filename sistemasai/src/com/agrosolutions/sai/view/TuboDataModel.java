package com.agrosolutions.sai.view;

import java.util.List;

import javax.faces.model.ListDataModel;

import org.primefaces.model.SelectableDataModel;

import com.agrosolutions.sai.model.Tubo;
  
public class TuboDataModel extends ListDataModel<Tubo> implements SelectableDataModel<Tubo> {    
  
    public TuboDataModel() {  
    }  
  
    public TuboDataModel(List<Tubo> data) {  
        super(data);  
    }  
      
    @Override  
    public Tubo getRowData(String rowKey) {  
          
        @SuppressWarnings("unchecked")
		List<Tubo> datas = (List<Tubo>) getWrappedData();  
          
        for(Tubo tubo : datas) {  
            if(tubo.getId().equals(rowKey))  
                return tubo;  
        }  
          
        return null;  
    }  
  
    @Override  
    public Object getRowKey(Tubo tubo) {  
        return tubo.getId();  
    }  
}  
                     