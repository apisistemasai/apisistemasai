package com.agrosolutions.sai.view;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.primefaces.model.StreamedContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.exception.SaiException;
import com.agrosolutions.sai.model.Cultura;
import com.agrosolutions.sai.model.Irrigante;
import com.agrosolutions.sai.service.CulturaService;
import com.agrosolutions.sai.service.IrriganteService;
import com.agrosolutions.sai.util.DateUtils;
import com.agrosolutions.sai.util.RelatorioUtil;
import com.agrosolutions.sai.util.ResourceBundle;

/**
* Use case : Relatorio de Demanda Hidrica
* 
* @since   : Mar 19, 2013, 20:23:20 PM
* @author  : rpf1404@gmail.com
*/

@Component("relDemandaHidricaBaciaBean")
@Scope("session")
public class RelDemandaHidricaBaciaBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = -2517922488516944879L;

	public static Logger logger = Logger.getLogger(RelDemandaHidricaBaciaBean.class);

	@Autowired
	private RelatorioUtil relatorioUtil;
	@Autowired
	private CulturaService culturaService;
	@Autowired
	private IrriganteService irriganteService;

	private Date dataIni;
	private Date dataFim;
	private String opcao = new String("S");
	private List<Irrigante> irrigantes;
	private List<Cultura> culturas;
	private String localizacao;
	private Irrigante irrigante;
	private Cultura cultura;
	private StreamedContent file;
	
	public String carregar(){
		this.setIrrigantes(irriganteService.pesquisarPorDistrito(getDistritosPesquisa() ));
		this.culturas = culturaService.obterTodos();
		return null;
	}
	   
	public String imprimir() {
		boolean erro = false;
		try {

			// validando campos da entidade
			if ( getDataIni() == null ){
				errorMsg(ResourceBundle.getMessage("demandaDataI")+ ".", null);
				erro = true;
			}
			if ( getDataFim() == null ){
				errorMsg(ResourceBundle.getMessage("demandaDataF")+ ".", null);
				erro = true;
			}
			if (!erro && getDataIni().after(getDataFim())){
				errorMsg(ResourceBundle.getMessage("demandaDataM")+ ".", null);
				erro = true;
			}

			if (erro) {
				return null;
			}

			Map<String, Object> parametros = new HashMap<String, Object>();
			
			parametros.put("dtIni", "'" + DateUtils.toStringUS(getDataIni()) + "'");
			parametros.put("dtFim", "'" + DateUtils.toStringUS(getDataFim()) + "'");
			parametros.put("irrigante", getIrrigante()==null?null:getIrrigante().getId());
			parametros.put("cultura", cultura==null?null:cultura.getId());
			parametros.put("listaDistrito", prepararDistritos());
			//parametros.put("culturaNome", cultura==null?"Todas":cultura.getNome());
			
			file = relatorioUtil.relatorio("rptIrrigacao_DemandaHidricaBacia.jasper", parametros, "DemandaHidricaBacia.pdf");

		} catch (SaiException e) {
			errorMsg(ResourceBundle.getMessage("ocorreuErro")+ ":" + e.getMessage(), null);
			logger.warn("Ocorreu o seguinte erro: " + e.getMessage() + e.getCause().getMessage());
			return null;
		} catch (Exception e) {
			errorMsg(ResourceBundle.getMessage("ocorreuErro")+ ":" + e.getMessage(), null);
			logger.fatal("Ocorreu o seguinte erro: " + e.getMessage() + e.getCause().getMessage());
			return null;
		}
		infoMsg(ResourceBundle.getMessage("demandaRelatorio")+ ".", null);
		return null;
	}

	public Date getDataIni() {
		return dataIni;
	}


	public void setDataIni(Date dataIni) {
		this.dataIni = dataIni;
	}


	public Date getDataFim() {
		return dataFim;
	}


	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}

	public String getOpcao() {
		return opcao;
	}


	public void setOpcao(String opcao) {
		this.opcao = opcao;
	}


	public List<Cultura> getCulturas() {
		return culturas;
	}


	public void setCulturas(List<Cultura> culturas) {
		this.culturas = culturas;
	}


	public String getLocalizacao() {
		return localizacao;
	}


	public void setLocalizacao(String localizacao) {
		this.localizacao = localizacao;
	}

	public Cultura getCultura() {
		return cultura;
	}

	public void setCultura(Cultura cultura) {
		this.cultura = cultura;
	}

	public StreamedContent getFile() {
		return file;
	}

	public void setFile(StreamedContent file) {
		this.file = file;
	}

	public Irrigante getIrrigante() {
		return irrigante;
	}

	public void setIrrigante(Irrigante irrigante) {
		this.irrigante = irrigante;
	}

	public List<Irrigante> getIrrigantes() {
		return irrigantes;
	}

	public void setIrrigantes(List<Irrigante> irrigantes) {
		this.irrigantes = irrigantes;
	}
}
