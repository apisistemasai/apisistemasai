package com.agrosolutions.sai.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.event.ActionEvent;

import org.apache.log4j.Logger;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.Emissor;
import com.agrosolutions.sai.model.Irrigante;
import com.agrosolutions.sai.model.MotivoFinalPlantio;
import com.agrosolutions.sai.model.Plantio;
import com.agrosolutions.sai.model.Setor;
import com.agrosolutions.sai.model.TipoCalculoKL;
import com.agrosolutions.sai.model.TipoEmissor;
import com.agrosolutions.sai.model.TipoFertirrigacao;
import com.agrosolutions.sai.model.TipoFormulaGotejo;
import com.agrosolutions.sai.model.TipoFormulaMicro;
import com.agrosolutions.sai.model.TipoManejoIrrigacao;
import com.agrosolutions.sai.service.EmissorService;
import com.agrosolutions.sai.service.PlantioService;
import com.agrosolutions.sai.service.SetorService;
import com.agrosolutions.sai.util.ResourceBundle;

/**
* Use case : Manter Plantio
* 
* @since   : Aug 2, 2012, 10:45:20 AM
* @author  : rpf1404@gmail.com
*/

@Component("manterPlantioBean")
@Scope("session")
public class ManterPlantioBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = -263292341204958897L;

	public static Logger logger = Logger.getLogger(ManterPlantioBean.class);

	@Autowired
	private SetorService setorService;
	@Autowired
	private PlantioService plantioService;
	@Autowired
	private EmissorService emissorService;
	
	private Plantio plantio;
	private Irrigante irrigante;
	private List<Setor> setores;
	private String operacao;
	private Emissor[] emissoresSelecionados;
	private Boolean gotejo;
	private Boolean micro;
	private Boolean gotejoKL;
	
	public void carregarDadosIniciais() {
		if (gotejo==null) {gotejo = false;}
		if (micro==null) {micro = false;}
		setores = setorService.pesquisar(irrigante);
		plantio.setEmissores(emissorService.pesquisarPorPlantio(plantio));
		plantio.setFormula(plantioService.verificarFormula(plantio));
		if(emissoresSelecionados == null){
			if(plantio.getEmissores() != null && !plantio.getEmissores().isEmpty()){
				int i = 0;
				emissoresSelecionados = new Emissor[plantio.getEmissores().size()];
				for (Emissor e : plantio.getEmissores()) {
					emissoresSelecionados[i++] = e;
					verificarEmissor(e);
				}
			}else{
				for (Emissor e : plantio.getSetor().getEmissores()) {
					verificarEmissor(e);
				}

			}
		}
		if(micro && gotejo){
			micro = false;
			gotejo = false;
			if (plantio.getTipoCalculoKL()!=null) {
				gotejoKL = true;
			}
		}

	}
	
	private void verificarEmissor(Emissor e){
		if (e.getTipoEmissor().equals(TipoEmissor.Gotejamento)){
			gotejo = true;
		}
		if (e.getTipoEmissor().equals(TipoEmissor.Microaspersao)){
			micro = true;
		}
		
	}

	public void gravar(ActionEvent actionEvent) {
		List<Emissor> emissores = new ArrayList<Emissor>();
		for (int i = 0; i < emissoresSelecionados.length; i++) {
			emissores.add(emissoresSelecionados[i]);
		}
		
		plantio.setEmissores(emissores);
		plantio.setFormula(plantioService.verificarFormula(plantio));
		plantio = plantioService.definirTipoFormula( plantio );
		plantio = plantioService.salvar(plantio);
		infoMsg(ResourceBundle.getMessage("plantioAlterar")+ ".", null);
	}

	public String voltar() {
		gotejo = null;
		micro = null;
		gotejoKL  = null;
		emissoresSelecionados = null;
		return "consultarPlantio?faces-redirect=true";
	}
	
	public String simular() { 
		return "simularIrrigacao?faces-redirect=true";
	}

	public void emissorSelecionado(SelectEvent event) {
    	verificaEmissor();
    }

    public void emissorDeselecionado(UnselectEvent event) {  
    	verificaEmissor();
    }  
    
	private void verificaEmissor() {
		gotejo = false;
		micro = false;
		gotejoKL = false;
		for (int i = 0; i < emissoresSelecionados.length; i++) {
			verificarEmissor(emissoresSelecionados[i]);
		}
	}  
  
	public void verificarFormulaGotejo() {
		if(plantio.getTipoFormulaGotejo()==null){
			gotejoKL = false;
		}else{
			if(plantio.getTipoFormulaGotejo().equals(TipoFormulaGotejo.AreaCoberturaKl) || plantio.getTipoFormulaGotejo().equals(TipoFormulaGotejo.FaixaKl)){
			gotejoKL = true;
			}
		}
	}  
	
	public void verificarFormulaMicro() {
		if(plantio.getTipoFormulaMicro()==null){
			gotejoKL = false;
		}else{
			if(plantio.getTipoFormulaMicro().equals(TipoFormulaMicro.HasteNormalKL)){
			gotejoKL = true;
			}
		}
	}  

	public Irrigante getIrrigante() {
		return irrigante;
	}

	public void setIrrigante(Irrigante irrigante) {
		this.irrigante = irrigante;
	}

	public TipoFertirrigacao[] getTiposFertirrigacao() {
		return TipoFertirrigacao.values();
	}

	public TipoFormulaGotejo[] getTiposFormulaGotejo() {
		return TipoFormulaGotejo.values();
	}

	public TipoFormulaMicro[] getTiposFormulaMicro() {
		return TipoFormulaMicro.values();
	}

	public TipoCalculoKL[] getTiposCalculoKL() {
		return TipoCalculoKL.values();
	}

	public TipoManejoIrrigacao[] getTiposManejo() {
		return TipoManejoIrrigacao.values();
	}

	public MotivoFinalPlantio[] getMotivoFinalizacao() {
		return MotivoFinalPlantio.values();
	}

	public List<Setor> getSetores() {
		return setores;
	}

	public void setSetores(List<Setor> setores) {
		this.setores = setores;
	}

	public Plantio getPlantio() {
		return plantio;
	}

	public void setPlantio(Plantio plantio) {
		this.plantio = plantio;
	}

	public String getOperacao() {
		return operacao;
	}

	public void setOperacao(String operacao) {
		this.operacao = operacao;
	}

	public Emissor[] getEmissoresSelecionados() {
		return emissoresSelecionados;
	}

	public void setEmissoresSelecionados(Emissor[] emissoresSelecionados) {
		this.emissoresSelecionados = emissoresSelecionados;
	}

	public Boolean getGotejo() {
		return gotejo;
	}

	public void setGotejo(Boolean gotejo) {
		this.gotejo = gotejo;
	}
	public Boolean getMicro() {
		return micro;
	}

	public void setMicro(Boolean micro) {
		this.micro = micro;
	}

	public Boolean getGotejoKL() {
		return gotejoKL;
	}

	public void setGotejoKL(Boolean gotejoKL) {
		this.gotejoKL = gotejoKL;
	}

}
