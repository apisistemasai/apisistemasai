package com.agrosolutions.sai.view;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.faces.model.SelectItem;

import org.apache.log4j.Logger;
import org.primefaces.model.chart.CartesianChartModel;
import org.primefaces.model.chart.ChartSeries;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.AtividadeEconomica;
import com.agrosolutions.sai.model.GraficoBarra;
import com.agrosolutions.sai.model.Indicadores;
import com.agrosolutions.sai.model.PesoIndicadorCiclo;
import com.agrosolutions.sai.model.PesoIndicadores;
import com.agrosolutions.sai.model.PesoIndicadoresSegHidrica;
import com.agrosolutions.sai.service.AtividadeEconomicaService;
import com.agrosolutions.sai.service.IndicadoresService;
import com.agrosolutions.sai.util.BigDecimalUtil;
import com.agrosolutions.sai.util.ResourceBundle;

/**
* Use case : Cadastrar Indicadores
* 
* @since   : Fev 20, 2016, 03:23:20 AM
* @author  : raphael@iasoft.me
*/

@Component("cadastrarIndicadoresBean")
@Scope("view")
public class CadastrarIndicadoresBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = -4133228879239287046L;

	public static Logger logger = Logger.getLogger(CadastrarIndicadoresBean.class);

	@Autowired
	private IndicadoresService indicadoresService;
	@Autowired
	private AtividadeEconomicaService atividadeEconomicaService;
	
	private List<Indicadores> indicadores;
	private Indicadores indicadoresSelecionado;
	private String operacao;
	private SelectItem[] filtroDatas;
	private List<Date> dates;
	private Date data;
	private Boolean calculado;

	private Boolean indicadorKgHa;
	private Boolean indicadorReceitaLiquidaHa;
	private Boolean indicadorM3Ha;
	private Boolean indicadorEmpregosHa;
	private Boolean indicadorKgM3;
	private Boolean indicadorReceitaLiquidaM3;
	private Boolean indicadorEmpregosM3;
	private Boolean indicadorCiclo;	
	
	private Integer p1;
	private Integer p2;
	private Integer p3;
	private Integer p4;
	private Integer p5;
	private Integer p6;
	private Integer p7;
	private Integer p8;

	private CartesianChartModel graficoIndicadoresCorteHidricoBacia;

	
	public void carregarDadosIniciais() {
		if (p1 == null){
			p1 = 1;
			p2 = 1;
			p3 = 1;
			p4 = 1;
			p5 = 1;
			p6 = 1;
			p7 = 1;
			p8 = 1;
			indicadorKgHa = true;
			indicadorReceitaLiquidaHa = true;
			indicadorM3Ha = true;
			indicadorEmpregosHa = true;
			indicadorKgM3 = true;
			indicadorReceitaLiquidaM3 = true;
			indicadorEmpregosM3 = true;
			indicadorCiclo = true;
		}
		
		if (indicadores ==  null){
			setCalculado(false);
			dates = indicadoresService.pesquisarDatas();
			if (dates != null && !dates.isEmpty()){
				filtroDatas = criarFiltroData(dates);
				data = dates.get(0);
				consulta();
			}else{
				novo();
			}
		}
	}

	private CartesianChartModel criarGraficoCorteHidrico(List<Indicadores> indicadores) {
		List<GraficoBarra> retorno = new ArrayList<GraficoBarra>();
		
		for (Indicadores indicador : indicadores) {
			String nome = indicador.getAtividadeEconomica().getCultura().getNome();
			double qtd = new BigDecimal(indicador.getPesoMedio().toString(), BigDecimalUtil.MC).setScale(2, RoundingMode.FLOOR).doubleValue();
			
			GraficoBarra g = new GraficoBarra( nome, qtd, null );
			retorno.add(g);
			
		}

		CartesianChartModel grafico = new CartesianChartModel();
		Collections.sort(retorno);
		montarGraficoBarraHorizontal(grafico, retorno, ResourceBundle.getMessage("corteHidrico"));
		
		return grafico;
	}

	private void montarGraficoBarraHorizontal(CartesianChartModel graficoModel, List<GraficoBarra> graficosBarra, String titulo) {
		if ( !graficosBarra.isEmpty() ) {
			ChartSeries chartSeries = new ChartSeries();
			chartSeries.setLabel( ResourceBundle.getMessage("corteHidrico") );
			
			for (GraficoBarra grafico : graficosBarra) {
				//Calculo Percentual
				double percent = grafico.getValorEixoY()/100;
				BigDecimal percentual = new BigDecimal(percent).setScale(2, RoundingMode.FLOOR);
				
				chartSeries.set( grafico.getValorEixoX() + " - Corte:" + grafico.getValorEixoY() + "%", percentual );
			}
			graficoModel.addSeries(chartSeries);
		} else {
			ChartSeries chartSeries = new ChartSeries();
			chartSeries.setLabel(ResourceBundle.getMessage("semDados"));
			chartSeries.set(0, 0);
	        graficoModel.addSeries(chartSeries);
		}
	}

	public void consulta() {
		indicadores = indicadoresService.pesquisarPorData(data);
		for (Indicadores indicador : indicadores) {
			indicador.setMunicipios(atividadeEconomicaService.pesquisarPorVariedades(indicador.getAtividadeEconomica().getVariedade()));
			BigDecimal areaTotal = calculaAreaTotal(indicador);
			indicador.setAreaTotal(areaTotal);
			if(indicador.getPesoMedio()!= null){
				calculado = true;
			}
		}
		if(calculado){
			graficoIndicadoresCorteHidricoBacia = criarGraficoCorteHidrico(indicadores);
		}
	}

	public void novo() {
		indicadores = new ArrayList<Indicadores>();
		if (verificarData(new Date())){
			List<AtividadeEconomica> atividadesBacia = new ArrayList<AtividadeEconomica>();
			List<AtividadeEconomica> atividadesMunicipios = atividadeEconomicaService.pesquisarTodosDeMunicipio();
			for (AtividadeEconomica atividadeEconomica : atividadesMunicipios) {
				boolean jaExiste = false;
				for (AtividadeEconomica atividadeBacia : atividadesBacia) {
					if(atividadeBacia.getVariedade().equals(atividadeEconomica.getVariedade())){
						jaExiste = true;
					}

				}
				if (!jaExiste){
					AtividadeEconomica atividade = new AtividadeEconomica(atividadeEconomica);
					atividadesBacia.add(atividade);
				}
			}
			for (AtividadeEconomica atividadeEconomica : atividadesBacia) {
				Indicadores indicador = new Indicadores(atividadeEconomica);
				indicador.setData(new Date());
				indicador.setMunicipios(atividadeEconomicaService.pesquisarPorVariedades(atividadeEconomica.getVariedade()));
				BigDecimal areaTotal = calculaAreaTotal(indicador);
				indicador.setAreaTotal(areaTotal);
				atividadeEconomica.setAreatotal(areaTotal);
				indicadores.add(indicador);
			}
		}
	}

	public void retirarMunicipio(AtividadeEconomica a, Boolean tirar, Indicadores i) {
		if(tirar){
			i.setAreaTotal(i.getAreaTotal().subtract(a.getAreatotal()));
			a.setIncluido(false);
		}else{
			i.setAreaTotal(i.getAreaTotal().add(a.getAreatotal()));
			a.setIncluido(true);
		}
	}

	private BigDecimal calculaAreaTotal(Indicadores indicador) {
		BigDecimal areaTotal = new BigDecimal(0);
		for (AtividadeEconomica a : indicador.getMunicipios()) {
			areaTotal = areaTotal.add(a.getAreatotal());
			a.setIncluido(true);
		}
		return areaTotal;
	}

	public void calcularCH() {
		for (Indicadores indicador : indicadores) {

			verificarCH(indicador);
			
		}
		graficoIndicadoresCorteHidricoBacia = criarGraficoCorteHidrico(indicadores);
		infoMsg(ResourceBundle.getMessage("calculado"), null);
		
	}

	private void verificarCH(Indicadores indicador) {
		BigDecimal somaR = new BigDecimal(0).setScale(2);
		int pesos = 0;

		if (indicadorKgM3) {
			somaR = somaR.add(indicador.getPesoKgM3().getValor().multiply(new BigDecimal(p1)));
			pesos = pesos + p1;
		}
		if (indicadorKgHa) {
			somaR = somaR.add(indicador.getPesoKgHa().getValor().multiply(new BigDecimal(p2)));
			pesos = pesos + p2;
		}
		if (indicadorReceitaLiquidaM3) {
			somaR = somaR.add(indicador.getPesoReceitaLiquidaM3().getValor().multiply(new BigDecimal(p3)));
			pesos = pesos + p3;
		}
		if (indicadorReceitaLiquidaHa) {
			somaR = somaR.add(indicador.getPesoReceitaLiquidaHa().getValor().multiply(new BigDecimal(p4)));
			pesos = pesos + p4;
		}
		if (indicadorEmpregosM3) {
			somaR = somaR.add(indicador.getPesoEmpregosM3().getValor().multiply(new BigDecimal(p5)));
			pesos = pesos + p5;
		}
		if (indicadorEmpregosHa) {
			somaR = somaR.add(indicador.getPesoEmpregosHa().getValor().multiply(new BigDecimal(p6)));
			pesos = pesos + p6;
		}
		if (indicadorM3Ha) {
			somaR = somaR.add(indicador.getPesoM3Ha().getValor().multiply(new BigDecimal(p7)));
			pesos = pesos + p7;
		}
		if (indicadorCiclo) {
			somaR = somaR.add(indicador.getCicloCultura().getValor().multiply(new BigDecimal(p8)));
			pesos = pesos + p8;
		}
		
		try {
			indicador.setR(somaR.divide(new BigDecimal(pesos)).setScale(2));
		} catch (ArithmeticException e) {
			indicador.setR(somaR.divide(new BigDecimal(pesos), BigDecimal.ROUND_UP).setScale(2, BigDecimal.ROUND_UP));
		}
		
		indicador.setPesoMedio(new BigDecimal(1).subtract(indicador.getR()));
		indicador.setPesoMedio(new BigDecimal(100).multiply(indicador.getPesoMedio()).setScale(2, BigDecimal.ROUND_UP));
		indicador.setConsumoBacia(indicador.getAreaTotal().multiply(indicador.getM3Ha()).setScale(2, BigDecimal.ROUND_UP));

		
		indicador.setChm3(indicador.getPesoMedio().multiply(indicador.getConsumoBacia()));
		
		try {
			indicador.setChm3(indicador.getChm3().divide(new BigDecimal(100)));
		} catch (ArithmeticException e) {
			indicador.setChm3(indicador.getChm3().divide(new BigDecimal(100), BigDecimal.ROUND_UP));
		}
		indicador.setChm3(indicador.getChm3().setScale(2, BigDecimal.ROUND_UP));

		indicador.setCa(indicador.getPesoMedio().multiply(indicador.getAreaTotal()));

		try {
			indicador.setCa(indicador.getCa().divide(new BigDecimal(100)));
		} catch (ArithmeticException e) {
			indicador.setCa(indicador.getCa().divide(new BigDecimal(100), BigDecimal.ROUND_UP));
		}
		indicador.setCa(indicador.getCa().setScale(2, BigDecimal.ROUND_UP));
		
	}
	public void calcular() {
		BigDecimal totalKgM3 = new BigDecimal(0);
		BigDecimal totalKgHa = new BigDecimal(0);
		BigDecimal totalReceitaLiquidaM3 = new BigDecimal(0);
		BigDecimal totalReceitaLiquidaHa = new BigDecimal(0);
		BigDecimal totalEmpregosM3 = new BigDecimal(0);
		BigDecimal totalEmpregosHa = new BigDecimal(0);
		BigDecimal totalM3Ha = new BigDecimal(0);
		BigDecimal totalCicloAnual = new BigDecimal(0);
		int qtd = 0;
		for (Indicadores indicador : indicadores) {
			
			if (indicador.getM3Ha() == null || indicador.getM3Ha().compareTo(new BigDecimal(0)) == 0) {
				errorMsg(ResourceBundle.getMessage("faltaM3Ha"), null);
				continue;
			}
			if (indicador.getKgHa() == null) {
				errorMsg(ResourceBundle.getMessage("faltaKgHa"), null);
				continue;
			}
			if (indicador.getReceitaLiquidaHa() == null) {
				errorMsg(ResourceBundle.getMessage("faltaReceitaLiquida"), null);
				continue;
			}
			if (indicador.getEmpregosHa() == null) {
				errorMsg(ResourceBundle.getMessage("faltaEmpHa"), null);
				continue;
			}
			if (indicador.getKgM3() == null) {
				errorMsg(ResourceBundle.getMessage("faltaKgM3"), null);
				continue;
			}

			qtd++;
			try {
				indicador.setKgM3(indicador.getKgHa().divide(indicador.getM3Ha()));
			} catch (ArithmeticException e) {
				indicador.setKgM3(indicador.getKgHa().divide(indicador.getM3Ha(), BigDecimal.ROUND_HALF_UP));
			}
			
			indicador.setReceitaLiquidaM3(indicador.getReceitaLiquidaHa().divide(indicador.getM3Ha(), BigDecimal.ROUND_HALF_UP));
			BigDecimal divide;
			try {
				divide = indicador.getEmpregosHa().divide(indicador.getM3Ha());
			} catch (ArithmeticException e) {
				divide = indicador.getEmpregosHa().setScale(5).divide(indicador.getM3Ha(), BigDecimal.ROUND_UP);
			}
			indicador.setEmpregosM3(divide.multiply(new BigDecimal(1000)));
			
			
			totalKgM3 = totalKgM3.add(indicador.getKgM3());
			totalKgHa = totalKgHa.add(indicador.getKgHa());
			totalReceitaLiquidaM3 = totalReceitaLiquidaM3.add(indicador.getReceitaLiquidaM3());
			totalReceitaLiquidaHa = totalReceitaLiquidaHa.add(indicador.getReceitaLiquidaHa());
			totalEmpregosM3 = totalEmpregosM3.add(indicador.getEmpregosM3());
			totalEmpregosHa = totalEmpregosHa.add(indicador.getEmpregosHa());
			totalCicloAnual = totalCicloAnual.add(indicador.getQtdDiasCicloAnual());
			totalM3Ha = totalM3Ha.add(indicador.getM3Ha());
			
		}
		
		BigDecimal mediaKgM3 = totalKgM3.divide(new BigDecimal(qtd), BigDecimal.ROUND_HALF_UP);
		BigDecimal mediaKgHa = totalKgHa.divide(new BigDecimal(qtd), BigDecimal.ROUND_HALF_UP);
		BigDecimal mediaM3Ha = totalM3Ha.divide(new BigDecimal(qtd), BigDecimal.ROUND_HALF_UP);
		BigDecimal mediaReceitaLiquidaM3 = totalReceitaLiquidaM3.divide(new BigDecimal(qtd), BigDecimal.ROUND_HALF_UP);
		BigDecimal mediaReceitaLiquidaHa = totalReceitaLiquidaHa.divide(new BigDecimal(qtd), BigDecimal.ROUND_HALF_UP);
		BigDecimal mediaEmpregosM3 = totalEmpregosM3.divide(new BigDecimal(qtd), BigDecimal.ROUND_HALF_UP);
		BigDecimal mediaEmpregosHa = totalEmpregosHa.divide(new BigDecimal(qtd), BigDecimal.ROUND_HALF_UP);
		BigDecimal mediaCicloAnual = totalCicloAnual.divide(new BigDecimal(qtd), BigDecimal.ROUND_HALF_UP);
		
		BigDecimal media70KgM3 = mediaKgM3.multiply(new BigDecimal(0.7));
		BigDecimal media70KgHa = mediaKgHa.multiply(new BigDecimal(0.7));
		BigDecimal media70ReceitaLiquidaM3 = mediaReceitaLiquidaM3.multiply(new BigDecimal(0.7));
		BigDecimal media70ReceitaLiquidaHa = mediaReceitaLiquidaHa.multiply(new BigDecimal(0.7));
		BigDecimal media70EmpregosM3 = mediaEmpregosM3.multiply(new BigDecimal(0.7));
		BigDecimal media70EmpregosHa = mediaEmpregosHa.multiply(new BigDecimal(0.7));
		BigDecimal media70M3Ha = mediaM3Ha.multiply(new BigDecimal(0.7));
		BigDecimal media70CicloAnual = mediaCicloAnual.multiply(new BigDecimal(0.7));
		
		//Calcular os Pesos
		for (Indicadores indicador : indicadores) {
			
			if (indicador.getKgM3().compareTo(media70KgM3) < 0){
				indicador.setPesoKgM3(PesoIndicadores.BAIXA);
			}
			if (indicador.getKgM3().compareTo(media70KgM3) >= 0 && indicador.getKgM3().compareTo(mediaKgM3) < 0){
				indicador.setPesoKgM3(PesoIndicadores.MEDIA);
			}
			if (indicador.getKgM3().compareTo(mediaKgM3) >= 0){
				indicador.setPesoKgM3(PesoIndicadores.ALTA);
			}

			if (indicador.getKgHa().compareTo(media70KgHa) < 0){
				indicador.setPesoKgHa(PesoIndicadores.BAIXA);
			}
			if (indicador.getKgHa().compareTo(media70KgHa) >= 0 && indicador.getKgHa().compareTo(mediaKgHa) < 0){
				indicador.setPesoKgHa(PesoIndicadores.MEDIA);
			}
			if (indicador.getKgHa().compareTo(mediaKgHa) >= 0){
				indicador.setPesoKgHa(PesoIndicadores.ALTA);
			}

			if (indicador.getReceitaLiquidaM3().compareTo(media70ReceitaLiquidaM3) < 0){
				indicador.setPesoReceitaLiquidaM3(PesoIndicadores.BAIXA);
			}
			if (indicador.getReceitaLiquidaM3().compareTo(media70ReceitaLiquidaM3) >= 0 && indicador.getReceitaLiquidaM3().compareTo(mediaReceitaLiquidaM3) < 0){
				indicador.setPesoReceitaLiquidaM3(PesoIndicadores.MEDIA);
			}
			if (indicador.getReceitaLiquidaM3().compareTo(mediaReceitaLiquidaM3) >= 0){
				indicador.setPesoReceitaLiquidaM3(PesoIndicadores.ALTA);
			}

			if (indicador.getReceitaLiquidaHa().compareTo(media70ReceitaLiquidaHa) < 0){
				indicador.setPesoReceitaLiquidaHa(PesoIndicadores.BAIXA);
			}
			if (indicador.getReceitaLiquidaHa().compareTo(media70ReceitaLiquidaHa) >= 0 && indicador.getReceitaLiquidaHa().compareTo(mediaReceitaLiquidaHa) < 0){
				indicador.setPesoReceitaLiquidaHa(PesoIndicadores.MEDIA);
			}
			if (indicador.getReceitaLiquidaHa().compareTo(mediaReceitaLiquidaHa) >= 0){
				indicador.setPesoReceitaLiquidaHa(PesoIndicadores.ALTA);
			}
			
			if (indicador.getEmpregosM3().compareTo(media70EmpregosM3) < 0){
				indicador.setPesoEmpregosM3(PesoIndicadores.BAIXA);
			}
			if (indicador.getEmpregosM3().compareTo(media70EmpregosM3) >= 0 && indicador.getEmpregosM3().compareTo(mediaEmpregosM3) < 0){
				indicador.setPesoEmpregosM3(PesoIndicadores.MEDIA);
			}
			if (indicador.getEmpregosM3().compareTo(mediaEmpregosM3) >= 0){
				indicador.setPesoEmpregosM3(PesoIndicadores.ALTA);
			}

			if (indicador.getEmpregosHa().compareTo(media70EmpregosHa) < 0){
				indicador.setPesoEmpregosHa(PesoIndicadores.BAIXA);
			}
			if (indicador.getEmpregosHa().compareTo(media70EmpregosHa) >= 0 && indicador.getEmpregosHa().compareTo(mediaEmpregosHa) < 0){
				indicador.setPesoEmpregosHa(PesoIndicadores.MEDIA);
			}
			if (indicador.getEmpregosHa().compareTo(mediaEmpregosHa) >= 0){
				indicador.setPesoEmpregosHa(PesoIndicadores.ALTA);
			}

			if (indicador.getM3Ha().compareTo(media70M3Ha) < 0){
				indicador.setPesoM3Ha(PesoIndicadoresSegHidrica.BAIXA);
			}
			if (indicador.getM3Ha().compareTo(media70M3Ha) >= 0 && indicador.getM3Ha().compareTo(mediaM3Ha) < 0){
				indicador.setPesoM3Ha(PesoIndicadoresSegHidrica.MEDIA);
			}
			if (indicador.getM3Ha().compareTo(mediaM3Ha) >= 0){
				indicador.setPesoM3Ha(PesoIndicadoresSegHidrica.ALTA);
			}

			if (indicador.getQtdDiasCicloAnual().compareTo(media70CicloAnual) < 0){
				indicador.setCicloCultura(PesoIndicadorCiclo.TEMPOCURTO);
			}
			if (indicador.getQtdDiasCicloAnual().compareTo(media70CicloAnual) >= 0 && indicador.getQtdDiasCicloAnual().compareTo(mediaCicloAnual) < 0){
				indicador.setCicloCultura(PesoIndicadorCiclo.TEMPOLONGO);
			}
			if (indicador.getQtdDiasCicloAnual().compareTo(mediaCicloAnual) >= 0){
				indicador.setCicloCultura(PesoIndicadorCiclo.PERMANENTE);
			}

			verificarCH(indicador);
			
		}
		graficoIndicadoresCorteHidricoBacia = criarGraficoCorteHidrico(indicadores);
		calculado = true;
		infoMsg(ResourceBundle.getMessage("calculado"), null);
	}

	public boolean verificarData(Date data){
		Calendar dataHoje = new GregorianCalendar();
		dataHoje.setTime(data);
		for (Date date : getDates()) {
			Calendar d = new GregorianCalendar();
			d.setTime(date);
			if (d.get(Calendar.DAY_OF_MONTH) == dataHoje.get(Calendar.DAY_OF_MONTH) &&
					d.get(Calendar.MONTH) == dataHoje.get(Calendar.MONTH) &&
							d.get(Calendar.YEAR) == dataHoje.get(Calendar.YEAR)){
				return false;
			}
		}
		return true;
	}

	private SelectItem[] criarFiltroData(List<Date> indicadores)  {  
        SelectItem[] options = new SelectItem[indicadores.size()];  
  
        for(int i = 0; i < indicadores.size(); i++) {  
        	Date data = indicadores.get(i);
        	Calendar dia = new GregorianCalendar();
        	dia.setTime(data);
        	String dataFormatada = dia.get(Calendar.DAY_OF_MONTH) + "/" + (dia.get(Calendar.MONTH)+1) + "/" + dia.get(Calendar.YEAR);
            options[i] = new SelectItem(data, dataFormatada);  
        }  
  
        return options;  
    }  

	public PesoIndicadores[] getPesos() {
		return PesoIndicadores.values();
	}
	
	public PesoIndicadorCiclo[] getPesosCiclo() {
		return PesoIndicadorCiclo.values();
	}

	public PesoIndicadoresSegHidrica[] getPesosSegHid() {
		return PesoIndicadoresSegHidrica.values();
	}

	public void gravar() {
		boolean valido = true;
		List<Indicadores> listaAtividadeInvalida = new ArrayList<Indicadores>();
		if (valido){
			for (Indicadores atividadeEconomica : indicadores) {
				if (!listaAtividadeInvalida.contains(atividadeEconomica)) {
					atividadeEconomica.setAtividadeEconomica(atividadeEconomicaService.salvar(atividadeEconomica.getAtividadeEconomica()));
					atividadeEconomica = indicadoresService.salvar(atividadeEconomica);
				}
			}
			infoMsg(ResourceBundle.getMessage("indicadoresSalvo"), null);
			indicadores =  null;
		}
	}

	public List<Indicadores> getIndicadores() {
		return indicadores;
	}

	public void setIndicadores(List<Indicadores> atividadeEconomicas) {
		this.indicadores = atividadeEconomicas;
	}

	public Indicadores getIndicadoresSelecionado() {
		return indicadoresSelecionado;
	}

	public void setIndicadoresSelecionado(Indicadores atividadeEconomicaSelecionado) {
		this.indicadoresSelecionado = atividadeEconomicaSelecionado;
	}

	public String getOperacao() {
		return operacao;
	}

	public void setOperacao(String operacao) {
		this.operacao = operacao;
	}

	public SelectItem[] getFiltroDatas() {
		return filtroDatas;
	}

	public void setFiltroDatas(SelectItem[] filtroDatas) {
		this.filtroDatas = filtroDatas;
	}

	public List<Date> getDates() {
		return dates;
	}

	public void setDates(List<Date> dates) {
		this.dates = dates;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public Boolean getCalculado() {
		return calculado;
	}

	public void setCalculado(Boolean calculado) {
		this.calculado = calculado;
	}

	public Boolean getIndicadorKgHa() {
		return indicadorKgHa;
	}

	public void setIndicadorKgHa(Boolean indicadorKgHa) {
		this.indicadorKgHa = indicadorKgHa;
	}

	public Boolean getIndicadorReceitaLiquidaHa() {
		return indicadorReceitaLiquidaHa;
	}

	public void setIndicadorReceitaLiquidaHa(Boolean indicadorReceitaLiquidaHa) {
		this.indicadorReceitaLiquidaHa = indicadorReceitaLiquidaHa;
	}

	public Boolean getIndicadorM3Ha() {
		return indicadorM3Ha;
	}

	public void setIndicadorM3Ha(Boolean indicadorM3Ha) {
		this.indicadorM3Ha = indicadorM3Ha;
	}

	public Boolean getIndicadorEmpregosHa() {
		return indicadorEmpregosHa;
	}

	public void setIndicadorEmpregosHa(Boolean indicadorEmpregosHa) {
		this.indicadorEmpregosHa = indicadorEmpregosHa;
	}

	public Boolean getIndicadorKgM3() {
		return indicadorKgM3;
	}

	public void setIndicadorKgM3(Boolean indicadorKgM3) {
		this.indicadorKgM3 = indicadorKgM3;
	}

	public Boolean getIndicadorReceitaLiquidaM3() {
		return indicadorReceitaLiquidaM3;
	}

	public void setIndicadorReceitaLiquidaM3(Boolean indicadorReceitaLiquidaM3) {
		this.indicadorReceitaLiquidaM3 = indicadorReceitaLiquidaM3;
	}

	public Boolean getIndicadorEmpregosM3() {
		return indicadorEmpregosM3;
	}

	public void setIndicadorEmpregosM3(Boolean indicadorEmpregosM3) {
		this.indicadorEmpregosM3 = indicadorEmpregosM3;
	}

	public Boolean getIndicadorCiclo() {
		return indicadorCiclo;
	}

	public void setIndicadorCiclo(Boolean indicadorCiclo) {
		this.indicadorCiclo = indicadorCiclo;
	}

	public Integer getP1() {
		return p1;
	}

	public void setP1(Integer p1) {
		this.p1 = p1;
	}

	public Integer getP2() {
		return p2;
	}

	public void setP2(Integer p2) {
		this.p2 = p2;
	}

	public Integer getP3() {
		return p3;
	}

	public void setP3(Integer p3) {
		this.p3 = p3;
	}

	public Integer getP4() {
		return p4;
	}

	public void setP4(Integer p4) {
		this.p4 = p4;
	}

	public Integer getP5() {
		return p5;
	}

	public void setP5(Integer p5) {
		this.p5 = p5;
	}

	public Integer getP6() {
		return p6;
	}

	public void setP6(Integer p6) {
		this.p6 = p6;
	}

	public Integer getP7() {
		return p7;
	}

	public void setP7(Integer p7) {
		this.p7 = p7;
	}

	public Integer getP8() {
		return p8;
	}

	public void setP8(Integer p8) {
		this.p8 = p8;
	}

	public CartesianChartModel getGraficoIndicadoresCorteHidricoBacia() {
		return graficoIndicadoresCorteHidricoBacia;
	}

	public void setGraficoIndicadoresCorteHidricoBacia(
			CartesianChartModel graficoIndicadoresCorteHidricoBacia) {
		this.graficoIndicadoresCorteHidricoBacia = graficoIndicadoresCorteHidricoBacia;
	}

}
