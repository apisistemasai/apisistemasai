/*package com.agrosolutions.sai.view;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.RequestScoped;
import javax.faces.model.SelectItem;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.Atividade;
import com.agrosolutions.sai.model.Irrigante;
import com.agrosolutions.sai.model.Lote;
import com.agrosolutions.sai.service.AtividadeService;
import com.agrosolutions.sai.service.LoteService;

*//**
* Use case : Consultar Lote
* 
* @since   : Jan 06, 2011, 02:23:20 AM
* @author  : raphael.ferreira@ivia.com.br
*//*

@Component("consultarLoteBean")
@RequestScoped
public class ConsultarLoteBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = -6312802113416655960L;

	public static Logger logger = Logger.getLogger(ConsultarLoteBean.class);

	public static final String MSG_TITULO = "Consultar Lotes";

	@Autowired
	private LoteService loteService;
	@Autowired
	private AtividadeService atividadeService;
	
	private List<Lote> lotes;
	private Lote loteSelecionado;
	private Irrigante irrigante;
	private SelectItem[] atividades;
	
	public void carregarDadosIniciais() {
	    lotes = loteService.pesquisar(irrigante);
		List<Atividade> atividades = atividadeService.obterTodos();
		this.atividades = criarFiltroAtividades(atividades);
	}

	public void excluir(){
	   loteService.excluir(loteSelecionado);
	   lotes = loteService.pesquisar(irrigante);
	   loteSelecionado = null;
	   infoMsg("Lote exclu�do com sucesso.", MSG_TITULO);
   }

	private SelectItem[] criarFiltroAtividades(List<Atividade> atividades)  {  
		SelectItem[] options = new SelectItem[atividades.size() + 1];  
		
		options[0] = new SelectItem("", "Todas");  
		for(int i = 0; i < atividades.size(); i++) {  
		    options[i + 1] = new SelectItem(atividades.get(i).getNome(), atividades.get(i).getNome());  
		}  
		return options;  
	}  

	public String cadastro() {
		return "cadastrarLote?faces-redirect=true";
	}
	
	public String voltar() {
		return "consultarIrrigante?faces-redirect=true";
	}

	public String setor() {
		return "consultarSetor?faces-redirect=true";
	}

	public Irrigante getIrrigante() {
		return irrigante;
	}

	public void setIrrigante(Irrigante irrigante) {
		this.irrigante = irrigante;
	}

	public List<Lote> getLotes() {
		return lotes;
	}

	public void setLotes(List<Lote> lotes) {
		this.lotes = lotes;
	}


	public SelectItem[] getAtividades() {
		return atividades;
	}


	public void setAtividades(SelectItem[] atividades) {
		this.atividades = atividades;
	}

	public Lote getLoteSelecionado() {
		return loteSelecionado;
	}

	public void setLoteSelecionado(Lote loteSelecionado) {
		this.loteSelecionado = loteSelecionado;
	}
}
*/