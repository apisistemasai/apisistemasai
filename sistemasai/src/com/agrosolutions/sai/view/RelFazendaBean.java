package com.agrosolutions.sai.view;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;

import org.apache.log4j.Logger;
import org.primefaces.model.StreamedContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.exception.SaiException;
import com.agrosolutions.sai.model.Irrigante;
import com.agrosolutions.sai.service.IrriganteService;
import com.agrosolutions.sai.util.RelatorioUtil;
import com.agrosolutions.sai.util.ResourceBundle;

/**
* Use case : Relatório de Fazenda
* 
* @since   : Mar 19, 2013, 20:23:20 PM
* @author  : rpf1404@gmail.com
*/

@Component("relFazendaBean")
@Scope("session")
public class RelFazendaBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = 2000210947938981045L;

	public static Logger logger = Logger.getLogger(RelFazendaBean.class);

	@Autowired
	private RelatorioUtil relatorioUtil;
	@Autowired
	private IrriganteService irriganteService;

	private String localizacao;
	private StreamedContent file;
	
	public String carregar(){
		return null;
	}
	   
	public String imprimir() {
		boolean erro = false;
		try {
			Irrigante i = null;
			if ( !localizacao.isEmpty() ){
				try {
					i = irriganteService.pesquisarPorLote(localizacao, super.getDistritosPesquisa());
				} catch (SaiException e) {
					errorMsg(e.getMessage(), null);
					erro = true;
				}
			}

			if (erro) {
				return null;
			}
			FacesContext facesContext = FacesContext.getCurrentInstance();
			ServletContext servletContext = (ServletContext) facesContext.getExternalContext().getContext();

			Map<String, Object> parametros = new HashMap<String, Object>();

			parametros.put("IDIRRIGANTE", i.getId());
			parametros.put("SETORES", servletContext.getRealPath("//relatorios/rptFazenda_setores.jasper") );
			parametros.put("BOMBAS",  servletContext.getRealPath("//relatorios/rptFazenda_bombas.jasper") );
			parametros.put("FILTROS", servletContext.getRealPath("//relatorios/rptFazenda_filtros.jasper") );
			parametros.put("TUBOS",   servletContext.getRealPath("//relatorios/rptFazenda_tubos.jasper") );
			parametros.put("VALVULAS",servletContext.getRealPath("//relatorios/rptFazenda_valvulas.jasper") );

			parametros.put("EMISSORES",  servletContext.getRealPath("//relatorios/rptFazenda_emissores.jasper") );
			parametros.put("PLANTIOS",  servletContext.getRealPath("//relatorios/rptFazenda_plantios.jasper") );

			parametros.put("EMISSORES_PLANTIO",  servletContext.getRealPath("//relatorios/rptPlantio_emissores.jasper") );


			file = relatorioUtil.relatorio("rptFazenda.jasper", parametros, "Fazenda" + i.getLocalizacao()+ " .pdf");
		} catch (SaiException e) {
			errorMsg(ResourceBundle.getMessage("ocorreuErro")+ ":" + e.getMessage(), null);
			logger.warn("Ocorreu o seguinte erro: " + e.getMessage() + e.getCause().getMessage());
			return null;
		} catch (Exception e) {
			errorMsg(ResourceBundle.getMessage("ocorreuErro")+ ":" + e.getMessage(), null);
			logger.fatal("Ocorreu o seguinte erro: " + e.getMessage() + e.getCause().getMessage());
			return null;
		}
		infoMsg(ResourceBundle.getMessage("demandaRelatorio")+ ".", null);
		return null;
	}

	public String getLocalizacao() {
		return localizacao;
	}


	public void setLocalizacao(String localizacao) {
		this.localizacao = localizacao;
	}

	public StreamedContent getFile() {
		return file;
	}

	public void setFile(StreamedContent file) {
		this.file = file;
	}
}
