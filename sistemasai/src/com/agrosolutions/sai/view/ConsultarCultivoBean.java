package com.agrosolutions.sai.view;

import java.io.Serializable;
import java.util.List;

import javax.faces.model.SelectItem;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.Cultivo;
import com.agrosolutions.sai.model.Cultura;
import com.agrosolutions.sai.model.Irrigante;
import com.agrosolutions.sai.model.Variedade;
import com.agrosolutions.sai.service.CultivoService;
import com.agrosolutions.sai.util.ResourceBundle;

/**
* Use case : Consultar Cultivo
* 
* @since   : Jan 18, 2011, 03:23:20 AM
* @author  : raphael.ferreira@ivia.com.br
*/

@Component("consultarCultivoBean")
@Scope("session")
public class ConsultarCultivoBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = -8378356362675872915L;

	public static Logger logger = Logger.getLogger(ConsultarCultivoBean.class);

	public static final String MSG_TITULO = "Consultar Cultivos";

	@Autowired
	private CultivoService cultivoService;
	
	private Irrigante irrigante;
	private List<Cultivo> cultivos;
	private Cultivo cultivoSelecionado;
	private SelectItem[] culturas;
	private SelectItem[] variedades;
	
	public void carregarDadosIniciais() {
		cultivos = cultivoService.pesquisar(irrigante);
		
		//Popular combobox de filtros
		List<Cultura> culturas = cultivoService.pesquisarCulturas(irrigante);
		this.culturas = criarFiltroCulturas(culturas);
		List<Variedade> variedades = cultivoService.pesquisarVariedades(irrigante);
		this.variedades = criarFiltroVariedades(variedades);
	}

	private SelectItem[] criarFiltroCulturas(List<Cultura> culturas)  {  
        SelectItem[] options = new SelectItem[culturas.size() + 1];  
  
        options[0] = new SelectItem("", "Todas");  
        for(int i = 0; i < culturas.size(); i++) {  
            options[i + 1] = new SelectItem(culturas.get(i).getNome(), culturas.get(i).getNome());  
        }  
  
        return options;  
    }  

	private SelectItem[] criarFiltroVariedades(List<Variedade> variedades)  {  
        SelectItem[] options = new SelectItem[variedades.size() + 1];  
  
        options[0] = new SelectItem("", "Todas");  
        for(int i = 0; i < variedades.size(); i++) {  
            options[i + 1] = new SelectItem(variedades.get(i).getNome(), variedades.get(i).getNome());  
        }  
  
        return options;  
    }  

	public String cadastro() {
		return "cadastrarCultivo?faces-redirect=true";
	}
	
	public String voltar() {
		return "consultarIrrigante?faces-redirect=true";
	}

	public void excluir(){
	    cultivoService.excluir(cultivoSelecionado);
	    cultivos = null;
	    cultivoSelecionado = null;
		infoMsg(ResourceBundle.getMessage("cultivoExcluir")+ ".", MSG_TITULO);
   }  	
	
	public Irrigante getIrrigante() {
		return irrigante;
	}

	public void setIrrigante(Irrigante irrigante) {
		this.irrigante = irrigante;
	}

	public List<Cultivo> getCultivos() {
		return cultivos;
	}

	public void setCultivos(List<Cultivo> cultivos) {
		this.cultivos = cultivos;
	}

	public SelectItem[] getCulturas() {
		return culturas;
	}

	public void setCulturas(SelectItem[] culturas) {
		this.culturas = culturas;
	}

	public SelectItem[] getVariedades() {
		return variedades;
	}

	public void setVariedades(SelectItem[] variedades) {
		this.variedades = variedades;
	}

	public Cultivo getCultivoSelecionado() {
		return cultivoSelecionado;
	}

	public void setCultivoSelecionado(Cultivo cultivoSelecionado) {
		this.cultivoSelecionado = cultivoSelecionado;
	}

}
