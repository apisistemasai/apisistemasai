package com.agrosolutions.sai.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.Bomba;
import com.agrosolutions.sai.model.Fabricante;
import com.agrosolutions.sai.model.TipoFabricante;
import com.agrosolutions.sai.service.BombaService;
import com.agrosolutions.sai.service.FabricanteService;
import com.agrosolutions.sai.util.ResourceBundle;

/**
* Use case : Consultar Bomba
* 
* @since   : Jan 06, 2011, 22:23:20 AM
* @author  : raphael.ferreira@ivia.com.br
*/

@Component("consultarBombaBean")
@Scope("view")
public class ConsultarBombaBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = 562578786037144708L;

	public static Logger logger = Logger.getLogger(ConsultarBombaBean.class);

	@Autowired
	private BombaService bombaService;
	@Autowired
	private FabricanteService fabricanteService;
	
	private LazyDataModel<Bomba> bombas;
	private Bomba bombaSelecionada;
	private List<Fabricante> fabricantes;
	private Bomba bombaBusca;


	public void carregarDadosIniciais() {
		
		if(bombaBusca == null){
			bombaBusca = new Bomba();
			bombaBusca.setSortField("modelo");
			bombaBusca.setSortOrder(SortOrder.ASCENDING);

			this.setFabricantes(fabricanteService.pesquisar(TipoFabricante.Bomba));

			consultar();
		}
		
	}

	public void consultar() {
		bombas = new LazyDataModel<Bomba>() {

			private static final long serialVersionUID = -7252669285117066497L;

			@Override
			public List<Bomba> load(int first, int pageSize, String sortField,
					SortOrder sortOrder, Map<String, String> filters) {
				List<Bomba> bombas = new ArrayList<Bomba>();

		        if(sortField == null){
					sortField = "modelo";
					sortOrder = SortOrder.ASCENDING;
				}
		        
				bombaBusca.setSortField(sortField);
				bombaBusca.setSortOrder(sortOrder);
				bombas = bombaService.pesquisar(bombaBusca, first, pageSize);

				return bombas;
			}
		 };
		 int quantidade = bombaService.quantidade(bombaBusca);
	     bombas.setRowCount(quantidade==0?1:quantidade);
	   }

	
	public void excluir(){
	   bombaService.excluir(bombaSelecionada);
		infoMsg(ResourceBundle.getMessage("bombaExcluir")+ ".", null);
   }

   public String cadastro() {
		return "cadastrarBomba?faces-redirect=true";
	}
	
	public LazyDataModel<Bomba> getBombas() {
		return bombas;
	}

	public void setBombas(LazyDataModel<Bomba> bombas) {
		this.bombas = bombas;
	}

	public Bomba getBombaSelecionada() {
		return bombaSelecionada;
	}

	public List<Fabricante> getFabricantes() {
		return fabricantes;
	}

	public void setFabricantes(List<Fabricante> fabricantes) {
		this.fabricantes = fabricantes;
	}

	public void setBombaSelecionada(Bomba bombaSelecionada) {
		this.bombaSelecionada = bombaSelecionada;
	}

	public void setBombaBusca(Bomba bombaBusca) {
		this.bombaBusca = bombaBusca;
	}

	public Bomba getBombaBusca() {
		return bombaBusca;
	}

}
