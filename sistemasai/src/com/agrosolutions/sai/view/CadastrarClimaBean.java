package com.agrosolutions.sai.view;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.faces.event.ActionEvent;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.Area;
import com.agrosolutions.sai.model.Clima;
import com.agrosolutions.sai.service.ClimaService;
import com.agrosolutions.sai.util.ResourceBundle;

/**
* Use case : Cadastrar Clima
* 
* @since   : Mar 09, 2012, 20:23:20 PM
* @author  : raphael.ferreira@ivia.com.br
*/

@Component("cadastrarClimaBean")
@Scope("session")
public class CadastrarClimaBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = 3055712674382182387L;

	public static Logger logger = Logger.getLogger(CadastrarClimaBean.class);

	@Autowired
	private ClimaService climaService;
	
	private Area area;
	private Clima clima;
	private String operacao;
	private BigDecimal altitude;
	
	public void carregarDadosIniciais() {
		operacao = getOperacao();
		area = getArea();
		
		if (operacao.equals("NOVO") &&clima == null) {
			clima = new Clima(new Date(), area, null);
		}
		
	}
	
	public void gravar(ActionEvent actionEvent) {
		clima = climaService.prepararClimaCalcularEto(clima);
		clima = climaService.salvar(clima);
		if (operacao.equals("NOVO")) {
			clima = null;
			infoMsg(ResourceBundle.getMessage("climaSalvar")+ ".", null);
		}else{
			infoMsg(ResourceBundle.getMessage("climaAlterar")+ ".", null);
		}
	}
	
	public String voltar() {
		return "consultarClima?faces-redirect=true";
	}
	
	public Area getArea() {
		if (area == null) {
			area = (Area) getFlashScope().get("area");
		}
		return area;
	}

	public void setArea(Area area) {
		this.area = area;
		getFlashScope().put("area", area);
	}

	public String getOperacao() {
		if (operacao == null) {
			operacao = (String) getFlashScope().get("operacao");
		}
		return operacao;
	}

	public void setOperacao(String operacao) {
		this.operacao = operacao;
		getFlashScope().put("operacao", operacao);
	}

	public Clima getClima() {
		return clima;
	}

	public void setClima(Clima clima) {
		this.clima = clima;
	}

	public BigDecimal getAltitude() {
		return altitude;
	}

	public void setAltitude(BigDecimal altitude) {
		this.altitude = altitude;
	}
}
