package com.agrosolutions.sai.view;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.context.Flash;
import javax.servlet.http.HttpServletRequest;

import org.springframework.security.core.context.SecurityContextHolder;

import com.agrosolutions.sai.model.Distrito;
import com.agrosolutions.sai.model.Usuario;

/**
 *
 * @author Raphael
 */
public class ManagerBean {

	public Usuario usuarioLogado;
	public Boolean multidistrito;
	private List<Distrito> distritosPesquisa = new ArrayList<Distrito>();
	private Distrito distritoPadrao = new Distrito();
	
	
	public Flash getFlashScope() {
		return FacesContext.getCurrentInstance().getExternalContext().getFlash();
	}

	public String getRequestParameter(String key) {
		return FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get(key);
	}
	/**
	 * Limpa os valores de todos os componentes filhos do componente passado por parametro
	 *
	 * @param uiParentComponent
	 */
	protected void resetForm(UIComponent uiParentComponent) {
		for (UIComponent uiComponent : uiParentComponent.getChildren()) {
			if (uiComponent instanceof UIInput) {
				UIInput uii = (UIInput) uiComponent;
				uii.setValue(null);
				// uii.setSubmittedValue(null);
				uii.setLocalValueSet(false);
			}
			resetForm(uiComponent);
		}
	}

	public HttpServletRequest getRequestSession() {
		return (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
	}	
	
	public HttpServletRequest getSession() {
		return (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
	}	

	public static void errorMsg(String titulo, String msg){

		FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,titulo, msg);
		FacesContext fc = FacesContext.getCurrentInstance();
		fc.addMessage(null, fm);
	}

	public static void infoMsg(String titulo, String msg){

		FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO, titulo, msg);
		FacesContext fc = FacesContext.getCurrentInstance();
		fc.addMessage(null, fm);
	}
	
	public static void warnMsg(String titulo, String msg){

		FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_WARN, titulo, msg);
		FacesContext fc = FacesContext.getCurrentInstance();
		fc.addMessage(null, fm);
	}

	public Usuario getUsuarioLogado(){
		return (Usuario) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	}

	public void setUsuarioLogado(Usuario usuarioLogado) {
		this.usuarioLogado = usuarioLogado;
	}

	public String prepararDistritos() {
		String codigos = "(";
		for (Distrito d : getDistritosPesquisa()) {
			codigos = codigos + d.getId() + ",";
		}
		return codigos = codigos.substring(0,codigos.length() - 1) + ")";
	}

	public Boolean getMultidistrito() {
		if (getUsuarioLogado().getDistritos() != null && getUsuarioLogado().getDistritos().size() > 1 ){
			return true;
		}else{
			return false;
		}
	}
	
	/**
	 * Verifica na sess�o se o usu�rio � multidistrito e seta os distritos que ser�o usados nas pesquisas
	 * 
	 * @return List<Distrito>
	 */
	public List<Distrito> getDistritosPesquisa() {
		distritosPesquisa.clear();
		
		if (this.multidistrito == null) {
			this.multidistrito = getMultidistrito();
		}
		
		if (this.multidistrito){
			distritosPesquisa.add(getUsuarioLogado().getDistritoPadrao());
		}else{
			distritosPesquisa.add( (Distrito) getRequestSession().getSession(true).getAttribute("distritosUsuario") );
		}
		
		return distritosPesquisa;
	}

	public void setDistritosPesquisa(List<Distrito> distritosPesquisa) {
		this.distritosPesquisa = distritosPesquisa;
	}

	public Distrito getDistritoPadrao() {
		if (getRequestSession().getSession(true).getAttribute("distritoPadrao") != null ){	
			this.distritoPadrao = (Distrito) getRequestSession().getSession(true).getAttribute("distritoPadrao");
		}
		
		return distritoPadrao;
	}

	public void setDistritoPadrao(Distrito distritoPadrao) {
		getRequestSession().getSession().setAttribute("distritoPadrao", distritoPadrao );
		this.distritoPadrao = distritoPadrao;
	}	
}
