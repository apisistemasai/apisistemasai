package com.agrosolutions.sai.view;

import java.io.Serializable;

import javax.faces.event.ActionEvent;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.BaciaHidrografica;
import com.agrosolutions.sai.service.BaciaHidrograficaService;
import com.agrosolutions.sai.util.ResourceBundle;

/**
* Use case : Cadastrar BaciaHidrografica
* 
* @since   : Jan 06, 2016, 15:00:20 AM
* @author  : raphael@iasoft.me
*/

@Component("cadastrarBaciaHidrograficaBean")
@Scope("session")
public class CadastrarBaciaHidrograficaBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = -665693771073831553L;

	public static Logger logger = Logger.getLogger(CadastrarBaciaHidrograficaBean.class);

	@Autowired
	private BaciaHidrograficaService baciaHidrograficaService;
	
	private BaciaHidrografica baciaHidrografica;
	private String operacao;

	public void carregarDadosIniciais() {
		operacao = getOperacao();
		baciaHidrografica = getBaciaHidrografica();
		
		if (operacao.equals("NOVO") && baciaHidrografica == null) {
			setBaciaHidrografica(new BaciaHidrografica());
		}
	}
	
	public void gravar(ActionEvent actionEvent) {
		baciaHidrografica = baciaHidrograficaService.salvar(baciaHidrografica); 
		if (operacao.equals("NOVO")) {
		baciaHidrografica = null;
			infoMsg(ResourceBundle.getMessage("baciaHidrograficaSalvar")+ ".", null);
		}else{
			infoMsg(ResourceBundle.getMessage("baciaHidrograficaAlterar")+ ".", null);
		}
	}
	public String voltar() {
		return "consultarBaciaHidrografica?faces-redirect=true";
	}

	public BaciaHidrografica getBaciaHidrografica() {
		if (baciaHidrografica == null) {
			baciaHidrografica = (BaciaHidrografica) getFlashScope().get("baciaHidrografica");
		}
		return baciaHidrografica;
	}

	public void setBaciaHidrografica(BaciaHidrografica baciaHidrografica) {
		this.baciaHidrografica = baciaHidrografica;
		getFlashScope().put("baciaHidrografica", baciaHidrografica);
	}
	
	public String getOperacao() {
		if (operacao == null) {
			operacao = (String) getFlashScope().get("operacao");
		}
		return operacao;
	}

	public void setOperacao(String operacao) {
		this.operacao = operacao;
		getFlashScope().put("operacao", operacao);
	}
}
