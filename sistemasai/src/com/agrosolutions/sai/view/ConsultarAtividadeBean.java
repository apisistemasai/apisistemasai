package com.agrosolutions.sai.view;

import java.io.Serializable;
import java.util.List;

import org.apache.log4j.Logger;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.Atividade;
import com.agrosolutions.sai.service.AtividadeService;
import com.agrosolutions.sai.util.ResourceBundle;

/**
* Use case : Consultar Atividade
* 
* @since   : Jan 27, 2012, 23:23:20 AM
* @author  : raphael.ferreira@ivia.com.br
*/

@Component("consultarAtividadeBean")
@Scope("view")
public class ConsultarAtividadeBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = 2357476627027394279L;

	public static Logger logger = Logger.getLogger(ConsultarAtividadeBean.class);

	@Autowired
	private AtividadeService atividadeService;
	
	private List<Atividade> atividades;
	private Atividade atividadeSelecionado;
	private Atividade atividadeBusca;
	
	public void carregarDadosIniciais() {
		if (atividadeBusca == null){
			atividadeBusca = new Atividade();
			atividadeBusca.setSortField("nome");
			atividadeBusca.setSortOrder(SortOrder.ASCENDING);
			
			atividadeService.obterTodos();		
			consultar();	
		}
	}

	public String cadastro() {
		return "cadastrarAtividade?faces-redirect=true";
	}
	
	public void consultar(){	
		atividades = atividadeService.pesquisar(atividadeBusca, null, null);
	}

	public void excluir(){
	   atividadeService.excluir(atividadeSelecionado);
	   infoMsg(ResourceBundle.getMessage("atividadeExcluir")+ ".","");
	}

	public List<Atividade> getAtividades() {
		return atividades;
	}

	public void setAtividades(List<Atividade> atividades) {
		this.atividades = atividades;
	}

	public Atividade getAtividadeSelecionado() {
		return atividadeSelecionado;
	}
	
	public void setAtividadeSelecionado(Atividade atividadeSelecionado) {
		this.atividadeSelecionado = atividadeSelecionado;
	}

	public void setAtividadeBusca(Atividade atividadeBusca) {
		this.atividadeBusca = atividadeBusca;
	}

	public Atividade getAtividadeBusca() {
		return atividadeBusca;
	}
}
