package com.agrosolutions.sai.view;

import java.io.Serializable;

import javax.faces.event.ActionEvent;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.Doenca;
import com.agrosolutions.sai.service.DoencaService;
import com.agrosolutions.sai.util.ResourceBundle;

/**
* Use case : Cadastrar Doenca
* 
* @since   : Jan 27, 2012, 03:23:20 AM
* @author  : raphael.ferreira@ivia.com.br
*/

@Component("cadastrarDoencaBean")
@Scope("session")
public class CadastrarDoencaBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = 672611418348485586L;

	public static Logger logger = Logger.getLogger(CadastrarDoencaBean.class);

	public static final String MSG_TITULO = "Cadastro de Doenca";

	@Autowired
	private DoencaService doencaService;
	
	private Doenca doenca;
	private String operacao;
	
	public void carregarDadosIniciais() {
		operacao = getOperacao();
		doenca = getDoenca();
		
		if (operacao.equals("NOVO") && doenca == null) {
			doenca = new Doenca();
		}
	}
	public void gravar(ActionEvent actionEvent) {
		doenca = doencaService.salvar(doenca); 
		if (operacao.equals("NOVO")) {
			doenca = null;
			infoMsg(ResourceBundle.getMessage("doencaSalvar")+ ".", MSG_TITULO);
		}else{
			infoMsg(ResourceBundle.getMessage("doencaAlterar")+ ".", MSG_TITULO);
		}
	}

	public String voltar() {
		return "consultarDoenca?faces-redirect=true";
	}
	public Doenca getDoenca() {
		if (doenca == null) {
			doenca = (Doenca) getFlashScope().get("doenca");
		}
		return doenca;
	}

	public void setDoenca(Doenca doenca) {
		this.doenca = doenca;
		getFlashScope().put("doenca", doenca);
	}

	public String getOperacao() {
		if (operacao == null) {
			operacao = (String) getFlashScope().get("operacao");
		}
		return operacao;
	}

	public void setOperacao(String operacao) {
		this.operacao = operacao;
		getFlashScope().put("operacao", operacao);
	}
}
