package com.agrosolutions.sai.view;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import org.apache.log4j.Logger;
import org.primefaces.event.TabChangeEvent;
import org.primefaces.model.chart.PieChartModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.Area;
import com.agrosolutions.sai.model.Distrito;
import com.agrosolutions.sai.model.GraficoPizza;
import com.agrosolutions.sai.service.AreaService;
import com.agrosolutions.sai.service.ClimaService;
import com.agrosolutions.sai.service.PlantioService;
import com.agrosolutions.sai.util.BigDecimalUtil;

/**
* Use case : Consultar Equipamentos
* 
* @since   : Jun 22, 2012, 14:00:20 
* @author  : walljrdm@gmail.com
*/

@Component("consultarEEquipamentosBean")
@Scope("session")
public class ConsultarEEquipamentosBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = 598143180287771601L;

	public static Logger logger = Logger.getLogger(ConsultarEEquipamentosBean.class);

	public static final String MSG_TITULO = "Consultar �reas";

	@Autowired
	private AreaService areaService;
	@Autowired
	private ClimaService climaService;
	@Autowired
	private PlantioService plantioService;
	
	private List<Area> areas;
	private List<Distrito> distritos;
	private Boolean porArea;

	
	public void carregarDadosIniciais() {
		if (porArea == null) {
			porArea = true;
		}
		
		if (porArea) {
			areas = areaService.pesquisar(super.getDistritosPesquisa());
			for (Area a : areas) {
				a.setClima(climaService.ultimoPorArea(a));
				a.setGraficoBomba(criarGraficoBomba(a));
				a.setGraficoEmissor(criarGraficoEmissor(a));
				a.setGraficoFiltro(criarGraficoFiltro(a));
				a.setGraficoValvula(criarGraficoValvula(a));
				a.setGraficoTubo(criarGraficoTubo(a));

			}
		} else {
			distritos = super.getDistritosPesquisa();
			for (Distrito d : distritos) {
				d.setClima(climaService.ultimoPorDistrito(d));
				d.setGraficoBomba(criarGraficoBomba(d));
				d.setGraficoEmissor(criarGraficoEmissor(d));
				d.setGraficoFiltro(criarGraficoFiltro(d));
				d.setGraficoValvula(criarGraficoValvula(d));
				d.setGraficoTubo(criarGraficoTubo(d));

			}

		}
	}

   public void mudancaTabDistrito(TabChangeEvent event) {  
        infoMsg("Active Tab: " + event.getTab().getTitle(), event.getTab().getId());  
    }  
	   
   public void mudancaTabArea(TabChangeEvent event) {  
	      infoMsg("Active Tab: " + event.getTab().getTitle(), event.getTab().getId());  
    }

	private PieChartModel criarGraficoEmissor(Area a) {
		PieChartModel grafico = new PieChartModel();
		List<GraficoPizza> emissor = plantioService.emissorPorArea(a);
		montarGrafico(grafico, emissor);
		
		return grafico;
	}
	
	private PieChartModel criarGraficoBomba(Area a) {
		PieChartModel grafico = new PieChartModel();
		List<GraficoPizza> bomba = plantioService.bombaPorArea(a);
		montarGrafico(grafico, bomba);
		
		return grafico;
	}
	
	private PieChartModel criarGraficoFiltro(Area a) {
		PieChartModel grafico = new PieChartModel();
		List<GraficoPizza> filtro = plantioService.filtroPorArea(a);
		montarGrafico(grafico, filtro);
		
		return grafico;
	}
	
	private PieChartModel criarGraficoValvula(Area a) {
		PieChartModel grafico = new PieChartModel();
		List<GraficoPizza> valvula = plantioService.valvulaPorArea(a);
		montarGrafico(grafico, valvula);
		
		return grafico;
	}
	
	private PieChartModel criarGraficoTubo(Area a) {
		PieChartModel grafico = new PieChartModel();
		List<GraficoPizza> tubo = plantioService.tuboPorArea(a);
		montarGrafico(grafico, tubo);
		
		return grafico;
	}

	private PieChartModel criarGraficoEmissor(Distrito area) {
		PieChartModel grafico = new PieChartModel();
		List<GraficoPizza> emissor = plantioService.emissorPorDistrito(area);
		montarGrafico(grafico, emissor);
		
		return grafico;
	}

	private PieChartModel criarGraficoBomba(Distrito area) {
		PieChartModel grafico = new PieChartModel();
		List<GraficoPizza> bomba = plantioService.bombaPorDistrito(area);
		montarGrafico(grafico, bomba);
		
		return grafico;
	}
	
	private PieChartModel criarGraficoFiltro(Distrito area) {
		PieChartModel grafico = new PieChartModel();
		List<GraficoPizza> filtro = plantioService.filtroPorDistrito(area);
		montarGrafico(grafico, filtro);
		
		return grafico;
	}
	
	private PieChartModel criarGraficoValvula(Distrito area) {
		PieChartModel grafico = new PieChartModel();
		List<GraficoPizza> valvula = plantioService.valvulaPorDistrito(area);
		montarGrafico(grafico, valvula);
		
		return grafico;
	}
	
	private PieChartModel criarGraficoTubo(Distrito area) {
		PieChartModel grafico = new PieChartModel();
		List<GraficoPizza> tubo = plantioService.tuboPorDistrito(area);
		montarGrafico(grafico, tubo);
		
		return grafico;
	}

	private void montarGrafico(PieChartModel grafico, List<GraficoPizza> graficoPizza) {
		BigDecimal totalParcial = new BigDecimal(0);
		BigDecimal total = new BigDecimal(0);
		BigDecimal perc100 = new BigDecimal(100);
		BigDecimal percParcial = new BigDecimal(0); 
		String und = "";
		
		if(graficoPizza.isEmpty() || graficoPizza.get(0).getTotal()==null){
			for (GraficoPizza v : graficoPizza) {
				total = total.add(new BigDecimal(v.getQtd()));
			}
		}else{
			total = new BigDecimal(graficoPizza.get(0).getTotal());
		}
		int qtd = graficoPizza.size();
		int i = 0;
		for (GraficoPizza v : graficoPizza) {
			BigDecimal perc;
			i = i + 1;
			if (i == qtd && v.getTotal()==null){
				perc = perc100.subtract(percParcial);
			}else{
				perc = (new BigDecimal(v.getQtd()).multiply(perc100)).divide(total, BigDecimalUtil.MC_RUP);
				und = v.getUnd();
			}
			percParcial = percParcial.add(new BigDecimal(perc.intValue()==0?1:perc.intValue()));
			grafico.set(v.getNome() + ": " + (perc.intValue()==0?1:perc.intValue()) + "%" + " - " + v.getQtd() + " " + v.getUnd() , v.getQtd());
			totalParcial = totalParcial.add(new BigDecimal(v.getQtd()));
		}
		if (totalParcial.compareTo(total) != 0) {
			BigDecimal perc = perc100.subtract(percParcial);
			grafico.set("Outros" + ": " + perc + "%" + " - " + total.subtract(totalParcial) + " " + und, total.subtract(totalParcial));
		}
	}

	public List<Area> getAreas() {
		return areas;
	}

	public void setAreas(List<Area> areas) {
		this.areas = areas;
	}

	public List<Distrito> getDistritos() {
		return distritos;
	}

	public void setDistritos(List<Distrito> distritos) {
		this.distritos = distritos;
	}

	public Boolean getPorArea() {
		return porArea;
	}

	public void setPorArea(Boolean porArea) {
		this.porArea = porArea;
	}
}
