package com.agrosolutions.sai.view;

import java.io.Serializable;

import javax.faces.event.ActionEvent;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.Adubo;
import com.agrosolutions.sai.service.AduboService;
import com.agrosolutions.sai.util.ResourceBundle;

/**
* Use case : Cadastrar Adubo
* 
* @since   : Jan 27, 2012, 05:23:20 AM
* @author  : raphael.ferreira@ivia.com.br
*/

@Component("cadastrarAduboBean")
@Scope("session")
public class CadastrarAduboBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = -3282700631180475987L;

	public static Logger logger = Logger.getLogger(CadastrarAduboBean.class);

	public static final String MSG_TITULO = "Cadastro de Adubo";

	@Autowired
	private AduboService aduboService;
	
	private Adubo adubo;
	private String operacao;
	
	public void carregarDadosIniciais() {
		operacao = getOperacao();
		adubo = getAdubo();
		
		if (operacao.equals("NOVO") && adubo == null) {
			adubo = new Adubo();
		}
	}
	
	public void gravar(ActionEvent actionEvent) {
		adubo = aduboService.salvar(adubo); 
		if (operacao.equals("NOVO")) {
		adubo = null;
			infoMsg(ResourceBundle.getMessage("aduboSalvar")+ ".", MSG_TITULO);
		}else{
			infoMsg(ResourceBundle.getMessage("aduboAlterar")+ ".", MSG_TITULO);
		}
	}
	public String voltar() {
		return "consultarAdubo?faces-redirect=true";
	}

	public Adubo getAdubo() {
		if (adubo == null) {
			adubo = (Adubo) getFlashScope().get("adubo");
		}
		return adubo;
	}

	public void setAdubo(Adubo adubo) {
		this.adubo = adubo;
		getFlashScope().put("adubo", adubo);
	}

	public String getOperacao() {
		if (operacao == null) {
			operacao = (String) getFlashScope().get("operacao");
		}
		return operacao;
	}

	public void setOperacao(String operacao) {
		this.operacao = operacao;
		getFlashScope().put("operacao", operacao);
	}
}
