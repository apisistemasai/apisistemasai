package com.agrosolutions.sai.view;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.RequestScoped;
import javax.faces.event.ActionEvent;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.Fabricante;
import com.agrosolutions.sai.model.Filtro;
import com.agrosolutions.sai.model.TipoFabricante;
import com.agrosolutions.sai.model.TipoFiltro;
import com.agrosolutions.sai.service.FabricanteService;
import com.agrosolutions.sai.service.FiltroService;
import com.agrosolutions.sai.util.ResourceBundle;

/**
* Use case : Cadastrar Filtro
* 
* @since   : Abr 12, 2012, 11:10:20 AM
* @author  : rpf1404@gmail.com
*/

@Component("cadastrarFiltroBean")
@RequestScoped
public class CadastrarFiltroBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = 3836875025779951044L;

	public static Logger logger = Logger.getLogger(CadastrarFiltroBean.class);

	public static final String MSG_TITULO = "Cadastro de Filtro";

	@Autowired
	private FiltroService filtroService;
	@Autowired
	private FabricanteService fabricanteService;
	
	private Filtro filtro;
	private List<Fabricante> fabricantes;
	private Fabricante fabricante;
	private String operacao;
	
	public void carregarDadosIniciais() {
		if (operacao.equals("NOVO") && filtro == null) {
			filtro = new Filtro();
		}
		if (fabricantes == null) {
			fabricantes = fabricanteService.pesquisar(TipoFabricante.Filtro);
		}
		if (fabricante != null) {
			filtro.setFabricante(fabricante);
			fabricante = null;
		}
	}
	
	public void gravar(ActionEvent actionEvent) {
		filtro = filtroService.salvar(filtro);
		if (operacao.equals("NOVO")) {
			filtro = null;
			infoMsg(ResourceBundle.getMessage("filtroSalvar")+ ".", MSG_TITULO);
		}else{
			infoMsg(ResourceBundle.getMessage("filtroAlterar")+ ".", MSG_TITULO);
		}
    }

	public String novoFabricante() {
		return "cadastrarFabricante?faces-redirect=true";
	}

	public String voltar() {
		return "consultarFiltro?faces-redirect=true";
	}

	public Filtro getFiltro() {
		return filtro;
	}

	public void setFiltro(Filtro filtro) {
		this.filtro = filtro;
	}

	public List<Fabricante> getFabricantes() {
		return fabricantes;
	}

	public void setFabricantes(List<Fabricante> fabricantes) {
		this.fabricantes = fabricantes;
	}

	public Fabricante getFabricante() {
		return fabricante;
	}

	public void setFabricante(Fabricante fabricante) {
		this.fabricante = fabricante;
	}

	public String getOperacao() {
		return operacao;
	}

	public void setOperacao(String operacao) {
		this.operacao = operacao;
	}

	public TipoFiltro[] getTipos() {
		return TipoFiltro.values();
	}
}
