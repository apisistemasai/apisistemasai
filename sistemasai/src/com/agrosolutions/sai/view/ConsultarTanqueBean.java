package com.agrosolutions.sai.view;

import java.io.Serializable;
import java.util.List;

import org.apache.log4j.Logger;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.Tanque;
import com.agrosolutions.sai.service.TanqueService;
import com.agrosolutions.sai.util.ResourceBundle;

/**
* Use case : Consultar Tanque
* 
* @since   : Dez 13, 2015, 22:23:20 AM
* @author  : raphael@iasoft.me
*/

@Component("consultarTanqueBean")
@Scope("view")
public class ConsultarTanqueBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = -4899119053066636869L;

	public static Logger logger = Logger.getLogger(ConsultarTanqueBean.class);

	@Autowired
	private TanqueService tanqueService;
	
	private List<Tanque> tanques;
	private Tanque tanqueSelecionada;
	private List<Tanque> tanquesDestino;
	private Tanque tanqueBusca;

	public void carregarDadosIniciais() {
		
		if(tanqueBusca == null){
			tanqueBusca = new Tanque();
			tanqueBusca.setSortField("nome");
			tanqueBusca.setSortOrder(SortOrder.ASCENDING);

			this.setTanquesDestino(tanqueService.obterTodos());

			consultar();
		}
		
	}

	public void consultar(){	
		tanques = tanqueService.pesquisar(tanqueBusca, null, null);
	}
	
	public void excluir(){
	   tanqueService.excluir(tanqueSelecionada);
		infoMsg(ResourceBundle.getMessage("tanqueExcluir")+ ".", null);
   }

   public String cadastro() {
		return "cadastrarTanque?faces-redirect=true";
	}
	
	public List<Tanque> getTanques() {
		return tanques;
	}

	public void setTanques(List<Tanque> tanques) {
		this.tanques = tanques;
	}

	public Tanque getTanqueSelecionada() {
		return tanqueSelecionada;
	}

	public List<Tanque> getTanquesDestino() {
		return tanquesDestino;
	}

	public void setTanquesDestino(List<Tanque> tanquesDestino) {
		this.tanquesDestino = tanquesDestino;
	}

	public void setTanqueSelecionada(Tanque tanqueSelecionada) {
		this.tanqueSelecionada = tanqueSelecionada;
	}

	public void setTanqueBusca(Tanque tanqueBusca) {
		this.tanqueBusca = tanqueBusca;
	}

	public Tanque getTanqueBusca() {
		return tanqueBusca;
	}

}
