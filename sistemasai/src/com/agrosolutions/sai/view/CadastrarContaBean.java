package com.agrosolutions.sai.view;

import java.io.Serializable;

import javax.faces.event.ActionEvent;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.Conta;
import com.agrosolutions.sai.model.TipoConta;
import com.agrosolutions.sai.service.ContaService;
import com.agrosolutions.sai.util.ResourceBundle;

/**
 * Use case : Cadastrar Conta
 * 
 * @since : Nov 27, 2012, 17:40:20 PM
 * @author : carlos.torres@ivia.com.br
 */

@Component("cadastrarContaBean")
@Scope("session")
public class CadastrarContaBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = -2616008175923748350L;

	public static Logger logger = Logger.getLogger(CadastrarContaBean.class);

	public static final String MSG_TITULO = "Cadastro de Conta";
	@Autowired
	private ContaService contaService;

	private Conta conta;
	private String operacao;

	public void carregarDadosIniciais() {
		if (operacao.equals("NOVO") && conta == null) {
			conta = new Conta();
		}
	}

	public void gravar(ActionEvent actionEvent) {
		conta = contaService.salvar(conta);
		if (operacao.equals("NOVO")) {
			conta = null;
			infoMsg(ResourceBundle.getMessage("contaSalvar")+ ".", MSG_TITULO);
		}else{
			infoMsg(ResourceBundle.getMessage("contaAlterar")+ ".", MSG_TITULO);
		}
	}

	public String voltar() {
		return "consultarContas?faces-redirect=true";
	}

	public String getOperacao() {
		return operacao;
	}

	public void setOperacao(String operacao) {
		this.operacao = operacao;
	}

	public Conta getConta() {
		return conta;
	}

	public void setConta(Conta conta) {
		this.conta = conta;
	}

	public TipoConta[] getTipos() {
		return TipoConta.values();
	}
}
