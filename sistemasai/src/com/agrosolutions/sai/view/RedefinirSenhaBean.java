package com.agrosolutions.sai.view;

import java.io.Serializable;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.Usuario;
import com.agrosolutions.sai.service.AuthenticationService;
import com.agrosolutions.sai.service.UsuarioService;
import com.agrosolutions.sai.util.ResourceBundle;


@Component("redefinirSenhaBean")
@Scope("session")
public class RedefinirSenhaBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = 8044483992349805362L;
	@Autowired
	private AuthenticationService authenticationService;
	@Autowired
	private UsuarioService usuarioService;

	private String username;
	private String nome;
	private String senhaNova;
	private String senhaConfirma;
	private Boolean hashOk;
	
	public String carregarDadosUsuario(){
		setHashOk(true);
		HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
		Map<String, String[]> parameterMap = request.getParameterMap();
		
		if (parameterMap.get("usr") == null || parameterMap.get("hash") == null){
			setHashOk(false);
			errorMsg(ResourceBundle.getMessage("hashInvalido"), null);
			return null;
		}
		
		String idUsuario = parameterMap.get("usr")[0];
		String hash = parameterMap.get("hash")[0];
		Usuario usuario = usuarioService.pesquisarPorId(idUsuario);
		
		if (usuario.getHashCodigo() == null){
			setHashOk(false);
			errorMsg(ResourceBundle.getMessage("hashInvalido"), null);
			return null;
		}
		
		//	Adiciona o usuario na sess�o
		if (usuario.getHashCodigo().equals(hash)){
			request.getSession().setAttribute("usuario", usuario);
			request.getSession().setAttribute("username", usuario.getUsername());
			setUsername(usuario.getUsername());
			setNome(usuario.getNome());
		}else{
			errorMsg(ResourceBundle.getMessage("hashInvalido"), null);			
		}
	
		return null;
	}

	public String definirSenha() {

		Usuario usuario = null;
		
	    if (senhaNova != null && senhaConfirma != null) {
	    	 if(senhaNova.equals(senhaConfirma)){
	    		 HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
	    		 usuario = (Usuario) request.getSession().getAttribute("usuario");
	    		 usuario.setPassword(senhaNova);
	    		 usuario.setHashCodigo(null);
	    		 usuarioService.alterarSenha(usuario);
	    	 }else{
	    		 errorMsg(ResourceBundle.getMessage("senhaNaoConfere"), null);
	    		 return null;
	    	 }
	    }else{
		    errorMsg(ResourceBundle.getMessage("senhaNaoConfere"), null);
		    return null;
	    }
	    
	    //	Adiciona a msg na sessao
	    getFlashScope().put("msg", ResourceBundle.getMessage("senhaAlteradaSucesso"));
	    return "login?faces-redirect=true";
	}


	public String getSenhaNova() {
		return senhaNova;
	}

	public void setSenhaNova(String senhaNova) {
		this.senhaNova = senhaNova;
	}

	public String getSenhaConfirma() {
		return senhaConfirma;
	}

	public void setSenhaConfirma(String senhaConfirma) {
		this.senhaConfirma = senhaConfirma;
	}

	public void setHashOk(Boolean hashOk) {
		this.hashOk = hashOk;
	}

	public Boolean getHashOk() {
		return hashOk;
	}
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
}
