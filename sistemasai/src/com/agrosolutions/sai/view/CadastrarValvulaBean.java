package com.agrosolutions.sai.view;

import java.io.Serializable;
import java.util.List;

import javax.faces.event.ActionEvent;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.Fabricante;
import com.agrosolutions.sai.model.TipoFabricante;
import com.agrosolutions.sai.model.TipoValvula;
import com.agrosolutions.sai.model.Valvula;
import com.agrosolutions.sai.service.FabricanteService;
import com.agrosolutions.sai.service.ValvulaService;
import com.agrosolutions.sai.util.ResourceBundle;

/**
* Use case : Cadastrar Valvula
* 
* @since   : Maio 14, 2012, 01:59:20 AM
* @author  : walljr@gmail.com
*/

@Component("cadastrarValvulaBean")
@Scope("session")
public class CadastrarValvulaBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = -1575813616869080534L;

	public static Logger logger = Logger.getLogger(CadastrarValvulaBean.class);

	public static final String MSG_TITULO = "Cadastro de Valvula";

	@Autowired
	private ValvulaService valvulaService;
	@Autowired
	private FabricanteService fabricanteService;
	
	private Valvula valvula;
	private List<Fabricante> fabricantes;
	private Fabricante fabricante;
	private String operacao;
	
	public void carregarDadosIniciais() {
		operacao = getOperacao();
		valvula = getValvula();
		fabricante = getFabricante();
		
		if (operacao.equals("NOVO") && valvula == null) {
			valvula = new Valvula();
		}
		if (fabricantes == null) {
			fabricantes = fabricanteService.pesquisar(TipoFabricante.Valvula);
		}
		if (fabricante != null) {
			valvula.setFabricante(fabricante);
			fabricante = null;
		}
	}
	
	public void gravar(ActionEvent actionEvent) {
		valvula = valvulaService.salvar(valvula);
		if (operacao.equals("NOVO")) {
			valvula = null;
			infoMsg(ResourceBundle.getMessage("valvulaSalvar")+ ".", MSG_TITULO);
		}else{
			infoMsg(ResourceBundle.getMessage("valvulaAlterar")+ ".", MSG_TITULO);
		}
    }

	public String novoFabricante() {
		return "cadastrarFabricante?faces-redirect=true";
	}

	public String voltar() {
		return "consultarValvula?faces-redirect=true";
	}

	public Valvula getValvula() {
		return valvula;
	}

	public void setValvula(Valvula valvula) {
		this.valvula = valvula;
	}

	public List<Fabricante> getFabricantes() {
		return fabricantes;
	}

	public void setFabricantes(List<Fabricante> fabricantes) {
		this.fabricantes = fabricantes;
	}

	public Fabricante getFabricante() {
		return fabricante;
	}

	public void setFabricante(Fabricante fabricante) {
		this.fabricante = fabricante;
	}

	public String getOperacao() {
		return operacao;
	}

	public void setOperacao(String operacao) {
		this.operacao = operacao;
	}
	public TipoValvula[] getTipos() {
		return TipoValvula.values();
	}

}
