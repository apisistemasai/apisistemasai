package com.agrosolutions.sai.view;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.List;

import org.apache.log4j.Logger;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.Coordenada;
import com.agrosolutions.sai.model.Estacao;
import com.agrosolutions.sai.model.Irrigante;
import com.agrosolutions.sai.service.ClimaService;
import com.agrosolutions.sai.service.EstacaoService;
import com.agrosolutions.sai.util.KmlUtil;


@Component("importarCoordenadasBean")
@Scope("view")
public class ImportarCoordenadasBean extends ManagerBean implements Serializable {
	
	private static final long serialVersionUID = -1330155829642077030L;

	public static Logger logger = Logger.getLogger(ImportarCoordenadasBean.class);

	@Autowired
	private EstacaoService estacaoService;
	@Autowired
	private ClimaService climaService;

	private List<Estacao> estacoes;
	private Estacao estacao;
	private Irrigante irrigante;
	
	

	public void importarCoordenadas(FileUploadEvent event) {
		
		try {
			UploadedFile arq = event.getFile();
			InputStream in = new BufferedInputStream(arq.getInputstream());
			
			KmlUtil kmlUtil = new KmlUtil();
			List<Coordenada> coordenadas = kmlUtil.getCoordenadas(in);
			irrigante.setCoordenadasPoligono(coordenadas);
			
	        infoMsg(event.getFile().getFileName() + " foi carregado.", null);
		} catch (IOException e) {
	        errorMsg(event.getFile().getFileName() + " n�o foi carregado.", null);
			e.printStackTrace();
		}
	}

	public List<Estacao> getEstacoes() { 
		return estacoes;
	}

	public void setEstacoes(List<Estacao> estacoes) {
		this.estacoes = estacoes;
	}

	public Estacao getEstacao() {
		return estacao;
	}

	public void setEstacao(Estacao estacao) {
		this.estacao = estacao;
	}

	public Irrigante getIrrigante() {
		return irrigante;
	}

	public void setIrrigante(Irrigante irrigante) {
		this.irrigante = irrigante;
	}  
}
