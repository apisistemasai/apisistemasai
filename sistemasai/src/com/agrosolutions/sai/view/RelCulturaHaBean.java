package com.agrosolutions.sai.view;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.primefaces.model.StreamedContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.exception.SaiException;
import com.agrosolutions.sai.service.IrriganteService;
import com.agrosolutions.sai.util.RelatorioUtil;
import com.agrosolutions.sai.util.ResourceBundle;


@Component("relCulturaHaBean")
public class RelCulturaHaBean extends ManagerBean implements Serializable {
	
	private static final long serialVersionUID = -8320656327562741022L;
	public static Logger logger = Logger.getLogger(RelCulturaHaBean.class);
	@Autowired
	private RelatorioUtil relatorioUtil;
	@Autowired
	private IrriganteService irriganteService;
	private StreamedContent file;
	private Integer formatoRelatorio;
	private Integer consorcioSequeiro;
	private Integer lotes;
	private String relatorio;
	
	
	public String imprimir() {
		try {
			// Par�metros enviados para gerar o relat�rio
			Map<String, Object> parametros = new HashMap<String, Object>();
			parametros.put( "DISTRITO", getUsuarioLogado().getDistritos().get(0).getId().intValue() );
			
			relatorio = "rptCulturas_";
			switch (consorcioSequeiro) {
			case 1:	relatorio += "consorcio.jasper";
				break;
			case 2:	relatorio += "sequeiro.jasper";
				break;
			
				default: relatorio += "areaTotal.jasper";
				break;
			}
			
			// Imprime o relat�rio
			if ( getFormatoRelatorio() == 1 || getFormatoRelatorio() == 0){
				file = relatorioUtil.relatorio(relatorio, parametros, "relatorio_culturas.pdf");
			}else{
				file = relatorioUtil.relatorioXLS(relatorio, parametros, "relatorio_culturas.xls");
			}
		} catch (SaiException e) {
			errorMsg(ResourceBundle.getMessage("ocorreuErro")+ ":" + e.getMessage(), null);
			logger.warn("Ocorreu o seguinte erro: " + e.getMessage() + e.getCause().getMessage());
			return null;
		} catch (Exception e) {
			errorMsg(ResourceBundle.getMessage("ocorreuErro")+ ":" + e.getMessage(), null);
			logger.fatal("Ocorreu o seguinte erro: " + e.getMessage() + e.getCause().getMessage());
			return null;
		}
		infoMsg(ResourceBundle.getMessage("friRelatorio"), null);
		return null;
	}
	
	public StreamedContent getFile() {
		return file;
	}

	public void setFile(StreamedContent file) {
		this.file = file;
	}
	
	public String getRelatorio() {
		return relatorio;
	}

	public void setRelatorio(String relatorio) {
		this.relatorio = relatorio;
	}

	public Integer getConsorcioSequeiro() {
		return consorcioSequeiro;
	}

	public void setConsorcioSequeiro(Integer consorcioSequeiro) {
		this.consorcioSequeiro = consorcioSequeiro;
	}

	public Integer getLotes() {
		return lotes;
	}

	public void setLotes(Integer lotes) {
		this.lotes = lotes;
	}

	public Integer getFormatoRelatorio() {
		return formatoRelatorio;
	}

	public void setFormatoRelatorio(Integer formatoRelatorio) {
		this.formatoRelatorio = formatoRelatorio;
	}	
}