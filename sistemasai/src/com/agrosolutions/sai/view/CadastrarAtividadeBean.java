package com.agrosolutions.sai.view;

import java.io.Serializable;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.Atividade;
import com.agrosolutions.sai.service.AtividadeService;
import com.agrosolutions.sai.util.ResourceBundle;

/**
* Use case : Cadastrar Atividade
* 
* @since   : Jan 27, 2012, 03:23:20 AM
* @author  : raphael.ferreira@ivia.com.br
*/

@Component("cadastrarAtividadeBean")
@Scope("session")
public class CadastrarAtividadeBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = 8666560690340386588L;

	public static Logger logger = Logger.getLogger(CadastrarAtividadeBean.class);

	public static final String MSG_TITULO = "Cadastro de Atividade";

	@Autowired
	private AtividadeService atividadeService;
	
	private Atividade atividade;
	private Boolean cadastroCultura;
	private String operacao;
	
	public void carregarDadosIniciais() {
		operacao = getOperacao();
		atividade = getAtividade();
		cadastroCultura = getCadastroCultura();
		
		if (getOperacao().equals("NOVO") && getAtividade() == null) {
			atividade = new Atividade();
		}
	}
	
	public String gravar() {
		String volta = null;
		atividade = atividadeService.salvar(atividade); 

		if (operacao.equals("NOVO")) {
			atividade = new Atividade();
			infoMsg(ResourceBundle.getMessage("atividadeSalvar")+ ".", MSG_TITULO);
		}else{
			infoMsg(ResourceBundle.getMessage("atividadeAlterar")+ ".", MSG_TITULO);
		}

		if (cadastroCultura != null && cadastroCultura) {
			cadastroCultura = false;
			volta = "cadastrarCultura";
		}
		
		return volta;
}

	public String voltar() {
		String volta;
		volta = "consultarAtividade?faces-redirect=true";

		if (cadastroCultura != null && cadastroCultura) {
			cadastroCultura = false;
			volta = "cadastrarCultura?faces-redirect=true";
		}
		
		return volta;
	}

	public Atividade getAtividade() {
		if (atividade==null){
			atividade = (Atividade) getFlashScope().get("atividade");
		}
		return atividade;
	}

	public void setAtividade(Atividade atividade) {
		this.atividade = atividade;
		getFlashScope().put("atividade", atividade);
	}

	public Boolean getCadastroCultura() {
		if (cadastroCultura==null){
			cadastroCultura = (Boolean) getFlashScope().get("cadastroCultura");
		}
		return cadastroCultura;
	}

	public void setCadastroCultura(Boolean cadastroCultura) {
		this.cadastroCultura = cadastroCultura;
		getFlashScope().put("cadastroCultura", cadastroCultura);
	}

	public String getOperacao() {
		if (operacao == null) {
			operacao = (String) getFlashScope().get("operacao");
		}
		return operacao;
	}

	public void setOperacao(String operacao) {
		this.operacao = operacao;
		getFlashScope().put("operacao", operacao);
	}
}
