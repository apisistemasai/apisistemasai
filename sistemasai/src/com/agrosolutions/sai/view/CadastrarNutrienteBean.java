package com.agrosolutions.sai.view;

import java.io.Serializable;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.Elemento;
import com.agrosolutions.sai.model.Nutriente;
import com.agrosolutions.sai.model.TipoNutriente;
import com.agrosolutions.sai.service.ElementoService;
import com.agrosolutions.sai.service.NutrienteService;
import com.agrosolutions.sai.util.ResourceBundle;

/**
* Use case : Cadastrar Nutriente
* 
* @since   : Dez 9, 2015, 03:23:20 AM
* @author  : raphael@iasoft.me
*/

@Component("cadastrarNutrienteBean")
@Scope("session")
public class CadastrarNutrienteBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = -1474453286102960211L;

	public static Logger logger = Logger.getLogger(CadastrarNutrienteBean.class);

	@Autowired
	private NutrienteService nutrienteService;
	@Autowired
	private ElementoService elementoService;
	
	private Nutriente nutriente;
	private List<Elemento> elementos;
	private String operacao;
	
	public void carregarDadosIniciais() {
		operacao = getOperacao();
		nutriente = getNutriente();
		elementos = elementoService.obterTodos();
		if (getOperacao().equals("NOVO") && getNutriente() == null) {
			nutriente = new Nutriente();
		}
	}
	
	public String gravar() {
		String volta = null;
		nutriente = nutrienteService.salvar(nutriente); 

		if (operacao.equals("NOVO")) {
			nutriente = new Nutriente();
			infoMsg(ResourceBundle.getMessage("nutrienteSalvar"), null);
		}else{
			infoMsg(ResourceBundle.getMessage("nutrienteAlterar"), null);
		}

		return volta;
}

	public String voltar() {
		String volta = "consultarNutrientes?faces-redirect=true";

		return volta;
	}

	public Nutriente getNutriente() {
		if (nutriente==null){
			nutriente = (Nutriente) getFlashScope().get("nutriente");
		}
		return nutriente;
	}

	public void setNutriente(Nutriente nutriente) {
		this.nutriente = nutriente;
		getFlashScope().put("nutriente", nutriente);
	}

	public String getOperacao() {
		if (operacao == null) {
			operacao = (String) getFlashScope().get("operacao");
		}
		return operacao;
	}

	public void setOperacao(String operacao) {
		this.operacao = operacao;
		getFlashScope().put("operacao", operacao);
	}

	public List<Elemento> getElementos() {
		return elementos;
	}

	public void setElementos(List<Elemento> elementos) {
		this.elementos = elementos;
	}

	public TipoNutriente[] getTipos() {
		return TipoNutriente.values();
	}

}
