package com.agrosolutions.sai.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.faces.model.SelectItem;

import org.apache.log4j.Logger;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.Distrito;
import com.agrosolutions.sai.model.Escolaridade;
import com.agrosolutions.sai.model.Tecnico;
import com.agrosolutions.sai.service.TecnicoService;
import com.agrosolutions.sai.util.ResourceBundle;

/**
* Use case : Consultar Tecnico
* 
* @since   : Jan 11, 2012, 02:23:20 AM
* @author  : raphael.ferreira@ivia.com.br
*/

@Component("consultarTecnicoBean")
@Scope("view")
public class ConsultarTecnicoBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = -5188037621690407211L;

	public static Logger logger = Logger.getLogger(ConsultarTecnicoBean.class);

	public static final String MSG_TITULO = "Consultar Tecnicos";

	@Autowired
	private TecnicoService tecnicoService;
	
	private LazyDataModel<Tecnico> tecnicos;
	private Tecnico tecnicoSelecionado;
	private SelectItem[] distritos;
	private boolean multiDistrito;
	private Tecnico tecnicoBusca;
	
	public void carregarDadosIniciais() {
		
		if (tecnicoBusca == null){
			tecnicoBusca = new Tecnico();
			tecnicoBusca.setSortField("nome");
			tecnicoBusca.setSortOrder(SortOrder.ASCENDING);
			consultar();
		}
		
		List<Distrito> distritos = getUsuarioLogado().getDistritos();
		this.distritos = criarFiltroDistritos(distritos);
	}
	
	public void consultar(){
		tecnicos = new LazyDataModel<Tecnico>() {

			private static final long serialVersionUID = -4516421974865107877L;

			@Override
			public List<Tecnico> load(int first, int pageSize, String sortField,
					SortOrder sortOrder, Map<String, String> filters) {
				List<Tecnico> tecnicos = new ArrayList<Tecnico>();

				if(sortField == null){
					sortField = "nome";
					sortOrder = SortOrder.ASCENDING;
				}
		        
				tecnicoBusca.setSortField(sortField);
				tecnicoBusca.setSortOrder(sortOrder);
				
				tecnicos = tecnicoService.pesquisar(tecnicoBusca, first, pageSize);
				
				return tecnicos;
			}
		};
		int quantidade = tecnicoService.quantidade(tecnicoBusca);
	    tecnicos.setRowCount(quantidade==0?1:quantidade);
	}
	
	private SelectItem[] criarFiltroDistritos(List<Distrito> distritos)  {  
        SelectItem[] options = new SelectItem[distritos.size() + 1];  
  
        options[0] = new SelectItem("", "Todas");  
        for(int i = 0; i < distritos.size(); i++) {  
            options[i + 1] = new SelectItem(distritos.get(i).getNome(), distritos.get(i).getNome());  
        }  
  
        return options;  
    }  

	public String cadastro() {
		return "cadastrarTecnico?faces-redirect=true";
	}

	public void excluir(){
	    tecnicoService.excluir(tecnicoSelecionado);
	    setTecnicos(null);
		infoMsg(ResourceBundle.getMessage("tecnicoExcluir")+ ".", MSG_TITULO);
	}

	public SelectItem[] getDistritos() {
		return distritos;
	}

	public void setDistritos(SelectItem[] distritos) {
		this.distritos = distritos;
	}

	public boolean isMultiDistrito() {
		return multiDistrito;
	}

	public void setMultiDistrito(boolean multiDistrito) {
		this.multiDistrito = multiDistrito;
	}

	public Tecnico getTecnicoSelecionado() {
		return tecnicoSelecionado;
	}

	public void setTecnicoSelecionado(Tecnico tecnicoSelecionado) {
		this.tecnicoSelecionado = tecnicoSelecionado;
	}

	public void setTecnicos(LazyDataModel<Tecnico> tecnicos) {
		this.tecnicos = tecnicos;
	}

	public LazyDataModel<Tecnico> getTecnicos() {
		return tecnicos;
	}

	public void setTecnicoBusca(Tecnico tecnicoBusca) {
		this.tecnicoBusca = tecnicoBusca;
	}

	public Tecnico getTecnicoBusca() {
		return tecnicoBusca;
	}
	public Escolaridade[] getEscolaridades(){
		return Escolaridade.values();
	}
}
