package com.agrosolutions.sai.view;

import java.io.Serializable;

import javax.faces.event.ActionEvent;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.agrosolutions.sai.model.Praga;
import com.agrosolutions.sai.service.PragaService;
import com.agrosolutions.sai.util.ResourceBundle;

/**
* Use case : Cadastrar Praga
* 
* @since   : Jan 27, 2012, 03:23:20 AM
* @author  : raphael.ferreira@ivia.com.br
*/

@Component("cadastrarPragaBean")
@Scope("session")
public class CadastrarPragaBean extends ManagerBean implements Serializable {

	private static final long serialVersionUID = 6093427198943873399L;

	public static Logger logger = Logger.getLogger(CadastrarPragaBean.class);

	public static final String MSG_TITULO = "Cadastro de Praga";

	@Autowired
	private PragaService pragaService;
	
	private Praga praga;
	private String operacao;
	
	public void carregarDadosIniciais() {
		operacao = getOperacao();
		praga = getPraga();
		
		if (operacao.equals("NOVO") && praga == null) {
			praga = new Praga();
		}
	}
	
	public void gravar(ActionEvent actionEvent) {
		praga = pragaService.salvar(praga); 
		if (operacao.equals("NOVO")) {
			praga = null;
			infoMsg(ResourceBundle.getMessage("pragaSalvar")+ ".", MSG_TITULO);
		}else{
			infoMsg(ResourceBundle.getMessage("pragaAlterar")+ ".", MSG_TITULO);
		}
	}
	public String voltar() {
		return "consultarPraga?faces-redirect=true";
	}

	public Praga getPraga() {
		if (praga == null) {
			praga = (Praga) getFlashScope().get("praga");
		}
		return praga;
	}

	public void setPraga(Praga praga) {
		this.praga = praga;
		getFlashScope().put("praga", praga);
	}
	public String getOperacao() {
		if (operacao == null) {
			operacao = (String) getFlashScope().get("operacao");
		}
		return operacao;
	}

	public void setOperacao(String operacao) {
		this.operacao = operacao;
		getFlashScope().put("operacao", operacao);
	}
}
