package com.agrosolutions.sai.exception;

import java.util.List;

/**
 * Exce��o para propagar mensagens de valida��o de regras de neg�cio Como a
 * exce��o herda de RuntimeException, o container faz rollback da transa��o
 * corrente automaticamente.
 * 
 * @author Ivia
 */
public class SaiException extends RuntimeException {


	private static final long serialVersionUID = 1057944920073266703L;
	private List<String> messages;

	public SaiException(Throwable throwable) {
		super(throwable);
	}

	public SaiException(String message) {
		super(message);
	}

	public SaiException(String message, Exception exception) {
		super(message, exception);
	}

	public SaiException(List<String> messages) {
		this.messages = messages;
	}

	public List<String> getMessages() {
		return messages;
	}

	public void setMessages(List<String> messages) {
		this.messages = messages;
	}
}
