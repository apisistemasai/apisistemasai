package com.agrosolutions.sai.dao;

import java.util.List;

import com.agrosolutions.sai.model.Area;
import com.agrosolutions.sai.model.Cultura;
import com.agrosolutions.sai.model.Distrito;
import com.agrosolutions.sai.model.Irrigante;
import com.agrosolutions.sai.model.TipoIrrigante;
import com.agrosolutions.sai.model.Usuario;
import com.agrosolutions.sai.model.Variedade;

public interface IrriganteDAO extends DAO<Irrigante> {

	void excluir(Irrigante irrigante);

	Irrigante pesquisarPorUsuario(Usuario usu);

	List<Irrigante> pesquisarPorCultura(Irrigante i, Integer first, Integer pageSize);

	List<Irrigante> pesquisarPorVariedade(Irrigante i, Integer first, Integer pageSize);

	Long qtdPorVariedade(Variedade v, TipoIrrigante tipo, Area area, List<Distrito> distritos,
			Integer first, Integer pageSize);

	Long qtdPorCultura(Cultura v, TipoIrrigante tipo, Area area, List<Distrito> distritos,
			Integer first, Integer pageSize);

	Irrigante pesquisarPorLote(String lote, List<Distrito> distritos);

	List<Irrigante> pesquisarPorDistrito(List<Distrito> distritos);

	List<Irrigante> pesquisarPorDistrito(List<Distrito> distritos,
			Integer indiceInicio, Integer quantidade);

	Long totalIrrigantePorDistrito(List<Distrito> distritos);

	List<Irrigante> pesquisarPorArea(Area area);
	
}
