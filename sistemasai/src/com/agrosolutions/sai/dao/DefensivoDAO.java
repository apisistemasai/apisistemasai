package com.agrosolutions.sai.dao;

import com.agrosolutions.sai.model.Defensivo;

public interface DefensivoDAO extends DAO<Defensivo> {

	void excluir(Defensivo defensivo);

}
