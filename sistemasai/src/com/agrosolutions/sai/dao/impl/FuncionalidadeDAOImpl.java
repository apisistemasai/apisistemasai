package com.agrosolutions.sai.dao.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.agrosolutions.sai.dao.FuncionalidadeDAO;
import com.agrosolutions.sai.model.Funcionalidade;

@Repository
public class FuncionalidadeDAOImpl extends DAOImpl<Funcionalidade> implements FuncionalidadeDAO {

	private static final long serialVersionUID = -2567305953238519772L;
	static Logger logger = Logger.getLogger(FuncionalidadeDAOImpl.class);

    @Override
    public List<Funcionalidade> obterTodos() {
    	return executarQuery("Funcionalidade.todos", null, null, null);
    }	
}
