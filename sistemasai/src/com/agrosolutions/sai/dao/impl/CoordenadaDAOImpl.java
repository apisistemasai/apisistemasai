package com.agrosolutions.sai.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.agrosolutions.sai.dao.CoordenadaDAO;
import com.agrosolutions.sai.exception.SaiException;
import com.agrosolutions.sai.model.Coordenada;
import com.agrosolutions.sai.model.Irrigante;

@Repository
public class CoordenadaDAOImpl extends DAOImpl<Coordenada> implements CoordenadaDAO {
	
	private static final long serialVersionUID = -3482342300376253919L;
	static Logger logger = Logger.getLogger(CoordenadaDAOImpl.class);
	
	public void salvar(List<Coordenada> coordenadas) {
		try {
			super.salvar(coordenadas);
		} catch (Exception e) {
			e.getStackTrace();
		} 
	}
	
	@Override
	public List<Coordenada> pesquisarPorIrrigante(Irrigante irrigante) {
		List<Coordenada> coordenadas = new ArrayList<Coordenada>();
		try {
			String jpql = "SELECT c FROM Irrigante i INNER JOIN i.coordenadasPoligono c WHERE i = :pIrrigante ORDER BY c.ordem ASC";
			coordenadas = this.entityManager.createQuery(jpql, Coordenada.class).setParameter("pIrrigante", irrigante).getResultList();			
		} catch (SaiException e) {
			e.getMessages();
			e.getStackTrace();
		}
		return coordenadas;
	}

    
}
