package com.agrosolutions.sai.dao.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.collections.map.HashedMap;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.primefaces.model.SortOrder;
import org.springframework.stereotype.Repository;

import com.agrosolutions.sai.dao.VariedadeDAO;
import com.agrosolutions.sai.model.Cultura;
import com.agrosolutions.sai.model.Variedade;

@Repository
public class VariedadeDAOImpl extends DAOImpl<Variedade> implements VariedadeDAO {

	private static final long serialVersionUID = 794437734522707802L;
	static Logger logger = Logger.getLogger(VariedadeDAOImpl.class);

	@Override
	public List<Variedade> pesquisar(Cultura cultura) {
		@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
		parametros.put("pCultura", cultura);

		return executarQuery("Variedade.pesquisar", parametros, null, null);
	}

	@Override
    public void excluir(Variedade variedade) {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
    	parametros.put("pId",variedade.getId());
        executarComando("Variedade.excluir", parametros);
    }	

    @Override
    public List<Variedade> obterTodos() {
    	return executarQuery("Variedade.todos", null, null, null);
    }	
    @Override
	public Criteria criarCriteriaExample(Variedade entidade, MatchMode match) {

		Criteria criteria = super.criarCriteriaExample(entidade, match);
				
		criteria = prepararCriteria(entidade, criteria);		
		return criteria;
	}

	private Criteria prepararCriteria(Variedade entidadeLocal, Criteria criteria) {
		Criteria c = null;
		if (entidadeLocal.getCultura() != null) {
			c = criteria.createCriteria("cultura");
			c.add(Restrictions.eq("id", entidadeLocal.getCultura().getId()));
		}
		if (entidadeLocal.getSortField().equals("nome")) {
			if (entidadeLocal.getSortOrder().equals(SortOrder.DESCENDING)) {
				criteria.addOrder(Order.desc("nome"));
			}else{
				criteria.addOrder(Order.asc("nome"));
			}
		}
		return criteria;
	}
}
