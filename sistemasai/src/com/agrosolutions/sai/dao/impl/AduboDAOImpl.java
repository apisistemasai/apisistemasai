package com.agrosolutions.sai.dao.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.collections.map.HashedMap;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.primefaces.model.SortOrder;
import org.springframework.stereotype.Repository;

import com.agrosolutions.sai.dao.AduboDAO;
import com.agrosolutions.sai.model.Adubo;

@Repository
public class AduboDAOImpl extends DAOImpl<Adubo> implements AduboDAO {

	private static final long serialVersionUID = -8414124749154726843L;
	static Logger logger = Logger.getLogger(AduboDAOImpl.class);
	
    @Override
    public void excluir(Adubo adubo) {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
    	parametros.put("pId",adubo.getId());
        executarComando("Adubo.excluir", parametros);
    }	

    @Override
    public List<Adubo> obterTodos() {
    	return executarQuery("Adubo.todos", null, null, null);
    }
    @Override
	public Criteria criarCriteriaExample(Adubo entidade, MatchMode match) {

		Criteria criteria = super.criarCriteriaExample(entidade, match);
				
		criteria = prepararCriteria(entidade, criteria);		
		return criteria;
	}

	private Criteria prepararCriteria(Adubo entidadeLocal, Criteria criteria) {
		if (entidadeLocal.getSortField().equals("nome")) {
			if (entidadeLocal.getSortOrder().equals(SortOrder.DESCENDING)) {
				criteria.addOrder(Order.desc("nome"));
			}else{
				criteria.addOrder(Order.asc("nome"));
			}
		}
		return criteria;
	}
}
