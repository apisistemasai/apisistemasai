package com.agrosolutions.sai.dao.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.collections.map.HashedMap;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.primefaces.model.SortOrder;
import org.springframework.stereotype.Repository;

import com.agrosolutions.sai.dao.EstacaoDAO;
import com.agrosolutions.sai.model.Distrito;
import com.agrosolutions.sai.model.Estacao;

@Repository
public class EstacaoDAOImpl extends DAOImpl<Estacao> implements EstacaoDAO {
	
	private static final long serialVersionUID = 6081765224325112212L;
	static Logger logger = Logger.getLogger(EstacaoDAOImpl.class);

	@Override
	public List<Estacao> pesquisar(List<Distrito> distritos) {
		@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
		
		parametros.put("pDistrito", distritos);

		return executarQuery("Estacao.pesquisar", parametros, null, null);
	}
	
	@Override
    public void excluir(Estacao estacao) {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
    	parametros.put("pId",estacao.getId());
        executarComando("Estacao.excluir", parametros);
    }	

    @Override
	public Criteria criarCriteriaExample(Estacao entidade, MatchMode match) {

		Criteria criteria = super.criarCriteriaExample(entidade, match);
				
		criteria = prepararCriteria(entidade, criteria);		
		return criteria;
	}

	private Criteria prepararCriteria(Estacao entidadeLocal, Criteria criteria) {
		criteria = criteria.add(Restrictions.in("distrito", entidadeLocal.getDistritos()));

		if (entidadeLocal.getSortField().equals("nome")) {
			if (entidadeLocal.getSortOrder().equals(SortOrder.DESCENDING)) {
				criteria.addOrder(Order.desc("nome"));
			}else{
				criteria.addOrder(Order.asc("nome"));
			}
		}
		return criteria;	
	}
}
