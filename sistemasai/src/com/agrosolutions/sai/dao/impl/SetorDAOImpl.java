package com.agrosolutions.sai.dao.impl;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.map.HashedMap;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
 
import com.agrosolutions.sai.dao.SetorDAO;
import com.agrosolutions.sai.model.Area;
import com.agrosolutions.sai.model.Distrito;
import com.agrosolutions.sai.model.Irrigante;
import com.agrosolutions.sai.model.Setor;

@Repository
public class SetorDAOImpl extends DAOImpl<Setor> implements SetorDAO {

	private static final long serialVersionUID = 4015430898225638888L;
	static Logger logger = Logger.getLogger(SetorDAOImpl.class);

	@Override
	public List<Setor> pesquisar(Irrigante irrigante) {
		@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
		parametros.put("pIrrigante", irrigante);

		return executarQuery("Setor.pesquisar", parametros, null, null);
	}

	@Override
    public void excluir(Setor setor) {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
    	parametros.put("pId",setor.getId());
        executarComando("Setor.excluir", parametros);
    }	
	
    @Override
    public Long totalPorArea(Area a) {
    	String jpql = "SELECT COUNT(s) FROM Setor s JOIN s.irrigante i WHERE i.area = :pArea";
    	return this.entityManager.createQuery(jpql, Long.class)
			.setParameter("pArea", a).getSingleResult();    
	}	
    @Override
    public Long totalPorDistrito(Distrito d) {
    	String jpql = "SELECT COUNT(s) FROM Setor s JOIN s.irrigante i JOIN i.area a WHERE a.distrito = :pDistrito";
    	return this.entityManager.createQuery(jpql, Long.class)
			.setParameter("pDistrito", d).getSingleResult();    
	}	

    @Override
    public BigDecimal totalHecPorArea(Area a) {
    	String jpql = "SELECT SUM(s.areaTotal) FROM Setor s JOIN s.irrigante i WHERE i.area = :pArea";
    	return this.entityManager.createQuery(jpql, BigDecimal.class)
			.setParameter("pArea", a).getSingleResult();    
	}	
    @Override
    public BigDecimal totalHecPorDistrito(Distrito d) {
    	String jpql = "SELECT SUM(s.areaTotal) FROM Setor s JOIN s.irrigante i JOIN i.area a WHERE a.distrito = :pDistrito";
    	return this.entityManager.createQuery(jpql, BigDecimal.class)
			.setParameter("pDistrito", d).getSingleResult();    
	}
    
    @Override
    public BigDecimal totalHecComPlantioPorDistrito(Distrito d) {
    	String jpql = "SELECT SUM(s.areaTotal) FROM Setor s JOIN s.irrigante i JOIN i.area a WHERE a.distrito = :pDistrito AND s in (select distinct p.setor from Plantio p where p.setor = s)";
    	return this.entityManager.createQuery(jpql, BigDecimal.class)
			.setParameter("pDistrito", d).getSingleResult();    
	}
     
    @Override
    public BigDecimal totalHecComPlantioPorArea(Area a) {
    	String jpql = "SELECT SUM(s.areaTotal) FROM Setor s JOIN s.irrigante i WHERE i.area = :pArea AND s in (select distinct p.setor from Plantio p where p.setor = s)";
    	return this.entityManager.createQuery(jpql, BigDecimal.class)
			.setParameter("pArea", a).getSingleResult();    
	}
    

    /**
     * N�o inclui sequeiro e consorcio
     */
    @Override
    public BigDecimal totalHecComPlantioIrrigadoPorDistrito(Distrito d) {
    	String jpql = "SELECT sum(p.areatotal) FROM Plantio p JOIN p.setor s JOIN s.irrigante i JOIN i.area a WHERE a.distrito = :pDistrito AND p.dataFim IS NULL AND p.consorcio = false AND p.sequeiro = false";
    	
    	return this.entityManager.createQuery(jpql, BigDecimal.class)
			.setParameter("pDistrito", d).getSingleResult();    
	}
}
