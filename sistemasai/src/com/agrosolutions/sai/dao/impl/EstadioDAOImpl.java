package com.agrosolutions.sai.dao.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.collections.map.HashedMap;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.agrosolutions.sai.dao.EstadioDAO;
import com.agrosolutions.sai.model.Estadio;
import com.agrosolutions.sai.model.Referencia;

@Repository
public class EstadioDAOImpl extends DAOImpl<Estadio> implements EstadioDAO {

	private static final long serialVersionUID = -3557797338176827808L;
	static Logger logger = Logger.getLogger(EstadioDAOImpl.class);

	@Override
	public List<Estadio> pesquisar(Referencia param) {
		@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
		parametros.put("pReferencia", param);

		return executarQuery("Estadio.pesquisar", parametros, null, null);
	}

	@Override
    public void excluir(Estadio e) {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
    	parametros.put("pId",e.getId());
        executarComando("Estadio.excluir", parametros);
    }	
	
}
