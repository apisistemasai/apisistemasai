package com.agrosolutions.sai.dao.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.map.HashedMap;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.agrosolutions.sai.dao.ClimaDAO;
import com.agrosolutions.sai.model.Area;
import com.agrosolutions.sai.model.Clima;
import com.agrosolutions.sai.model.Distrito;
import com.agrosolutions.sai.model.Estacao;

@Repository
public class ClimaDAOImpl extends DAOImpl<Clima> implements ClimaDAO {

	private static final long serialVersionUID = -6314670750677813729L;
	static Logger logger = Logger.getLogger(ClimaDAOImpl.class);

	@Override
	public List<Clima> pesquisarPorArea(Area area) {
		@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
		parametros.put("pArea", area);

		return executarQuery("Clima.pesquisarPorArea", parametros, null, null);
	}

	@Override
	public Clima ultimoPorArea(Area area) {
		@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
		parametros.put("pArea", area);

		return executarQuery("Clima.ultimoPorArea", parametros);
	}

	@Override
	public List<Clima> ultimoPorDistrito(Distrito dist) {
		@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
		parametros.put("pDistrito", dist);

		return executarQuery("Clima.ultimoPorDistrito", parametros, null, null);
	}

	@Override
	public Clima pesquisarPorData(Area area, Date data) {
		@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
		parametros.put("pData", data);
		parametros.put("pArea", area);

		return executarQuery("Clima.pesquisarPorData", parametros);
	}
	
	@Override
	public List<Clima> pesquisarPorPeriodo(Area area, Date dataIni, Date dataFim) {
		@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
		parametros.put("pDataIni", dataIni);
		parametros.put("pDataFim", dataFim);
		parametros.put("pArea", area);

		return executarQuery("Clima.pesquisarPorPeriodo", parametros, null, null);
	}

	@Override
	public List<Clima> estacaoPorPeriodo(Estacao estacao, Date dataIni, Date dataFim) {
		@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
		parametros.put("pDataIni", dataIni);
		parametros.put("pDataFim", dataFim);
		parametros.put("pEstacao", estacao);

		return executarQuery("Clima.estacaoPorPeriodo", parametros, null, null);
	}
}
