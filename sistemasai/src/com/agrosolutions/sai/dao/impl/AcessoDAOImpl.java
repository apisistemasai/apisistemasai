package com.agrosolutions.sai.dao.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.collections.map.HashedMap;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.agrosolutions.sai.dao.AcessoDAO;
import com.agrosolutions.sai.model.Acesso;
import com.agrosolutions.sai.model.Usuario;

@Repository
public class AcessoDAOImpl extends DAOImpl<Acesso> implements AcessoDAO {

	private static final long serialVersionUID = -6234289602596359158L;
	static Logger logger = Logger.getLogger(AcessoDAOImpl.class);

	@Override
	public Acesso pesquisarUltimoAcesso(String u) {
		@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
		parametros.put("pUsuario", u);
		return executarQuery("Acesso.ultimoPorUsuario", parametros);
	}

	@Override
	public List<Acesso> pesquisarPorUsuario(Usuario u) {
		@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
		parametros.put("pUsuario", u.getUsername());

		return executarQuery("Acesso.pesquisarPorUsuario", parametros, null, null);
	}

}
