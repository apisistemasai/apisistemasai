package com.agrosolutions.sai.dao.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.collections.map.HashedMap;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.agrosolutions.sai.dao.ClienteDAO;
import com.agrosolutions.sai.model.Cliente;

@Repository 
public class ClienteDAOImpl extends DAOImpl<Cliente> implements ClienteDAO {
	
	private static final long serialVersionUID = -1044042250168510411L;
	static Logger logger = Logger.getLogger(CategoriaDAOImpl.class);
	
    @Override
    public void excluir(Cliente c) {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
    	parametros.put("pId",c.getId());
        executarComando("Cliente.excluir", parametros);
    }	

    @Override
    public List<Cliente> obterTodos() {
    	return executarQuery("Cliente.todos", null, null, null);
    }	
 
}
