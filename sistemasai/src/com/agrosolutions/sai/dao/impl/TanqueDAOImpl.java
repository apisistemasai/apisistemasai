package com.agrosolutions.sai.dao.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.collections.map.HashedMap;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.primefaces.model.SortOrder;
import org.springframework.stereotype.Repository;

import com.agrosolutions.sai.dao.TanqueDAO;
import com.agrosolutions.sai.model.Tanque;

@Repository
public class TanqueDAOImpl extends DAOImpl<Tanque> implements TanqueDAO {

	private static final long serialVersionUID = -2235178161972456251L;
	static Logger logger = Logger.getLogger(TanqueDAOImpl.class);

    @Override
    public void excluir(Tanque elemento) {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
    	parametros.put("pId",elemento.getId());
        executarComando("Tanque.excluir", parametros);
    }	

    @Override
    public List<Tanque> obterTodos() {
    	return executarQuery("Tanque.todos", null, null, null);
    }
    
    @Override
	public Criteria criarCriteriaExample(Tanque entidade, MatchMode match) {

		Criteria criteria = super.criarCriteriaExample(entidade, match);
				
		criteria = prepararCriteria(entidade, criteria);		
		return criteria;
	}

	private Criteria prepararCriteria(Tanque entidadeLocal, Criteria criteria) {
		if (entidadeLocal.getSortField().equals("nome")) {
			if (entidadeLocal.getSortOrder().equals(SortOrder.DESCENDING)) {
				criteria.addOrder(Order.desc("nome"));
			}else{
				criteria.addOrder(Order.asc("nome"));
			}
		}
		return criteria;
		
	}
}
