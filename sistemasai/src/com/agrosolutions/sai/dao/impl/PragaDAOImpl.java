package com.agrosolutions.sai.dao.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.collections.map.HashedMap;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.primefaces.model.SortOrder;
import org.springframework.stereotype.Repository;

import com.agrosolutions.sai.dao.PragaDAO;
import com.agrosolutions.sai.model.Praga;

@Repository
public class PragaDAOImpl extends DAOImpl<Praga> implements PragaDAO {

	private static final long serialVersionUID = -2088804064417870806L;
	static Logger logger = Logger.getLogger(PragaDAOImpl.class);
	
    @Override
    public void excluir(Praga praga) {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
    	parametros.put("pId",praga.getId());
        executarComando("Praga.excluir", parametros);
    }	

    @Override
    public List<Praga> obterTodos() {
    	return executarQuery("Praga.todos", null, null, null);
    }	
    
    @Override
	public Criteria criarCriteriaExample(Praga entidade, MatchMode match) {
		Criteria criteria = super.criarCriteriaExample(entidade, match);
		criteria = prepararCriteria(entidade, criteria);		
		return criteria;
	}

	private Criteria prepararCriteria(Praga entidadeLocal, Criteria criteria) {
		if (entidadeLocal.getSortField().equals("nome")) {
			if (entidadeLocal.getSortOrder().equals(SortOrder.DESCENDING)) {
				criteria.addOrder(Order.desc("nome"));
			}else{
				criteria.addOrder(Order.asc("nome"));
			}
		}
		return criteria;
	}
}
