package com.agrosolutions.sai.dao.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.collections.map.HashedMap;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.primefaces.model.SortOrder;
import org.springframework.stereotype.Repository;

import com.agrosolutions.sai.dao.DoencaDAO;
import com.agrosolutions.sai.model.Doenca;

@Repository
public class DoencaDAOImpl extends DAOImpl<Doenca> implements DoencaDAO {

	private static final long serialVersionUID = 3116663534848562947L;
	static Logger logger = Logger.getLogger(DoencaDAOImpl.class);
	
    @Override
    public void excluir(Doenca doenca) {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
    	parametros.put("pId",doenca.getId());
        executarComando("Doenca.excluir", parametros);
    }	

    @Override
    public List<Doenca> obterTodos() {
    	return executarQuery("Doenca.todos", null, null, null);
    }	
    
    @Override
	public Criteria criarCriteriaExample(Doenca entidade, MatchMode match) {

		Criteria criteria = super.criarCriteriaExample(entidade, match);
				
		criteria = prepararCriteria(entidade, criteria);		
		return criteria;
	}

	private Criteria prepararCriteria(Doenca entidadeLocal, Criteria criteria) {
		if (entidadeLocal.getSortField().equals("nome")) {
			if (entidadeLocal.getSortOrder().equals(SortOrder.DESCENDING)) {
				criteria.addOrder(Order.desc("nome"));
			}else{
				criteria.addOrder(Order.asc("nome"));
			}
		}
		return criteria;
		
	}
}
