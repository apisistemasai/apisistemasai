package com.agrosolutions.sai.dao.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.collections.map.HashedMap;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.primefaces.model.SortOrder;
import org.springframework.stereotype.Repository;

import com.agrosolutions.sai.dao.DefensivoDAO;
import com.agrosolutions.sai.model.Defensivo;

@Repository
public class DefensivoDAOImpl extends DAOImpl<Defensivo> implements DefensivoDAO {

	private static final long serialVersionUID = -8083098823024911195L;
	static Logger logger = Logger.getLogger(DefensivoDAOImpl.class);
	
    @Override
    public void excluir(Defensivo defensivo) {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
    	parametros.put("pId",defensivo.getId());
        executarComando("Defensivo.excluir", parametros);
    }	

    @Override
    public List<Defensivo> obterTodos() {
    	return executarQuery("Defensivo.todos", null, null, null);
    }
    @Override
	public Criteria criarCriteriaExample(Defensivo entidade, MatchMode match) {
		Criteria criteria = super.criarCriteriaExample(entidade, match);
		criteria = prepararCriteria(entidade, criteria);		
		return criteria;
	}

	private Criteria prepararCriteria(Defensivo entidadeLocal, Criteria criteria) {
		if (entidadeLocal.getSortField().equals("nome")) {
			if (entidadeLocal.getSortOrder().equals(SortOrder.DESCENDING)) {
				criteria.addOrder(Order.desc("nome"));
			}else{
				criteria.addOrder(Order.asc("nome"));
			}
		}
		return criteria;
	}
}
