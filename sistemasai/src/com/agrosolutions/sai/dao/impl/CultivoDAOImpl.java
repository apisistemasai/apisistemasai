package com.agrosolutions.sai.dao.impl;

import java.util.List;
import java.util.Map;

import javax.persistence.Query;

import org.apache.commons.collections.map.HashedMap;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.agrosolutions.sai.dao.CultivoDAO;
import com.agrosolutions.sai.model.Cultivo;
import com.agrosolutions.sai.model.Cultura;
import com.agrosolutions.sai.model.Irrigante;
import com.agrosolutions.sai.model.Variedade;

@Repository
public class CultivoDAOImpl extends DAOImpl<Cultivo> implements CultivoDAO {

	private static final long serialVersionUID = 5493318289657198353L;
	static Logger logger = Logger.getLogger(CultivoDAOImpl.class);
	
    @Override
    public void excluir(Cultivo cultivo) {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
    	parametros.put("pId",cultivo.getId());
        executarComando("Cultivo.excluir", parametros);
    }	

	@Override
	public List<Cultivo> pesquisar(Irrigante irrigante) {
		@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
		parametros.put("pIrrigante", irrigante);

		return executarQuery("Cultivo.pesquisar", parametros, null, null);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Cultura> pesquisarCulturas(Irrigante irrigante) {
		Query queryNamed = entityManager.createNamedQuery("Cultivo.pesquisarCulturas");

		queryNamed.setParameter("pIrrigante", irrigante);
		return queryNamed.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Variedade> pesquisarVariedades(Irrigante irrigante) {
		Query queryNamed = entityManager.createNamedQuery("Cultivo.pesquisarVariedades");

		queryNamed.setParameter("pIrrigante", irrigante);
		return queryNamed.getResultList();
	}
}
