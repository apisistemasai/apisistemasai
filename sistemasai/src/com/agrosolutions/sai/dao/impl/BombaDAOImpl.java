package com.agrosolutions.sai.dao.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.collections.map.HashedMap;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.primefaces.model.SortOrder;
import org.springframework.stereotype.Repository;

import com.agrosolutions.sai.dao.BombaDAO;
import com.agrosolutions.sai.model.Bomba;
import com.agrosolutions.sai.model.Fabricante;
import com.agrosolutions.sai.model.Irrigante;

@Repository
public class BombaDAOImpl extends DAOImpl<Bomba> implements BombaDAO {

	private static final long serialVersionUID = -1449592022338314105L;
	static Logger logger = Logger.getLogger(BombaDAOImpl.class);

	@Override
	public List<Bomba> pesquisar(Fabricante fabricante) {
		@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
		parametros.put("pFabricante", fabricante);

		return executarQuery("Bomba.pesquisar", parametros, null, null);
	}

	@Override
	public List<Bomba> pesquisarPorIrrigante(Irrigante irrigante) {
		@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
		parametros.put("pIrrigante", irrigante);

		return executarQuery("Bomba.pesquisarPorIrrigante", parametros, null, null);
	}

	@Override
    public void excluir(Bomba bomba) {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
    	parametros.put("pId",bomba.getId());
        executarComando("Bomba.excluir", parametros);
    }	

    @Override
    public List<Bomba> obterTodos() {
    	return executarQuery("Bomba.todos", null, null, null);
    }

	@Override
	public Criteria criarCriteriaExample(Bomba entidade, MatchMode match) {

		Criteria criteria = super.criarCriteriaExample(entidade, match);
				
		criteria = prepararCriteria(entidade, criteria);		
		return criteria;
	}

	private Criteria prepararCriteria(Bomba entidadeLocal, Criteria criteria) {
		Criteria a = null;
		if (entidadeLocal.getFabricante() != null) {
			a = criteria.createCriteria("fabricante");
			a.add(Restrictions.eq("id", entidadeLocal.getFabricante().getId()));
		}
		
		if (entidadeLocal.getSortField() != null && entidadeLocal.getSortField().equals("modelo")) {
			if (entidadeLocal.getSortOrder().equals(SortOrder.DESCENDING)) {
				criteria.addOrder(Order.desc("modelo"));
			}else{
				criteria.addOrder(Order.asc("modelo"));
			}
		}else{
			criteria.addOrder(Order.asc("modelo"));
		}

		return criteria;
	}
    
}
