package com.agrosolutions.sai.dao.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.collections.map.HashedMap;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.agrosolutions.sai.dao.ParametroDAO;
import com.agrosolutions.sai.model.Parametro;

@Repository
public class ParametroDAOImpl extends DAOImpl<Parametro> implements ParametroDAO {
	
	private static final long serialVersionUID = -224391486790278834L;
	
	static Logger logger = Logger.getLogger(ParametroDAOImpl.class);

	@SuppressWarnings("unchecked")
	public Parametro pesquisarPorCodigo(String codigo) {
		Map<String, Object> parametros = new HashedMap();
		parametros.put("pCodigo", codigo);

		return executarQuery("Parametro.porCodigo", parametros);
		
	}

	@Override
    public List<Parametro> obterTodos() {
    	return executarQuery("Parametro.todos", null, null, null);
    }	

}
