package com.agrosolutions.sai.dao.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.collections.map.HashedMap;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.primefaces.model.SortOrder;
import org.springframework.stereotype.Repository;

import com.agrosolutions.sai.dao.AreaDAO;
import com.agrosolutions.sai.model.Area;
import com.agrosolutions.sai.model.Distrito;

@Repository
public class AreaDAOImpl extends DAOImpl<Area> implements AreaDAO {
	
	private static final long serialVersionUID = 371181965971838289L;
	static Logger logger = Logger.getLogger(AreaDAOImpl.class);

	@Override
	public List<Area> pesquisar(List<Distrito> distritos) {
		@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
		
		parametros.put("pDistrito", distritos);
		return executarQuery("Area.pesquisar", parametros, null, null);
	}
	
	@Override
    public void excluir(Area area) {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
    	parametros.put("pId",area.getId());
        executarComando("Area.excluir", parametros);
    }	
	
	 @Override
		public Criteria criarCriteriaExample(Area entidade, MatchMode match) {

			Criteria criteria = super.criarCriteriaExample(entidade, match);
					
			criteria = prepararCriteria(entidade, criteria);		
			return criteria;
		}

		private Criteria prepararCriteria(Area entidadeLocal, Criteria criteria) {

			criteria = criteria.add(Restrictions.in("distrito", entidadeLocal.getDistritos()));

			if (entidadeLocal.getSortField().equals("nome")) {
				if (entidadeLocal.getSortOrder().equals(SortOrder.DESCENDING)) {
					criteria.addOrder(Order.desc("nome"));
				}else{
					criteria.addOrder(Order.asc("nome"));
				}
			}
			return criteria;	
		}
}
