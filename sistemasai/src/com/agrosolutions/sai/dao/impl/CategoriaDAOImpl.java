package com.agrosolutions.sai.dao.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.collections.map.HashedMap;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.agrosolutions.sai.dao.CategoriaDAO;
import com.agrosolutions.sai.model.Categoria;

@Repository
public class CategoriaDAOImpl extends DAOImpl<Categoria> implements CategoriaDAO {
	
	private static final long serialVersionUID = -7666411754607654416L;
	static Logger logger = Logger.getLogger(CategoriaDAOImpl.class);
	
    @Override
    public void excluir(Categoria c) {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
    	parametros.put("pId",c.getId());
        executarComando("Categoria.excluir", parametros);
    }	

    @Override
    public List<Categoria> obterTodos() {
    	return executarQuery("Categoria.todos", null, null, null);
    }	
 
    @Override
    public List<Categoria> obterSuperiores() {
    	return executarQuery("Categoria.pai", null, null, null);
    }	
}
