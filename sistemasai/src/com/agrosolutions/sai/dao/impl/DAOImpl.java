package com.agrosolutions.sai.dao.impl;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.ejb.EntityManagerImpl;
import org.springframework.stereotype.Repository;

import com.agrosolutions.sai.dao.DAO;
import com.agrosolutions.sai.model.Model;

/**
 *
 * @author Raphael
 */
@Repository
public class DAOImpl<Entidade extends Model> implements DAO<Entidade> {

	private static final long serialVersionUID = 7538940694726270539L;

	@PersistenceContext
	protected EntityManager entityManager;
	
	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	public Entidade salvar(Entidade entidade) {
		if (entidade.getId() != null) {
			return entityManager.merge(entidade);
		}
		entityManager.persist(entidade);
		return entidade;
	}

	public void salvar(List<Entidade> entidades) {
		for (Entidade entidade : entidades) {
			salvar(entidade);
		}
		entityManager.flush();
	}

	public void remover(Entidade entidade) {
		entityManager.remove(entidade);
		entityManager.flush();
	}

	public void remover(List<Entidade> entidades) {
		for (Entidade entidade : entidades) {
			remover(entidade);
		}
	}

	public List<Entidade> obterTodos() {
		return entityManager.createQuery("FROM " + getClasseEntidade().getSimpleName(), getClasseEntidade()).getResultList();
	}

	public Entidade obterPorId(Long idEntidade) {
		return entityManager.find(getClasseEntidade(), idEntidade);
	}

	@SuppressWarnings("unchecked")
	public List<Entidade> obterPorParametro(Entidade entidade, Integer indiceInicio, Integer quantidade) {
		Criteria criterio = criarCriteriaExample(entidade, MatchMode.START);
		if(indiceInicio!=null){
			criterio.setFirstResult(indiceInicio);
			criterio.setMaxResults(quantidade);
		}
		return criterio.list();
	}

	public Entidade obterPorReferencia(Long id) {
		return this.entityManager.getReference(getClasseEntidade(),id);
	}	
	public int quantidade(Entidade entidade) {
		return ((Number) criarCriteriaExample(entidade, MatchMode.ANYWHERE).setProjection(Projections.rowCount()).uniqueResult()).intValue();
	}

	/**
	 * Cria um criteria e adiciona a entidade passada por parametro como exemplo para a pesquisa.
	 * Este metodo deve ser sobrescrito para adicionar outros parametros na busca.
	 *
	 * @param entidade
	 * @return
	 */
	protected Criteria criarCriteriaExample(Entidade entidade, MatchMode match) {
		Example criteriaExample = Example.create(entidade);
		return getSession().createCriteria(getClasseEntidade()).add(criteriaExample.enableLike(match).ignoreCase().excludeZeroes());
	}

	/**
	 * Obtem a classe do conjunto de entidades que o ManagerBean corrente gerencia
	 *
	 * @return
	 */
	@SuppressWarnings("unchecked")
	protected Class<Entidade> getClasseEntidade() {
		return (Class<Entidade>) ((java.lang.reflect.ParameterizedType) this.getClass().getGenericSuperclass())
				.getActualTypeArguments()[0];
	}

	protected Session getSession() {
		if (entityManager.getDelegate() instanceof EntityManagerImpl) {
			return ((EntityManagerImpl) entityManager.getDelegate()).getSession();
		} else if (entityManager.getDelegate() instanceof Session) {
			return (Session) entityManager.getDelegate();
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	public List<Entidade> executarQuery(String query, Map<String, Object> parametros, Integer indice, Integer qtdItemMaximo) {
		Query queryNamed = null;
		try {
			queryNamed = entityManager.createNamedQuery(query);
		} catch (Exception e) {
			queryNamed = entityManager.createQuery(query, getClasseEntidade());
		}
		if (indice!=null){
			queryNamed.setFirstResult(indice);
			queryNamed.setMaxResults(qtdItemMaximo);
		}
		if(parametros!=null){
			Set<String> keyParametros = parametros.keySet();
			for (String nomeParamentro : keyParametros) {
				queryNamed.setParameter(nomeParamentro, parametros.get(nomeParamentro));
			}
		}
		return queryNamed.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public Entidade executarQuery(String query, Map<String, Object> parametros) {
		Query queryNamed = entityManager.createNamedQuery(query);
		if(parametros!=null){
			Set<String> keyParametros = parametros.keySet();
			for (String nomeParamentro : keyParametros) {
				queryNamed.setParameter(nomeParamentro, parametros.get(nomeParamentro));
			}
		}
		return (Entidade) queryNamed.getSingleResult();
	}	

	public int executarComando(String query, Map<String, Object> parametros) {
		Query queryNamed = entityManager.createNamedQuery(query);
		if(parametros!=null){
			Set<String> keyParametros = parametros.keySet();
			for (String nomeParamentro : keyParametros) {
				queryNamed.setParameter(nomeParamentro, parametros.get(nomeParamentro));
			}
		}
		return queryNamed.executeUpdate();
	}
	
	@SuppressWarnings("unchecked")
	public List<String> executarQueryString(String query, Map<String, Object> parametros, Integer indice, Integer qtdItemMaximo) {
		Query queryNamed = entityManager.createNamedQuery(query, String.class);
		if (indice!=null){
			queryNamed.setFirstResult(indice);
			queryNamed.setMaxResults(qtdItemMaximo);
		}
		if(parametros!=null){
			Set<String> keyParametros = parametros.keySet();
			for (String nomeParamentro : keyParametros) {
				queryNamed.setParameter(nomeParamentro, parametros.get(nomeParamentro));
			}
		}
		return queryNamed.getResultList();
	}	
		
}
