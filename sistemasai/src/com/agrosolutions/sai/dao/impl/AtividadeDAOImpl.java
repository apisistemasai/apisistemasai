package com.agrosolutions.sai.dao.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.collections.map.HashedMap;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.primefaces.model.SortOrder;
import org.springframework.stereotype.Repository;

import com.agrosolutions.sai.dao.AtividadeDAO;
import com.agrosolutions.sai.model.Atividade;

@Repository
public class AtividadeDAOImpl extends DAOImpl<Atividade> implements AtividadeDAO {

	private static final long serialVersionUID = -5821661984048535053L;
	static Logger logger = Logger.getLogger(AtividadeDAOImpl.class);

    @Override
    public void excluir(Atividade atividade) {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
    	parametros.put("pId",atividade.getId());
        executarComando("Atividade.excluir", parametros);
    }	

    @Override
    public List<Atividade> obterTodos() {
    	return executarQuery("Atividade.todos", null, null, null);
    }
    
    @Override
	public Criteria criarCriteriaExample(Atividade entidade, MatchMode match) {

		Criteria criteria = super.criarCriteriaExample(entidade, match);
				
		criteria = prepararCriteria(entidade, criteria);		
		return criteria;
	}

	private Criteria prepararCriteria(Atividade entidadeLocal, Criteria criteria) {
		if (entidadeLocal.getSortField().equals("nome")) {
			if (entidadeLocal.getSortOrder().equals(SortOrder.DESCENDING)) {
				criteria.addOrder(Order.desc("nome"));
			}else{
				criteria.addOrder(Order.asc("nome"));
			}
		}
		return criteria;
		
	}
}
