package com.agrosolutions.sai.dao.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.collections.map.HashedMap;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.agrosolutions.sai.dao.ReferenciaDAO;
import com.agrosolutions.sai.model.Cultura;
import com.agrosolutions.sai.model.Referencia;
import com.agrosolutions.sai.model.Variedade;

@Repository
public class ReferenciaDAOImpl extends DAOImpl<Referencia> implements ReferenciaDAO {

	private static final long serialVersionUID = 6085068407953112462L;
	static Logger logger = Logger.getLogger(ReferenciaDAOImpl.class);

	@Override
	public List<Referencia> pesquisar(Variedade variedade) {
		@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
		parametros.put("pVariedade", variedade);

		return executarQuery("Referencia.pesquisar", parametros, null, null);
	}

	@Override
	public List<Referencia> pesquisarCultura(Cultura c) {
		@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
		parametros.put("pCultura", c);

		return executarQuery("Referencia.pesquisarCultura", parametros, null, null);
	}

	@Override
    public void excluir(Referencia referencia) {
    	@SuppressWarnings("unchecked")
		Map<String, Object> parametros = new HashedMap();
    	parametros.put("pId",referencia.getId());
        executarComando("Referencia.excluir", parametros);
    }	

    @Override
    public List<Referencia> obterTodos() {
    	return executarQuery("Referencia.todos", null, null, null);
    }	
}
