package com.agrosolutions.sai.dao;

import com.agrosolutions.sai.model.Doenca;

public interface DoencaDAO extends DAO<Doenca> {

	void excluir(Doenca doenca);
	
}
