package com.agrosolutions.sai.dao;

import com.agrosolutions.sai.model.ArduinoIn;

public interface ArduinoInDAO extends DAO<ArduinoIn> {

}
