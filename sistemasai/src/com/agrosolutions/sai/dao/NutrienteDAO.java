package com.agrosolutions.sai.dao;

import com.agrosolutions.sai.model.Nutriente;

public interface NutrienteDAO extends DAO<Nutriente> {

	void excluir(Nutriente atividade);
}
