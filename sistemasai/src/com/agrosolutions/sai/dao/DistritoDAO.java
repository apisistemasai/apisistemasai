package com.agrosolutions.sai.dao;

import java.util.List;

import com.agrosolutions.sai.model.Distrito;

public interface DistritoDAO extends DAO<Distrito> {

	Distrito salvar(Distrito distrito);
	void excluir(Distrito distrito);
	List<Distrito> obterTodos(List<Distrito> distritos);
	List<Distrito> pesquisarTodos();
	
}
