package com.agrosolutions.sai.dao;

import java.util.List;

import com.agrosolutions.sai.model.Acesso;
import com.agrosolutions.sai.model.Usuario;

public interface AcessoDAO {

	Acesso pesquisarUltimoAcesso(String u);

	List<Acesso> pesquisarPorUsuario(Usuario u);
	
	Acesso salvar(Acesso a);
	
}
