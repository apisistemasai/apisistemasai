package com.agrosolutions.sai.dao;

import java.util.List;

import com.agrosolutions.sai.model.Cultivo;
import com.agrosolutions.sai.model.Cultura;
import com.agrosolutions.sai.model.Irrigante;
import com.agrosolutions.sai.model.Variedade;

public interface CultivoDAO extends DAO<Cultivo> {

	void excluir(Cultivo cultivo);
	List<Cultivo> pesquisar(Irrigante irrigante);
	List<Cultura> pesquisarCulturas(Irrigante irrigante);
	List<Variedade> pesquisarVariedades(Irrigante irrigante);
}
