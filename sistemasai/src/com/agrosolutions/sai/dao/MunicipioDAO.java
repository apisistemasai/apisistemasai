package com.agrosolutions.sai.dao;

import java.util.List;

import com.agrosolutions.sai.model.BaciaHidrografica;
import com.agrosolutions.sai.model.Municipio;

public interface MunicipioDAO extends DAO<Municipio> {

	void excluir(Municipio municipio);

    public List<Municipio> pesquisarPorBacia(List<BaciaHidrografica> bacias);
    
	public List<Municipio> pesquisarPorBacia(List<BaciaHidrografica> bacias, Integer indiceInicio, Integer quantidade);
	
	public Long totalMunicipioPorBacia(List<BaciaHidrografica> bacias);
	
}
