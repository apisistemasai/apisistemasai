package com.agrosolutions.sai.dao;

import java.util.Date;
import java.util.List;

import com.agrosolutions.sai.model.Area;
import com.agrosolutions.sai.model.BaciaHidrografica;
import com.agrosolutions.sai.model.Cultura;
import com.agrosolutions.sai.model.GraficoPizza;
import com.agrosolutions.sai.model.GraficoPizzaIndicadores;
import com.agrosolutions.sai.model.Municipio;

public interface CulturaDAO extends DAO<Cultura> {

	void excluir(Cultura cultura);

	List<GraficoPizza> culturaAtivaPorArea(Area a, Date data);

	List<GraficoPizzaIndicadores> totalHaPorCulturaPorMunicipio(Municipio m);

	List<GraficoPizzaIndicadores> totalHaPorCulturaPorBacia(BaciaHidrografica d);

	List<GraficoPizzaIndicadores> totalCicloCulturaPorBacia(BaciaHidrografica d);
	
}
