package com.agrosolutions.sai.dao;

import java.util.List;

import com.agrosolutions.sai.model.Emissor;
import com.agrosolutions.sai.model.Fabricante;
import com.agrosolutions.sai.model.Plantio;
import com.agrosolutions.sai.model.Setor;

public interface EmissorDAO extends DAO<Emissor> {

	List<Emissor> pesquisar(Fabricante fabricante);

	void excluir(Emissor emissor);

	List<Emissor> pesquisarPorSetor(Setor setor);
 
	List<Emissor> pesquisarPorPlantio(Plantio plantio);
}
