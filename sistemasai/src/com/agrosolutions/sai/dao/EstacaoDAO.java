package com.agrosolutions.sai.dao;

import java.util.List;

import com.agrosolutions.sai.model.Distrito;
import com.agrosolutions.sai.model.Estacao;

public interface EstacaoDAO extends DAO<Estacao> {

	public List<Estacao> pesquisar(List<Distrito> distrito);

	void excluir(Estacao area);
}
