package com.agrosolutions.sai.dao;

import com.agrosolutions.sai.model.Elemento;

public interface ElementoDAO extends DAO<Elemento> {

	void excluir(Elemento elemento);
}
