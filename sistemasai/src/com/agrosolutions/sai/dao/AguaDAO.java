package com.agrosolutions.sai.dao;

import java.util.Date;
import java.util.List;

import com.agrosolutions.sai.model.Agua;
import com.agrosolutions.sai.model.Irrigante;

public interface AguaDAO extends DAO<Agua> {

	List<Agua> pesquisar(Irrigante param);
	void excluir(Agua agua);
	Agua pesquisarAnterior(Irrigante irrigante, Date data);
	List<Agua> pesquisarPeriodo(Date data);
}
