package com.agrosolutions.sai.dao;

import com.agrosolutions.sai.model.DemandaHidrica;

public interface DemandaHidricaDAO extends DAO<DemandaHidrica> {
	
}