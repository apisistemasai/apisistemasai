package com.agrosolutions.sai.dao;

import java.util.List;

import com.agrosolutions.sai.model.Distrito;
import com.agrosolutions.sai.model.Tecnico;

public interface TecnicoDAO extends DAO<Tecnico> {

	List<Tecnico> pesquisar(List<Distrito> distrito);

	void excluir(Tecnico tecnico);
}
