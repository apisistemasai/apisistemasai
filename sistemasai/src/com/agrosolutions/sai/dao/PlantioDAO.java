package com.agrosolutions.sai.dao;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.agrosolutions.sai.model.Area;
import com.agrosolutions.sai.model.Cultivo;
import com.agrosolutions.sai.model.Distrito;
import com.agrosolutions.sai.model.GraficoPizza;
import com.agrosolutions.sai.model.Irrigante;
import com.agrosolutions.sai.model.Plantio;
import com.agrosolutions.sai.model.Setor;

public interface PlantioDAO extends DAO<Plantio> {

	void excluir(Plantio plantio);
	List<Plantio> pesquisar(Cultivo cultivo);
	List<Plantio> pesquisarPorIrrigante(Irrigante param);
	List<Plantio> pesquisarPorSetor(Setor setor);
	List<Plantio> ativos(Area area, Date data);
	List<GraficoPizza> culturaAtivaPorArea(Area a, Date data);
	List<GraficoPizza> soloPorArea(Area a);
	List<GraficoPizza> atividadePorArea(Area a, Date data);
	List<GraficoPizza> irrigantePorArea(Area a);
	List<GraficoPizza> atividadePorDistrito(Distrito a, Date data);
	List<GraficoPizza> irrigantePorDistrito(Distrito a);
	List<GraficoPizza> soloPorDistrito(Distrito a);
	List<GraficoPizza> culturaAtivaPorDistrito(Distrito a, Date data);
	List<GraficoPizza> emissorPorArea(Area a);
	List<GraficoPizza> emissorPorDistrito(Distrito area);
	List<GraficoPizza> bombaPorArea(Area a);
	List<GraficoPizza> bombaPorDistrito(Distrito area);
	List<GraficoPizza> filtroPorArea(Area a);
	List<GraficoPizza> filtroPorDistrito(Distrito area);
	List<GraficoPizza> valvulaPorArea(Area a);
	List<GraficoPizza> valvulaPorDistrito(Distrito area);
	List<GraficoPizza> tuboPorArea(Area a);
	List<GraficoPizza> tuboPorDistrito(Distrito area);
	List<GraficoPizza> culturaHecPorArea(Area a, Date data);
	List<GraficoPizza> culturaHecPorDistrito(Distrito a, Date data);
	BigDecimal totalHecSequeiro(Distrito d);
	BigDecimal totalHecConsorcio(Distrito d);
	BigDecimal totalHecSequeiro(Area a);
	BigDecimal totalHecConsorcio(Area a);
	List<Plantio> ativos2(Area area, Date data);
}
