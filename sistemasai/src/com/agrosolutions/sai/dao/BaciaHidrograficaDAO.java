package com.agrosolutions.sai.dao;

import java.util.List;

import com.agrosolutions.sai.model.BaciaHidrografica;

public interface BaciaHidrograficaDAO extends DAO<BaciaHidrografica> {

	BaciaHidrografica salvar(BaciaHidrografica baciaHidrografica);
	void excluir(BaciaHidrografica baciaHidrografica);
	List<BaciaHidrografica> obterTodos(List<BaciaHidrografica> baciaHidrograficas);
	List<BaciaHidrografica> pesquisarTodos();
	
}
