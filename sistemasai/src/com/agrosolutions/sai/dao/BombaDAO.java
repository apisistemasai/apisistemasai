package com.agrosolutions.sai.dao;

import java.util.List;

import com.agrosolutions.sai.model.Bomba;
import com.agrosolutions.sai.model.Fabricante;
import com.agrosolutions.sai.model.Irrigante;

public interface BombaDAO extends DAO<Bomba> {

	List<Bomba> pesquisar(Fabricante fabricante);

	void excluir(Bomba bomba);

	List<Bomba> pesquisarPorIrrigante(Irrigante irrigante);
}
