package com.agrosolutions.sai.dao;

import java.util.Date;
import java.util.List;

import com.agrosolutions.sai.model.Area;
import com.agrosolutions.sai.model.Clima;
import com.agrosolutions.sai.model.Distrito;
import com.agrosolutions.sai.model.Estacao;

public interface ClimaDAO extends DAO<Clima> {

	Clima pesquisarPorData(Area area, Date data);
	List<Clima> pesquisarPorPeriodo(Area area, Date dataIni, Date dataFim);
	List<Clima> pesquisarPorArea(Area param);
	Clima ultimoPorArea(Area area);
	List<Clima> estacaoPorPeriodo(Estacao estacao, Date dataIni, Date dataFim);
	List<Clima> ultimoPorDistrito(Distrito dist);

}