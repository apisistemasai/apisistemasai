package com.agrosolutions.sai.dao;

import com.agrosolutions.sai.model.Fornecedor;

public interface FornecedorDAO extends DAO<Fornecedor> {

	void excluir(Fornecedor c);

}