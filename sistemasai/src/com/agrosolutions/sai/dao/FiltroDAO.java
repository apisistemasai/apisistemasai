package com.agrosolutions.sai.dao;

import java.util.List;

import com.agrosolutions.sai.model.Fabricante;
import com.agrosolutions.sai.model.Filtro;
import com.agrosolutions.sai.model.Irrigante;

public interface FiltroDAO extends DAO<Filtro> {

	List<Filtro> pesquisar(Fabricante fabricante);

	void excluir(Filtro filtro);

	List<Filtro> pesquisarPorIrrigante(Irrigante irrigante);
}
