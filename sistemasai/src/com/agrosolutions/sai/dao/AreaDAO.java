package com.agrosolutions.sai.dao;

import java.util.List;

import com.agrosolutions.sai.model.Area;
import com.agrosolutions.sai.model.Distrito;

public interface AreaDAO extends DAO<Area> {

	public List<Area> pesquisar(List<Distrito> distrito);

	void excluir(Area area);
}
