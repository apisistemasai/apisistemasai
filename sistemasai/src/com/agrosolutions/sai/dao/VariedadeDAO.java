package com.agrosolutions.sai.dao;

import java.util.List;

import com.agrosolutions.sai.model.Cultura;
import com.agrosolutions.sai.model.Variedade;

public interface VariedadeDAO extends DAO<Variedade> {

	List<Variedade> pesquisar(Cultura cultura);
	void excluir(Variedade variedade);
}
