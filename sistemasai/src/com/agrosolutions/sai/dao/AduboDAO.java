package com.agrosolutions.sai.dao;

import com.agrosolutions.sai.model.Adubo;

public interface AduboDAO extends DAO<Adubo> {

	void excluir(Adubo adubo);
}
