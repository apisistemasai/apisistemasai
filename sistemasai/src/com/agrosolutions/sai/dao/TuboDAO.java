package com.agrosolutions.sai.dao;

import java.util.List;

import com.agrosolutions.sai.model.Fabricante;
import com.agrosolutions.sai.model.Irrigante;
import com.agrosolutions.sai.model.Tubo;

public interface TuboDAO extends DAO<Tubo> {

	List<Tubo> pesquisar(Fabricante fabricante);

	void excluir(Tubo tubo);

	List<Tubo> pesquisarPorIrrigante(Irrigante irrigante);
}
