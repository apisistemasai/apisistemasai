package com.agrosolutions.sai.dao;

import java.util.List;

import com.agrosolutions.sai.model.AtividadeEconomica;
import com.agrosolutions.sai.model.BaciaHidrografica;
import com.agrosolutions.sai.model.Cultura;
import com.agrosolutions.sai.model.Municipio;
import com.agrosolutions.sai.model.Variedade;

public interface AtividadeEconomicaDAO extends DAO<AtividadeEconomica> {

	void excluir(AtividadeEconomica atividadeEconomica);
	List<AtividadeEconomica> pesquisarPorMunicipio(Municipio municipio);
	List<AtividadeEconomica> pesquisarPorBacia(BaciaHidrografica bacia);
	List<Cultura> pesquisarCulturas(Municipio municipio);
	List<Variedade> pesquisarVariedades(Municipio municipio);
	List<AtividadeEconomica> pesquisarTodosDeMunicipio();
	List<AtividadeEconomica> pesquisarTodosDaBacia();
	List<AtividadeEconomica> pesquisarPorVariedades(Variedade variedade);
}
