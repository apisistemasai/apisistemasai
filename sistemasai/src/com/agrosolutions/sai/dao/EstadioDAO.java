package com.agrosolutions.sai.dao;

import java.util.List;

import com.agrosolutions.sai.model.Estadio;
import com.agrosolutions.sai.model.Referencia;

public interface EstadioDAO extends DAO<Estadio> {
 
	List<Estadio> pesquisar(Referencia param);
	void excluir(Estadio estadio);
}
