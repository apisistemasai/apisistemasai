package com.agrosolutions.sai.dao;

import java.util.List;

import com.agrosolutions.sai.model.Perfil;
import com.agrosolutions.sai.model.TipoPerfil;

public interface PerfilDAO extends DAO<Perfil> {

	void excluir(Perfil perfil);
	List<Perfil> porTipo(TipoPerfil tipo);

}
