var MASK_MONEY_MONETARIO = {
	symbol: "R$",
	decimal: ",",
	precision: 2,
	thousands: ".",
	showSymbol: false,
	allowZero: true,
	initWithMask: false //SE allowZero == true, PODE INICIAR OU N�O COM M�SCARA
};

var MASK_VALOR = {
		decimal: ",",
		precision: 1,
		thousands: ".",
		showSymbol: false,
		allowZero: true,
		initWithMask: false //SE allowZero == true, PODE INICIAR OU N�O COM M�SCARA
};

var MASK_AREA = {
		decimal: ",",
		precision: 3,
		thousands: ".",
		showSymbol: false,
		allowZero: true,
		initWithMask: false //SE allowZero == true, PODE INICIAR OU N�O COM M�SCARA
};

