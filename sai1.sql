/*
Navicat PGSQL Data Transfer

Source Server         : LocalHostPostGres
Source Server Version : 90603
Source Host           : localhost:5432
Source Database       : sai
Source Schema         : sai

Target Server Type    : PGSQL
Target Server Version : 90603
File Encoding         : 65001

Date: 2017-07-09 21:06:12
*/


-- ----------------------------
-- Sequence structure for sq_acesso
-- ----------------------------
DROP SEQUENCE IF EXISTS "sai"."sq_acesso";
CREATE SEQUENCE "sai"."sq_acesso"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for sq_adubo
-- ----------------------------
DROP SEQUENCE IF EXISTS "sai"."sq_adubo";
CREATE SEQUENCE "sai"."sq_adubo"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for sq_agua
-- ----------------------------
DROP SEQUENCE IF EXISTS "sai"."sq_agua";
CREATE SEQUENCE "sai"."sq_agua"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for sq_arduinoin
-- ----------------------------
DROP SEQUENCE IF EXISTS "sai"."sq_arduinoin";
CREATE SEQUENCE "sai"."sq_arduinoin"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for sq_area
-- ----------------------------
DROP SEQUENCE IF EXISTS "sai"."sq_area";
CREATE SEQUENCE "sai"."sq_area"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for sq_atividade
-- ----------------------------
DROP SEQUENCE IF EXISTS "sai"."sq_atividade";
CREATE SEQUENCE "sai"."sq_atividade"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for sq_atividadeeconomica
-- ----------------------------
DROP SEQUENCE IF EXISTS "sai"."sq_atividadeeconomica";
CREATE SEQUENCE "sai"."sq_atividadeeconomica"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for sq_baciahidrografica
-- ----------------------------
DROP SEQUENCE IF EXISTS "sai"."sq_baciahidrografica";
CREATE SEQUENCE "sai"."sq_baciahidrografica"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for sq_bomba
-- ----------------------------
DROP SEQUENCE IF EXISTS "sai"."sq_bomba";
CREATE SEQUENCE "sai"."sq_bomba"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for sq_categoria
-- ----------------------------
DROP SEQUENCE IF EXISTS "sai"."sq_categoria";
CREATE SEQUENCE "sai"."sq_categoria"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for sq_cliente
-- ----------------------------
DROP SEQUENCE IF EXISTS "sai"."sq_cliente";
CREATE SEQUENCE "sai"."sq_cliente"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for sq_clima
-- ----------------------------
DROP SEQUENCE IF EXISTS "sai"."sq_clima";
CREATE SEQUENCE "sai"."sq_clima"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for sq_colheita
-- ----------------------------
DROP SEQUENCE IF EXISTS "sai"."sq_colheita";
CREATE SEQUENCE "sai"."sq_colheita"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for sq_conta
-- ----------------------------
DROP SEQUENCE IF EXISTS "sai"."sq_conta";
CREATE SEQUENCE "sai"."sq_conta"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for sq_coordenadas
-- ----------------------------
DROP SEQUENCE IF EXISTS "sai"."sq_coordenadas";
CREATE SEQUENCE "sai"."sq_coordenadas"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for sq_cultivo
-- ----------------------------
DROP SEQUENCE IF EXISTS "sai"."sq_cultivo";
CREATE SEQUENCE "sai"."sq_cultivo"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for sq_cultura
-- ----------------------------
DROP SEQUENCE IF EXISTS "sai"."sq_cultura";
CREATE SEQUENCE "sai"."sq_cultura"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for sq_defensivo
-- ----------------------------
DROP SEQUENCE IF EXISTS "sai"."sq_defensivo";
CREATE SEQUENCE "sai"."sq_defensivo"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for sq_demandahidrica
-- ----------------------------
DROP SEQUENCE IF EXISTS "sai"."sq_demandahidrica";
CREATE SEQUENCE "sai"."sq_demandahidrica"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for sq_distrito
-- ----------------------------
DROP SEQUENCE IF EXISTS "sai"."sq_distrito";
CREATE SEQUENCE "sai"."sq_distrito"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for sq_doenca
-- ----------------------------
DROP SEQUENCE IF EXISTS "sai"."sq_doenca";
CREATE SEQUENCE "sai"."sq_doenca"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for sq_elemento
-- ----------------------------
DROP SEQUENCE IF EXISTS "sai"."sq_elemento";
CREATE SEQUENCE "sai"."sq_elemento"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for sq_emissor
-- ----------------------------
DROP SEQUENCE IF EXISTS "sai"."sq_emissor";
CREATE SEQUENCE "sai"."sq_emissor"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for sq_endereco
-- ----------------------------
DROP SEQUENCE IF EXISTS "sai"."sq_endereco";
CREATE SEQUENCE "sai"."sq_endereco"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for sq_energia
-- ----------------------------
DROP SEQUENCE IF EXISTS "sai"."sq_energia";
CREATE SEQUENCE "sai"."sq_energia"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for sq_estacao
-- ----------------------------
DROP SEQUENCE IF EXISTS "sai"."sq_estacao";
CREATE SEQUENCE "sai"."sq_estacao"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for sq_estadio
-- ----------------------------
DROP SEQUENCE IF EXISTS "sai"."sq_estadio";
CREATE SEQUENCE "sai"."sq_estadio"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for sq_fabricante
-- ----------------------------
DROP SEQUENCE IF EXISTS "sai"."sq_fabricante";
CREATE SEQUENCE "sai"."sq_fabricante"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for sq_fertirrigacaoaplicada
-- ----------------------------
DROP SEQUENCE IF EXISTS "sai"."sq_fertirrigacaoaplicada";
CREATE SEQUENCE "sai"."sq_fertirrigacaoaplicada"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for sq_filtro
-- ----------------------------
DROP SEQUENCE IF EXISTS "sai"."sq_filtro";
CREATE SEQUENCE "sai"."sq_filtro"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for sq_fornecedor
-- ----------------------------
DROP SEQUENCE IF EXISTS "sai"."sq_fornecedor";
CREATE SEQUENCE "sai"."sq_fornecedor"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for sq_funcionalidade
-- ----------------------------
DROP SEQUENCE IF EXISTS "sai"."sq_funcionalidade";
CREATE SEQUENCE "sai"."sq_funcionalidade"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for sq_indicadores
-- ----------------------------
DROP SEQUENCE IF EXISTS "sai"."sq_indicadores";
CREATE SEQUENCE "sai"."sq_indicadores"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for sq_irrigacao
-- ----------------------------
DROP SEQUENCE IF EXISTS "sai"."sq_irrigacao";
CREATE SEQUENCE "sai"."sq_irrigacao"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for sq_irrigante
-- ----------------------------
DROP SEQUENCE IF EXISTS "sai"."sq_irrigante";
CREATE SEQUENCE "sai"."sq_irrigante"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for sq_mensagem
-- ----------------------------
DROP SEQUENCE IF EXISTS "sai"."sq_mensagem";
CREATE SEQUENCE "sai"."sq_mensagem"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for sq_modulo
-- ----------------------------
DROP SEQUENCE IF EXISTS "sai"."sq_modulo";
CREATE SEQUENCE "sai"."sq_modulo"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for sq_municipio
-- ----------------------------
DROP SEQUENCE IF EXISTS "sai"."sq_municipio";
CREATE SEQUENCE "sai"."sq_municipio"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for sq_nutriente
-- ----------------------------
DROP SEQUENCE IF EXISTS "sai"."sq_nutriente";
CREATE SEQUENCE "sai"."sq_nutriente"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for sq_parametro
-- ----------------------------
DROP SEQUENCE IF EXISTS "sai"."sq_parametro";
CREATE SEQUENCE "sai"."sq_parametro"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for sq_perfil
-- ----------------------------
DROP SEQUENCE IF EXISTS "sai"."sq_perfil";
CREATE SEQUENCE "sai"."sq_perfil"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for sq_plantio
-- ----------------------------
DROP SEQUENCE IF EXISTS "sai"."sq_plantio";
CREATE SEQUENCE "sai"."sq_plantio"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for sq_praga
-- ----------------------------
DROP SEQUENCE IF EXISTS "sai"."sq_praga";
CREATE SEQUENCE "sai"."sq_praga"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for sq_referencia
-- ----------------------------
DROP SEQUENCE IF EXISTS "sai"."sq_referencia";
CREATE SEQUENCE "sai"."sq_referencia"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for sq_revisao
-- ----------------------------
DROP SEQUENCE IF EXISTS "sai"."sq_revisao";
CREATE SEQUENCE "sai"."sq_revisao"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for sq_setor
-- ----------------------------
DROP SEQUENCE IF EXISTS "sai"."sq_setor";
CREATE SEQUENCE "sai"."sq_setor"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for sq_solo
-- ----------------------------
DROP SEQUENCE IF EXISTS "sai"."sq_solo";
CREATE SEQUENCE "sai"."sq_solo"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for sq_solucao
-- ----------------------------
DROP SEQUENCE IF EXISTS "sai"."sq_solucao";
CREATE SEQUENCE "sai"."sq_solucao"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for sq_tanque
-- ----------------------------
DROP SEQUENCE IF EXISTS "sai"."sq_tanque";
CREATE SEQUENCE "sai"."sq_tanque"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for sq_tecnico
-- ----------------------------
DROP SEQUENCE IF EXISTS "sai"."sq_tecnico";
CREATE SEQUENCE "sai"."sq_tecnico"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for sq_testeeficiencia
-- ----------------------------
DROP SEQUENCE IF EXISTS "sai"."sq_testeeficiencia";
CREATE SEQUENCE "sai"."sq_testeeficiencia"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for sq_tubo
-- ----------------------------
DROP SEQUENCE IF EXISTS "sai"."sq_tubo";
CREATE SEQUENCE "sai"."sq_tubo"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for sq_usuario
-- ----------------------------
DROP SEQUENCE IF EXISTS "sai"."sq_usuario";
CREATE SEQUENCE "sai"."sq_usuario"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for sq_valvula
-- ----------------------------
DROP SEQUENCE IF EXISTS "sai"."sq_valvula";
CREATE SEQUENCE "sai"."sq_valvula"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for sq_variedade
-- ----------------------------
DROP SEQUENCE IF EXISTS "sai"."sq_variedade";
CREATE SEQUENCE "sai"."sq_variedade"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Table structure for acesso
-- ----------------------------
DROP TABLE IF EXISTS "sai"."acesso";
CREATE TABLE "sai"."acesso" (
"id" int8 NOT NULL,
"version" int8,
"data" timestamp(6),
"usuario_id" int8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for adubo
-- ----------------------------
DROP TABLE IF EXISTS "sai"."adubo";
CREATE TABLE "sai"."adubo" (
"id" int8 NOT NULL,
"version" int8,
"nome" varchar(255) COLLATE "default",
"observacao" varchar(800) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for adubo_aud
-- ----------------------------
DROP TABLE IF EXISTS "sai"."adubo_aud";
CREATE TABLE "sai"."adubo_aud" (
"id" int8 NOT NULL,
"cd_revisao" int4 NOT NULL,
"tipo_revisao" int2,
"nome" varchar(255) COLLATE "default",
"observacao" varchar(800) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for agua
-- ----------------------------
DROP TABLE IF EXISTS "sai"."agua";
CREATE TABLE "sai"."agua" (
"id" int8 NOT NULL,
"version" int8,
"data" date,
"datainicial" timestamp(6),
"et" numeric(19,2),
"irr" numeric(19,2),
"k2fixo" numeric(19,2),
"k2variavel" numeric(19,2),
"lacre" varchar(255) COLLATE "default",
"leituraanterior" int8,
"leituraatual" int8,
"valor" numeric(19,2),
"irrigante_id" int8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for agua_aud
-- ----------------------------
DROP TABLE IF EXISTS "sai"."agua_aud";
CREATE TABLE "sai"."agua_aud" (
"id" int8 NOT NULL,
"cd_revisao" int4 NOT NULL,
"tipo_revisao" int2,
"data" date,
"datainicial" timestamp(6),
"et" numeric(19,2),
"irr" numeric(19,2),
"k2fixo" numeric(19,2),
"k2variavel" numeric(19,2),
"lacre" varchar(255) COLLATE "default",
"leituraanterior" int8,
"leituraatual" int8,
"valor" numeric(19,2),
"irrigante_id" int8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for arduinoin
-- ----------------------------
DROP TABLE IF EXISTS "sai"."arduinoin";
CREATE TABLE "sai"."arduinoin" (
"id" int8 NOT NULL,
"version" int8,
"data" timestamp(6),
"volume" numeric(19,2),
"irrigante_id" int8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for arduinoin_aud
-- ----------------------------
DROP TABLE IF EXISTS "sai"."arduinoin_aud";
CREATE TABLE "sai"."arduinoin_aud" (
"id" int8 NOT NULL,
"cd_revisao" int4 NOT NULL,
"tipo_revisao" int2,
"data" timestamp(6),
"volume" numeric(19,2),
"irrigante_id" int8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for area
-- ----------------------------
DROP TABLE IF EXISTS "sai"."area";
CREATE TABLE "sai"."area" (
"id" int8 NOT NULL,
"version" int8,
"altitude" int4,
"areairrigada" numeric(19,2),
"areatotal" numeric(19,2),
"datacadastro" date,
"datainatividade" date,
"gerarti" bool DEFAULT true NOT NULL,
"latitude" varchar(255) COLLATE "default",
"localizacao" varchar(255) COLLATE "default",
"longitude" varchar(255) COLLATE "default",
"nome" varchar(255) COLLATE "default",
"observacao" varchar(800) COLLATE "default",
"distrito_id" int8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for area_aud
-- ----------------------------
DROP TABLE IF EXISTS "sai"."area_aud";
CREATE TABLE "sai"."area_aud" (
"id" int8 NOT NULL,
"cd_revisao" int4 NOT NULL,
"tipo_revisao" int2,
"altitude" int4,
"areairrigada" numeric(19,2),
"areatotal" numeric(19,2),
"datacadastro" date,
"datainatividade" date,
"gerarti" bool DEFAULT true,
"latitude" varchar(255) COLLATE "default",
"localizacao" varchar(255) COLLATE "default",
"longitude" varchar(255) COLLATE "default",
"nome" varchar(255) COLLATE "default",
"observacao" varchar(800) COLLATE "default",
"distrito_id" int8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for atividade
-- ----------------------------
DROP TABLE IF EXISTS "sai"."atividade";
CREATE TABLE "sai"."atividade" (
"id" int8 NOT NULL,
"version" int8,
"nome" varchar(255) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for atividade_aud
-- ----------------------------
DROP TABLE IF EXISTS "sai"."atividade_aud";
CREATE TABLE "sai"."atividade_aud" (
"id" int8 NOT NULL,
"cd_revisao" int4 NOT NULL,
"tipo_revisao" int2,
"nome" varchar(255) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for atividadeeconomica
-- ----------------------------
DROP TABLE IF EXISTS "sai"."atividadeeconomica";
CREATE TABLE "sai"."atividadeeconomica" (
"id" int8 NOT NULL,
"version" int8,
"areatotal" numeric(19,2),
"datafim" date,
"datainicio" date,
"observacao" varchar(800) COLLATE "default",
"qtddiascultivo" int8,
"bacia_id" int8,
"cultura_id" int8,
"municipio_id" int8,
"referencia_id" int8,
"variedade_id" int8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for atividadeeconomica_aud
-- ----------------------------
DROP TABLE IF EXISTS "sai"."atividadeeconomica_aud";
CREATE TABLE "sai"."atividadeeconomica_aud" (
"id" int8 NOT NULL,
"cd_revisao" int4 NOT NULL,
"tipo_revisao" int2,
"areatotal" numeric(19,2),
"datafim" date,
"datainicio" date,
"observacao" varchar(800) COLLATE "default",
"qtddiascultivo" int8,
"bacia_id" int8,
"cultura_id" int8,
"municipio_id" int8,
"referencia_id" int8,
"variedade_id" int8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for baciahidrografica
-- ----------------------------
DROP TABLE IF EXISTS "sai"."baciahidrografica";
CREATE TABLE "sai"."baciahidrografica" (
"id" int8 NOT NULL,
"version" int8,
"datacadastro" date,
"latitude" varchar(255) COLLATE "default",
"localizacao" varchar(255) COLLATE "default",
"longitude" varchar(255) COLLATE "default",
"nome" varchar(255) COLLATE "default",
"observacao" varchar(800) COLLATE "default",
"uf" varchar(255) COLLATE "default",
"volumetotal" numeric(19,2),
"zoommapa" int4
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for baciahidrografica_aud
-- ----------------------------
DROP TABLE IF EXISTS "sai"."baciahidrografica_aud";
CREATE TABLE "sai"."baciahidrografica_aud" (
"id" int8 NOT NULL,
"cd_revisao" int4 NOT NULL,
"tipo_revisao" int2,
"datacadastro" date,
"latitude" varchar(255) COLLATE "default",
"localizacao" varchar(255) COLLATE "default",
"longitude" varchar(255) COLLATE "default",
"nome" varchar(255) COLLATE "default",
"observacao" varchar(800) COLLATE "default",
"uf" varchar(255) COLLATE "default",
"volumetotal" numeric(19,2),
"zoommapa" int4
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for bomba
-- ----------------------------
DROP TABLE IF EXISTS "sai"."bomba";
CREATE TABLE "sai"."bomba" (
"id" int8 NOT NULL,
"version" int8,
"acionamentobomba" int4,
"modelo" varchar(255) COLLATE "default",
"potencia" numeric(19,2),
"rendimento" numeric(19,2),
"tipo" int4,
"vazao" numeric(19,2),
"fabricante_id" int8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for bomba_aud
-- ----------------------------
DROP TABLE IF EXISTS "sai"."bomba_aud";
CREATE TABLE "sai"."bomba_aud" (
"id" int8 NOT NULL,
"cd_revisao" int4 NOT NULL,
"tipo_revisao" int2,
"acionamentobomba" int4,
"modelo" varchar(255) COLLATE "default",
"potencia" numeric(19,2),
"rendimento" numeric(19,2),
"tipo" int4,
"vazao" numeric(19,2),
"fabricante_id" int8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for categoria
-- ----------------------------
DROP TABLE IF EXISTS "sai"."categoria";
CREATE TABLE "sai"."categoria" (
"id" int8 NOT NULL,
"version" int8,
"codigo" varchar(255) COLLATE "default",
"nome" varchar(255) COLLATE "default",
"ordem" int4,
"pai" bytea,
"tipo" int4
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for cliente
-- ----------------------------
DROP TABLE IF EXISTS "sai"."cliente";
CREATE TABLE "sai"."cliente" (
"id" int8 NOT NULL,
"version" int8,
"celular" varchar(255) COLLATE "default",
"cnpj" varchar(255) COLLATE "default",
"codigo" varchar(255) COLLATE "default",
"contato" varchar(255) COLLATE "default",
"cpf" varchar(255) COLLATE "default",
"nome" varchar(255) COLLATE "default",
"observacao" varchar(255) COLLATE "default",
"telefone" varchar(255) COLLATE "default",
"endereco_id" int8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for clima
-- ----------------------------
DROP TABLE IF EXISTS "sai"."clima";
CREATE TABLE "sai"."clima" (
"id" int8 NOT NULL,
"version" int8,
"chuva" numeric(19,2),
"chuvaefetiva" numeric(19,2),
"data" date,
"eto" numeric(19,2),
"etocalculado" numeric(19,2),
"orvalhomax" numeric(19,2),
"orvalhomed" numeric(19,2),
"orvalhomin" numeric(19,2),
"pressaomax" numeric(19,2),
"pressaomed" numeric(19,2),
"pressaomin" numeric(19,2),
"radliquida" numeric(19,2),
"radiacao" numeric(19,2),
"tempmax" numeric(19,2),
"tempmed" numeric(19,2),
"tempmin" numeric(19,2),
"umidademax" numeric(19,2),
"umidademed" numeric(19,2),
"umidademin" numeric(19,2),
"vento" numeric(19,2),
"ventorajada" numeric(19,2),
"area_id" int8,
"estacao_id" int8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for clima_aud
-- ----------------------------
DROP TABLE IF EXISTS "sai"."clima_aud";
CREATE TABLE "sai"."clima_aud" (
"id" int8 NOT NULL,
"cd_revisao" int4 NOT NULL,
"tipo_revisao" int2,
"chuva" numeric(19,2),
"chuvaefetiva" numeric(19,2),
"data" date,
"eto" numeric(19,2),
"etocalculado" numeric(19,2),
"orvalhomax" numeric(19,2),
"orvalhomed" numeric(19,2),
"orvalhomin" numeric(19,2),
"pressaomax" numeric(19,2),
"pressaomed" numeric(19,2),
"pressaomin" numeric(19,2),
"radliquida" numeric(19,2),
"radiacao" numeric(19,2),
"tempmax" numeric(19,2),
"tempmed" numeric(19,2),
"tempmin" numeric(19,2),
"umidademax" numeric(19,2),
"umidademed" numeric(19,2),
"umidademin" numeric(19,2),
"vento" numeric(19,2),
"ventorajada" numeric(19,2),
"area_id" int8,
"estacao_id" int8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for colheita
-- ----------------------------
DROP TABLE IF EXISTS "sai"."colheita";
CREATE TABLE "sai"."colheita" (
"id" int8 NOT NULL,
"version" int8,
"datacolheita" date,
"qtdcolhida" int4,
"qtdperdida" int4,
"plantio_id" int8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for colheita_aud
-- ----------------------------
DROP TABLE IF EXISTS "sai"."colheita_aud";
CREATE TABLE "sai"."colheita_aud" (
"id" int8 NOT NULL,
"cd_revisao" int4 NOT NULL,
"tipo_revisao" int2,
"datacolheita" date,
"qtdcolhida" int4,
"qtdperdida" int4,
"plantio_id" int8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for conta
-- ----------------------------
DROP TABLE IF EXISTS "sai"."conta";
CREATE TABLE "sai"."conta" (
"id" int8 NOT NULL,
"version" int8,
"codigo" int8 NOT NULL,
"datafinal" timestamp(6),
"datainicial" timestamp(6),
"descricao" varchar(255) COLLATE "default",
"saldoatual" numeric(19,2),
"saldoinicial" numeric(19,2),
"tipo" int4
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for coordenada
-- ----------------------------
DROP TABLE IF EXISTS "sai"."coordenada";
CREATE TABLE "sai"."coordenada" (
"id" int8 NOT NULL,
"version" int8,
"altitude" float8 NOT NULL,
"latitude" float8 NOT NULL,
"local" varchar(255) COLLATE "default",
"longitude" float8 NOT NULL,
"ordem" float8 NOT NULL,
"irrigante_id" int8,
"municipio_id" int8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for coordenada_aud
-- ----------------------------
DROP TABLE IF EXISTS "sai"."coordenada_aud";
CREATE TABLE "sai"."coordenada_aud" (
"id" int8 NOT NULL,
"cd_revisao" int4 NOT NULL,
"tipo_revisao" int2,
"altitude" float8,
"latitude" float8,
"local" varchar(255) COLLATE "default",
"longitude" float8,
"ordem" float8,
"irrigante_id" int8,
"municipio_id" int8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for cultivo
-- ----------------------------
DROP TABLE IF EXISTS "sai"."cultivo";
CREATE TABLE "sai"."cultivo" (
"id" int8 NOT NULL,
"version" int8,
"datafim" date,
"datainicio" date,
"observacao" varchar(800) COLLATE "default",
"qtddiascultivo" int8,
"cultura_id" int8,
"irrigante_id" int8,
"referencia_id" int8,
"variedade_id" int8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for cultivo_aud
-- ----------------------------
DROP TABLE IF EXISTS "sai"."cultivo_aud";
CREATE TABLE "sai"."cultivo_aud" (
"id" int8 NOT NULL,
"cd_revisao" int4 NOT NULL,
"tipo_revisao" int2,
"datafim" date,
"datainicio" date,
"observacao" varchar(800) COLLATE "default",
"qtddiascultivo" int8,
"cultura_id" int8,
"irrigante_id" int8,
"referencia_id" int8,
"variedade_id" int8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for cultura
-- ----------------------------
DROP TABLE IF EXISTS "sai"."cultura";
CREATE TABLE "sai"."cultura" (
"id" int8 NOT NULL,
"version" int8,
"image" varchar(255) COLLATE "default",
"nome" varchar(255) COLLATE "default",
"atividade_id" int8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for cultura_aud
-- ----------------------------
DROP TABLE IF EXISTS "sai"."cultura_aud";
CREATE TABLE "sai"."cultura_aud" (
"id" int8 NOT NULL,
"cd_revisao" int4 NOT NULL,
"tipo_revisao" int2,
"image" varchar(255) COLLATE "default",
"nome" varchar(255) COLLATE "default",
"atividade_id" int8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for defensivo
-- ----------------------------
DROP TABLE IF EXISTS "sai"."defensivo";
CREATE TABLE "sai"."defensivo" (
"id" int8 NOT NULL,
"version" int8,
"nome" varchar(255) COLLATE "default",
"observacao" varchar(800) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for defensivo_aud
-- ----------------------------
DROP TABLE IF EXISTS "sai"."defensivo_aud";
CREATE TABLE "sai"."defensivo_aud" (
"id" int8 NOT NULL,
"cd_revisao" int4 NOT NULL,
"tipo_revisao" int2,
"nome" varchar(255) COLLATE "default",
"observacao" varchar(800) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for demandahidrica
-- ----------------------------
DROP TABLE IF EXISTS "sai"."demandahidrica";
CREATE TABLE "sai"."demandahidrica" (
"id" int8 NOT NULL,
"version" int8,
"data" date,
"etc" numeric(19,2),
"eto" numeric(19,2),
"kc" numeric(19,2),
"clima_id" int8,
"irrigante_id" int8,
"plantio_id" int8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for distrito
-- ----------------------------
DROP TABLE IF EXISTS "sai"."distrito";
CREATE TABLE "sai"."distrito" (
"id" int8 NOT NULL,
"version" int8,
"areapreservada" numeric(19,2),
"areatotal" numeric(19,2),
"cep" varchar(255) COLLATE "default",
"cidade" varchar(255) COLLATE "default",
"datacadastro" date,
"datainatividade" date,
"endereco" varchar(255) COLLATE "default",
"latitude" varchar(255) COLLATE "default",
"localizacao" varchar(255) COLLATE "default",
"longitude" varchar(255) COLLATE "default",
"nome" varchar(255) COLLATE "default",
"observacao" varchar(800) COLLATE "default",
"telefonecelular" varchar(255) COLLATE "default",
"telefonecontato" varchar(255) COLLATE "default",
"uf" varchar(255) COLLATE "default",
"zoommapa" int4
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for distrito_aud
-- ----------------------------
DROP TABLE IF EXISTS "sai"."distrito_aud";
CREATE TABLE "sai"."distrito_aud" (
"id" int8 NOT NULL,
"cd_revisao" int4 NOT NULL,
"tipo_revisao" int2,
"areapreservada" numeric(19,2),
"areatotal" numeric(19,2),
"cep" varchar(255) COLLATE "default",
"cidade" varchar(255) COLLATE "default",
"datacadastro" date,
"datainatividade" date,
"endereco" varchar(255) COLLATE "default",
"latitude" varchar(255) COLLATE "default",
"localizacao" varchar(255) COLLATE "default",
"longitude" varchar(255) COLLATE "default",
"nome" varchar(255) COLLATE "default",
"observacao" varchar(800) COLLATE "default",
"telefonecelular" varchar(255) COLLATE "default",
"telefonecontato" varchar(255) COLLATE "default",
"uf" varchar(255) COLLATE "default",
"zoommapa" int4
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for doenca
-- ----------------------------
DROP TABLE IF EXISTS "sai"."doenca";
CREATE TABLE "sai"."doenca" (
"id" int8 NOT NULL,
"version" int8,
"nome" varchar(255) COLLATE "default",
"observacao" varchar(800) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for doenca_aud
-- ----------------------------
DROP TABLE IF EXISTS "sai"."doenca_aud";
CREATE TABLE "sai"."doenca_aud" (
"id" int8 NOT NULL,
"cd_revisao" int4 NOT NULL,
"tipo_revisao" int2,
"nome" varchar(255) COLLATE "default",
"observacao" varchar(800) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for elemento
-- ----------------------------
DROP TABLE IF EXISTS "sai"."elemento";
CREATE TABLE "sai"."elemento" (
"id" int8 NOT NULL,
"version" int8,
"nome" varchar(255) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for elemento_aud
-- ----------------------------
DROP TABLE IF EXISTS "sai"."elemento_aud";
CREATE TABLE "sai"."elemento_aud" (
"id" int8 NOT NULL,
"cd_revisao" int4 NOT NULL,
"tipo_revisao" int2,
"nome" varchar(255) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for emissor
-- ----------------------------
DROP TABLE IF EXISTS "sai"."emissor";
CREATE TABLE "sai"."emissor" (
"id" int8 NOT NULL,
"version" int8,
"modelo" varchar(255) COLLATE "default",
"pressao" numeric(19,2),
"raio" numeric(19,2),
"tipoemissor" int4,
"vazao" numeric(19,2),
"fabricante_id" int8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for emissor_aud
-- ----------------------------
DROP TABLE IF EXISTS "sai"."emissor_aud";
CREATE TABLE "sai"."emissor_aud" (
"id" int8 NOT NULL,
"cd_revisao" int4 NOT NULL,
"tipo_revisao" int2,
"modelo" varchar(255) COLLATE "default",
"pressao" numeric(19,2),
"raio" numeric(19,2),
"tipoemissor" int4,
"vazao" numeric(19,2),
"fabricante_id" int8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for endereco
-- ----------------------------
DROP TABLE IF EXISTS "sai"."endereco";
CREATE TABLE "sai"."endereco" (
"id" int8 NOT NULL,
"version" int8,
"bairro" varchar(255) COLLATE "default",
"cep" varchar(255) COLLATE "default",
"cidade" varchar(255) COLLATE "default",
"complemento" varchar(255) COLLATE "default",
"estado" varchar(255) COLLATE "default",
"logradouro" varchar(255) COLLATE "default",
"numero" int4
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for energia
-- ----------------------------
DROP TABLE IF EXISTS "sai"."energia";
CREATE TABLE "sai"."energia" (
"id" int8 NOT NULL,
"version" int8,
"data" date,
"demandap" numeric(19,2),
"faturadofp" numeric(19,2),
"faturadop" numeric(19,2),
"outros" numeric(19,2),
"quantidade" numeric(19,2),
"reativofp" numeric(19,2),
"reativop" numeric(19,2),
"valor" numeric(19,2),
"irrigante_id" int8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for energia_aud
-- ----------------------------
DROP TABLE IF EXISTS "sai"."energia_aud";
CREATE TABLE "sai"."energia_aud" (
"id" int8 NOT NULL,
"cd_revisao" int4 NOT NULL,
"tipo_revisao" int2,
"data" date,
"demandap" numeric(19,2),
"faturadofp" numeric(19,2),
"faturadop" numeric(19,2),
"outros" numeric(19,2),
"quantidade" numeric(19,2),
"reativofp" numeric(19,2),
"reativop" numeric(19,2),
"valor" numeric(19,2),
"irrigante_id" int8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for estacao
-- ----------------------------
DROP TABLE IF EXISTS "sai"."estacao";
CREATE TABLE "sai"."estacao" (
"id" int8 NOT NULL,
"version" int8,
"altitude" numeric(19,2),
"datacadastro" date,
"datainatividade" date,
"latitude" varchar(255) COLLATE "default",
"localizacao" varchar(255) COLLATE "default",
"longitude" varchar(255) COLLATE "default",
"nome" varchar(255) COLLATE "default",
"observacao" varchar(800) COLLATE "default",
"distrito_id" int8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for estacao_area
-- ----------------------------
DROP TABLE IF EXISTS "sai"."estacao_area";
CREATE TABLE "sai"."estacao_area" (
"id_estacao" int8 NOT NULL,
"id_area" int8 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for estacao_area_aud
-- ----------------------------
DROP TABLE IF EXISTS "sai"."estacao_area_aud";
CREATE TABLE "sai"."estacao_area_aud" (
"cd_revisao" int4 NOT NULL,
"id_estacao" int8 NOT NULL,
"id_area" int8 NOT NULL,
"tipo_revisao" int2
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for estacao_aud
-- ----------------------------
DROP TABLE IF EXISTS "sai"."estacao_aud";
CREATE TABLE "sai"."estacao_aud" (
"id" int8 NOT NULL,
"cd_revisao" int4 NOT NULL,
"tipo_revisao" int2,
"altitude" numeric(19,2),
"datacadastro" date,
"datainatividade" date,
"latitude" varchar(255) COLLATE "default",
"localizacao" varchar(255) COLLATE "default",
"longitude" varchar(255) COLLATE "default",
"nome" varchar(255) COLLATE "default",
"observacao" varchar(800) COLLATE "default",
"distrito_id" int8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for estadio
-- ----------------------------
DROP TABLE IF EXISTS "sai"."estadio";
CREATE TABLE "sai"."estadio" (
"id" int8 NOT NULL,
"version" int8,
"alturafruta" numeric(19,2),
"diametrocopa" numeric(19,2),
"esgotamento" numeric(19,2),
"fase" int4,
"fatorrendimento" numeric(19,2),
"kc" numeric(19,2),
"ordem" int4,
"poda" bool,
"pospoda" bool,
"profundidaderaiz" numeric(19,2),
"qtddias" int8,
"referencia_id" int8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for estadio_aud
-- ----------------------------
DROP TABLE IF EXISTS "sai"."estadio_aud";
CREATE TABLE "sai"."estadio_aud" (
"id" int8 NOT NULL,
"cd_revisao" int4 NOT NULL,
"tipo_revisao" int2,
"alturafruta" numeric(19,2),
"diametrocopa" numeric(19,2),
"esgotamento" numeric(19,2),
"fase" int4,
"fatorrendimento" numeric(19,2),
"kc" numeric(19,2),
"ordem" int4,
"poda" bool,
"pospoda" bool,
"profundidaderaiz" numeric(19,2),
"qtddias" int8,
"referencia_id" int8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for fabricante
-- ----------------------------
DROP TABLE IF EXISTS "sai"."fabricante";
CREATE TABLE "sai"."fabricante" (
"id" int8 NOT NULL,
"version" int8,
"nome" varchar(255) COLLATE "default",
"observacao" varchar(800) COLLATE "default",
"tipofabricante" int4
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for fabricante_aud
-- ----------------------------
DROP TABLE IF EXISTS "sai"."fabricante_aud";
CREATE TABLE "sai"."fabricante_aud" (
"id" int8 NOT NULL,
"cd_revisao" int4 NOT NULL,
"tipo_revisao" int2,
"nome" varchar(255) COLLATE "default",
"observacao" varchar(800) COLLATE "default",
"tipofabricante" int4
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for fertirrigacaoaplicada
-- ----------------------------
DROP TABLE IF EXISTS "sai"."fertirrigacaoaplicada";
CREATE TABLE "sai"."fertirrigacaoaplicada" (
"id" int8 NOT NULL,
"version" int8,
"base" bool,
"custo" numeric(19,2),
"data" date,
"tempofinal" timestamp(6),
"tempoinicial" timestamp(6),
"volume" numeric(19,2),
"plantio_id" int8,
"tanque_id" int8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for fertirrigacaoaplicada_aud
-- ----------------------------
DROP TABLE IF EXISTS "sai"."fertirrigacaoaplicada_aud";
CREATE TABLE "sai"."fertirrigacaoaplicada_aud" (
"id" int8 NOT NULL,
"cd_revisao" int4 NOT NULL,
"tipo_revisao" int2,
"base" bool,
"custo" numeric(19,2),
"data" date,
"tempofinal" timestamp(6),
"tempoinicial" timestamp(6),
"volume" numeric(19,2),
"plantio_id" int8,
"tanque_id" int8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for fertirrigacaoaplicada_nutriente
-- ----------------------------
DROP TABLE IF EXISTS "sai"."fertirrigacaoaplicada_nutriente";
CREATE TABLE "sai"."fertirrigacaoaplicada_nutriente" (
"fertirrigacaoaplicada_id" int8 NOT NULL,
"nutrientes_id" int8 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for fertirrigacaoaplicada_nutriente_aud
-- ----------------------------
DROP TABLE IF EXISTS "sai"."fertirrigacaoaplicada_nutriente_aud";
CREATE TABLE "sai"."fertirrigacaoaplicada_nutriente_aud" (
"cd_revisao" int4 NOT NULL,
"fertirrigacaoaplicada_id" int8 NOT NULL,
"nutrientes_id" int8 NOT NULL,
"tipo_revisao" int2
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for filtro
-- ----------------------------
DROP TABLE IF EXISTS "sai"."filtro";
CREATE TABLE "sai"."filtro" (
"id" int8 NOT NULL,
"version" int8,
"elemento" int4,
"filtragem" numeric(19,2),
"modelo" varchar(255) COLLATE "default",
"pressao" numeric(19,2),
"tipofiltro" int4,
"fabricante_id" int8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for filtro_aud
-- ----------------------------
DROP TABLE IF EXISTS "sai"."filtro_aud";
CREATE TABLE "sai"."filtro_aud" (
"id" int8 NOT NULL,
"cd_revisao" int4 NOT NULL,
"tipo_revisao" int2,
"elemento" int4,
"filtragem" numeric(19,2),
"modelo" varchar(255) COLLATE "default",
"pressao" numeric(19,2),
"tipofiltro" int4,
"fabricante_id" int8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for fornecedor
-- ----------------------------
DROP TABLE IF EXISTS "sai"."fornecedor";
CREATE TABLE "sai"."fornecedor" (
"id" int8 NOT NULL,
"version" int8,
"celular" varchar(255) COLLATE "default",
"cnpj" varchar(255) COLLATE "default",
"codigo" int8,
"contato" varchar(255) COLLATE "default",
"cpf" varchar(255) COLLATE "default",
"nome" varchar(255) COLLATE "default",
"observacao" varchar(255) COLLATE "default",
"telefone" varchar(255) COLLATE "default",
"endereco_id" int8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for funcionalidade
-- ----------------------------
DROP TABLE IF EXISTS "sai"."funcionalidade";
CREATE TABLE "sai"."funcionalidade" (
"id" int8 NOT NULL,
"version" int8,
"descricao" varchar(255) COLLATE "default",
"mnemonico" varchar(255) COLLATE "default",
"ordem" int4
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for funcionalidade_aud
-- ----------------------------
DROP TABLE IF EXISTS "sai"."funcionalidade_aud";
CREATE TABLE "sai"."funcionalidade_aud" (
"id" int8 NOT NULL,
"cd_revisao" int4 NOT NULL,
"tipo_revisao" int2,
"descricao" varchar(255) COLLATE "default",
"mnemonico" varchar(255) COLLATE "default",
"ordem" int4
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for funcionalidade_perfil
-- ----------------------------
DROP TABLE IF EXISTS "sai"."funcionalidade_perfil";
CREATE TABLE "sai"."funcionalidade_perfil" (
"id_perfil" int8 NOT NULL,
"id_funcionalidade" int8 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for funcionalidade_perfil_aud
-- ----------------------------
DROP TABLE IF EXISTS "sai"."funcionalidade_perfil_aud";
CREATE TABLE "sai"."funcionalidade_perfil_aud" (
"cd_revisao" int4 NOT NULL,
"id_perfil" int8 NOT NULL,
"id_funcionalidade" int8 NOT NULL,
"tipo_revisao" int2
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for indicadores
-- ----------------------------
DROP TABLE IF EXISTS "sai"."indicadores";
CREATE TABLE "sai"."indicadores" (
"id" int8 NOT NULL,
"version" int8,
"areatotal" numeric(19,2),
"ca" numeric(19,2),
"chm3" numeric(19,2),
"ciclocultura" int4,
"consumobacia" numeric(19,2),
"data" date,
"empregos" numeric(19,2),
"empregosha" numeric(19,2),
"empregosm3" numeric(19,2),
"kgha" numeric(19,2),
"kgm3" numeric(19,2),
"laminaaplicada" numeric(19,2),
"litrossegundo" numeric(19,2),
"litrossegundoha" numeric(19,2),
"m3ha" numeric(19,2),
"pesoempregosha" int4,
"pesoempregosm3" int4,
"pesokgha" int4,
"pesokgm3" int4,
"pesolitrossegundoha" int4,
"pesom3ha" int4,
"pesomedio" numeric(19,2),
"pesoreceitaliquidaha" int4,
"pesoreceitaliquidam3" int4,
"produtividade" numeric(19,2),
"qtddiascicloanual" numeric(19,2),
"r" numeric(19,2),
"receitaliquida" numeric(19,2),
"receitaliquidaha" numeric(19,2),
"receitaliquidam3" numeric(19,2),
"vbp" numeric(19,2),
"atividadeeconomica_id" int8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for indicadores_aud
-- ----------------------------
DROP TABLE IF EXISTS "sai"."indicadores_aud";
CREATE TABLE "sai"."indicadores_aud" (
"id" int8 NOT NULL,
"cd_revisao" int4 NOT NULL,
"tipo_revisao" int2,
"areatotal" numeric(19,2),
"ca" numeric(19,2),
"chm3" numeric(19,2),
"ciclocultura" int4,
"consumobacia" numeric(19,2),
"data" date,
"empregos" numeric(19,2),
"empregosha" numeric(19,2),
"empregosm3" numeric(19,2),
"kgha" numeric(19,2),
"kgm3" numeric(19,2),
"laminaaplicada" numeric(19,2),
"litrossegundo" numeric(19,2),
"litrossegundoha" numeric(19,2),
"m3ha" numeric(19,2),
"pesoempregosha" int4,
"pesoempregosm3" int4,
"pesokgha" int4,
"pesokgm3" int4,
"pesolitrossegundoha" int4,
"pesom3ha" int4,
"pesomedio" numeric(19,2),
"pesoreceitaliquidaha" int4,
"pesoreceitaliquidam3" int4,
"produtividade" numeric(19,2),
"qtddiascicloanual" numeric(19,2),
"r" numeric(19,2),
"receitaliquida" numeric(19,2),
"receitaliquidaha" numeric(19,2),
"receitaliquidam3" numeric(19,2),
"vbp" numeric(19,2),
"atividadeeconomica_id" int8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for irrigacao
-- ----------------------------
DROP TABLE IF EXISTS "sai"."irrigacao";
CREATE TABLE "sai"."irrigacao" (
"id" int8 NOT NULL,
"version" int8,
"chuva" numeric(19,2),
"data" date,
"eficiencia" numeric(19,2),
"emailenviado" bool,
"enviaremail" bool DEFAULT false NOT NULL,
"enviarsms" bool DEFAULT false NOT NULL,
"estadio" int4,
"etc" numeric(19,2),
"eto" numeric(19,2),
"kc" numeric(19,2),
"memoriacalculo" varchar(800) COLLATE "default",
"motivo" varchar(255) COLLATE "default",
"smsenviado" bool,
"tempoirrigacao" numeric(19,2),
"z" numeric(19,2),
"plantio_id" int8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for irrigacao_aud
-- ----------------------------
DROP TABLE IF EXISTS "sai"."irrigacao_aud";
CREATE TABLE "sai"."irrigacao_aud" (
"id" int8 NOT NULL,
"cd_revisao" int4 NOT NULL,
"tipo_revisao" int2,
"chuva" numeric(19,2),
"data" date,
"eficiencia" numeric(19,2),
"emailenviado" bool,
"enviaremail" bool DEFAULT false,
"enviarsms" bool DEFAULT false,
"estadio" int4,
"etc" numeric(19,2),
"eto" numeric(19,2),
"kc" numeric(19,2),
"memoriacalculo" varchar(800) COLLATE "default",
"motivo" varchar(255) COLLATE "default",
"smsenviado" bool,
"tempoirrigacao" numeric(19,2),
"z" numeric(19,2),
"plantio_id" int8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for irrigante
-- ----------------------------
DROP TABLE IF EXISTS "sai"."irrigante";
CREATE TABLE "sai"."irrigante" (
"id" int8 NOT NULL,
"version" int8,
"areairrigada" numeric(19,2),
"areatotal" numeric(19,2),
"celular" varchar(255) COLLATE "default",
"cep" varchar(255) COLLATE "default",
"cidade" varchar(255) COLLATE "default",
"classificacao" int4,
"cnpj" varchar(255) COLLATE "default",
"codigo" varchar(255) COLLATE "default",
"cpf" varchar(255) COLLATE "default",
"datacadastro" date,
"datainatividade" date,
"endereco" varchar(255) COLLATE "default",
"enviaremail" bool DEFAULT false NOT NULL,
"enviarsms" bool DEFAULT false NOT NULL,
"escolaridade" int4,
"foto" varchar(255) COLLATE "default",
"lacrehidrometro" varchar(255) COLLATE "default",
"latitude" varchar(255) COLLATE "default",
"localizacao" varchar(255) COLLATE "default",
"longitude" varchar(255) COLLATE "default",
"nome" varchar(255) COLLATE "default",
"observacao" varchar(800) COLLATE "default",
"operadora" int4,
"piloto" bool,
"possuiinternet" bool,
"pressao" numeric(19,2),
"rg" varchar(255) COLLATE "default",
"sistemaautomatico" bool,
"telefonecontato" varchar(255) COLLATE "default",
"tipoirrigante" int4,
"uf" varchar(255) COLLATE "default",
"area_id" int8,
"usuario_id" int8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for irrigante_aud
-- ----------------------------
DROP TABLE IF EXISTS "sai"."irrigante_aud";
CREATE TABLE "sai"."irrigante_aud" (
"id" int8 NOT NULL,
"cd_revisao" int4 NOT NULL,
"tipo_revisao" int2,
"areairrigada" numeric(19,2),
"areatotal" numeric(19,2),
"celular" varchar(255) COLLATE "default",
"cep" varchar(255) COLLATE "default",
"cidade" varchar(255) COLLATE "default",
"classificacao" int4,
"cnpj" varchar(255) COLLATE "default",
"codigo" varchar(255) COLLATE "default",
"cpf" varchar(255) COLLATE "default",
"datacadastro" date,
"datainatividade" date,
"endereco" varchar(255) COLLATE "default",
"enviaremail" bool DEFAULT false,
"enviarsms" bool DEFAULT false,
"escolaridade" int4,
"foto" varchar(255) COLLATE "default",
"lacrehidrometro" varchar(255) COLLATE "default",
"latitude" varchar(255) COLLATE "default",
"localizacao" varchar(255) COLLATE "default",
"longitude" varchar(255) COLLATE "default",
"nome" varchar(255) COLLATE "default",
"observacao" varchar(800) COLLATE "default",
"operadora" int4,
"piloto" bool,
"possuiinternet" bool,
"pressao" numeric(19,2),
"rg" varchar(255) COLLATE "default",
"sistemaautomatico" bool,
"telefonecontato" varchar(255) COLLATE "default",
"tipoirrigante" int4,
"uf" varchar(255) COLLATE "default",
"area_id" int8,
"usuario_id" int8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for irrigante_bomba
-- ----------------------------
DROP TABLE IF EXISTS "sai"."irrigante_bomba";
CREATE TABLE "sai"."irrigante_bomba" (
"irrigante_id" int8 NOT NULL,
"bombas_id" int8 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for irrigante_bomba_aud
-- ----------------------------
DROP TABLE IF EXISTS "sai"."irrigante_bomba_aud";
CREATE TABLE "sai"."irrigante_bomba_aud" (
"cd_revisao" int4 NOT NULL,
"irrigante_id" int8 NOT NULL,
"bombas_id" int8 NOT NULL,
"tipo_revisao" int2
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for irrigante_filtro
-- ----------------------------
DROP TABLE IF EXISTS "sai"."irrigante_filtro";
CREATE TABLE "sai"."irrigante_filtro" (
"irrigante_id" int8 NOT NULL,
"filtros_id" int8 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for irrigante_filtro_aud
-- ----------------------------
DROP TABLE IF EXISTS "sai"."irrigante_filtro_aud";
CREATE TABLE "sai"."irrigante_filtro_aud" (
"cd_revisao" int4 NOT NULL,
"irrigante_id" int8 NOT NULL,
"filtros_id" int8 NOT NULL,
"tipo_revisao" int2
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for irrigante_tubo
-- ----------------------------
DROP TABLE IF EXISTS "sai"."irrigante_tubo";
CREATE TABLE "sai"."irrigante_tubo" (
"irrigante_id" int8 NOT NULL,
"tubos_id" int8 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for irrigante_tubo_aud
-- ----------------------------
DROP TABLE IF EXISTS "sai"."irrigante_tubo_aud";
CREATE TABLE "sai"."irrigante_tubo_aud" (
"cd_revisao" int4 NOT NULL,
"irrigante_id" int8 NOT NULL,
"tubos_id" int8 NOT NULL,
"tipo_revisao" int2
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for irrigante_valvula
-- ----------------------------
DROP TABLE IF EXISTS "sai"."irrigante_valvula";
CREATE TABLE "sai"."irrigante_valvula" (
"irrigante_id" int8 NOT NULL,
"valvulas_id" int8 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for irrigante_valvula_aud
-- ----------------------------
DROP TABLE IF EXISTS "sai"."irrigante_valvula_aud";
CREATE TABLE "sai"."irrigante_valvula_aud" (
"cd_revisao" int4 NOT NULL,
"irrigante_id" int8 NOT NULL,
"valvulas_id" int8 NOT NULL,
"tipo_revisao" int2
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for mensagem
-- ----------------------------
DROP TABLE IF EXISTS "sai"."mensagem";
CREATE TABLE "sai"."mensagem" (
"id" int8 NOT NULL,
"version" int8,
"anexo" varchar(255) COLLATE "default",
"assunto" varchar(255) COLLATE "default",
"dataenvio" date,
"texto" varchar(255) COLLATE "default",
"tipomensagem" int4,
"distrito_id" int8,
"usuario_id" int8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for mensagem_aud
-- ----------------------------
DROP TABLE IF EXISTS "sai"."mensagem_aud";
CREATE TABLE "sai"."mensagem_aud" (
"id" int8 NOT NULL,
"cd_revisao" int4 NOT NULL,
"tipo_revisao" int2,
"anexo" varchar(255) COLLATE "default",
"assunto" varchar(255) COLLATE "default",
"dataenvio" date,
"texto" varchar(255) COLLATE "default",
"tipomensagem" int4,
"distrito_id" int8,
"usuario_id" int8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for mensagem_irrigante
-- ----------------------------
DROP TABLE IF EXISTS "sai"."mensagem_irrigante";
CREATE TABLE "sai"."mensagem_irrigante" (
"mensagem_id" int8 NOT NULL,
"destinatarios_id" int8 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for mensagem_irrigante_aud
-- ----------------------------
DROP TABLE IF EXISTS "sai"."mensagem_irrigante_aud";
CREATE TABLE "sai"."mensagem_irrigante_aud" (
"cd_revisao" int4 NOT NULL,
"mensagem_id" int8 NOT NULL,
"destinatarios_id" int8 NOT NULL,
"tipo_revisao" int2
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for modulo
-- ----------------------------
DROP TABLE IF EXISTS "sai"."modulo";
CREATE TABLE "sai"."modulo" (
"id" int8 NOT NULL,
"version" int8,
"descricao" varchar(255) COLLATE "default",
"imagem" varchar(255) COLLATE "default",
"siglas" varchar(255) COLLATE "default",
"url" varchar(255) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for municipio
-- ----------------------------
DROP TABLE IF EXISTS "sai"."municipio";
CREATE TABLE "sai"."municipio" (
"id" int8 NOT NULL,
"version" int8,
"areairrigada" numeric(19,2),
"areatotal" numeric(19,2),
"classificacao" int4,
"codigo" varchar(255) COLLATE "default",
"datacadastro" date,
"foto" varchar(255) COLLATE "default",
"latitude" varchar(255) COLLATE "default",
"localizacao" varchar(255) COLLATE "default",
"longitude" varchar(255) COLLATE "default",
"nome" varchar(255) COLLATE "default",
"observacao" varchar(800) COLLATE "default",
"bacia_id" int8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for municipio_aud
-- ----------------------------
DROP TABLE IF EXISTS "sai"."municipio_aud";
CREATE TABLE "sai"."municipio_aud" (
"id" int8 NOT NULL,
"cd_revisao" int4 NOT NULL,
"tipo_revisao" int2,
"areairrigada" numeric(19,2),
"areatotal" numeric(19,2),
"classificacao" int4,
"codigo" varchar(255) COLLATE "default",
"datacadastro" date,
"foto" varchar(255) COLLATE "default",
"latitude" varchar(255) COLLATE "default",
"localizacao" varchar(255) COLLATE "default",
"longitude" varchar(255) COLLATE "default",
"nome" varchar(255) COLLATE "default",
"observacao" varchar(800) COLLATE "default",
"bacia_id" int8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for nutriente
-- ----------------------------
DROP TABLE IF EXISTS "sai"."nutriente";
CREATE TABLE "sai"."nutriente" (
"id" int8 NOT NULL,
"version" int8,
"nome" varchar(255) COLLATE "default",
"percentual" int4,
"tiponutriente" int4,
"elemento_id" int8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for nutriente_aud
-- ----------------------------
DROP TABLE IF EXISTS "sai"."nutriente_aud";
CREATE TABLE "sai"."nutriente_aud" (
"id" int8 NOT NULL,
"cd_revisao" int4 NOT NULL,
"tipo_revisao" int2,
"nome" varchar(255) COLLATE "default",
"percentual" int4,
"tiponutriente" int4,
"elemento_id" int8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for parametro
-- ----------------------------
DROP TABLE IF EXISTS "sai"."parametro";
CREATE TABLE "sai"."parametro" (
"id" int8 NOT NULL,
"version" int8,
"codigo" varchar(20) COLLATE "default" NOT NULL,
"descricao" varchar(80) COLLATE "default",
"valor" varchar(50) COLLATE "default" NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for parametro_aud
-- ----------------------------
DROP TABLE IF EXISTS "sai"."parametro_aud";
CREATE TABLE "sai"."parametro_aud" (
"id" int8 NOT NULL,
"cd_revisao" int4 NOT NULL,
"tipo_revisao" int2,
"codigo" varchar(20) COLLATE "default",
"descricao" varchar(80) COLLATE "default",
"valor" varchar(50) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for perfil
-- ----------------------------
DROP TABLE IF EXISTS "sai"."perfil";
CREATE TABLE "sai"."perfil" (
"id" int8 NOT NULL,
"version" int8,
"descricao" varchar(255) COLLATE "default",
"tipo" int4
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for perfil_aud
-- ----------------------------
DROP TABLE IF EXISTS "sai"."perfil_aud";
CREATE TABLE "sai"."perfil_aud" (
"id" int8 NOT NULL,
"cd_revisao" int4 NOT NULL,
"tipo_revisao" int2,
"descricao" varchar(255) COLLATE "default",
"tipo" int4
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for plantio
-- ----------------------------
DROP TABLE IF EXISTS "sai"."plantio";
CREATE TABLE "sai"."plantio" (
"id" int8 NOT NULL,
"version" int8,
"areatotal" numeric(19,2),
"consorcio" bool DEFAULT false NOT NULL,
"datafim" date,
"enviaremail" bool DEFAULT false NOT NULL,
"enviarsms" bool DEFAULT false NOT NULL,
"espe" float8,
"esple" float8,
"espacamento1" float8,
"espacamento2" float8,
"espacamento3" float8,
"fertirrigacao" bool,
"motivofinalizacao" int4,
"sequeiro" bool DEFAULT false NOT NULL,
"tipocalculokl" int4,
"tipofertirrigacao" int4,
"tipoformulagotejo" int4,
"tipoformulamicro" int4,
"tipomanejoirrigacao" int4,
"totalemissores" int4,
"totalplanta" int4,
"turnorega" int4,
"cultivo_id" int8,
"setor_id" int8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for plantio_adubo
-- ----------------------------
DROP TABLE IF EXISTS "sai"."plantio_adubo";
CREATE TABLE "sai"."plantio_adubo" (
"plantio_id" int8 NOT NULL,
"adubos_id" int8 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for plantio_adubo_aud
-- ----------------------------
DROP TABLE IF EXISTS "sai"."plantio_adubo_aud";
CREATE TABLE "sai"."plantio_adubo_aud" (
"cd_revisao" int4 NOT NULL,
"plantio_id" int8 NOT NULL,
"adubos_id" int8 NOT NULL,
"tipo_revisao" int2
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for plantio_aud
-- ----------------------------
DROP TABLE IF EXISTS "sai"."plantio_aud";
CREATE TABLE "sai"."plantio_aud" (
"id" int8 NOT NULL,
"cd_revisao" int4 NOT NULL,
"tipo_revisao" int2,
"areatotal" numeric(19,2),
"consorcio" bool DEFAULT false,
"datafim" date,
"enviaremail" bool DEFAULT false,
"enviarsms" bool DEFAULT false,
"espe" float8,
"esple" float8,
"espacamento1" float8,
"espacamento2" float8,
"espacamento3" float8,
"fertirrigacao" bool,
"motivofinalizacao" int4,
"sequeiro" bool DEFAULT false,
"tipocalculokl" int4,
"tipofertirrigacao" int4,
"tipoformulagotejo" int4,
"tipoformulamicro" int4,
"tipomanejoirrigacao" int4,
"totalemissores" int4,
"totalplanta" int4,
"turnorega" int4,
"cultivo_id" int8,
"setor_id" int8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for plantio_colheita
-- ----------------------------
DROP TABLE IF EXISTS "sai"."plantio_colheita";
CREATE TABLE "sai"."plantio_colheita" (
"plantio_id" int8 NOT NULL,
"colheitas_id" int8 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for plantio_colheita_aud
-- ----------------------------
DROP TABLE IF EXISTS "sai"."plantio_colheita_aud";
CREATE TABLE "sai"."plantio_colheita_aud" (
"cd_revisao" int4 NOT NULL,
"plantio_id" int8 NOT NULL,
"colheitas_id" int8 NOT NULL,
"tipo_revisao" int2
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for plantio_defensivo
-- ----------------------------
DROP TABLE IF EXISTS "sai"."plantio_defensivo";
CREATE TABLE "sai"."plantio_defensivo" (
"plantio_id" int8 NOT NULL,
"defensivos_id" int8 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for plantio_defensivo_aud
-- ----------------------------
DROP TABLE IF EXISTS "sai"."plantio_defensivo_aud";
CREATE TABLE "sai"."plantio_defensivo_aud" (
"cd_revisao" int4 NOT NULL,
"plantio_id" int8 NOT NULL,
"defensivos_id" int8 NOT NULL,
"tipo_revisao" int2
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for plantio_doenca
-- ----------------------------
DROP TABLE IF EXISTS "sai"."plantio_doenca";
CREATE TABLE "sai"."plantio_doenca" (
"plantio_id" int8 NOT NULL,
"doencas_id" int8 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for plantio_doenca_aud
-- ----------------------------
DROP TABLE IF EXISTS "sai"."plantio_doenca_aud";
CREATE TABLE "sai"."plantio_doenca_aud" (
"cd_revisao" int4 NOT NULL,
"plantio_id" int8 NOT NULL,
"doencas_id" int8 NOT NULL,
"tipo_revisao" int2
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for plantio_emissor
-- ----------------------------
DROP TABLE IF EXISTS "sai"."plantio_emissor";
CREATE TABLE "sai"."plantio_emissor" (
"plantio_id" int8 NOT NULL,
"emissores_id" int8 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for plantio_emissor_aud
-- ----------------------------
DROP TABLE IF EXISTS "sai"."plantio_emissor_aud";
CREATE TABLE "sai"."plantio_emissor_aud" (
"cd_revisao" int4 NOT NULL,
"plantio_id" int8 NOT NULL,
"emissores_id" int8 NOT NULL,
"tipo_revisao" int2
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for plantio_praga
-- ----------------------------
DROP TABLE IF EXISTS "sai"."plantio_praga";
CREATE TABLE "sai"."plantio_praga" (
"plantio_id" int8 NOT NULL,
"pragas_id" int8 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for plantio_praga_aud
-- ----------------------------
DROP TABLE IF EXISTS "sai"."plantio_praga_aud";
CREATE TABLE "sai"."plantio_praga_aud" (
"cd_revisao" int4 NOT NULL,
"plantio_id" int8 NOT NULL,
"pragas_id" int8 NOT NULL,
"tipo_revisao" int2
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for praga
-- ----------------------------
DROP TABLE IF EXISTS "sai"."praga";
CREATE TABLE "sai"."praga" (
"id" int8 NOT NULL,
"version" int8,
"nome" varchar(255) COLLATE "default",
"observacao" varchar(800) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for praga_aud
-- ----------------------------
DROP TABLE IF EXISTS "sai"."praga_aud";
CREATE TABLE "sai"."praga_aud" (
"id" int8 NOT NULL,
"cd_revisao" int4 NOT NULL,
"tipo_revisao" int2,
"nome" varchar(255) COLLATE "default",
"observacao" varchar(800) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for referencia
-- ----------------------------
DROP TABLE IF EXISTS "sai"."referencia";
CREATE TABLE "sai"."referencia" (
"id" int8 NOT NULL,
"version" int8,
"ano" int4,
"autor" varchar(255) COLLATE "default",
"cidadep" varchar(255) COLLATE "default",
"coautor" bool,
"estadop" varchar(255) COLLATE "default",
"observacao" varchar(800) COLLATE "default",
"paisp" varchar(255) COLLATE "default",
"tipometodologiaetc" int4,
"tipometodologiaeto" int4,
"variedade_id" int8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for referencia_aud
-- ----------------------------
DROP TABLE IF EXISTS "sai"."referencia_aud";
CREATE TABLE "sai"."referencia_aud" (
"id" int8 NOT NULL,
"cd_revisao" int4 NOT NULL,
"tipo_revisao" int2,
"ano" int4,
"autor" varchar(255) COLLATE "default",
"cidadep" varchar(255) COLLATE "default",
"coautor" bool,
"estadop" varchar(255) COLLATE "default",
"observacao" varchar(800) COLLATE "default",
"paisp" varchar(255) COLLATE "default",
"tipometodologiaetc" int4,
"tipometodologiaeto" int4,
"variedade_id" int8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for revisao
-- ----------------------------
DROP TABLE IF EXISTS "sai"."revisao";
CREATE TABLE "sai"."revisao" (
"id" int4 NOT NULL,
"dataauditoria" timestamp(6),
"usuario_id" int8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for setor
-- ----------------------------
DROP TABLE IF EXISTS "sai"."setor";
CREATE TABLE "sai"."setor" (
"id" int8 NOT NULL,
"version" int8,
"areatotal" numeric(19,2),
"cc" numeric(19,2),
"esgotamento" numeric(19,2),
"fatorhidrico" numeric(19,2),
"identificacao" int4,
"massasolo" numeric(19,2),
"maxtaxainfiltracaochuva" numeric(19,2),
"observacao" varchar(800) COLLATE "default",
"pmp" numeric(19,2),
"tiposolo" int4,
"umidadeinicial" numeric(19,2),
"vazaoobservada" numeric(19,2),
"irrigante_id" int8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for setor_aud
-- ----------------------------
DROP TABLE IF EXISTS "sai"."setor_aud";
CREATE TABLE "sai"."setor_aud" (
"id" int8 NOT NULL,
"cd_revisao" int4 NOT NULL,
"tipo_revisao" int2,
"areatotal" numeric(19,2),
"cc" numeric(19,2),
"esgotamento" numeric(19,2),
"fatorhidrico" numeric(19,2),
"identificacao" int4,
"massasolo" numeric(19,2),
"maxtaxainfiltracaochuva" numeric(19,2),
"observacao" varchar(800) COLLATE "default",
"pmp" numeric(19,2),
"tiposolo" int4,
"umidadeinicial" numeric(19,2),
"vazaoobservada" numeric(19,2),
"irrigante_id" int8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for setor_emissor
-- ----------------------------
DROP TABLE IF EXISTS "sai"."setor_emissor";
CREATE TABLE "sai"."setor_emissor" (
"setor_id" int8 NOT NULL,
"emissores_id" int8 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for setor_emissor_aud
-- ----------------------------
DROP TABLE IF EXISTS "sai"."setor_emissor_aud";
CREATE TABLE "sai"."setor_emissor_aud" (
"cd_revisao" int4 NOT NULL,
"setor_id" int8 NOT NULL,
"emissores_id" int8 NOT NULL,
"tipo_revisao" int2
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for solo
-- ----------------------------
DROP TABLE IF EXISTS "sai"."solo";
CREATE TABLE "sai"."solo" (
"id" int8 NOT NULL,
"version" int8,
"cc" numeric(19,2),
"esgotamento" numeric(19,2),
"massasolo" numeric(19,2),
"maxtaxainfiltracaochuva" numeric(19,2),
"nome" varchar(255) COLLATE "default",
"pmp" numeric(19,2),
"tiposolo" int4,
"umidadeinicial" numeric(19,2)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for solo_aud
-- ----------------------------
DROP TABLE IF EXISTS "sai"."solo_aud";
CREATE TABLE "sai"."solo_aud" (
"id" int8 NOT NULL,
"cd_revisao" int4 NOT NULL,
"tipo_revisao" int2,
"cc" numeric(19,2),
"esgotamento" numeric(19,2),
"massasolo" numeric(19,2),
"maxtaxainfiltracaochuva" numeric(19,2),
"nome" varchar(255) COLLATE "default",
"pmp" numeric(19,2),
"tiposolo" int4,
"umidadeinicial" numeric(19,2)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for solucao
-- ----------------------------
DROP TABLE IF EXISTS "sai"."solucao";
CREATE TABLE "sai"."solucao" (
"id" int8 NOT NULL,
"version" int8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for solucao_aud
-- ----------------------------
DROP TABLE IF EXISTS "sai"."solucao_aud";
CREATE TABLE "sai"."solucao_aud" (
"id" int8 NOT NULL,
"cd_revisao" int4 NOT NULL,
"tipo_revisao" int2
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for solucao_nutriente
-- ----------------------------
DROP TABLE IF EXISTS "sai"."solucao_nutriente";
CREATE TABLE "sai"."solucao_nutriente" (
"solucao_id" int8 NOT NULL,
"nutrientes_id" int8 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for solucao_nutriente_aud
-- ----------------------------
DROP TABLE IF EXISTS "sai"."solucao_nutriente_aud";
CREATE TABLE "sai"."solucao_nutriente_aud" (
"cd_revisao" int4 NOT NULL,
"solucao_id" int8 NOT NULL,
"nutrientes_id" int8 NOT NULL,
"tipo_revisao" int2
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for tanque
-- ----------------------------
DROP TABLE IF EXISTS "sai"."tanque";
CREATE TABLE "sai"."tanque" (
"id" int8 NOT NULL,
"version" int8,
"nome" varchar(255) COLLATE "default",
"vazao" numeric(19,2),
"volume" numeric(19,2),
"bomba_id" int8,
"tanquedestino_id" int8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for tanque_aud
-- ----------------------------
DROP TABLE IF EXISTS "sai"."tanque_aud";
CREATE TABLE "sai"."tanque_aud" (
"id" int8 NOT NULL,
"cd_revisao" int4 NOT NULL,
"tipo_revisao" int2,
"nome" varchar(255) COLLATE "default",
"vazao" numeric(19,2),
"volume" numeric(19,2),
"bomba_id" int8,
"tanquedestino_id" int8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for tecnico
-- ----------------------------
DROP TABLE IF EXISTS "sai"."tecnico";
CREATE TABLE "sai"."tecnico" (
"id" int8 NOT NULL,
"version" int8,
"celular" varchar(255) COLLATE "default",
"cep" varchar(255) COLLATE "default",
"cidade" varchar(255) COLLATE "default",
"datacadastro" date,
"datainatividade" date,
"endereco" varchar(255) COLLATE "default",
"escolaridade" int4,
"foto" varchar(255) COLLATE "default",
"nome" varchar(255) COLLATE "default",
"observacao" varchar(800) COLLATE "default",
"operadora" int4,
"telefonecontato" varchar(255) COLLATE "default",
"uf" varchar(255) COLLATE "default",
"distrito_id" int8,
"usuario_id" int8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for tecnico_aud
-- ----------------------------
DROP TABLE IF EXISTS "sai"."tecnico_aud";
CREATE TABLE "sai"."tecnico_aud" (
"id" int8 NOT NULL,
"cd_revisao" int4 NOT NULL,
"tipo_revisao" int2,
"celular" varchar(255) COLLATE "default",
"cep" varchar(255) COLLATE "default",
"cidade" varchar(255) COLLATE "default",
"datacadastro" date,
"datainatividade" date,
"endereco" varchar(255) COLLATE "default",
"escolaridade" int4,
"foto" varchar(255) COLLATE "default",
"nome" varchar(255) COLLATE "default",
"observacao" varchar(800) COLLATE "default",
"operadora" int4,
"telefonecontato" varchar(255) COLLATE "default",
"uf" varchar(255) COLLATE "default",
"distrito_id" int8,
"usuario_id" int8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for testeeficiencia
-- ----------------------------
DROP TABLE IF EXISTS "sai"."testeeficiencia";
CREATE TABLE "sai"."testeeficiencia" (
"id" int8 NOT NULL,
"version" int8,
"data" date,
"eficiencia" numeric(19,2),
"linha1emissor1rep1" numeric(19,2),
"linha1emissor1rep2" numeric(19,2),
"linha1emissor1rep3" numeric(19,2),
"linha1emissor2id" varchar(255) COLLATE "default",
"linha1emissor2rep1" numeric(19,2),
"linha1emissor2rep2" numeric(19,2),
"linha1emissor2rep3" numeric(19,2),
"linha1emissor3id" varchar(255) COLLATE "default",
"linha1emissor3rep1" numeric(19,2),
"linha1emissor3rep2" numeric(19,2),
"linha1emissor3rep3" numeric(19,2),
"linha1emissor4rep1" numeric(19,2),
"linha1emissor4rep2" numeric(19,2),
"linha1emissor4rep3" numeric(19,2),
"linha2emissor1rep1" numeric(19,2),
"linha2emissor1rep2" numeric(19,2),
"linha2emissor1rep3" numeric(19,2),
"linha2emissor2id" varchar(255) COLLATE "default",
"linha2emissor2rep1" numeric(19,2),
"linha2emissor2rep2" numeric(19,2),
"linha2emissor2rep3" numeric(19,2),
"linha2emissor3id" varchar(255) COLLATE "default",
"linha2emissor3rep1" numeric(19,2),
"linha2emissor3rep2" numeric(19,2),
"linha2emissor3rep3" numeric(19,2),
"linha2emissor4rep1" numeric(19,2),
"linha2emissor4rep2" numeric(19,2),
"linha2emissor4rep3" numeric(19,2),
"linha3emissor1rep1" numeric(19,2),
"linha3emissor1rep2" numeric(19,2),
"linha3emissor1rep3" numeric(19,2),
"linha3emissor2id" varchar(255) COLLATE "default",
"linha3emissor2rep1" numeric(19,2),
"linha3emissor2rep2" numeric(19,2),
"linha3emissor2rep3" numeric(19,2),
"linha3emissor3id" varchar(255) COLLATE "default",
"linha3emissor3rep1" numeric(19,2),
"linha3emissor3rep2" numeric(19,2),
"linha3emissor3rep3" numeric(19,2),
"linha3emissor4rep1" numeric(19,2),
"linha3emissor4rep2" numeric(19,2),
"linha3emissor4rep3" numeric(19,2),
"linha4emissor1rep1" numeric(19,2),
"linha4emissor1rep2" numeric(19,2),
"linha4emissor1rep3" numeric(19,2),
"linha4emissor2id" varchar(255) COLLATE "default",
"linha4emissor2rep1" numeric(19,2),
"linha4emissor2rep2" numeric(19,2),
"linha4emissor2rep3" numeric(19,2),
"linha4emissor3id" varchar(255) COLLATE "default",
"linha4emissor3rep1" numeric(19,2),
"linha4emissor3rep2" numeric(19,2),
"linha4emissor3rep3" numeric(19,2),
"linha4emissor4rep1" numeric(19,2),
"linha4emissor4rep2" numeric(19,2),
"linha4emissor4rep3" numeric(19,2),
"observacao" varchar(2000) COLLATE "default",
"pressaofinallinha1" numeric(19,2),
"pressaofinallinha2" numeric(19,2),
"pressaofinallinha3" numeric(19,2),
"pressaofinallinha4" numeric(19,2),
"tempoteste" int4,
"responsavel_id" int8,
"setor_id" int8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for testeeficiencia_aud
-- ----------------------------
DROP TABLE IF EXISTS "sai"."testeeficiencia_aud";
CREATE TABLE "sai"."testeeficiencia_aud" (
"id" int8 NOT NULL,
"cd_revisao" int4 NOT NULL,
"tipo_revisao" int2,
"data" date,
"eficiencia" numeric(19,2),
"linha1emissor1rep1" numeric(19,2),
"linha1emissor1rep2" numeric(19,2),
"linha1emissor1rep3" numeric(19,2),
"linha1emissor2id" varchar(255) COLLATE "default",
"linha1emissor2rep1" numeric(19,2),
"linha1emissor2rep2" numeric(19,2),
"linha1emissor2rep3" numeric(19,2),
"linha1emissor3id" varchar(255) COLLATE "default",
"linha1emissor3rep1" numeric(19,2),
"linha1emissor3rep2" numeric(19,2),
"linha1emissor3rep3" numeric(19,2),
"linha1emissor4rep1" numeric(19,2),
"linha1emissor4rep2" numeric(19,2),
"linha1emissor4rep3" numeric(19,2),
"linha2emissor1rep1" numeric(19,2),
"linha2emissor1rep2" numeric(19,2),
"linha2emissor1rep3" numeric(19,2),
"linha2emissor2id" varchar(255) COLLATE "default",
"linha2emissor2rep1" numeric(19,2),
"linha2emissor2rep2" numeric(19,2),
"linha2emissor2rep3" numeric(19,2),
"linha2emissor3id" varchar(255) COLLATE "default",
"linha2emissor3rep1" numeric(19,2),
"linha2emissor3rep2" numeric(19,2),
"linha2emissor3rep3" numeric(19,2),
"linha2emissor4rep1" numeric(19,2),
"linha2emissor4rep2" numeric(19,2),
"linha2emissor4rep3" numeric(19,2),
"linha3emissor1rep1" numeric(19,2),
"linha3emissor1rep2" numeric(19,2),
"linha3emissor1rep3" numeric(19,2),
"linha3emissor2id" varchar(255) COLLATE "default",
"linha3emissor2rep1" numeric(19,2),
"linha3emissor2rep2" numeric(19,2),
"linha3emissor2rep3" numeric(19,2),
"linha3emissor3id" varchar(255) COLLATE "default",
"linha3emissor3rep1" numeric(19,2),
"linha3emissor3rep2" numeric(19,2),
"linha3emissor3rep3" numeric(19,2),
"linha3emissor4rep1" numeric(19,2),
"linha3emissor4rep2" numeric(19,2),
"linha3emissor4rep3" numeric(19,2),
"linha4emissor1rep1" numeric(19,2),
"linha4emissor1rep2" numeric(19,2),
"linha4emissor1rep3" numeric(19,2),
"linha4emissor2id" varchar(255) COLLATE "default",
"linha4emissor2rep1" numeric(19,2),
"linha4emissor2rep2" numeric(19,2),
"linha4emissor2rep3" numeric(19,2),
"linha4emissor3id" varchar(255) COLLATE "default",
"linha4emissor3rep1" numeric(19,2),
"linha4emissor3rep2" numeric(19,2),
"linha4emissor3rep3" numeric(19,2),
"linha4emissor4rep1" numeric(19,2),
"linha4emissor4rep2" numeric(19,2),
"linha4emissor4rep3" numeric(19,2),
"observacao" varchar(2000) COLLATE "default",
"pressaofinallinha1" numeric(19,2),
"pressaofinallinha2" numeric(19,2),
"pressaofinallinha3" numeric(19,2),
"pressaofinallinha4" numeric(19,2),
"tempoteste" int4,
"responsavel_id" int8,
"setor_id" int8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for tubo
-- ----------------------------
DROP TABLE IF EXISTS "sai"."tubo";
CREATE TABLE "sai"."tubo" (
"id" int8 NOT NULL,
"version" int8,
"diametroexterno" numeric(19,2),
"diametrointerno" numeric(19,2),
"modelo" varchar(255) COLLATE "default",
"pressao" numeric(19,2),
"tipo" int4,
"vazao" numeric(19,2),
"fabricante_id" int8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for tubo_aud
-- ----------------------------
DROP TABLE IF EXISTS "sai"."tubo_aud";
CREATE TABLE "sai"."tubo_aud" (
"id" int8 NOT NULL,
"cd_revisao" int4 NOT NULL,
"tipo_revisao" int2,
"diametroexterno" numeric(19,2),
"diametrointerno" numeric(19,2),
"modelo" varchar(255) COLLATE "default",
"pressao" numeric(19,2),
"tipo" int4,
"vazao" numeric(19,2),
"fabricante_id" int8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for usuario
-- ----------------------------
DROP TABLE IF EXISTS "sai"."usuario";
CREATE TABLE "sai"."usuario" (
"id" int8 NOT NULL,
"version" int8,
"ativo" bool NOT NULL,
"email" varchar(255) COLLATE "default",
"hashcodigo" varchar(16) COLLATE "default",
"nome" varchar(255) COLLATE "default",
"password" varchar(255) COLLATE "default",
"senhaexpirada" bool NOT NULL,
"username" varchar(255) COLLATE "default",
"baciapadrao_id" int8,
"distritopadrao_id" int8,
"perfil_id" int8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for usuario_aud
-- ----------------------------
DROP TABLE IF EXISTS "sai"."usuario_aud";
CREATE TABLE "sai"."usuario_aud" (
"id" int8 NOT NULL,
"cd_revisao" int4 NOT NULL,
"tipo_revisao" int2,
"ativo" bool,
"email" varchar(255) COLLATE "default",
"nome" varchar(255) COLLATE "default",
"password" varchar(255) COLLATE "default",
"senhaexpirada" bool,
"username" varchar(255) COLLATE "default",
"baciapadrao_id" int8,
"distritopadrao_id" int8,
"perfil_id" int8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for usuario_baciahidrografica
-- ----------------------------
DROP TABLE IF EXISTS "sai"."usuario_baciahidrografica";
CREATE TABLE "sai"."usuario_baciahidrografica" (
"id_usuario" int8 NOT NULL,
"id_baciahidrografica" int8 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for usuario_distrito
-- ----------------------------
DROP TABLE IF EXISTS "sai"."usuario_distrito";
CREATE TABLE "sai"."usuario_distrito" (
"id_usuario" int8 NOT NULL,
"id_distrito" int8 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for usuario_modulo
-- ----------------------------
DROP TABLE IF EXISTS "sai"."usuario_modulo";
CREATE TABLE "sai"."usuario_modulo" (
"id_usuario" int8 NOT NULL,
"id_modulo" int8 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for valvula
-- ----------------------------
DROP TABLE IF EXISTS "sai"."valvula";
CREATE TABLE "sai"."valvula" (
"id" int8 NOT NULL,
"version" int8,
"modelo" varchar(255) COLLATE "default",
"pressao" numeric(19,2),
"tipo" int4,
"vazao" numeric(19,2),
"fabricante_id" int8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for valvula_aud
-- ----------------------------
DROP TABLE IF EXISTS "sai"."valvula_aud";
CREATE TABLE "sai"."valvula_aud" (
"id" int8 NOT NULL,
"cd_revisao" int4 NOT NULL,
"tipo_revisao" int2,
"modelo" varchar(255) COLLATE "default",
"pressao" numeric(19,2),
"tipo" int4,
"vazao" numeric(19,2),
"fabricante_id" int8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for variedade
-- ----------------------------
DROP TABLE IF EXISTS "sai"."variedade";
CREATE TABLE "sai"."variedade" (
"id" int8 NOT NULL,
"version" int8,
"nome" varchar(255) COLLATE "default",
"cultura_id" int8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for variedade_aud
-- ----------------------------
DROP TABLE IF EXISTS "sai"."variedade_aud";
CREATE TABLE "sai"."variedade_aud" (
"id" int8 NOT NULL,
"cd_revisao" int4 NOT NULL,
"tipo_revisao" int2,
"nome" varchar(255) COLLATE "default",
"cultura_id" int8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Alter Sequences Owned By 
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table acesso
-- ----------------------------
ALTER TABLE "sai"."acesso" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Uniques structure for table adubo
-- ----------------------------
ALTER TABLE "sai"."adubo" ADD UNIQUE ("nome");

-- ----------------------------
-- Primary Key structure for table adubo
-- ----------------------------
ALTER TABLE "sai"."adubo" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table adubo_aud
-- ----------------------------
ALTER TABLE "sai"."adubo_aud" ADD PRIMARY KEY ("id", "cd_revisao");

-- ----------------------------
-- Primary Key structure for table agua
-- ----------------------------
ALTER TABLE "sai"."agua" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table agua_aud
-- ----------------------------
ALTER TABLE "sai"."agua_aud" ADD PRIMARY KEY ("id", "cd_revisao");

-- ----------------------------
-- Primary Key structure for table arduinoin
-- ----------------------------
ALTER TABLE "sai"."arduinoin" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table arduinoin_aud
-- ----------------------------
ALTER TABLE "sai"."arduinoin_aud" ADD PRIMARY KEY ("id", "cd_revisao");

-- ----------------------------
-- Primary Key structure for table area
-- ----------------------------
ALTER TABLE "sai"."area" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table area_aud
-- ----------------------------
ALTER TABLE "sai"."area_aud" ADD PRIMARY KEY ("id", "cd_revisao");

-- ----------------------------
-- Primary Key structure for table atividade
-- ----------------------------
ALTER TABLE "sai"."atividade" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table atividade_aud
-- ----------------------------
ALTER TABLE "sai"."atividade_aud" ADD PRIMARY KEY ("id", "cd_revisao");

-- ----------------------------
-- Primary Key structure for table atividadeeconomica
-- ----------------------------
ALTER TABLE "sai"."atividadeeconomica" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table atividadeeconomica_aud
-- ----------------------------
ALTER TABLE "sai"."atividadeeconomica_aud" ADD PRIMARY KEY ("id", "cd_revisao");

-- ----------------------------
-- Primary Key structure for table baciahidrografica
-- ----------------------------
ALTER TABLE "sai"."baciahidrografica" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table baciahidrografica_aud
-- ----------------------------
ALTER TABLE "sai"."baciahidrografica_aud" ADD PRIMARY KEY ("id", "cd_revisao");

-- ----------------------------
-- Uniques structure for table bomba
-- ----------------------------
ALTER TABLE "sai"."bomba" ADD UNIQUE ("modelo");

-- ----------------------------
-- Primary Key structure for table bomba
-- ----------------------------
ALTER TABLE "sai"."bomba" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table bomba_aud
-- ----------------------------
ALTER TABLE "sai"."bomba_aud" ADD PRIMARY KEY ("id", "cd_revisao");

-- ----------------------------
-- Primary Key structure for table categoria
-- ----------------------------
ALTER TABLE "sai"."categoria" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table cliente
-- ----------------------------
ALTER TABLE "sai"."cliente" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table clima
-- ----------------------------
ALTER TABLE "sai"."clima" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table clima_aud
-- ----------------------------
ALTER TABLE "sai"."clima_aud" ADD PRIMARY KEY ("id", "cd_revisao");

-- ----------------------------
-- Primary Key structure for table colheita
-- ----------------------------
ALTER TABLE "sai"."colheita" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table colheita_aud
-- ----------------------------
ALTER TABLE "sai"."colheita_aud" ADD PRIMARY KEY ("id", "cd_revisao");

-- ----------------------------
-- Primary Key structure for table conta
-- ----------------------------
ALTER TABLE "sai"."conta" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table coordenada
-- ----------------------------
ALTER TABLE "sai"."coordenada" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table coordenada_aud
-- ----------------------------
ALTER TABLE "sai"."coordenada_aud" ADD PRIMARY KEY ("id", "cd_revisao");

-- ----------------------------
-- Primary Key structure for table cultivo
-- ----------------------------
ALTER TABLE "sai"."cultivo" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table cultivo_aud
-- ----------------------------
ALTER TABLE "sai"."cultivo_aud" ADD PRIMARY KEY ("id", "cd_revisao");

-- ----------------------------
-- Uniques structure for table cultura
-- ----------------------------
ALTER TABLE "sai"."cultura" ADD UNIQUE ("nome");

-- ----------------------------
-- Primary Key structure for table cultura
-- ----------------------------
ALTER TABLE "sai"."cultura" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table cultura_aud
-- ----------------------------
ALTER TABLE "sai"."cultura_aud" ADD PRIMARY KEY ("id", "cd_revisao");

-- ----------------------------
-- Uniques structure for table defensivo
-- ----------------------------
ALTER TABLE "sai"."defensivo" ADD UNIQUE ("nome");

-- ----------------------------
-- Primary Key structure for table defensivo
-- ----------------------------
ALTER TABLE "sai"."defensivo" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table defensivo_aud
-- ----------------------------
ALTER TABLE "sai"."defensivo_aud" ADD PRIMARY KEY ("id", "cd_revisao");

-- ----------------------------
-- Primary Key structure for table demandahidrica
-- ----------------------------
ALTER TABLE "sai"."demandahidrica" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table distrito
-- ----------------------------
ALTER TABLE "sai"."distrito" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table distrito_aud
-- ----------------------------
ALTER TABLE "sai"."distrito_aud" ADD PRIMARY KEY ("id", "cd_revisao");

-- ----------------------------
-- Uniques structure for table doenca
-- ----------------------------
ALTER TABLE "sai"."doenca" ADD UNIQUE ("nome");

-- ----------------------------
-- Primary Key structure for table doenca
-- ----------------------------
ALTER TABLE "sai"."doenca" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table doenca_aud
-- ----------------------------
ALTER TABLE "sai"."doenca_aud" ADD PRIMARY KEY ("id", "cd_revisao");

-- ----------------------------
-- Uniques structure for table elemento
-- ----------------------------
ALTER TABLE "sai"."elemento" ADD UNIQUE ("nome");

-- ----------------------------
-- Primary Key structure for table elemento
-- ----------------------------
ALTER TABLE "sai"."elemento" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table elemento_aud
-- ----------------------------
ALTER TABLE "sai"."elemento_aud" ADD PRIMARY KEY ("id", "cd_revisao");

-- ----------------------------
-- Uniques structure for table emissor
-- ----------------------------
ALTER TABLE "sai"."emissor" ADD UNIQUE ("modelo");

-- ----------------------------
-- Primary Key structure for table emissor
-- ----------------------------
ALTER TABLE "sai"."emissor" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table emissor_aud
-- ----------------------------
ALTER TABLE "sai"."emissor_aud" ADD PRIMARY KEY ("id", "cd_revisao");

-- ----------------------------
-- Primary Key structure for table endereco
-- ----------------------------
ALTER TABLE "sai"."endereco" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table energia
-- ----------------------------
ALTER TABLE "sai"."energia" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table energia_aud
-- ----------------------------
ALTER TABLE "sai"."energia_aud" ADD PRIMARY KEY ("id", "cd_revisao");

-- ----------------------------
-- Primary Key structure for table estacao
-- ----------------------------
ALTER TABLE "sai"."estacao" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table estacao_area_aud
-- ----------------------------
ALTER TABLE "sai"."estacao_area_aud" ADD PRIMARY KEY ("cd_revisao", "id_estacao", "id_area");

-- ----------------------------
-- Primary Key structure for table estacao_aud
-- ----------------------------
ALTER TABLE "sai"."estacao_aud" ADD PRIMARY KEY ("id", "cd_revisao");

-- ----------------------------
-- Primary Key structure for table estadio
-- ----------------------------
ALTER TABLE "sai"."estadio" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table estadio_aud
-- ----------------------------
ALTER TABLE "sai"."estadio_aud" ADD PRIMARY KEY ("id", "cd_revisao");

-- ----------------------------
-- Uniques structure for table fabricante
-- ----------------------------
ALTER TABLE "sai"."fabricante" ADD UNIQUE ("nome");

-- ----------------------------
-- Primary Key structure for table fabricante
-- ----------------------------
ALTER TABLE "sai"."fabricante" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table fabricante_aud
-- ----------------------------
ALTER TABLE "sai"."fabricante_aud" ADD PRIMARY KEY ("id", "cd_revisao");

-- ----------------------------
-- Primary Key structure for table fertirrigacaoaplicada
-- ----------------------------
ALTER TABLE "sai"."fertirrigacaoaplicada" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table fertirrigacaoaplicada_aud
-- ----------------------------
ALTER TABLE "sai"."fertirrigacaoaplicada_aud" ADD PRIMARY KEY ("id", "cd_revisao");

-- ----------------------------
-- Uniques structure for table fertirrigacaoaplicada_nutriente
-- ----------------------------
ALTER TABLE "sai"."fertirrigacaoaplicada_nutriente" ADD UNIQUE ("nutrientes_id");

-- ----------------------------
-- Primary Key structure for table fertirrigacaoaplicada_nutriente_aud
-- ----------------------------
ALTER TABLE "sai"."fertirrigacaoaplicada_nutriente_aud" ADD PRIMARY KEY ("cd_revisao", "fertirrigacaoaplicada_id", "nutrientes_id");

-- ----------------------------
-- Uniques structure for table filtro
-- ----------------------------
ALTER TABLE "sai"."filtro" ADD UNIQUE ("modelo");

-- ----------------------------
-- Primary Key structure for table filtro
-- ----------------------------
ALTER TABLE "sai"."filtro" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table filtro_aud
-- ----------------------------
ALTER TABLE "sai"."filtro_aud" ADD PRIMARY KEY ("id", "cd_revisao");

-- ----------------------------
-- Primary Key structure for table fornecedor
-- ----------------------------
ALTER TABLE "sai"."fornecedor" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Uniques structure for table funcionalidade
-- ----------------------------
ALTER TABLE "sai"."funcionalidade" ADD UNIQUE ("mnemonico");

-- ----------------------------
-- Primary Key structure for table funcionalidade
-- ----------------------------
ALTER TABLE "sai"."funcionalidade" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table funcionalidade_aud
-- ----------------------------
ALTER TABLE "sai"."funcionalidade_aud" ADD PRIMARY KEY ("id", "cd_revisao");

-- ----------------------------
-- Primary Key structure for table funcionalidade_perfil_aud
-- ----------------------------
ALTER TABLE "sai"."funcionalidade_perfil_aud" ADD PRIMARY KEY ("cd_revisao", "id_perfil", "id_funcionalidade");

-- ----------------------------
-- Primary Key structure for table indicadores
-- ----------------------------
ALTER TABLE "sai"."indicadores" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table indicadores_aud
-- ----------------------------
ALTER TABLE "sai"."indicadores_aud" ADD PRIMARY KEY ("id", "cd_revisao");

-- ----------------------------
-- Primary Key structure for table irrigacao
-- ----------------------------
ALTER TABLE "sai"."irrigacao" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table irrigacao_aud
-- ----------------------------
ALTER TABLE "sai"."irrigacao_aud" ADD PRIMARY KEY ("id", "cd_revisao");

-- ----------------------------
-- Primary Key structure for table irrigante
-- ----------------------------
ALTER TABLE "sai"."irrigante" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table irrigante_aud
-- ----------------------------
ALTER TABLE "sai"."irrigante_aud" ADD PRIMARY KEY ("id", "cd_revisao");

-- ----------------------------
-- Primary Key structure for table irrigante_bomba_aud
-- ----------------------------
ALTER TABLE "sai"."irrigante_bomba_aud" ADD PRIMARY KEY ("cd_revisao", "irrigante_id", "bombas_id");

-- ----------------------------
-- Primary Key structure for table irrigante_filtro_aud
-- ----------------------------
ALTER TABLE "sai"."irrigante_filtro_aud" ADD PRIMARY KEY ("cd_revisao", "irrigante_id", "filtros_id");

-- ----------------------------
-- Primary Key structure for table irrigante_tubo_aud
-- ----------------------------
ALTER TABLE "sai"."irrigante_tubo_aud" ADD PRIMARY KEY ("cd_revisao", "irrigante_id", "tubos_id");

-- ----------------------------
-- Primary Key structure for table irrigante_valvula_aud
-- ----------------------------
ALTER TABLE "sai"."irrigante_valvula_aud" ADD PRIMARY KEY ("cd_revisao", "irrigante_id", "valvulas_id");

-- ----------------------------
-- Primary Key structure for table mensagem
-- ----------------------------
ALTER TABLE "sai"."mensagem" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table mensagem_aud
-- ----------------------------
ALTER TABLE "sai"."mensagem_aud" ADD PRIMARY KEY ("id", "cd_revisao");

-- ----------------------------
-- Primary Key structure for table mensagem_irrigante_aud
-- ----------------------------
ALTER TABLE "sai"."mensagem_irrigante_aud" ADD PRIMARY KEY ("cd_revisao", "mensagem_id", "destinatarios_id");

-- ----------------------------
-- Uniques structure for table modulo
-- ----------------------------
ALTER TABLE "sai"."modulo" ADD UNIQUE ("siglas");

-- ----------------------------
-- Primary Key structure for table modulo
-- ----------------------------
ALTER TABLE "sai"."modulo" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table municipio
-- ----------------------------
ALTER TABLE "sai"."municipio" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table municipio_aud
-- ----------------------------
ALTER TABLE "sai"."municipio_aud" ADD PRIMARY KEY ("id", "cd_revisao");

-- ----------------------------
-- Uniques structure for table nutriente
-- ----------------------------
ALTER TABLE "sai"."nutriente" ADD UNIQUE ("nome");

-- ----------------------------
-- Primary Key structure for table nutriente
-- ----------------------------
ALTER TABLE "sai"."nutriente" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table nutriente_aud
-- ----------------------------
ALTER TABLE "sai"."nutriente_aud" ADD PRIMARY KEY ("id", "cd_revisao");

-- ----------------------------
-- Primary Key structure for table parametro
-- ----------------------------
ALTER TABLE "sai"."parametro" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table parametro_aud
-- ----------------------------
ALTER TABLE "sai"."parametro_aud" ADD PRIMARY KEY ("id", "cd_revisao");

-- ----------------------------
-- Primary Key structure for table perfil
-- ----------------------------
ALTER TABLE "sai"."perfil" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table perfil_aud
-- ----------------------------
ALTER TABLE "sai"."perfil_aud" ADD PRIMARY KEY ("id", "cd_revisao");

-- ----------------------------
-- Primary Key structure for table plantio
-- ----------------------------
ALTER TABLE "sai"."plantio" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Uniques structure for table plantio_adubo
-- ----------------------------
ALTER TABLE "sai"."plantio_adubo" ADD UNIQUE ("adubos_id");

-- ----------------------------
-- Primary Key structure for table plantio_adubo_aud
-- ----------------------------
ALTER TABLE "sai"."plantio_adubo_aud" ADD PRIMARY KEY ("cd_revisao", "plantio_id", "adubos_id");

-- ----------------------------
-- Primary Key structure for table plantio_aud
-- ----------------------------
ALTER TABLE "sai"."plantio_aud" ADD PRIMARY KEY ("id", "cd_revisao");

-- ----------------------------
-- Uniques structure for table plantio_colheita
-- ----------------------------
ALTER TABLE "sai"."plantio_colheita" ADD UNIQUE ("colheitas_id");

-- ----------------------------
-- Primary Key structure for table plantio_colheita_aud
-- ----------------------------
ALTER TABLE "sai"."plantio_colheita_aud" ADD PRIMARY KEY ("cd_revisao", "plantio_id", "colheitas_id");

-- ----------------------------
-- Uniques structure for table plantio_defensivo
-- ----------------------------
ALTER TABLE "sai"."plantio_defensivo" ADD UNIQUE ("defensivos_id");

-- ----------------------------
-- Primary Key structure for table plantio_defensivo_aud
-- ----------------------------
ALTER TABLE "sai"."plantio_defensivo_aud" ADD PRIMARY KEY ("cd_revisao", "plantio_id", "defensivos_id");

-- ----------------------------
-- Uniques structure for table plantio_doenca
-- ----------------------------
ALTER TABLE "sai"."plantio_doenca" ADD UNIQUE ("doencas_id");

-- ----------------------------
-- Primary Key structure for table plantio_doenca_aud
-- ----------------------------
ALTER TABLE "sai"."plantio_doenca_aud" ADD PRIMARY KEY ("cd_revisao", "plantio_id", "doencas_id");

-- ----------------------------
-- Uniques structure for table plantio_emissor
-- ----------------------------
ALTER TABLE "sai"."plantio_emissor" ADD UNIQUE ("emissores_id");

-- ----------------------------
-- Primary Key structure for table plantio_emissor_aud
-- ----------------------------
ALTER TABLE "sai"."plantio_emissor_aud" ADD PRIMARY KEY ("cd_revisao", "plantio_id", "emissores_id");

-- ----------------------------
-- Uniques structure for table plantio_praga
-- ----------------------------
ALTER TABLE "sai"."plantio_praga" ADD UNIQUE ("pragas_id");

-- ----------------------------
-- Primary Key structure for table plantio_praga_aud
-- ----------------------------
ALTER TABLE "sai"."plantio_praga_aud" ADD PRIMARY KEY ("cd_revisao", "plantio_id", "pragas_id");

-- ----------------------------
-- Uniques structure for table praga
-- ----------------------------
ALTER TABLE "sai"."praga" ADD UNIQUE ("nome");

-- ----------------------------
-- Primary Key structure for table praga
-- ----------------------------
ALTER TABLE "sai"."praga" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table praga_aud
-- ----------------------------
ALTER TABLE "sai"."praga_aud" ADD PRIMARY KEY ("id", "cd_revisao");

-- ----------------------------
-- Primary Key structure for table referencia
-- ----------------------------
ALTER TABLE "sai"."referencia" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table referencia_aud
-- ----------------------------
ALTER TABLE "sai"."referencia_aud" ADD PRIMARY KEY ("id", "cd_revisao");

-- ----------------------------
-- Primary Key structure for table revisao
-- ----------------------------
ALTER TABLE "sai"."revisao" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table setor
-- ----------------------------
ALTER TABLE "sai"."setor" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table setor_aud
-- ----------------------------
ALTER TABLE "sai"."setor_aud" ADD PRIMARY KEY ("id", "cd_revisao");

-- ----------------------------
-- Uniques structure for table setor_emissor
-- ----------------------------
ALTER TABLE "sai"."setor_emissor" ADD UNIQUE ("emissores_id");

-- ----------------------------
-- Primary Key structure for table setor_emissor_aud
-- ----------------------------
ALTER TABLE "sai"."setor_emissor_aud" ADD PRIMARY KEY ("cd_revisao", "setor_id", "emissores_id");

-- ----------------------------
-- Primary Key structure for table solo
-- ----------------------------
ALTER TABLE "sai"."solo" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table solo_aud
-- ----------------------------
ALTER TABLE "sai"."solo_aud" ADD PRIMARY KEY ("id", "cd_revisao");

-- ----------------------------
-- Primary Key structure for table solucao
-- ----------------------------
ALTER TABLE "sai"."solucao" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table solucao_aud
-- ----------------------------
ALTER TABLE "sai"."solucao_aud" ADD PRIMARY KEY ("id", "cd_revisao");

-- ----------------------------
-- Uniques structure for table solucao_nutriente
-- ----------------------------
ALTER TABLE "sai"."solucao_nutriente" ADD UNIQUE ("nutrientes_id");

-- ----------------------------
-- Primary Key structure for table solucao_nutriente_aud
-- ----------------------------
ALTER TABLE "sai"."solucao_nutriente_aud" ADD PRIMARY KEY ("cd_revisao", "solucao_id", "nutrientes_id");

-- ----------------------------
-- Uniques structure for table tanque
-- ----------------------------
ALTER TABLE "sai"."tanque" ADD UNIQUE ("nome");

-- ----------------------------
-- Primary Key structure for table tanque
-- ----------------------------
ALTER TABLE "sai"."tanque" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table tanque_aud
-- ----------------------------
ALTER TABLE "sai"."tanque_aud" ADD PRIMARY KEY ("id", "cd_revisao");

-- ----------------------------
-- Primary Key structure for table tecnico
-- ----------------------------
ALTER TABLE "sai"."tecnico" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table tecnico_aud
-- ----------------------------
ALTER TABLE "sai"."tecnico_aud" ADD PRIMARY KEY ("id", "cd_revisao");

-- ----------------------------
-- Primary Key structure for table testeeficiencia
-- ----------------------------
ALTER TABLE "sai"."testeeficiencia" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table testeeficiencia_aud
-- ----------------------------
ALTER TABLE "sai"."testeeficiencia_aud" ADD PRIMARY KEY ("id", "cd_revisao");

-- ----------------------------
-- Uniques structure for table tubo
-- ----------------------------
ALTER TABLE "sai"."tubo" ADD UNIQUE ("modelo");

-- ----------------------------
-- Primary Key structure for table tubo
-- ----------------------------
ALTER TABLE "sai"."tubo" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table tubo_aud
-- ----------------------------
ALTER TABLE "sai"."tubo_aud" ADD PRIMARY KEY ("id", "cd_revisao");

-- ----------------------------
-- Uniques structure for table usuario
-- ----------------------------
ALTER TABLE "sai"."usuario" ADD UNIQUE ("username");

-- ----------------------------
-- Primary Key structure for table usuario
-- ----------------------------
ALTER TABLE "sai"."usuario" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table usuario_aud
-- ----------------------------
ALTER TABLE "sai"."usuario_aud" ADD PRIMARY KEY ("id", "cd_revisao");

-- ----------------------------
-- Uniques structure for table valvula
-- ----------------------------
ALTER TABLE "sai"."valvula" ADD UNIQUE ("modelo");

-- ----------------------------
-- Primary Key structure for table valvula
-- ----------------------------
ALTER TABLE "sai"."valvula" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table valvula_aud
-- ----------------------------
ALTER TABLE "sai"."valvula_aud" ADD PRIMARY KEY ("id", "cd_revisao");

-- ----------------------------
-- Uniques structure for table variedade
-- ----------------------------
ALTER TABLE "sai"."variedade" ADD UNIQUE ("nome");

-- ----------------------------
-- Primary Key structure for table variedade
-- ----------------------------
ALTER TABLE "sai"."variedade" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table variedade_aud
-- ----------------------------
ALTER TABLE "sai"."variedade_aud" ADD PRIMARY KEY ("id", "cd_revisao");

-- ----------------------------
-- Foreign Key structure for table "sai"."acesso"
-- ----------------------------
ALTER TABLE "sai"."acesso" ADD FOREIGN KEY ("usuario_id") REFERENCES "sai"."usuario" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."adubo_aud"
-- ----------------------------
ALTER TABLE "sai"."adubo_aud" ADD FOREIGN KEY ("cd_revisao") REFERENCES "sai"."revisao" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."agua"
-- ----------------------------
ALTER TABLE "sai"."agua" ADD FOREIGN KEY ("irrigante_id") REFERENCES "sai"."irrigante" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."agua_aud"
-- ----------------------------
ALTER TABLE "sai"."agua_aud" ADD FOREIGN KEY ("cd_revisao") REFERENCES "sai"."revisao" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."arduinoin"
-- ----------------------------
ALTER TABLE "sai"."arduinoin" ADD FOREIGN KEY ("irrigante_id") REFERENCES "sai"."irrigante" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."arduinoin_aud"
-- ----------------------------
ALTER TABLE "sai"."arduinoin_aud" ADD FOREIGN KEY ("cd_revisao") REFERENCES "sai"."revisao" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."area"
-- ----------------------------
ALTER TABLE "sai"."area" ADD FOREIGN KEY ("distrito_id") REFERENCES "sai"."distrito" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."area_aud"
-- ----------------------------
ALTER TABLE "sai"."area_aud" ADD FOREIGN KEY ("cd_revisao") REFERENCES "sai"."revisao" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."atividade_aud"
-- ----------------------------
ALTER TABLE "sai"."atividade_aud" ADD FOREIGN KEY ("cd_revisao") REFERENCES "sai"."revisao" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."atividadeeconomica"
-- ----------------------------
ALTER TABLE "sai"."atividadeeconomica" ADD FOREIGN KEY ("cultura_id") REFERENCES "sai"."cultura" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "sai"."atividadeeconomica" ADD FOREIGN KEY ("municipio_id") REFERENCES "sai"."municipio" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "sai"."atividadeeconomica" ADD FOREIGN KEY ("referencia_id") REFERENCES "sai"."referencia" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "sai"."atividadeeconomica" ADD FOREIGN KEY ("variedade_id") REFERENCES "sai"."variedade" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "sai"."atividadeeconomica" ADD FOREIGN KEY ("bacia_id") REFERENCES "sai"."baciahidrografica" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."atividadeeconomica_aud"
-- ----------------------------
ALTER TABLE "sai"."atividadeeconomica_aud" ADD FOREIGN KEY ("cd_revisao") REFERENCES "sai"."revisao" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."baciahidrografica_aud"
-- ----------------------------
ALTER TABLE "sai"."baciahidrografica_aud" ADD FOREIGN KEY ("cd_revisao") REFERENCES "sai"."revisao" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."bomba"
-- ----------------------------
ALTER TABLE "sai"."bomba" ADD FOREIGN KEY ("fabricante_id") REFERENCES "sai"."fabricante" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."bomba_aud"
-- ----------------------------
ALTER TABLE "sai"."bomba_aud" ADD FOREIGN KEY ("cd_revisao") REFERENCES "sai"."revisao" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."cliente"
-- ----------------------------
ALTER TABLE "sai"."cliente" ADD FOREIGN KEY ("endereco_id") REFERENCES "sai"."endereco" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."clima"
-- ----------------------------
ALTER TABLE "sai"."clima" ADD FOREIGN KEY ("area_id") REFERENCES "sai"."area" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "sai"."clima" ADD FOREIGN KEY ("estacao_id") REFERENCES "sai"."estacao" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."clima_aud"
-- ----------------------------
ALTER TABLE "sai"."clima_aud" ADD FOREIGN KEY ("cd_revisao") REFERENCES "sai"."revisao" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."colheita"
-- ----------------------------
ALTER TABLE "sai"."colheita" ADD FOREIGN KEY ("plantio_id") REFERENCES "sai"."plantio" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."colheita_aud"
-- ----------------------------
ALTER TABLE "sai"."colheita_aud" ADD FOREIGN KEY ("cd_revisao") REFERENCES "sai"."revisao" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."coordenada"
-- ----------------------------
ALTER TABLE "sai"."coordenada" ADD FOREIGN KEY ("irrigante_id") REFERENCES "sai"."irrigante" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "sai"."coordenada" ADD FOREIGN KEY ("municipio_id") REFERENCES "sai"."municipio" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."coordenada_aud"
-- ----------------------------
ALTER TABLE "sai"."coordenada_aud" ADD FOREIGN KEY ("cd_revisao") REFERENCES "sai"."revisao" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."cultivo"
-- ----------------------------
ALTER TABLE "sai"."cultivo" ADD FOREIGN KEY ("variedade_id") REFERENCES "sai"."variedade" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "sai"."cultivo" ADD FOREIGN KEY ("irrigante_id") REFERENCES "sai"."irrigante" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "sai"."cultivo" ADD FOREIGN KEY ("cultura_id") REFERENCES "sai"."cultura" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "sai"."cultivo" ADD FOREIGN KEY ("referencia_id") REFERENCES "sai"."referencia" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."cultivo_aud"
-- ----------------------------
ALTER TABLE "sai"."cultivo_aud" ADD FOREIGN KEY ("cd_revisao") REFERENCES "sai"."revisao" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."cultura"
-- ----------------------------
ALTER TABLE "sai"."cultura" ADD FOREIGN KEY ("atividade_id") REFERENCES "sai"."atividade" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."cultura_aud"
-- ----------------------------
ALTER TABLE "sai"."cultura_aud" ADD FOREIGN KEY ("cd_revisao") REFERENCES "sai"."revisao" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."defensivo_aud"
-- ----------------------------
ALTER TABLE "sai"."defensivo_aud" ADD FOREIGN KEY ("cd_revisao") REFERENCES "sai"."revisao" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."demandahidrica"
-- ----------------------------
ALTER TABLE "sai"."demandahidrica" ADD FOREIGN KEY ("plantio_id") REFERENCES "sai"."plantio" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "sai"."demandahidrica" ADD FOREIGN KEY ("irrigante_id") REFERENCES "sai"."irrigante" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "sai"."demandahidrica" ADD FOREIGN KEY ("clima_id") REFERENCES "sai"."clima" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."distrito_aud"
-- ----------------------------
ALTER TABLE "sai"."distrito_aud" ADD FOREIGN KEY ("cd_revisao") REFERENCES "sai"."revisao" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."doenca_aud"
-- ----------------------------
ALTER TABLE "sai"."doenca_aud" ADD FOREIGN KEY ("cd_revisao") REFERENCES "sai"."revisao" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."elemento_aud"
-- ----------------------------
ALTER TABLE "sai"."elemento_aud" ADD FOREIGN KEY ("cd_revisao") REFERENCES "sai"."revisao" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."emissor"
-- ----------------------------
ALTER TABLE "sai"."emissor" ADD FOREIGN KEY ("fabricante_id") REFERENCES "sai"."fabricante" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."emissor_aud"
-- ----------------------------
ALTER TABLE "sai"."emissor_aud" ADD FOREIGN KEY ("cd_revisao") REFERENCES "sai"."revisao" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."energia"
-- ----------------------------
ALTER TABLE "sai"."energia" ADD FOREIGN KEY ("irrigante_id") REFERENCES "sai"."irrigante" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."energia_aud"
-- ----------------------------
ALTER TABLE "sai"."energia_aud" ADD FOREIGN KEY ("cd_revisao") REFERENCES "sai"."revisao" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."estacao"
-- ----------------------------
ALTER TABLE "sai"."estacao" ADD FOREIGN KEY ("distrito_id") REFERENCES "sai"."distrito" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."estacao_area"
-- ----------------------------
ALTER TABLE "sai"."estacao_area" ADD FOREIGN KEY ("id_area") REFERENCES "sai"."area" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "sai"."estacao_area" ADD FOREIGN KEY ("id_estacao") REFERENCES "sai"."estacao" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."estacao_area_aud"
-- ----------------------------
ALTER TABLE "sai"."estacao_area_aud" ADD FOREIGN KEY ("cd_revisao") REFERENCES "sai"."revisao" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."estacao_aud"
-- ----------------------------
ALTER TABLE "sai"."estacao_aud" ADD FOREIGN KEY ("cd_revisao") REFERENCES "sai"."revisao" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."estadio"
-- ----------------------------
ALTER TABLE "sai"."estadio" ADD FOREIGN KEY ("referencia_id") REFERENCES "sai"."referencia" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."estadio_aud"
-- ----------------------------
ALTER TABLE "sai"."estadio_aud" ADD FOREIGN KEY ("cd_revisao") REFERENCES "sai"."revisao" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."fabricante_aud"
-- ----------------------------
ALTER TABLE "sai"."fabricante_aud" ADD FOREIGN KEY ("cd_revisao") REFERENCES "sai"."revisao" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."fertirrigacaoaplicada"
-- ----------------------------
ALTER TABLE "sai"."fertirrigacaoaplicada" ADD FOREIGN KEY ("plantio_id") REFERENCES "sai"."plantio" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "sai"."fertirrigacaoaplicada" ADD FOREIGN KEY ("tanque_id") REFERENCES "sai"."tanque" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."fertirrigacaoaplicada_aud"
-- ----------------------------
ALTER TABLE "sai"."fertirrigacaoaplicada_aud" ADD FOREIGN KEY ("cd_revisao") REFERENCES "sai"."revisao" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."fertirrigacaoaplicada_nutriente"
-- ----------------------------
ALTER TABLE "sai"."fertirrigacaoaplicada_nutriente" ADD FOREIGN KEY ("nutrientes_id") REFERENCES "sai"."nutriente" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "sai"."fertirrigacaoaplicada_nutriente" ADD FOREIGN KEY ("fertirrigacaoaplicada_id") REFERENCES "sai"."fertirrigacaoaplicada" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."fertirrigacaoaplicada_nutriente_aud"
-- ----------------------------
ALTER TABLE "sai"."fertirrigacaoaplicada_nutriente_aud" ADD FOREIGN KEY ("cd_revisao") REFERENCES "sai"."revisao" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."filtro"
-- ----------------------------
ALTER TABLE "sai"."filtro" ADD FOREIGN KEY ("fabricante_id") REFERENCES "sai"."fabricante" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."filtro_aud"
-- ----------------------------
ALTER TABLE "sai"."filtro_aud" ADD FOREIGN KEY ("cd_revisao") REFERENCES "sai"."revisao" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."fornecedor"
-- ----------------------------
ALTER TABLE "sai"."fornecedor" ADD FOREIGN KEY ("endereco_id") REFERENCES "sai"."endereco" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."funcionalidade_aud"
-- ----------------------------
ALTER TABLE "sai"."funcionalidade_aud" ADD FOREIGN KEY ("cd_revisao") REFERENCES "sai"."revisao" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."funcionalidade_perfil"
-- ----------------------------
ALTER TABLE "sai"."funcionalidade_perfil" ADD FOREIGN KEY ("id_funcionalidade") REFERENCES "sai"."funcionalidade" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "sai"."funcionalidade_perfil" ADD FOREIGN KEY ("id_perfil") REFERENCES "sai"."perfil" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."funcionalidade_perfil_aud"
-- ----------------------------
ALTER TABLE "sai"."funcionalidade_perfil_aud" ADD FOREIGN KEY ("cd_revisao") REFERENCES "sai"."revisao" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."indicadores"
-- ----------------------------
ALTER TABLE "sai"."indicadores" ADD FOREIGN KEY ("atividadeeconomica_id") REFERENCES "sai"."atividadeeconomica" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."indicadores_aud"
-- ----------------------------
ALTER TABLE "sai"."indicadores_aud" ADD FOREIGN KEY ("cd_revisao") REFERENCES "sai"."revisao" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."irrigacao"
-- ----------------------------
ALTER TABLE "sai"."irrigacao" ADD FOREIGN KEY ("plantio_id") REFERENCES "sai"."plantio" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."irrigacao_aud"
-- ----------------------------
ALTER TABLE "sai"."irrigacao_aud" ADD FOREIGN KEY ("cd_revisao") REFERENCES "sai"."revisao" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."irrigante"
-- ----------------------------
ALTER TABLE "sai"."irrigante" ADD FOREIGN KEY ("usuario_id") REFERENCES "sai"."usuario" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "sai"."irrigante" ADD FOREIGN KEY ("area_id") REFERENCES "sai"."area" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."irrigante_aud"
-- ----------------------------
ALTER TABLE "sai"."irrigante_aud" ADD FOREIGN KEY ("cd_revisao") REFERENCES "sai"."revisao" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."irrigante_bomba"
-- ----------------------------
ALTER TABLE "sai"."irrigante_bomba" ADD FOREIGN KEY ("bombas_id") REFERENCES "sai"."bomba" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "sai"."irrigante_bomba" ADD FOREIGN KEY ("irrigante_id") REFERENCES "sai"."irrigante" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."irrigante_bomba_aud"
-- ----------------------------
ALTER TABLE "sai"."irrigante_bomba_aud" ADD FOREIGN KEY ("cd_revisao") REFERENCES "sai"."revisao" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."irrigante_filtro"
-- ----------------------------
ALTER TABLE "sai"."irrigante_filtro" ADD FOREIGN KEY ("filtros_id") REFERENCES "sai"."filtro" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "sai"."irrigante_filtro" ADD FOREIGN KEY ("irrigante_id") REFERENCES "sai"."irrigante" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."irrigante_filtro_aud"
-- ----------------------------
ALTER TABLE "sai"."irrigante_filtro_aud" ADD FOREIGN KEY ("cd_revisao") REFERENCES "sai"."revisao" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."irrigante_tubo"
-- ----------------------------
ALTER TABLE "sai"."irrigante_tubo" ADD FOREIGN KEY ("tubos_id") REFERENCES "sai"."tubo" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "sai"."irrigante_tubo" ADD FOREIGN KEY ("irrigante_id") REFERENCES "sai"."irrigante" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."irrigante_tubo_aud"
-- ----------------------------
ALTER TABLE "sai"."irrigante_tubo_aud" ADD FOREIGN KEY ("cd_revisao") REFERENCES "sai"."revisao" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."irrigante_valvula"
-- ----------------------------
ALTER TABLE "sai"."irrigante_valvula" ADD FOREIGN KEY ("valvulas_id") REFERENCES "sai"."valvula" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "sai"."irrigante_valvula" ADD FOREIGN KEY ("irrigante_id") REFERENCES "sai"."irrigante" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."irrigante_valvula_aud"
-- ----------------------------
ALTER TABLE "sai"."irrigante_valvula_aud" ADD FOREIGN KEY ("cd_revisao") REFERENCES "sai"."revisao" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."mensagem"
-- ----------------------------
ALTER TABLE "sai"."mensagem" ADD FOREIGN KEY ("usuario_id") REFERENCES "sai"."usuario" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "sai"."mensagem" ADD FOREIGN KEY ("distrito_id") REFERENCES "sai"."distrito" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."mensagem_aud"
-- ----------------------------
ALTER TABLE "sai"."mensagem_aud" ADD FOREIGN KEY ("cd_revisao") REFERENCES "sai"."revisao" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."mensagem_irrigante"
-- ----------------------------
ALTER TABLE "sai"."mensagem_irrigante" ADD FOREIGN KEY ("mensagem_id") REFERENCES "sai"."mensagem" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "sai"."mensagem_irrigante" ADD FOREIGN KEY ("destinatarios_id") REFERENCES "sai"."irrigante" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."mensagem_irrigante_aud"
-- ----------------------------
ALTER TABLE "sai"."mensagem_irrigante_aud" ADD FOREIGN KEY ("cd_revisao") REFERENCES "sai"."revisao" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."municipio"
-- ----------------------------
ALTER TABLE "sai"."municipio" ADD FOREIGN KEY ("bacia_id") REFERENCES "sai"."baciahidrografica" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."municipio_aud"
-- ----------------------------
ALTER TABLE "sai"."municipio_aud" ADD FOREIGN KEY ("cd_revisao") REFERENCES "sai"."revisao" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."nutriente"
-- ----------------------------
ALTER TABLE "sai"."nutriente" ADD FOREIGN KEY ("elemento_id") REFERENCES "sai"."elemento" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."nutriente_aud"
-- ----------------------------
ALTER TABLE "sai"."nutriente_aud" ADD FOREIGN KEY ("cd_revisao") REFERENCES "sai"."revisao" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."parametro_aud"
-- ----------------------------
ALTER TABLE "sai"."parametro_aud" ADD FOREIGN KEY ("cd_revisao") REFERENCES "sai"."revisao" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."perfil_aud"
-- ----------------------------
ALTER TABLE "sai"."perfil_aud" ADD FOREIGN KEY ("cd_revisao") REFERENCES "sai"."revisao" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."plantio"
-- ----------------------------
ALTER TABLE "sai"."plantio" ADD FOREIGN KEY ("setor_id") REFERENCES "sai"."setor" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "sai"."plantio" ADD FOREIGN KEY ("cultivo_id") REFERENCES "sai"."cultivo" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."plantio_adubo"
-- ----------------------------
ALTER TABLE "sai"."plantio_adubo" ADD FOREIGN KEY ("plantio_id") REFERENCES "sai"."plantio" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "sai"."plantio_adubo" ADD FOREIGN KEY ("adubos_id") REFERENCES "sai"."adubo" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."plantio_adubo_aud"
-- ----------------------------
ALTER TABLE "sai"."plantio_adubo_aud" ADD FOREIGN KEY ("cd_revisao") REFERENCES "sai"."revisao" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."plantio_aud"
-- ----------------------------
ALTER TABLE "sai"."plantio_aud" ADD FOREIGN KEY ("cd_revisao") REFERENCES "sai"."revisao" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."plantio_colheita"
-- ----------------------------
ALTER TABLE "sai"."plantio_colheita" ADD FOREIGN KEY ("colheitas_id") REFERENCES "sai"."colheita" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "sai"."plantio_colheita" ADD FOREIGN KEY ("plantio_id") REFERENCES "sai"."plantio" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."plantio_colheita_aud"
-- ----------------------------
ALTER TABLE "sai"."plantio_colheita_aud" ADD FOREIGN KEY ("cd_revisao") REFERENCES "sai"."revisao" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."plantio_defensivo"
-- ----------------------------
ALTER TABLE "sai"."plantio_defensivo" ADD FOREIGN KEY ("plantio_id") REFERENCES "sai"."plantio" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "sai"."plantio_defensivo" ADD FOREIGN KEY ("defensivos_id") REFERENCES "sai"."defensivo" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."plantio_defensivo_aud"
-- ----------------------------
ALTER TABLE "sai"."plantio_defensivo_aud" ADD FOREIGN KEY ("cd_revisao") REFERENCES "sai"."revisao" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."plantio_doenca"
-- ----------------------------
ALTER TABLE "sai"."plantio_doenca" ADD FOREIGN KEY ("plantio_id") REFERENCES "sai"."plantio" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "sai"."plantio_doenca" ADD FOREIGN KEY ("doencas_id") REFERENCES "sai"."doenca" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."plantio_doenca_aud"
-- ----------------------------
ALTER TABLE "sai"."plantio_doenca_aud" ADD FOREIGN KEY ("cd_revisao") REFERENCES "sai"."revisao" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."plantio_emissor"
-- ----------------------------
ALTER TABLE "sai"."plantio_emissor" ADD FOREIGN KEY ("emissores_id") REFERENCES "sai"."emissor" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "sai"."plantio_emissor" ADD FOREIGN KEY ("plantio_id") REFERENCES "sai"."plantio" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."plantio_emissor_aud"
-- ----------------------------
ALTER TABLE "sai"."plantio_emissor_aud" ADD FOREIGN KEY ("cd_revisao") REFERENCES "sai"."revisao" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."plantio_praga"
-- ----------------------------
ALTER TABLE "sai"."plantio_praga" ADD FOREIGN KEY ("pragas_id") REFERENCES "sai"."praga" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "sai"."plantio_praga" ADD FOREIGN KEY ("plantio_id") REFERENCES "sai"."plantio" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."plantio_praga_aud"
-- ----------------------------
ALTER TABLE "sai"."plantio_praga_aud" ADD FOREIGN KEY ("cd_revisao") REFERENCES "sai"."revisao" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."praga_aud"
-- ----------------------------
ALTER TABLE "sai"."praga_aud" ADD FOREIGN KEY ("cd_revisao") REFERENCES "sai"."revisao" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."referencia"
-- ----------------------------
ALTER TABLE "sai"."referencia" ADD FOREIGN KEY ("variedade_id") REFERENCES "sai"."variedade" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."referencia_aud"
-- ----------------------------
ALTER TABLE "sai"."referencia_aud" ADD FOREIGN KEY ("cd_revisao") REFERENCES "sai"."revisao" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."revisao"
-- ----------------------------
ALTER TABLE "sai"."revisao" ADD FOREIGN KEY ("usuario_id") REFERENCES "sai"."usuario" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."setor"
-- ----------------------------
ALTER TABLE "sai"."setor" ADD FOREIGN KEY ("irrigante_id") REFERENCES "sai"."irrigante" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."setor_aud"
-- ----------------------------
ALTER TABLE "sai"."setor_aud" ADD FOREIGN KEY ("cd_revisao") REFERENCES "sai"."revisao" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."setor_emissor"
-- ----------------------------
ALTER TABLE "sai"."setor_emissor" ADD FOREIGN KEY ("setor_id") REFERENCES "sai"."setor" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "sai"."setor_emissor" ADD FOREIGN KEY ("emissores_id") REFERENCES "sai"."emissor" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."setor_emissor_aud"
-- ----------------------------
ALTER TABLE "sai"."setor_emissor_aud" ADD FOREIGN KEY ("cd_revisao") REFERENCES "sai"."revisao" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."solo_aud"
-- ----------------------------
ALTER TABLE "sai"."solo_aud" ADD FOREIGN KEY ("cd_revisao") REFERENCES "sai"."revisao" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."solucao_aud"
-- ----------------------------
ALTER TABLE "sai"."solucao_aud" ADD FOREIGN KEY ("cd_revisao") REFERENCES "sai"."revisao" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."solucao_nutriente"
-- ----------------------------
ALTER TABLE "sai"."solucao_nutriente" ADD FOREIGN KEY ("nutrientes_id") REFERENCES "sai"."nutriente" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "sai"."solucao_nutriente" ADD FOREIGN KEY ("solucao_id") REFERENCES "sai"."solucao" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."solucao_nutriente_aud"
-- ----------------------------
ALTER TABLE "sai"."solucao_nutriente_aud" ADD FOREIGN KEY ("cd_revisao") REFERENCES "sai"."revisao" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."tanque"
-- ----------------------------
ALTER TABLE "sai"."tanque" ADD FOREIGN KEY ("bomba_id") REFERENCES "sai"."bomba" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "sai"."tanque" ADD FOREIGN KEY ("tanquedestino_id") REFERENCES "sai"."tanque" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."tanque_aud"
-- ----------------------------
ALTER TABLE "sai"."tanque_aud" ADD FOREIGN KEY ("cd_revisao") REFERENCES "sai"."revisao" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."tecnico"
-- ----------------------------
ALTER TABLE "sai"."tecnico" ADD FOREIGN KEY ("distrito_id") REFERENCES "sai"."distrito" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "sai"."tecnico" ADD FOREIGN KEY ("usuario_id") REFERENCES "sai"."usuario" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."tecnico_aud"
-- ----------------------------
ALTER TABLE "sai"."tecnico_aud" ADD FOREIGN KEY ("cd_revisao") REFERENCES "sai"."revisao" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."testeeficiencia"
-- ----------------------------
ALTER TABLE "sai"."testeeficiencia" ADD FOREIGN KEY ("responsavel_id") REFERENCES "sai"."usuario" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "sai"."testeeficiencia" ADD FOREIGN KEY ("setor_id") REFERENCES "sai"."setor" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."testeeficiencia_aud"
-- ----------------------------
ALTER TABLE "sai"."testeeficiencia_aud" ADD FOREIGN KEY ("cd_revisao") REFERENCES "sai"."revisao" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."tubo"
-- ----------------------------
ALTER TABLE "sai"."tubo" ADD FOREIGN KEY ("fabricante_id") REFERENCES "sai"."fabricante" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."tubo_aud"
-- ----------------------------
ALTER TABLE "sai"."tubo_aud" ADD FOREIGN KEY ("cd_revisao") REFERENCES "sai"."revisao" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."usuario"
-- ----------------------------
ALTER TABLE "sai"."usuario" ADD FOREIGN KEY ("distritopadrao_id") REFERENCES "sai"."distrito" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "sai"."usuario" ADD FOREIGN KEY ("perfil_id") REFERENCES "sai"."perfil" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "sai"."usuario" ADD FOREIGN KEY ("baciapadrao_id") REFERENCES "sai"."baciahidrografica" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."usuario_aud"
-- ----------------------------
ALTER TABLE "sai"."usuario_aud" ADD FOREIGN KEY ("cd_revisao") REFERENCES "sai"."revisao" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."usuario_baciahidrografica"
-- ----------------------------
ALTER TABLE "sai"."usuario_baciahidrografica" ADD FOREIGN KEY ("id_baciahidrografica") REFERENCES "sai"."baciahidrografica" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "sai"."usuario_baciahidrografica" ADD FOREIGN KEY ("id_usuario") REFERENCES "sai"."usuario" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."usuario_distrito"
-- ----------------------------
ALTER TABLE "sai"."usuario_distrito" ADD FOREIGN KEY ("id_distrito") REFERENCES "sai"."distrito" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "sai"."usuario_distrito" ADD FOREIGN KEY ("id_usuario") REFERENCES "sai"."usuario" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."usuario_modulo"
-- ----------------------------
ALTER TABLE "sai"."usuario_modulo" ADD FOREIGN KEY ("id_usuario") REFERENCES "sai"."usuario" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "sai"."usuario_modulo" ADD FOREIGN KEY ("id_modulo") REFERENCES "sai"."modulo" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."valvula"
-- ----------------------------
ALTER TABLE "sai"."valvula" ADD FOREIGN KEY ("fabricante_id") REFERENCES "sai"."fabricante" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."valvula_aud"
-- ----------------------------
ALTER TABLE "sai"."valvula_aud" ADD FOREIGN KEY ("cd_revisao") REFERENCES "sai"."revisao" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."variedade"
-- ----------------------------
ALTER TABLE "sai"."variedade" ADD FOREIGN KEY ("cultura_id") REFERENCES "sai"."cultura" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "sai"."variedade_aud"
-- ----------------------------
ALTER TABLE "sai"."variedade_aud" ADD FOREIGN KEY ("cd_revisao") REFERENCES "sai"."revisao" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
